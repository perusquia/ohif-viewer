apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
{{ include "ohif-viewer.labels" . | indent 4 }}
  name: {{ include "ohif-viewer.fullname" . }}
spec:
  replicas: {{ .Values.replicas }}
  selector:
    matchLabels:
{{ include "ohif-viewer.matchLabels" . | indent 6 }}
  template:
    metadata:
      labels:
{{ include "ohif-viewer.labels" . | indent 8 }}
      annotations:
        checksum/configOhif: {{ include (print $.Template.BasePath "/configmap-ohif.yaml") . | sha256sum }}
        checksum/configNginx: {{ include (print $.Template.BasePath "/configmap-nginx.yaml") . | sha256sum }}
    spec:
      containers:
      - name: nginx
        image: {{ .Values.image.repository }}:{{ .Values.image.tag }}
        imagePullPolicy: {{ .Values.image.pullPolicy }}
        resources:
          requests:
            memory: {{ .Values.resources.requests.memory }}
            cpu: {{ .Values.resources.requests.cpu }}
          limits:
            memory: {{ .Values.resources.limits.memory }}
        ports:
        - name: http-port
          containerPort: 80
        volumeMounts:
        - name: nginx-config
          mountPath: /etc/nginx/conf.d
        - name: ohif-config
          mountPath: /var/www/ohif-viewer/app-config.js
          subPath: app-config.js
        {{- if .Values.branchBrowser.enabled }}
        - name: branch-data
          mountPath: /var/www/ohif-viewer/branch
        {{- end }}
        readinessProbe:
          httpGet:
            path: /ohif-viewer/manifest.webapp
            port: http-port
          failureThreshold: 2
          initialDelaySeconds: 5
          periodSeconds: 5
      {{- if .Values.branchBrowser.enabled }}
      - name: sync
        image: google/cloud-sdk
        resources:
          requests:
            memory: {{ .Values.branchBrowser.resources.requests.memory }}
            cpu: {{ .Values.branchBrowser.resources.requests.cpu }}
            ephemeral-storage: {{ .Values.branchBrowser.resources.requests.ephemeralStorage }}
          limits:
            memory: {{ .Values.branchBrowser.resources.limits.memory }}
            ephemeral-storage: {{ .Values.branchBrowser.resources.limits.ephemeralStorage }}
        workingDir: /opt/branch
        args:
        - /bin/sh
        - sync_refs.sh
        - /var/www/ohif-viewer/branch
        env:
        - name: GITLAB_TOKEN
          valueFrom:
            secretKeyRef:
              name: {{ include "ohif-viewer.fullname" . }}-branch
              key: GITLAB_TOKEN
        - name: SYNC_INTERVAL
          value: {{ .Values.branchBrowser.syncInterval | quote }}
        - name: BRANCH_ACTIVE_DAYS
          value: {{ .Values.branchBrowser.branchActiveDays | quote }}
        - name: TAG_ACTIVE_DAYS
          value: {{ .Values.branchBrowser.tagActiveDays | quote }}
        - name: PINNED_REFS
          value: {{ .Values.branchBrowser.pinnedRefs | toJson | quote }}
        volumeMounts:
        - name: branch-script
          mountPath: /opt/branch
        - name: branch-secret
          mountPath: /var/secret
        - name: branch-data
          mountPath: /var/www/ohif-viewer/branch
      {{- end }}
      affinity:
         podAntiAffinity:
           preferredDuringSchedulingIgnoredDuringExecution:
           - weight: 50
             podAffinityTerm:
               labelSelector:
                 matchExpressions:
                 - key: app.kubernetes.io/name
                   operator: In
                   values:
                   - {{ include "ohif-viewer.name" . }}
               topologyKey: {{ .Values.global.podAntiAffinityTopologyKey | default "failure-domain.beta.kubernetes.io/zone" }}
      imagePullSecrets:
      - name: regcred
      volumes:
      - name: nginx-config
        configMap:
          name: {{ include "ohif-viewer.fullname" . }}-nginx
      - name: ohif-config
        configMap:
          name: {{ include "ohif-viewer.fullname" . }}-ohif
      {{- if .Values.branchBrowser.enabled }}
      - name: branch-script
        configMap:
          name: {{ include "ohif-viewer.fullname" . }}-branch
      - name: branch-secret
        secret:
          secretName: {{ include "ohif-viewer.fullname" . }}-branch
      - name: branch-data
        emptyDir: {}
      {{- end }}
