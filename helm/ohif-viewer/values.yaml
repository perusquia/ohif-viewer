image:
  # image.pullPolicy -- policy for pulling the runtime image
  pullPolicy: IfNotPresent
  # image.repository -- Docker repository for the runtime image
  repository: 'flywheel/ohif-viewer'
  # image.tag -- image tag for the runtime image
  tag: 'master'

global:
  # global.podAntiAffinityTopologyKey -- The optional alternative key to use for anti-affinity in pod-scheduling
  podAntiAffinityTopologyKey:

ohifConfig:
  # ohifConfig.showStudyList -- show study list for projects
  showStudyList: true
  # ohifConfig.prefetchStudies -- eagerly load study images
  prefetchStudies: true
  # ohifConfig.enableFileAdapter -- enable file loader support: nifti or web image
  enableFileAdapter: true
  # ohifConfig.filterQueryParam -- enable filtering on url query param support
  filterQueryParam: true
  # ohifConfig.enableStudyLazyLoad -- enable lazy load support
  enableStudyLazyLoad: true
  # ohifConfig.enableAutoFullDynamicWWWC -- enable auto apply full dynamic wwwc
  enableAutoFullDynamicWWWC: false
  # ohifConfig.enableMeasurementTableWarning -- enable warnings on measurement table
  enableMeasurementTableWarning: false
  # ohifConfig.enableMeasurementSaveBtn -- enable measurement save btn on measurement table
  enableMeasurementSaveBtn: false
  # ohifConfig.wadoUriRoot -- path where the WADO-URI API is served
  wadoUriRoot: '/io-proxy/wado'
  # ohifConfig.qidoRoot -- path where the QIDO-RS API is served
  qidoRoot: '/io-proxy/wado'
  # ohifConfig.wadoRoot -- path where the WADO-RS API is served
  wadoRoot: '/io-proxy/wado'
  # ohifConfig.qidoSupportsIncludeField -- enables "include" query support for QIDO-RS
  qidoSupportsIncludeField: false
  # ohifConfig.imageRendering -- protocol used to render images
  imageRendering: 'wadors'
  # ohifConfig.thumbnailRendering -- protocol used to render thumbnails
  thumbnailRendering: 'wadors'

# replicas -- number of pods maintained for this deployment
replicas: 1

# servicePort -- port exposed by the service
servicePort: 80

# servicePrefix -- path where the service is mounted
servicePrefix: /ohif-viewer

contentSecurityPolicy:
  # contentSecurityPolicy.frameAncestors -- which origins can load this app in an iframe
  frameAncestors:
    - "'self'" # same origin, single quotes mandatory

# resources sets the resource requests and limits for the pod
resources:
  # requests set the initial requested resources
  requests:
    # resources.requests.memory -- requested memory for the pod
    memory: '64Mi'
    # resources.requests.cpu -- requested cpu for the pod where 1.0 is one "core" or hyperthread
    cpu: '0.1'
  # limits set the absolute limits of the pod
  limits:
    # By practice, don't specify a cpu limit
    # See: https://medium.com/@betz.mark/understanding-resource-limits-in-kubernetes-cpu-time-9eff74d3161b

    # resources.limits.memory -- absolute limit of memory for the pod
    memory: '512Mi'

# branchBrowser enables the /branch route that allows browsing of recent branches and tags
branchBrowser:
  # branchBrowser.enabled -- enable the branch browser route and cronjob
  enabled: false
  # branchBrowser.gcloudServiceAccount -- service account json with read access to the fw-ohif bucket
  gcloudServiceAccount:
  # branchBrowser.gitlabToken -- access token with api scope for the Gitlab API
  gitlabToken:
  # branchBrowser.syncInterval -- interval to check for updates in minutes
  syncInterval: '5'
  # branchBrowser.branchActiveDays -- number of days back to consider a branch as active
  branchActiveDays: '7'
  # branchBrowser.tagActiveDays -- number of days back to consider a tag as active
  tagActiveDays: '90'
  # branchBrowser.pinnedRefs -- tags or branches to always show in the list
  pinnedRefs: []
  # set resource limits for the sync container
  resources:
    # requests set the initial requested resources
    requests:
      # branchBrowser.resources.requests.memory -- memory is the requested memory for the pod
      memory: "64Mi"
      # branchBrowser.resources.requests.cpu -- cpu is the requested cpu for the pod where 1.0 is one "core" or hyperthread
      cpu: "0.1"
      # branchBrowser.resources.requests.ephemeralStorage -- ephemeralStorage is the requested ephemeral storage capacity allocated to a container
      ephemeralStorage: "4Gi"
    # limits set the absolute limits of the pod
    limits:
      # branchBrowser.resources.limits.memory -- memory is the absolute limit of memory for the pod
      memory: "512Mi"
      # branchBrowser.resources.limits.ephemeralStorage -- ephemeralStorage is the absolute limit of ephemeral storage capacity allocated to a container
      ephemeralStorage: "12Gi"

# extraEnv -- Additional environment variables
extraEnv: {}
