OHIF Viewer for Flywheel
========================

This repository is a fork of https://github.com/OHIF/Viewers, adapted to integrate with the Flywheel general API and WADO service.

### Platform Modifications

We've adapted some of the core code in the `platform/` packages to be more Flywheel friendly. These changes include:
* Custom routes for project list, project study list, and NIfTI/image handlers
* Sidebar customizations using components in extension modules
* Toolbar reorganization and a persisted tool state
* Modified hotkey manager
* Study prefetcher for visible viewports

### Extension Modules

Most of our changes are isolated to modules in `extensions/`.
* @flywheel/extension-flywheel-common - Holds common importables that are used in other extensions
  * API clients
  * Static utility functions
  * Custom Cornerstone tools
  * Redux modules
  * RxJS utilities
* @flywheel/extension-flywheel - Establishes the integration with the Flywheel platform
  * Initialization routine for reading query parameters and fetching Flywheel data
  * Initialization for Cornerstone event listerners and modifications
  * Redux reducer registration
* @flywheel/extension-cornerstone-infusions - Cornerstone viewport modifications that are independent of Flywheel
  * Tools: crosshair/scout/reference lines, viewport scroll/wwwc/zoom link
  * Color maps
  * Hanging protocols
* @flywheel/extension-cornerstone-nifti - Support for the NIfTI file loader
  * SOP class handler, commands, and tools for NIfTI viewports
* @flywheel/extension-flywheel-dicom-siemens - Support for Siemens image features
  * Mosaic image slicing

Getting Started
---------------

You'll need to install
* Node.js (14+)
* Yarn

Install packages
```shell
yarn
```

The simplist way to integrate with an environment is to proxy through the Webpack dev server:
```shell
PROXY_HOST=core-test.staging.flywheel.io yarn dev:flywheel
```

The application will be available at `http://localhost:3000`.

You'll need to get a user token from the remote environment and set it in your localhost storage. This token is in local storage under the `satellizer_token` key.
```js
// in remote environment
localStorage.getItem('satellizer_token');

// in local environment
localStorage.setItem('satellizer_token', '<copied token>');
```

Here is a Tampermonkey/Greasemonkey script to automate this somewhat: https://gitlab.com/snippets/1997392

Uploading Data
--------------

Use the DICOM uploader page (e.g. https://core-test.staging.flywheel.io/#/dicom-uploader/) in the proxied environment to upload DICOM files into a project. The new studies should be indexed in a few seconds and available in the study list within the OHIF viewer.

NIfTI and image files need to be uploaded to a container in the Flywheel interface. They can be uploaded as acquisition data or as container attachments.

Translating Flywheel Data to Local Dev Routes
---------------------------------------------

When viewing a particular study/file in the proxied environment, click Open in New Tab to get the current route. Copy everything after `/ohif-viewer` to get the route for your localhost server. For example,
```
https://frontend.dev.flywheel.io/ohif-viewer/project/5f3bdde6470f3c04b6635712/viewer/1.3.6.1.4.1.34692.183032025641642497355098000276096643903/1.2.840.113619.2.354.3.50990086.808.1585054551.671.3
=>
http://localhost:3000/project/5f3bdde6470f3c04b6635712/viewer/1.3.6.1.4.1.34692.183032025641642497355098000276096643903/1.2.840.113619.2.354.3.50990086.808.1585054551.671.3
```
