#!/bin/bash

if ! helm list 2>/dev/null | grep flywheel >/dev/null; then
  echo "A Helm release for the Flywheel umbrella chart was not found. A release must be installed in order to be patched for local development."
  exit 1
fi

if kubectl get service flywheel-ohif-viewer >/dev/null 2>&1; then
  # loosen the service selector a little bit
  kubectl set selector service flywheel-ohif-viewer 'app.kubernetes.io/instance=flywheel,app.kubernetes.io/name=ohif-viewer'
fi
