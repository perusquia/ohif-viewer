import DICOMDict from './DICOMDict';
import * as ImageIdUtils from './imageId';
import getTypedArrayConstructor from './getTypedArrayConstructor';
import getMaximumExtractableTilesFromMosaicImage from './getMaximumExtractableTilesFromMosaicImage';

const utils = {
  DICOMDict,
  ImageIdUtils,
  getTypedArrayConstructor,
  getMaximumExtractableTilesFromMosaicImage,
};

export default utils;
