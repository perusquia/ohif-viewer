import { cloneDeep } from 'lodash';

export default function getMaximumExtractableTilesFromMosaicImage(
  sopInstance,
  mosaicTileSizeStrategy
) {
  const clonedSopInstance = cloneDeep(sopInstance);
  mosaicTileSizeStrategy(clonedSopInstance);

  return (
    parseInt(sopInstance.Rows / clonedSopInstance.Rows, 10) *
    parseInt(sopInstance.Columns / clonedSopInstance.Columns, 10)
  );
}
