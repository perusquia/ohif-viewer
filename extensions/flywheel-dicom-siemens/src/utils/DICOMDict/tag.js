import _ from 'lodash';

export const createTag = (name, vr, Value) => {
  const arrayValue = Array.isArray(Value) ? Value : [Value];
  return {
    [name]: {
      vr,
      Value: arrayValue,
    },
  };
};

export const updateTagValue = (instance, name, Value, vr) => {
  (instance[name] || {}).Value = Value;
};

export const exchangeTagValue = (source, mapTagName) => {
  const result = {};
  Object.keys(mapTagName).forEach(toKey => {
    const fromKey = mapTagName[toKey];
    result[toKey] = source[fromKey];
  });
  return result;
};

export const exchangeTagName = (items, mapTagName) => {
  return _.mapKeys(items, (value, key) => mapTagName[key] || key);
};
