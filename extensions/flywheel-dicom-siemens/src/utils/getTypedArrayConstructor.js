/** Get typed array constructor of given typed array
 * @param {TypedArray} array - instance of a typed array
 * @returns Constructor of the TypedArray of input
 */
export default function getTypedArrayConstructor(array) {
  if (array instanceof Int8Array) return Int8Array;
  if (array instanceof Int16Array) return Int16Array;
  if (array instanceof Uint8Array) return Uint8Array;
  if (array instanceof Uint16Array) return Uint16Array;
  if (array instanceof Uint8ClampedArray) return Uint8ClampedArray;
  if (array instanceof Uint32Array) return Uint32Array;
  if (array instanceof Int32Array) return Int32Array;
  if (array instanceof Float32Array) return Float32Array;
  if (array instanceof Float64Array) return Float64Array;
  if (array instanceof BigInt64Array) return BigInt64Array;
  if (array instanceof BigUint64Array) return BigUint64Array;
}
