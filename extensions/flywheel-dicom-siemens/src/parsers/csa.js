import OHIF from '@ohif/core';

const N_TAGS_MIN = 1;
const N_TAGS_MAX = 128;
const NAME_BYTES_LENGTH = 64;
const VR_BYTES_LENGTH = 4;
const VR_LENGTH = 3;

class CSA {
  constructor(byteStream) {
    this.byteStream = byteStream;
    this.csaMaxPos = byteStream.byteArray.length;
    this.parsedObj = {};
  }

  /** abstract methods */
  readHeader() {}
  readItemLength() {}
  itemLengthValidation() {}
  /** abstract methods end */

  nTagsValidation() {
    if (this.nTags <= N_TAGS_MIN || this.nTags > N_TAGS_MAX) {
      throw new Error('Invalid nTags');
    }
  }

  skipBytes(bytes) {
    if (bytes) {
      this.byteStream.readByteStream(bytes);
    }
  }

  itemLengthValidation(itemLength) {
    return false;
  }

  parsedObjValidation() {
    return true;
  }

  isAtEnd() {
    return this.byteStream.position > this.csaMaxPos;
  }
  readTag() {
    if (this.isAtEnd()) {
      return;
    }

    if (!this.parsedObjValidation()) {
      return;
    }

    const name = this.byteStream.readFixedString(NAME_BYTES_LENGTH);
    const vm = this.byteStream.readUint32();
    const vr = (
      this.byteStream.readFixedString(VR_BYTES_LENGTH) || ''
    ).substring(0, VR_LENGTH);
    const syngodt = this.byteStream.readUint32();
    const nItems = this.byteStream.readUint32();

    this.parsedObj[name] = {
      Value: [],
      vr,
      vm,
      name,
    };

    for (let it = 0; it < nItems; it++) {
      const itemValue = this.readTagItem(vr);
      this.parsedObj[name].push(itemValue);
    }

    readTag();
  }

  readTagItem(vr) {
    const itemLength = this.readItemLength();

    this.itemLengthValidation(itemLength);

    return this.byteStream.readFixedString(itemLength);
  }

  readPrivateTag() {
    this.readHeader();
    this.nTags = this.byteStream.readUint32();
    try {
      this.nTagsValidation();
      this.skipBytes(32);
      this.readTag();

      return this.parsedObj;
    } catch (e) {
      OHIF.log.warn('Error while reading CSA private tag');
      return;
    }
  }
}

export default CSA;
