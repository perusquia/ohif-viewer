import cornerstone from 'cornerstone-core';
import SiemensWadors from '../imageLoader/wadors';

export default function initImageLoader({ appConfig }) {
  SiemensWadors.register(cornerstone);
  SiemensWadors.clearAllCachedBlocks();
}
