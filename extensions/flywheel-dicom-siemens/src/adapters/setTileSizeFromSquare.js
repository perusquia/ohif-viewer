import dicomDict from '../utils/DICOMDict';

export default function setTileSizeFromSquare(sopInstance) {
  let totalImages = 1;
  if (Array.isArray(sopInstance.SourceImageSequence)) {
    totalImages = sopInstance.SourceImageSequence.length;
  } else {
    // try getting from siemens private tag
    totalImages =
      parseInt(
        sopInstance[dicomDict.dict.DICOMDescriptionsTag.NumberOfImagesInMosaic]
      ) || totalImages;
  }
  // find number of tiles in a row, "assuming" mosaic image contains NxN tiles
  const nRowBlocks = Math.ceil(Math.sqrt(totalImages));
  // update col and row based on number of tiles in a row
  sopInstance.Rows = sopInstance.Rows / nRowBlocks;
  sopInstance.Columns = sopInstance.Columns / nRowBlocks;
}
