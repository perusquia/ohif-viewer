import dicomDict from '../utils/DICOMDict';
import OHIF from '@ohif/core';
import setTileSizeFromAcquisitionMatrix from './setTileSizeFromAcquisitionMatrix';
import setTileSizeFromSiemensResolutionTag from './setTileSizeFromSiemensResolutionTag';
import setTileSizeFromSquare from './setTileSizeFromSquare';
import Utils from '../utils';

const { DICOMDict, getMaximumExtractableTilesFromMosaicImage } = Utils;
/**
 * It splits siemens study instance into multiple instances.
 * For that it changes some tags properly
 * @param {Array<Object>} originalSopInstanceList list of instance (tags not naturalized)
 * @param {Object} study given study
 */
export default function makeSopInstanceList(
  originalSopInstanceList = [],
  study
) {
  const instances = [];
  if (study.Manufacturer === 'SIEMENS') {
    // TODO to implement an async strategy to load and parse privateTags
    //const privateTagBuffer = await fetch('http://localhost:8080/dcm4chee-arc/aets/DCM4CHEE/rs/studies/1.3.12.2.229160.139453.541621.891114.010558.362859.30000017090118453783600000028/series/1.3.12.2.254117.186192.504315.913320.576141.541752.0/instances/1.3.12.2.485121.245121.517713.916380.141212.121196.201709060818064995304049/bulkdata/00291020').then(response => response.arrayBuffer());
    //const parsedTags = CSATagParser(privateTagBuffer);
    originalSopInstanceList.forEach(sopInstance => {
      splitSopInstance(instances, sopInstance);
    });
  } else {
    instances.push(...originalSopInstanceList);
  }
  return instances;
}

// If there is no unique identifier for frames in a mosaic, create unique ids
// per frame by appending frameIndex number to common SOPInstanceUID
const makeDynamicSOPInstanceUIDs = sopInstance => {
  const totalTileCount = parseInt(
    sopInstance[dicomDict.dict.DICOMDescriptionsTag.NumberOfImagesInMosaic]
  );
  if (totalTileCount > 1) {
    const sourceImageSequence = [];
    for (let frameIndex = 0; frameIndex < totalTileCount; frameIndex++) {
      sourceImageSequence.push({
        ReferencedSOPClassUID: sopInstance.SOPClassUID,
        ReferencedSOPInstanceUID: `${sopInstance.SOPInstanceUID}.${frameIndex}`,
      });
    }
    sopInstance.SourceImageSequence = sourceImageSequence;
  }
};

const splitSopInstance = (instances, sopInstance) => {
  if (!Array.isArray(sopInstance.SourceImageSequence)) {
    makeDynamicSOPInstanceUIDs(sopInstance);
  }

  const mapReferenceKey = {
    ['ReferencedSOPClassUID']: 'SOPClassUID',
    ['ReferencedSOPInstanceUID']: 'SOPInstanceUID',
  };

  const exchangeTagsName = sequenceItem => {
    return DICOMDict.tag.exchangeTagName(sequenceItem, mapReferenceKey);
  };

  const createNewTags = instance => {
    const sourceObj = DICOMDict.tag.exchangeTagValue(instance, mapReferenceKey);
    instance.SourceInstanceSequence = sourceObj;
  };

  const createNewItemTags = index => {
    return { [DICOMDict.dict.DICOMDescriptionsTag.CSASequenceIndex]: index };
  };

  let mosaicTileSizeStrategy;
  if (sopInstance[dicomDict.dict.DICOMDescriptionsTag.InPlaneResolution]) {
    mosaicTileSizeStrategy = setTileSizeFromSiemensResolutionTag;
  } else if (sopInstance.AcquisitionMatrix) {
    mosaicTileSizeStrategy = setTileSizeFromAcquisitionMatrix;
  } else if (
    sopInstance.ImageType &&
    sopInstance.ImageType.includes('MOSAIC')
  ) {
    mosaicTileSizeStrategy = setTileSizeFromSquare;
  } else {
    instances.push(sopInstance); // do not split and push original instance
    return;
  }

  const updateCurrentTags = sopInstance => {
    // update col and row
    mosaicTileSizeStrategy(sopInstance);
  };
  // read the updated sequence value
  const sequence = sopInstance.SourceImageSequence;

  const numberOfImagesInMosaic =
    sopInstance[dicomDict.dict.DICOMDescriptionsTag.NumberOfImagesInMosaic] ||
    sequence?.length;
  let maximumExtractableTilesFromMosaicImage = 0;
  if (numberOfImagesInMosaic > 1) {
    maximumExtractableTilesFromMosaicImage = getMaximumExtractableTilesFromMosaicImage(
      sopInstance,
      mosaicTileSizeStrategy
    );
  }

  if (
    numberOfImagesInMosaic > 1 &&
    numberOfImagesInMosaic <= maximumExtractableTilesFromMosaicImage
  ) {
    updateCurrentTags(sopInstance);
    createNewTags(sopInstance);
    const firstItem = sequence[0];
    for (let index = 0; index < numberOfImagesInMosaic; index++) {
      const item = sequence[index] ?? firstItem;
      const instance = {
        ...sopInstance,
        ...exchangeTagsName(item),
        ...createNewItemTags(index),
        getImageId: function() {
          const wadorsId = OHIF.utils.getWADORSImageId(this, index);
          return Utils.ImageIdUtils.replaceToSiemensIdScheme(wadorsId);
        },
      };

      instances.push(instance);
    }
  } else {
    instances.push(sopInstance); // do not split and push original instance
  }
};
