import throttle from 'lodash.throttle';
import {
  vtkInteractorStyleMPRWindowLevel,
  vtkInteractorStyleMPRRotate,
  vtkInteractorStyleMPRSlice,
} from 'react-vtkjs-viewport';

import vtkSVGCrosshairIndicatorWidget from './vtkSVGCrosshairIndicatorWidget';
import vtkSVGDetachedRotatableCrosshairWidget from './vtkSVGDetachedRotatableCrosshairWidget';
import vtkInteractorStyleDetachedRotatableMPRCrosshairs from './vtkInteractorStyleDetachedRotatableMPRCrosshairs';
import vtkInteractorStyleMPRFovIndicators from './vtkInteractorStyleMPRFovIndicators';
import vtkColorTransferFunction from 'vtk.js/Sources/Rendering/Core/ColorTransferFunction';
import vtkColorMaps from 'vtk.js/Sources/Rendering/Core/ColorTransferFunction/ColorMaps';
import { getImageData } from 'react-vtkjs-viewport';
import { vec2, vec3 } from 'gl-matrix';
import cornerstone from 'cornerstone-core';
import csTools from 'cornerstone-tools';
import setMPRLayout from './utils/setMPRLayout.js';
import setProtocolLayout from './utils/setProtocolLayout.js';
import setMultiViewMPRLayout from './utils/setMultiViewMPRLayout';
import setViewportToVTK from './utils/setViewportToVTK.js';
import Constants from 'vtk.js/Sources/Rendering/Core/VolumeMapper/Constants.js';
import OHIFVTKViewport from './OHIFVTKViewport';
import {
  syncTargetApis,
  distanceFromLine,
  moveCrosshairs,
  getCustomInteractorStyleDefinitions,
  addVerticalListener,
  addScrollListener,
  getPosition,
  registerSyncCallback,
  getWorldPosFromDisplayPos,
  setNewSlice,
  setWindowLevel,
  getFullDynamicWWWC,
  fitToViewport,
} from './utils/vtkJsInteractorSupport.js';
import { indicatorUtils, bSliceUtils, fovUtils } from './utils/Indicator';
import { setFOVCenter } from './utils/setFOVCenter.js';
import { INTERACTOR_NAMES, INDICATOR_TYPES } from './utils/constants.js';
import {
  isCrosshairStyle,
  getInteractorClassName,
  getVolumeRadius,
  validateScale,
} from './utils/vtkUtils';
import { getCurrentDisplayState } from './utils/displayPropertyUtils.js';
import {
  getMatchingVTKViewportData,
  convertPlanesDataFrom2DToVtkjsOrientation,
  convertPlanesDataFromVtkjsOrientationTo2D,
} from './utils/viewportUtils.js';
import InteractionPresets from 'vtk.js/Sources/Interaction/Style/InteractorStyleManipulator/Presets';
import Interaction from './Interaction';
import manipulators from './utils/manipulators';
import {
  getMultiSessionViewCount,
  getMultiSessionViewStatus,
  getMultiSessionViewSeries,
} from './utils/getMultiSessionViewData';
import setLayoutAndViewportData from './utils/setLayoutAndViewportData';
import {
  Redux as FlywheelRedux,
  Utils as FlywheelUtils,
} from '@flywheel/extension-flywheel';
import vtkCoordinate from 'vtk.js/Sources/Rendering/Core/Coordinate';
import OHIF, { utils, log } from '@ohif/core';
import {
  Redux as FlywheelCommonRedux,
  Utils,
} from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';
import { getWindowingDifference } from './utils/computeWindowing';
import { cornerstoneUtils } from '@flywheel/extension-cornerstone-infusions';
import { DEFAULT_COLOR_ID } from './toolbarComponents/colorMap';

const imagePointToPatientPoint = csTools.import(
  'util/imagePointToPatientPoint'
);

const { setActiveVtkTool, setFovGridRestoreInfo } = FlywheelCommonRedux.actions;
const { selectFovGridRestoreInfo } = FlywheelCommonRedux.selectors;
const {
  getCondensedProjectConfig,
  isCurrentFile,
  getAllFovealRois,
  getDefaultROILabelFromHPLayout,
  getRoiCenterPoint,
  getDisplaySetForSeries,
} = Utils;
const { studyMetadataManager } = utils;
const { resetLayoutToHP } = FlywheelUtils;
const { setViewportActive } = OHIF.redux.actions;
const {
  selectProjectConfig,
  selectActiveProtocolStore,
} = FlywheelRedux.selectors;
const { BlendMode } = Constants;

const commandsModule = ({ commandsManager, UINotificationService }) => {
  // TODO: Put this somewhere else
  let mprApis = {};
  let defaultVOI;
  let isMprApisInitialized = false;
  let initialReferenceLinePoints = {};
  let activeApiData = null;
  let isInSingleView = false;
  let restoreLayoutData = [];
  let viewportLayoutData = {
    viewPortIndex: 0,
    doubleClickedSeriesInstanceUID: null,
  };
  let fovGrid = {};

  function setLayoutToSingleOrMultiple(isSingleView) {
    // This is work around fix to delay for getting new viewports after unmounting the existing viewports.
    setTimeout(async () => {
      let displaySet = restoreLayoutData[0].activeDisplaySet;
      const multiSessionIndex = restoreLayoutData.findIndex(
        item =>
          item.activeDisplaySet.SeriesInstanceUID ===
          viewportLayoutData.doubleClickedSeriesInstanceUID
      );
      if (isSingleView) {
        if (restoreLayoutData.length > 1 && multiSessionIndex >= 0) {
          displaySet = restoreLayoutData[multiSessionIndex].activeDisplaySet;
        }

        const viewportProps = [
          {
            orientation:
              restoreLayoutData[multiSessionIndex].orientation[
                viewportLayoutData.viewPortIndex
              ],
          },
        ];

        await createMPR2DLayout([displaySet], viewportProps, false, 1);
        const apis = mprApis[displaySet.SeriesInstanceUID];

        const slice = restoreLayoutData[multiSessionIndex].slice;
        const pScale = restoreLayoutData[multiSessionIndex].parallelScale;
        const slabThickness =
          restoreLayoutData[multiSessionIndex].slabThickness;

        // Set default interactorStyle of each viewport.
        apis.forEach((api, apiIndex) => {
          api.addSVGWidget(
            vtkSVGCrosshairIndicatorWidget.newInstance(),
            'crosshairsWidget'
          );
          const uid = api.uid;
          let istyle = null;
          if (!restoreLayoutData[multiSessionIndex].isCrosshairEnabled) {
            api.svgWidgets.crosshairsWidget.setDisplay(false);
          } else {
            api.svgWidgets.crosshairsWidget.setDisplay(true);
            istyle = vtkInteractorStyleMPRFovIndicators.newInstance();

            api.setInteractorStyle({
              istyle,
              configuration: { apis, apiIndex, uid },
            });
            setMouseConfigAfterToolEnabled(istyle, api, apis, apiIndex);
          }
        });
        if (!restoreLayoutData[multiSessionIndex].isCrosshairEnabled) {
          setMouseConfig();
        }

        apis.forEach((api, apiIndex) => {
          api.genericRenderWindow
            .getInteractor()
            .getInteractorStyle()
            .setSlice(slice);
          api.genericRenderWindow
            .getRenderer()
            .getActiveCamera()
            .setParallelScale(pScale);
          api.setSlabThickness(slabThickness);
          api.svgWidgets.crosshairsWidget.resetCrosshairs(apis, 0);
          api.genericRenderWindow.getRenderWindow().render();
        });
        const worldPos = restoreLayoutData[multiSessionIndex].worldPos;
        apis[0].svgWidgets.crosshairsWidget.moveCrosshairs(worldPos, apis);
      } else {
        let viewportProps = [];
        const displaySets = [];
        restoreLayoutData.forEach(layoutData => {
          displaySets.push(layoutData.activeDisplaySet);
          layoutData.orientation.forEach(orientation => {
            const vpOrientation = {
              orientation,
            };
            viewportProps.push(vpOrientation);
          });
        });
        const numberOfRowsForViewports = displaySets.length;

        await createMPR2DLayout(
          displaySets,
          viewportProps,
          restoreLayoutData.length > 1
        );

        let viewPortIndex = 0;
        Object.keys(mprApis).forEach((key, keyIndex) => {
          const apis = mprApis[key];
          const curRestoreLayout = restoreLayoutData[keyIndex];
          // Add widgets and set default interactorStyle of each viewport.
          apis.forEach((api, apiIndex) => {
            api.addSVGWidget(
              vtkSVGDetachedRotatableCrosshairWidget.newInstance(),
              'rotatableCrosshairsWidget'
            );

            if (curRestoreLayout.isCrosshairEnabled) {
              const uid = api.uid;
              const istyle = vtkInteractorStyleDetachedRotatableMPRCrosshairs.newInstance();
              api.setInteractorStyle({
                istyle,
                configuration: { apis, apiIndex, uid },
              });
              setMouseConfigAfterToolEnabled(istyle, api, apis, apiIndex);
            }

            api.svgWidgets.rotatableCrosshairsWidget.setApiIndex(apiIndex);
            api.svgWidgets.rotatableCrosshairsWidget.setApis(apis);
          });
        });

        if (!restoreLayoutData[multiSessionIndex].isCrosshairEnabled) {
          setMouseConfig();
        }

        Object.keys(mprApis).forEach((key, keyIndex) => {
          const apis = mprApis[key];
          const curRestoreLayout = restoreLayoutData[keyIndex];

          apis.forEach((api, apiIndex) => {
            // Set active/inactive viewport status to the widget.
            api.svgWidgets.rotatableCrosshairsWidget.setViewportActive(
              curRestoreLayout.activeCrossHairViewports[apiIndex]
            );
            api.svgWidgets.rotatableCrosshairsWidget.setBorder(
              getWidgetBorderStyle(
                curRestoreLayout.allCrossHairViewportsActive,
                curRestoreLayout.activeCrossHairViewports[apiIndex],
                viewPortIndex,
                numberOfRowsForViewports
              )
            );
            viewPortIndex++;

            if (!curRestoreLayout.isCrosshairEnabled) {
              api.svgWidgets.rotatableCrosshairsWidget.setDisplay(false);
            } else {
              api.svgWidgets.rotatableCrosshairsWidget.setDisplay(true);
            }
            activeApiData = null;
          });
          apis[
            viewportLayoutData.viewPortIndex
          ].svgWidgets.rotatableCrosshairsWidget.resetCrosshairs(apis, 0);
          apis[viewportLayoutData.viewPortIndex].genericRenderWindow
            .getInteractor()
            .getInteractorStyle()
            .setSlice(restoreLayoutData[keyIndex].slice);
          apis.forEach((api, apiIndex) => {
            const pScale = curRestoreLayout.parallelScale;
            api.genericRenderWindow
              .getRenderer()
              .getActiveCamera()
              .setParallelScale(pScale);
            api.setSlabThickness(curRestoreLayout.slabThickness);
            api.genericRenderWindow.getRenderWindow().render();
          });
          const renderer = apis[
            viewportLayoutData.viewPortIndex
          ].genericRenderWindow.getRenderer();
          if (!restoreLayoutData[multiSessionIndex].isCrosshairEnabled) {
            moveCrosshairs(
              apis,
              viewportLayoutData.viewPortIndex,
              curRestoreLayout.position,
              renderer
            );
          } else {
            const worldPos = restoreLayoutData[multiSessionIndex].worldPos;
            apis[
              viewportLayoutData.viewPortIndex
            ].svgWidgets.rotatableCrosshairsWidget.moveCrosshairs(
              worldPos,
              apis
            );
          }
        });
        registerSyncSessionApiEvents(true);
      }
    }, 0);
  }

  function setFOVCenterWithAnnotation(
    annotationForFovGrid,
    displaySet,
    fovCenter
  ) {
    const fovAnnotationImgIndex = annotationForFovGrid[0].sliceNumber - 1;
    const fovAnnotationPoint = getRoiCenterPoint(annotationForFovGrid[0]);
    const imageId = displaySet.isMultiFrame
      ? displaySet.images[0].getImageId(fovAnnotationImgIndex)
      : displaySet.images[fovAnnotationImgIndex].getImageId();
    const imagePlane = cornerstone.metaData.get('imagePlaneModule', imageId);
    setFOVCenter(fovCenter, imagePlane, fovAnnotationPoint);
  }

  function updateFOVGrid(propertiesToSync) {
    const defaultRoiLabels = propertiesToSync.defaultRoiLabels;
    const requiresUpdate = propertiesToSync.requiresUpdate || false;

    if (defaultRoiLabels?.length > 0) {
      const state = store.getState();
      const projectConfig = getCondensedProjectConfig();
      const sliceSettings = projectConfig?.bSlices?.settings;
      const defaultRoiLabelDetails = getDefaultROILabelFromHPLayout();

      Object.keys(mprApis).forEach(key => {
        const apis = mprApis[key];
        const studies = studyMetadataManager.all();
        const study = studies.find(stdy =>
          stdy.getData().series.some(s => s.SeriesInstanceUID === key)
        );
        const displaySet = getDisplaySetForSeries(study, key);
        const defaultRoiLabelsForSeries = defaultRoiLabelDetails[key];

        const imageIds =
          OHIFVTKViewport.getCornerstoneStackFromDisplaySet(
            study.getData(),
            displaySet
          )?.imageIds || [];
        const fovDetail = fovGrid.fovDetail[key];

        const restoreInfo = selectFovGridRestoreInfo(state);
        const bSliceIndicatorDataInWorld = !isCurrentFile(state)
          ? bSliceUtils.getBSliceIndicatorDataInWorld(
              sliceSettings,
              imageIds,
              apis[0].genericRenderWindow.getRenderer()
            ) || []
          : [];

        const apiList = [];
        apis.forEach(api => {
          if (
            api.genericRenderWindow
              .getRenderWindow()
              ?.getInteractor()
              ?.getInteractorStyle()
          ) {
            apiList.push(api);
          }
        });

        let fovCenter = [...fovGrid.fovGridCenter?.[key]];
        const annotations = getAllFovealRois(defaultRoiLabelsForSeries);
        const annotationsForSeries = annotations.filter(
          annotation => annotation.SeriesInstanceUID === key
        );
        const annotationForFovGrid = annotationsForSeries;

        if (annotationForFovGrid?.length === 1) {
          setFOVCenterWithAnnotation(
            annotationForFovGrid,
            displaySet,
            fovCenter
          );
        }

        apis.forEach((api, apiIndex) => {
          // Initialize the indicators world position data in svg widget and render it
          let indicatorDataInWorld = [].concat(bSliceIndicatorDataInWorld);

          if (fovDetail && fovDetail[apiIndex]) {
            const fovIndicatorData =
              fovUtils.getFovIndicatorDataInWorld(
                fovCenter,
                fovDetail[apiIndex].fovDiameters,
                fovDetail[apiIndex].hideFovGrid,
                api,
                restoreInfo.isCenterEnabled
              ) || [];
            indicatorDataInWorld = indicatorDataInWorld.concat(
              fovIndicatorData
            );

            if (restoreInfo.selectedFovIndex >= 0) {
              fovUtils.updateFovIndicatorSelection(
                indicatorDataInWorld,
                restoreInfo.selectedFovIndex
              );
            }
          }

          if (indicatorDataInWorld?.length) {
            api.svgWidgets.crosshairsWidget.setIndicatorsDataInWorld(
              indicatorDataInWorld
            );
            api.svgWidgets.crosshairsWidget.adjustIndicators(apiList);
            if (annotationForFovGrid?.length >= 1 || requiresUpdate) {
              updateOtherFovIndicators(api, apiIndex, true);
            }
          }
        });
      });
    }
  }

  function updateOtherFovIndicators(api, apiIndex, need2DUpdate) {
    const viewports = store.getState().viewports;
    const crosshairsWidget = api.svgWidgets.crosshairsWidget;
    const indicatorsDataInWorld = crosshairsWidget.getIndicatorsDataInWorld();
    const fovReferencesInfo = fovUtils.getFovIndicatorPointsInWorld(
      indicatorsDataInWorld,
      apiIndex,
      api,
      true
    );
    const tools = ['StackScroll'];

    fovReferencesInfo.need2DUpdate = need2DUpdate;

    updateOtherViewports(viewports, api, tools, null, fovReferencesInfo);
  }

  async function _getActiveViewportVTKApi(viewports) {
    const {
      numRows,
      numColumns,
      layout,
      viewportSpecificData,
      activeViewportIndex,
    } = viewports;

    const currentData = layout.viewports[activeViewportIndex];
    if (currentData && currentData.plugin === 'vtk') {
      // TODO: I was storing/pulling this from Redux but ran into weird issues
      const seriesInstanceUID =
        viewportSpecificData[activeViewportIndex].SeriesInstanceUID;
      const viewportIndex = getApiIndex(viewports);
      return mprApis[seriesInstanceUID].apis[viewportIndex];
    }

    const displaySet = viewportSpecificData[activeViewportIndex];
    let api;
    if (!api) {
      try {
        api = await setViewportToVTK(
          displaySet,
          activeViewportIndex,
          numRows,
          numColumns,
          layout,
          viewportSpecificData
        );
      } catch (error) {
        throw new Error(error);
      }
    }

    return api;
  }

  function _setView(api, sliceNormal, viewUp) {
    const renderWindow = api.genericRenderWindow.getRenderWindow();
    const istyle = renderWindow.getInteractor().getInteractorStyle();
    istyle.setSliceNormal(...sliceNormal);
    istyle.setViewUp(...viewUp);

    renderWindow.render();
  }

  // To create the 2D MPR layout by creating vtk viewport apis.
  // Number of rows is skipped as it will be count of display sets.
  async function createMPR2DLayout(
    displaySets,
    viewportProps,
    isMultiSessionViewEnabled,
    numColumns = 3,
    isFusionMode = false
  ) {
    try {
      if (isMultiSessionViewEnabled) {
        mprApis = await setMultiViewMPRLayout(
          displaySets,
          viewportProps,
          displaySets.length,
          numColumns,
          isFusionMode
        );
      } else {
        const apis = await setMPRLayout(
          displaySets[0],
          viewportProps,
          1,
          numColumns,
          isFusionMode
        );
        mprApis[displaySets[0].SeriesInstanceUID] = apis;
      }
    } catch (error) {
      throw new Error(error);
    }
  }

  // Perform the 2D MPR mode initialization
  async function createAndInitializeMPR2DLayout(
    viewports,
    viewportProps = null,
    isFusionMode = false
  ) {
    // TODO push a lot of this backdoor logic lower down to the library level.
    initializeMprAttributes();
    const displaySet =
      viewports.viewportSpecificData[viewports.activeViewportIndex];

    defaultVOI = {};

    viewportProps = viewportProps || [
      {
        //Axial
        orientation: {
          sliceNormal: [0, 0, 1],
          viewUp: [0, -1, 0],
        },
      },
      {
        // Sagittal
        orientation: {
          sliceNormal: [-1, 0, 0],
          viewUp: [0, 0, 1],
        },
      },
      {
        // Coronal
        orientation: {
          sliceNormal: [0, 1, 0],
          viewUp: [0, 0, 1],
        },
      },
    ];
    const isMultiSessionViewEnabled = getMultiSessionViewStatus();
    const displaySets = [];
    if (isMultiSessionViewEnabled) {
      Object.keys(viewports.viewportSpecificData).forEach(key => {
        if (
          !displaySets.find(
            ds =>
              ds.displaySetInstanceUID ===
              viewports.viewportSpecificData[key].displaySetInstanceUID
          )
        ) {
          const ds = viewports.viewportSpecificData[key];
          displaySets.push(ds);
          // Get current VOI if cornerstone viewport.
          const cornerstoneVOI = getVOIFromCornerstoneViewport(false, ds);
          defaultVOI[ds.SeriesInstanceUID] = cornerstoneVOI;
        }
      });
      viewportProps.push(viewportProps[0]);
      viewportProps.push(viewportProps[1]);
      viewportProps.push(viewportProps[2]);
    } else {
      // Get current VOI if cornerstone viewport.
      const cornerstoneVOI = getVOIFromCornerstoneViewport();
      defaultVOI[displaySet.SeriesInstanceUID] = cornerstoneVOI;
      displaySets.push(displaySet);
    }
    await createMPR2DLayout(
      displaySets,
      viewportProps,
      isMultiSessionViewEnabled,
      3,
      isFusionMode
    );

    Object.keys(defaultVOI).forEach(key => {
      if (defaultVOI[key]) {
        setVOI(defaultVOI[key], key);
      }
    });
    initializeMPRApis(true, isMultiSessionViewEnabled);
    isMprApisInitialized = true;
    registerSyncSessionApiEvents(true);
  }

  function getVOIFromCornerstone(cornerstoneElement) {
    if (cornerstoneElement && cornerstoneElement.image) {
      const imageId = cornerstoneElement.image.imageId;

      const Modality = cornerstone.metaData.get('Modality', imageId);

      if (Modality !== 'PT') {
        const { windowWidth, windowCenter } = cornerstoneElement.viewport.voi;

        return {
          windowWidth,
          windowCenter,
        };
      }
    }
  }

  function getVOIFromCornerstoneViewport(
    isProtocolLayout = false,
    displaySet = null
  ) {
    if (!isProtocolLayout && !displaySet) {
      const dom = commandsManager.runCommand('getActiveViewportEnabledElement');
      const cornerstoneElement = dom
        ? cornerstone.getEnabledElement(dom)
        : null;
      if (
        cornerstoneElement &&
        cornerstone
          .getEnabledElements()
          .find(enabledElement => enabledElement === cornerstoneElement)
      ) {
        return getVOIFromCornerstone(cornerstoneElement);
      }
    } else if (displaySet) {
      // Retrieve the windowing properties from the same series cornerstone viewport for initializing
      // the vtk apis in protocol layout.
      const { StudyInstanceUID, SeriesInstanceUID, dataProtocol } = displaySet;

      return getVOIFromCornerstone(
        getCornerstoneElement(StudyInstanceUID, SeriesInstanceUID, dataProtocol)
      );
    }
  }

  function updateViewWithImage(viewports, displaySet, imageId) {
    const { StudyInstanceUID, SeriesInstanceUID } = displaySet;

    const imagePlane = cornerstone.metaData.get('imagePlaneModule', imageId);

    if (imagePlane?.imagePositionPatient) {
      const patientPos = imagePointToPatientPoint(
        {
          x: Math.round(imagePlane.columns / 2),
          y: Math.round(imagePlane.rows / 2),
        },
        imagePlane
      );
      actions.updateViewportProperties({
        viewports,
        propertiesToSync: {
          studyInstanceUID: StudyInstanceUID,
          seriesInstanceUID: SeriesInstanceUID,
          patientPos,
        },
      });
    }
  }

  function getCornerstoneElement(
    studyInstanceUID,
    seriesInstanceUID,
    dataProtocol
  ) {
    let cornerstoneElement = null;
    cornerstone.getEnabledElements().every(enabledElement => {
      if (
        enabledElement &&
        enabledElement.image &&
        enabledElement.image.imageId.includes(studyInstanceUID) &&
        (enabledElement.image.imageId.includes(seriesInstanceUID) ||
          dataProtocol === 'nifti')
      ) {
        cornerstoneElement = enabledElement;
        return false;
      }
      return true;
    });

    return cornerstoneElement;
  }

  function getActiveApiDetailsFromMouseData(srcWindow) {
    let apiData = null;
    if (srcWindow) {
      apiData = {};
      Object.keys(mprApis).every(key => {
        const apis = mprApis[key];
        apiData.viewportIndex = apis.findIndex(
          api => api.genericRenderWindow.getRenderWindow() === srcWindow
        );
        if (apiData.viewportIndex >= 0) {
          apiData.seriesInstanceUID = key;
          return false;
        }
        return true;
      });
      if (apiData.viewportIndex < 0) {
        apiData = null;
      }
    }
    if (!apiData) {
      const currrentViewports = window.store?.getState()?.viewports;
      const { viewportSpecificData, activeViewportIndex } = currrentViewports;
      if (!isMprApisInitialized) {
        return apiData;
      }
      apiData = {};
      apiData.seriesInstanceUID =
        viewportSpecificData[activeViewportIndex].SeriesInstanceUID;
      apiData.viewportIndex = getApiIndex(currrentViewports);
    }
    return apiData;
  }

  function getFirstApiKey() {
    const seriesUids = getMultiSessionViewSeries();
    const key =
      !!seriesUids && seriesUids.length > 1
        ? seriesUids[0]
        : Object.keys(mprApis)[0];
    return key;
  }

  function getApiIndex(viewports, viewportIndex = -1) {
    const isMultiSessionViewEnabled = getMultiSessionViewStatus();
    const multiSessionViewCount = getMultiSessionViewCount();
    const { viewportSpecificData, activeViewportIndex } = viewports;
    const seriesInstanceUID =
      viewportSpecificData[activeViewportIndex].SeriesInstanceUID;
    viewportIndex = viewportIndex === -1 ? activeViewportIndex : viewportIndex;
    if (mprApis[seriesInstanceUID]) {
      if (isMultiSessionViewEnabled) {
        const totalViewports = Object.keys(viewportSpecificData).length;
        const viewportsPerView = totalViewports / multiSessionViewCount;
        viewportIndex = viewportIndex % viewportsPerView;
      }
    }
    return viewportIndex;
  }

  function isMatchingApi(updateApi, apiIndex, activeApi, activeApiIndex) {
    if (
      !updateApi.svgWidgets.rotatableCrosshairsWidget ||
      !activeApi.svgWidgets.rotatableCrosshairsWidget
    ) {
      return false;
    }
    const updateWidgetId = updateApi.svgWidgets.rotatableCrosshairsWidget.getWidgetId();
    const activeWidgetId = activeApi.svgWidgets.rotatableCrosshairsWidget.getWidgetId();
    if (updateWidgetId === activeWidgetId) {
      if (apiIndex === activeApiIndex) {
        return true;
      }
    }
    return false;
  }

  // Crosshair widget modification update.
  const onCrosshairUpdate = publicAPI => {
    if (!isMprApisInitialized) {
      return;
    }
    const publicApiIndex = publicAPI.getApiIndex();
    const apis = publicAPI.getApis();
    if (!event) {
      return;
    }
    if (!event.buttons && !event.wheelDelta) {
      return;
    } else if (!event.wheelDelta) {
      if (event.type !== 'mousemove') {
        return;
      }
    }
    if (!activeApiData) {
      activeApiData = getActiveApiDetailsFromMouseData(
        apis[publicApiIndex].genericRenderWindow.getRenderWindow()
      );
    }
    const seriesInstanceUID = activeApiData.seriesInstanceUID;
    const viewportIndex = activeApiData.viewportIndex;
    if (
      isMatchingApi(
        apis[publicApiIndex],
        publicApiIndex,
        mprApis[seriesInstanceUID][viewportIndex],
        viewportIndex
      )
    ) {
      syncTargetApis(
        mprApis,
        seriesInstanceUID,
        viewportIndex,
        initialReferenceLinePoints
      );
    }
    return;
  };

  function registerSyncSessionApiEvents(shouldUpdateInteractor = false) {
    Object.keys(mprApis).forEach(key => {
      const apis = mprApis[key];
      apis.forEach(api => {
        if (shouldUpdateInteractor) {
          api.svgWidgets.rotatableCrosshairsWidget.onModified(
            onCrosshairUpdate
          );
          const vtkInteractor = api.genericRenderWindow
            .getRenderWindow()
            .getInteractor();
          vtkInteractor.onLeftButtonPress(callData => {
            if (!isMprApisInitialized) {
              return;
            }
            const srcWindow =
              !!callData.pokedRenderer && !!callData.pokedRenderer.getVTKWindow
                ? callData.pokedRenderer.getVTKWindow()
                : null;
            activeApiData = getActiveApiDetailsFromMouseData(srcWindow);
            const seriesInstanceUID = activeApiData.seriesInstanceUID;
            const viewportIndex = activeApiData.viewportIndex;
            const thisApi = mprApis[seriesInstanceUID][viewportIndex];

            //store the crosshairs line points  of source and target viewports so that they can be synced later.
            initialReferenceLinePoints.src = [];
            initialReferenceLinePoints.tgt = [];
            thisApi.svgWidgets.rotatableCrosshairsWidget
              .getReferenceLines()
              .forEach((line, lineIndex) => {
                initialReferenceLinePoints.src[lineIndex] = line.points;
              });

            Object.keys(mprApis).forEach(key1 => {
              if (seriesInstanceUID !== key1) {
                initialReferenceLinePoints.tgt[key1] = [];
                mprApis[key1][
                  viewportIndex
                ].svgWidgets.rotatableCrosshairsWidget
                  .getReferenceLines()
                  .forEach((line, lineIndex) => {
                    initialReferenceLinePoints.tgt[key1][lineIndex] =
                      line.points;
                  });
              }
            });
          });

          vtkInteractor.onLeftButtonRelease(callData => {
            const srcWindow =
              callData.pokedRenderer && callData.pokedRenderer.getVTKWindow
                ? callData.pokedRenderer.getVTKWindow()
                : null;
            // Move the crosshairs only if the crosshair mode is active
            if (
              !srcWindow ||
              !srcWindow.getInteractor() ||
              getInteractorClassName(srcWindow) !==
                INTERACTOR_NAMES.rotatableCrosshair
            ) {
              activeApiData = null;
              initialReferenceLinePoints = {};
              return;
            }
            activeApiData = getActiveApiDetailsFromMouseData(srcWindow);
            const seriesInstanceUID = activeApiData.seriesInstanceUID;
            const viewportIndex = activeApiData.viewportIndex;
            const thisApi = mprApis[seriesInstanceUID][viewportIndex];
            const { rotatableCrosshairsWidget } = thisApi.svgWidgets;
            const { svgWidgetManager } = thisApi;
            const size = svgWidgetManager.getSize();
            const scale = svgWidgetManager.getScale();
            const height = size[1];

            const { position, pokedRenderer } = callData;
            // Map the click point to the same coords as the SVG.
            const pos = {
              x: position.x * scale,
              y: height - position.y * scale,
            };

            // If dissociateCrosshair is enabled, crosshair need not get shifted on clicking the top right circle.
            const projectConfig = selectProjectConfig(store.getState());
            const dissociateCrosshairs =
              projectConfig?.dissociateCrosshairs === undefined
                ? true
                : projectConfig?.dissociateCrosshairs;
            const {
              center,
              radius,
            } = rotatableCrosshairsWidget.getWidgetCircle();
            const distanceFromCenter = vec2.distance(
              [center.x, center.y],
              [pos.x, pos.y]
            );

            if (distanceFromCenter > radius || !dissociateCrosshairs) {
              const lines = rotatableCrosshairsWidget.getReferenceLines();
              const centerRadius = rotatableCrosshairsWidget.getCenterRadius();
              const distanceFromLine1 = distanceFromLine(lines[0], pos);
              const distanceFromLine2 = distanceFromLine(lines[1], pos);

              // If the clicked point is outside the line grab distance, then move crosshair to that point
              if (
                distanceFromLine1 > centerRadius &&
                distanceFromLine2 > centerRadius
              ) {
                const srcApis = mprApis[activeApiData.seriesInstanceUID];
                moveCrosshairs(
                  srcApis,
                  activeApiData.viewportIndex,
                  position,
                  pokedRenderer
                );
                syncTargetApis(
                  mprApis,
                  activeApiData.seriesInstanceUID,
                  activeApiData.viewportIndex,
                  initialReferenceLinePoints
                );
              }
            } else {
              // Activate clicked viewport and deactivate other viewports.
              let viewportIndex = 0;
              const noOfSeries = Object.keys(mprApis).length;
              Object.keys(mprApis).forEach((key, keyIndex) => {
                const curApis = mprApis[key];
                curApis.forEach((api, apiIndex) => {
                  const isActive =
                    activeApiData.viewportIndex === apiIndex ? true : false;
                  api.svgWidgets.rotatableCrosshairsWidget.setViewportActive(
                    isActive
                  );
                  api.svgWidgets.rotatableCrosshairsWidget.setBorder(
                    getWidgetBorderStyle(
                      false,
                      isActive,
                      viewportIndex,
                      noOfSeries
                    )
                  );
                  api.svgWidgetManager.render();
                  viewportIndex++;
                });
              });
            }
            activeApiData = null;
            initialReferenceLinePoints = {};
          });
        }
      });
    });
  }

  /**
   * Register all custom Manipulators in vtkjs
   */
  function registerCustomManipulatorType() {
    const vtkManipulators = Interaction.Manipulators;
    const keys = Object.keys(vtkManipulators);
    keys.forEach(manipulator => {
      InteractionPresets.registerManipulatorType(
        manipulators[manipulator],
        vtkManipulators[manipulator]
      );
    });
  }

  function initializeMprAttributes() {
    mprApis = {};
    isMprApisInitialized = false;
    initialReferenceLinePoints = {};
    activeApiData = null;
  }

  function getVOIWithDisplaySet(displaySet, key) {
    const study = studyMetadataManager.get(displaySet.StudyInstanceUID);
    const voi = cornerstone.metaData.get(
      'voiLutModule',
      study.displaySets?.[0]?.images?.[0]?._imageId
    );
    if (Array.isArray(voi.windowWidth)) {
      return;
    }
    const cornerstoneVOI = {
      windowWidth: voi.windowWidth,
      windowCenter: voi.windowCenter,
    };
    return cornerstoneVOI;
  }

  function setVOI(voi, seriesInstanceUID = null) {
    const { windowWidth, windowCenter } = voi;
    const lower = windowCenter - windowWidth / 2.0;
    const upper = windowCenter + windowWidth / 2.0;
    const key = seriesInstanceUID ? seriesInstanceUID : getFirstApiKey();
    const rgbTransferFunction = mprApis[key][0].volumes[0]
      .getProperty()
      .getRGBTransferFunction(0);

    rgbTransferFunction.setRange(lower, upper);

    Object.keys(mprApis).forEach(key => {
      if (seriesInstanceUID && seriesInstanceUID !== key) {
        return;
      }
      const apis = mprApis[key];
      apis.forEach(api => {
        api.updateVOI(windowWidth, windowCenter);
      });
    });
  }

  function toWindowLevel(low, high) {
    const windowWidth = Math.abs(low - high);
    const windowCenter = low + windowWidth / 2;

    return { windowWidth, windowCenter };
  }

  const _convertModelToWorldSpace = (position, vtkImageData) => {
    const indexToWorld = vtkImageData.getIndexToWorld();
    const pos = vec3.create();

    position[0] += 0.5; /* Move to the centre of the voxel. */
    position[1] += 0.5; /* Move to the centre of the voxel. */
    position[2] += 0.5; /* Move to the centre of the voxel. */

    vec3.set(pos, position[0], position[1], position[2]);
    vec3.transformMat4(pos, pos, indexToWorld);

    return pos;
  };

  function getWidgetBorderStyle(
    allViewportsActive,
    currentViewportActive,
    viewportIndex,
    numberOfRowsForViewports
  ) {
    let borderStyle = { left: false, right: false, top: false, bottom: false };
    if (allViewportsActive) {
      if (numberOfRowsForViewports === 1) {
        switch (viewportIndex) {
          case 0: {
            borderStyle = {
              ...borderStyle,
              left: true,
              top: true,
              bottom: true,
            };
            break;
          }
          case 1: {
            borderStyle = { ...borderStyle, top: true, bottom: true };
            break;
          }
          case 2: {
            borderStyle = {
              ...borderStyle,
              right: true,
              top: true,
              bottom: true,
            };
            break;
          }
          default: {
            break;
          }
        }
      } else {
        switch (viewportIndex) {
          case 0: {
            borderStyle = { ...borderStyle, left: true, top: true };
            break;
          }
          case 1: {
            borderStyle = { ...borderStyle, top: true };
            break;
          }
          case 2: {
            borderStyle = { ...borderStyle, right: true, top: true };
            break;
          }
          case 3: {
            borderStyle = { ...borderStyle, left: true, bottom: true };
            break;
          }
          case 4: {
            borderStyle = { ...borderStyle, bottom: true };
            break;
          }
          case 5: {
            borderStyle = { ...borderStyle, right: true, bottom: true };
            break;
          }
          default: {
            break;
          }
        }
      }
    } else {
      if (currentViewportActive) {
        if (numberOfRowsForViewports === 1) {
          borderStyle = { left: true, right: true, top: true, bottom: true };
        } else {
          if ([0, 1, 2].includes(viewportIndex)) {
            borderStyle = {
              ...borderStyle,
              left: true,
              right: true,
              top: true,
            };
          } else {
            borderStyle = {
              ...borderStyle,
              left: true,
              right: true,
              bottom: true,
            };
          }
        }
      }
    }
    return borderStyle;
  }

  // Checks the click event happened on the top right circle of any crosshair widget .
  function checkLinkViewportActivated(event) {
    let linked = false;
    Object.keys(mprApis).forEach(key => {
      if (!linked) {
        const apis = mprApis[key];
        if (apis.length > 1) {
          for (let i = 0; i < apis.length; i++) {
            let point = { x: event.clientX, y: event.clientY };
            const { svgWidgetManager, genericRenderWindow } = apis[i];
            const bounds = svgWidgetManager
              .getContainer()
              .getBoundingClientRect();
            const canvas = genericRenderWindow
              .getInteractor()
              .getView()
              .getCanvas();
            const scale = {
              x: canvas.width / bounds.width,
              y: canvas.height / bounds.height,
            };
            const size = svgWidgetManager.getSize();
            const height = size[1];
            const { center, radius } = apis[
              i
            ].svgWidgets.rotatableCrosshairsWidget.getWidgetCircle();
            const position = {
              x: scale.x * (point.x - bounds.left),
              y: height - scale.y * (bounds.height - point.y + bounds.top),
            };
            const distanceFromCenter = vec2.distance(
              [center.x, center.y],
              [position.x, position.y]
            );
            if (distanceFromCenter <= radius) {
              linked = true;
              return;
            }
          }
        }
      }
    });
    return linked;
  }

  const getViewportProperties = (
    viewportSpecificData,
    api,
    toolNames,
    bSliceBounds,
    fovReferencesInfo
  ) => {
    const propertiesToSync = {};
    propertiesToSync.seriesInstanceUID = viewportSpecificData.SeriesInstanceUID;
    propertiesToSync.studyInstanceUID = viewportSpecificData.StudyInstanceUID;
    (toolNames || []).forEach(toolName => {
      if (toolName === 'WindowLevel') {
        const rgbTransferFunction = api.volumes[0]
          .getProperty()
          .getRGBTransferFunction(0);
        const range = rgbTransferFunction.getMappingRange();
        const windowWidth = Math.abs(range[1] - range[0]);
        const windowCenter = range[0] + windowWidth / 2;
        propertiesToSync.windowLevel = {
          windowCenter,
          windowWidth,
        };
      } else if (toolName === 'StackScroll') {
        const imageIds = OHIFVTKViewport.getCornerstoneStackFromDisplaySet(
          studyMetadataManager.get(viewportSpecificData.StudyInstanceUID)._data,
          viewportSpecificData
        )?.imageIds;
        let patientPos = null;
        if (imageIds?.length) {
          if (bSliceBounds && !fovReferencesInfo?.center) {
            patientPos = bSliceUtils.getSlicePosInWorldFromBound(
              bSliceBounds,
              imageIds
            );
          } else {
            // Use the fov center pos in the fovReferences first item if it exist to sync other viewports
            // else from the default crosshair position
            if (fovReferencesInfo?.isPatientPosSync) {
              patientPos = fovReferencesInfo?.center || null;
            } else if (!fovReferencesInfo) {
              patientPos = api.get('cachedCrosshairWorldPosition');
            }
            if (patientPos) {
              // Adjust the position from vtk js orientation to sync with 2D DICOM orientation
              const imagePlane = cornerstone.metaData.get(
                'imagePlaneModule',
                imageIds[0]
              );
              const planeData = convertPlanesDataFromVtkjsOrientationTo2D(
                [patientPos],
                imagePlane
              );
              patientPos = planeData.worldPosArray[0];
            }
          }

          if (fovReferencesInfo?.center) {
            const imagePlane = cornerstone.metaData.get(
              'imagePlaneModule',
              imageIds[0]
            );
            let referenceData = convertPlanesDataFromVtkjsOrientationTo2D(
              [fovReferencesInfo.center],
              imagePlane
            );
            propertiesToSync.fovReferencesInfo = {
              center: referenceData.worldPosArray[0],
              referenceLineDir: fovReferencesInfo.referenceLineDir,
            };
          }

          if (fovReferencesInfo?.need2DUpdate) {
            propertiesToSync.fovReferencesInfo = {
              ...propertiesToSync.fovReferencesInfo,
              need2DUpdate: fovReferencesInfo.need2DUpdate,
            };
          }

          if (fovReferencesInfo?.circumferencePts?.length) {
            const imagePlane = cornerstone.metaData.get(
              'imagePlaneModule',
              imageIds[0]
            );
            let referenceData = convertPlanesDataFromVtkjsOrientationTo2D(
              fovReferencesInfo.circumferencePts,
              imagePlane
            );
            if (!propertiesToSync.fovReferencesInfo) {
              propertiesToSync.fovReferencesInfo = {};
            }
            propertiesToSync.fovReferencesInfo.circumferencePts =
              referenceData.worldPosArray;
          }
          if (propertiesToSync.fovReferencesInfo) {
            propertiesToSync.fovReferencesInfo.isCenterEnabled =
              fovReferencesInfo.isCenterEnabled;
            propertiesToSync.fovReferencesInfo.referenceLineDir =
              fovReferencesInfo.referenceLineDir;
          }
        }
        propertiesToSync.patientPos = patientPos;
      } else if (toolName === 'Zoom') {
        const radius = getVolumeRadius(api) || 100;
        const currentScale = api.genericRenderWindow
          .getRenderer()
          .getActiveCamera()
          .getParallelScale();
        if (currentScale !== radius) {
          propertiesToSync.zoom = {
            type: 'relative',
            scale: (radius - currentScale) / radius,
          };
        }
      }
    });
    return propertiesToSync;
  };

  const updateOtherViewports = (
    viewports,
    api,
    toolNames,
    bSliceBounds,
    fovReferencesInfo
  ) => {
    const apiData = getActiveApiDetailsFromMouseData(
      api.genericRenderWindow.getRenderWindow()
    );
    if (!apiData) {
      return null;
    }

    const viewportDetails = getMatchingVTKViewportData(
      viewports,
      apiData.seriesInstanceUID
    );
    const plugin = viewportDetails.displaySet.plugin;
    if (viewportDetails) {
      const propertiesToSync = getViewportProperties(
        viewportDetails.displaySet,
        api,
        toolNames,
        bSliceBounds,
        fovReferencesInfo
      );
      commandsManager.runCommand('crossViewportSync', {
        plugin,
        propertiesToSync,
      });
    }
  };

  // Performing initialization of the created api components
  const initializeMPRApis = (
    isRotatableWidget,
    isMultiSessionViewEnabled = false,
    preferredTool
  ) => {
    const widgetName = isRotatableWidget
      ? 'rotatableCrosshairsWidget'
      : 'crosshairsWidget';
    let viewportIndex = 0;
    Object.keys(mprApis).forEach(key => {
      const apis = mprApis[key];
      apis.forEach((api, apiIndex) => {
        const widgetInstance = isRotatableWidget
          ? vtkSVGDetachedRotatableCrosshairWidget.newInstance()
          : vtkSVGCrosshairIndicatorWidget.newInstance();
        api.addSVGWidget(widgetInstance, widgetName);

        const uid = api.uid;
        const istyle = vtkInteractorStyleMPRSlice.newInstance();

        api.setInteractorStyle({
          istyle,
          configuration: { apis, apiIndex, uid },
        });

        if (isRotatableWidget) {
          api.svgWidgets[widgetName].setApiIndex(apiIndex);
          api.svgWidgets[widgetName].setApis(apis);
          //Inform widget about the viewport state.
          const projectConfig = selectProjectConfig(store.getState());
          const dissociateCrosshairs =
            projectConfig?.dissociateCrosshairs === undefined
              ? true
              : projectConfig?.dissociateCrosshairs;

          let isViewportActive = false;
          if (apiIndex === 0 || !dissociateCrosshairs) {
            isViewportActive = true;
          }
          api.svgWidgets[widgetName].setViewportActive(isViewportActive);
          api.svgWidgets[widgetName].setBorder(
            getWidgetBorderStyle(
              false,
              isViewportActive,
              viewportIndex,
              isMultiSessionViewEnabled
            )
          );
          viewportIndex++;
        }
        api.svgWidgets[widgetName].setDisplay(false);
      });

      // Initialise crosshairs
      apis[0].svgWidgets[widgetName].resetCrosshairs(apis, 0);
    });

    const key = getFirstApiKey();
    const firstApi = mprApis[key][0];

    // Check if we have full WebGL 2 support
    const openGLRenderWindow = firstApi.genericRenderWindow.getOpenGLRenderWindow();

    if (!openGLRenderWindow.getWebgl2()) {
      // Throw a warning if we don't have WebGL 2 support,
      // And the volume is too big to fit in a 2D texture

      const openGLContext = openGLRenderWindow.getContext();
      const maxTextureSizeInBytes = openGLContext.getParameter(
        openGLContext.MAX_TEXTURE_SIZE
      );

      const maxBufferLengthFloat32 =
        (maxTextureSizeInBytes * maxTextureSizeInBytes) / 4;

      const dimensions = firstApi.volumes[0]
        .getMapper()
        .getInputData()
        .getDimensions();

      const volumeLength = dimensions[0] * dimensions[1] * dimensions[2];

      if (volumeLength > maxBufferLengthFloat32) {
        UINotificationService.show({
          title: 'Browser does not support WebGL 2',
          message:
            'This volume is too large to fit in WebGL 1 textures and will display incorrectly. Please use a different browser to view this data',
          type: 'error',
          autoClose: false,
        });
      }
    }
    const isInitialize = true;
    setMouseConfig(preferredTool, isInitialize);
  };

  // Performing initialization of the created api components
  const initializeMPRApiIndicators = (
    imageIds,
    apis,
    fovDetail,
    projectConfig,
    SeriesInstanceUID
  ) => {
    const sliceSettings = projectConfig?.bSlices?.settings;
    const state = store.getState();

    const studies = studyMetadataManager.all();
    const study = studies.find(stdy =>
      stdy.getData().series.some(s => s.SeriesInstanceUID === SeriesInstanceUID)
    );
    const displaySet = getDisplaySetForSeries(study, SeriesInstanceUID);
    const defaultRoiLabelDetails = getDefaultROILabelFromHPLayout();
    const defaultRoiLabels = defaultRoiLabelDetails[SeriesInstanceUID];

    const restoreInfo = selectFovGridRestoreInfo(state);
    // Todo: Verify if imageIds are taken from correct series,
    // when that series is not in any viewports or
    // should it be taken from displaySet.
    const bSliceIndicatorDataInWorld = !isCurrentFile(state)
      ? bSliceUtils.getBSliceIndicatorDataInWorld(
          sliceSettings,
          imageIds,
          apis[0].genericRenderWindow.getRenderer()
        ) || []
      : [];

    let annotationForFovGrid = [];

    if (defaultRoiLabels?.length > 0) {
      const annotations = getAllFovealRois(defaultRoiLabels);
      const annotationsForSeries = annotations.filter(
        annotation => annotation.SeriesInstanceUID === SeriesInstanceUID
      );
      annotationForFovGrid = annotationsForSeries;
    }

    apis.forEach((api, apiIndex) => {
      // Initialize the indicators world position data in svg widget and render it
      let indicatorDataInWorld = [].concat(bSliceIndicatorDataInWorld);

      if (fovDetail && fovDetail[apiIndex]) {
        let fovCenter = api.get('cachedCrosshairWorldPosition');

        if (!fovGrid.fovGridCenter?.[SeriesInstanceUID]) {
          if (!fovGrid.fovGridCenter) {
            fovGrid.fovGridCenter = {};
          }
          fovGrid.fovGridCenter[SeriesInstanceUID] = fovCenter;
        }

        if (
          defaultRoiLabels?.length > 0 &&
          annotationForFovGrid?.length === 1
        ) {
          setFOVCenterWithAnnotation(
            annotationForFovGrid,
            displaySet,
            fovCenter
          );
        }

        const fovIndicatorData =
          fovUtils.getFovIndicatorDataInWorld(
            fovCenter,
            fovDetail[apiIndex].fovDiameters,
            fovDetail[apiIndex].hideFovGrid,
            api,
            restoreInfo.isCenterEnabled
          ) || [];
        indicatorDataInWorld = indicatorDataInWorld.concat(fovIndicatorData);
        if (restoreInfo.selectedFovIndex >= 0) {
          fovUtils.updateFovIndicatorSelection(
            indicatorDataInWorld,
            restoreInfo.selectedFovIndex
          );
        }
      }

      if (indicatorDataInWorld?.length) {
        api.svgWidgets.crosshairsWidget.setIndicatorsDataInWorld(
          indicatorDataInWorld
        );
        api.svgWidgets.crosshairsWidget.adjustIndicators(apis);
      }

      const currentInteractor = api.genericRenderWindow
        .getRenderWindow()
        .getInteractor();
      currentInteractor.onLeftButtonPress(callData => {
        syncOnProtocolViewEvent(apis, api, callData, true);
      });
      currentInteractor.onMouseMove(callData => {
        const isCrosshairActive = isCrosshairStyle(
          '',
          api.genericRenderWindow.getRenderWindow()
        );
        if (isCrosshairActive) {
          const iStyle = currentInteractor.getInteractorStyle();
          const crosshairsWidget = api.svgWidgets.crosshairsWidget;
          if (
            crosshairsWidget.getIsFovRepositionMode() ||
            iStyle.getIsCenterClicked()
          ) {
            if (!crosshairsWidget.getIsFovRepositionMode()) {
              crosshairsWidget.setIsFovRepositionMode(true);
            }
            // Perform FOV center position adjustment on mouse move
            syncOnProtocolViewEvent(apis, api, callData);
          } else {
            // Highlight the FOV ring if the mouse is hovered on top of it
            const indicatorsData = crosshairsWidget.getIndicators();
            const fovIndex = fovUtils.getClickedFovRingIndex(
              indicatorsData,
              callData
            );
            if (fovIndex !== crosshairsWidget.getFocussedIndicatorIndex()) {
              crosshairsWidget.setFocussedIndicatorIndex(fovIndex);
              api.svgWidgetManager.render();
            }
          }
        }
      });
      currentInteractor.onLeftButtonRelease(callData => {
        syncOnProtocolViewEvent(apis, api, callData);
      });
      currentInteractor.onRightButtonRelease(callData => {
        syncOnProtocolViewEvent(apis, api, callData);
      });
      currentInteractor.onMiddleButtonRelease(callData => {
        syncOnProtocolViewEvent(apis, api, callData);
      });
      currentInteractor.onEndMouseWheel(callData => {
        syncOnProtocolViewEvent(apis, api, callData);
      });
    });
  };

  const setMouseConfigAfterToolEnabled = (
    istyle,
    api,
    apis,
    apiIndex,
    enabledTool
  ) => {
    const interactorStyleDefinitions = getCustomInteractorStyleDefinitions();
    InteractionPresets.applyDefinitions(interactorStyleDefinitions, istyle);
    setApisForModel(interactorStyleDefinitions, istyle, apis, api);
    const scrollIndex = interactorStyleDefinitions.findIndex(
      def => def.options.button === 2 && def.options.scrollEnabled
    );
    if (scrollIndex !== -1) {
      addScrollListener(istyle, api, apis, apiIndex, scrollIndex);
    }
    registerSyncCallback(apiIndex, api, mprApis, enabledTool);
  };

  const initMouseConfig = toolName => {
    switch (toolName) {
      case 'Rotate': {
        actions.enableRotateTool();
        break;
      }
      case 'StackScroll': {
        actions.enableStackScrollTool();
        break;
      }
      case 'Zoom': {
        actions.enableZoomTool();
        break;
      }
      case 'Wwwc': {
        actions.enableLevelTool();
        break;
      }
      case 'Crosshair': {
        actions.enableCrosshairsTool(false);
        break;
      }
      case 'Pan': {
        actions.enablePanTool();
        break;
      }
      default: {
        actions.enableStackScrollTool();
        break;
      }
    }
  };

  // Sets mouse actions as per config, with preferred tool overwriting left config.
  function setMouseConfig(preferredLeftTool, isInitialize = false) {
    registerCustomManipulatorType();
    if (!preferredLeftTool) {
      const config = store.getState()?.flywheel?.projectConfig;
      const mouseActions = config?.mouseActions;
      if (mouseActions) {
        const leftAction = mouseActions.find(
          action => action.button === 'left'
        );
        if (leftAction) {
          preferredLeftTool = leftAction.toolName;
        }
      }
    }
    if (preferredLeftTool && preferredLeftTool !== 'Crosshair') {
      initMouseConfig(preferredLeftTool);
    } else if (isInitialize && preferredLeftTool === 'Crosshair') {
      actions.enableCrosshairsTool(false);
    } else {
      Object.keys(mprApis).forEach(key => {
        const apis = mprApis[key];
        apis.forEach((api, apiIndex) => {
          setStyle(api, apis, apiIndex, 'stackscroll');
        });
      });
    }
  }

  const setStyle = (api, apis, apiIndex, type, shouldAddVertical = true) => {
    let istyle = vtkInteractorStyleMPRSlice.newInstance();
    let interactorStyleDefinitions = getCustomInteractorStyleDefinitions(
      1,
      type,
      false,
      true
    );
    api.setInteractorStyle({
      istyle,
      configuration: { apis, apiIndex, uid: api.uid },
    });
    InteractionPresets.applyDefinitions(interactorStyleDefinitions, istyle);
    setApisForModel(interactorStyleDefinitions, istyle, apis, api);
    const scrollIndex = interactorStyleDefinitions.findIndex(
      def => def.options.button === 2 && def.options.scrollEnabled
    );
    const dragIndex = interactorStyleDefinitions.findIndex(
      def => def.options.button === 1 && def.options.dragEnabled
    );
    if (scrollIndex !== -1) {
      addScrollListener(istyle, api, apis, apiIndex, scrollIndex);
    }
    if (dragIndex !== -1 && shouldAddVertical) {
      addVerticalListener(istyle, api, apis, apiIndex, dragIndex);
    }
    registerSyncCallback(apiIndex, api, mprApis, type);
  };

  function getImagePosition(viewportSpecificData, api, bSliceBounds) {
    const imageIds = OHIFVTKViewport.getCornerstoneStackFromDisplaySet(
      studyMetadataManager.get(viewportSpecificData.StudyInstanceUID)._data,
      viewportSpecificData
    )?.imageIds;

    let patientPos = null;
    if (imageIds?.length) {
      if (bSliceBounds) {
        patientPos = bSliceUtils.getSlicePosInWorldFromBound(
          bSliceBounds,
          imageIds
        );
      } else {
        patientPos = api.get('cachedCrosshairWorldPosition');
      }
    }
    return patientPos;
  }

  function getScale(parallelScale, physicalScale, imageIds) {
    let zoomFactor = 0;
    const imageURL = imageIds[0];
    const cachedImage = cornerstone.imageCache.cachedImages.find(x =>
      x.imageId.includes(imageURL[0])
    );

    if (cachedImage) {
      const rows = cornerstone.metaData.get('Rows', cachedImage.imageId);

      let dicomToVtkFactor = cornerstone.metaData.get(
        'PixelSpacing',
        cachedImage.imageId
      )?.[1];

      const vtkScale = physicalScale / parallelScale;
      if (!dicomToVtkFactor) {
        dicomToVtkFactor = physicalScale / rows;
      }
      zoomFactor = vtkScale * dicomToVtkFactor * 2;
    }
    return zoomFactor;
  }

  // Update indicators and trigger update to other type of viewports
  function syncOnProtocolViewEvent(
    apis,
    activeApi,
    callData,
    skipUpdate = false
  ) {
    const crosshairsWidget = activeApi.svgWidgets.crosshairsWidget;
    crosshairsWidget.adjustIndicators(apis);
    crosshairsWidget.updateCrosshairForApi(activeApi);
    crosshairsWidget.moveCrosshairs(
      activeApi.get('cachedCrosshairWorldPosition'),
      apis
    );
    const isCrosshairActive = isCrosshairStyle(
      '',
      activeApi.genericRenderWindow.getRenderWindow()
    );
    const tools = ['WindowLevel', 'Zoom'];
    if (isCrosshairActive) {
      tools.push('StackScroll');
    }
    let bSliceBounds = null;
    let fovReferencesInfo = null;
    const isProcessEvent =
      callData.type === 'LeftButtonPress' ||
      callData.type === 'LeftButtonRelease' ||
      callData.type === 'MouseMove' ||
      false;
    if (isCrosshairActive && isProcessEvent) {
      const iStyle = activeApi.genericRenderWindow
        .getInteractor()
        .getInteractorStyle();
      const indicatorsData = crosshairsWidget.getIndicators();
      const indicatorsDataInWorld = crosshairsWidget.getIndicatorsDataInWorld();
      const isFovIndicatorExist = indicatorUtils.isIndicatorExist(
        indicatorsDataInWorld,
        INDICATOR_TYPES.FOV
      );
      if (callData.type === 'LeftButtonPress') {
        // Pressing on center and mouse move allows to adjust the FOV positioning
        if (isFovIndicatorExist) {
          if (fovUtils.isClickedOnCenter(indicatorsData, callData)) {
            iStyle.setIsCenterClicked(true);
          }
        }
      } else if (
        callData.type === 'LeftButtonRelease' ||
        callData.type === 'MouseMove'
      ) {
        // Finds the B Slice plane position data matching to the corresponding 2D plane
        const bSliceIndex = bSliceUtils.getClickedBSliceIndicatorIndex(
          indicatorsData,
          callData
        );
        if (bSliceIndex > -1) {
          const bIndicatorData = indicatorsDataInWorld[bSliceIndex];
          bSliceBounds = bIndicatorData
            ? bIndicatorData.TopLeft.concat(bIndicatorData.BottomRight)
            : null;
        }

        if (isFovIndicatorExist) {
          const fovIndex = fovUtils.getClickedFovIndex(
            indicatorsData,
            callData
          );
          const isPositionCenter = crosshairsWidget.getIsFovRepositionMode();

          if (isPositionCenter) {
            const { position, pokedRenderer } = callData;
            const worldPos = getWorldPosFromDisplayPos(
              activeApi,
              position,
              pokedRenderer
            );
            fovUtils.updateFovIndicatorCenter(worldPos, indicatorsDataInWorld);
          } else {
            // Update fov selection(select/unselect) and move the fov center if not selected
            fovUtils.updateFovIndicatorSelection(
              indicatorsDataInWorld,
              fovIndex
            );
          }
          fovReferencesInfo = fovUtils.getFovIndicatorPointsInWorld(
            indicatorsDataInWorld,
            fovIndex,
            activeApi
          );
          fovReferencesInfo.isPatientPosSync =
            isPositionCenter || fovReferencesInfo.isPatientPosSync;
          fovReferencesInfo.isCenterEnabled = fovUtils.isCenterEnabled(
            indicatorsDataInWorld
          );

          crosshairsWidget.setIndicatorsDataInWorld(indicatorsDataInWorld);
          crosshairsWidget.adjustIndicators(apis);
          activeApi.genericRenderWindow.getRenderWindow().render();
          if (callData.type === 'LeftButtonRelease') {
            if (iStyle.getIsCenterClicked()) {
              iStyle.setIsCenterClicked(false);
            }
            if (crosshairsWidget.getIsFovRepositionMode()) {
              crosshairsWidget.setIsFovRepositionMode(false);
              activeApi.svgWidgetManager.render();
            }
            const isClickedOnCenter = fovUtils.isClickedOnCenter(
              indicatorsData,
              callData
            );
            const currentSelectedFovIndex = selectFovGridRestoreInfo(
              store.getState()
            ).selectedFovIndex;
            const selectedFovIndex = !isClickedOnCenter
              ? fovIndex
              : currentSelectedFovIndex;
            store.dispatch(
              setFovGridRestoreInfo({
                centerPos: fovReferencesInfo.center,
                isCenterEnabled: fovReferencesInfo.isCenterEnabled,
                selectedFovIndex: fovReferencesInfo.circumferencePts?.length
                  ? selectedFovIndex
                  : -1,
              })
            );
          }
        }
      }
    }
    if (!skipUpdate) {
      updateOtherViewports(
        store.getState().viewports,
        activeApi,
        tools,
        bSliceBounds,
        fovReferencesInfo
      );
    }
  }

  const actions = {
    getVtkApis: ({ index }) => {
      const key = getFirstApiKey();
      return mprApis[key][index];
    },
    getVtkPropertiesForView: ({ displaySet }) => {
      const apis = mprApis[displaySet.SeriesInstanceUID];
      const api = apis?.[0];
      if (api) {
        const viewProperties = getViewportProperties(displaySet, api, [
          'WindowLevel',
          'Zoom',
          'StackScroll',
        ]);
        viewProperties.orientations = [];
        apis.forEach(obj => {
          viewProperties.orientations.push({
            orientation: {
              sliceNormal: obj.getSliceNormal(),
              viewUp: obj.getViewUp(),
            },
          });
        });
        return viewProperties;
      }
      return null;
    },
    resetMPRView() {
      // Reset orientation
      try {
        Object.keys(mprApis).forEach(key => {
          const apis = mprApis[key];
          apis.forEach(api => {
            if (api.genericRenderWindow.getRenderWindow()) {
              fitToViewport(api);
              api.resetOrientation();
            }
          });

          // Reset VOI
          if (defaultVOI[key]) {
            setVOI(defaultVOI[key], key);
          }
        });

        // Reset the crosshairs
        Object.keys(mprApis).forEach(key => {
          const apis = mprApis[key];
          if (isInSingleView || apis[0].svgWidgets.crosshairsWidget) {
            apis[0].svgWidgets.crosshairsWidget.resetCrosshairs(apis, 0);
            apis[0].svgWidgets.crosshairsWidget.adjustIndicators(apis);
          } else {
            apis[0].svgWidgets.rotatableCrosshairsWidget.resetCrosshairs(
              apis,
              0
            );
          }
          updateOtherViewports(store.getState().viewports, apis[0], [
            'WindowLevel',
            'Zoom',
            'StackScroll',
          ]);
        });
      } catch (error) {
        // Reset will cause error if the api's gets unmounted like if any MPR viewport is modified
        // with 2D viewport through series thumbnail click or measurement panel click
        log.error(error);
      }
      const state = store.getState();
      const protocolStore = selectActiveProtocolStore(state);
      // Reset to hanging protocol layout if a valid protocol already applied and if the current layout
      // or the viewport displaySet is different
      if (protocolStore?.protocolStore) {
        resetLayoutToHP(commandsManager, store, protocolStore);
      }
      store.dispatch(setViewportActive(0));
    },
    axial: async ({ viewports }) => {
      const api = await _getActiveViewportVTKApi(viewports);
      const { viewportSpecificData, activeViewportIndex } = viewports;
      const seriesInstanceUID =
        viewportSpecificData[activeViewportIndex].SeriesInstanceUID;
      const viewportIndex = getApiIndex(viewports);

      mprApis[seriesInstanceUID][viewportIndex] = api;

      _setView(api, [0, 0, 1], [0, -1, 0]);
    },
    sagittal: async ({ viewports }) => {
      const api = await _getActiveViewportVTKApi(viewports);

      const { viewportSpecificData, activeViewportIndex } = viewports;
      const seriesInstanceUID =
        viewportSpecificData[activeViewportIndex].SeriesInstanceUID;
      const viewportIndex = getApiIndex(viewports);

      mprApis[seriesInstanceUID][viewportIndex] = api;

      _setView(api, [-1, 0, 0], [0, 0, 1]);
    },
    coronal: async ({ viewports }) => {
      const api = await _getActiveViewportVTKApi(viewports);

      const { viewportSpecificData, activeViewportIndex } = viewports;
      const seriesInstanceUID =
        viewportSpecificData[activeViewportIndex].SeriesInstanceUID;
      const viewportIndex = getApiIndex(viewports);

      mprApis[seriesInstanceUID][viewportIndex] = api;

      _setView(api, [0, 1, 0], [0, 0, 1]);
    },
    requestNewSegmentation: async ({ viewports }) => {
      const allViewports = Object.values(viewports.viewportSpecificData);
      const promises = allViewports.map(async (viewport, viewportIndex) => {
        const apiIndex = getApiIndex(viewports, viewportIndex);
        const seriesInstanceUID = viewport.SeriesInstanceUID;
        let api = mprApis[seriesInstanceUID][apiIndex];

        if (!api) {
          api = await _getActiveViewportVTKApi(viewports);
          mprApis[seriesInstanceUID][apiIndex] = api;
        }

        api.requestNewSegmentation();
        api.updateImage();
      });
      await Promise.all(promises);
    },
    jumpToSlice: async ({
      viewports,
      studies,
      StudyInstanceUID,
      displaySetInstanceUID,
      SOPClassUID,
      SOPInstanceUID,
      segmentNumber,
      frameIndex,
      frame,
      done = () => {},
    }) => {
      const { viewportSpecificData, activeViewportIndex } = viewports;
      const seriesInstanceUID =
        viewportSpecificData[activeViewportIndex].SeriesInstanceUID;
      const viewportIndex = getApiIndex(viewports);

      let api = mprApis[seriesInstanceUID][viewportIndex];

      if (!api) {
        api = await _getActiveViewportVTKApi(viewports);
        mprApis[seriesInstanceUID][viewportIndex] = api;
      }

      const stack = OHIFVTKViewport.getCornerstoneStack(
        studies,
        StudyInstanceUID,
        displaySetInstanceUID,
        SOPClassUID,
        SOPInstanceUID,
        frameIndex
      );

      const imageDataObject = getImageData(
        stack.imageIds,
        displaySetInstanceUID
      );

      let pixelIndex = 0;
      let x = 0;
      let y = 0;
      let count = 0;

      const rows = imageDataObject.dimensions[1];
      const cols = imageDataObject.dimensions[0];

      for (let j = 0; j < rows; j++) {
        for (let i = 0; i < cols; i++) {
          // [i, j] =
          const pixel = frame.pixelData[pixelIndex];
          if (pixel === segmentNumber) {
            x += i;
            y += j;
            count++;
          }
          pixelIndex++;
        }
      }
      x /= count;
      y /= count;

      const position = [x, y, frameIndex];
      const worldPos = _convertModelToWorldSpace(
        position,
        imageDataObject.vtkImageData
      );

      api.svgWidgets.rotatableCrosshairsWidget.moveCrosshairs(
        worldPos,
        mprApis[seriesInstanceUID]
      );
      done();
    },
    setSegmentationConfiguration: async ({
      viewports,
      globalOpacity,
      visible,
      renderOutline,
      outlineThickness,
    }) => {
      const allViewports = Object.values(viewports.viewportSpecificData);
      const promises = allViewports.map(async (viewport, viewportIndex) => {
        const apiIndex = getApiIndex(viewports, viewportIndex);
        const seriesInstanceUID = viewport.SeriesInstanceUID;
        let api = mprApis[seriesInstanceUID][apiIndex];

        if (!api) {
          api = await _getActiveViewportVTKApi(viewports);
          mprApis[seriesInstanceUID][apiIndex] = api;
        }

        api.setGlobalOpacity(globalOpacity);
        api.setVisibility(visible);
        api.setOutlineThickness(outlineThickness);
        api.setOutlineRendering(renderOutline);
        api.updateImage();
      });
      await Promise.all(promises);
    },
    setSegmentConfiguration: async ({ viewports, visible, segmentNumber }) => {
      const allViewports = Object.values(viewports.viewportSpecificData);
      const promises = allViewports.map(async (viewport, viewportIndex) => {
        const apiIndex = getApiIndex(viewports, viewportIndex);
        const seriesInstanceUID = viewport.SeriesInstanceUID;
        let api = mprApis[seriesInstanceUID][apiIndex];

        if (!api) {
          api = await _getActiveViewportVTKApi(viewports);
          mprApis[seriesInstanceUID][apiIndex] = api;
        }

        api.setSegmentVisibility(segmentNumber, visible);
        api.updateImage();
      });
      await Promise.all(promises);
    },
    enableRotateTool: () => {
      Object.keys(mprApis).forEach(key => {
        const apis = mprApis[key];
        apis.forEach((api, apiIndex) => {
          const istyle = vtkInteractorStyleMPRRotate.newInstance();

          api.setInteractorStyle({
            istyle,
            configuration: { apis, apiIndex, uid: api.uid },
          });
          setMouseConfigAfterToolEnabled(istyle, api, apis, apiIndex);
        });
      });
    },
    enableStackScrollTool: () => {
      Object.keys(mprApis).forEach(key => {
        const apis = mprApis[key];
        apis.forEach((api, apiIndex) => {
          setStyle(api, apis, apiIndex, 'stackscroll');
        });
      });
      store.dispatch(setActiveVtkTool('MPRStackScroll'));
    },
    enablePanTool: () => {
      Object.keys(mprApis).forEach(key => {
        const apis = mprApis[key];
        apis.forEach((api, apiIndex) => {
          setStyle(api, apis, apiIndex, 'pan');
        });
      });
    },
    enableCrosshairsTool: (syncWithOtherViewports = true) => {
      const firstApi = getFirstApiKey();
      const isCrosshairActive = isCrosshairStyle(
        '',
        mprApis[firstApi][0].genericRenderWindow.getRenderWindow()
      )
        ? true
        : false;
      if (isCrosshairActive) {
        store.dispatch(setActiveVtkTool(''));
      } else {
        store.dispatch(setActiveVtkTool('MPRCrosshairs'));
      }
      Object.keys(mprApis).forEach(key => {
        const apis = mprApis[key];
        apis.forEach((api, apiIndex) => {
          const crosshairWidget = api.svgWidgets.rotatableCrosshairsWidget
            ? api.svgWidgets.rotatableCrosshairsWidget
            : api.svgWidgets.crosshairsWidget;
          // If the crosshairs are visible and active for interaction, then hide it
          // Else ensure it is visible and make it active
          if (isCrosshairActive) {
            crosshairWidget.setDisplay(false);
            // Reset the mouse actions to the configured items
            setMouseConfig();
          } else {
            if (!crosshairWidget.getDisplay()) {
              crosshairWidget.setDisplay(true);
            }

            let istyle = null;
            if (isInSingleView || api.svgWidgets.crosshairsWidget) {
              istyle = vtkInteractorStyleMPRFovIndicators.newInstance();
            } else {
              istyle = vtkInteractorStyleDetachedRotatableMPRCrosshairs.newInstance();
            }
            api.setInteractorStyle({
              istyle,
              configuration: {
                apis,
                apiIndex,
                uid: api.uid,
              },
            });
            setMouseConfigAfterToolEnabled(istyle, api, apis, apiIndex);
          }
          api.svgWidgetManager.render();
        });

        const crosshairWidget = apis[0].svgWidgets.rotatableCrosshairsWidget
          ? apis[0].svgWidgets.rotatableCrosshairsWidget
          : apis[0].svgWidgets.crosshairsWidget;

        if (apis[0].svgWidgets.rotatableCrosshairsWidget) {
          const referenceLines = crosshairWidget.getReferenceLines();

          // Initialize crosshairs if not initialized.
          if (!referenceLines || !referenceLines[0]) {
            crosshairWidget.resetCrosshairs(apis, 0);
          }
        } else {
          crosshairWidget.resetCrosshairs(apis, 0);
          crosshairWidget.adjustIndicators(apis);
          if (syncWithOtherViewports) {
            updateOtherViewports(store.getState().viewports, apis[0], [
              'StackScroll',
            ]);
          }
        }
      });
    },
    enableLevelTool: () => {
      function updateVOI(apis, windowWidth, windowCenter) {
        apis.forEach(api => {
          api.updateVOI(windowWidth, windowCenter);
          const renderWindow = api.genericRenderWindow.getRenderWindow();
          renderWindow.render();
        });
      }

      const throttledUpdateVOIs = throttle(updateVOI, 16, { trailing: true }); // ~ 60 fps

      Object.keys(mprApis).forEach(key => {
        const apis = mprApis[key];
        apis.forEach((api, apiIndex) => {
          const istyle = vtkInteractorStyleMPRWindowLevel.newInstance();

          let initialWindowWidth = 0;
          let initialWindowCentre = 0;
          const callbacks = {
            setOnLevelsChanged: ({ windowCenter, windowWidth }) => {
              const sourcedx = windowWidth - initialWindowWidth;
              const sourcedy = initialWindowCentre - windowCenter;
              Object.keys(mprApis).forEach(key => {
                const apis = mprApis[key];
                if (apis.includes(api)) {
                  throttledUpdateVOIs(apis, windowWidth, windowCenter);
                } else {
                  const srcWindowingDiff = {
                    windowWidth: windowWidth - initialWindowWidth,
                    windowCenter: initialWindowCentre - windowCenter,
                  };
                  const targetApi = apis[0];
                  const targetIstyle = targetApi.genericRenderWindow
                    .getInteractor()
                    .getInteractorStyle();

                  const tgtWindowingDiff = getWindowingDifference(
                    srcWindowingDiff,
                    api,
                    targetApi
                  );
                  let newWindowWidth =
                    targetIstyle.getWindowLevel().windowWidth +
                    (isNaN(tgtWindowingDiff.windowWidth)
                      ? 0
                      : tgtWindowingDiff.windowWidth);
                  const newWindowCenter =
                    targetIstyle.getWindowLevel().windowCenter -
                    (isNaN(tgtWindowingDiff.windowCenter)
                      ? 0
                      : tgtWindowingDiff.windowCenter);
                  newWindowWidth = Math.max(0.01, newWindowWidth);

                  const lower = newWindowCenter - newWindowWidth / 2.0;
                  const upper = newWindowCenter + newWindowWidth / 2.0;
                  targetIstyle
                    .getVolumeActor()
                    .getProperty()
                    .getRGBTransferFunction(0)
                    .setMappingRange(lower, upper);

                  throttledUpdateVOIs(apis, newWindowWidth, newWindowCenter);

                  initialWindowWidth = windowWidth;
                  initialWindowCentre = windowCenter;
                }
              });
            },
          };

          api.setInteractorStyle({
            istyle,
            callbacks,
            configuration: { apis, apiIndex, uid: api.uid },
          });
          setMouseConfigAfterToolEnabled(istyle, api, apis, apiIndex);

          istyle.onStartInteractionEvent(() => {
            initialWindowWidth = istyle.getWindowLevel().windowWidth;
            initialWindowCentre = istyle.getWindowLevel().windowCenter;
          });
        });
      });
      store.dispatch(setActiveVtkTool('WWWC'));
    },
    setSlabThickness: ({ slabThickness }) => {
      Object.keys(mprApis).forEach(key => {
        const apis = mprApis[key];
        apis.forEach(api => {
          api.setSlabThickness(slabThickness);
        });
      });
    },
    changeSlabThickness: ({ change }) => {
      Object.keys(mprApis).forEach(key => {
        const apis = mprApis[key];
        apis.forEach(api => {
          const slabThickness = Math.max(api.getSlabThickness() + change, 0.1);

          api.setSlabThickness(slabThickness);
        });
      });
    },
    setBlendModeToComposite: () => {
      Object.keys(mprApis).forEach(key => {
        const apis = mprApis[key];
        apis.forEach((api, apiIndex) => {
          const renderWindow = api.genericRenderWindow.getRenderWindow();
          const istyle = renderWindow.getInteractor().getInteractorStyle();

          const slabThickness = api.getSlabThickness();

          const mapper = api.volumes[0].getMapper();
          if (mapper.setBlendModeToComposite) {
            mapper.setBlendModeToComposite();
          }

          if (istyle.setSlabThickness) {
            istyle.setSlabThickness(slabThickness);
          }
          renderWindow.render();
        });
      });
    },
    setBlendModeToMaximumIntensity: () => {
      Object.keys(mprApis).forEach(key => {
        const apis = mprApis[key];
        apis.forEach((api, apiIndex) => {
          const renderWindow = api.genericRenderWindow.getRenderWindow();
          const mapper = api.volumes[0].getMapper();
          if (mapper.setBlendModeToMaximumIntensity) {
            mapper.setBlendModeToMaximumIntensity();
          }
          renderWindow.render();
        });
      });
    },
    setBlendMode: ({ blendMode }) => {
      Object.keys(mprApis).forEach(key => {
        const apis = mprApis[key];
        apis.forEach((api, apiIndex) => {
          const renderWindow = api.genericRenderWindow.getRenderWindow();

          api.volumes[0].getMapper().setBlendMode(blendMode);

          renderWindow.render();
        });
      });
    },
    mpr2dFusion: async ({ viewports }) => {
      if (!Object.keys(mprApis).length) {
        const isFusionMode = true;
        await actions.mpr2d({ viewports, isFusionMode });
        actions.setFusionPreset({ presetName: DEFAULT_COLOR_ID });
      }
    },
    setFusionPreset: async ({ presetName }) => {
      Object.keys(mprApis).forEach(key => {
        const apis = mprApis[key];
        const actor2d = apis[0].volumes[1];

        const preset = vtkColorMaps.getPresetByName(presetName);

        [actor2d].forEach(actor => {
          if (!actor) {
            return;
          }

          const cfun = actor.getProperty().getRGBTransferFunction(0);

          // TODO: Looks like this is returned by reference and mutated when
          // applyColorMap is run, so we are copying the array with .slice().
          // - Bit surprised we have to do this though. I wonder where else this is
          // causing issues
          const cRange = cfun.getMappingRange().slice();
          cfun.applyColorMap(preset);

          const newCfun = vtkColorTransferFunction.newInstance();
          newCfun.applyColorMap(preset);
          newCfun.setMappingRange(cRange[0], cRange[1]);

          // TODO: Why doesn't mutating the current RGBTransferFunction work?
          actor.getProperty().setRGBTransferFunction(0, newCfun);
        });
        apis.forEach((api, apiIndex) => {
          const renderWindow = api.genericRenderWindow.getRenderWindow();

          renderWindow.render();
        });
      });
    },
    mpr2d: async ({ viewports, viewerProperties, isFusionMode = false }) => {
      const infusions = store.getState().infusions;
      if (infusions.protocolLayout) {
        // Clear applied protocol layout
        commandsManager.runCommand('clearProtocolLayout', {});
        const unsubscribeFn = store.subscribe(async () => {
          const state = store.getState();
          if (
            !state.infusions.protocolLayout &&
            cornerstone.getEnabledElements().length ===
              state.viewports.layout.viewports.length
          ) {
            unsubscribeFn();
            await createAndInitializeMPR2DLayout(
              viewports,
              viewerProperties?.orientations,
              isFusionMode
            );
            if (viewerProperties) {
              actions.updateViewportProperties({
                viewports: store.getState().viewports,
                propertiesToSync: viewerProperties,
              });
            }
          }
        });
      } else {
        await createAndInitializeMPR2DLayout(
          viewports,
          viewerProperties?.orientations,
          isFusionMode
        );
        if (viewerProperties) {
          actions.updateViewportProperties({
            viewports: store.getState().viewports,
            propertiesToSync: viewerProperties,
          });
        }
      }
      store.dispatch(setViewportActive(0));
    },
    enableZoomTool: () => {
      const projectConfig = selectProjectConfig(store.getState());
      const zoomLevel = projectConfig?.zoomLevel;
      Object.keys(mprApis).forEach(key => {
        const apis = mprApis[key];
        const radius = getVolumeRadius(apis[0]);
        apis.forEach((api, apiIndex) => {
          const istyle = vtkInteractorStyleMPRSlice.newInstance();
          const interactorStyleDefinitions = getCustomInteractorStyleDefinitions(
            1,
            'zoom',
            false,
            true
          );
          api.setInteractorStyle({
            istyle,
            configuration: { apis, apiIndex, uid: api.uid },
          });
          InteractionPresets.applyDefinitions(
            interactorStyleDefinitions,
            istyle
          );
          setApisForModel(interactorStyleDefinitions, istyle, apis, api);
          const scrollIndex = interactorStyleDefinitions.findIndex(
            def => def.options.button === 2 && def.options.scrollEnabled
          );
          if (scrollIndex !== -1) {
            addScrollListener(istyle, api, apis, apiIndex, scrollIndex);
          }

          registerSyncCallback(apiIndex, api, mprApis, 'zoom');
        });
      });
      store.dispatch(setActiveVtkTool('MPRZoom'));
    },
    panelView: async ({ event, viewports }) => {
      const projectConfig = selectProjectConfig(store.getState());
      const dissociateCrosshairs =
        projectConfig?.dissociateCrosshairs === undefined
          ? true
          : projectConfig?.dissociateCrosshairs;

      // TODO: Double click feature allowed only in the 2D MPR layout or all
      // the viewports are vtk js layout. The behavior will be skipped
      // in single vtk js viewport(after restoring the reader task state)
      // and the mixed HP layout.
      if (
        !isInSingleView &&
        (!viewports.layout.viewports.every(
          vp => !!vp.vtk || vp.plugin === 'vtk'
        ) ||
          viewports.layout.viewports.length === 1)
      ) {
        return;
      }

      if (dissociateCrosshairs) {
        const linkedViewports = checkLinkViewportActivated(event);

        if (linkedViewports) {
          const noOfSeries = Object.keys(mprApis).length;
          let viewportIndex = 0;
          Object.keys(mprApis).forEach(key => {
            const apis = mprApis[key];

            apis.forEach(api => {
              api.svgWidgets.rotatableCrosshairsWidget.setViewportActive(true);
              api.svgWidgets.rotatableCrosshairsWidget.setBorder(
                getWidgetBorderStyle(true, true, viewportIndex, noOfSeries)
              );
              api.genericRenderWindow.getRenderWindow().render();
              viewportIndex++;
            });
          });
          return;
        }
      }

      let layout = null;
      if (!isInSingleView) {
        viewportLayoutData.viewPortIndex = viewports.activeViewportIndex % 3;
        restoreLayoutData =
          getCurrentDisplayState(
            mprApis,
            viewportLayoutData.viewPortIndex,
            viewports
          ) || [];
        viewportLayoutData.doubleClickedSeriesInstanceUID =
          viewports.viewportSpecificData[viewports.activeViewportIndex]
            .SeriesInstanceUID || null;
        isInSingleView = true;
        Object.keys(mprApis).forEach(key => {
          mprApis[key] = [];
        });
        mprApis = {};
        const newViewports = [{ plugin: 'vtk' }];
        layout = {
          numRows: 1,
          numColumns: 1,
          viewports: newViewports,
        };
      } else {
        isInSingleView = false;
        const multiSessionIndex = restoreLayoutData.findIndex(
          item =>
            item.activeDisplaySet.SeriesInstanceUID ===
            viewportLayoutData.doubleClickedSeriesInstanceUID
        );
        const currentDisplayState =
          getCurrentDisplayState(mprApis, 0, viewports) || [];
        restoreLayoutData[multiSessionIndex].slice =
          currentDisplayState[0].slice;
        restoreLayoutData[multiSessionIndex].worldPos =
          currentDisplayState[0].worldPos;
        const pScale = currentDisplayState[0].parallelScale;
        const slabThickness = currentDisplayState[0].slabThickness;
        restoreLayoutData.map(item => {
          item.parallelScale = pScale;
          item.slabThickness = slabThickness;
        });
        Object.keys(mprApis).forEach(key => {
          mprApis[key] = [];
        });
        mprApis = {};
        const newViewports = [];
        for (let i = 0; i < 3; i++) {
          const plugin = { plugin: 'vtk' };
          newViewports.push(plugin);
        }
        const numberOfRowsForLayout =
          restoreLayoutData[multiSessionIndex].numberOfLayout / 3;
        layout = {
          numRows: numberOfRowsForLayout,
          numColumns: 3,
          viewports: newViewports,
        };
      }
      // Vtkjs fires the mouseup events during the double click.
      // This is work around fix for delaying the layout change and vtkjs api component unmounting after the mouseup event gets fired.
      setTimeout(async () => {
        setLayoutAndViewportData(layout, {});
        setLayoutToSingleOrMultiple(isInSingleView);
      }, 10);
    },
    initializeWithProtocolView: async ({
      viewports,
      viewportSpecificData,
      viewportProps,
      layout,
      hideIndicators,
    }) => {
      // TODO push a lot of this backdoor logic lower down to the library level.
      initializeMprAttributes();
      defaultVOI = {};

      const numRows = layout && layout.numRows ? layout.numRows : 1;
      const numColumns = layout && layout.numColumns ? layout.numColumns : 3;
      const fovDetails = {};
      const viewportProperties = layout?.viewportProperties
        ? layout.viewportProperties
        : [];
      // Get current VOI if cornerstone viewport.
      try {
        mprApis = await setProtocolLayout(
          viewportSpecificData,
          viewportProps,
          numRows,
          numColumns,
          fovDetails,
          viewportProperties,
          false
        );
      } catch (error) {
        throw new Error(error);
      }
      let preferredTool;
      if (
        Object.keys(viewportSpecificData).find(key =>
          viewportSpecificData[key].plugin?.includes('cornerstone')
        )
      ) {
        preferredTool = 'Crosshair';
      }
      initializeMPRApis(false, false, preferredTool);
      isMprApisInitialized = true;

      const state = store.getState();
      const projectConfig = getCondensedProjectConfig();

      Object.keys(mprApis).forEach(key => {
        const apis = mprApis[key];

        // Find the displaySet to get the cornerstone voi for initialization
        const displaySetKey = Object.keys(viewportSpecificData).find(
          dsKey =>
            viewportSpecificData[dsKey] &&
            viewportSpecificData[dsKey].SeriesInstanceUID === key
        );

        // Find is the 2D viewport of the same series available for initialization of VOI
        const is2DViewportExist = !!Object.keys(viewportSpecificData).find(
          dsKey =>
            viewportSpecificData[dsKey] &&
            viewportSpecificData[dsKey].SeriesInstanceUID === key &&
            viewportSpecificData[dsKey].plugin === 'cornerstone'
        );

        const displaySet = viewportSpecificData[displaySetKey];
        let cornerstoneVOI = getVOIFromCornerstoneViewport(true, displaySet);
        if (cornerstoneVOI) {
          setVOI(cornerstoneVOI, key);
        } else {
          if (!is2DViewportExist) {
            const voi = apis[0].genericRenderWindow
              .getInteractor()
              .getInteractorStyle()
              .getWindowLevel?.();
            if (voi) {
              cornerstoneVOI = {
                windowWidth: voi.windowWidth,
                windowCenter: voi.windowCenter,
              };
            }
          }
          if (!cornerstoneVOI) {
            cornerstoneVOI = getVOIWithDisplaySet(displaySet, key);
            if (cornerstoneVOI?.windowWidth || cornerstoneVOI?.windowCenter) {
              setVOI(cornerstoneVOI, key);
            }
          }
        }
        defaultVOI[key] = cornerstoneVOI;

        const imageIds =
          OHIFVTKViewport.getCornerstoneStackFromDisplaySet(
            studyMetadataManager.get(displaySet.StudyInstanceUID)._data,
            displaySet
          )?.imageIds || [];

        if (!hideIndicators) {
          fovGrid.fovDetail = fovDetails;
          initializeMPRApiIndicators(
            imageIds,
            apis,
            fovDetails[key],
            projectConfig,
            key
          );
        }

        if (cornerstoneVOI) {
          apis.forEach(api =>
            api.genericRenderWindow.getRenderWindow().render()
          );
        }

        const frameIndex = displaySet.frameIndex || 0;
        if (imageIds.length > frameIndex) {
          updateViewWithImage(
            state.viewports,
            displaySet,
            imageIds[frameIndex]
          );
        }
      });
    },
    updateViewportProperties: ({ viewports, propertiesToSync }) => {
      if (mprApis[propertiesToSync.seriesInstanceUID]) {
        const apis = mprApis[propertiesToSync.seriesInstanceUID];
        const widgetName = apis[0].svgWidgets.rotatableCrosshairsWidget
          ? 'rotatableCrosshairsWidget'
          : 'crosshairsWidget';
        let pScale = 0;
        if (propertiesToSync && propertiesToSync.windowLevel) {
          if (!defaultVOI[propertiesToSync.seriesInstanceUID]) {
            defaultVOI[propertiesToSync.seriesInstanceUID] =
              propertiesToSync.windowLevel;
          }
          setVOI(
            propertiesToSync.windowLevel,
            propertiesToSync.seriesInstanceUID
          );
        }
        if (propertiesToSync?.zoom?.hasOwnProperty('scale')) {
          const radius =
            apis.length > 0 ? getVolumeRadius(apis[0]) || 100 : 100;
          pScale = radius - propertiesToSync.zoom.scale * radius;
          pScale = validateScale(radius, pScale);
        }
        if (propertiesToSync?.patientPos) {
          const viewportDetails = getMatchingVTKViewportData(
            viewports,
            propertiesToSync.seriesInstanceUID
          );
          const imageIds = OHIFVTKViewport.getCornerstoneStackFromDisplaySet(
            studyMetadataManager.get(propertiesToSync.studyInstanceUID)._data,
            viewportDetails.displaySet
          )?.imageIds;
          if (imageIds?.length) {
            const imagePlane = cornerstone.metaData.get(
              'imagePlaneModule',
              imageIds[0]
            );
            let patientPos = Array.isArray(propertiesToSync.patientPos)
              ? propertiesToSync.patientPos
              : [
                  propertiesToSync.patientPos.x,
                  propertiesToSync.patientPos.y,
                  propertiesToSync.patientPos.z,
                ];
            // Adjust the position from 2D DICOM orientation to sync with vtk js orientation
            const planeData = convertPlanesDataFrom2DToVtkjsOrientation(
              [patientPos],
              imagePlane
            );
            patientPos =
              planeData && planeData.worldPosArray?.length
                ? planeData.worldPosArray[0]
                : patientPos;

            const dPos = vtkCoordinate.newInstance();
            dPos.setCoordinateSystemToWorld();

            dPos.setValue(patientPos[0], patientPos[1], patientPos[2]);
            let worldPos = dPos.getComputedWorldValue(
              apis[0].genericRenderWindow.getRenderer()
            );

            apis[0].svgWidgets[widgetName]?.moveCrosshairs(worldPos, apis);
          }
        }
        apis.forEach(api => {
          const renderWindow = api.genericRenderWindow.getRenderWindow();
          if (pScale > 0) {
            api.genericRenderWindow
              .getRenderer()
              .getActiveCamera()
              .setParallelScale(pScale);
          }
          if (propertiesToSync?.resize) {
            api.genericRenderWindow.resize();
          }
          renderWindow.render();
          api.svgWidgets[widgetName]?.updateCrosshairForApi(api);
          if (api.svgWidgets.crosshairsWidget) {
            api.svgWidgets.crosshairsWidget?.adjustIndicators(apis);
          }
        });
      }

      if (propertiesToSync.defaultRoiLabels) {
        updateFOVGrid(propertiesToSync);
      }
    },
    exit2dMprView: async () => {
      const viewports = store.getState().viewports || {};
      const displaySet =
        viewports.viewportSpecificData[viewports.activeViewportIndex];
      let properties = [];
      Object.keys(mprApis).forEach(key => {
        const apis = mprApis[key];

        const imageIds = OHIFVTKViewport.getCornerstoneStackFromDisplaySet(
          studyMetadataManager.get(displaySet.StudyInstanceUID)._data,
          displaySet
        )?.imageIds;
        let patientPos = getImagePosition(displaySet, apis[0]);

        const parallelScale = apis[0].genericRenderWindow
          .getRenderer()
          .getActiveCamera?.()
          .getParallelScale();
        const physicalScale = apis[0].genericRenderWindow
          .getRenderer()
          .getActiveCamera?.()
          .getPhysicalScale();
        const windowRange = apis[0].volumes[0]
          .getProperty()
          .getRGBTransferFunction?.(0)
          .getMappingRange()
          .slice();

        const window = toWindowLevel(...windowRange).windowWidth;
        const level = toWindowLevel(...windowRange).windowCenter;

        let scale = getScale(parallelScale, physicalScale, imageIds);

        properties.push({
          seriesInstanceUID: displaySet.SeriesInstanceUID,
          studyInstanceUID: displaySet.StudyInstanceUID,
          windowLevel: {
            windowCenter: Number(level),
            windowWidth: Number(window),
          },
          zoom: { scale: scale, type: 'exact' },
          patientPos: patientPos,
        });
      });

      commandsManager.runCommand('setCornerstoneLayout');
      properties.forEach(propertiesToSync => {
        setTimeout(() => {
          commandsManager.runCommand('update2DViewport', {
            viewports,
            propertiesToSync,
          });
        }, 500);
      });
      store.dispatch(setViewportActive(0));
      initializeMprAttributes();
    },
    setColorIndicationFor2DMPR: async () => {
      const state = store.getState();
      let projectConfig = selectProjectConfig(state);

      const sliceSettings = projectConfig?.bSlices?.settings;
      Object.keys(mprApis).forEach(key => {
        const apis = mprApis[key];
        apis.forEach(api => {
          const crosshairsWidget = api.svgWidgets.crosshairsWidget;
          if (!crosshairsWidget) {
            return;
          }
          const indicatorsDataInWorld = api.svgWidgets.crosshairsWidget.getIndicatorsDataInWorld();
          if (indicatorsDataInWorld?.length) {
            bSliceUtils.updateBsliceIndicatorColor(
              indicatorsDataInWorld,
              sliceSettings
            );
            api.svgWidgets.crosshairsWidget.setIndicatorsDataInWorld(
              indicatorsDataInWorld
            );
            api.svgWidgets.crosshairsWidget.adjustIndicators(apis);
          }
        });
      });
    },
    scaleViewport: async ({ direction }) => {
      Object.keys(mprApis).forEach(key => {
        const apis = mprApis[key];
        apis.forEach((api, apiIndex) => {
          const renderer = api.genericRenderWindow;
          const camera = renderer.getRenderer().getActiveCamera();
          if (camera.getParallelProjection()) {
            const scale = camera.getParallelScale();
            camera.setParallelScale((1 - direction) * scale);
            renderer.getRenderWindow().render();
          }
        });
      });
    },
    fitViewportToWindow: async () => {
      Object.keys(mprApis).forEach(key => {
        const apis = mprApis[key];
        apis.forEach((api, apiIndex) => {
          fitToViewport(api);
        });
      });
    },
    nextImage: ({ viewports }) => {
      const viewportIndex = getApiIndex(viewports);
      Object.keys(mprApis).forEach(key => {
        const apis = mprApis[key];
        const activeApi = apis[viewportIndex];
        setNewSlice(activeApi, -1);
        const renderer = activeApi.genericRenderWindow.getRenderer();
        if (activeApi?.svgWidgets?.rotatableCrosshairsWidget) {
          const pos = getPosition(activeApi, renderer);
          const apiIndex = apis.findIndex(x => x.uid === activeApi.uid);
          moveCrosshairs(apis, apiIndex, pos, renderer);
        }
        renderer.getRenderWindow().render();
      });
    },
    previousImage: ({ viewports }) => {
      const viewportIndex = getApiIndex(viewports);
      Object.keys(mprApis).forEach(key => {
        const apis = mprApis[key];
        const activeApi = apis[viewportIndex];
        setNewSlice(activeApi, 1);
        const renderer = activeApi.genericRenderWindow.getRenderer();
        if (activeApi?.svgWidgets?.rotatableCrosshairsWidget) {
          const pos = getPosition(activeApi, renderer);
          const apiIndex = apis.findIndex(x => x.uid === activeApi.uid);
          moveCrosshairs(apis, apiIndex, pos, renderer);
        }
        renderer.getRenderWindow().render();
      });
    },
    setFullDynamicWWWC: ({ viewports }) => {
      Object.keys(mprApis).forEach(key => {
        const apis = mprApis[key];
        apis.forEach((api, apiIndex) => {
          const apiData = getActiveApiDetailsFromMouseData(
            api.genericRenderWindow.getRenderWindow()
          );
          if (!apiData) {
            return null;
          }

          const viewportDetails = getMatchingVTKViewportData(
            viewports,
            apiData.seriesInstanceUID
          );
          const imageIds = OHIFVTKViewport.getCornerstoneStackFromDisplaySet(
            studyMetadataManager.get(
              viewportDetails.displaySet.StudyInstanceUID
            )._data,
            viewportDetails.displaySet
          )?.imageIds;
          const patientPos = getImagePosition(viewportDetails.displaySet, api);
          const newImageIdIndex = cornerstoneUtils.getNearestSliceFromImagesIds(
            imageIds,
            patientPos
          );
          const imageId = imageIds[newImageIdIndex];
          new Promise(resolve => {
            cornerstone.loadImage(imageId).then(image => {
              const { windowWidth, windowCenter } = getFullDynamicWWWC(image);
              setWindowLevel(apis, windowWidth, windowCenter);
              resolve(true);
            });
          });
        });
      });
    },
    setWindowLevelPreset: ({ preset }) => {
      const state = store.getState();
      const { preferences = {} } = state;
      const { window, level } =
        preferences.windowLevelData && preferences.windowLevelData[preset];

      if (window && level) {
        Object.keys(mprApis).forEach(key => {
          const apis = mprApis[key];
          setWindowLevel(apis, window, level);
        });
      }
    },
  };

  window.vtkActions = actions;

  const definitions = {
    requestNewSegmentation: {
      commandFn: actions.requestNewSegmentation,
      storeContexts: ['viewports'],
      options: {},
    },
    jumpToSlice: {
      commandFn: actions.jumpToSlice,
      storeContexts: ['viewports'],
      options: {},
    },
    setSegmentationConfiguration: {
      commandFn: actions.setSegmentationConfiguration,
      storeContexts: ['viewports'],
      options: {},
    },
    setSegmentConfiguration: {
      commandFn: actions.setSegmentConfiguration,
      storeContexts: ['viewports'],
      options: {},
    },
    axial: {
      commandFn: actions.axial,
      storeContexts: ['viewports'],
      options: {},
    },
    coronal: {
      commandFn: actions.coronal,
      storeContexts: ['viewports'],
      options: {},
    },
    sagittal: {
      commandFn: actions.sagittal,
      storeContexts: ['viewports'],
      options: {},
    },
    enableRotateTool: {
      commandFn: actions.enableRotateTool,
      options: {},
    },
    enableCrosshairsTool: {
      commandFn: actions.enableCrosshairsTool,
      options: {},
    },
    enableLevelTool: {
      commandFn: actions.enableLevelTool,
      options: {},
    },
    resetMPRView: {
      commandFn: actions.resetMPRView,
      options: {},
    },
    setBlendModeToComposite: {
      commandFn: actions.setBlendModeToComposite,
      options: { blendMode: BlendMode.COMPOSITE_BLEND },
    },
    setBlendModeToMaximumIntensity: {
      commandFn: actions.setBlendModeToMaximumIntensity,
      options: { blendMode: BlendMode.MAXIMUM_INTENSITY_BLEND },
    },
    setBlendModeToMinimumIntensity: {
      commandFn: actions.setBlendMode,
      options: { blendMode: BlendMode.MINIMUM_INTENSITY_BLEND },
    },
    setBlendModeToAverageIntensity: {
      commandFn: actions.setBlendMode,
      options: { blendMode: BlendMode.AVERAGE_INTENSITY_BLEND },
    },
    setSlabThickness: {
      // TODO: How do we pass in a function argument?
      commandFn: actions.setSlabThickness,
      options: {},
    },
    increaseSlabThickness: {
      commandFn: actions.changeSlabThickness,
      options: {
        change: 3,
      },
    },
    decreaseSlabThickness: {
      commandFn: actions.changeSlabThickness,
      options: {
        change: -3,
      },
    },
    mpr2d: {
      commandFn: actions.mpr2d,
      storeContexts: ['viewports'],
      options: {},
      context: 'VIEWER',
      disabledStoreContexts: ['viewports', 'infusions'],
      isDisabled: args => {
        const { viewports = {}, infusions = {} } = args || {};
        const { viewportSpecificData = {} } = viewports;

        // Hide the 2D MPR tool action from shortcut when protocol layout with a vtk js(MPR view) viewport is active
        return Object.keys(viewportSpecificData).find(
          key => viewportSpecificData[key].plugin === 'vtk'
        )
          ? store.getState().infusions?.protocolLayout
          : false;
      },
    },
    mpr2dFusion: {
      commandFn: actions.mpr2dFusion,
      storeContexts: ['viewports'],
      options: {},
      context: 'VIEWER',
      disabledStoreContexts: ['viewports', 'infusions'],
      isDisabled: args => {
        const { viewports = {}, infusions = {} } = args || {};
        const { viewportSpecificData = {} } = viewports;

        // Hide the 2D MPR tool action from shortcut when protocol layout with a vtk js(MPR view) viewport is active
        return Object.keys(viewportSpecificData).find(
          key => viewportSpecificData[key].plugin === 'vtk'
        )
          ? store.getState().infusions?.protocolLayout
          : false;
      },
    },
    setFusionPreset: {
      commandFn: actions.setFusionPreset,
      context: 'VIEWER',
      disabledStoreContexts: ['viewports', 'infusions'],
    },
    getVtkApiForViewportIndex: {
      commandFn: actions.getVtkApis,
      context: 'VIEWER',
    },
    getVtkPropertiesForView: {
      commandFn: actions.getVtkPropertiesForView,
      context: 'VIEWER',
    },
    enableZoomTool: {
      commandFn: actions.enableZoomTool,
      context: 'VIEWER',
    },
    enableStackScrollTool: {
      commandFn: actions.enableStackScrollTool,
      options: {},
    },
    panelView: {
      commandFn: actions.panelView,
      context: 'VIEWER',
    },
    protocolView: {
      commandFn: actions.initializeWithProtocolView,
      storeContexts: ['viewports'],
      options: {},
      context: 'VIEWER',
    },
    update2DMPRViewport: {
      commandFn: actions.updateViewportProperties,
      storeContexts: ['viewports'],
      options: {},
      context: 'VIEWER',
    },
    exit2dMprView: {
      commandFn: actions.exit2dMprView,
      storeContexts: ['viewports'],
      options: {},
      context: 'VIEWER',
    },
    setColorIndicationFor2DMPR: {
      commandFn: actions.setColorIndicationFor2DMPR,
      storeContexts: ['viewports'],
      options: {},
      context: 'VIEWER',
    },
    scaleUpViewport: {
      commandFn: actions.scaleViewport,
      storeContexts: ['viewports'],
      options: { direction: 0.1 },
    },
    scaleDownViewport: {
      commandFn: actions.scaleViewport,
      storeContexts: ['viewports'],
      options: { direction: -0.1 },
    },
    fitViewportToWindow: {
      commandFn: actions.fitViewportToWindow,
      storeContexts: ['viewports'],
      options: { direction: 0 },
    },
    nextImage: {
      commandFn: actions.nextImage,
      storeContexts: ['viewports'],
      options: {},
    },
    previousImage: {
      commandFn: actions.previousImage,
      storeContexts: ['viewports'],
      options: {},
    },
    resetViewport: {
      commandFn: actions.resetMPRView,
      options: {},
    },
    setFullDynamicWWWC: {
      commandFn: actions.setFullDynamicWWWC,
      storeContexts: ['viewports'],
      options: {},
    },
    // Window level Presets
    windowLevelPreset1: {
      commandFn: actions.setWindowLevelPreset,
      storeContexts: ['viewports'],
      options: { preset: 1 },
    },
    windowLevelPreset2: {
      commandFn: actions.setWindowLevelPreset,
      storeContexts: ['viewports'],
      options: { preset: 2 },
    },
    windowLevelPreset3: {
      commandFn: actions.setWindowLevelPreset,
      storeContexts: ['viewports'],
      options: { preset: 3 },
    },
    windowLevelPreset4: {
      commandFn: actions.setWindowLevelPreset,
      storeContexts: ['viewports'],
      options: { preset: 4 },
    },
    windowLevelPreset5: {
      commandFn: actions.setWindowLevelPreset,
      storeContexts: ['viewports'],
      options: { preset: 5 },
    },
    windowLevelPreset6: {
      commandFn: actions.setWindowLevelPreset,
      storeContexts: ['viewports'],
      options: { preset: 6 },
    },
    windowLevelPreset7: {
      commandFn: actions.setWindowLevelPreset,
      storeContexts: ['viewports'],
      options: { preset: 7 },
    },
    windowLevelPreset8: {
      commandFn: actions.setWindowLevelPreset,
      storeContexts: ['viewports'],
      options: { preset: 8 },
    },
    windowLevelPreset9: {
      commandFn: actions.setWindowLevelPreset,
      storeContexts: ['viewports'],
      options: { preset: 9 },
    },
  };

  return {
    definitions,
    defaultContext: 'ACTIVE_VIEWPORT::VTK',
  };
};

/** Set apis in manipulators
 *
 * @param {Array} interactorStyleDefinitions
 * @param {Object} istyle
 * @param {Array} apis
 * @param {Array} api
 */
function setApisForModel(interactorStyleDefinitions, istyle, apis, api) {
  for (let idx = 0; idx < interactorStyleDefinitions.length; idx++) {
    const currentManipulator = istyle.getMouseManipulator?.(idx);
    currentManipulator?.setApis?.(apis);
    currentManipulator?.setApiInstance?.(api);
  }
}

export default commandsModule;
