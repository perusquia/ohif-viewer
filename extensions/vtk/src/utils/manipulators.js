const manipulators = {
  vtkStackScrollManipulators: 'stackscroll',
  vtkWwwcManipulator: 'wwwc',
};

export default manipulators;
