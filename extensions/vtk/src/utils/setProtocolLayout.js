import setLayoutAndViewportData from './setLayoutAndViewportData.js';

// Supports creation of vtk js viewports along with the cornerstone viewport(mix viewports)
export default function setProtocolLayout(
  appliedDisplaysets,
  viewportPropsArray,
  numRows = 1,
  numColumns = 1,
  fovDetails = null,
  viewportProperties = {},
  isFusionMode = false
) {
  return new Promise((resolve, reject) => {
    const viewports = [];
    const numViewports = Object.keys(appliedDisplaysets).length;

    if (viewportPropsArray && viewportPropsArray.length !== numViewports) {
      reject(
        new Error(
          'viewportProps is supplied but its length is not equal to numViewports'
        )
      );
    }

    const viewportSpecificData = appliedDisplaysets;

    for (let i = 0; i < numViewports; i++) {
      viewports.push(
        viewportSpecificData?.[i]?.plugin
          ? { plugin: viewportSpecificData[i].plugin }
          : {}
      );
    }

    const mprApis = {};
    const apis = [];
    viewports.forEach((viewport, index) => {
      if (!viewportPropsArray[index]) {
        // For non-vtk js viewports, skip creation
        return;
      }
      apis[index] = null;
      const viewportProps = viewportPropsArray[index];
      viewports[index] = Object.assign({}, viewports[index], {
        vtk: {
          mode: 'mpr', // TODO: not used
          isFusionMode,
          afterCreation: api => {
            apis[index] = api;
            const seriesInstanceUID = viewportSpecificData[index]
              ? viewportSpecificData[index].SeriesInstanceUID
              : viewportSpecificData[0].SeriesInstanceUID;
            if (!mprApis[seriesInstanceUID]) {
              mprApis[seriesInstanceUID] = [];
            }
            mprApis[seriesInstanceUID].push(api);

            // Fill the fov details to display the fov grid on viewport from the protocol viewport configuration
            if (fovDetails) {
              if (!fovDetails[seriesInstanceUID]) {
                fovDetails[seriesInstanceUID] = [];
              }
              let fovData = null;
              if (
                fovDetails &&
                viewportProps.showFov &&
                viewportProps.fovDiameters
              ) {
                fovData = {
                  showFov: viewportProps.showFov,
                  fovDiameters: viewportProps.fovDiameters,
                };
                fovData.hideFovGrid = viewportProps.hideFovGrid;
              }
              fovDetails[seriesInstanceUID].push(fovData);
            }

            if (apis.every(a => !!a)) {
              const apiArray = [];
              apis.forEach(api => apiArray.push(api));
              resolve(mprApis);
            }
          },
          ...viewportProps,
        },
      });
    });

    let index = 0;
    viewportProperties.forEach(viewportProp => {
      viewportProp.forEach(viewport => {
        viewports[index] = {
          ...viewports[index],
          readOnly: viewport?.readOnly,
        };
        index++;
      });
    });

    setLayoutAndViewportData(
      {
        numRows,
        numColumns,
        viewports,
      },
      viewportSpecificData
    );
  });
}
