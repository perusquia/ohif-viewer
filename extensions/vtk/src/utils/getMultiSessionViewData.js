export function getMultiSessionViewStatus() {
  return window.store?.getState()?.multiSessionData?.isMultiSessionViewEnabled;
}

export function getMultiSessionViewCount() {
  return window.store?.getState()?.multiSessionData?.acquisitionData?.length;
}

export function getMultiSessionViewSeries() {
  const acquisitionData = window.store?.getState()?.multiSessionData
    ?.acquisitionData;
  const seriesUids = [];
  if (!!acquisitionData && acquisitionData.length > 1) {
    acquisitionData.forEach(acquisition => {
      seriesUids.push(acquisition.seriesUid);
    });
  }
  return seriesUids;
}
