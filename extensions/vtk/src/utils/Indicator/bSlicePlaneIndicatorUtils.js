import { vec3 } from 'gl-matrix';
import vtkCoordinate from 'vtk.js/Sources/Rendering/Core/Coordinate';
import cornerstoneTools from 'cornerstone-tools';
import cornerstoneMath from 'cornerstone-math';
import { Utils } from '@flywheel/extension-flywheel-common';
import {
  convertPlanesDataFrom2DToVtkjsOrientation,
  convertPlanesDataFromVtkjsOrientationTo2D,
} from '../viewportUtils.js';
import { INDICATOR_TYPES } from '../constants.js';
import { PlaneIndicatorData } from './IndicatorData.js';
import { getIndicatorHitStatus } from './IndicatorUtils.js';

const { bSliceColorUtils } = Utils;
const convertToVector3 = cornerstoneTools.import('util/convertToVector3');
const imagePointToPatientPoint = cornerstoneTools.import('util/imagePointToPatientPoint');

/**
 * Retrieves the B Slice indicators data in world(patient) position
 * @param {Object} bSliceSettings - b slice settings
 * @param {Array} imageIds - image id collection
 * @param {Object} renderer - api renderer object
 * @returns {Array} b slice indicator collection in world co-ordinate
 */
export const getBSliceIndicatorDataInWorld = (bSliceSettings, imageIds, renderer) => {
  const slicesBoundDataInWorld = [];
  const sliceThickness = calculateSliceThicknessFromPatientPos(imageIds);
  (imageIds || []).forEach((imageId, index) => {
    const sliceNumberKey = `${index + 1}`;
    if (bSliceSettings && bSliceSettings[sliceNumberKey]) {
      const imagePlane = cornerstone.metaData.get('imagePlaneModule', imageId);

      if (imagePlane?.imagePositionPatient) {
        const sliceBound = getSliceBoundInWorld(imagePlane, sliceThickness);

        if (!sliceBound || sliceBound.length < 6) {
          return;
        }

        const dPos = vtkCoordinate.newInstance();
        dPos.setCoordinateSystemToWorld();

        dPos.setValue(sliceBound[0], sliceBound[1], sliceBound[2]);
        const tlWorldPos = dPos.getComputedWorldValue(renderer);

        dPos.setValue(sliceBound[3], sliceBound[4], sliceBound[5]);
        const brWorldPos = dPos.getComputedWorldValue(renderer);

        const planeIndicator = new PlaneIndicatorData(
          "rectangle",
          bSliceColorUtils.getBSliceIndicatorColor(sliceNumberKey, bSliceSettings) || null);
        planeIndicator.TopLeft = tlWorldPos;
        planeIndicator.BottomRight = brWorldPos;
        planeIndicator.PlaneNumber = index + 1;
        slicesBoundDataInWorld.push(planeIndicator);
      }
    }
  });

  return slicesBoundDataInWorld;
}

/**
 * Update the b slice plane indicator color based on the b slice settings
 * @param {Array} indicatorsDataInWorld - indicator data in world co-ordinates
 * @param {Object} bSliceSettings - b slice settings
 * @returns Nil
 */
export const updateBsliceIndicatorColor = (indicatorsDataInWorld, bSliceSettings) => {
  if (indicatorsDataInWorld?.length) {
    indicatorsDataInWorld.forEach((indicator, index) => {
      if (indicator.Type === INDICATOR_TYPES.PLANE) {
        const sliceNumberKey = `${indicator.PlaneNumber}`;
        if (bSliceSettings && bSliceSettings[sliceNumberKey]) {
          indicator.Color = bSliceColorUtils.getBSliceIndicatorColor(sliceNumberKey, bSliceSettings) || indicator.Color;
        }
      }
    });
  }
}

/**
 * Retrieve the B Slice bound center position in world co-ordinate
 * @param {Object} bSliceIndicator - b slice indicator for which center to be found
 * @returns {Array} center position
 */
export const getBSliceCenterInWorld = (bSliceIndicator) => {
  return [(bSliceIndicator.TopLeft[0] + bSliceIndicator.BottomRight[0]) / 2,
  (bSliceIndicator.TopLeft[1] + bSliceIndicator.BottomRight[1]) / 2,
  (bSliceIndicator.TopLeft[2] + bSliceIndicator.BottomRight[2]) / 2];
}

/**
 * Retrieves the B Slice bound data in world(patient) position from the image plane
 * @param {Object} imagePlane - image plane metadata
 * @param {number} sliceThickness - image slice thickness to use
 * @returns {Object} b slice bound data
 */
export const getSliceBoundInWorld = (imagePlane, sliceThickness) => {
  if (imagePlane?.imagePositionPatient && sliceThickness) {
    const imagePosition = imagePlane.imagePositionPatient;
    const brPosition = imagePointToPatientPoint({ x: imagePlane.columns - 1, y: imagePlane.rows - 1 }, imagePlane);
    const brImagePosition = [brPosition.x, brPosition.y, brPosition.z];

    const planeData = convertPlanesDataFrom2DToVtkjsOrientation([imagePosition, brImagePosition], imagePlane);
    if (!planeData?.worldPosArray?.length) {
      return [tlBoundPos.x, tlBoundPos.y, tlBoundPos.z, brBoundPos.x, brBoundPos.y, brBoundPos.z];;
    }

    const dirVec = new cornerstoneMath.Vector3(planeData.normal[0], planeData.normal[1], planeData.normal[2]);
    const tlBoundPos = convertToVector3(planeData.worldPosArray[0]).clone().add(dirVec.clone().multiplyScalar(-1 * sliceThickness / 2));
    const brBoundPos = convertToVector3(planeData.worldPosArray[1]).clone().add(dirVec.clone().multiplyScalar(sliceThickness / 2));

    return [tlBoundPos.x, tlBoundPos.y, tlBoundPos.z, brBoundPos.x, brBoundPos.y, brBoundPos.z];

  }
  return null;
}

/**
 * Finds the slice position from the B Slice bound data in world(patient)
 * @param {Array} bound - bound position array
 * @param {Array} imageIds - image id collection
 * @returns {Array} slice position as array in world(patient)
 */
export const getSlicePosInWorldFromBound = (bound, imageIds) => {
  if (!imageIds || imageIds.length < 1) {
    return null;
  }
  const sliceThickness = calculateSliceThicknessFromPatientPos(imageIds);
  const imagePlane = cornerstone.metaData.get('imagePlaneModule', imageIds[0]);
  const row = vec3.create();
  const column = vec3.create();
  const normal = vec3.create();

  vec3.set(row, imagePlane.rowCosines[0], imagePlane.rowCosines[1], imagePlane.rowCosines[2]);
  vec3.set(column, imagePlane.columnCosines[0], imagePlane.columnCosines[1], imagePlane.columnCosines[2]);
  vec3.cross(normal, row, column);

  const dirVec = new cornerstoneMath.Vector3(normal[0], normal[1], normal[2]);
  const tlBoundPos = convertToVector3([bound[0], bound[1], bound[2]]);

  const imagePosition = tlBoundPos.clone().add(dirVec.clone().multiplyScalar(sliceThickness / 2));
  const worldPos = [imagePosition.x, imagePosition.y, imagePosition.z];

  const planeData = convertPlanesDataFromVtkjsOrientationTo2D([worldPos], imagePlane);
  if (!planeData?.worldPosArray?.length) {
    return worldPos;
  }

  return planeData.worldPosArray[0];
}

/**
 * Finds the B slice indicator from the clicked mouse position
 * @param {Array} indicatorsData - indicator data collection in display co-ordinate
 * @param {Object} callData - click event data from vtk js
 * @returns {number} hit b slice indicator index
 */
export const getClickedBSliceIndicatorIndex = (indicatorsData, callData) => {
  let indicatorIndex = -1;
  if (indicatorsData && indicatorsData.length > 0) {
    indicatorsData.forEach((indicator, index) => {
      if (indicator.Type === INDICATOR_TYPES.PLANE) {
        // Finds the clicked indicator index(taking the last matching item)
        if (getIndicatorHitStatus(indicator, callData)) {
          indicatorIndex = index;
        }
      }
    });
  }
  return indicatorIndex;
}

/**
 * Finds the slice thickness from the consecutive plane's position
 * @param {Array} imageIds - image id collection
 * @returns {number} slice thickness
 */
export const calculateSliceThicknessFromPatientPos = (imageIds) => {
  let sliceThickness = null;
  if (imageIds.length >= 2) {
    const firstImagePlane = cornerstone.metaData.get('imagePlaneModule', imageIds[0]);
    const secondImagePlane = cornerstone.metaData.get('imagePlaneModule', imageIds[1]);
    if (firstImagePlane && firstImagePlane.imagePositionPatient && secondImagePlane && secondImagePlane.imagePositionPatient) {
      const firstPos = vec3.create();
      const secondPos = vec3.create();

      vec3.set(firstPos, firstImagePlane.imagePositionPatient[0], firstImagePlane.imagePositionPatient[1], firstImagePlane.imagePositionPatient[2]);
      vec3.set(secondPos, secondImagePlane.imagePositionPatient[0], secondImagePlane.imagePositionPatient[1], secondImagePlane.imagePositionPatient[2]);

      sliceThickness = vec3.dist(firstPos, secondPos);
    }
  }
  if (!sliceThickness && imageIds.length > 0) {
    const imagePlane = cornerstone.metaData.get('imagePlaneModule', imageIds[0]);
    sliceThickness = imagePlane.sliceThickness || imagePlane.rowPixelSpacing || imagePlane.columnPixelSpacing;
  }
  return sliceThickness;
}
