import { vec2 } from 'gl-matrix';
import vtkCoordinate from 'vtk.js/Sources/Rendering/Core/Coordinate';
import { INDICATOR_TYPES } from '../constants.js';

/**
 * Check the event hits on the indicator or not
 * @param {Array} indicatorsData - indicator data collection in display co-ordinate
 * @param {Object} callData - click event data from vtk js
 * @returns {boolean} true if hit else false
 */
export const getIndicatorHitStatus = (indicatorsData, callData) => {
  const { position, pokedRenderer } = callData;
  let isHitOnIndicator = false;

  if (!indicatorsData.Points?.length || !indicatorsData.IsVisible) {
    return isHitOnIndicator;
  }

  const points = indicatorsData.Points;

  const dPos = vtkCoordinate.newInstance();
  dPos.setCoordinateSystemToDisplay();

  const cornerDPos1 = points[0];
  dPos.setValue(cornerDPos1[0], cornerDPos1[1], 0);
  const cornerLPos1 = dPos.getComputedLocalDisplayValue(pokedRenderer);

  const cornerDPos2 = points[1];
  dPos.setValue(cornerDPos2[0], cornerDPos2[1], 0);
  const cornerLPos2 = dPos.getComputedLocalDisplayValue(pokedRenderer);

  dPos.setValue(position.x, position.y, 0);
  const currentLPos = dPos.getComputedLocalDisplayValue(pokedRenderer);

  // Checks whether the event position is in the tolerance range of the indicator
  if (indicatorsData.Type === INDICATOR_TYPES.PLANE) {
    const tlDPos = [Math.min(cornerLPos1[0], cornerLPos2[0]), Math.min(cornerLPos1[1], cornerLPos2[1])];
    const brDPos = [Math.max(cornerLPos1[0], cornerLPos2[0]), Math.max(cornerLPos1[1], cornerLPos2[1])];

    // Check the event position is within the top left and bottom right bound of plane indicator
    if (tlDPos[0] <= currentLPos[0] && tlDPos[1] <= currentLPos[1] &&
      brDPos[0] >= currentLPos[0] && brDPos[1] >= currentLPos[1]) {
      isHitOnIndicator = true;
    }
  } else if (indicatorsData.Type === INDICATOR_TYPES.FOV) {
    const distanceFromCenter = vec2.distance(cornerLPos1, cornerLPos2);
    const distanceFromCenterToClicked = vec2.distance(cornerLPos1, currentLPos);
    if (indicatorsData.Shape === "circle") {
      if (Math.abs(distanceFromCenterToClicked - distanceFromCenter) < 5) {
        isHitOnIndicator = true;
      }
    } else if (indicatorsData.Shape === "crosshair") {
      if (distanceFromCenterToClicked <= distanceFromCenter) {
        isHitOnIndicator = true;
      }
    }
  }
  return isHitOnIndicator;
}

/**
 * Check if the given type indicator exist in the given collection of indicators
 * @param {Array} indicatorsData - indicator data collection
 * @param {string} type - type of indicator to find
 * @returns {boolean} true if at least one given type indicator found else false
 */
export const isIndicatorExist = (indicatorsData, type) => {
  if (indicatorsData && indicatorsData.length > 0) {
    return !!indicatorsData.find(indicator => indicator.Type === type);
  }
  return false;
}
