import { INDICATOR_TYPES } from '../constants.js';

// Class representing the indicator with default properties
export class IndicatorData {
  type = null;
  shape = null;
  color = null;
  isVisible = true;
  points = null;

  constructor(type, shape, color, isVisible = true, points = null) {
    this.type = type;
    this.shape = shape;
    this.color = color;
    this.isVisible = isVisible;
    this.points = points;
  }

  get Type() {
    return this.type;
  }

  get Shape() {
    return this.shape;
  }

  get Color() {
    return this.color;
  }

  set Color(color) {
    this.color = color;
  }

  get IsVisible() {
    return this.isVisible;
  }

  set IsVisible(isVisible) {
    this.isVisible = isVisible;
  }

  get Points() {
    return this.points;
  }

  set Points(points) {
    this.points = points;
  }
}

// Class representing the plane indicator with extended properties
export class PlaneIndicatorData extends IndicatorData {
  tlPos = null;
  brPos = null;
  planeNumber = -1;

  constructor(shape, color, isVisible, points) {
    super(INDICATOR_TYPES.PLANE, shape, color, isVisible, points);
  }

  get TopLeft() {
    return this.tlPos;
  }

  get BottomRight() {
    return this.brPos;
  }

  get PlaneNumber() {
    return this.planeNumber;
  }

  set TopLeft(tlPos) {
    this.tlPos = tlPos;
  }

  set BottomRight(brPos) {
    this.brPos = brPos;
  }

  set PlaneNumber(planeNumber) {
    this.planeNumber = planeNumber;
  }
}

// Class representing the fov indicator with extended properties
export class FovIndicatorData extends IndicatorData {
  centerPos = null;
  radiusPt = null;
  startPt = null;
  endPt = null;

  constructor(shape, color, isVisible, points) {
    super(INDICATOR_TYPES.FOV, shape, color, isVisible, points);
  }

  get CenterPos() {
    return this.centerPos;
  }

  get RadiusPoint() {
    return this.radiusPt;
  }

  get StartPoint() {
    return this.startPt;
  }

  get EndPoint() {
    return this.endPt;
  }

  set CenterPos(centerPos) {
    this.centerPos = centerPos;
  }

  set RadiusPoint(radiusPt) {
    this.radiusPt = radiusPt;
  }

  set StartPoint(startPt) {
    this.startPt = startPt;
  }

  set EndPoint(endPt) {
    this.endPt = endPt;
  }
}
