import csTools from 'cornerstone-tools';
const imagePointToPatientPoint = csTools.import(
  'util/imagePointToPatientPoint'
);

export const getFovealPointByAnnotation = (imagePlane, fovAnnotationPoint) => {
  const referencePoint = imagePointToPatientPoint({x: fovAnnotationPoint.x, y: fovAnnotationPoint.y}, imagePlane);
  return referencePoint;
}
