import { vec3 } from 'gl-matrix';
import vtkMatrixBuilder from 'vtk.js/Sources/Common/Core/MatrixBuilder';

/**
 * Finds the matching vtk js plugin displaySet from the viewports for the series
 * @param {Object} viewports
 * @returns {string} seriesInstanceUID
 */
export const getMatchingVTKViewportData = (viewports, seriesInstanceUID) => {
  let viewportDetails = null;
  viewports.layout.viewports.every((viewport, index) => {
    const viewportData = viewports.viewportSpecificData[index] || {};
    if (viewport.vtk && viewportData.SeriesInstanceUID === seriesInstanceUID) {
      viewportDetails = { index, displaySet: viewportData };
      return false;
    }
    return true;
  });
  return viewportDetails;
};

/**
 * Convert the planes position from DICOM orientation of image plane to the vtk js orientation
 * @param {Array} worldPosArray - collection of patient positions from plane
 * @param {Object} imagePlane - reference image plane metadata
 * @param {Array} targetNormal - target normal direction number array, if null find it from the image plane
 * @returns {Object} converted positions array and the target normal
 */
export const convertPlanesDataFrom2DToVtkjsOrientation = (worldPosArray, imagePlane, targetNormal = null) => {
  if (imagePlane) {
    const row = vec3.create();
    const column = vec3.create();

    const normal = vec3.create();
    const roundedNormal = vec3.create();

    vec3.set(row, imagePlane.rowCosines[0], imagePlane.rowCosines[1], imagePlane.rowCosines[2]);
    vec3.set(column, imagePlane.columnCosines[0], imagePlane.columnCosines[1], imagePlane.columnCosines[2]);

    vec3.cross(normal, row, column);

    if (targetNormal && targetNormal.length === 3) {
      vec3.set(roundedNormal, targetNormal[0], targetNormal[1], targetNormal[2]);
    } else {
      vec3.set(row, Math.round(imagePlane.rowCosines[0]), Math.round(imagePlane.rowCosines[1]), Math.round(imagePlane.rowCosines[2]));
      vec3.set(column, Math.round(imagePlane.columnCosines[0]), Math.round(imagePlane.columnCosines[1]), Math.round(imagePlane.columnCosines[2]));

      vec3.cross(roundedNormal, row, column);
    }

    const transform = vtkMatrixBuilder
      .buildFromDegree()
      .identity()
      .rotateFromDirections(normal, roundedNormal);

    const outputWorldPosArray = [];
    worldPosArray.forEach(worldPos => {
      let mutatedWorldPos = [worldPos[0], worldPos[1], worldPos[2]];
      transform.apply(mutatedWorldPos);
      outputWorldPosArray.push(mutatedWorldPos);
    });

    return { normal: roundedNormal, worldPosArray: outputWorldPosArray };
  }

  return null;
}

/**
 * Convert the planes position from vtk js orientation to the DICOM orientation of image plane
 * @param {Array} worldPosArray - collection of patient positions from vtk js
 * @param {Object} imagePlane - reference image plane metadata
 * @param {Array} targetNormal - sourceNormal normal direction number array of vtk js, if null find it from the image plane
 * @returns {Object} converted positions array and the target normal
 */
export const convertPlanesDataFromVtkjsOrientationTo2D = (worldPosArray, imagePlane, sourceNormal = null) => {
  if (imagePlane) {
    const row = vec3.create();
    const column = vec3.create();

    const normal = vec3.create();
    const roundedNormal = vec3.create();

    vec3.set(row, imagePlane.rowCosines[0], imagePlane.rowCosines[1], imagePlane.rowCosines[2]);
    vec3.set(column, imagePlane.columnCosines[0], imagePlane.columnCosines[1], imagePlane.columnCosines[2]);

    vec3.cross(normal, row, column);

    if (sourceNormal && sourceNormal.length === 3) {
      vec3.set(roundedNormal, sourceNormal[0], sourceNormal[1], sourceNormal[2]);
    } else {
      vec3.set(row, Math.round(imagePlane.rowCosines[0]), Math.round(imagePlane.rowCosines[1]), Math.round(imagePlane.rowCosines[2]));
      vec3.set(column, Math.round(imagePlane.columnCosines[0]), Math.round(imagePlane.columnCosines[1]), Math.round(imagePlane.columnCosines[2]));

      vec3.cross(roundedNormal, row, column);
    }

    const transform = vtkMatrixBuilder
      .buildFromDegree()
      .identity()
      .rotateFromDirections(roundedNormal, normal);

    const outputWorldPosArray = [];
    worldPosArray.forEach(worldPos => {
      let mutatedWorldPos = [worldPos[0], worldPos[1], worldPos[2]];
      transform.apply(mutatedWorldPos);
      outputWorldPosArray.push(mutatedWorldPos);
    });

    return { normal, worldPosArray: outputWorldPosArray };
  }

  return null;
}
