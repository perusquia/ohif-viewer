import store from '@ohif/viewer/src/store';
import { Redux as FlywheelRedux } from '@flywheel/extension-flywheel';
import { INTERACTOR_NAMES } from './constants.js';

const { selectProjectConfig } = FlywheelRedux.selectors;

export const isCrosshairStyle = (name, renderWindow = null) => {
  let interactorName = name;
  if (renderWindow) {
    interactorName = getInteractorClassName(renderWindow);
  }
  return (
    interactorName === INTERACTOR_NAMES.rotatableCrosshair ||
    interactorName === INTERACTOR_NAMES.crosshair
  );
};

export const getInteractorClassName = renderWindow => {
  return renderWindow
    .getInteractor()
    .getInteractorStyle()
    .getClassName();
};

export function getMouseButtonValue(button) {
  switch (button) {
    case 'left':
      return 1;
    case 'right':
      return 2;
    case 'middle':
      return 4;
    case 'wheel':
      return 4;
    default:
      return 0;
  }
}

function getDefaultMouseButtonForTool(tool) {
  switch (tool) {
    case 'range':
      return 4;
    case 'zoom':
      return 2;
    case 'pan':
      return 4;
    default:
      return 0;
  }
}

//Returns configured mouse buttons for specific tool
export function getConfiguredMouseActionButtons(toolName, leftBtnTool) {
  const toolButtons = [];
  const config = selectProjectConfig(store.getState());
  const mouseActions = config?.mouseActions;
  if (mouseActions) {
    const toolActions = mouseActions.filter(
      action =>
        getToolName(action.toolName, action.button) === toolName &&
        action.button !== 'left'
    );
    toolActions.forEach(action => {
      toolButtons.push(getMouseButtonValue(action.button));
    });
  } else {
    toolButtons.push(getDefaultMouseButtonForTool(toolName));
  }
  if (leftBtnTool === toolName) {
    toolButtons.push(1);
  }
  return toolButtons;
}

// Copy of finding the radius inside the vtk js.
export function getVolumeRadius(api) {
  let radius = 0;
  if (api) {
    let bounds = api.volumes[0].getBounds();
    if (!bounds) {
      return radius;
    }
    let w1 = bounds[1] - bounds[0];
    let w2 = bounds[3] - bounds[2];
    let w3 = bounds[5] - bounds[4];
    w1 *= w1;
    w2 *= w2;
    w3 *= w3;
    radius = w1 + w2 + w3;

    // If we have just a single point, pick a radius of 1.0
    radius = radius === 0 ? 1.0 : radius;

    // compute the radius of the enclosing sphere
    radius = Math.sqrt(radius) * 0.5;
  }

  return radius;
}

export function validateScale(radius, pScale) {
  // Validate the parallel scale to limit between minimum and maximum range configuration
  const projectConfig = selectProjectConfig(store.getState());
  const zoomLevel = projectConfig?.zoomLevel;

  const scaleConvertFactor = radius > 0 ? radius : 100;
  const defaultMinZoom = 0.25;
  const defaultMaxZoom = 20;
  const minPSale = zoomLevel
    ? zoomLevel.minimum * scaleConvertFactor
    : defaultMinZoom * scaleConvertFactor;
  const maxPSale = zoomLevel
    ? zoomLevel.maximum * scaleConvertFactor
    : defaultMaxZoom * scaleConvertFactor;
  if (pScale < minPSale || pScale > maxPSale) {
    pScale =
      pScale < minPSale ? minPSale : pScale > maxPSale ? maxPSale : pScale;
  }
  return pScale;
}

export function getToolName(toolName, button) {
  const defaultTool = 'range';
  if (
    toolName === 'Pan' ||
    toolName === 'Zoom' ||
    toolName === 'Rotate' ||
    toolName === 'StackScroll'||
    toolName === 'Wwwc'
  ) {
    return toolName.toLowerCase();
  } else {
    if (button === 'right') {
      return 'zoom';
    }
    if (button === 'middle') {
      return 'pan';
    }
  }
  return defaultTool;
}

export function getMouseButtonMask(button) {
  switch (button) {
    case 'left':
      return 1;
    case 'right':
      return 3;
    case 'middle':
      return 2;
    case 'wheel':
      return 2;
    default:
      return 0;
  }
}

export function getConfiguredMouseWheelAction() {
  let wheelActionButton = '';
  const config = selectProjectConfig(store.getState());
  const mouseActions = config?.mouseActions;
  if (mouseActions) {
    wheelActionButton = mouseActions
      .find(action => action.button === 'wheel')
      .toolName.toLowerCase();
  }
  return wheelActionButton;
}
