import macro from 'vtk.js/Sources/macro';
import { vtkInteractorStyleMPRWindowLevel } from 'react-vtkjs-viewport';
import Constants from 'vtk.js/Sources/Rendering/Core/InteractorStyle/Constants';
import vtkCompositeMouseManipulator from 'vtk.js/Sources/Interaction/Manipulators/CompositeMouseManipulator';
import { toLowHighRange } from '../../../utils/vtkJsInteractorSupport.js';
const { States } = Constants;

// ----------------------------------------------------------------------------
// Global methods
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// vtkWwwcManipulator methods
// ----------------------------------------------------------------------------

function vtkWwwcManipulator(publicAPI, model) {
  // Set our className
  model.classHierarchy.push('vtkWwwcManipulator');

  publicAPI.onButtonDown = (interactor, renderer, position) => {
    const iStyle = interactor.getInteractorStyle();
    model.volumeActor = iStyle.getVolumeActor();
    model.state = States.IS_NONE;
    publicAPI.startWindowLevel(interactor, iStyle);
    model.previousPosition = position;
  };

  publicAPI.onMouseMove = (interactor, renderer, position) => {
    const pos = [position.x, position.y];
    const iStyle = interactor.getInteractorStyle();
    publicAPI.windowLevelFromMouse(pos, renderer, iStyle);
  };

  publicAPI.windowLevelFromMouse = (pos, renderer, iStyle) => {
    const range = model.volumeActor
      .getMapper()
      .getInputData()
      .getPointData()
      .getScalars()
      .getRange();
    const imageDynamicRange = range[1] - range[0];
    const multiplier = (imageDynamicRange / 1024) * publicAPI.getLevelScale();
    const dx = (pos[0] - model.previousPosition.x) * multiplier;
    const dy = (pos[1] - model.previousPosition.y) * multiplier;
    model.previousPosition.x = pos[0];
    model.previousPosition.y = pos[1];

    if (!model.levels) {
      const { windowWidth, windowCenter } = publicAPI.getWindowLevel();
      model.levels = { windowWidth, windowCenter };
    }

    let windowWidth = model.levels.windowWidth + dx;
    let windowCenter = model.levels.windowCenter - dy;

    windowWidth = Math.max(0.01, windowWidth);

    if (
      model.windowWidth === windowWidth &&
      model.windowCenter === windowCenter
    ) {
      return;
    }

    publicAPI.setWindowLevel(windowWidth, windowCenter);

    model.wlStartPos[0] = Math.round(pos[0]);
    model.wlStartPos[1] = Math.round(pos[1]);
  };

  publicAPI.startWindowLevel = (interactor, iStyle) => {
    model.state = States.IS_WINDOW_LEVEL;
    interactor.requestAnimation(iStyle);
    iStyle.invokeStartInteractionEvent?.({ type: 'StartInteractionEvent' });
    iStyle.invokeStartWindowLevelEvent?.({ type: `StartWindowLevelEvent` });
  };

  publicAPI.getWindowLevel = () => {
    const range = model.volumeActor
      .getMapper()
      .getInputData()
      .getPointData()
      .getScalars()
      .getRange();
    return toWindowLevel(...range);
  };

  publicAPI.setWindowLevel = (windowWidth, windowCenter) => {
    const lowHigh = toLowHighRange(windowWidth, windowCenter);

    model.levels.windowWidth = windowWidth;
    model.levels.windowCenter = windowCenter;

    model.volumeActor
      .getProperty()
      .getRGBTransferFunction(0)
      .setMappingRange(lowHigh.lower, lowHigh.upper);
    publicAPI.getOnLevelsChanged({ windowCenter, windowWidth });
  };

  publicAPI.setApis = apis => {
    model.apis = apis;
  };

  publicAPI.getOnLevelsChanged = ({ windowCenter, windowWidth }) => {
    model.apis.forEach(api => {
      api.updateVOI(windowWidth, windowCenter);
    });
  };
}

function toWindowLevel(low, high) {
  const windowWidth = Math.abs(low - high);
  const windowCenter = low + windowWidth / 2;

  return { windowWidth, windowCenter };
}

// ----------------------------------------------------------------------------
// Object factory
// ----------------------------------------------------------------------------

const DEFAULT_VALUES = {
  wlStartPos: [0, 0],
  levelScale: 1,
};

// ----------------------------------------------------------------------------

export function extend(publicAPI, model, initialValues = {}) {
  Object.assign(model, DEFAULT_VALUES, initialValues);

  // Inheritance
  vtkInteractorStyleMPRWindowLevel.extend(publicAPI, model, initialValues);
  vtkCompositeMouseManipulator.extend(publicAPI, model, initialValues);

  macro.setGet(publicAPI, model, ['onLevelsChanged', 'levelScale']);

  // Object specific methods
  vtkWwwcManipulator(publicAPI, model);
}

// ----------------------------------------------------------------------------

export const newInstance = macro.newInstance(extend, 'vtkWwwcManipulator');

// ----------------------------------------------------------------------------

export default Object.assign({ newInstance, extend });
