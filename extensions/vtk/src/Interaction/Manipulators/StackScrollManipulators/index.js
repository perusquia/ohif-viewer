import macro from 'vtk.js/Sources/macro';
import vtkMouseCameraSliceManipulator from 'vtk.js/Sources/Interaction/Manipulators/MouseRangeManipulator';
import {
  moveCrosshairs,
  getPosition,
  getNewSlice,
} from '../../../utils/vtkJsInteractorSupport.js';
// ----------------------------------------------------------------------------
// vtkMouseCameraSliceManipulator methods
// ----------------------------------------------------------------------------

function vtkStackScrollManipulator(publicAPI, model) {
  // Set our className
  model.classHierarchy.push('vtkStackScrollManipulator');

  // Internal methods
  //-------------------------------------------------------------------------
  function scaleDeltaToRange(listener, normalizedDelta) {
    return (
      normalizedDelta * ((listener.max - listener.min) / (listener.step + 1))
    );
  }

  publicAPI.onMouseMove = (interactor, renderer, position) => {
    if (!position) {
      return;
    }

    const renderWindow = model?.api.genericRenderWindow.getRenderWindow();
    const istyle = renderWindow.getInteractor().getInteractorStyle();
    const range = istyle.getSliceRange();
    const step = Math.abs(range[1] - range[0]) < 10 ? 0.1 : 1;
    const dyNorm =
      (position.y - model.previousPosition.y) / model.containerSize[1];
    const listener = {
      min: range[0],
      max: range[1],
      step,
      getValue: istyle.getSlice,
    };
    const dy = scaleDeltaToRange(listener, dyNorm);
    const newSlice = getNewSlice(listener, dy);
    if (newSlice) {
      istyle.setSlice(newSlice);
    }

    if (model?.api?.svgWidgets?.rotatableCrosshairsWidget) {
      const pos = getPosition(model.api, renderer);
      const apiIndex = model.apis.findIndex(x => x.uid === model.api.uid);
      moveCrosshairs(model.apis, apiIndex, pos, renderer);
    }
    model.previousPosition = position;
  };

  publicAPI.setApiInstance = api => {
    model.api = api;
  };

  publicAPI.setApis = apis => {
    model.apis = apis;
  };
}

// ----------------------------------------------------------------------------
// Object factory
// ----------------------------------------------------------------------------

const DEFAULT_VALUES = {};

// ----------------------------------------------------------------------------

export function extend(publicAPI, model, initialValues = {}) {
  Object.assign(model, DEFAULT_VALUES, initialValues);

  // Inheritance
  macro.obj(publicAPI, model);
  vtkMouseCameraSliceManipulator.extend(publicAPI, model, initialValues);

  // Object specific methods
  vtkStackScrollManipulator(publicAPI, model);
}

// ----------------------------------------------------------------------------

export const newInstance = macro.newInstance(
  extend,
  'vtkStackScrollManipulator'
);

// ----------------------------------------------------------------------------

export default Object.assign({ newInstance, extend });
