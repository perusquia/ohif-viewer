import macro from 'vtk.js/Sources/macro';
import { vtkInteractorStyleMPRCrosshairs } from 'react-vtkjs-viewport';
import { INDICATOR_TYPES } from './utils/constants.js';
import { isIndicatorExist } from './utils/Indicator/IndicatorUtils.js';

// ----------------------------------------------------------------------------
// Global methods
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// vtkInteractorStyleMPRFovIndicators methods
// ----------------------------------------------------------------------------

function vtkInteractorStyleMPRFovIndicators(publicAPI, model) {
  // Set our className
  model.classHierarchy.push('vtkInteractorStyleMPRFovIndicators');

  const superHandleLeftButtonPress = publicAPI.handleLeftButtonPress;
  publicAPI.handleLeftButtonPress = callData => {
    if (!callData.shiftKey && !callData.controlKey) {
      if (model.volumeActor) {
        const { apis, apiIndex } = model;
        const api = apis[apiIndex];
        const indicatorsDataInWorld = api.svgWidgets.crosshairsWidget.getIndicatorsDataInWorld();
        // Skip default crosshair movement when fov indicator is visible
        if (!isIndicatorExist(indicatorsDataInWorld, INDICATOR_TYPES.FOV) && superHandleLeftButtonPress) {
          superHandleLeftButtonPress(callData);
        }
      }
    } else if (superHandleLeftButtonPress) {
      superHandleLeftButtonPress(callData);
    }
  };
}

// ----------------------------------------------------------------------------
// Object factory
// ----------------------------------------------------------------------------

const DEFAULT_VALUES = {
  isCenterClicked: false,
};

// ----------------------------------------------------------------------------

export function extend(publicAPI, model, initialValues = {}) {
  Object.assign(model, DEFAULT_VALUES, initialValues);

  // Inheritance
  vtkInteractorStyleMPRCrosshairs.extend(publicAPI, model, initialValues);

  macro.setGet(publicAPI, model, ['isCenterClicked']);

  // Object specific methods
  vtkInteractorStyleMPRFovIndicators(publicAPI, model);
}

// ----------------------------------------------------------------------------

export const newInstance = macro.newInstance(
  extend,
  'vtkInteractorStyleMPRFovIndicators'
);

// ----------------------------------------------------------------------------

export default Object.assign({ newInstance, extend });
