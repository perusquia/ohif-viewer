import macro from 'vtk.js/Sources/macro';
import { vec2 } from 'gl-matrix';
import vtkCoordinate from 'vtk.js/Sources/Rendering/Core/Coordinate';
import { vtkSVGCrosshairsWidget } from 'react-vtkjs-viewport';
import { INDICATOR_TYPES } from './utils/constants.js';
import { IndicatorData } from './utils/Indicator/IndicatorData.js';

function getWidgetNode(svgContainer, widgetId) {
  let node = svgContainer.querySelector(`#${widgetId}`);
  if (!node) {
    node = document.createElement('g');
    node.setAttribute('id', widgetId);
    svgContainer.appendChild(node);
  }
  return node;
}

function getCrosshairElements(model, width, height, scale) {
  const { point, strokeColor, strokeWidth, strokeDashArray, padding,
    isFovRepositionMode } = model;

  let innerHtml = ``;
  if (point[0] === null || point[1] === null) {
    return innerHtml;
  }
  const p = point.slice();
  p[0] = point[0] * scale;
  p[1] = height - point[1] * scale;

  const left = [0, height / scale / 2];
  const top = [width / scale / 2, 0];
  const right = [width / scale, height / scale / 2];
  const bottom = [width / scale / 2, height / scale];
  const strokeOpacity = isFovRepositionMode ? 0.2 : 1;

  innerHtml += `
      <!-- Top !-->
       <line
         x1="${p[0]}"
         y1="${top[1]}"
         x2="${p[0]}"
         y2="${p[1] - padding}"
         stroke="${strokeColor}"
         stroke-dasharray="${strokeDashArray}"
         stroke-linecap="round"
         stroke-linejoin="round"
         stroke-width="${strokeWidth}"
         stroke-opacity="${strokeOpacity}"
       ></line>
       <!-- Right !-->
       <line
         x1="${right[0]}"
         y1="${p[1]}"
         x2="${p[0] + padding}"
         y2="${p[1]}"
         stroke-dasharray="${strokeDashArray}"
         stroke="${strokeColor}"
         stroke-linecap="round"
         stroke-linejoin="round"
         stroke-width=${strokeWidth}
         stroke-opacity="${strokeOpacity}"
       ></line>
       <!-- Bottom !-->
       <line
         x1="${p[0]}"
         y1="${bottom[1]}"
         x2="${p[0]}"
         y2="${p[1] + padding}"
         stroke-dasharray="${strokeDashArray}"
         stroke="${strokeColor}"
         stroke-linecap="round"
         stroke-linejoin="round"
         stroke-width=${strokeWidth}
         stroke-opacity="${strokeOpacity}"
       ></line>
       <!-- Left !-->
       <line
         x1="${left[0]}"
         y1="${p[1]}"
         x2="${p[0] - padding}"
         y2="${p[1]}"
         stroke-dasharray="${strokeDashArray}"
         stroke="${strokeColor}"
         stroke-linecap="round"
         stroke-linejoin="round"
         stroke-width=${strokeWidth}
         stroke-opacity="${strokeOpacity}"
       ></line>
          `;
  return innerHtml;
}

function getIndicatorElements(model, width, height, scale) {
  const { indicators, strokeColor, strokeWidth, strokeDashArray, padding,
    isFovRepositionMode, focussedIndicatorIndex } = model;

  let innerHtml = ``;
  if (!indicators || !indicators.length) {
    return innerHtml;
  }

  indicators.forEach((indicator, index) => {
    if (!indicator.IsVisible) {
      return;
    }
    const points = [];
    // Adjust the points to the svg widget co-ordinates
    (indicator.Points || []).forEach(point => {
      points.push([point[0] / scale, height - point[1] * scale]);
    })
    const indicatorColor = (focussedIndicatorIndex === index) ? "blue" : indicator.Color || strokeColor;
    let strokeOpacity = indicator.Shape === "crosshair" ? 1 : isFovRepositionMode ? 0.2 : 1;

    if (indicator.Shape === "rectangle" && (indicator.Points?.length > 1)) {
      const p1 = points[0];
      const p2 = points[1];

      const rectLines = "M" + p1[0] + "," + p1[1] + " L" +
        p2[0] + "," + p1[1] + " L" + p2[0] + "," + p2[1] +
        " L" + p1[0] + "," + p2[1] + " L" + p1[0] + "," + p1[1];

      innerHtml += `
        <path d="${rectLines}" fill="none" stroke="${indicatorColor}"
          stroke-dasharray="${strokeDashArray}"
          stroke-linecap="round"
          stroke-linejoin="round"
          stroke-width="${strokeWidth}"
          stroke-opacity="${strokeOpacity}" />`;
    } else if ((indicator.Shape === "circle" || indicator.Shape === "crosshair") && indicator.Points) {
      const strokeSize = (indicator.Shape === "crosshair") ? strokeWidth : strokeWidth * 2;
      const p1 = points[0];
      const p2 = points[1];

      const distanceFromCenter = vec2.distance(p1, p2);
      const radius = (indicator.Shape === "crosshair") ? distanceFromCenter / 2 : distanceFromCenter;

      innerHtml += `
        <circle cx="${p1[0]}" cy="${p1[1]}" r="${radius}"
          stroke="${indicatorColor}"
          stroke-dasharray="${strokeDashArray}"
          stroke-linecap="round"
          stroke-linejoin="round"
          stroke-width="${strokeSize}"
          fill-opacity="0"
          stroke-opacity="${strokeOpacity}"/>`;

      // Drawing crosshair lines along with the circle
      if (indicator.Shape === "crosshair") {
        const hLine = "M" + (p1[0] - distanceFromCenter) + "," + p1[1] + " h" + (distanceFromCenter * 2);
        const vLine = "M" + p1[0] + "," + (p1[1] - distanceFromCenter) + " v" + (distanceFromCenter * 2);
        innerHtml += `
          <path d="${hLine + vLine}" fill="none" stroke="${indicatorColor}" stroke-width="${strokeSize}"
          stroke-opacity="${strokeOpacity}"/>`
      }
    } else if (indicator.Shape === "diagonal") {
      const strokeSize = strokeWidth * 2;

      const diag1Start = points[0];
      const diag1End = points[1];
      const diag2Start = points[2];
      const diag2End = points[3];
      const diag3Start = points[4];
      const diag3End = points[5];
      const diag4Start = points[6];
      const diag4End = points[7];

      const diagLines = "M" + diag1Start[0] + "," + diag1Start[1] + " L" +
        diag1End[0] + "," + diag1End[1] + " M" + diag2Start[0] + "," + diag2Start[1] + " L" +
        diag2End[0] + "," + diag2End[1] + " M" + diag3Start[0] + "," + diag3Start[1] + " L" +
        diag3End[0] + "," + diag3End[1] + " M" + diag4Start[0] + "," + diag4Start[1] + " L" +
        diag4End[0] + "," + diag4End[1];

      innerHtml += `
        <path d="${diagLines}" fill="none" stroke="${indicatorColor}"
          stroke-dasharray="${strokeDashArray}"
          stroke-linecap="round"
          stroke-linejoin="round"
          stroke-width="${strokeSize}"
          stroke-opacity="${strokeOpacity}" />`;
    }
  });
  return innerHtml;
}

// ----------------------------------------------------------------------------

function vtkSVGCrosshairIndicatorWidget(publicAPI, model) {

  publicAPI.render = (svgContainer, scale) => {
    const node = getWidgetNode(svgContainer, model.widgetId);

    const width = parseInt(svgContainer.getAttribute('width'), 10);
    const height = parseInt(svgContainer.getAttribute('height'), 10);
    // Unused
    // const widthScale = svgContainer.getBoundingClientRect().width / width;
    // const heightScale = svgContainer.getBoundingClientRect().height / height;
    // const widthClient = svgContainer.getBoundingClientRect().width;
    // const heightClient = svgContainer.getBoundingClientRect().height;

    if (model.display || model.displayIndicators) {
      node.innerHTML = `
      <g id="container" fill-opacity="1" stroke-dasharray="none" stroke="none" stroke-opacity="1" fill="none">
       <g>
       <!-- TODO: Why is this <svg> necessary?? </svg> If I don't include it, nothing renders !-->
       <svg version="1.1" viewBox="0 0 ${width} ${height}" width=${width} height=${height} style="width: 100%; height: 100%">
       ${model.displayIndicators && getIndicatorElements(model, width, height, scale)}
       ${model.display && getCrosshairElements(model, width, height, scale)}
       </g>
      </g>
            `;
    } else {
      node.innerHTML = '';
    }
  };

  publicAPI.adjustIndicators = (apis) => {
    apis.forEach(api => {
      const renderer = api.genericRenderWindow.getRenderer();
      const iStyle = api.genericRenderWindow.getRenderWindow().getInteractor().getInteractorStyle();
      const svgWidget = api.svgWidgets.crosshairsWidget;
      const camera = renderer.getActiveCamera();
      const directionOfProjection = camera.getDirectionOfProjection();
      const sliceRange = iStyle.getSliceRange()
      const slice = iStyle.getSlice();
      const delta = slice - sliceRange[0];
      const axisIndex = directionOfProjection.findIndex(val => val !== 0);
      // This is workaround to find the slice position matching to the volume bound range co-ordinate
      // when the image is positioned in -ve axis(specifically for sagittal orientation)
      const sliceFromVolume = api.volumes[0].getBounds()[axisIndex * 2] + delta;
      const indicatorsDataInWorld = svgWidget.getIndicatorsDataInWorld();
      const indicators = [];
      if (indicatorsDataInWorld && indicatorsDataInWorld.length > 0) {
        indicatorsDataInWorld.forEach(indicatorInWorld => {
          let isVisible = indicatorInWorld.Isvisible;
          const points = [];

          if (indicatorInWorld.Type === INDICATOR_TYPES.PLANE && indicatorInWorld.Shape === "rectangle") {
            const topLeft = indicatorInWorld.TopLeft;
            const bottomRight = indicatorInWorld.BottomRight;
            const minPos = Math.min(topLeft[axisIndex], bottomRight[axisIndex]);
            const maxPos = Math.max(topLeft[axisIndex], bottomRight[axisIndex]);

            isVisible = ((sliceFromVolume <= maxPos) && (sliceFromVolume >= minPos)) ? true : false;

            const dPos = vtkCoordinate.newInstance();
            dPos.setCoordinateSystemToWorld();

            dPos.setValue(topLeft[0], topLeft[1], topLeft[2]);
            const tlDisplayPos = dPos.getComputedDisplayValue(renderer);

            dPos.setValue(bottomRight[0], bottomRight[1], bottomRight[2]);
            const brDisplayPos = dPos.getComputedDisplayValue(renderer);

            points.push([tlDisplayPos[0], tlDisplayPos[1]]);
            points.push([brDisplayPos[0], brDisplayPos[1]]);
          } else if (indicatorInWorld.Type === INDICATOR_TYPES.FOV) {
            if (indicatorInWorld.Shape === "circle") {
              const dPos = vtkCoordinate.newInstance();
              dPos.setCoordinateSystemToWorld();

              dPos.setValue(indicatorInWorld.CenterPos[0], indicatorInWorld.CenterPos[1], indicatorInWorld.CenterPos[2]);
              const centerDisplayPos = dPos.getComputedDisplayValue(renderer);

              const radiusPos = indicatorInWorld.RadiusPoint;
              dPos.setValue(radiusPos[0], radiusPos[1], radiusPos[2]);
              const radDisplayPos = dPos.getComputedDisplayValue(renderer);

              points.push(centerDisplayPos);
              points.push(radDisplayPos);
            } else if (indicatorInWorld.Shape === "crosshair") {
              const dPos = vtkCoordinate.newInstance();
              dPos.setCoordinateSystemToWorld();

              dPos.setValue(indicatorInWorld.CenterPos[0], indicatorInWorld.CenterPos[1], indicatorInWorld.CenterPos[2]);
              const centerDisplayPos = dPos.getComputedDisplayValue(renderer);

              const radDisplayPos = vec2.create();
              vec2.add(radDisplayPos, centerDisplayPos, vec2.fromValues(12, 0));

              points.push(centerDisplayPos);
              points.push(radDisplayPos);
            } else if (indicatorInWorld.Shape === "diagonal") {
              const dPos = vtkCoordinate.newInstance();
              dPos.setCoordinateSystemToWorld();

              dPos.setValue(indicatorInWorld.CenterPos[0], indicatorInWorld.CenterPos[1], indicatorInWorld.CenterPos[2]);
              let centerDisplayPos = dPos.getComputedDisplayValue(renderer);

              dPos.setValue(indicatorInWorld.StartPoint[0], indicatorInWorld.StartPoint[1], indicatorInWorld.StartPoint[2]);
              let startDisplayPos = dPos.getComputedDisplayValue(renderer);

              dPos.setValue(indicatorInWorld.EndPoint[0], indicatorInWorld.EndPoint[1], indicatorInWorld.EndPoint[2]);
              let endDisplayPos = dPos.getComputedDisplayValue(renderer);

              const diag1Start = vec2.create();
              const diag1End = vec2.create();
              const diag2Start = vec2.create();
              const diag2End = vec2.create();
              const diag3Start = vec2.create();
              const diag3End = vec2.create();
              const diag4Start = vec2.create();
              const diag4End = vec2.create();

              const DEG_TO_RAD = Math.PI / 180;

              // Find the diagonal lines by rotating the horizontal line points
              vec2.rotate(diag1Start, startDisplayPos, centerDisplayPos, 45 * DEG_TO_RAD);
              vec2.rotate(diag1End, endDisplayPos, centerDisplayPos, 45 * DEG_TO_RAD);

              vec2.rotate(diag2Start, startDisplayPos, centerDisplayPos, 225 * DEG_TO_RAD);
              vec2.rotate(diag2End, endDisplayPos, centerDisplayPos, 225 * DEG_TO_RAD);

              vec2.rotate(diag3Start, startDisplayPos, centerDisplayPos, -45 * DEG_TO_RAD);
              vec2.rotate(diag3End, endDisplayPos, centerDisplayPos, -45 * DEG_TO_RAD);

              vec2.rotate(diag4Start, startDisplayPos, centerDisplayPos, 135 * DEG_TO_RAD);
              vec2.rotate(diag4End, endDisplayPos, centerDisplayPos, 135 * DEG_TO_RAD);

              points.push(diag1Start);
              points.push(diag1End);
              points.push(diag2Start);
              points.push(diag2End);
              points.push(diag3Start);
              points.push(diag3End);
              points.push(diag4Start);
              points.push(diag4End);
            }
          }
          indicators.push(new IndicatorData(indicatorInWorld.Type, indicatorInWorld.Shape, indicatorInWorld.Color,
            isVisible, points));
        });
      }
      svgWidget.setIndicators(indicators);
      api.svgWidgetManager.render();
    });
  };
}

// ----------------------------------------------------------------------------

const DEFAULT_VALUES = {
  point: [null, null],
  strokeColor: '#00ff00',
  strokeWidth: 1,
  strokeDashArray: '',
  padding: 20,
  display: true,
  indicators: [],
  indicatorsDataInWorld: [],
  displayIndicators: true,
  focussedIndicatorIndex: -1,
  isFovRepositionMode: false,
};

// ----------------------------------------------------------------------------

export function extend(publicAPI, model, initialValues = {}) {
  Object.assign(model, DEFAULT_VALUES, initialValues);

  vtkSVGCrosshairsWidget.extend(publicAPI, model, initialValues);

  macro.setGet(publicAPI, model, ['indicatorsDataInWorld', 'indicators', 'displayIndicators', 'focussedIndicatorIndex', 'isFovRepositionMode']);

  vtkSVGCrosshairIndicatorWidget(publicAPI, model);
}

// ----------------------------------------------------------------------------

export const newInstance = macro.newInstance(
  extend,
  'vtkSVGCrosshairIndicatorWidget'
);

// ----------------------------------------------------------------------------

export default Object.assign({ newInstance, extend });
