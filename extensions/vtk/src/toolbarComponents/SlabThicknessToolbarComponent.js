import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Range, Checkbox, OldSelect } from '@ohif/ui';

import './slab-thickness-toolbar-button.styl';
import './SlabThicknessManualInput.css';

const SLIDER = {
  MIN: 0.1,
  MAX: 1000,
  STEP: 0.1,
};

const ToolbarLabel = props => {
  const { label } = props;
  return <div className="toolbar-button-label">{label}</div>;
};

ToolbarLabel.propTypes = {
  label: PropTypes.string.isRequired,
};

const ToolbarSlider = props => {
  const {
    value,
    min,
    max,
    onChange,
    changeThickness,
    onDismiss,
    slabTextActive,
    onClickValue,
  } = props;

  const onKeyPress = (txt, evt) => {
    const charCode = evt.which ? evt.which : evt.keyCode;
    const enterKey = 13,
      dotKey = 46,
      limitCharCode0 = 48,
      limitCharCode9 = 57;
    if (charCode === enterKey) {
      onDismiss();
    }
    if (charCode === dotKey) {
      if (txt.indexOf('.') !== -1 || parseInt(txt) === SLIDER.MAX) {
        evt.preventDefault();
      }
    } else {
      if (charCode < limitCharCode0 || charCode > limitCharCode9) {
        evt.preventDefault();
      }
    }
  };

  const onPaste = e => {
    const value = e.clipboardData.getData('Text');
    if (isNaN(parseFloat(value)) || !value.match(/^[-+]?[0-9]+\.[0-9]+$/)) {
      e.preventDefault();
    }
  };

  const className = slabTextActive
    ? 'slab-thickness-input'
    : 'slab-thickness-value';
  return (
    <div className="toolbar-slider-container">
      <label htmlFor="toolbar-slider">
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <div onClick={onClickValue}>
            <input
              type="text"
              className={className}
              value={value}
              onChange={changeThickness}
              onKeyPress={e => onKeyPress(e.target.value, e)}
              onPaste={onPaste}
              id="toolbar-slider-text"
              onBlur={onDismiss}
              autoComplete="off"
            />
          </div>
          <span style={{ marginLeft: '2px' }}>mm</span>
        </div>
      </label>
      <Range
        value={Number(value) || Math.max(SLIDER.MIN, min)}
        min={min}
        max={max}
        step={SLIDER.STEP}
        onChange={onChange}
        id="toolbar-slider"
      />
    </div>
  );
};

ToolbarSlider.propTypes = {
  value: PropTypes.string.isRequired,
  slabTextActive: PropTypes.bool.isRequired,
  min: PropTypes.number.isRequired,
  max: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
  changeThickness: PropTypes.func.isRequired,
  onDismiss: PropTypes.func.isRequired,
  onClickValue: PropTypes.func.isRequired,
};

const _getSelectOptions = button => {
  return button.operationButtons.map(button => {
    return {
      key: button.label,
      value: button.id,
    };
  });
};

const _getClassNames = (isActive, className) => {
  return classnames('toolbar-button', 'slab-thickness', className, {
    active: isActive,
  });
};

const _applySlabThickness = (
  value,
  modeChecked,
  toolbarClickCallback,
  button
) => {
  if (!modeChecked || !toolbarClickCallback) {
    return;
  }

  const { actionButton } = button;

  const generateOperation = (operation, value) => {
    // Combine slider value into slider operation
    const generatedOperation = { ...operation };
    generatedOperation.commandOptions = {
      ...operation.commandOptions,
      slabThickness: value,
    };

    return generatedOperation;
  };

  const operation = generateOperation(actionButton, value);
  toolbarClickCallback(operation, event);
};

const _applyModeOperation = (
  operation,
  modeChecked,
  toolbarClickCallback,
  button
) => {
  // in case modeChecked has not being triggered by user yet
  if (typeof modeChecked !== 'boolean') {
    return;
  }

  const { deactivateButton } = button;

  const _operation = modeChecked ? operation : deactivateButton;
  if (toolbarClickCallback && _operation) {
    toolbarClickCallback(_operation);
  }
};

const _getInitialState = currentSelectedOption => {
  return {
    value: SLIDER.MIN,
    sliderMin: SLIDER.MIN,
    sliderMax: SLIDER.MAX,
    modeChecked: undefined,
    operation: currentSelectedOption,
  };
};

const INITIAL_OPTION_INDEX = 0;
const _getInitialtSelectedOption = (button = {}) => {
  return (
    button.operationButtons && button.operationButtons[INITIAL_OPTION_INDEX]
  );
};

function SlabThicknessToolbarComponent({
  parentContext,
  toolbarClickCallback,
  button,
  activeButtons,
  isActive,
  className,
}) {
  const currentSelectedOption = _getInitialtSelectedOption(button);
  const [state, setState] = useState(_getInitialState(currentSelectedOption));
  const [slabTextActive, setSlabTextActive] = useState(false);
  const { label, operationButtons } = button;
  const _className = _getClassNames(isActive, className);
  const selectOptions = _getSelectOptions(button);
  function onChangeSelect(selectedValue) {
    // find select value
    const operation = operationButtons.find(
      button => button.id === selectedValue
    );

    if (operation === state.operation) {
      return;
    }

    setState({ ...state, operation });
  }

  function onChangeCheckbox(checked) {
    setState({ ...state, modeChecked: checked });
  }

  function onChangeSlider(event) {
    const value = Number(event.target.value);

    if (value !== state.value) {
      setState({ ...state, value, modeChecked: true });
    }
  }

  function onChangeSlabThickness(event) {
    let val = event.target.value;
    const maxDigit = 6;
    const maxDecimalPoints = 2;
    if (parseFloat(val) > SLIDER.MAX || val.length > maxDigit) {
      event.preventDefault();
      return;
    } else if (
      val.split('.')[1] &&
      val.split('.')[1].length > maxDecimalPoints
    ) {
      val = parseFloat(val).toFixed(2);
    }
    if (val !== state.value) {
      setState({ ...state, value: val, modeChecked: true });
    }
  }

  function changeInputToText() {
    setSlabTextActive(false);
    if (!Number(state.value)) {
      setState({
        ...state,
        value: SLIDER.MIN,
      });
    } else {
      setState({
        ...state,
        value: Number(state.value),
      });
      document.getElementById('toolbar-slider').focus();
    }
  }

  function changeTextToInput() {
    if (!slabTextActive) {
      setSlabTextActive(true);
    }
  }

  useEffect(() => {
    _applyModeOperation(
      state.operation,
      state.modeChecked,
      toolbarClickCallback,
      button
    );
  }, [state.modeChecked, state.operation]);

  useEffect(() => {
    _applySlabThickness(
      Number(state.value) || SLIDER.MIN,
      state.modeChecked,
      toolbarClickCallback,
      button
    );
  }, [state.operation, state.modeChecked, state.value]);

  return (
    <>
      <div className={_className}>
        <div className="container">
          <ToolbarSlider
            value={state.value.toString()}
            min={state.sliderMin}
            max={state.sliderMax}
            onChange={onChangeSlider}
            changeThickness={onChangeSlabThickness}
            slabTextActive={slabTextActive}
            onClickValue={() => changeTextToInput()}
            onDismiss={changeInputToText}
          />
          <ToolbarLabel key="toolbar-label" label={label} />
        </div>

        <div className="controller">
          <Checkbox
            label="mode"
            checked={state.modeChecked}
            onChange={onChangeCheckbox}
          ></Checkbox>
          <OldSelect
            key="toolbar-select"
            options={selectOptions}
            value={selectOptions[INITIAL_OPTION_INDEX].value}
            onChange={onChangeSelect}
          ></OldSelect>
        </div>
      </div>
    </>
  );
}

SlabThicknessToolbarComponent.propTypes = {
  parentContext: PropTypes.object.isRequired,
  toolbarClickCallback: PropTypes.func.isRequired,
  button: PropTypes.object.isRequired,
  activeButtons: PropTypes.array.isRequired,
  isActive: PropTypes.bool,
  className: PropTypes.string,
};

export default SlabThicknessToolbarComponent;
