import OHIF from '@ohif/core';
import redux from '../redux';
const { setActiveProtocolStore } = redux.actions;
const { ProtocolEngineUtils } = OHIF.hangingProtocols;

/**
 * Reset the current layout to HP layout if not already HP layout and then
 * invoke the callback with the status of checking is current layout is HP or not.
 * @param {Object} commandsManager
 * @param {Object} store
 * @param {Object} protocolStore
 * @param {Object} callBack - callback to invoked the current layout matching with HP status
 * @return {void}
 */
const resetLayoutToHP = (
  commandsManager,
  store,
  protocolStore,
  callBack = null
) => {
  const state = store.getState();
  ProtocolEngineUtils.isCurrentLayoutMatchingWithHP(
    state,
    protocolStore.protocolStore,
    isMatching => {
      if (!isMatching) {
        commandsManager?.runCommand('setCornerstoneLayout');
        // Put the protocol store entry update with a slight delay as the layout update to clear
        // the protocol layout in store is asynchronous.
        setTimeout(() => {
          store.dispatch(
            setActiveProtocolStore(
              protocolStore.projectId,
              protocolStore.protocolStore,
              true
            )
          );
        }, 10);
      }
      if (callBack) {
        callBack(isMatching);
      }
    }
  );
};

export default resetLayoutToHP;
