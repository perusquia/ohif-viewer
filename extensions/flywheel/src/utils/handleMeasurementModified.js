import { importInternal } from 'cornerstone-tools';
import _ from 'lodash';
const csToolsThrottle = importInternal('util/throttle');

/**
 * It defers invoking handler param (method) in case measurementData has changed.
 * There is a cornerstoneTools side effect, which silent update the measurementData (updateCacheStats).
 * To prevent unwanted behavior this method ensures handler is called at least one more time once after updatedCacheStats is triggered.
 *
 * This method can be removed once cornerstoneTools fires events for silent updating such as updateCacheStats
 * @param {Event} event Source event for on modified measurement.
 * @param {Function} handler callback method to be called once more.
 */
const _deferOnMeasurementModified = (event, handler) => {
  // make a local copy. Used to compare and reduce unnecessary update
  const measurementDataTemp = _.cloneDeep(event.detail.measurementData);

  if (measurementDataTemp && handler) {
    /**
     * It caches targed method (with name targetMethodName) into a new object property. It copies target method into a different property.
     * @param {Object} toolInstance Tool instance
     * @param {String} targetMethodName Target Method name to suffer injection
     * @param {String} cachedMethodName Target cache method name which is a copy of original method
     */
    const cacheTargetMethod = (
      toolInstance,
      targetMethodName,
      cachedMethodName
    ) => {
      const originalCachedMethod = _.get(toolInstance, cachedMethodName);

      // if not cached yet and target exists
      if (!originalCachedMethod && toolInstance[targetMethodName]) {
        _.set(toolInstance, cachedMethodName, toolInstance[targetMethodName]);
      }
    };

    /**
     * It gets current tool instance for given event
     * @param {Event} event Event object fired by tool modified
     */
    const getToolInstance = event => {
      try {
        const enabledElement = cornerstone.getEnabledElement(
          event.detail.element
        );
        const toolName = event.detail.toolName;

        const toolInstance = cornerstoneTools.getToolForElement(
          enabledElement.element,
          toolName
        );

        return toolInstance;
      } catch (e) {}
    };

    /**
     * It changes target method (with name targetMethodName) defined by cornerstoneTools.
     * It injects and ensures afterHandler method is called whenever target method is called.
     * It also considers changing throttleTargetMethod if existing (this is necessary because, usually, throttle methods are bound to tool instance during object build time)
     * Ex: It ensures, for instance, afterHandler is called just after tool instance updates cached value (silently).
     *
     * @param {Object} toolInstance Tool instance from cornerstoneTools
     * @param {String} targetMethodName Target method name to suffer adaption
     * @param {String} cachedMethodName Method name which contains the original method (equivalent to targetMethod before adaption).
     * @param {String} throttleTargetMethod Throttle method name in case it is used to schedule a call for target method (named as targetMethodName)
     * @param {Function} afterHandler Method to be called after target method (named as targetMethodName) is called.
     */
    const scheduleAfter = (
      toolInstance,
      targetMethodName,
      cachedMethodName,
      throttleTargetMethod,
      afterHandler
    ) => {
      // cut in case there is no target method
      if (!toolInstance[targetMethodName]) {
        return;
      }

      // adapt target method to call original method and then afterHandler method
      toolInstance[targetMethodName] = _.bind((image, element, data) => {
        // originalMethod is cached into cachedMethodName. Use it.
        toolInstance[cachedMethodName].call(toolInstance, image, element, data);
        // do the call only if measurement data has changed
        // IMPORTANT:: cornerstoneTools can potentially change by ref event.detail.measurementData after calling to update data
        if (!_.isEqual(measurementDataTemp, event.detail.measurementData)) {
          afterHandler(event);
        }
      });

      if (toolInstance[throttleTargetMethod]) {
        // ensure throttledUpdateCachedStats method uses adapted target (reassign it)
        // assuming original throttle method has used csToolsThrottle we should also use it.
        // this is required because cornerstone bound targetMethodName to throttle method during object build time, then later, we lose actual target method bound.
        toolInstance[throttleTargetMethod] = csToolsThrottle(
          toolInstance[targetMethodName],
          110
        );
      }
    };

    const toolInstance = getToolInstance(event);

    if (toolInstance) {
      // preparing tool instance to call handler method always after when updateCachedStats is called.
      const targetMethodName = 'updateCachedStats';
      const cachedMethodName = `_${targetMethodName}`;
      const throttledMethodName = 'throttledUpdateCachedStats';

      cacheTargetMethod(toolInstance, targetMethodName, cachedMethodName);
      scheduleAfter(
        toolInstance,
        targetMethodName,
        cachedMethodName,
        throttledMethodName,
        handler
      );
    }
  }
};
/**
 * It handles on measurement modified.
 * It uses throttle strategy to invoke onModifiedCallback.
 * To prevent cornerstoneTools updating side effects (ex. updating cached length), this method schedule a last chance call for onModifiedCallback with the most updated tool data.
 * @param {Function} onModifiedCallback callback method when throttle is satisfied and stack is cleared.
 * @param {Number} time Throttle time
 * @return {Function} Throttle callback
 */
const onModifiedMeasurement = (onModifiedCallback, time) => {
  // apply throttle and schedule a last chance onModifiedCallback call.
  return _.throttle(event => {
    _deferOnMeasurementModified(event, onModifiedCallback);

    return onModifiedCallback(event);
  }, time);
};

export default onModifiedMeasurement;
