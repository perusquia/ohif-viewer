import _ from 'lodash';

/**
 * It returns UIDProperty of given imageId. It uses cornerstone metadata provider to get given property value.
 *
 * @param  {String} imageId The imageId for which we're looking for the study.
 * @param  {String} UIDProperty UID property name
 * @return {string}  UIDProperty value if existing
 */
const _getUIDValue = (imageId, UIDProperty) => {
  try {
    const instance = cornerstone.metaData.get('instance', imageId);
    const UIDValue =
      instance && [UIDProperty] in instance && instance[UIDProperty];

    if (!UIDValue) {
      console.warn(
        `The image '${imageId}' does not have an associated ${UIDProperty}`
      );
      return null;
    }
    return UIDValue;
  } catch (e) {
    return null;
  }
};

/**
 * Get imageId from a event triggered by cornerstone
 * @param {Event} event
 * @return {string | undefined} Image id string or undefined if not present
 */
const getImageId = event => {
  return _.get(event, 'detail.image.imageId');
};

/**
 * Get imageId from a event triggered by cornerstone for old Image.
 * For some events cornerstone provides the oldImage object containing id.
 * @param {Event} event
 * @return {string | undefined} Image id string or undefined if not present
 */
const getOldImageId = event => {
  return _.get(event, 'detail.oldImage.imageId');
};

/**
 * It returns if two images ids refers to same study.
 * It uses provider to get StudyInstanceUID for given imageId
 * @param {string} imageId image id
 * @param {string} otherImageId another image id
 * @return {boolean} Check if image ids refers to same study
 */
const sameStudy = (imageId, otherImageId) => {
  const StudyInstanceUID = _getUIDValue(imageId, 'StudyInstanceUID');
  const otherStudyInstanceUID = _getUIDValue(otherImageId, 'StudyInstanceUID');

  return StudyInstanceUID === otherStudyInstanceUID;
};

/**
 * It returns if two images ids refers to same series.
 * It uses provider to get SeriesInstanceUID for given imageId
 * @param {string} imageId image id
 * @param {string} otherImageId another image id
 * @return {boolean} Check if image ids refers to same series
 */
const sameSeries = (imageId, otherImageId) => {
  const SeriesInstanceUID = _getUIDValue(imageId, 'SeriesInstanceUID');
  const otherSeriesInstanceUID = _getUIDValue(
    otherImageId,
    'SeriesInstanceUID'
  );

  return SeriesInstanceUID === otherSeriesInstanceUID;
};

/**
 * It returns if two images ids refers to same instance.
 * It uses provider to get SOPInstanceUID for given imageId
 * @param {string} imageId image id
 * @param {string} otherImageId another image id
 * @return {boolean} Check if image ids refers to same instance
 */
const sameInstance = (imageId, otherImageId) => {
  const SOPInstanceUID = _getUIDValue(imageId, 'SOPInstanceUID');
  const otherSOPInstanceUID = _getUIDValue(otherImageId, 'SOPInstanceUID');

  return SOPInstanceUID === otherSOPInstanceUID;
};

/**
 * It calculates the minimum, maximum, and mean value in the given pixel array
 *
 * @param {number[]} values array of pixels
 * @param {number} globalMin starting "min" value
 * @param {number} globalMax starting "max" value
 * @returns {Object} {min: number, max: number, mean: number }
 */
const calculateMinMaxMean = (values, globalMin, globalMax) => {
  const numValues = values.length;
  let min = globalMax;
  let max = globalMin;
  let sum = 0;

  if (numValues < 2) {
    return {
      min,
      max,
      mean: (globalMin + globalMax) / 2,
    };
  }

  for (let index = 0; index < numValues; index++) {
    const spv = values[index];

    min = Math.min(min, spv);
    max = Math.max(max, spv);
    sum += spv;
  }

  return {
    min,
    max,
    mean: sum / numValues,
  };
};

export {
  getImageId,
  getOldImageId,
  sameStudy,
  sameSeries,
  sameInstance,
  calculateMinMaxMean,
};
