const filterSupportedSingleFileTypes = new Set([
  'nifti',
  'ITK MetaIO Header',
  'image',
]);

const filterSupportedFileTypes = new Set([
  ...filterSupportedSingleFileTypes,
  'dicom',
]);

/**
 * Get a collection of all the files with file name and file id in each file entry
 * @param {*} filteredAcquisitionsMap
 * @returns {[Object]} - collection file infos
 */
function getFileDetailsFromAcquisition(filteredAcquisitionsMap) {
  const fileDetails = [];
  Object.keys(filteredAcquisitionsMap).forEach(key => {
    const files = filteredAcquisitionsMap[key].files;
    files.forEach(file => {
      if (filterSupportedFileTypes.has(file.type)) {
        fileDetails.push({ fileName: file.name, fileId: file._id });
      }
    });
  });
  return fileDetails;
}

/**
 * Get the filteres acquisitions and the filtered files in acquisition
 * as map with acquisition is as key
 * @param {*} filters
 * @param {*} allAcquisition
 * @returns {Object} - object of format {["acq Id"]: {acquisition, files}}
 */
function getFilteredAcquisitionMap(filters = [], allAcquisition = []) {
  let acquisitionRegex;
  let fileRegex;
  const filteredDetails = {};
  if (filters?.length) {
    filters.forEach(filter => {
      if (filter.type === 'acquisition') {
        acquisitionRegex = new RegExp(filter.match);
        allAcquisition = allAcquisition.filter(acq =>
          new RegExp(filter.match).test(acq.label)
        );
        allAcquisition.forEach(acq => {
          const filteredFiles = acq.files?.filter(file =>
            filterSupportedFileTypes.has(file.type)
          );
          if (filteredFiles.length) {
            filteredDetails[acq._id] = {
              acquisition: acq,
              files: filteredFiles,
            };
          }
        });
      }
    });
    filters.forEach(filter => {
      if (filter.type === 'file') {
        fileRegex = new RegExp(filter.match);
        allAcquisition.forEach(acq => {
          const filteredFiles =
            acq.files?.filter(file => {
              if (
                filterSupportedFileTypes.has(file.type) &&
                fileRegex.test(file.name)
              ) {
                return file;
              }
            }) || [];
          if (filteredFiles.length) {
            filteredDetails[acq._id] = {
              acquisition: acq,
              files: filteredFiles,
            };
          } else {
            delete filteredDetails[acq._id];
          }
        });
      }
    });
  } else {
    allAcquisition.forEach(acq => {
      const filteredFiles = acq.files?.filter(file =>
        filterSupportedFileTypes.has(file.type)
      );
      if (filteredFiles.length) {
        filteredDetails[acq._id] = {
          acquisition: acq,
          files: filteredFiles,
        };
      }
    });
  }
  return filteredDetails;
}

export { getFileDetailsFromAcquisition, getFilteredAcquisitionMap };
