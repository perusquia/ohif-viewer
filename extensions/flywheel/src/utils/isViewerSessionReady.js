import redux from '../redux';
import _ from 'lodash';

import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';
const { isCurrentFile } = FlywheelCommonUtils;
const { selectors } = redux;
/** List of items to represent required data for ready session*/
const defaultViewerSessionRequirements = [
  selectors.selectActiveProject,
  selectors.selectAssociations,
];
const fileViewerSessionRequirements = [selectors.selectActiveProject];

/**
 * It checks if session on viewer page is ready based on store state
 * Flywheel app depends on some initial pre configuration which adds to store.
 * So, it will be ready when store state is ready.
 * @param {Object} state store state
 * @return {Boolean} True in case viewer session is ready
 */
const isViewerSessionReady = state => {
  // TODO webimage change here after MR99
  const viewerSessionRequirements = isCurrentFile(state)
    ? fileViewerSessionRequirements
    : defaultViewerSessionRequirements;

  for (let selectData of viewerSessionRequirements) {
    const data = selectData(state);
    if (_.isEmpty(data)) {
      return false;
    }
  }

  return true;
};

export default isViewerSessionReady;
