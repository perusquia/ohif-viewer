import retrieveMeasurements from './retrieveMeasurements';
import storeMeasurements from './storeMeasurements';

let storeInstance;
let appConfigInstance;

const dataExchangeInstance = {
  retrieveMeasurements: (...args) =>
    retrieveMeasurements(storeInstance, appConfigInstance, ...args),
  storeMeasurements: (...args) => storeMeasurements(storeInstance, ...args),
  bindStoreInstance: store => (storeInstance = store),
  bindAppConfigInstance: appConfig => (appConfigInstance = appConfig),
};

export default dataExchangeInstance;
