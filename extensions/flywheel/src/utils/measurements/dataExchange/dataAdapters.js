import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';
import { log } from '@ohif/core';

const toApiCaseChangeMap = {
  studyinstanceuid: 'studyInstanceUid',
  seriesinstanceuid: 'seriesInstanceUid',
  sopinstanceuid: 'sopInstanceUid',
  studydate: 'studyDate',
  timedate: 'timeDate',
  patientname: 'patientName',
  patientid: 'patientId',
};

const { DataAdapters } = FlywheelCommonUtils;

const measurementsToolGroupName = 'allTools';

/**
 * It stores any hidden measurements. Or in this case, any filtered measurements.
 * Its an auxiliary array store a not used measurement but to be sent back api when it occurs.
 * @type {Array}
 */
let hiddenMeasurements = [];

/**
 * It adapts data from OHIF/MeasurementAPI to FLYW api data shape
 *
 * @param {AppMeasurementAPIMeasurements} grouppedMeasurements
 * @return {ApiMeasurementData} Measurements adapted to FLYW api shape
 * @throws Will throw an error in case it has failed to adapt data
 */
export const adaptToApi = grouppedMeasurements => {
  if (!grouppedMeasurements || typeof grouppedMeasurements !== 'object') {
    return grouppedMeasurements;
  }

  try {
    const allMeasurements = grouppedMeasurements[measurementsToolGroupName];
    const adaptedMeasurements = {};
    if (!allMeasurements) {
      return;
    }

    const adaptMeasurements = (measurements = [], adaptedMeasurements) => {
      _.forEach(measurements, toolData => {
        adaptedMeasurements[toolData.toolType] =
          adaptedMeasurements[toolData.toolType] || [];
        // groupping by tool type (an adapted data)
        adaptedMeasurements[toolData.toolType].push(
          DataAdapters.adaptToCase(toolData, toApiCaseChangeMap)
        );
      });
    };

    adaptMeasurements(allMeasurements, adaptedMeasurements);
    // recover and add to measurements any hidden measurement
    adaptMeasurements(hiddenMeasurements, adaptedMeasurements);

    return adaptedMeasurements;
  } catch (e) {
    throw new Error('Failed to adapt app measurement data to api shape');
  }
};

/**
 * Tool type name
 * @typedef {string} ToolType
 *
 */

/**
 * @typedef {Object} ToolData Tool data object
 * @property {string} ToolData.toolType it tells the type of measurement tool
 * @property {any} ToolData.* Other properties
 */
/**
 * Data shape of measurements is different from FLYW api and app OHIF/MeasurementAPI.
 * The main differences:
 *  - Api group measurements by tool type
 *  - Api uses a different property case (@see toApiCaseChangeMap) compared to OHIF/MeasurementAPI
 *  - OHIF/MeasurementAPI group all measurements into a single object (@see measurementsToolGroupName)
 * @typedef {Object.<ToolType, Array<ToolData>>} ApiMeasurementData
 */
/**
 * Data shape of measurements is different from FLYW api and app OHIF/MeasurementAPI.
 * @typedef {Object} AppMeasurementAPIMeasurements
 * @property {Array<ToolData>} AppMeasurementAPIMeasurements.allTools
 */
/**
 * It adapts data from api to OHIF/MeasurementAPI data shape
 *
 * @param {ApiMeasurementData} apiMeasurementData
 * @param {function} filterMethod Allow consumer to provide an external filter method to be applied as part of adaptation process (method will be called at the very end process)
 * @return {AppMeasurementAPIMeasurements}
 */
export const adaptToApp = (apiMeasurementData, filterMethod) => {
  hiddenMeasurements = [];
  if (!apiMeasurementData || typeof apiMeasurementData !== 'object') {
    return apiMeasurementData;
  }

  try {
    const adaptedMeasurements = [];
    _.forEach(apiMeasurementData, toolDataList => {
      let adapted = toolDataList.map(data => DataAdapters.adaptToCase(data));
      if (filterMethod) {
        const [filtered, remaining = []] = filterMethod(adapted);
        adapted = filtered;
        hiddenMeasurements.push(...remaining);
      }
      adaptedMeasurements.push(...adapted);
    });

    // Measurement API expects tools being grouped
    return {
      [measurementsToolGroupName]: adaptedMeasurements,
    };
  } catch (e) {
    log.warn(
      'Failed to adapt api measurement data to app MeasurementAPI shape'
    );
    return;
  }
};
