import { Measurements } from '../utils';

export default function init(store, appConfig) {
  // expose store into Measurements.dataExchange package
  Measurements.dataExchange.bindStoreInstance(store);
  Measurements.dataExchange.bindAppConfigInstance(appConfig);
}
