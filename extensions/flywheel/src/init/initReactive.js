import Reactive from '../reactive';
import { Reactive as FlywheelCommonReactive } from '@flywheel/extension-flywheel-common';

/**
 * It initializes some reactive components. On extension initialization
 * @param {object} store app store
 */
function initReactive(store) {
  Reactive.wwwcPresets.subscribe(
    store,
    FlywheelCommonReactive.store.observables.storeObservable$
  );
}

export default initReactive;
