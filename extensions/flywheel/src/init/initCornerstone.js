import cornerstone from 'cornerstone-core';
import Reactive from '../reactive';
import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';
import PermissionDialog from './../components/PermissionDialog/PermissionDialog';

const { getSubAnnotations } = FlywheelCommonUtils;
const popupFromLeft = 175;
const popupFromTop = 220;

export default function init({ commandsManager, appConfig, servicesManager }) {
  const { enableAutoFullDynamicWWWC } = appConfig;
  function elementEnabledHandler(event) {
    const elements = event.detail.element;
    elements.addEventListener(
      'fwCornerstoneMeasurementDeleting',
      deleteAnnotations
    );
    if (enableAutoFullDynamicWWWC) {
      Reactive.fullDynamicWWWC.subscribe(event, commandsManager);
    }
  }

  function elementDisabledHandler(event) {
    const elements = event.detail.element;
    elements.removeEventListener(
      'fwCornerstoneMeasurementDeleting',
      deleteAnnotations
    );
  }

  function deleteAnnotations(event) {
    let hasSubAnnotations = false;
    const dispatchActions = require('@ohif/viewer/src/appExtensions/MeasurementsPanel/dispatchActions.js');
    const measurements = event.detail.measurement;
    if (measurements?.length > 1) {
      measurements.forEach(data => {
        const subAnnotations = getSubAnnotations(data);
        if (subAnnotations.length) {
          hasSubAnnotations = true;
          subAnnotations.forEach(subAnnotation => {
            const data = measurements.find(
              measurement => measurement._id === subAnnotation._id
            );
            if (!data) {
              measurements.push(subAnnotation);
            }
          });
        }
      });

      if (!hasSubAnnotations) {
        dispatchActions.deleteAnnotations(measurements, {});
        return;
      }
      const { UIDialogService } = servicesManager.services;
      const subFormLabel =
        'Some annotation contains sub form response. Do you want to continue?';
      let helpDialogId = null;
      if (helpDialogId) {
        UIDialogService.dismiss({ id: helpDialogId });
        helpDialogId = null;
        return;
      }
      helpDialogId = UIDialogService.create({
        content: PermissionDialog,
        defaultPosition: {
          x: window.innerWidth / 2 - popupFromLeft,
          y: window.innerHeight / 2 - popupFromTop,
        },
        showOverlay: true,
        contentProps: {
          measurements: measurements,
          label: subFormLabel,
          onClose: () => {
            UIDialogService.dismiss({ id: helpDialogId });
            helpDialogId = null;
          },
          onConfirm: () => {},
          onDelete: dispatchActions.deleteAnnotations,
          toolActivationContext: {},
        },
      });
    } else {
      dispatchActions.default.onDeleteClick(event, measurements[0], {}, true);
    }
  }

  cornerstone.events.addEventListener(
    cornerstone.EVENTS.ELEMENT_ENABLED,
    elementEnabledHandler
  );
  cornerstone.events.addEventListener(
    cornerstone.EVENTS.ELEMENT_DISABLED,
    elementDisabledHandler
  );
}
