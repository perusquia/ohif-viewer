import { combineReducers } from 'redux';
import { redux as OhifRedux } from '@ohif/core';

import Redux from '../redux';
const reducers = Redux.reducers;

function initStore(appConfig) {
  const { store } = appConfig;
  // add flywheel reducer
  OhifRedux.reducers.flywheel = reducers.common;
  OhifRedux.reducers.studylist = reducers.studyList;
  OhifRedux.reducers.multipleReaderTask = reducers.multipleReaderTask;
  store.replaceReducer(combineReducers(OhifRedux.reducers));

  return store;
}

export default initStore;
