import fullDynamicWWWC from './fullDynamicWWWC';
import measurements from './measurements';
import store from './store';
import wwwcPresets from './wwwcPresets';

const reactive = {
  fullDynamicWWWC,
  measurements,
  store,
  wwwcPresets,
};

export default reactive;
