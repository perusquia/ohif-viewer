import { Subscription } from 'rxjs';
import { distinctUntilChanged, map, skipWhile } from 'rxjs/operators';
import { redux as OhifRedux } from '@ohif/core';
import _ from 'lodash';

import Redux from '../redux';

const { setUserPreferences } = OhifRedux.actions;
const { selectProjectConfig } = Redux.selectors;

let subscription$ = Subscription.EMPTY;

// create a stream to receive project wwwcPresets updates
const subscribeWWWCPresets = (store, store$) => {
  unsubscribeWWWCPresets();

  subscription$ = store$
    .pipe(
      map(state => _.get(selectProjectConfig(state), 'wwwcPresets', {})),
      distinctUntilChanged((oldPresets, newPresets) => {
        return _.isEqual(oldPresets, newPresets);
      }),
      skipWhile(wwwcPresets => _.isEmpty(wwwcPresets))
    )
    .subscribe(wwwcPresets => {
      const action = setUserPreferences({
        windowLevelData: { ...wwwcPresets },
      });
      store.dispatch(action);
    });
};

function unsubscribeWWWCPresets() {
  subscription$.unsubscribe();
}

/** Reactive object structure */

const subscribe = subscribeWWWCPresets;
const unsubscribe = unsubscribeWWWCPresets;

// define package object
const reactive = { subscribe, unsubscribe };
/* Uncomment if necessary
Object.defineProperty(reactive, 'subscriptions', {
  get: () => {
    return {
      subscription$,
    };
  },
  configurable: false,
  enumerable: false,
});
*/

export default reactive;
