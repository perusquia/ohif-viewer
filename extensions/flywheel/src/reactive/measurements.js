import { log } from '@ohif/core';
import cornerstoneTools from 'cornerstone-tools';
import { Subject } from 'rxjs';
import { buffer, debounceTime } from 'rxjs/operators';

import getMeasurementLocationCallback from '@ohif/viewer/src/appExtensions/MeasurementsPanel/getMeasurementLocationCallback';
import redux from '../redux';

const { selectUser } = redux.selectors;

/**
 * Collect measurement events and batch them for a period of time.
 * If a batch contains a MEASUREMENT_ADDED event, trigger a labelling
 * prompt to set an initial location/label on the measurement.
 */
const labelSource$ = new Subject();
const labelNotify$ = labelSource$.pipe(debounceTime(100));
const labelBuffer$ = labelSource$.pipe(buffer(labelNotify$));
labelBuffer$.subscribe(eventBatch => {
  try {
    const addedEvents = eventBatch.filter(
      event =>
        event.type === cornerstoneTools.EVENTS.MEASUREMENT_ADDED &&
        event.detail.toolType !== 'stackPrefetch' &&
        event.detail.toolType !== 'stack'
    );
    const completedEvents = eventBatch.filter(
      event =>
        event.type === cornerstoneTools.EVENTS.MEASUREMENT_COMPLETED &&
        event.detail.toolType !== 'stackPrefetch' &&
        event.detail.toolType !== 'stack'
    );

    for (
      let addedEventIndex = 0;
      addedEventIndex < addedEvents.length;
      addedEventIndex++
    ) {
      const addedEvent = addedEvents[addedEventIndex];
      const completedEventIndex = completedEvents.findIndex(
        event =>
          event.detail.measurementData._id ===
          addedEvent.detail.measurementData._id
      );
      if (completedEventIndex !== -1) {
        const completedEvent = completedEvents[completedEventIndex];
        const {
          detail: { element, toolType, measurementData },
        } = completedEvent;

        getMeasurementLocationCallback(
          {
            event: {
              clientX: element.clientWidth,
              clientY: element.clientHeight,
            },
            element: element,
          },
          {
            ...measurementData,
            toolType: toolType,
            location: null,
          },
          {
            skipAddLabelButton: true,
            editLocation: true,
          }
        );
        completedEvents.splice(completedEventIndex, 1);
        addedEvents.splice(addedEventIndex, 1);
        break;
      }
    }
    addedEvents.concat(completedEvents).forEach(event => {
      labelSource$.next(event);
    });
  } catch (error) {
    log.error(error);
  }
});

export function handleMeasurementEvent(event) {
  if (
    event.type === cornerstoneTools.EVENTS.MEASUREMENT_ADDED &&
    !_.has(event.detail, 'measurementData.flywheelOrigin')
  ) {
    const user = selectUser(store.getState());
    if (user) {
      _.set(event.detail, 'measurementData.flywheelOrigin', {
        type: 'user',
        id: user._id,
      });
    }
  }
  labelSource$.next(event);
}

/** Reactive object structure */

// default methods
const subscribe = () => {};
const unsubscribe = () => {};
const next = handleMeasurementEvent;

// define package object
const reactive = { subscribe, unsubscribe, next };

export default reactive;
