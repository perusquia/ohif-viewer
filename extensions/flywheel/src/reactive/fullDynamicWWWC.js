import cornerstone from 'cornerstone-core';
import { fromEvent } from 'rxjs';
import { distinctUntilChanged, switchMap, take } from 'rxjs/operators';
import { cornerstoneUtils } from '../utils';
import _ from 'lodash';

/**
 * Array with validators to be considered when evaluating to apply or not wwwc. Each compares old image and new image.
 * In case function returns false is a indicator wwwc should be applied.
 * For current values it means: apply full dynamic in case NOT same study or NOT same series. Add another array entry (cornerstoneUtils.sameInstance)
 * in case you want to apply even when instance has changed
 *
 * @typedef FullDynamicValidators
 * @type {Array[Function]} Each function will be a validator to be considered on applying or not full dynamic.
 */
const fullDynamicValidators = [
  cornerstoneUtils.sameStudy,
  cornerstoneUtils.sameSeries,
];

/**
 * Check if given cornerstone event param we should apply auto fulldynamicWWWC.
 * It uses fullDynamicValidators array to validate if wwwc should be applied or not.
 *
 * @param {Event} event
 * @return {boolean} True in case full dynamic must be applied
 */
const _shouldApplyFullDynamicWWWC = event => {
  const previousImageId = cornerstoneUtils.getOldImageId(event);
  const imageId = cornerstoneUtils.getImageId(event);

  for (let validator of fullDynamicValidators) {
    const validatorFailed = !validator(previousImageId, imageId);
    if (validatorFailed) {
      return true;
    }
  }

  return false;
};

// auxiliary map to unsubscribe subscriptions.
// It maps unique element parent identifier to its subscription.
const newImageSubscriptionMap$ = new Map();

/**
 * It returns parent identifier. OHIF application defines a selector for a given element which will be unique on subscription map.
 * @param {Object} element HTML element
 * @return {Object | undefined} It returns parent identifier
 */
const _getParentIdentifier = (element = {}) => {
  return _.get(element, 'parentNode.className');
};

/**
 * Add subscription into map.
 * @param {Object} element Element which subscription refers to.
 * @param {Object} subscription$ Subscription to add into map
 */
const _addSubscription = (element, subscription$) => {
  const identifier = _getParentIdentifier(element);

  if (identifier) {
    newImageSubscriptionMap$.set(identifier, subscription$);
  }
};

/**
 * It returns any existing subscription of given element. It uses element`s parent to get unique identifier.
 * @param {Object} element Element which subscription refers to.
 * @return {Object | undefined} Subscription if existing
 */
const _getSubscription = element => {
  const identifier = _getParentIdentifier(element);

  if (identifier) {
    return newImageSubscriptionMap$.get(identifier);
  }
};

/**
 * Remove subscription from map if existing
 * @param {Object} element Element which subscription refers to.
 * @return {boolean} True in case of success
 */
const _removeSubscription = element => {
  const identifier = _getParentIdentifier(element);

  if (identifier) {
    return newImageSubscriptionMap$.delete(identifier);
  }

  return false;
};

/**
 * It subscribes the command setFullDynamicWWWC from commandsManager when there is a new image.
 * The flow works as: on cs event cornerstone.EVENTS.NEW_IMAGE, in case should apply wwwc, then on cs event cornerstone.EVENTS.IMAGE_RENDERED, then apply wwwc.
 *
 * @param {Event} event cornerstone event
 * @param {Object} commandsManager OHIF commands manager
 */
const subscribe = (event, commandsManager) => {
  const element = event.detail.element;
  unsubscribe(event);
  const newImageObservable$ = fromEvent(element, cornerstone.EVENTS.NEW_IMAGE);

  const newImageSubscription$ = newImageObservable$
    .pipe(
      distinctUntilChanged(
        (previousEvent, event) => !_shouldApplyFullDynamicWWWC(event)
      ),
      switchMap((
        event // only after image is rendered. To avoid event race condition issue
      ) =>
        fromEvent(event.detail.element, cornerstone.EVENTS.IMAGE_RENDERED).pipe(
          take(1)
        )
      )
    )
    .subscribe(event => {
      commandsManager.runCommand('setFullDynamicWWWC', {
        element: event.detail.element,
      });
    });

  _addSubscription(element, newImageSubscription$);
};

/**
 * It unsubscribes any subscription of given event. It also removes subscription from map object.
 * By default, considering it has being used fromEvent, a subscription from a removed element must be auto unsubscribed, but this method ensure other cases.
 *
 * @param {Event} event cornerstone event to look for any existing subscription.
 */
const unsubscribe = event => {
  const element = event.detail.element;
  const subscription$ = _getSubscription(element);

  if (subscription$) {
    subscription$.unsubscribe();
    _removeSubscription(element);
  }
};

// define package object
const reactive = { subscribe, unsubscribe };

export default reactive;
