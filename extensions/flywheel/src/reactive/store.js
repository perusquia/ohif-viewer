import { Observable } from 'rxjs';

let storeObservable$;

const createObservable = store => {
  storeObservable$ = Observable.create(subscriber =>
    store.subscribe(() => {
      subscriber.next(store.getState());
    })
  );

  return storeObservable$;
};

/** Reactive object structure */

// define package object
const reactive = { createObservable };
Object.defineProperty(reactive, 'observables', {
  get: () => {
    return {
      storeObservable$,
    };
  },
  configurable: false,
  enumerable: false,
});

export default reactive;
