import initExtension from './init';
import commandsModule from './commandsModule';
import Components from './components';
import Redux from './redux';
import Reactive from './reactive';
import {
  isViewerSessionReady,
  Measurements,
  resetLayoutToHP,
  getDisplayProperties,
  applyDisplayProperties,
  avatarClick,
  onAvatarMouseEnter,
  onAvatarMouseLeave,
  cornerstoneUtils,
} from './utils';

// control what will be exposed from utils folder
const Utils = {
  isViewerSessionReady,
  Measurements,
  resetLayoutToHP,
  getDisplayProperties,
  applyDisplayProperties,
  avatarClick,
  onAvatarMouseEnter,
  onAvatarMouseLeave,
  cornerstoneUtils,
};

export default {
  id: 'flywheel-extension',

  preRegistration({
    commandsManager,
    servicesManager,
    appConfig,
    hotkeysManager = {},
  }) {
    initExtension({
      commandsManager,
      servicesManager,
      appConfig,
      hotkeysManager,
    });
  },
  getCommandsModule() {
    return commandsModule();
  },
};

export { Components, Redux, Reactive, Utils };
