import cornerstone from 'cornerstone-core';
import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';

const { measurementTools, selectActiveTool } = FlywheelCommonRedux.selectors;

export function setupFullscreen(store) {
  document.body.addEventListener('dblclick', event => {
    if (event.target instanceof HTMLCanvasElement) {
      try {
        const enabledElement = cornerstone.getEnabledElement(
          event.target.parentElement
        );
        toggleFullscreen(enabledElement, store);
      } catch (error) {}
    }
  });
  document.body.addEventListener('keydown', event => {
    if (event.key === 'Escape') {
      exitFullscreen();
    }
  });
}

const state = {
  element: null,
  style: null,
};

function toggleFullscreen(enabledElement, store) {
  if (state.style) {
    exitFullscreen();
    return;
  }
  const activeTool = selectActiveTool(store.getState());
  const measurementToolActive = measurementTools.includes(activeTool);
  if (!measurementToolActive) {
    state.element = enabledElement.element;
    state.style = applyStyle(enabledElement.element, {
      position: 'fixed',
      zIndex: 1,
      top: 0,
      left: 0,
    });
    cornerstone.resize(enabledElement.element);
  }
}

function exitFullscreen() {
  if (state.element && state.style) {
    applyStyle(state.element, state.style);
    cornerstone.resize(state.element);
  }
  state.element = null;
  state.style = null;
}

function applyStyle(element, style) {
  const activeStyle = {};
  for (const [key, value] of Object.entries(style)) {
    activeStyle[key] = element.style[key];
    element.style[key] = value;
  }
  return activeStyle;
}
