import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import './ProjectList.styl';

import Redux from '../redux';
import {
  HTTP as FlywheelCommonHTTP,
  Utils as FlywheelCommonUtils,
} from '@flywheel/extension-flywheel-common';

const { setAllProjects } = Redux.actions;
const { listProjects } = FlywheelCommonHTTP.services;
const { getRouteParams } = FlywheelCommonUtils;

class ProjectList extends Component {
  componentDidMount() {
    const { taskId } = getRouteParams();
    if (taskId) {
      return;
    }
    listProjects().then(projects => {
      this.props.setAllProjects(
        Object.assign(
          {},
          ...projects.map(project => ({ [project._id]: project }))
        )
      );
    });
  }

  render() {
    const { taskId } = getRouteParams();
    if (taskId) {
      return null;
    }
    const { projects } = this.props;
    const projectList = Object.values(projects).sort((a, b) => {
      a = (a.label || '').toLowerCase();
      b = (b.label || '').toLowerCase();
      return a > b ? 1 : b > a ? -1 : 0;
    });

    return (
      <div className="project-list">
        <h1 className="project-list-header">Projects</h1>
        {projectList.length === 0 && (
          <div className="project-list-empty">No projects available</div>
        )}
        {projectList.map(project => (
          <Link
            key={project._id}
            to={`/project/${project._id}`}
            className="project-list-item"
          >
            {project.label}
          </Link>
        ))}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  projects: state.flywheel.allProjects,
});

const mapDispatchToActions = { setAllProjects };

export default connect(mapStateToProps, mapDispatchToActions)(ProjectList);
