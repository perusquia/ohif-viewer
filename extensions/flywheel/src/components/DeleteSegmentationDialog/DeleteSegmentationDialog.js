import React from 'react';
import { SimpleDialog } from '@ohif/ui';

const DeleteSegmentationDialog = props => {
  return (
    <SimpleDialog
      headerTitle="Delete Segmentation"
      onClose={() => {}}
      onConfirm={() => {}}
      showFooterButtons={false}
      hideCloseButton={true}
    >
      <div className="label-style">{props.label}</div>
      <div className="button-container">
        <div
          className="button-style"
          onClick={() => {
            props.onDelete();
            props.onClose();
          }}
        >
          Yes
        </div>

        <div onClick={props.onClose} className="button-style">
          No
        </div>
      </div>
    </SimpleDialog>
  );
};
export default DeleteSegmentationDialog;
