export const percentColorsConfig = [
  { from: 0, to: 25, color: 'rgb(206,61,32)' },
  { from: 26, to: 50, color: 'rgb(246,154,16)' },
  { from: 51, to: 75, color: 'rgb(255,249,58)' },
  { from: 76, to: 100, color: 'rgb(41,239,19)' },
];
