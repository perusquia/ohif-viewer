import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import cornerstone from 'cornerstone-core';
import cornerstoneTools from 'cornerstone-tools';
import { SimpleDialog } from '@ohif/ui';
import {
  Redux as FlywheelCommonRedux,
  Utils as FlywheelCommonUtils,
} from '@flywheel/extension-flywheel-common';
import {
  getSelectedSegmentationData,
  getActiveSegmentationDisplaySet,
  getActiveViewport,
} from '@flywheel/extension-cornerstone-infusions';
import './EditSegmentationLabel.styl';

const { getSmartCTRangeList } = FlywheelCommonUtils;
const { setSmartCTRange } = FlywheelCommonRedux.actions;
const { setters, state, getters } = cornerstoneTools.getModule('segmentation');

export default function EditSegmentationLabelDialog(props) {
  const { onClose, segmentationData } = props.contentProps;
  const dispatch = useDispatch();
  const smartCTRangeList = getSmartCTRangeList();
  const selectedRange = useSelector(state => state.smartCT.selectedRange);

  const [selectedItem, setSelectedItem] = useState(selectedRange);

  const updateSegmentPixels = segmentIndex => {
    const { dataProtocol } = getActiveViewport();

    if (dataProtocol === 'nifti') {
      const {
        activeSegmentationDisplaySets,
        firstImageIds,
        elements,
      } = getSelectedSegmentationData(segmentationData);

      if (activeSegmentationDisplaySets.length !== elements.length) {
        activeSegmentationDisplaySets.forEach((displaySet, index) => {
          updateSegmentDetailsOnImageOffline(
            segmentIndex,
            firstImageIds[index],
            displaySet,
            segmentationData
          );
        });
      }
      elements.forEach((element, index) => {
        const viewportSpecificData = store.getState().viewports
          .viewportSpecificData[index];

        const displaySet = activeSegmentationDisplaySets.find(
          ds =>
            ds?.SeriesInstanceUID?.indexOf(
              viewportSpecificData.SeriesInstanceUID + '-derived'
            ) === 0
        );

        updateSegmentDetailsOnImage(
          segmentIndex,
          firstImageIds[index],
          displaySet,
          element,
          segmentationData
        );
      });
    } else {
      const {
        activeSegmentationDisplaySets,
        firstImageIds,
        elements,
      } = getSelectedSegmentationData(segmentationData);
      elements.forEach((element, index) => {
        updateSegmentDetailsOnImage(
          segmentIndex,
          firstImageIds[index],
          activeSegmentationDisplaySets[index],
          element,
          segmentationData
        );
      });
    }
  };

  const updateSegmentDetailsOnImageOffline = (
    segmentIndex,
    firstImageId,
    activeSegmentationDisplaySet,
    segmentationData
  ) => {
    let activeLabelmap = {};
    const brushStackState = state.series[firstImageId];
    const labelmaps3D = brushStackState.labelmaps3D;
    if (activeSegmentationDisplaySet?.labelmapIndexes[0]) {
      if (labelmaps3D) {
        activeLabelmap =
          labelmaps3D[activeSegmentationDisplaySet.labelmapIndexes[0]];
      }
    }
    const labelMaps2D = activeLabelmap.labelmaps2D;
    const prevSegmentIndex = activeLabelmap.activeSegmentIndex;
    if (!labelMaps2D) {
      return;
    }

    Object.keys(labelMaps2D).forEach(imageIndex => {
      if (labelMaps2D[imageIndex].segmentsOnLabelmap) {
        const labelMap2D = labelMaps2D[imageIndex];
        const indexOnSegment = labelMap2D.segmentsOnLabelmap.findIndex(
          index => index === prevSegmentIndex
        );
        if (indexOnSegment > -1) {
          labelMap2D.segmentsOnLabelmap[indexOnSegment] = segmentIndex;
          labelMap2D.pixelData.forEach((segmentationIndex, index) => {
            if (segmentationIndex === prevSegmentIndex) {
              labelMap2D.pixelData[index] = segmentIndex;
            }
          });
        }
      }
    });

    brushStackState.labelmaps3D[
      activeSegmentationDisplaySet.labelmapIndexes[0]
    ].activeSegmentIndex = segmentIndex;

    setters.activeSegmentIndexOffline(firstImageId, segmentIndex);
    activeSegmentationDisplaySet.segmentationIndexes = [segmentIndex];

    const segDisplaySet = getActiveSegmentationDisplaySet(segmentationData);
    if (segDisplaySet) {
      segDisplaySet.segmentationIndexes = [segmentIndex];
    }
  };

  const updateSegmentDetailsOnImage = (
    segmentIndex,
    firstImageId,
    activeSegmentationDisplaySet,
    element,
    segmentationData
  ) => {
    let activeLabelmap = {};
    if (activeSegmentationDisplaySet?.labelmapIndexes[0] && element) {
      const { labelmaps3D } = getters.labelmaps3D(element);
      if (labelmaps3D) {
        activeLabelmap = getters.labelmap3D(
          element,
          activeSegmentationDisplaySet.labelmapIndexes[0]
        );
      }
    }
    const labelMaps2D = activeLabelmap.labelmaps2D;
    const prevSegmentIndex = activeLabelmap.activeSegmentIndex;
    if (!labelMaps2D) {
      return;
    }

    Object.keys(labelMaps2D).forEach(imageIndex => {
      if (labelMaps2D[imageIndex].segmentsOnLabelmap) {
        const labelMap2D = labelMaps2D[imageIndex];
        const indexOnSegment = labelMap2D.segmentsOnLabelmap.findIndex(
          index => index === prevSegmentIndex
        );
        if (indexOnSegment > -1) {
          labelMap2D.segmentsOnLabelmap[indexOnSegment] = segmentIndex;
          labelMap2D.pixelData.forEach((segmentationIndex, index) => {
            if (segmentationIndex === prevSegmentIndex) {
              labelMap2D.pixelData[index] = segmentIndex;
            }
          });
        }
      }
    });
    const brushStackState = state.series[firstImageId];

    brushStackState.labelmaps3D[
      activeSegmentationDisplaySet.labelmapIndexes[0]
    ].activeSegmentIndex = segmentIndex;

    setters.activeSegmentIndex(element, segmentIndex);
    activeSegmentationDisplaySet.segmentationIndexes = [segmentIndex];

    const segDisplaySet = getActiveSegmentationDisplaySet(segmentationData);
    if (segDisplaySet) {
      segDisplaySet.segmentationIndexes = [segmentIndex];
    }
    onClose();
    cornerstone.updateImage(element);
  };

  const setSelectedLabel = () => {
    dispatch(setSmartCTRange(selectedItem));

    const segmentIndex = smartCTRangeList.findIndex(
      segmentation => segmentation.label === selectedItem.label
    );

    if (segmentIndex > -1) {
      updateSegmentPixels(segmentIndex + 1);
    }
    onClose();
  };

  const listItems = smartCTRangeList.map(item => (
    <li
      key={item.label}
      onClick={() => setSelectedItem(item)}
      className={`${selectedItem.label === item.label && 'active-li'}`}
    >
      <div
        className="color-picker-container"
        style={{
          backgroundColor: `rgba(${item.color[0]}, ${item.color[1]}, ${item.color[2]}, 1.0 )`,
        }}
      ></div>
      <div style={{ color: 'white' }}>{item.label}</div>
    </li>
  ));

  return (
    <SimpleDialog
      headerTitle={'Change Segmentation Label'}
      rootClass="segment-label-chooser-dialog"
      onClose={onClose}
      onConfirm={setSelectedLabel}
      showFooterButtons={true}
      confirmButtonTitle={'Save'}
      enableConfirm={true}
    >
      <div className="selectLabel-container">
        <ul>{listItems}</ul>
      </div>
    </SimpleDialog>
  );
}
