import { Component } from 'react';
import React from 'react';
import PropTypes from 'prop-types';
import { SimpleDialog } from '@ohif/ui';
import bounding from '@ohif/viewer/src/lib/utils/bounding.js';
import './DisplaySetDragDropMenuDialog.styl';

export default class DisplaySetDragDropMenuDialog extends Component {
  static defaultProps = {
    componentRef: React.createRef(),
    componentStyle: {},
  };

  static propTypes = {
    hideFusionButton: PropTypes.bool,
    onApply: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired,
    componentRef: PropTypes.object,
    componentStyle: PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.mainElement = React.createRef();
  }

  componentDidMount = () => {
    bounding(this.mainElement);
  };

  render() {
    return (
      <SimpleDialog
        headerTitle="Select Display Option"
        onClose={this.onClose}
        onConfirm={() => {}}
        componentRef={this.mainElement}
        showFooterButtons={false}
      >
        <div className="btnContainer">
          <div onClick={() => this.handleClick()} className="btnStyle">
            View Series
          </div>
          {!this.props.hideFusionButton && (
            <div onClick={() => this.handleClick(true)} className="btnStyle">
              Fuse Series
            </div>
          )}
        </div>
      </SimpleDialog>
    );
  }

  handleClick = (applyFusion = false) => {
    this.props.onApply(applyFusion);
  };

  onClose = () => {
    this.props.onClose();
  };
}
