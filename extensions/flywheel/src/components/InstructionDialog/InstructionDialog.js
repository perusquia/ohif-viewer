import React, { Component } from 'react';
import PropTypes from 'prop-types';

import store from '@ohif/viewer/src/store';
import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';
import { ToolbarButton } from '@ohif/ui/src/viewer/ToolbarButton.js';
import { SimpleDialog } from '@ohif/ui';
import bounding from '@ohif/viewer/src/lib/utils/bounding.js';
import IFrame from '../IFrame/IFrame.js';

import './InstructionDialog.styl';

const { RightHandPanelModes } = FlywheelCommonRedux.constants;

export default class InstructionDialog extends Component {
  static defaultProps = {
    componentRef: React.createRef(),
  };

  static propTypes = {
    instruction: PropTypes.object.isRequired,
    onClose: PropTypes.func.isRequired,
    componentRef: PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.state = { currentIndex: 0 };
    this.mainElement = React.createRef();
  }

  componentDidMount = () => {
    bounding(this.mainElement);
  };

  nextPrev(right = true) {
    const { instruction } = this.props.contentProps;
    const imagesList = instruction?.images || [];
    if (!imagesList.length) {
      return;
    }
    let newIndex = this.state.currentIndex;
    newIndex = right ? newIndex + 1 : newIndex - 1;
    if (newIndex < imagesList.length && newIndex > -1) {
      this.setState({ currentIndex: newIndex });
    }
  }

  hasInstruction(instruction) {
    if (!instruction || (!instruction?.url && !instruction?.label)) {
      return false;
    }
    return true;
  }

  getDialogHeader(instruction) {
    if (!this.hasInstruction(instruction)) {
      return null;
    }
    const { url, label } = instruction;
    if (url && label) {
      return (
        <div className="instruction-label">
          <a href={url} className="link-color" target="_blank" title={label}>
            {label}
          </a>
        </div>
      );
    }
    if (url && !label) {
      return (
        <div className="instruction-label">
          <a href={url} className="link-color" target="_blank" title={url}>
            {url}
          </a>
        </div>
      );
    }
    return <div className="instruction-label">{label}</div>;
  }

  getLinkHeader(instruction) {
    if (!this.hasInstruction(instruction)) {
      return null;
    }
    const { label, url } = instruction;
    if (url && !label) {
      return (
        <div className="align-text">
          <a className="link-color" href={url} target="_blank">
            {url}
          </a>
        </div>
      );
    }
    return (
      <div className="align-text">
        <a className="link-color" href={url} target="_blank">
          {label}
        </a>
      </div>
    );
  }

  isExpandedRightHandPanel = () => {
    const state = store.getState();
    return state?.rightHandPanel?.panelMode === RightHandPanelModes.EXPAND;
  };

  render() {
    const isExpanded = this.isExpandedRightHandPanel();
    const expandedRIghtHandPanel = isExpanded
      ? 'expanded-right-hand-panel'
      : '';
    const { instruction, clearTimeOut } = this.props.contentProps;
    const { currentIndex } = this.state;
    const imageStyle = {
      width: '100%',
      height: '100%',
      objectFit: 'contain',
    };
    return (
      <SimpleDialog
        headerTitle=""
        onClose={() => {}}
        onConfirm={() => {}}
        componentRef={this.mainElement}
        showFooterButtons={false}
        hideHeaderSection={true}
        rootClass="main-dialog"
      >
        <div
          className={
            instruction?.images
              ? `instruction-dialog ${expandedRIghtHandPanel}`
              : `instruction-dialog-link ${expandedRIghtHandPanel}`
          }
          onMouseEnter={() => clearTimeOut()}
          onMouseLeave={() => this.props.contentProps?.onClose()}
        >
          <div className="main-content">
            {instruction?.images ? (
              <>
                {instruction.images.length !== 1 && (
                  <div className="prev-arrow">
                    <span
                      className="user-select-none prev-icon-container"
                      onClick={e => this.nextPrev(false)}
                    >
                      <ToolbarButton
                        className="material-icons icon-size prev-icon"
                        icon={'arrow_back_ios'}
                        label=""
                        t={l => l}
                      />
                    </span>
                  </div>
                )}
                <div className="image-container">
                  {this.getDialogHeader(instruction)}
                  <div className="instruction-new-tab">
                    <a
                      href={instruction.images[currentIndex]}
                      target="_blank"
                      title="New Tab"
                    >
                      <ToolbarButton
                        t={l => l}
                        label=""
                        icon="open_in_new"
                        class="material-icons icon-size"
                      ></ToolbarButton>
                    </a>
                  </div>
                  <IFrame {...this.props}>
                    <img
                      style={imageStyle}
                      src={instruction.images[currentIndex]}
                    />
                  </IFrame>
                </div>
                {instruction.images.length !== 1 && (
                  <div className="next-arrow">
                    <span
                      className="user-select-none next-icon-container"
                      onClick={e => this.nextPrev()}
                    >
                      <ToolbarButton
                        className="material-icons icon-size next-icon"
                        icon={'arrow_forward_ios'}
                        label=""
                        t={l => l}
                      />
                    </span>
                  </div>
                )}
              </>
            ) : (
              <div className="instruction-link">
                <div className="instruction-link-content">
                  {this.getLinkHeader(instruction)}
                </div>
              </div>
            )}
          </div>
        </div>
      </SimpleDialog>
    );
  }
}
