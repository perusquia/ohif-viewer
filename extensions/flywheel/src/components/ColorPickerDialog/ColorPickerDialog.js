import React, { Component } from 'react';

import store from '@ohif/viewer/src/store';

import './ColorPickerDialog.styl';

const ColorPicker = require('a-color-picker');
const DEFAULT_COLOR = 'rgba(255, 255, 0, 0.2)';

export default class ColorPickerDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedColor: DEFAULT_COLOR,
      showColorPicker: false,
      colorPickCoordinates: { left: 0, top: 0 },
    };
  }

  componentDidUpdate(prevProps, prevState) {
    const {
      colorPickCoordinates,
      showColorPicker,
      selectedColor,
      measurementData,
      palette,
    } = this.props;
    if (prevProps.showColorPicker !== this.props.showColorPicker) {
      this.setState(
        {
          colorPickCoordinates,
          showColorPicker,
          selectedColor,
          measurementData,
          palette,
        },
        () => this.onColorChangeHandler()
      );
    }
  }

  static getDerivedStateFromProps(props, state) {
    if (props.showColorPicker !== state.showColorPicker) {
      const {
        colorPickCoordinates,
        showColorPicker,
        selectedColor,
        measurementData,
        palette,
      } = props;
      return {
        colorPickCoordinates,
        showColorPicker,
        selectedColor,
        measurementData,
        palette,
      };
    }
    return null;
  }

  onColorChangeHandler = () => {
    ColorPicker.from('.picker').on('change', picker => {
      const { palette } = this.props;
      const selectedColor = `rgba(${picker.rgba.join(',')})`;
      this.setState(
        {
          selectedColor,
        },
        () => {
          if (Array.isArray(this.state.measurementData)) {
            this.state.measurementData.forEach(x =>
              this.props.onChangeColor(selectedColor, x)
            );
          } else {
            this.props.onChangeColor(selectedColor, this.state.measurementData);
          }

          store.dispatch(
            this.props.dispatchUpdateRoiColor({
              current: selectedColor,
              next: palette[this.props.paletteIndex],
            })
          );
        }
      );
    });
  };

  closeColorPicker = () => {
    const { measurements } = this.props;
    store.dispatch(this.props.dispatchUpdatePaletteColors(measurements));
    this.props?.closeColorPicker();
  };

  getColorSelectorDialog() {
    return (
      <div
        tabIndex={1}
        className="color-picker"
        onBlurCapture={() => this.closeColorPicker()}
        onBlur={() => this.closeColorPicker()}
        style={{
          left: this.state.colorPickCoordinates.left,
          top: this.state.colorPickCoordinates.top,
        }}
        onMouseDown={e => e.preventDefault()}
        onClick={e => e.preventDefault()}
      >
        <div
          className="picker"
          acp-color={this.state.selectedColor}
          acp-palette={this.state.palette.join('|')}
          acp-show-rgb="no"
          acp-show-hsl="no"
          acp-show-hex="no"
          acp-show-alpha="yes"
          acp-palette-editable="no"
          acp-use-alpha-in-palette="no"
        ></div>
      </div>
    );
  }

  render() {
    if (!this.props.showColorPicker) {
      return null;
    }
    return this.getColorSelectorDialog();
  }
}
