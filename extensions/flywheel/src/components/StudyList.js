import React from 'react';
import { Link } from 'react-router-dom';
import store from '@ohif/viewer/src/store';
import { ToolbarButton } from '@ohif/ui';

function StudyListComponent({ button }) {
  const { id, label, icon } = button;

  const projectId = store.getState().flywheel.project?._id;
  const studyListPath = projectId ? `/project/${projectId}` : '/';
  return (
    <Link
      to={{
        pathname: studyListPath,
        state: { studyLink: window.location.pathname },
      }}
    >
      <ToolbarButton
        key={id}
        label={label}
        icon={icon}
        class={button.class}
      ></ToolbarButton>
    </Link>
  );
}

export default StudyListComponent;
