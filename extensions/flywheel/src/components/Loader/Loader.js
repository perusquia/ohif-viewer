import React from 'react';
import './Loader.css';
export default function Loader({ loader }) {
  return (
    <div
      className="loader-container"
      style={{ display: loader ? 'flex' : 'none' }}
    >
      <div className="loader"></div>
    </div>
  );
}
