/**
 * Auxiliary method which will updateComponent using given updateMethod for promise stages: before, during, just after(on error or on success), later (schedule from timeout)
 * @param {Object} props object with possible property values (init, success, error, default)
 * @param {Promise} promise Promise to be invoked
 * @param {Function} setOnComponent Component update method
 * @param {number} timeout waiting period to set component to defaults state
 */
const updateComponentByPromise = async (
  props,
  promise,
  setOnComponent,
  timeout = 2000
) => {
  setOnComponent(props.init);
  try {
    await promise();
    setOnComponent(props.success);
  } catch (e) {
    setOnComponent(props.error, e);
  } finally {
    setTimeout(() => {
      setOnComponent(props.default);
    }, timeout);
  }
};

export { updateComponentByPromise };
