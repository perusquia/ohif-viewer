import ErrorPage from './ErrorPage';
import HelpDialog from './HelpDialog';
import LabellingOverlay from './Labelling/ConnectedLabellingOverlay';
import MeasurementPanel from './MeasurementPanel';
import ProjectList from './ProjectList';
import ToolContextMenu from './ToolContextMenu/ConnectedToolContextMenu';
import SegmentationFileChooser from './SegmentationFileChooser/SegmentationFileChooser';
import EditSegmentationLabelDialog from './EditSegmentationLabelDialog/EditSegmentationLabelDialog';
import SaveSegmentationDialog from './SaveSegmentationDialog/SaveSegmentationDialog';
import ContourCollisionDialog from './ContourCollisionDialog/ContourCollisionDialog';
import EditLabelDescriptionDialog from './EditLabelDescriptionDialog/EditLabelDescriptionDialog';
import DeleteDialog from './DeleteDialog/DeleteDialog';
import PermissionDialog from './PermissionDialog/PermissionDialog';
import Loader from './Loader/Loader';
import StudyFormQuestionConfirmationDialog from './StudyForm/dialog/StudyFormQuestionConfirmationDialog.js';
import SubFormDialog from './StudyForm/dialog/SubFormDialog.js';
import StudyListComponent from './StudyList';
import DisplaySetDragDropMenuDialog from './DisplaySetDragDropMenuDialog/DisplaySetDragDropMenuDialog';
import VerticalResizer from './VerticalResizer/VerticalResizer';
import DeleteSegmentationDialog from './DeleteSegmentationDialog/DeleteSegmentationDialog';

export default {
  ErrorPage,
  HelpDialog,
  LabellingOverlay,
  MeasurementPanel,
  ProjectList,
  ToolContextMenu,
  SegmentationFileChooser,
  EditSegmentationLabelDialog,
  SaveSegmentationDialog,
  ContourCollisionDialog,
  DeleteDialog,
  PermissionDialog,
  EditLabelDescriptionDialog,
  Loader,
  StudyFormQuestionConfirmationDialog,
  SubFormDialog,
  StudyListComponent,
  DisplaySetDragDropMenuDialog,
  VerticalResizer,
  DeleteSegmentationDialog,
};
