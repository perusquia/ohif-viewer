import {
  SimpleDialog,
  OverlayTrigger,
  Tooltip,
  withTranslation,
} from '@ohif/ui';
import OHIF from '@ohif/core';
import classnames from 'classnames';
import _, { omit, isEqual, cloneDeep } from 'lodash';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import cornerstoneTools from 'cornerstone-tools';
import cornerstone from 'cornerstone-core';
import { updateComponentByPromise } from '../utils.js';
import Redux from '../../redux/index.js';
import {
  getDisplayProperties,
  applyDisplayProperties,
  check2dmprViewportPresent,
} from '../../utils/index.js';
import {
  Redux as FlywheelCommonRedux,
  Utils as FlywheelCommonUtils,
} from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';
import ConnectedInstructionContainer from './ui/InstructionSet/ConnectedInstructionContainer.js';
import {
  validateWhileQuestionSwitches,
  validateForm,
  draftStudyForm,
  bSliceRequiredValidation,
  errorSetting,
  getBSlices,
  sendTaskSignals,
  isFormViewOnly,
  isAnnotationsViewOnly,
  getMeasurementLabels,
  isOptionAvailable,
  bSliceQuestionValidation,
  getVisibleQuestions,
  isMeasurementTools,
} from './validation/ValidationEngine.js';
import {
  getCurrentOption,
  getToolsWithLabels,
  getAvailableTools,
  clickColorPicker,
  getAndSetBSliceValidatedInfo,
  getStudyFormActiveToolsForEnable,
  getStudyFormToolsForEnable,
  formatOptions,
  subFormEdit,
  updateAnnotationKeyAnswerAndNotes,
  updateAnnotationKeyAnswer,
  updateNoteInMixedOrROI,
  getQuestionAnswerAndTabDetails,
  getValueFromNoteAndSubFormName,
  isQuestionDisplay,
  sustainCurrentActiveTool,
  getAvailableLabels,
  getSubFormQuestionDetails,
  getDeletedMeasurementDetail,
  getSubFormTabDetails,
  updateStudyFormNoteSubFormQuestionDeleted,
  setNotes,
  getValue,
  getValueSubForm,
  selectQuestionValues,
  scrollCornerstoneViewport,
  updateSessionNotes,
  getAvailableLabelsForSet,
  getActiveTool,
  hasMeasurementTools,
  getReActivateQuestionDetails,
  getDailogIdAndDisplayLabelLimitReachedDailog,
} from './utils';
import './StudyForm.styl';
import './SubForm.styl';
import ColorPickerDialog from '../ColorPickerDialog/ColorPickerDialog.js';
import { Header } from './ui/StudyFormComponents/index.js';
import ui from './ui/index.js';
import StudyFormQuestionConfirmationDialog from './dialog/StudyFormQuestionConfirmationDialog.js';
import SubFormDialog from './dialog/SubFormDialog.js';

const {
  StudyFormContent,
  SubFormPanel,
  SubFormTabHeader,
  SubFormTabContent,
} = ui;
const {
  selectMeasurementsByStudyUid,
  selectSessionRead,
  selectStudyList,
  selectSessionFileRead,
  selectUser,
  selectReaderTask,
  hasPermission,
} = Redux.selectors;
const { setReaderTaskWithFormResponse, setReaderTask } = Redux.actions;
const {
  setActiveTool,
  setAvailableLabels,
  setActiveQuestion,
  updateRoiColors,
  updatePaletteColors,
  setMeasurementColorIntensity,
  subFormPopupVisible,
  setLabelForQuestion,
  subFormQuestionAnswered,
  studyFormAnswerInMixedMode,
  studyFormBSliceValidatedInfo,
  setStudyStateNote,
  setMultipleTaskNotes,
  setSubFormPopupClose,
  setWorkFlowTools,
  setPanelSwitched,
  setToolWhilePanelSwitch,
  setLimitReachedDialog,
} = FlywheelCommonRedux.actions;
const { selectActiveTool, measurementTools } = FlywheelCommonRedux.selectors;
const measurementApi = OHIF.measurements.MeasurementApi;
const { setMeasurementDeleted } = OHIF.redux.actions;
const {
  getRouteParams,
  isCurrentFile,
  isCurrentZipFile,
  getCondensedProjectConfig,
  hasActiveToolLimitReached,
  getMeasurementToolsForAnswer,
  getBSliceSettings,
  getLabelsForTool,
  getBSliceTools,
  getToolsAndLabelsForQuestionAnswer,
  validateAllBSlice,
  validateBSlice,
  cornerstoneUtils,
  shouldIgnoreUserSettings,
  getLabelsForToolWorkFlow,
  getSelectedOptionForQuestion,
  isEditModeForMultipleTask,
} = FlywheelCommonUtils;
const readStatus = {
  Todo: 'Todo',
  Inprogress: 'In_progress',
  Complete: 'Complete',
};
const DROPDOWN_DEFAULT_VALUE = '--- SELECT ---';
const DEFAULT_COLOR = 'rgba(255, 255, 0, 0.2)';
const ERASERTOOL = 'Eraser';

class StudyForm extends Component {
  static propTypes = {
    history: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
    measurementLabels: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.string),
      PropTypes.object,
    ]),
    showStudyFormButtonsOnly: PropTypes.bool,
    projectConfig: PropTypes.shape({
      allowDraft: PropTypes.bool,
      resetTool: PropTypes.bool,
      showStudyList: PropTypes.bool,
      questions: PropTypes.arrayOf(
        PropTypes.shape({
          key: PropTypes.string.isRequired,
          options: PropTypes.arrayOf(
            PropTypes.shape({
              label: PropTypes.string,
              value: PropTypes.string.isRequired,
              excludeMeasurements: PropTypes.arrayOf(PropTypes.string),
              requireMeasurements: PropTypes.arrayOf(PropTypes.string),
              require: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string)),
            })
          ),
          prompt: PropTypes.string.isRequired,
          required: PropTypes.bool,
          type: PropTypes.oneOf(['radio', 'text']).isRequired,
          when: PropTypes.object,
        })
      ),
      studyFormWorkflow: PropTypes.string,
      studyForm: PropTypes.shape({
        // https://github.com/formio/formio.js/wiki/Components-JSON-Schema
        components: PropTypes.arrayOf(
          PropTypes.shape({
            key: PropTypes.string.isRequired,
            label: PropTypes.string,
            defaultValue: PropTypes.string,
            values: PropTypes.arrayOf(
              PropTypes.shape({
                directive: PropTypes.string,
                instructionSet: PropTypes.array,
                label: PropTypes.string,
                value: PropTypes.string,
                excludeMeasurements: PropTypes.arrayOf(PropTypes.string),
                requireMeasurements: PropTypes.arrayOf(PropTypes.string),
              })
            ),
            validate: PropTypes.shape({
              required: PropTypes.bool,
              custom: PropTypes.object,
              minSelectedCount: PropTypes.number,
              maxSelectedCount: PropTypes.number,
            }),
            type: PropTypes.oneOf([
              'content',
              'radio',
              'text',
              'selectboxes',
              'textarea',
              'textfield',
              'dropdown',
            ]).isRequired,
            // This is for conditionally rendering this question.
            conditional: PropTypes.shape({
              // https://github.com/jwadhams/json-logic-js/
              json: PropTypes.object,
            }),
          })
        ),
        subForms: PropTypes.shape({
          subFormName: PropTypes.shape({
            label: PropTypes.string,
            components: PropTypes.arrayOf(
              PropTypes.shape({
                key: PropTypes.string.isRequired,
                label: PropTypes.string,
                defaultValue: PropTypes.string,
                values: PropTypes.arrayOf(
                  PropTypes.shape({
                    directive: PropTypes.string,
                    instructionSet: PropTypes.array,
                    label: PropTypes.string,
                    value: PropTypes.string,
                    excludeMeasurements: PropTypes.arrayOf(PropTypes.string),
                    requireMeasurements: PropTypes.arrayOf(PropTypes.string),
                  })
                ),
                validate: PropTypes.shape({
                  required: PropTypes.bool,
                  custom: PropTypes.object,
                  minSelectedCount: PropTypes.number,
                  maxSelectedCount: PropTypes.number,
                }),
                type: PropTypes.oneOf([
                  'content',
                  'radio',
                  'text',
                  'selectboxes',
                  'textarea',
                  'textfield',
                  'dropdown',
                ]).isRequired,
                // This is for conditionally rendering this question.
                conditional: PropTypes.shape({
                  // https://github.com/jwadhams/json-logic-js/
                  json: PropTypes.object,
                }),
              })
            ),
          }),
        }),
      }),
      bSlices: PropTypes.shape({
        settings: PropTypes.objectOf(
          PropTypes.shape({
            hiddenQuestions: PropTypes.arrayOf(PropTypes.string),
            measurementTools: PropTypes.objectOf(
              PropTypes.arrayOf(PropTypes.string)
            ),
          })
        ),
        required: PropTypes.shape({
          all: PropTypes.arrayOf(PropTypes.string),
          any: PropTypes.arrayOf(PropTypes.string),
          one: PropTypes.arrayOf(PropTypes.string),
          specific: PropTypes.objectOf(PropTypes.arrayOf(PropTypes.string)),
        }),
      }),
    }).isRequired,
    saveStudy: PropTypes.func.isRequired,
    showUserErrorMessage: PropTypes.func.isRequired,
    getReadTime: PropTypes.func.isRequired,
    sessionRead: PropTypes.shape({
      date: PropTypes.string, //isRequired, Todo enable when reader task supports saving date in info field
      notes: PropTypes.object,
      readOnly: PropTypes.bool,
      readStatus: PropTypes.string,
    }),
    setAvailableLabels: PropTypes.func.isRequired,
    StudyInstanceUID: PropTypes.string.isRequired,
    t: PropTypes.func.isRequired,
    servicesManager: PropTypes.object,
    commandsManager: PropTypes.object,
    sliceInfo: PropTypes.shape({
      imageId: PropTypes.string,
      sliceNumber: PropTypes.number,
      viewportIndex: PropTypes.number,
    }),
    setActiveTool: PropTypes.func,
  };

  state = {
    confirmCallback: null,
    errors: {},
    notes: {},
    readOnly: false,
    save: null,
    currentQuestion: null,
    previousQuestion: null,
    previousTool: 'Wwwc',
    isComplete: false,
    questionValidation: true,
    draftSave: null,
    submit: null,
    draftMessage: null,
    readerTaskStatus: null,
    readerTask: null,
    ownAnnotationPermission: false,
    hasEditAnnotationsOthersPermission: false,
    ownFormResponsesPermission: false,
    hasEditFormOthersPermission: false,
    formReadonly: false,
    currentSliceInfo: null,
    isStateUpdate: false,
    showInstructionColorPicker: {},
    selectedColor: DEFAULT_COLOR,
    showColorPicker: false,
    colorPickCoordinates: { left: 0, top: 0 },
    measurementData: null,
    currentType: null,
    currentKey: null,
    subFormPanel: {},
    subFormTab: {},
    subFormTabWithAnnotationId: {},
    subFormTabList: {},
    annotationIdForQuestionKey: {},
    isSubFormQuestion: false,
    subFormName: '',
    subFormQuestion: null,
    subFormAnnotationId: null,
    currentAvailableLabels: null,
    currentAvailableTools: null,
    currentTool: null,
    confirmationDialogRoiAnswer: [],
    answer: '',
    confirmationDialogMeasurementList: [],
    lastAnsweredValue: {},
    confirmationDialogCurrentQuestion: {},
    subFormDialogCurrentQuestion: {},
    subFormDialogMeasurementList: [],
    subFormDialogRoiAnswer: [],
    studyFormQuestionToolForSubForm: '',
    studyFormQuestionKey: null,
  };

  previousVisibleQuestion = null;
  scrollableRef = null;
  questionRefs = {};
  roiQuestionDialogId = null;
  roiSubFormDialogId = null;
  toolsWithLabels = {};

  async getReaderTask() {
    const readerTask = await selectReaderTask(store.getState());
    if (readerTask) {
      this.setState({
        readerTaskStatus: readerTask.status,
      });
    }
  }

  onMeasurementRemoved(event) {
    const state = store.getState();
    const isFile = isCurrentFile(state);
    const activeTool = state.infusions.activeTool;
    const { projectConfig, sliceInfo, measurementLabels } = this.props;
    const { UIDialogService } = this.props.servicesManager.services;
    const isStudyFormContainsMeasurementTools = projectConfig?.studyForm?.components?.some(
      isMeasurementTools
    );
    const slice = sliceInfo.sliceNumber;
    const lastDeletedMeasurementId = event.detail.measurementData.uuid;
    const deletedMeasurement = event.detail.measurementData;
    let measurementLabelsDetails;
    let questionTools;

    if (
      !deletedMeasurement.isSubForm &&
      deletedMeasurement.location &&
      this.roiSubFormDialogId
    ) {
      UIDialogService.dismiss({ id: this.roiSubFormDialogId });
      this.roiSubFormDialogId = null;
    }

    if (
      !deletedMeasurement?.isSubForm &&
      deletedMeasurement.location &&
      this.roiQuestionDialogId
    ) {
      UIDialogService.dismiss({ id: this.roiQuestionDialogId });
      this.roiQuestionDialogId = null;
    }

    if (
      (projectConfig.studyFormWorkflow === 'ROI' &&
        isStudyFormContainsMeasurementTools &&
        (!deletedMeasurement.isSubForm || !this.roiSubFormDialogId)) ||
      (projectConfig.studyFormWorkflow === 'Mixed' &&
        isStudyFormContainsMeasurementTools &&
        !deletedMeasurement.isSubForm)
    ) {
      questionTools = getStudyFormActiveToolsForEnable(
        projectConfig,
        this.props,
        null,
        this.toolsWithLabels
      );
      measurementLabelsDetails = { ...measurementLabels };
      let allLabels;

      if (
        projectConfig.bSlices?.settings &&
        deletedMeasurement.sliceNumber &&
        deletedMeasurement.location
      ) {
        allLabels =
          measurementLabelsDetails.slices[deletedMeasurement.sliceNumber]
            ?.component;
        const index = allLabels?.findIndex(
          label => label === deletedMeasurement.location
        );
        if (allLabels) {
          delete allLabels[index];
        }
      } else if (!projectConfig.bSlices && deletedMeasurement.location) {
        allLabels = measurementLabelsDetails?.component;
        const index = allLabels?.findIndex(
          label => label === deletedMeasurement.location
        );
        if (allLabels) {
          delete allLabels[index];
        }
      }

      questionTools = getAvailableTools(
        questionTools,
        projectConfig,
        this.toolsWithLabels,
        measurementLabelsDetails,
        slice
      );
      this.props.setAvailableLabels([''], questionTools);
    }

    if (
      !deletedMeasurement.isSubForm && projectConfig.bSlices && !isFile
        ? this.state.annotationIdForQuestionKey[
            deletedMeasurement?.sliceNumber
          ]?.[deletedMeasurement?.questionKey]?.includes(
            lastDeletedMeasurementId
          )
        : this.state.annotationIdForQuestionKey[
            deletedMeasurement?.questionKey
          ]?.includes(lastDeletedMeasurementId)
    ) {
      let studyNotes = JSON.parse(JSON.stringify(this.state.notes));
      studyNotes = updateStudyFormNoteSubFormQuestionDeleted(
        projectConfig,
        deletedMeasurement,
        lastDeletedMeasurementId,
        studyNotes
      );
      const componentKeyListDelete = projectConfig.studyForm?.components?.map(
        component => component.key
      );
      const tabDelete = getSubFormTabDetails(
        componentKeyListDelete,
        projectConfig,
        measurementTools,
        this.props.measurements,
        this.state
      );
      this.setState(prevState => ({
        ...prevState,
        notes: { ...studyNotes },
        subFormTabList: { ...tabDelete.subFormTabList },
        subFormPanel: { ...tabDelete.subFormPanel },
        subFormTab: { ...tabDelete.subFormTab },
        subFormTabWithAnnotationId: {
          ...tabDelete.subFormTabWithAnnotationId,
        },
        annotationIdForQuestionKey: {
          ...tabDelete.annotationIdForQuestionKey,
        },
      }));
    }

    const visibleQuestions = getVisibleQuestions(this.state, this.props);
    const notes = this.state.notes;

    let isNoteForAnnotation = false;
    const notesForQn =
      !isFile && projectConfig.bSlices
        ? notes.slices[deletedMeasurement.sliceNumber]
        : notes;
    if (deletedMeasurement.isSubForm) {
      const annotations =
        notesForQn?.[deletedMeasurement.question]?.[
          deletedMeasurement.subFormName
        ];
      const matchedAnnotation = annotations?.find(
        annotation =>
          annotation.annotationId === deletedMeasurement.subFormAnnotationId
      );
      if (
        matchedAnnotation &&
        matchedAnnotation?.[deletedMeasurement.questionKey]
      ) {
        isNoteForAnnotation = true;
      }
    } else if (
      (!deletedMeasurement.isSubForm &&
        !Array.isArray(notesForQn?.[deletedMeasurement.questionKey]) &&
        typeof notesForQn?.[deletedMeasurement.questionKey] === 'object' &&
        notesForQn?.[deletedMeasurement.questionKey]?.value) ||
      (Array.isArray(notesForQn?.[deletedMeasurement.questionKey]) &&
        notesForQn?.[deletedMeasurement.questionKey]?.length) ||
      typeof notesForQn?.[deletedMeasurement.questionKey] === 'string'
    ) {
      isNoteForAnnotation = true;
    }

    if (isNoteForAnnotation) {
      const questionDetails = getReActivateQuestionDetails(
        deletedMeasurement,
        visibleQuestions,
        isFile,
        projectConfig,
        notes,
        isStudyFormContainsMeasurementTools
      );

      if (questionDetails?.deletedQuestion) {
        questionDetails.deletedQuestion.isSubForm =
          deletedMeasurement?.isSubForm;
        this.setState({ currentQuestion: questionDetails.deletedQuestion });
        const toolList = questionDetails.tools;

        if (
          (projectConfig.studyFormWorkflow === 'ROI' ||
            projectConfig.studyFormWorkflow === 'Mixed') &&
          questionTools?.length
        ) {
          toolList.push(...questionTools);
        }

        if (activeTool !== ERASERTOOL) {
          this.props.setAvailableLabels(questionDetails.location, toolList);
          this.props.setActiveTool(questionDetails.toolType);
        }

        if (
          (projectConfig.studyFormWorkflow === 'ROI' ||
            projectConfig.studyFormWorkflow === 'Mixed') &&
          deletedMeasurement?.isSubForm &&
          this.roiSubFormDialogId
        ) {
          const question = projectConfig.studyForm?.components?.find(
            component => component.key === deletedMeasurement?.question
          );
          const deletedAnswer = deletedMeasurement?.answer;
          const lastAnsweredValue = question?.values?.find(
            value => value.value === deletedAnswer
          );
          const allMeasurements = state.timepointManager.measurements;
          const measurementList = [];

          if (lastAnsweredValue) {
            Object.keys(allMeasurements).forEach(key => {
              const annotation = allMeasurements[key]?.find(
                keyAnnotation =>
                  keyAnnotation.uuid === deletedMeasurement?.subFormAnnotationId
              );
              if (annotation) {
                measurementList.push(annotation);
              }
            });
          }
          UIDialogService.dismiss({ id: this.roiSubFormDialogId });
          this.roiSubFormDialogId = null;

          this.addOREditSubForm(
            projectConfig,
            question,
            measurementList,
            lastAnsweredValue,
            this.state.notes
          );
        }
      }
    }

    store.dispatch(setMeasurementDeleted(false));
  }

  subscribeMeasurementRemoved(element, isAdd) {
    element.removeEventListener(
      cornerstoneTools.EVENTS.MEASUREMENT_REMOVED,
      this._onMeasurementRemoved
    );
    if (isAdd) {
      element.addEventListener(
        cornerstoneTools.EVENTS.MEASUREMENT_REMOVED,
        this._onMeasurementRemoved
      );
    }
  }

  elementEnabledHandler(evt) {
    const element = evt?.detail?.element;
    if (element) {
      this.subscribeMeasurementRemoved(element, true);
    }
  }

  elementDisabledHandler(evt) {
    const element = evt?.detail?.element;
    if (element) {
      this.subscribeMeasurementRemoved(element, false);
    }
  }

  labelApplied(evt) {
    const type = evt?.type;
    const { projectConfig } = this.props;
    if (
      type === 'labelApplied' &&
      (projectConfig.studyFormWorkflow === 'ROI' ||
        projectConfig.studyFormWorkflow === 'Mixed')
    ) {
      const deletedMeasurement = evt.detail.measurementData;

      if (
        deletedMeasurement.isSubForm === false &&
        deletedMeasurement.location
      ) {
        const allTools = getStudyFormToolsForEnable(
          this.props,
          this.toolsWithLabels
        );
        this.props.setAvailableLabels([''], allTools);
      }
    }
  }

  panelClick(evt) {
    const type = evt?.type;
    if (type === 'annotationPanel' || type === 'formsPanel') {
      this.panelSwitched();
    }
  }

  formatMeasurementList(allMeasurements, annotationId) {
    const measurementList = [];
    Object.keys(allMeasurements).forEach(key => {
      const annotation = allMeasurements[key]?.find(
        keyAnnotation => keyAnnotation.uuid === annotationId
      );
      if (annotation) {
        measurementList.push(annotation);
      }
    });
    return measurementList || [];
  }

  panelSwitched = () => {
    const { projectConfig } = this.props;
    const state = store.getState();
    const isPanelSwitched = state.infusions?.isPanelSwitched;
    const timePointManager = state.timepointManager;
    if (isPanelSwitched) {
      const currentSelectedQuestion = state.infusions?.currentSelectedQuestion;
      const subFormDialogData = state.infusions?.subFormDialogData;

      if (
        currentSelectedQuestion?.isSubForm &&
        state.infusions?.subFormDialog
      ) {
        const qKey = currentSelectedQuestion.question;
        const question = projectConfig.studyForm?.components?.find(
          component => component.key === qKey
        );
        const lastAnsweredValue = subFormDialogData.subFormDialogRoiAnswer;
        const allMeasurements = timePointManager.measurements;
        let measurementList = [];

        if (lastAnsweredValue) {
          measurementList = this.formatMeasurementList(
            allMeasurements,
            currentSelectedQuestion?.subFormAnnotationId
          );
        }

        if (measurementList.length > 0 && currentSelectedQuestion) {
          this.addOREditSubForm(
            projectConfig,
            question,
            measurementList,
            lastAnsweredValue,
            this.state.notes
          );
        }
      } else if (
        (!currentSelectedQuestion || !currentSelectedQuestion?.isSubForm) &&
        state.infusions?.subFormDialog
      ) {
        this.addOREditSubForm(
          projectConfig,
          subFormDialogData.subFormDialogCurrentQuestion,
          subFormDialogData.subFormDialogMeasurementList,
          subFormDialogData.subFormDialogRoiAnswer,
          this.state.notes
        );
      }

      if (state.infusions?.confirmationDialog) {
        const confirmationDialogData = state.infusions?.confirmationDialogData;
        const studyFormNotes = this.state.notes;
        this.confirmationDialogShow(
          confirmationDialogData.currentQuestion,
          confirmationDialogData.confirmationDialogMeasurementList,
          this.props.projectConfig,
          confirmationDialogData.lastAnsweredValue,
          confirmationDialogData.sliceInfo?.sliceNumber,
          confirmationDialogData.confirmationDialogRoiAnswer,
          studyFormNotes,
          confirmationDialogData.sliceInfo,
          confirmationDialogData.answer
        );
      }

      const panel = {
        isPanelSwitched: false,
        confirmationDialog: false,
        subFormDialog: false,
        confirmationDialogData: state.infusions?.confirmationDialogData,
        subFormDialogData: state.infusions?.subFormDialogData,
      };
      store.dispatch(setPanelSwitched(panel));
    }
  };

  deleteDialog(evt) {
    const type = evt?.type;
    const { UIDialogService } = this.props.servicesManager.services;
    if (type === 'deleteDialog') {
      UIDialogService.dismiss({ id: this.roiQuestionDialogId });
      this.roiQuestionDialogId = null;
      UIDialogService.dismiss({ id: this.roiSubFormDialogId });
      this.roiSubFormDialogId = null;
    }
  }

  componentDidMount() {
    this._elementEnabledHandler = this.elementEnabledHandler.bind(this);
    this._elementDisabledHandler = this.elementDisabledHandler.bind(this);
    this._onMeasurementRemoved = this.onMeasurementRemoved.bind(this);
    const enabledElements = cornerstone.getEnabledElements();
    enabledElements.forEach(enabledElement => {
      this.subscribeMeasurementRemoved(enabledElement.element, true);
    });

    cornerstone.events.addEventListener(
      cornerstone.EVENTS.ELEMENT_ENABLED,
      this._elementEnabledHandler
    );

    cornerstone.events.addEventListener(
      cornerstone.EVENTS.ELEMENT_DISABLED,
      this._elementDisabledHandler
    );

    this._panelClick = this.panelClick.bind(this);

    document.addEventListener('annotationPanel', this._panelClick);

    document.addEventListener('formsPanel', this._panelClick);

    this._labelApplied = this.labelApplied.bind(this);

    document.addEventListener('labelApplied', this._labelApplied);

    this._deleteDialog = this.deleteDialog.bind(this);

    document.addEventListener('deleteDialog', this._deleteDialog);

    this.getReaderTask();
    const { projectConfig, sessionRead } = this.props;
    const state = store.getState();
    const isPanelSwitched = state.infusions?.isPanelSwitched;
    if (sessionRead) {
      const notes = sessionRead.notes;
      const currentSelectedQuestion = state.infusions?.currentSelectedQuestion;
      if (isPanelSwitched && currentSelectedQuestion) {
        this.setState(prevState => ({
          ...prevState,
          notes: { ...notes } || {},
          readOnly: sessionRead.readOnly || false,
          isSubFormQuestion: currentSelectedQuestion.isSubForm,
          subFormName: currentSelectedQuestion.subFormName,
          subFormQuestion: currentSelectedQuestion.question,
          subFormAnnotationId: currentSelectedQuestion.subFormAnnotationId,
          currentKey:
            !currentSelectedQuestion.isSubForm && currentSelectedQuestion.key,
          currentQuestion:
            !currentSelectedQuestion.isSubForm && currentSelectedQuestion,
        }));
      } else {
        this.setState(prevState => ({
          ...prevState,
          notes: { ...notes } || {},
          readOnly: sessionRead.readOnly || false,
        }));
      }

      const confirmationDialogData = state.infusions?.confirmationDialogData;
      const subFormDialogData = state.infusions?.subFormDialogData;

      if (Object.keys(subFormDialogData)?.length) {
        this.setState(prevState => ({
          ...prevState,
          subFormDialogCurrentQuestion:
            subFormDialogData.subFormDialogCurrentQuestion,
          subFormDialogRoiAnswer: subFormDialogData.subFormDialogRoiAnswer,
          subFormDialogMeasurementList:
            subFormDialogData.subFormDialogMeasurementList,
          currentQuestion: subFormDialogData.subFormDialogQuestion,
          currentSliceInfo: subFormDialogData.sliceInfo,
        }));
      }

      if (Object.keys(confirmationDialogData)?.length) {
        this.setState(prevState => ({
          ...prevState,
          confirmationDialogRoiAnswer:
            confirmationDialogData.confirmationDialogRoiAnswer,
          confirmationDialogMeasurementList:
            confirmationDialogData.confirmationDialogMeasurementList,
          confirmationDialogCurrentQuestion:
            confirmationDialogData.currentQuestion,
          lastAnsweredValue: confirmationDialogData.lastAnsweredValue,
          answer: confirmationDialogData.answer,
          currentQuestion: confirmationDialogData.currentQuestion,
          currentSliceInfo: confirmationDialogData.sliceInfo,
        }));
      }

      if (projectConfig.studyFormWorkflow === 'Form') {
        const componentKeyList = projectConfig.studyForm?.components?.map(
          component => component.key
        );
        const tab = getSubFormTabDetails(
          componentKeyList,
          projectConfig,
          measurementTools,
          this.props.measurements,
          this.state
        );
        this.setState(prevState => ({
          ...prevState,
          subFormTabList: { ...tab.subFormTabList },
          subFormPanel: { ...tab.subFormPanel },
          subFormTab: { ...tab.subFormTab },
          annotationIdForQuestionKey: { ...tab.annotationIdForQuestionKey },
        }));
      }
    }
    const currentSelectedQuestion = state.infusions.currentSelectedQuestion;
    if (currentSelectedQuestion && !isPanelSwitched) {
      const option = getCurrentOption(currentSelectedQuestion);
      const subFormQuestionKey = currentSelectedQuestion.isSubForm
        ? currentSelectedQuestion.questionKey
        : currentSelectedQuestion.question;
      const studyFormQuestionKey = currentSelectedQuestion.isSubForm
        ? currentSelectedQuestion.question
        : currentSelectedQuestion.questionKey;
      this.clickType(
        option,
        studyFormQuestionKey,
        subFormQuestionKey,
        currentSelectedQuestion.subFormName,
        currentSelectedQuestion.subFormAnnotationId,
        false,
        false
      );
    }

    if (projectConfig.resetTool && !isPanelSwitched) {
      this.props.setActiveTool('Wwwc');
    }

    this.setReaderTaskAndPermissions();

    if (
      !isCurrentFile(state) &&
      projectConfig.bSlices?.settings &&
      !isPanelSwitched
    ) {
      this.props.setAvailableLabels([], []);
      this.props.setActiveTool();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const {
      measurementLabels,
      projectConfig,
      sessionRead,
      StudyInstanceUID,
      sliceInfo,
    } = this.props;
    let slice = 1;
    const state = store.getState();
    const isPanelSwitched = state.infusions?.isPanelSwitched;
    const isFile = isCurrentFile(state);
    const timePointManager = state.timepointManager;
    const { UIDialogService } = this.props.servicesManager.services;
    const isStudyFormContainsMeasurementTools = projectConfig?.studyForm?.components?.some(
      isMeasurementTools
    );
    const isSubFormEnabled =
      this.roiQuestionDialogId || this.roiSubFormDialogId ? true : false;
    const toolsWithLabel = getToolsWithLabels(
      projectConfig.studyForm.components,
      isSubFormEnabled
    );

    if (!Object.keys(this.toolsWithLabels).length) {
      this.toolsWithLabels = toolsWithLabel;
    }

    if (
      sessionRead?.displayProperties &&
      !prevProps.sessionRead?.displayProperties
    ) {
      const shouldIgnore = shouldIgnoreUserSettings(state);
      if (!shouldIgnore) {
        applyDisplayProperties(
          this.props.commandsManager,
          state,
          sessionRead?.displayProperties
        );
      }

      if (state.flywheel.readerTaskWithFormResponse) {
        const readerTaskWithFormResponse = _.cloneDeep(
          state.flywheel.readerTaskWithFormResponse
        );
        if (!shouldIgnore) {
          delete readerTaskWithFormResponse.info.displayProperties;
        }
        store.dispatch(
          setReaderTaskWithFormResponse(readerTaskWithFormResponse)
        );
      }

      if (this.state.readerTask) {
        const readerTask = _.cloneDeep(this.state.readerTask);
        if (readerTask?.info?.displayProperties) {
          if (!shouldIgnore) {
            delete readerTask.info.displayProperties;
          }
          store.dispatch(setReaderTask(Promise.resolve(readerTask)));
        }
      }
    }

    if (
      !isFile &&
      this.state.currentSliceInfo &&
      sliceInfo &&
      this.state.currentSliceInfo?.imageId !== sliceInfo?.imageId &&
      projectConfig.bSlices
    ) {
      if (this.roiQuestionDialogId) {
        UIDialogService.dismiss({ id: this.roiQuestionDialogId });
        this.roiQuestionDialogId = null;
      } else if (this.roiSubFormDialogId) {
        UIDialogService.dismiss({ id: this.roiSubFormDialogId });
        this.roiSubFormDialogId = null;
      }

      if (state.subForm.isSubFormPopupEnabled) {
        store.dispatch(subFormPopupVisible(false));
      }

      if (state.infusions?.currentSelectedQuestion?.isSubForm) {
        this.setState({ currentQuestion: null, currentKey: null });
        store.dispatch(
          setActiveQuestion({
            isSubForm: true,
          })
        );
      }
    }

    if (
      !this.state.currentSliceInfo ||
      this.state.currentSliceInfo?.imageId !== sliceInfo?.imageId
    ) {
      const sliceNumber = sliceInfo?.sliceNumber;
      if (sliceNumber > 0) {
        this.setState({
          currentSliceInfo: {
            imageId: sliceInfo.imageId,
            sliceNumber,
            viewportIndex: sliceInfo.viewportIndex,
          },
        });
      }
      slice = sliceNumber;
    } else {
      slice = this.state.currentSliceInfo.sliceNumber;
    }

    getAvailableLabelsForSet(
      this.state,
      this.props,
      state,
      this.toolsWithLabels,
      isFile,
      slice,
      isStudyFormContainsMeasurementTools,
      isPanelSwitched,
      prevProps
    );

    const singleAvatar = state.viewerAvatar.singleAvatar;
    const isStudyStateNoteUpdate = state.viewerAvatar.isStudyStateNoteUpdate;
    const singleTaskId = state.viewerAvatar.taskId;
    const multipleTaskNotes = { ...state.viewerAvatar.multipleTaskNotes };
    const isMultipleReaderTask = state.multipleReaderTask?.TaskIds?.length > 1;

    if (
      !this.state.isStateUpdate &&
      isStudyStateNoteUpdate &&
      isMultipleReaderTask
    ) {
      let note;
      if (
        singleAvatar &&
        state.flywheel.readerTaskWithFormResponse?._id ===
          state.viewerAvatar.taskId
      ) {
        note = { ...state.flywheel.formResponse?.response_data };
      } else {
        note = { ...state.flywheel.readerTaskWithFormResponse?.info?.notes };
        store.dispatch(setActiveQuestion(null));
        this.setState({ currentQuestion: null });
      }
      this.setState({ notes: { ...note }, currentQuestion: null });
      store.dispatch(setStudyStateNote(false));
      return;
    }

    if (
      !this.state.isStateUpdate &&
      singleAvatar &&
      isMultipleReaderTask &&
      !isEqual(multipleTaskNotes[singleTaskId]?.notes?.[0], this.state.notes)
    ) {
      let noteForAvatar;
      if (isStudyStateNoteUpdate) {
        noteForAvatar = { ...state.flywheel.formResponse?.response_data };
      } else {
        noteForAvatar = { ...this.state.notes };
      }
      if (
        singleTaskId === this.state.readerTask?._id &&
        multipleTaskNotes[singleTaskId]
      ) {
        multipleTaskNotes[singleTaskId].notes[0] = noteForAvatar;
        store.dispatch(setMultipleTaskNotes(multipleTaskNotes));
        this.setState({ notes: noteForAvatar });
      }
    }

    if (
      (StudyInstanceUID !== prevProps.StudyInstanceUID ||
        !isEqual(sessionRead, prevProps.sessionRead)) &&
      !this.state.isStateUpdate &&
      !isEqual(sessionRead?.notes, this.state.notes)
    ) {
      let sessionReadNotes = cloneDeep(
        (sessionRead && sessionRead.notes) || {}
      );
      const readOnly =
        (sessionRead && sessionRead.readOnly) || this.state.readOnly;
      if (!isFile && projectConfig.bSlices && this.state.notes.slices) {
        const bSlices = getBSlices(projectConfig);
        for (const sliceNo of bSlices) {
          if (sessionReadNotes.slices) {
            sessionReadNotes.slices[sliceNo] = {
              ...sessionReadNotes.slices?.[sliceNo],
              ...this.state.notes.slices?.[sliceNo],
            };
          }
        }
      } else {
        sessionReadNotes = {
          ...sessionReadNotes,
          ...this.state.notes,
        };
      }
      let formNotes = sessionReadNotes;
      const viewerModeNotes = this.getViewerModeResponse();
      if (viewerModeNotes) {
        formNotes = { ...viewerModeNotes };
      }
      this.setState({ notes: { ...formNotes }, readOnly, errors: {} });
      if (
        this.scrollableRef &&
        StudyInstanceUID !== prevProps.StudyInstanceUID
      ) {
        this.scrollableRef.scrollTop = 0;
      }
    }
    if (measurementLabels !== prevProps.measurementLabels) {
      let notes = {};
      const props = {
        ...this.props,
        previousVisibleQuestion: this.state.previousQuestion,
        sliceInfo: this.props.sliceInfo,
      };
      const visibleQuestions = getVisibleQuestions(this.state, props);
      for (const question of visibleQuestions) {
        if (question.values) {
          const currentValue = this.state.notes.slices
            ? this.state.notes.slices[slice]
              ? this.state.notes.slices[slice][question.key]
              : undefined
            : this.state.notes[question.key];
          const validOptions = question.values.filter(opt =>
            isOptionAvailable(
              opt,
              measurementLabels,
              projectConfig,
              slice,
              false,
              this.state
            )
          );

          if (
            typeof currentValue === 'object' &&
            !validOptions.some(opt => {
              if (Array.isArray(currentValue.value)) {
                return currentValue.value.includes(opt.value);
              }
              return opt.value === currentValue.value;
            })
          ) {
            notes = setNotes(
              validOptions,
              state,
              projectConfig,
              notes,
              slice,
              question,
              currentValue
            );
          } else if (
            currentValue &&
            typeof currentValue !== 'object' &&
            !validOptions.some(opt => {
              if (Array.isArray(currentValue)) {
                return currentValue.includes(opt.value);
              }
              return opt.value === currentValue;
            })
          ) {
            notes = setNotes(
              validOptions,
              state,
              projectConfig,
              notes,
              slice,
              question,
              currentValue
            );
          }
        }
      }
      if (Object.keys(notes).length > 0) {
        if (!isFile && projectConfig.bSlices) {
          const stateNotes =
            this.state.notes.slices && notes.slices
              ? {
                  ...this.state.notes.slices[slice],
                  ...notes.slices[slice],
                }
              : this.state.notes.slices
              ? {
                  ...this.state.notes.slices[slice],
                }
              : notes.slices
              ? {
                  ...notes.slices[slice],
                }
              : {};
          let allNotes = { ...this.state.notes };
          if (!allNotes.slices) {
            allNotes.slices = {};
          }
          allNotes.slices[slice] = stateNotes;
          let formNotes = allNotes;
          const viewerModeNotes = this.getViewerModeResponse();
          if (viewerModeNotes) {
            formNotes = { ...viewerModeNotes };
          }
          this.setState({ notes: { ...formNotes } });
        } else {
          let formNotes = { ...this.state.notes, ...notes };
          const viewerModeNotes = this.getViewerModeResponse();
          if (viewerModeNotes) {
            formNotes = { ...viewerModeNotes };
          }
          this.setState({ notes: { ...formNotes } });
        }
      }
    }
    if (
      !isEqual(this.state.notes, prevState.notes) ||
      measurementLabels !== prevProps.measurementLabels
    ) {
      const componentKeyList = projectConfig.studyForm?.components?.map(
        component => component.key
      );
      const tab = getSubFormTabDetails(
        componentKeyList,
        projectConfig,
        measurementTools,
        this.props.measurements,
        this.state
      );
      let formNotes = this.state.notes;
      const viewerModeNotes = this.getViewerModeResponse();
      if (viewerModeNotes) {
        formNotes = { ...viewerModeNotes };
      }
      this.setState({
        ...this.state,
        notes: formNotes,
        subFormTabList: { ...tab.subFormTabList },
        subFormPanel: { ...tab.subFormPanel },
        subFormTab: { ...tab.subFormTab },
        subFormTabWithAnnotationId: { ...tab.subFormTabWithAnnotationId },
        annotationIdForQuestionKey: { ...tab.annotationIdForQuestionKey },
      });
    }

    if (
      StudyInstanceUID !== prevProps.StudyInstanceUID &&
      projectConfig.resetTool
    ) {
      this.props.setActiveTool('Wwwc');
    }

    if (
      state.subForm?.isSubFormPopupCloseFromOverlappingDelete ||
      state.subForm?.isSubFormPopupCloseFromOverlappingAutoAdjust
    ) {
      const data = { delete: false, autoAdjust: false };
      store.dispatch(setSubFormPopupClose(data));
      if (state.subForm?.isSubFormPopupCloseFromOverlappingDelete) {
        if (this.roiQuestionDialogId) {
          UIDialogService.dismiss({ id: this.roiQuestionDialogId });
          this.roiQuestionDialogId = null;
        } else if (this.roiSubFormDialogId) {
          UIDialogService.dismiss({ id: this.roiSubFormDialogId });
          this.roiSubFormDialogId = null;
        }
        const formQuestionTools = getStudyFormActiveToolsForEnable(
          projectConfig,
          this.props,
          null,
          this.toolsWithLabels
        );
        const label = formQuestionTools?.length ? [''] : null;
        let toolsList = formQuestionTools?.length ? formQuestionTools : null;
        toolsList = getAvailableTools(
          toolsList,
          projectConfig,
          this.toolsWithLabels,
          measurementLabels,
          slice
        );
        this.props.setAvailableLabels(label, toolsList);
      }
    }

    if (
      (projectConfig?.studyFormWorkflow === 'ROI' ||
        projectConfig?.studyFormWorkflow === 'Mixed') &&
      state.subForm?.isROIEdit
    ) {
      const edit = subFormEdit(
        state,
        projectConfig,
        this.props,
        state.subForm?.measurement?.uuid
      );
      this.addOREditSubForm(
        projectConfig,
        edit.roiSelectedQuestion,
        [edit.measurementEdit],
        edit.lastAnsweredValue
      );
    }

    if (
      (projectConfig?.studyFormWorkflow === 'ROI' ||
        projectConfig?.studyFormWorkflow === 'Mixed') &&
      state.ui?.labelling?.measurementData &&
      !state.ui?.labelling?.editLabelDescriptionOnDialog &&
      !timePointManager.isMeasurementDeletedForStudyFormQuestion
    ) {
      const measurementData = state.ui.labelling?.measurementData;
      const isInTimePointManager = state.timepointManager?.measurements?.[
        measurementData.toolType
      ]?.find(data => data._id === measurementData._id);

      if (
        measurementData.location &&
        measurementData.toolType &&
        isInTimePointManager
      ) {
        const roiQuestionList = projectConfig.studyForm?.components?.filter(
          item => {
            return item?.values?.some(
              value =>
                value.measurementTools?.includes(measurementData.toolType) &&
                value.requireMeasurements?.includes(measurementData.location)
            )
              ? item
              : null;
          }
        );
        let roiQuestion = [];

        if (roiQuestionList.length) {
          roiQuestion = roiQuestionList[0];
          const roiAnswer = roiQuestion.values.filter(
            value =>
              value.measurementTools?.includes(measurementData.toolType) &&
              value.requireMeasurements?.includes(measurementData.location)
          );
          let studyFormNotes = JSON.parse(JSON.stringify(this.state.notes));
          const stateAnswer = this.state.notes.slices
            ? this.state.notes.slices?.[slice]?.[roiQuestion.key]
            : this.state.notes?.[roiQuestion.key];
          const formQuestionAnswer =
            typeof stateAnswer === 'object' ? stateAnswer.value : stateAnswer;
          let allLabelApplied = true;
          roiAnswer.forEach(item => {
            item.measurementTools.forEach(tool => {
              item.requireMeasurements.forEach(label => {
                if (allLabelApplied) {
                  allLabelApplied =
                    !isFile && projectConfig.bSlices
                      ? this.props.measurements[tool]?.some(
                          measurement =>
                            measurement.location === label &&
                            measurement.questionKey === roiQuestion.key &&
                            measurement.sliceNumber === slice &&
                            measurement.toolType === tool
                        )
                      : this.props.measurements[tool]?.some(
                          measurement =>
                            measurement.location === label &&
                            measurement.questionKey === roiQuestion.key &&
                            measurement.toolType === tool
                        );
                }
              });
            });
          });

          if (
            roiQuestion.values &&
            roiAnswer.length > 1 &&
            (!roiQuestion?.positiveAnswers?.includes(formQuestionAnswer) ||
              !allLabelApplied)
          ) {
            if (this.roiQuestionDialogId) {
              return;
            }

            // Update all measurements by measurement number
            const measurementList = measurementApi.Instance.tools[
              measurementData.toolType
            ]?.filter(
              m => m.measurementNumber === measurementData.measurementNumber
            );
            let lastAnsweredValue;

            if (
              state.subForm?.isLabelApplied &&
              (projectConfig?.studyFormWorkflow === 'ROI' ||
                projectConfig?.studyFormWorkflow === 'Mixed')
            ) {
              store.dispatch(setLabelForQuestion(false));
              let annotationAnswer = roiAnswer[0]?.value;
              const answerHasSubForm = roiAnswer?.some(
                answer => answer.value === formQuestionAnswer
              );

              if (formQuestionAnswer && answerHasSubForm) {
                annotationAnswer = roiAnswer.find(
                  data => data.value === formQuestionAnswer
                )?.value;
                updateAnnotationKeyAnswer(
                  measurementList,
                  roiQuestion,
                  annotationAnswer
                );
                let annotationAnswerData = roiAnswer.filter(
                  data => data.value === formQuestionAnswer
                );
                const note = updateNoteInMixedOrROI(
                  projectConfig,
                  studyFormNotes,
                  sliceInfo,
                  roiQuestion,
                  annotationAnswerData
                );
                studyFormNotes = note;
              }

              if (
                projectConfig?.studyFormWorkflow === 'ROI' ||
                projectConfig?.studyFormWorkflow === 'Mixed'
              ) {
                this.setSubFormTabDetails(projectConfig, studyFormNotes, true);
              } else {
                this.setSubFormTabDetails(projectConfig, studyFormNotes, false);
              }
            }

            if (!roiQuestion?.positiveAnswers?.includes(formQuestionAnswer)) {
              this.setState({
                confirmationDialogCurrentQuestion: roiQuestion,
                confirmationDialogRoiAnswer: roiAnswer,
                confirmationDialogMeasurementList: measurementList,
              });
              this.confirmationDialogShow(
                roiQuestion,
                measurementList,
                projectConfig,
                lastAnsweredValue,
                slice,
                roiAnswer,
                studyFormNotes,
                sliceInfo
              );
            } else if (
              roiQuestion?.positiveAnswers?.includes(formQuestionAnswer) &&
              !this.roiSubFormDialogId
            ) {
              let annotationAnswer = roiAnswer[0];
              if (formQuestionAnswer) {
                annotationAnswer = roiAnswer.find(
                  data => data.value === formQuestionAnswer
                );
              }

              lastAnsweredValue = annotationAnswer;

              if (!state.infusions.isRelabel) {
                this.addOREditSubForm(
                  projectConfig,
                  roiQuestion,
                  measurementList,
                  lastAnsweredValue,
                  studyFormNotes
                );
              }
            }
          } else if (
            roiQuestion.values &&
            roiAnswer.length === 1 &&
            (!roiQuestion?.positiveAnswers?.includes(formQuestionAnswer) ||
              !allLabelApplied)
          ) {
            if (this.roiSubFormDialogId) {
              return;
            }

            let lastAnsweredValue;
            let measurementsList = [];

            if (state.subForm?.isLabelApplied) {
              store.dispatch(setLabelForQuestion(false));
              // Update all measurements by measurement number
              measurementsList = measurementApi.Instance.tools[
                measurementData.toolType
              ]?.filter(
                m => m.measurementNumber === measurementData.measurementNumber
              );
              updateAnnotationKeyAnswer(
                measurementsList,
                roiQuestion,
                roiAnswer[0].value
              );
              this.setSubFormTabDetails(projectConfig, studyFormNotes, false);
              const note = updateNoteInMixedOrROI(
                projectConfig,
                studyFormNotes,
                sliceInfo,
                roiQuestion,
                roiAnswer
              );
              studyFormNotes = note;

              if (projectConfig?.studyFormWorkflow === 'Mixed') {
                this.setSubFormTabDetails(projectConfig, studyFormNotes, true);
              } else {
                let formNotes = studyFormNotes;
                const viewerModeNotes = this.getViewerModeResponse();
                if (viewerModeNotes) {
                  formNotes = { ...viewerModeNotes };
                }
                this.setState(prevState => ({
                  ...prevState,
                  notes: formNotes,
                }));
              }

              lastAnsweredValue = roiAnswer[0];
              setTimeout(() => {
                this.saveWhileQuestionSwitches();
              }, 1);
            }
            if (
              roiAnswer[0].subForm &&
              projectConfig.studyForm?.subForms?.[roiAnswer[0].subForm]
                ?.components
            ) {
              this.addOREditSubForm(
                projectConfig,
                roiQuestion,
                measurementsList,
                lastAnsweredValue,
                studyFormNotes
              );
            }
          } else if (
            roiQuestion?.positiveAnswers?.includes(formQuestionAnswer) &&
            (projectConfig?.studyFormWorkflow === 'ROI' ||
              projectConfig?.studyFormWorkflow === 'Mixed')
          ) {
            if (
              roiAnswer[0].subForm &&
              projectConfig.studyForm?.subForms?.[roiAnswer[0].subForm]
                ?.components &&
              state.subForm?.isLabelApplied
            ) {
              store.dispatch(setLabelForQuestion(false));
              const lastAnsweredValue = roiQuestion.values?.find(
                value => value.value === formQuestionAnswer
              );
              const measurementList = measurementApi.Instance.tools[
                measurementData.toolType
              ]?.filter(
                m => m.measurementNumber === measurementData.measurementNumber
              );
              let annotationAnswer = roiAnswer[0]?.value;

              if (
                roiAnswer[0]?.value !== formQuestionAnswer &&
                roiAnswer.length === 1
              ) {
                const note = updateNoteInMixedOrROI(
                  projectConfig,
                  studyFormNotes,
                  sliceInfo,
                  roiQuestion,
                  roiAnswer
                );
                studyFormNotes = note;
                const viewerModeNotes = this.getViewerModeResponse();
                if (viewerModeNotes) {
                  studyFormNotes = { ...viewerModeNotes };
                }
                this.setState(prevState => ({
                  ...prevState,
                  notes: studyFormNotes,
                }));
                setTimeout(() => {
                  this.saveWhileQuestionSwitches();
                }, 1);
              }

              if (formQuestionAnswer && roiAnswer.length > 1) {
                annotationAnswer = roiAnswer.find(
                  data => data.value === formQuestionAnswer
                )?.value;
              }

              updateAnnotationKeyAnswer(
                measurementList,
                roiQuestion,
                annotationAnswer
              );

              if (!state.infusions.isRelabel) {
                this.addOREditSubForm(
                  projectConfig,
                  roiQuestion,
                  measurementList,
                  lastAnsweredValue,
                  studyFormNotes
                );
              }
              this.setSubFormTabDetails(projectConfig, studyFormNotes, false);
            }
          }
        }
      }
    }

    if (
      sessionRead?.notes &&
      !isFile &&
      projectConfig.bSlices &&
      (measurementLabels !== prevProps.measurementLabels ||
        sessionRead !== prevProps.sessionRead)
    ) {
      getAndSetBSliceValidatedInfo(this.props, this.state.notes);
      this.setColorIndicationForViewport(sliceInfo);
    } else if (
      !sessionRead?.notes &&
      !isFile &&
      projectConfig.bSlices &&
      (measurementLabels === prevProps.measurementLabels ||
        sessionRead === prevProps.sessionRead) &&
      !Object.keys(state.studyFormBSliceValidatedInfo?.bSliceValidation)
        .length &&
      sliceInfo?.sliceNumber &&
      projectConfig.bSlices.settings?.[sliceInfo.sliceNumber]
    ) {
      getAndSetBSliceValidatedInfo(this.props, this.state.notes);
      this.setColorIndicationForViewport(sliceInfo);
    }

    const studyFormWorkflow = projectConfig?.studyFormWorkflow || '';
    const isRoiOrMixedWorkflow = ['ROI', 'Mixed'].includes(studyFormWorkflow);
    if (
      isRoiOrMixedWorkflow &&
      this.state.currentQuestion &&
      !this.state.currentQuestion?.isSubForm
    ) {
      this.setState({ currentQuestion: null });
    }

    const isDifferentTask = () => {
      return (
        state.flywheel.readerTaskWithFormResponse?._id !==
        this.state.readerTask?._id
      );
    };

    if (isEditModeForMultipleTask() && isDifferentTask()) {
      const readerTaskNotes = state.flywheel.readerTaskWithFormResponse?.info
        ?.notes
        ? _.cloneDeep(state.flywheel.readerTaskWithFormResponse.info.notes)
        : {};
      this.setState({
        readerTask: state.flywheel.readerTaskWithFormResponse,
        notes: readerTaskNotes,
      });
    }
  }

  getViewerModeResponse() {
    const state = store.getState();
    if (
      state.viewerAvatar.singleAvatar &&
      state.viewerAvatar.multipleTaskNotes[state.viewerAvatar.taskId] &&
      state.flywheel.readerTaskWithFormResponse?._id ===
        state.viewerAvatar.taskId
    ) {
      const multipleTaskNotes = state.viewerAvatar.multipleTaskNotes;
      return multipleTaskNotes[state.viewerAvatar.taskId].notes[0];
    }
    return null;
  }

  confirmationDialogShow = (
    roiQuestion,
    measurementList,
    projectConfig,
    lastAnsweredValue,
    slice,
    roiAnswer,
    studyFormNotes,
    sliceInfo,
    answer = null
  ) => {
    const { UIDialogService } = this.props.servicesManager.services;
    const activeTool = store.getState().infusions?.activeTool;
    let annotateTools = [...measurementTools];
    const popupFromLeft = 175;
    const popupFromTop = 220;

    if (annotateTools.indexOf('DragProbe') > 0) {
      annotateTools.splice(annotateTools.indexOf('DragProbe'), 1);
    }

    this.props.setAvailableLabels([], []);

    if (
      annotateTools.includes(activeTool) &&
      activeTool !== this.state.studyFormQuestionToolForSubForm
    ) {
      this.setState({ studyFormQuestionToolForSubForm: activeTool });
    }

    this.props.setActiveTool('Wwwc');

    this.roiQuestionDialogId = UIDialogService.create({
      content: StudyFormQuestionConfirmationDialog,
      defaultPosition: {
        x: window.innerWidth / 2 - popupFromLeft,
        y: window.innerHeight / 2 - popupFromTop,
      },
      showOverlay: false,
      contentProps: {
        question: roiQuestion,
        value: {
          value: answer ? answer : '',
        },
        formatOptions: () => {
          return formatOptions(
            roiQuestion.values.filter(
              values => values.requireMeasurements?.length
            ),
            DROPDOWN_DEFAULT_VALUE
          );
        },
        getValue: (slice, question) => {
          getValue(slice, question, this.state);
        },
        slice: measurementList[0]?.sliceNumber,
        selectBoxOnChange: (option, question) => {
          lastAnsweredValue = option;
          this.clickType(option, question.key);
          this.onClickQuestionValues(question);

          const roiData = updateAnnotationKeyAnswerAndNotes(
            measurementList,
            roiQuestion,
            option.value,
            roiAnswer,
            projectConfig,
            studyFormNotes,
            sliceInfo
          );
          this.setSubFormTabDetails(
            projectConfig,
            roiData.studyFormNotes,
            true
          );
          this.setState({
            confirmationDialogCurrentQuestion: roiQuestion,
            confirmationDialogRoiAnswer: roiAnswer,
            answer: option.value,
            confirmationDialogMeasurementList: measurementList,
            lastAnsweredValue: lastAnsweredValue,
          });
        },
        onClickQuestionValues: question => {
          this.onClickQuestionValues(question);
        },
        onConfirm: () => {
          UIDialogService.dismiss({ id: this.roiQuestionDialogId });
          this.roiQuestionDialogId = null;
          if (
            lastAnsweredValue?.subForm &&
            projectConfig.studyForm.subForms[lastAnsweredValue?.subForm]
              .components &&
            roiQuestion.type !== 'selectboxes'
          ) {
            this.addOREditSubForm(
              projectConfig,
              roiQuestion,
              measurementList,
              lastAnsweredValue
            );
            this.props.setActiveTool(
              this.state.studyFormQuestionToolForSubForm
            );
            setTimeout(() => {
              this.props.setActiveTool('Wwwc');
            }, 0);
          } else {
            const studyFormQuestionTools = getStudyFormActiveToolsForEnable(
              projectConfig,
              this.props,
              null,
              this.toolsWithLabels
            );
            const label = studyFormQuestionTools?.length ? [''] : null;
            let tools = studyFormQuestionTools?.length
              ? studyFormQuestionTools
              : null;
            tools = getAvailableTools(
              tools,
              projectConfig,
              this.toolsWithLabels,
              this.props.measurementLabels,
              slice
            );
            this.props.setAvailableLabels(label, tools);
            this.props.setActiveTool(
              this.state.studyFormQuestionToolForSubForm
            );
            setTimeout(() => {
              this.props.setActiveTool('Wwwc');
            }, 0);
          }

          setTimeout(() => {
            this.saveWhileQuestionSwitches();
          }, 1);
        },
        onClose: () => {
          UIDialogService.dismiss({ id: this.roiQuestionDialogId });
          this.roiQuestionDialogId = null;
        },
        clickQuestion: answer => {
          const roiData = updateAnnotationKeyAnswerAndNotes(
            measurementList,
            roiQuestion,
            answer,
            roiAnswer,
            projectConfig,
            studyFormNotes,
            sliceInfo
          );
          lastAnsweredValue = roiData.data;
          this.setSubFormTabDetails(
            projectConfig,
            roiData.studyFormNotes,
            true
          );

          this.setState({
            confirmationDialogCurrentQuestion: roiQuestion,
            confirmationDialogRoiAnswer: roiAnswer,
            answer: answer,
            confirmationDialogMeasurementList: measurementList,
            lastAnsweredValue: lastAnsweredValue,
          });
        },
      },
    });
  };

  setSubFormTabDetails = (projectConfig, studyFormNotes, isNoteUpdate) => {
    const componentKeyList = projectConfig.studyForm?.components?.map(
      component => component.key
    );
    const tab = getSubFormTabDetails(
      componentKeyList,
      projectConfig,
      measurementTools,
      this.props.measurements,
      this.state
    );
    if (isNoteUpdate) {
      this.setState(prevState => ({
        ...prevState,
        notes: studyFormNotes,
        subFormTabList: { ...tab.subFormTabList },
        subFormPanel: { ...tab.subFormPanel },
        subFormTab: { ...tab.subFormTab },
        subFormTabWithAnnotationId: {
          ...tab.subFormTabWithAnnotationId,
        },
        annotationIdForQuestionKey: {
          ...tab.annotationIdForQuestionKey,
        },
      }));
    } else {
      this.setState(prevState => ({
        ...prevState,
        subFormTabList: { ...tab.subFormTabList },
        subFormPanel: { ...tab.subFormPanel },
        subFormTab: { ...tab.subFormTab },
        subFormTabWithAnnotationId: {
          ...tab.subFormTabWithAnnotationId,
        },
        annotationIdForQuestionKey: {
          ...tab.annotationIdForQuestionKey,
        },
      }));
    }
  };

  getAndSetBSliceValidatedInfo = () => {
    const { projectConfig } = this.props;
    const allBSliceValidatedData = validateAllBSlice(
      projectConfig,
      this.state.notes
    );
    if (Object.keys(allBSliceValidatedData).length) {
      store.dispatch(studyFormBSliceValidatedInfo(allBSliceValidatedData));
    }
  };

  setColorIndicationForViewport = sliceInfo => {
    if (check2dmprViewportPresent()) {
      this.props.commandsManager.runCommand('setColorIndicationFor2DMPR');
      if (sliceInfo) {
        scrollCornerstoneViewport(sliceInfo);
      }
    }
  };

  addOREditSubForm = (
    projectConfig,
    roiQuestion,
    measurements,
    lastAnsweredValue,
    studyFormNotes = null
  ) => {
    const state = store.getState();
    const isPanelSwitched = state.infusions?.isPanelSwitched;
    const { UIDialogService } = this.props.servicesManager.services;
    if (this.roiSubFormDialogId) {
      UIDialogService.dismiss({ id: this.roiSubFormDialogId });
      this.roiSubFormDialogId = null;
    }

    if (
      this.roiQuestionDialogId ||
      !projectConfig.studyForm?.subForms?.[lastAnsweredValue?.subForm]
        ?.components
    ) {
      return;
    }

    if (
      !isPanelSwitched &&
      this.state.subFormDialogMeasurementList?.[0]?.uuid !==
        measurements[0]?.uuid
    ) {
      this.props.setAvailableLabels([], []);
      this.setState({
        currentQuestion: null,
      });
      store.dispatch(setActiveQuestion(null));
    }
    this.setState({
      subFormDialogCurrentQuestion: roiQuestion,
      subFormDialogRoiAnswer: lastAnsweredValue,
      subFormDialogMeasurementList: measurements,
    });
    const formStyles = {
      pointerEvents: isFormViewOnly(this.state) ? 'none' : 'auto',
      opacity: isFormViewOnly(this.state) ? 0.5 : 1,
    };
    store.dispatch(subFormPopupVisible(true));
    let studyState = JSON.parse(JSON.stringify(this.state));
    if (studyFormNotes) {
      studyState.notes = studyFormNotes;
    }
    const popupFromLeft = 175;
    const popupFromTop = 220;
    this.roiSubFormDialogId = UIDialogService.create({
      content: SubFormDialog,
      defaultPosition: {
        x: window.innerWidth / 2 - popupFromLeft,
        y: window.innerHeight / 2 - popupFromTop,
      },
      showOverlay: false,
      contentProps: {
        subFormComponent:
          projectConfig.studyForm?.subForms?.[lastAnsweredValue.subForm]
            ?.components,
        annotationId: measurements[0]?.uuid,
        slice: measurements[0]?.sliceNumber,
        question: roiQuestion,
        studyFormState: studyState,
        clickType: (option, key, questionKey, subFormName, annotationId) => {
          const studyFormQuestionKey = key;
          const subFormQuestionKey = questionKey;
          this.clickType(
            option,
            studyFormQuestionKey,
            subFormQuestionKey,
            subFormName,
            annotationId
          );
        },
        formStyles: formStyles,
        onClickQuestionValues: (subFormQuestion, isSubForm, subFormName) => {
          this.onClickQuestionValues(subFormQuestion, isSubForm, subFormName);
        },
        getValueSubForm: (
          slice,
          subFormQuestion,
          key,
          subFormName,
          annotationId,
          dialogState
        ) => {
          return getValueSubForm(
            slice,
            subFormQuestion,
            key,
            subFormName,
            annotationId,
            dialogState
          );
        },
        formatOptions: values => {
          return formatOptions(values, DROPDOWN_DEFAULT_VALUE);
        },
        onValueChange: (question, value, subFormQuestion, annotationId) => {
          this.onSubFormValueChange(
            question,
            value,
            subFormQuestion,
            annotationId
          );
        },
        readOnly: false,
        measurementLabels: this.props.measurementLabels,
        questionRefs: this.questionRefs,
        onClose: () => {
          UIDialogService.dismiss({ id: this.roiSubFormDialogId });
          this.roiSubFormDialogId = null;
          store.dispatch(subFormPopupVisible(false));
        },
        onConfirm: () => {
          UIDialogService.dismiss({ id: this.roiSubFormDialogId });
          this.roiSubFormDialogId = null;
          store.dispatch(subFormPopupVisible(false));
          if (
            projectConfig?.studyFormWorkflow === 'ROI' ||
            projectConfig?.studyFormWorkflow === 'Mixed'
          ) {
            const studyFormQuestionTools = getStudyFormActiveToolsForEnable(
              projectConfig,
              this.props,
              null,
              this.toolsWithLabels
            );
            const label = studyFormQuestionTools?.length ? [''] : null;
            let tools = studyFormQuestionTools?.length
              ? studyFormQuestionTools
              : null;
            tools = getAvailableTools(
              tools,
              projectConfig,
              this.toolsWithLabels,
              this.props.measurementLabels,
              measurements[0]?.sliceNumber
            );
            this.props.setAvailableLabels(label, tools);
            store.dispatch(setActiveQuestion(null));
            this.setState({ currentQuestion: null });
            this.props.setActiveTool(
              this.state.studyFormQuestionToolForSubForm
            );
            setTimeout(() => {
              this.props.setActiveTool('Wwwc');
            }, 0);
          }
        },
        onClick: question => {
          this.handleOnClickQuestion(question, true);
        },
      },
    });
  };

  async setReaderTaskAndPermissions() {
    const state = store.getState();
    const user = selectUser(state);
    const isSiteAdmin = user.roles.includes('site_admin');
    const readerTask = await selectReaderTask(state);
    let ownAnnotationPermission =
      (await hasPermission(state, 'annotations_own')) ||
      (await hasPermission(state, 'annotations_manage'));
    let hasEditAnnotationsOthersPermission = await hasPermission(
      state,
      'annotations_edit_others'
    );
    let ownFormResponsesPermission =
      (await hasPermission(state, 'form_responses_own')) ||
      (await hasPermission(state, 'form_responses_manage'));
    let hasEditFormOthersPermission = await hasPermission(
      state,
      'form_responses_edit_others'
    );
    const featureConfig = state.flywheel.featureConfig;
    const isProtocolDeleted = state.flywheel.isProtocolDeleted;
    this.setState({
      readerTask: readerTask || null,
      readOnly:
        isProtocolDeleted ||
        (!ownFormResponsesPermission &&
        !hasEditFormOthersPermission &&
        !isSiteAdmin
          ? featureConfig?.features?.reader_tasks || false
          : false),
      ownAnnotationPermission: ownAnnotationPermission || isSiteAdmin,
      hasEditAnnotationsOthersPermission:
        hasEditAnnotationsOthersPermission || isSiteAdmin,
      ownFormResponsesPermission: ownFormResponsesPermission || isSiteAdmin,
      hasEditFormOthersPermission: hasEditFormOthersPermission || isSiteAdmin,
    });
  }

  onValueChangeStudyNoteUpdation = (
    question,
    subFormQuestion,
    annotationId,
    subFormName,
    value,
    key,
    type,
    projectConfig,
    shouldTriggerAutoSave = false
  ) => {
    const slice = this.state.currentSliceInfo?.sliceNumber || 1;
    let subFormPanel = { ...this.state.subFormPanel };
    const state = store.getState();
    const isFile = isCurrentFile(state);

    if (
      projectConfig.studyFormWorkflow === 'Form' ||
      projectConfig?.studyFormWorkflow === 'Mixed'
    ) {
      if (!isFile && projectConfig.bSlices && subFormPanel[slice]) {
        Object.keys(subFormPanel[slice]).forEach(questionKey => {
          if (!subFormQuestion) {
            subFormPanel[slice][questionKey] = false;
          } else {
            if (questionKey !== question.key) {
              subFormPanel[slice][questionKey] = false;
            } else {
              subFormPanel[slice][questionKey] = true;
            }
          }
        });
      } else {
        Object.keys(subFormPanel).forEach(questionKey => {
          if (!subFormQuestion) {
            subFormPanel[questionKey] = false;
          } else {
            if (questionKey !== question.key) {
              subFormPanel[questionKey] = false;
            } else {
              subFormPanel[questionKey] = true;
            }
          }
        });
      }
    }

    let stateNotes = JSON.parse(JSON.stringify(this.state.notes));
    let updatedNotes =
      !isFile && projectConfig.bSlices && stateNotes.slices
        ? stateNotes.slices[slice] || {}
        : stateNotes;

    if (subFormQuestion !== null) {
      if (type === 'selectboxes') {
        // multi-select
        if (!updatedNotes[question.key][subFormName]) {
          updatedNotes[question.key][subFormName] = [];
        }

        const isSubFormNotes = updatedNotes[question.key][subFormName].find(
          item => item.annotationId === annotationId
        );

        if (!isSubFormNotes) {
          const subFormNotes = {
            annotationId: annotationId,
            [key]: [value],
          };
          updatedNotes[question.key][subFormName].push(subFormNotes);
        } else if (
          isSubFormNotes &&
          (!isSubFormNotes[key] || !Array.isArray(isSubFormNotes[key]))
        ) {
          isSubFormNotes[key] = [value];
        } else if (!isSubFormNotes[key].includes(value)) {
          isSubFormNotes[key].push(value);
        } else if (isSubFormNotes[key].includes(value)) {
          const index = isSubFormNotes[key].indexOf(value);
          if (index > -1) {
            isSubFormNotes[key].splice(index, 1);
          }
        }
      } else {
        if (subFormQuestion.values) {
          subFormQuestion.values.forEach(item => {
            if (item.value === value) {
              if (
                subFormName &&
                typeof updatedNotes[question.key] === 'string'
              ) {
                updatedNotes[question.key] = {
                  value: item.value,
                };
              }

              if (!updatedNotes[question.key]) {
                updatedNotes[question.key] = {
                  value: item.value,
                };
              }

              if (!updatedNotes[question.key][subFormName]) {
                updatedNotes[question.key][subFormName] = [];
              }

              const isSubFormNotes = updatedNotes[question.key][
                subFormName
              ].find(item => item.annotationId === annotationId);

              if (!isSubFormNotes) {
                const subFormNotes = {
                  annotationId: annotationId,
                  [key]: value === DROPDOWN_DEFAULT_VALUE ? '' : value,
                };
                updatedNotes[question.key][subFormName].push(subFormNotes);
              } else {
                updatedNotes[question.key][subFormName].find(
                  item => item.annotationId === annotationId
                )[key] = value === DROPDOWN_DEFAULT_VALUE ? '' : value;
              }
            }
          });
        } else {
          updatedNotes[question.key][subFormName].find(
            item => item.annotationId === annotationId
          )[key] = value === DROPDOWN_DEFAULT_VALUE ? '' : value;
        }
      }
    } else {
      if (type === 'selectboxes') {
        // multi-select
        if (!updatedNotes[key] || !Array.isArray(updatedNotes[key])) {
          updatedNotes[key] = [value];
        } else if (!updatedNotes[key].includes(value)) {
          updatedNotes[key].push(value);
        } else {
          updatedNotes[key] = updatedNotes[key].filter(item => item !== value);
        }
      } else {
        if (question.values) {
          question.values.forEach(item => {
            if (item.value === value) {
              if (item.subForm) {
                const subFormName = item.subForm;

                if (typeof updatedNotes[key] === 'object') {
                  updatedNotes[key].value =
                    value === DROPDOWN_DEFAULT_VALUE ? '' : value;
                } else {
                  updatedNotes[key] =
                    value === DROPDOWN_DEFAULT_VALUE
                      ? {
                          value: '',
                        }
                      : {
                          value: value,
                        };
                  updatedNotes[key][subFormName] = [];
                }
              } else {
                updatedNotes[key] =
                  value === DROPDOWN_DEFAULT_VALUE ? '' : value;
              }
            }
          });
        } else {
          updatedNotes[key] = value === DROPDOWN_DEFAULT_VALUE ? '' : value;
        }
      }
    }

    const activeTool = selectActiveTool(store.getState());
    const previousTool = measurementTools.includes(activeTool)
      ? this.state.previousTool
      : !activeTool
      ? this.state.previousTool
      : activeTool;

    if (!isFile && projectConfig.bSlices) {
      if (!stateNotes.slices) {
        stateNotes = { slices: {} };
      }
      stateNotes.slices[slice] = updatedNotes;
    } else {
      stateNotes = updatedNotes;
    }

    if (subFormQuestion !== null) {
      store.dispatch(
        setActiveQuestion({
          ...subFormQuestion,
          answer: updatedNotes[question.key][subFormName].find(
            item => item.annotationId === annotationId
          )[key],
          questionKey: key,
          isSubForm: true,
          subFormName: subFormName,
          subFormAnnotationId: annotationId,
          question: question.key,
        })
      );
    } else {
      store.dispatch(
        setActiveQuestion({
          ...question,
          answer: updatedNotes[key],
          questionKey: key,
          isSubForm: false,
          subFormName: '',
          subFormAnnotationId: '',
          question: '',
        })
      );
    }

    if (!isFile && projectConfig.bSlices) {
      const allBSliceValidatedData =
        state.studyFormBSliceValidatedInfo.bSliceValidation;
      const bSliceValidatedData = validateBSlice(
        projectConfig,
        slice,
        stateNotes
      );

      if (
        Object.keys(allBSliceValidatedData).length &&
        allBSliceValidatedData[slice] !== !bSliceValidatedData
      ) {
        allBSliceValidatedData[slice] = !bSliceValidatedData;
        store.dispatch(studyFormBSliceValidatedInfo(allBSliceValidatedData));
        this.props.commandsManager.runCommand('setColorIndicationFor2DMPR');
        scrollCornerstoneViewport(this.state.currentSliceInfo);
      }
    }

    this.setState(
      {
        ...this.state,
        errors: omit(this.state.errors, key),
        notes: { ...stateNotes },
        currentQuestion: subFormQuestion
          ? { ...subFormQuestion }
          : { ...question },
        previousTool: previousTool,
        previousQuestion: this.state.currentQuestion,
        subFormPanel: subFormPanel,
      },
      () => {
        this.setStudyLabels();
        updateSessionNotes({ ...stateNotes });
        if (shouldTriggerAutoSave) {
          this.saveWhileQuestionSwitches();
        }
      }
    );
  };

  onSubFormValueChange = (
    question,
    value,
    subFormQuestion = null,
    annotationId = null,
    shouldTriggerAutoSave = false
  ) => {
    const { key, type } = subFormQuestion ? subFormQuestion : question;
    const { projectConfig } = this.props;
    let subFormName = null;

    if (
      (projectConfig?.studyFormWorkflow === 'ROI' ||
        projectConfig?.studyFormWorkflow === 'Mixed') &&
      !subFormQuestion
    ) {
      subFormName = question.values?.find(value => value.value === value)
        ?.subForm;
    } else if (subFormQuestion) {
      if (question?.subFormName) {
        subFormName = question?.subFormName;
      } else {
        const { currentSliceInfo } = this.state;
        const slice = currentSliceInfo?.sliceNumber;
        const noteValue = getValue(slice, question, this.state);
        subFormName =
          question.subFormName ||
          question.values?.find(v => v.value === noteValue)?.subForm;
      }
    }

    this.onValueChangeStudyNoteUpdation(
      question,
      subFormQuestion,
      annotationId,
      subFormName,
      value,
      key,
      type,
      projectConfig,
      shouldTriggerAutoSave
    );
  };

  onValueChange = (
    question,
    value,
    subFormQuestion = null,
    annotationId = null,
    shouldTriggerAutoSave = false
  ) => {
    this.onSubFormValueChange(
      question,
      value,
      subFormQuestion,
      annotationId,
      shouldTriggerAutoSave
    );
  };

  setStudyLabels() {
    const { projectConfig } = this.props;
    if (!projectConfig.labels) {
      return;
    }
    const state = store.getState();
    let toolsWithLabel = {};
    const timePointManager = state.timepointManager;
    const props = {
      ...this.props,
      previousVisibleQuestion: this.state.previousQuestion,
      sliceInfo: this.props.sliceInfo,
    };

    const visibleQuestions = getVisibleQuestions(this.state, props);
    if (!visibleQuestions.length) {
      return;
    }

    const isSubFormEnabled =
      this.roiQuestionDialogId || this.roiSubFormDialogId ? true : false;
    toolsWithLabel = getToolsWithLabels(visibleQuestions, isSubFormEnabled);

    const isStudyFormContainsMeasurementTools = hasMeasurementTools();

    let {
      requiredLabels,
      availableLabels,
      availableTools,
      subFormInfo,
    } = this.getToolsAndLabelsForCurrentAnswer(
      visibleQuestions,
      toolsWithLabel
    );

    if (isStudyFormContainsMeasurementTools) {
      // if there are no required measurements, allow all labels
      this.setAvailableLabelsAndToolsBasedOnCurrentAnswer(
        requiredLabels,
        timePointManager,
        subFormInfo,
        visibleQuestions,
        availableTools,
        availableLabels
      );
    } else {
      // if there are no configured measurement tools, allow all Tools
      this.props.setAvailableLabels([''], [...measurementTools]);
    }
  }

  getToolsAndLabelsForCurrentAnswer = (visibleQuestions, toolsWithLabel) => {
    const { projectConfig } = this.props;
    const { currentSliceInfo, currentQuestion, isSubFormQuestion } = this.state;
    const slice = currentSliceInfo?.sliceNumber || 1;
    const state = store.getState();
    const activeTool = state.infusions.activeTool;
    const studyFormWorkflow = projectConfig?.studyFormWorkflow || '';
    const isRoiOrMixedWorkflow = ['ROI', 'Mixed'].includes(studyFormWorkflow);
    const timePointManager = state.timepointManager;

    let components = visibleQuestions;

    const subFormInfo = getSubFormQuestionDetails(this.state, visibleQuestions);
    if (subFormInfo?.components?.some(x => x.key === currentQuestion?.key)) {
      components = subFormInfo.components;
    }

    // get required measurements across all question options and selected options
    const {
      requiredLabels,
      availableTools,
    } = this.getRequiredLabelsAndAvailableToolsForCurrentAnswer(
      components,
      subFormInfo
    );

    if (toolsWithLabel && Object.keys(toolsWithLabel)?.length) {
      this.toolsWithLabels = toolsWithLabel;
    }

    // filter labels down to those required by selected options
    let subFormInfoForLabels = { ...subFormInfo };

    if (timePointManager.isMeasurementDeletedForStudyFormQuestion) {
      const lastDeletedMeasurementId =
        timePointManager.lastDeletedMeasurementId;
      subFormInfoForLabels = getDeletedMeasurementDetail(
        timePointManager,
        lastDeletedMeasurementId
      );
    }

    let availableLabels = getAvailableLabels(
      projectConfig,
      requiredLabels,
      slice,
      subFormInfoForLabels.subFormAnnotationId
    );
    if (
      isRoiOrMixedWorkflow &&
      availableTools.size &&
      measurementTools.includes(Array.from(availableTools)?.[0]) &&
      !isSubFormQuestion
    ) {
      const tools = Array.from(availableTools);
      const tool = tools.includes(activeTool) ? activeTool : tools[0];
      const labelsForTool = getLabelsForToolWorkFlow(tool, slice, '');
      const newLabels = toolsWithLabel[tool];
      availableLabels =
        newLabels?.length && labelsForTool?.length
          ? [...newLabels, ...labelsForTool]
          : newLabels?.length
          ? [...newLabels]
          : [...labelsForTool];
      toolsWithLabel[tool] = Array.from(new Set(availableLabels));
      toolsWithLabel[tool].forEach(x => requiredLabels.add(x));
    }

    return { requiredLabels, availableLabels, availableTools, subFormInfo };
  };

  setToolsForBSliceConfig = (projectConfig, sliceInfo) => {
    const infusions = store.getState().infusions;
    const infusionActiveTool = infusions.activeTool;
    const currentQuestionKey = infusions.currentSelectedQuestion?.key;
    const currentQuestionAnswer =
      infusions.currentSelectedQuestion?.answer?.value ||
      infusions.currentSelectedQuestion?.answer;
    const values = infusions.currentSelectedQuestion?.values;
    let currentTool = this.state.currentTool;
    let isToolAvailableForQuestion = true;

    if (values?.length > 0) {
      const currentAnswer = values.find(v => v.value === currentQuestionAnswer);
      if (this.state.isSubFormQuestion) {
        currentTool = currentAnswer?.measurementTools?.[0];
      } else {
        if (currentAnswer?.instructionSet?.length) {
          currentTool = currentAnswer?.instructionSet[0].measurementTools[0];
        } else {
          currentTool = getActiveTool(
            currentAnswer?.measurementTools || [],
            this.state
          );
        }
      }
    }

    if (
      measurementTools.includes(infusionActiveTool) &&
      currentTool !== infusionActiveTool &&
      infusionActiveTool !== 'DragProbe'
    ) {
      currentTool = infusionActiveTool;
    }

    if (projectConfig?.bSlices?.settings?.[sliceInfo?.sliceNumber]) {
      const slices = this.state.notes.slices;
      const notesBySlice = slices?.[sliceInfo?.sliceNumber];

      if (
        this.state.isSubFormQuestion &&
        notesBySlice?.hasOwnProperty(this.state.currentKey)
      ) {
        const subFormLabel = notesBySlice?.[this.state.currentKey]?.[
          this.state.subFormName
        ].find(item => item.annotationId === this.state.subFormAnnotationId);
        if (subFormLabel?.hasOwnProperty(currentQuestionKey)) {
          const answer =
            subFormLabel[currentQuestionKey].value ||
            subFormLabel[currentQuestionKey];
          const value = values?.find(v => v.value === answer);
          if (!value?.measurementTools?.includes(currentTool)) {
            isToolAvailableForQuestion = false;
            cornerstoneUtils.setToolMode(currentTool, 'passive');
          }
        }
      } else if (notesBySlice?.hasOwnProperty(currentQuestionKey)) {
        const answer =
          notesBySlice[currentQuestionKey]?.value ||
          notesBySlice[currentQuestionKey];
        const value = values?.find(v => v.value === answer);
        if (
          !value?.measurementTools?.includes(currentTool) &&
          !value?.instructionSet?.filter(i =>
            i.measurementTools.includes(currentTool)
          )
        ) {
          isToolAvailableForQuestion = false;
          cornerstoneUtils.setToolMode(currentTool, 'passive');
        }
      } else {
        isToolAvailableForQuestion = false;
        cornerstoneUtils.setToolMode(currentTool, 'passive');
      }
    } else {
      if (currentTool === infusionActiveTool) {
        isToolAvailableForQuestion = false;
        cornerstoneUtils.setToolMode(infusionActiveTool, 'passive');
      }
    }

    setTimeout(() => {
      this.setAvailableLabelsForActiveModeTools(
        currentTool,
        sliceInfo,
        isToolAvailableForQuestion
      );
    }, 100);
  };

  // Set tools and labels enabled/disabled based on the tool mode of selected tool.
  // This will not change currently selected tool
  setAvailableLabelsForActiveModeTools = (
    currentTool,
    sliceInfo,
    isToolAvailableForQuestion
  ) => {
    const element = cornerstoneUtils.getEnabledElement(sliceInfo.viewportIndex);
    const tool = cornerstoneTools.getToolForElement(element, currentTool);
    const availableTools = store.getState().infusions.availableTools;
    if (
      tool?.mode !== 'active' &&
      (availableTools.includes(currentTool) ||
        (!availableTools.includes(currentTool) && !isToolAvailableForQuestion))
    ) {
      this.props.setAvailableLabels([], []);
    } else {
      const props = {
        ...this.props,
        previousVisibleQuestion: this.state.previousQuestion,
        sliceInfo: this.props.sliceInfo,
      };

      const visibleQuestions = getVisibleQuestions(this.state, props);
      if (!visibleQuestions.length) {
        return;
      }
      const {
        availableLabels,
        availableTools,
      } = this.getToolsAndLabelsForCurrentAnswer(visibleQuestions);
      const availableToolsList =
        availableTools instanceof Set
          ? Array.from(availableTools)
          : availableTools || [];
      if (availableLabels.length === 0) {
        if (!isToolAvailableForQuestion) {
          availableTools.clear();
          this.props.setAvailableLabels(
            [''],
            availableTools.size > 0 ? availableToolsList : null
          );
          if (getActiveTool([], this.state) !== ERASERTOOL) {
            sustainCurrentActiveTool(availableToolsList);
          }
        } else {
          this.props.setAvailableLabels([], []);
        }
      } else {
        this.props.setAvailableLabels(
          availableLabels,
          availableTools.size > 0 ? availableToolsList : null
        );
        if (getActiveTool([], this.state) !== ERASERTOOL) {
          sustainCurrentActiveTool(availableToolsList);
        }
      }
    }
  };

  getRequiredLabelsAndAvailableToolsForCurrentAnswer(components, subFormInfo) {
    const { projectConfig } = this.props;
    const { notes, currentQuestion, currentSliceInfo } = this.state;
    const slice = currentSliceInfo?.sliceNumber || 1;
    const state = store.getState();
    const timePointManager = state.timepointManager;

    const requiredLabels = new Set();
    const availableTools = new Set();

    const isStudyFormContainsMeasurementTools = projectConfig.studyForm?.components?.some(
      isMeasurementTools
    );
    const bSliceSettings = getBSliceSettings();
    if (
      state.infusions.currentSelectedQuestion === null &&
      isStudyFormContainsMeasurementTools &&
      !timePointManager.isMeasurementDeletedForStudyFormQuestion
    ) {
      return { requiredLabels, availableTools };
    }

    for (const question of components) {
      if (
        (currentQuestion && currentQuestion.key !== question.key) ||
        timePointManager.isMeasurementDeletedForStudyFormQuestion
      ) {
        continue;
      }
      for (const option of question.values || []) {
        let isSelected = false;
        let answer = '';
        if (subFormInfo.isSubForm) {
          let subFormNotes = null;
          if (notes.slices) {
            subFormNotes = notes?.slices?.[slice];
          } else {
            subFormNotes = notes;
          }

          answer = subFormNotes?.[subFormInfo.question]?.[
            subFormInfo.subFormName
          ]?.find(
            item => item.annotationId === subFormInfo.subFormAnnotationId
          );
          isSelected = option.value === answer?.[question.key];
          const selectedOption = components
            .find(qs => qs.key === question.key)
            ?.values?.find(option => option.value === answer?.[question.key]);
          if (
            !isSelected &&
            currentQuestion?.key == question.key &&
            selectedOption?.requireMeasurements?.length &&
            selectedOption?.measurementTools?.length
          ) {
            isSelected = true;
          }
        } else {
          if (typeof notes.slices?.[slice]?.[question.key] === 'object') {
            isSelected =
              option.value === notes.slices[slice][question.key].value;
          } else if (typeof notes[question.key] === 'object') {
            isSelected = option.value === notes[question.key].value;
          } else {
            isSelected = notes.slices
              ? notes.slices[slice]
                ? option.value === notes.slices[slice][question.key]
                : false
              : Array.isArray(notes[question.key])
              ? notes[question.key].includes(option.value)
              : option.value === notes[question.key];
          }
        }
        let requireMeasurements = option.requireMeasurements;
        if (
          !subFormInfo.isSubForm &&
          (!requireMeasurements || !requireMeasurements.length)
        ) {
          if (option?.instructionSet?.length) {
            requireMeasurements = option.instructionSet[0].requireMeasurements;
          }
        }
        if (isSelected) {
          const measurementToolsForAnswer = getMeasurementToolsForAnswer(
            option,
            null,
            question,
            bSliceSettings,
            slice
          );
          measurementToolsForAnswer.forEach(x => availableTools.add(x));
          const bSliceTools = getBSliceTools(question, bSliceSettings, slice);
          for (const tool of availableTools) {
            const labelsForTool = getLabelsForTool(tool, option, bSliceTools);
            labelsForTool.forEach(x => requiredLabels.add(x));
          }
        }
      }
    }

    return { requiredLabels, availableTools };
  }

  setAvailableLabelsAndToolsBasedOnCurrentAnswer(
    currentRequired,
    timePointManager,
    subFormInfo,
    visibleQuestions,
    availableTools,
    availableLabels
  ) {
    const { measurementLabels, projectConfig } = this.props;
    const { notes, currentQuestion, currentSliceInfo, readOnly } = this.state;
    const slice = currentSliceInfo?.sliceNumber || 1;
    const state = store.getState();

    if (
      this.roiSubFormDialogId &&
      state.infusions?.currentSelectedQuestion &&
      !state.infusions?.currentSelectedQuestion?.isSubForm
    ) {
      return;
    }

    let studyFormActiveTool = getActiveTool([], this.state);

    if (currentRequired.size === 0) {
      if (!this.state.isComplete) {
        if (!timePointManager.isMeasurementDeletedForStudyFormQuestion) {
          if (currentQuestion && studyFormActiveTool !== ERASERTOOL) {
            this.props.setActiveTool(state.infusions.activeTool);
          }
          this.validateWhileQuestionSwitch();
        }
      } else {
        this.setState({ isComplete: false });
        this.props.setAvailableLabels(null, null);
        this.props.setActiveTool();
        this.validateWhileQuestionSwitch();
      }
    }

    studyFormActiveTool = getActiveTool([], this.state);
    let availableToolsList = [];

    if (
      (projectConfig?.studyFormWorkflow === 'ROI' ||
        projectConfig?.studyFormWorkflow === 'Mixed') &&
      availableLabels.length
    ) {
      availableToolsList = getAvailableTools(
        availableTools,
        projectConfig,
        this.toolsWithLabels,
        measurementLabels,
        slice,
        subFormInfo.isSubForm
      );
    } else if (
      !projectConfig?.studyFormWorkflow ||
      projectConfig?.studyFormWorkflow === 'Form'
    ) {
      availableToolsList =
        availableTools instanceof Set
          ? Array.from(availableTools)
          : availableTools || [];
    }

    const isFile = isCurrentFile(state);
    let isSetActiveTool =
      isFile && state.infusions.activeTool === 'OpenFreehandRoi';

    if (
      (projectConfig.studyFormWorkflow === 'ROI' ||
        projectConfig.studyFormWorkflow === 'Mixed') &&
      state.infusions?.toolWhilePanelSwitch
    ) {
      store.dispatch(setToolWhilePanelSwitch(false));
      isSetActiveTool = availableToolsList[0] !== state.infusions.activeTool;
    }

    if (availableLabels.length === 0) {
      availableTools.clear();
      this.props.setAvailableLabels(
        [''],
        availableTools instanceof Set ? Array.from(availableTools) : null
      );

      if (
        !state.infusions?.currentSelectedQuestion &&
        availableToolsList[0] !== state.infusions.activeTool
      ) {
        isSetActiveTool = true;
      }

      if (!isSetActiveTool) {
        this.setState({
          studyFormQuestionToolForSubForm: state.infusions.activeTool,
        });
        if (studyFormActiveTool !== ERASERTOOL) {
          this.props.setActiveTool(availableToolsList[0]);
        }
      }
    } else {
      this.props.setAvailableLabels(
        availableLabels,
        availableTools.size > 0 ? availableToolsList : null
      );
      if (!isSetActiveTool && studyFormActiveTool !== ERASERTOOL) {
        const currentSelectedQuestion =
          state.infusions?.currentSelectedQuestion;
        const selectedAnswer =
          currentSelectedQuestion?.answer?.value ||
          currentSelectedQuestion?.answer;
        const selectedOption = currentSelectedQuestion?.values?.find(
          x => x.value === selectedAnswer
        );
        const { questionTools } = getToolsAndLabelsForQuestionAnswer(
          currentSelectedQuestion,
          selectedOption,
          slice
        );
        const currentTool = questionTools.includes(state.infusions.activeTool)
          ? state.infusions.activeTool
          : availableToolsList[0];
        this.props.setActiveTool(currentTool);
      }
    }

    studyFormActiveTool = getActiveTool([], this.state);

    if (!projectConfig?.bSlices || isFile) {
      this.setState(prevState => ({
        ...prevState,
        currentAvailableLabels: availableLabels,
        currentAvailableTools: availableTools,
        currentTool: [...availableToolsList][0],
      }));
    }

    let availableToolList = [...availableToolsList];
    const activeTool = getActiveTool(availableToolList, this.state);

    // re-evaluate current tool state
    if (availableTools.size > 0 && availableLabels.length > 0) {
      if (
        (projectConfig?.studyFormWorkflow === 'ROI' ||
          projectConfig?.studyFormWorkflow === 'Mixed') &&
        !this.roiSubFormDialogId &&
        !state.infusions?.currentSelectedQuestion?.isSubForm
      ) {
        const questionTools = getStudyFormActiveToolsForEnable(
          projectConfig,
          this.props,
          null,
          this.toolsWithLabels
        );
        const qLabels = questionTools?.length ? [''] : null;
        let qTools = questionTools?.length ? questionTools : [];
        Array.from(availableTools)?.forEach(tool => {
          if (!qTools.find(questionTool => questionTool === tool)) {
            qTools.push(tool);
          }
        });
        qTools = getAvailableTools(
          qTools,
          projectConfig,
          this.toolsWithLabels,
          measurementLabels,
          slice,
          subFormInfo.isSubForm
        );
        this.props.setAvailableLabels(qLabels, qTools);
      }
      const TaskIds = state.multipleReaderTask.TaskIds;
      const toolMode =
        TaskIds.length > 1 || hasActiveToolLimitReached(activeTool)
          ? 'passive'
          : 'active';

      if (studyFormActiveTool !== ERASERTOOL) {
        cornerstoneUtils.setToolMode(activeTool, toolMode);
      }

      if (!(TaskIds.length > 1) && studyFormActiveTool !== ERASERTOOL) {
        this.props.setActiveTool(activeTool);
      }
    } else {
      if (!this.state.isComplete) {
        if (
          !projectConfig?.studyFormWorkflow ||
          (!availableLabels.length && !state.subForm?.isSubFormPopupEnabled)
        ) {
          if (
            projectConfig?.studyFormWorkflow === 'ROI' ||
            (projectConfig?.studyFormWorkflow === 'Mixed' &&
              !state.infusions.currentSelectedQuestion?.isSubForm &&
              !this.roiSubFormDialogId)
          ) {
            const studyFormQuestionToolsList = getStudyFormActiveToolsForEnable(
              projectConfig,
              this.props,
              null,
              this.toolsWithLabels
            );
            const labelList =
              studyFormQuestionToolsList?.length &&
              state.subForm?.isSubFormPopupEnabled
                ? []
                : studyFormQuestionToolsList?.length &&
                  !state.subForm?.isSubFormPopupEnabled
                ? ['']
                : null;
            let toolList = studyFormQuestionToolsList?.length
              ? studyFormQuestionToolsList
              : null;
            toolList = getAvailableTools(
              toolList,
              projectConfig,
              this.toolsWithLabels,
              measurementLabels,
              slice
            );
            if (!readOnly) {
              this.props.setAvailableLabels(labelList, toolList);
            }
          } else {
            this.props.setAvailableLabels([], []);
          }

          if (availableLabels.length) {
            this.props.setActiveTool();
          }
        } else {
          if (studyFormActiveTool !== ERASERTOOL) {
            cornerstoneUtils.setToolMode(availableToolsList[0], 'passive');
          }
        }
      } else if (this.state.isComplete) {
        this.setState({ isComplete: false });
        this.props.setAvailableLabels(null, null);
      }

      if (studyFormActiveTool !== ERASERTOOL) {
        this.props.setActiveTool(activeTool);
      }
    }

    if (
      (projectConfig?.studyFormWorkflow === 'ROI' ||
        projectConfig?.studyFormWorkflow === 'Mixed') &&
      !state.subForm?.isSubFormPopupEnabled &&
      !subFormInfo.isSubForm
    ) {
      const currentActiveTool = selectActiveTool(store.getState());
      const studyFormQuestionTools = getStudyFormActiveToolsForEnable(
        projectConfig,
        this.props,
        null,
        this.toolsWithLabels
      );
      const label = studyFormQuestionTools?.length ? [''] : null;
      let tools = studyFormQuestionTools?.length
        ? studyFormQuestionTools
        : null;
      store.dispatch(setWorkFlowTools(tools));
      if (!tools?.includes?.(currentActiveTool)) {
        tools = getAvailableTools(
          tools,
          projectConfig,
          this.toolsWithLabels,
          measurementLabels,
          slice
        );

        if (!readOnly) {
          this.props.setAvailableLabels(label, tools);
        }
        const tool = getActiveTool([], this.state);
        if (
          tool !== ERASERTOOL &&
          measurementTools.includes(tool) &&
          tool !== 'DragProbe'
        ) {
          this.props.setActiveTool('Wwwc');
        }
      }

      if (this.roiQuestionDialogId) {
        this.props.setAvailableLabels([], []);

        if (getActiveTool([], this.state) !== ERASERTOOL) {
          this.props.setActiveTool('Wwwc');
        }
      }

      if (
        projectConfig?.studyFormWorkflow === 'ROI' &&
        this.state.currentType?.measurementTools?.length
      ) {
        store.dispatch(setActiveQuestion(null));
      }
    }
  }

  validateWhileQuestionSwitch = () => {
    const props = {
      ...this.props,
      previousVisibleQuestion: this.state.previousQuestion,
      sliceInfo: this.props.sliceInfo,
    };
    const visibleQuestions = getVisibleQuestions(this.state, props);
    const qValidation = validateWhileQuestionSwitches(
      this.props,
      visibleQuestions,
      this.state.notes
    );
    if (qValidation !== null) {
      this.setState({ questionValidation: qValidation });
    }
  };

  saveWhileQuestionSwitches = () => {
    const { projectConfig } = this.props;
    const allowDraft = projectConfig?.allowDraft !== false;
    const state = store.getState();
    const slice = this.state.currentSliceInfo?.sliceNumber || 1;
    if (allowDraft) {
      let questions = {};
      if (!isCurrentFile(state) && projectConfig?.bSlices) {
        questions.slices = {};
        const bSlices = getBSlices(projectConfig);
        const formState = _.cloneDeep(this.state);
        for (const sliceNo of bSlices) {
          formState.currentSliceInfo.sliceNumber = sliceNo;
          questions.slices[sliceNo] = getVisibleQuestions(
            formState,
            this.props
          );
        }
      } else {
        const props = {
          ...this.props,
          previousVisibleQuestion: this.state.previousQuestion,
          sliceInfo: this.props.sliceInfo,
        };
        questions = getVisibleQuestions(this.state, props);
      }
      const { notes } = draftStudyForm(questions, this.state, this.props);

      const readTime = this.props.getReadTime();
      const prevStatus = this.state.readerTaskStatus;
      const { taskId } = getRouteParams();
      updateSessionNotes(notes);
      updateComponentByPromise(
        {
          init: {
            errors: {},
            draftSave: 'Saving...',
            draftMessage: 'Please complete required items.',
          },
          success: { draftSave: 'Saved' },
          error: {
            draftSave: 'Problems saving',
          },
          default: { draftSave: null },
        },
        this.props.saveStudy.bind(
          undefined,
          this.state.readOnly ? null : notes,
          readTime,
          readStatus.Inprogress,
          slice
        ),
        (state, error) => {
          if (error && error.code !== 422) {
            this.props.showUserErrorMessage(error);
          }
          if (state.draftSave === 'Saving...') {
            state.isStateUpdate = true;
          } else {
            if (state.draftSave === 'Saved' && taskId) {
              sendTaskSignals(
                taskId,
                readStatus.Inprogress,
                prevStatus,
                this.props,
                readStatus
              );
            }
            state.isStateUpdate = false;
          }
        }
      );
      this.setState(prevState => ({
        ...prevState,
        isComplete: false,
        submit: 'Submit',
        readerTaskStatus: readStatus.Inprogress,
      }));
    }
  };

  noteUpdateForROI = (
    question,
    value,
    subFormQuestion = null,
    annotationId = null,
    subFormName = null
  ) => {
    const { key, type } = question;
    const { projectConfig } = this.props;
    this.onValueChangeStudyNoteUpdation(
      question,
      subFormQuestion,
      annotationId,
      subFormName,
      value,
      key,
      type,
      projectConfig
    );
  };

  handleOnClickQuestion(question, subFormQuestion) {
    const state = store.getState();
    const singleAvatar = state.viewerAvatar.singleAvatar;

    const isMultipleReaderTask = state.multipleReaderTask?.TaskIds?.length > 1;
    if (singleAvatar || !isMultipleReaderTask) {
      store.dispatch(
        setActiveQuestion({
          ...question,
          questionKey: question.key,
          isSubForm: subFormQuestion,
          question: question.key,
        })
      );
      this.setState({ currentQuestion: question });
      this.props.setActiveTool('Wwwc');
    }
  }

  onTextChange = (
    question,
    value,
    subFormQuestion = null,
    annotationId = null,
    subFormName = null
  ) => {
    this.noteUpdateForROI(
      question,
      value,
      subFormQuestion,
      annotationId,
      subFormName
    );
  };

  clickType = (
    option,
    key,
    subFormQuestionKey = null,
    subFormName = null,
    annotationId = null,
    shouldTriggerAutoSave = true,
    shouldResetEraser = true
  ) => {
    let qOption = { ...option };
    const { projectConfig } = this.props;
    const slice = this.state.currentSliceInfo?.sliceNumber || 1;
    const state = store.getState();
    if (
      (projectConfig?.studyFormWorkflow === 'Mixed' ||
        projectConfig?.studyFormWorkflow === 'ROI') &&
      ((this.state.subFormPanel?.[slice] &&
        Object.keys(this.state.subFormPanel?.[slice]).length) ||
        (!projectConfig?.bSlices &&
          this.state.subFormPanel &&
          Object.keys(this.state.subFormPanel).length))
    ) {
      const isInSubForm = subFormQuestionKey !== null;

      store.dispatch(subFormQuestionAnswered(isInSubForm));

      if (projectConfig?.studyFormWorkflow === 'Mixed') {
        const questionKeyValue = {
          key: key,
          value: qOption.value,
        };
        store.dispatch(studyFormAnswerInMixedMode(questionKeyValue));
      }
    }
    const isFile = isCurrentFile(state);

    if (subFormQuestionKey === null) {
      // To close the subForm dialog, if the option is changed
      if (this.roiSubFormDialogId) {
        const { UIDialogService } = this.props.servicesManager.services;
        UIDialogService.dismiss({ id: this.roiSubFormDialogId });
        this.roiSubFormDialogId = null;
      }

      if (
        !isFile &&
        projectConfig?.bSlices?.settings[slice]?.measurementTools &&
        !(qOption?.directive || qOption?.instructionSet)
      ) {
        if (projectConfig?.bSlices?.settings[slice]?.measurementTools[key]) {
          qOption.measurementTools =
            projectConfig?.bSlices?.settings[slice]?.measurementTools[key];
        }
      }

      if (qOption?.instructionSet) {
        qOption.instructionSet.forEach(x => {
          if (!qOption.measurementTools) {
            qOption.measurementTools = [];
          }
          if (!qOption.requireMeasurements) {
            qOption.requireMeasurements = [];
          }
          qOption.measurementTools = qOption.measurementTools.concat(
            x.measurementTools
          );
          qOption.requireMeasurements = qOption.requireMeasurements.concat(
            x.requireMeasurements
          );
        });
      }
    }

    if (shouldResetEraser && getActiveTool([], this.state) === ERASERTOOL) {
      this.props.setActiveTool('DragProbe');
    }

    this.setState(
      {
        ...this.state,
        currentType: { ...qOption },
        currentKey: subFormQuestionKey === null ? key : subFormQuestionKey,
        isSubFormQuestion: !_.isEmpty(subFormQuestionKey),
        subFormName: subFormQuestionKey === null ? '' : subFormName,
        subFormQuestion: subFormQuestionKey === null ? '' : subFormQuestionKey,
        subFormAnnotationId: subFormQuestionKey === null ? '' : annotationId,
        currentAvailableLabels: null,
        currentAvailableTools: null,
        currentTool: null,
        studyFormQuestionKey: subFormQuestionKey === null ? null : key,
      },
      () => {
        const question = projectConfig.studyForm?.components?.find(
          x => x.key === key
        );

        if (!_.isEmpty(subFormQuestionKey)) {
          const visibleQuestions = getVisibleQuestions(this.state, this.props);
          const studyFormComponent = visibleQuestions.find(
            qs => qs.components && qs.key === key
          );
          const subFormQuestion = projectConfig.studyForm?.subForms?.[
            subFormName
          ]?.components?.find(x => x.key === subFormQuestionKey);
          subFormQuestion.isSubForm = true;
          this.onValueChange(
            studyFormComponent,
            qOption.value,
            subFormQuestion,
            annotationId,
            shouldTriggerAutoSave
          );
        } else {
          this.onValueChange(
            question,
            qOption.value,
            null,
            null,
            shouldTriggerAutoSave
          );
        }
        store.dispatch(setMeasurementDeleted(false));
        if (projectConfig.studyFormWorkflow === 'Mixed' && subFormQuestionKey) {
          const allMeasurements = state.timepointManager.measurements;
          const measurementList = this.formatMeasurementList(
            allMeasurements,
            annotationId
          );
          this.setState({
            subFormDialogMeasurementList: measurementList,
          });
        }

        if (shouldTriggerAutoSave) {
          setTimeout(() => {
            this.saveWhileQuestionSwitches();
          }, 100);
        }
      }
    );
  };

  onDraftStudy = () => {
    const { UIDialogService } = this.props.servicesManager.services;
    const dialogWidth = 500;

    if (this.roiSubFormDialogId) {
      UIDialogService.dismiss({ id: this.roiSubFormDialogId });
      this.roiSubFormDialogId = null;
    }

    if (this.roiQuestionDialogId) {
      UIDialogService.dismiss({ id: this.roiQuestionDialogId });
      this.roiQuestionDialogId = null;
    }

    const dialogId = UIDialogService.create({
      content: SimpleDialog,
      defaultPosition: {
        x: window.innerWidth / 2 - dialogWidth / 2,
        y: window.innerHeight / 2 - 220,
      },
      showOverlay: true,
      contentProps: {
        componentStyle: { width: dialogWidth + 'px' },
        headerTitle: 'Confirm Draft?',
        showFooterButtons: true,
        confirmButtonTitle: 'Proceed',
        enableConfirm: true,
        children:
          'Saving form progress does not submit your completed study for review. You will be able to open the study in its current state and continue your evaluation at any time. Would you like to proceed?',
        onClose: () => UIDialogService.dismiss({ id: dialogId }),
        onConfirm: () => {
          UIDialogService.dismiss({ id: dialogId });
          const { projectConfig } = this.props;
          let questions = {};
          const state = store.getState();
          const isFile = isCurrentFile(state);
          if (!isFile && projectConfig.bSlices) {
            questions.slices = {};
            const bSlices = getBSlices(projectConfig);
            const formState = _.cloneDeep(this.state);
            for (const sliceNo of bSlices) {
              formState.currentSliceInfo.sliceNumber = sliceNo;
              questions.slices[sliceNo] = getVisibleQuestions(
                formState,
                this.props
              );
            }
          } else {
            const props = {
              ...this.props,
              previousVisibleQuestion: this.state.previousQuestion,
              sliceInfo: this.props.sliceInfo,
            };
            questions = getVisibleQuestions(this.state, props);
          }
          const { errors, notes } = validateForm(
            questions,
            this.state.notes,
            this.props.measurementLabels,
            projectConfig,
            this.props,
            this.state
          );
          if (!isFile && projectConfig.bSlices) {
            const bSlices = getBSlices(projectConfig);
            for (const sliceNo of bSlices) {
              if (
                errors.slices[sliceNo] &&
                Object.keys(errors.slices[sliceNo]).length > 0
              ) {
                this.setState({
                  isComplete: false,
                  submit: 'Submit',
                  readerTaskStatus: readStatus.Inprogress,
                  questionValidation: true,
                });
              }
            }
          } else {
            if (Object.keys(errors).length > 0) {
              this.setState({
                isComplete: false,
                submit: 'Submit',
                readerTaskStatus: readStatus.Inprogress,
                questionValidation: true,
              });
            }
          }
          const readTime = this.props.getReadTime();
          const slice = this.state.currentSliceInfo?.sliceNumber || 1;
          const prevStatus = this.state.readerTaskStatus;
          const { taskId } = getRouteParams();
          const displayProperties =
            taskId && !shouldIgnoreUserSettings(state)
              ? getDisplayProperties(this.props.commandsManager, state)
              : null;
          updateComponentByPromise(
            {
              init: {
                errors: {},
                draftSave: 'Saving...',
                draftMessage: 'Please complete required items.',
              },
              success: { draftSave: 'Saved', draftMessage: '' },
              error: {
                draftSave: 'Problems saving',
              },
              default: { draftSave: null, draftMessage: '' },
            },
            this.props.saveStudy.bind(
              undefined,
              this.state.readOnly ? null : notes,
              readTime,
              readStatus.Inprogress,
              slice,
              displayProperties
            ),
            (state, error) => {
              if (error) {
                this.props.showUserErrorMessage(error);
              }
              if (state.draftSave === 'Saved' && taskId) {
                sendTaskSignals(
                  taskId,
                  readStatus.Inprogress,
                  prevStatus,
                  this.props,
                  readStatus
                );
              }
              this.setState(state);
            }
          );
        },
      },
    });
  };

  onCompleteStudy = () => {
    const { projectConfig } = this.props;
    let questions = {};
    const state = store.getState();
    const isFile = isCurrentFile(state);
    const props = {
      ...this.props,
      previousVisibleQuestion: this.state.previousQuestion,
      sliceInfo: this.props.sliceInfo,
    };
    if (!isFile && projectConfig.bSlices) {
      questions.slices = {};
      const bSlices = getBSlices(projectConfig);
      const formState = _.cloneDeep(this.state);
      for (const sliceNo of bSlices) {
        formState.currentSliceInfo.sliceNumber = sliceNo;
        questions.slices[sliceNo] = getVisibleQuestions(formState, props);
      }
    } else {
      questions = getVisibleQuestions(this.state, props);
    }
    const { errors, notes, allLabels, allTools } = validateForm(
      questions,
      this.state.notes,
      this.props.measurementLabels,
      projectConfig,
      this.props,
      this.state
    );
    if (!isFile && projectConfig.bSlices) {
      const bSlices = getBSlices(projectConfig);
      const requiredError = bSliceRequiredValidation(
        projectConfig,
        bSlices,
        this.state.notes
      );
      const data = bSliceQuestionValidation(requiredError, projectConfig);
      if (data !== '') {
        this.setState({
          errors,
          save: 'Validation Failed',
          draftMessage: data,
          questionValidation: false,
        });
        setTimeout(() => {
          this.setState({ save: null });
        }, 2000);

        this.props.setAvailableLabels(allLabels, allTools);
        this.props.setActiveTool();
        return;
      }
      const visibleQuestions = getVisibleQuestions(this.state, props);
      for (const sliceNo of bSlices) {
        if (
          errors.slices?.[sliceNo] &&
          Object.keys(errors.slices?.[sliceNo]).length > 0
        ) {
          this.errorCreation(
            true,
            errors,
            sliceNo,
            allLabels,
            allTools,
            visibleQuestions,
            this.questionRefs
          );
          return;
        }
      }
    } else {
      if (Object.keys(errors).length > 0) {
        const visibleQuestions = getVisibleQuestions(this.state, props);
        this.errorCreation(
          false,
          errors,
          0,
          allLabels,
          allTools,
          visibleQuestions,
          this.questionRefs
        );
        return;
      }
    }
    const readTime = this.props.getReadTime();
    const slice = this.state.currentSliceInfo?.sliceNumber || 1;
    const prevStatus = this.state.readerTaskStatus;
    const { taskId } = getRouteParams();
    const displayProperties =
      taskId && !shouldIgnoreUserSettings(state)
        ? getDisplayProperties(this.props.commandsManager, state)
        : null;
    updateComponentByPromise(
      {
        init: { errors: {}, save: 'Saving...' },
        success: { save: 'Saved' },
        error: { save: 'Problems saving' },
        default: { save: null },
      },
      this.props.saveStudy.bind(
        undefined,
        this.state.readOnly ? null : notes,
        readTime,
        readStatus.Complete,
        slice,
        displayProperties
      ),
      (state, error) => {
        if (error) {
          this.props.showUserErrorMessage(error);
        }
        if (state.save === 'Saved' && taskId) {
          sendTaskSignals(
            taskId,
            readStatus.Complete,
            prevStatus,
            this.props,
            readStatus
          );
        }
        this.setState(state);
      }
    );
    this.setState({
      isComplete: true,
      submit: 'Resubmit',
      readerTaskStatus: readStatus.Complete,
      questionValidation: true,
    });
  };

  errorCreation = (
    isbSlice,
    errors,
    sliceNo,
    allLabels,
    allTools,
    visibleQuestions,
    questionRefs
  ) => {
    const errorData = errorSetting(
      isbSlice,
      errors,
      sliceNo,
      visibleQuestions,
      questionRefs,
      this.props,
      this.state.notes
    );

    if (errorData?.isbSlice) {
      this.setState({
        errors,
        save: 'Validation Failed',
        draftMessage: errorData?.message,
        questionValidation: false,
      });
    } else {
      this.setState({
        errors,
        save: 'Validation Failed',
        draftMessage: errorData?.message,
        questionValidation: false,
      });
    }

    setTimeout(() => {
      this.setState({ save: null });
    }, 2000);

    let availableTools = allTools;
    if (this.props.projectConfig.studyFormWorkflow) {
      const studyFormQuestionTools = getStudyFormActiveToolsForEnable(
        this.props.projectConfig,
        this.props,
        null,
        this.toolsWithLabels
      );
      let tools = studyFormQuestionTools?.length
        ? studyFormQuestionTools
        : null;
      availableTools = tools;
    }

    this.props.setAvailableLabels(allLabels, availableTools);
    this.props.setActiveTool();
  };

  confirmNavigation = callback => {
    const { projectConfig } = this.props;
    const savedNotes =
      (this.props.sessionRead && this.props.sessionRead.notes) || {};
    let questions = {};
    const state = store.getState();
    const isFile = isCurrentFile(state);
    if (!isFile && projectConfig.bSlices) {
      questions.slices = {};
      const bSlices = getBSlices(projectConfig);
      const formState = _.cloneDeep(this.state);
      for (const sliceNo of bSlices) {
        formState.currentSliceInfo.sliceNumber = sliceNo;
        questions.slices[sliceNo] = getVisibleQuestions(formState, this.props);
      }
    } else {
      const props = {
        ...this.props,
        previousVisibleQuestion: this.state.previousQuestion,
        sliceInfo: this.props.sliceInfo,
      };
      questions = getVisibleQuestions(this.state, props);
    }
    const { errors } = validateForm(
      questions,
      this.state.notes,
      this.props.measurementLabels,
      this.props.projectConfig
    );
    if (!isFile && projectConfig.bSlices) {
      const bSlices = getBSlices(projectConfig);
      for (const sliceNo of bSlices) {
        if (
          errors.slices[sliceNo] &&
          this.state.notes.slices[sliceNo] &&
          ((Object.keys(this.state.notes.slices[sliceNo]).length &&
            Object.keys(errors.slices[sliceNo]).length) ||
            !isEqual(savedNotes, this.state.notes))
        ) {
          this.setState({
            confirmCallback: () => {
              callback();
              this.setState({ confirmCallback: null });
            },
          });
        } else {
          callback();
        }
      }
    } else {
      if (
        (Object.keys(this.state.notes).length && Object.keys(errors).length) ||
        !isEqual(savedNotes, this.state.notes)
      ) {
        this.setState({
          confirmCallback: () => {
            callback();
            this.setState({ confirmCallback: null });
          },
        });
      } else {
        callback();
      }
    }
  };

  navigateStudy = offset => {
    const { StudyInstanceUID, studyList } = this.props;
    const { projectId } = this.props.match.params;
    const index = studyList.ordering.findIndex(uid => uid === StudyInstanceUID);
    const nextUid = studyList.ordering[index + offset];
    if (nextUid) {
      this.props.history.push({
        pathname: `/project/${projectId}/viewer/${nextUid}`,
      });
    } else {
      this.props.history.push({ pathname: `/project/${projectId}` });
    }
  };

  cleanHTML = html => {
    return html.replace(/<script[^>]*>[^<]*<\/script>/gm, '');
  };

  onAdvanceStudy = () => this.navigateStudy(1);
  onPreviousStudy = () => this.navigateStudy(-1);

  onClickQuestionValues(question, isSubForm = false, subFormName = null) {
    this.previousVisibleQuestion = question;
    const props = {
      ...this.props,
      previousVisibleQuestion: this.state.previousQuestion,
      sliceInfo: this.props.sliceInfo,
    };
    selectQuestionValues(question, props, subFormName, isSubForm, this.state);
  }

  // To Show the color selector dialog
  onClickColorPicker = (event, measurementData) => {
    const color = clickColorPicker(
      window,
      event,
      measurementData,
      DEFAULT_COLOR,
      this.props
    );

    this.setState({
      showColorPicker: !this.state.showColorPicker,
      colorPickCoordinates: color.colorPickCoordinates,
      measurementData,
      selectedColor: color.selectedColor,
    });
  };

  closeColorPicker = () => {
    this.setState({ showColorPicker: false });
  };

  isErrorMessage = message => {
    return message === 'Problems saving';
  };

  render() {
    const { measurementLabels, projectConfig, sessionRead } = this.props;
    const { confirmCallback, readOnly, readerTaskStatus } = this.state;
    const { taskId } = getRouteParams();
    let saveButtonText = this.state.submit ? this.state.submit : 'Submit';
    let draftButtonText = 'Draft';
    if (this.state.save) {
      saveButtonText = this.state.save;
    } else if (this.state.draftSave) {
      draftButtonText = this.state.draftSave;
    } else if (
      sessionRead &&
      sessionRead.notes &&
      Object.keys(sessionRead.notes).length &&
      !readOnly
    ) {
      if (
        (!taskId && sessionRead?.readStatus === readStatus.Inprogress) ||
        (taskId && readerTaskStatus === readStatus.Inprogress)
      ) {
        saveButtonText = 'Submit';
      } else if (
        (!taskId && sessionRead?.readStatus === readStatus.Complete) ||
        (taskId && readerTaskStatus === readStatus.Complete)
      ) {
        saveButtonText = 'Resubmit';
      }
      draftButtonText = 'Draft';
    }
    const slice = this.state.currentSliceInfo?.sliceNumber || 1;
    const draftMessage = this.state.draftMessage;
    const allowDraft = projectConfig?.allowDraft !== false;
    const showNavButtons = projectConfig.showStudyList !== false; // show if true or undefined
    const formStyles = {
      pointerEvents: isFormViewOnly(this.state) ? 'none' : 'auto',
      opacity: isFormViewOnly(this.state) ? 0.5 : 1,
    };
    const state = store.getState();
    const isProtocolDeleted = state.flywheel.isProtocolDeleted;
    let isSaveDisabled =
      isProtocolDeleted ||
      this.state.save ||
      (isFormViewOnly(this.state) && isAnnotationsViewOnly(this.state));
    const props = {
      ...this.props,
      previousVisibleQuestion: this.state.previousQuestion,
      sliceInfo: this.props.sliceInfo,
    };
    const multipleTaskNotes = { ...state.viewerAvatar.multipleTaskNotes };
    const taskIds = state.multipleReaderTask?.TaskIds;
    const visibleQuestions = getVisibleQuestions(this.state, props);
    const displayStyle = this.props.showStudyFormButtonsOnly ? 'none' : '';
    const isMultipleReaderTask = taskIds.length > 1;
    const singleAvatar = state.viewerAvatar.singleAvatar;
    if (isMultipleReaderTask && !singleAvatar) {
      isSaveDisabled = true;
    }
    return (
      <>
        <div
          key={`${projectConfig.studyFormWorkflow}`}
          className="study-completion-form"
          // Special handling to clear the focus from StudyForm on leaving mouse
          onMouseLeave={() => document.activeElement?.blur()}
        >
          <div
            style={{
              display: displayStyle,
            }}
          >
            <Header
              title="Viewer Form"
              measurements={this.props.measurements}
              slice={slice}
            />
          </div>
          <div
            style={{
              display: displayStyle,
            }}
            className="study-form-scrollable"
            ref={element => (this.scrollableRef = element)}
          >
            {visibleQuestions.map((question, index) =>
              this.studyQuestionAndAnswer(
                question,
                index,
                measurementLabels,
                readOnly,
                slice,
                formStyles,
                projectConfig,
                multipleTaskNotes,
                taskIds
              )
            )}
          </div>
          <div className="study-completion-buttons">
            {(showNavButtons && !isMultipleReaderTask) ||
            (showNavButtons && isMultipleReaderTask && singleAvatar) ? (
              <button
                type="button"
                onClick={() => this.confirmNavigation(this.onPreviousStudy)}
                className="study-complete-button"
                style={{ flex: 1 }}
              >
                &lt;&lt; {this.props.t('Back')}
              </button>
            ) : null}
            {allowDraft ? (
              <React.Fragment>
                <button
                  type="button"
                  onClick={this.onDraftStudy}
                  disabled={isSaveDisabled}
                  className="study-complete-button"
                  style={{
                    flex: 1,
                    opacity: isSaveDisabled ? '0.5' : '1',
                    pointerEvents: isSaveDisabled ? 'none' : 'auto',
                    fontWeight: this.isErrorMessage(draftButtonText)
                      ? 'bold'
                      : 'normal',
                  }}
                >
                  {this.props.t(draftButtonText)}
                </button>
                {!this.state.questionValidation && draftMessage ? (
                  <OverlayTrigger
                    key="menu-button"
                    placement="top"
                    rootClose={true}
                    overlay={
                      <Tooltip
                        placement="left"
                        className="in tooltip-warning"
                        id="tooltip-left"
                      >
                        <>
                          <div className="warningTitle">Validation Failed</div>
                          <div className="warningContent">
                            {this.props.t(draftMessage)}
                          </div>{' '}
                        </>
                      </Tooltip>
                    }
                  >
                    <button
                      type="button"
                      onClick={this.onCompleteStudy}
                      disabled={isSaveDisabled}
                      className="study-complete-button"
                      style={{
                        flex: 1,
                        opacity: '0.5',
                        pointerEvents: isSaveDisabled ? 'none' : 'auto',
                      }}
                    >
                      {this.props.t(saveButtonText)}
                    </button>
                  </OverlayTrigger>
                ) : (
                  <button
                    type="button"
                    onClick={this.onCompleteStudy}
                    disabled={isSaveDisabled}
                    className="study-complete-button"
                    style={{
                      flex: 1,
                      opacity: isSaveDisabled ? '0.5' : '1',
                      pointerEvents: isSaveDisabled ? 'none' : 'auto',
                    }}
                  >
                    {this.props.t(saveButtonText)}
                  </button>
                )}
              </React.Fragment>
            ) : null}
            {!allowDraft ? (
              <button
                type="button"
                onClick={this.onCompleteStudy}
                disabled={isSaveDisabled}
                className="study-complete-button"
                style={{ flex: 2 }}
              >
                {this.props.t(saveButtonText)}
              </button>
            ) : null}
            {(showNavButtons && !isMultipleReaderTask) ||
            (showNavButtons && isMultipleReaderTask && singleAvatar) ? (
              <button
                type="button"
                onClick={() => this.confirmNavigation(this.onAdvanceStudy)}
                className="study-complete-button"
                style={{ flex: 1 }}
              >
                {this.props.t('Next')} &gt;&gt;
              </button>
            ) : null}
          </div>
        </div>
        {confirmCallback && (
          <SimpleDialog
            componentStyle={{ bottom: 0, right: 0, top: 'auto', left: 'auto' }}
            headerTitle="Unsaved Changes"
            onClose={() => this.setState({ confirmCallback: null })}
            onConfirm={confirmCallback}
            showFooterButtons={true}
          >
            Unsaved changes from the form will be lost. Would you like to
            proceed?
          </SimpleDialog>
        )}
        <ColorPickerDialog
          closeColorPicker={this.closeColorPicker}
          showColorPicker={this.state.showColorPicker}
          measurementData={this.state.measurementData}
          colorPickCoordinates={this.state.colorPickCoordinates}
          selectedColor={this.state.selectedColor}
          {...this.props}
        />
      </>
    );
  }

  getInstructionSet(question, slice, sliceInfo, currentQuestion) {
    const state = store.getState();
    if (
      !question.values ||
      (question &&
        question.values &&
        !question.values.some(x => x.instructionSet || x.directive))
    ) {
      return null;
    }
    if (!question.values.some(x => x.instructionSet || x.directive)) {
      return null;
    }
    const questionAnswer = this.state.notes?.slices
      ? this.state.notes.slices[slice]?.[question.key] || ''
      : this.state.notes[question.key] || '';

    let qn;
    if (typeof questionAnswer === 'object') {
      qn = question.values.find(x =>
        Object.keys(x).find(
          key =>
            (key === 'directive' || key === 'instructionSet') &&
            (questionAnswer.value === x.label ||
              questionAnswer.value === x.value)
        )
      );
    } else {
      qn = question.values.find(x =>
        Object.keys(x).find(
          key =>
            (key === 'directive' || key === 'instructionSet') &&
            (questionAnswer === x.label || questionAnswer === x.value)
        )
      );
    }

    const selectedOption = getSelectedOptionForQuestion(
      question,
      sliceInfo?.sliceNumber,
      ''
    );

    if (qn?.directive) {
      return (
        <ConnectedInstructionContainer
          instruction={qn}
          question={question}
          notes={this.state.notes}
          sliceInfo={sliceInfo}
          servicesManager={this.props.servicesManager}
          groupClickStatus={this.state.showInstructionColorPicker}
          onGroupClick={state =>
            this.setState({ showInstructionColorPicker: { ...state } })
          }
          onClickColorPicker={this.onClickColorPicker}
          commandsManager={this.props.commandsManager}
          errors={
            this.state.errors.slices &&
            this.state.errors.slices[slice] &&
            this.state.errors.slices[slice][question.key]
              ? this.state.errors.slices[slice][question.key]
              : this.state.errors[question.key]
          }
          instructionSet={[]}
          measurements={this.props.measurements}
          currentTool={state.infusions.activeTool}
          questionReactivateByPencilIcon={this.questionReactivateByPencilIcon}
          isInstructionAvailableInSelectedQuestion={
            currentQuestion?.key === question?.key
          }
          setLimitReachedDialogId={this.setLimitReachedDialogId}
          limitReachedDialogId={
            state.infusions?.limitReachedDialog?.limitReachedDialogId
          }
          selectedOption={selectedOption}
        />
      );
    }
    if (qn?.instructionSet) {
      return qn.instructionSet.map((x, index) => (
        <ConnectedInstructionContainer
          key={index}
          notes={this.state.notes}
          instructionIndex={index}
          instruction={x}
          question={question}
          sliceInfo={sliceInfo}
          servicesManager={this.props.servicesManager}
          commandsManager={this.props.commandsManager}
          groupClickStatus={this.state.showInstructionColorPicker}
          onGroupClick={state =>
            this.setState({ showInstructionColorPicker: { ...state } })
          }
          onClickColorPicker={this.onClickColorPicker}
          errors={
            this.state.errors.slices &&
            this.state.errors.slices[slice] &&
            this.state.errors.slices[slice][question.key]
              ? this.state.errors.slices[slice][question.key]
              : this.state.errors[question.key]
          }
          instructionSet={qn.instructionSet}
          measurements={this.props.measurements}
          currentTool={state.infusions.activeTool}
          questionReactivateByPencilIcon={this.questionReactivateByPencilIcon}
          isInstructionAvailableInSelectedQuestion={
            currentQuestion?.key === question?.key
          }
          setLimitReachedDialogId={this.setLimitReachedDialogId}
          limitReachedDialogId={
            state.infusions?.limitReachedDialog?.limitReachedDialogId
          }
          selectedOption={selectedOption}
        />
      ));
    }
    return null;
  }

  setLimitReachedDialogId = (dialogId, newMeasures) => {
    const limitReachedDialog = {
      limitReachedDialogId: dialogId,
      measurementList: newMeasures,
    };
    store.dispatch(setLimitReachedDialog(limitReachedDialog));
  };

  questionReactivateByPencilIcon = (question, sliceInfo, measurementTools) => {
    const { projectConfig } = this.props;
    const { currentQuestion } = this.state;
    const state = store.getState();
    const isFile = isCurrentFile(state);
    let notes;

    if (!isFile && projectConfig.bSlices) {
      notes = this.state.notes.slices?.[sliceInfo.sliceNumber];
    } else {
      notes = this.state.notes;
    }

    const answer =
      typeof notes?.[question.key] === 'object'
        ? notes?.[question.key].value
        : notes?.[question.key];
    const option = question.values?.find(data => data.value === answer);
    let tool = 'Wwwc';
    this.props.setActiveTool(measurementTools[0]);
    if (
      !currentQuestion ||
      currentQuestion.key !== question.key ||
      !measurementTools?.includes(state.infusions?.activeTool)
    ) {
      tool = null;
      this.clickType(option, question.key);
    }

    if (tool) {
      this.props.setActiveTool(tool);
    }
  };

  subFormPanelSwitch = (questionKey, slice, projectConfig) => {
    const state = store.getState();
    let subFormPanel = { ...this.state.subFormPanel };
    if (!isCurrentFile(state) && projectConfig.bSlices) {
      if (!subFormPanel[slice]) {
        subFormPanel[slice] = {};
      }
      subFormPanel[slice][questionKey] = !subFormPanel[slice]?.[questionKey];
    } else {
      subFormPanel[questionKey] = !subFormPanel?.[questionKey];
    }
    this.setState({ subFormPanel });
  };

  subFormTabSwitch = (keyIndex, key, slice, projectConfig, tabAnnotationId) => {
    const state = store.getState();
    let subFormTab = _.cloneDeep({ ...this.state.subFormTab });
    let subFormTabWithAnnotationId = {
      ...this.state.subFormTabWithAnnotationId,
    };
    const isFile = isCurrentFile(state);
    let previousTab;

    if (!isFile && projectConfig.bSlices) {
      if (!subFormTab[slice]) {
        subFormTab[slice] = {};
      }

      subFormTab[slice][key] = keyIndex;

      if (!subFormTabWithAnnotationId[slice]) {
        subFormTabWithAnnotationId[slice] = {};
      }

      subFormTabWithAnnotationId[slice][key] = tabAnnotationId;
      previousTab = this.state.subFormTab[slice]?.[key];
    } else {
      subFormTab[key] = keyIndex;
      subFormTabWithAnnotationId[key] = tabAnnotationId;
      previousTab = this.state.subFormTab?.[key];
    }

    if (previousTab === keyIndex) {
      this.setState({
        subFormTab,
        subFormTabWithAnnotationId,
      });
    } else {
      this.setState({
        subFormTab,
        subFormTabWithAnnotationId,
        currentQuestion: null,
        currentKey: null,
      });
      this.props.setAvailableLabels([''], []);
      store.dispatch(setActiveQuestion(''));
    }
  };
  formatTabMeasurements(measurementTools, tabAnnotationId) {
    const measurements = [];
    measurementTools.forEach(tool => {
      this.props.measurements?.[tool]?.forEach(annotation => {
        if (annotation?.uuid === tabAnnotationId) {
          annotation.measurementId = annotation._id;
          measurements.push(annotation);
        }
      });
    });
    return measurements;
  }
  studyQuestionAndAnswer(
    question,
    index,
    measurementLabels,
    readOnly,
    slice,
    formStyles,
    projectConfig,
    multipleTaskNotes,
    taskIds
  ) {
    const answerAndTabData = getQuestionAnswerAndTabDetails(
      projectConfig,
      this.state,
      slice,
      question
    );
    const subFormTabs = answerAndTabData.subFormTabs;
    const questionAnswer = answerAndTabData.questionAnswer;
    const state = store.getState();
    const isFile = isCurrentFile(state);
    let showSubFormPanel = false;
    const studyForm = projectConfig?.studyForm?.components;
    const note = this.state.notes.slices
      ? this.state.notes.slices?.[slice]?.[question.key]
      : this.state.notes?.[question.key];
    const answerFromSessionNotes = typeof note === 'object' ? note.value : note;
    const data = studyForm.find(item => item.key === question.key);
    const dataValue =
      data &&
      data.values?.find(value => value.value === answerFromSessionNotes);
    const annotationIds = question[question.key];
    let answer = [];
    if (annotationIds) {
      Object.keys(annotationIds).forEach(key => {
        const measures = measurementApi.Instance.getAllMeasurements().find(
          measurement =>
            projectConfig.bSlices
              ? annotationIds[key].find(
                  annotation => annotation.annotationId === measurement.uuid
                )
              : measurement.uuid === annotationIds[key].annotationId
        );
        answer.push(
          typeof measures?.answer === 'object'
            ? measures?.answer.value
            : measures?.answer
        );
      });
    }
    if (
      question?.subFormName &&
      dataValue?.subForm &&
      question.subFormName === dataValue.subForm &&
      answer?.includes(answerFromSessionNotes)
    ) {
      showSubFormPanel = true;
    }
    if (
      (projectConfig?.studyFormWorkflow === 'Form' ||
        projectConfig?.studyFormWorkflow === 'Mixed') &&
      question.components?.length > 0 &&
      subFormTabs?.[question.key] &&
      question.positiveAnswers?.includes(questionAnswer) &&
      showSubFormPanel
    ) {
      let tabs = [];
      const subFormTabList =
        !isFile && projectConfig.bSlices
          ? question[question.key][slice]
          : question[question.key];
      for (let i = 0; i < subFormTabList.length; i++) {
        let tab = {
          name: i + 1,
          annotationId: subFormTabList[i].annotationId,
        };
        tabs.push(tab);
      }
      const subFormPanel =
        !isFile && projectConfig.bSlices
          ? this.state.subFormPanel[slice]
          : this.state.subFormPanel;
      return (
        <div key={`${index}_${question.key}`} className="subFormHeader">
          <SubFormPanel
            subFormPanelSwitch={() =>
              this.subFormPanelSwitch(question.key, slice, projectConfig)
            }
            slice={slice}
            question={question}
            studyFormState={this.state}
            t={this.props.t}
            projectConfig={projectConfig}
            measurements={this.props.measurements}
            readOnly={readOnly}
          />
          {subFormPanel?.[question.key] && (
            <div className="TabComponents">
              <div className="TabComponents_tabHeader">
                <div className="TabComponents_tabHeader_selector">
                  <div className="dialog-separator-after">
                    <ul className="nav nav-tabs">
                      {tabs.map((tab, tabPanelIndex) => {
                        const tabAnnotationId = tab.annotationId;
                        return (
                          <SubFormTabHeader
                            key={`${question.key}_${tabPanelIndex}_${tabAnnotationId}`}
                            slice={slice}
                            index={tabPanelIndex}
                            question={question}
                            tab={tab}
                            tabMouseEnter={e => {
                              if (!dispatchActions) {
                                dispatchActions = require('@ohif/viewer/src/appExtensions/MeasurementsPanel/dispatchActions.js')
                                  .default;
                              }
                              const measurements = this.formatTabMeasurements(
                                measurementTools,
                                tabAnnotationId
                              );
                              store.dispatch(
                                setMeasurementColorIntensity(true)
                              );
                              dispatchActions?.dispatchActiveHoveredMeasurements(
                                measurements,
                                store.dispatch
                              );
                            }}
                            tabMouseLeave={e => {
                              store.dispatch(
                                setMeasurementColorIntensity(false)
                              );
                              dispatchActions?.dispatchActiveHoveredMeasurements(
                                [],
                                store.dispatch
                              );
                            }}
                            tabClick={() =>
                              this.subFormTabSwitch(
                                question.key + tabPanelIndex,
                                question.key,
                                slice,
                                projectConfig,
                                tabAnnotationId
                              )
                            }
                            studyFormState={this.state}
                            projectConfig={projectConfig}
                            readOnly={readOnly}
                          />
                        );
                      })}
                    </ul>
                  </div>
                </div>
              </div>
              {tabs.map((tab, tabIndex) => {
                const { annotationId } = tab;
                const subFormTab =
                  !isFile && projectConfig.bSlices
                    ? this.state.subFormTab[slice]
                    : this.state.subFormTab;
                const subFormComponent =
                  !isFile && projectConfig.bSlices
                    ? question[question.key][slice]
                    : question[question.key];
                return (
                  <div
                    key={`${tabIndex}_${annotationId}_${index}_${question.key}`}
                    className={classnames(
                      'TabComponents_content',
                      question.key + tabIndex === subFormTab?.[question.key] &&
                        'active'
                    )}
                  >
                    {subFormComponent[tabIndex].annotationId &&
                      question.components.map(
                        (subFormQuestion, subFormIndex) => {
                          const questionDisplay = isQuestionDisplay(
                            subFormQuestion,
                            annotationId,
                            slice,
                            question,
                            projectConfig,
                            this.state.notes
                          );
                          const noteData = getValueFromNoteAndSubFormName(
                            this.state,
                            slice,
                            question,
                            projectConfig,
                            annotationId
                          );
                          let notesForSubForm = noteData.notesForSubForm;
                          let subFormName = noteData.subFormName;
                          if (questionDisplay) {
                            return (
                              <SubFormTabContent
                                key={`${subFormQuestion.key}_${tabIndex}_${question.key}_${subFormIndex}`}
                                subFormQuestion={subFormQuestion}
                                index={tabIndex}
                                slice={slice}
                                question={question}
                                annotationId={annotationId}
                                projectConfig={projectConfig}
                                studyFormState={this.state}
                                questionRefs={this.questionRefs}
                                formStyles={formStyles}
                                cleanHTML={() =>
                                  this.cleanHTML(subFormQuestion.html)
                                }
                                measurementLabels={measurementLabels}
                                radioOnClick={option => {
                                  const subFormQuestionKey =
                                    subFormQuestion.key;
                                  const studyFormQuestionKey = question.key;
                                  this.clickType(
                                    option,
                                    studyFormQuestionKey,
                                    subFormQuestionKey,
                                    subFormName,
                                    annotationId
                                  );
                                  this.onClickQuestionValues(
                                    subFormQuestion,
                                    true,
                                    subFormName
                                  );
                                }}
                                selectBoxOnClick={e => {
                                  const subFormQuestionKey =
                                    subFormQuestion.key;
                                  const studyFormQuestionKey = question.key;
                                  const selectedOption = subFormQuestion?.values?.find(
                                    x =>
                                      x.value === e.target.value ||
                                      x.label === e.target.value
                                  );
                                  if (selectedOption) {
                                    this.clickType(
                                      selectedOption,
                                      studyFormQuestionKey,
                                      subFormQuestionKey,
                                      subFormName,
                                      annotationId
                                    );
                                    this.onClickQuestionValues(
                                      subFormQuestion,
                                      true,
                                      subFormName
                                    );
                                  }

                                  this.handleOnClickQuestion(
                                    subFormQuestion,
                                    true
                                  );
                                }}
                                onClickQuestionValues={(question, option) => {
                                  this.clickType(
                                    option,
                                    question.key,
                                    subFormQuestion.key,
                                    subFormName,
                                    annotationId
                                  );
                                  this.onClickQuestionValues(
                                    subFormQuestion,
                                    true,
                                    subFormName
                                  );
                                }}
                                readOnly={readOnly}
                                getValueSubForm={() =>
                                  getValueSubForm(
                                    slice,
                                    subFormQuestion,
                                    question.key,
                                    subFormName,
                                    annotationId,
                                    this.state
                                  )
                                }
                                formatOptions={() =>
                                  formatOptions(
                                    subFormQuestion.values,
                                    DROPDOWN_DEFAULT_VALUE
                                  )
                                }
                                notesForSubForm={notesForSubForm}
                                multipleTaskNotes={multipleTaskNotes}
                                taskIds={taskIds}
                                measurements={this.props.measurements}
                                onTextChange={(
                                  question,
                                  value,
                                  subFormQuestion,
                                  annotationId,
                                  subFormName
                                ) => {
                                  this.onTextChange(
                                    question,
                                    value,
                                    subFormQuestion,
                                    annotationId,
                                    subFormName
                                  );
                                }}
                                onClick={question => {
                                  this.handleOnClickQuestion(question, true);
                                }}
                              />
                            );
                          }
                        }
                      )}
                  </div>
                );
              })}
            </div>
          )}
        </div>
      );
    } else {
      if (!question.components) {
        return (
          <StudyFormContent
            key={`${question.key}_${index}`}
            question={question}
            index={index}
            questionRefs={this.questionRefs}
            studyFormState={this.state}
            slice={slice}
            readOnly={readOnly}
            measurementLabels={measurementLabels}
            projectConfig={projectConfig}
            cleanHTML={question => this.cleanHTML(question.html)}
            radioOnClick={(option, question) => {
              this.clickType(option, question.key);
              this.onClickQuestionValues(question);
            }}
            getValue={(slice, question) =>
              getValue(slice, question, this.state)
            }
            selectBoxOnChange={(option, question) => {
              this.clickType(option, question.key);
              this.onClickQuestionValues(question);
            }}
            formatOptions={question =>
              formatOptions(question.values, DROPDOWN_DEFAULT_VALUE)
            }
            onClickQuestionValues={(question, option) => {
              this.clickType(option, question.key);
              this.onClickQuestionValues(question);
            }}
            getInstructionSet={(question, slice, studyFormState) =>
              this.getInstructionSet(
                question,
                slice,
                studyFormState.currentSliceInfo,
                studyFormState.currentQuestion
              )
            }
            formStyles={formStyles}
            multipleTaskNotes={multipleTaskNotes}
            taskIds={taskIds}
            measurements={this.props.measurements}
            onTextChange={(question, value) => {
              this.onTextChange(question, value);
            }}
            onClick={() => {
              this.handleOnClickQuestion(question, false);
            }}
          />
        );
      }
    }
  }

  componentWillUnmount() {
    let confirmationDialogData = {};
    let subFormDialogData = {};
    const state = store.getState();

    cornerstone.events.removeEventListener(
      cornerstone.EVENTS.ELEMENT_ENABLED,
      this._elementEnabledHandler
    );

    cornerstone.events.removeEventListener(
      cornerstone.EVENTS.ELEMENT_DISABLED,
      this._elementDisabledHandler
    );

    const enabledElements = cornerstone.getEnabledElements();
    enabledElements.forEach(enabledElement => {
      this.subscribeMeasurementRemoved(enabledElement.element, false);
    });

    document.removeEventListener('annotationPanel', this._panelClick);

    document.removeEventListener('formsPanel', this._panelClick);

    document.removeEventListener('labelApplied', this._labelApplied);

    document.removeEventListener('deleteDialog', this._deleteDialog);

    if (this.roiQuestionDialogId) {
      confirmationDialogData = {
        sliceInfo: this.props.sliceInfo,
        currentQuestion: this.state.confirmationDialogCurrentQuestion,
        confirmationDialogRoiAnswer: this.state.confirmationDialogRoiAnswer,
        answer: this.state.answer,
        confirmationDialogMeasurementList: this.state
          .confirmationDialogMeasurementList,
        lastAnsweredValue: this.state.lastAnsweredValue,
      };
    }

    if (this.roiSubFormDialogId) {
      subFormDialogData = {
        subFormDialogCurrentQuestion: this.state.subFormDialogCurrentQuestion,
        subFormDialogRoiAnswer: this.state.subFormDialogRoiAnswer,
        subFormDialogMeasurementList: this.state.subFormDialogMeasurementList,
        subFormDialogQuestion: this.state.currentQuestion,
        sliceInfo: this.props.sliceInfo,
        infusionLabelAndTool: {
          availableLabels: state.infusions?.availableLabels,
          availableTools: state.infusions?.availableTools,
        },
      };
    }

    const panel = {
      isPanelSwitched: true,
      confirmationDialog: this.roiQuestionDialogId ? true : false,
      subFormDialog: this.roiSubFormDialogId ? true : false,
      confirmationDialogData: confirmationDialogData,
      subFormDialogData: subFormDialogData,
    };
    store.dispatch(setPanelSwitched(panel));
    const isPanelSwitched = panel.isPanelSwitched;
    const { UIDialogService } = this.props.servicesManager.services;

    if (isPanelSwitched && this.roiSubFormDialogId) {
      UIDialogService.dismiss({ id: this.roiSubFormDialogId });
      this.roiSubFormDialogId = null;
    }

    if (isPanelSwitched && this.roiQuestionDialogId) {
      UIDialogService.dismiss({ id: this.roiQuestionDialogId });
      this.roiQuestionDialogId = null;
    }

    if (
      isPanelSwitched &&
      state.infusions?.limitReachedDialog?.limitReachedDialogId
    ) {
      UIDialogService.dismiss({
        id: state.infusions?.limitReachedDialog?.limitReachedDialogId,
      });
    }
  }
}

// measurements are mutated when the location/label is set
// so we do our own deep equality check
let cachedLabels = [];

const mapStateToProps = (state, props) => {
  let StudyInstanceUID = '';
  let sessionRead = null;
  const { colorPalette } = state;
  const measurements = state.timepointManager.measurements;
  const isFile = isCurrentFile(state);
  const currentSelectedQuestion = state.infusions.currentSelectedQuestion;
  const featureConfig = state.flywheel.featureConfig;
  const { taskId } = getRouteParams();
  const reader_tasks = featureConfig?.features?.reader_tasks;
  const isReaderTask = taskId && reader_tasks;

  if (isFile) {
    const { fileName, containerId } = props.match.params;
    StudyInstanceUID = isCurrentZipFile(fileName) ? containerId : fileName;
    if (isReaderTask) {
      sessionRead = state.flywheel.readerTaskWithFormResponse
        ? state.flywheel.readerTaskWithFormResponse.info
        : null;
    } else {
      sessionRead = selectSessionFileRead(state);
    }
  } else {
    const uids = props.match.params.studyInstanceUIDs || '';
    StudyInstanceUID = uids.split(',')[0];
    if (isReaderTask) {
      sessionRead = state.flywheel.readerTaskWithFormResponse
        ? state.flywheel.readerTaskWithFormResponse.info
        : null;
    } else {
      sessionRead = selectSessionRead(state);
    }
  }
  const slice = props.sliceInfo?.sliceNumber || 1;

  const projectConfig = getCondensedProjectConfig();

  const allMeasurements = selectMeasurementsByStudyUid(state);
  let studyMeasurements = {};

  Object.keys(allMeasurements)?.forEach(studyUID => {
    if (studyUID?.toLowerCase() === StudyInstanceUID?.toLowerCase()) {
      Object.keys(allMeasurements[studyUID]).forEach(key => {
        const toolMeasurement = studyMeasurements[StudyInstanceUID]?.[key]
          ? studyMeasurements[StudyInstanceUID][key]
          : [];
        studyMeasurements[StudyInstanceUID] = {
          ...studyMeasurements[StudyInstanceUID],
          [key]: [...toolMeasurement, ...allMeasurements[studyUID][key]],
        };
      });
    }
  });

  const newLabels = getMeasurementLabels(
    studyMeasurements[StudyInstanceUID] || {},
    projectConfig,
    slice
  );

  if (!isEqual(newLabels, cachedLabels)) {
    if (!isFile && projectConfig.bSlices) {
      cachedLabels = {};
    }
    cachedLabels = newLabels;
  }

  return {
    measurementLabels: cachedLabels,
    projectConfig: projectConfig,
    sessionRead: sessionRead,
    StudyInstanceUID: StudyInstanceUID,
    studyList: selectStudyList(state),
    paletteIndex: colorPalette.paletteIndex,
    palette: colorPalette.palette,
    measurements,
    currentSelectedQuestion,
  };
};

let dispatchActions = null;

const mapDispatchToActions = {
  setActiveTool,
  setAvailableLabels,
  dispatchUpdateRoiColor: roiColor => {
    return updateRoiColors(roiColor);
  },
  dispatchUpdatePaletteColors: measurements => {
    return updatePaletteColors(measurements);
  },
};

const mergeProps = (propsFromState, propsFromDispatch, ownProps) => {
  return {
    ...propsFromState,
    ...ownProps,
    ...propsFromDispatch,
    onChangeColor: (color, measurementData) => {
      if (!dispatchActions) {
        dispatchActions = require('@ohif/viewer/src/appExtensions/MeasurementsPanel/dispatchActions.js')
          .default;
      }
      return dispatchActions?.onChangeColor(color, measurementData);
    },
  };
};

const ConnectedStudyForm = withTranslation('Flywheel')(
  withRouter(
    connect(mapStateToProps, mapDispatchToActions, mergeProps)(StudyForm)
  )
);

export default ConnectedStudyForm;
