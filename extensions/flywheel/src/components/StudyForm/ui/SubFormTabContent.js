import React from 'react';
import classnames from 'classnames';
import store from '@ohif/viewer/src/store';
import { isOptionAvailable } from '../validation/ValidationEngine.js';
import Radio from './Radio.js';
import SelectBox from './SelectBox.js';
import Checkbox from './Checkbox.js';
import TextBox from './TextBox.js';
import TextArea from './TextArea.js';
import Label from './Label.js';
import '../StudyForm.styl';
import '../SubForm.styl';
import AgreementIndicator from './AgreementIndicator/AgreementIndicator.js';
import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';
const { getSelectedAnswer } = FlywheelCommonUtils;

function SubFormTabContent(props) {
  const {
    subFormQuestion,
    index,
    slice,
    question,
    annotationId,
    projectConfig,
    studyFormState,
    questionRefs,
    formStyles,
    cleanHTML,
    measurementLabels,
    radioOnClick,
    selectBoxOnClick,
    onClickQuestionValues,
    readOnly,
    getValueSubForm,
    formatOptions,
    notesForSubForm,
    multipleTaskNotes,
    taskIds,
    measurements,
    onTextChange,
    onClick,
  } = props;
  const state = store.getState();
  let hideSeparator =
    projectConfig.studyForm.hideSeparator === undefined
      ? false
      : projectConfig.studyForm.hideSeparator;
  if (subFormQuestion.hideSeparator !== undefined) {
    hideSeparator = subFormQuestion.hideSeparator;
  }

  const { infusions } = state;
  const { currentSelectedQuestion } = infusions;
  const isCurrentQuestion =
    currentSelectedQuestion?.key === subFormQuestion?.key &&
    annotationId === currentSelectedQuestion?.subFormAnnotationId;
  return (
    <div
      ref={element => (questionRefs[subFormQuestion.key] = element)}
      className={classnames([
        'study-form-question',
        {
          'has-error':
            studyFormState.errors.slices &&
            studyFormState.errors.slices[slice] &&
            studyFormState.errors.slices[slice][question.key]?.[
              question.subFormName
            ]?.find(item => item.annotationId === annotationId)?.[
              subFormQuestion.key
            ]
              ? studyFormState.errors.slices[slice][question.key][
                  question.subFormName
                ].find(item => item.annotationId === annotationId)?.[
                  subFormQuestion.key
                ]
              : studyFormState.errors[question.key]?.[
                  question.subFormName
                ]?.find(item => item.annotationId === annotationId)?.[
                  subFormQuestion.key
                ],
        },
        {
          'active-form': isCurrentQuestion,
        },
      ])}
      style={formStyles}
    >
      <div className="study-form-label">{subFormQuestion.label}</div>
      {(() => {
        if (taskIds.length > 1) {
          return (
            <div
              className={classnames([
                { 'study-form-seperator': !hideSeparator },
                { 'study-form-seperator-current-question': isCurrentQuestion },
              ])}
            >
              {subFormQuestion.type === 'radio' && (
                <AgreementIndicator
                  isOptionAvailable={isOptionAvailable}
                  question={subFormQuestion}
                  notes={studyFormState.notes}
                  slice={slice}
                  readOnly={readOnly}
                  measurementLabels={measurementLabels}
                  projectConfig={projectConfig}
                  onClick={option => radioOnClick(option, subFormQuestion)}
                  studyFormState={studyFormState}
                  multipleTaskNotes={multipleTaskNotes}
                  taskIds={taskIds}
                  index={index}
                  measurements={measurements}
                  notesForSubForm={notesForSubForm}
                />
              )}

              {['text', 'textarea'].includes(subFormQuestion.type) && (
                <TextArea
                  question={subFormQuestion}
                  value={getValueSubForm(
                    slice,
                    subFormQuestion,
                    question.key,
                    question.subFormName,
                    annotationId
                  )}
                  onChange={value =>
                    onTextChange(
                      question,
                      value.target.value,
                      subFormQuestion,
                      annotationId,
                      question.subFormName
                    )
                  }
                  onClick={() => onClick(subFormQuestion)}
                  disabled={readOnly}
                />
              )}
              {subFormQuestion.type === 'textfield' && (
                <TextBox
                  question={subFormQuestion}
                  value={getValueSubForm(
                    slice,
                    subFormQuestion,
                    question.key,
                    question.subFormName,
                    annotationId
                  )}
                  onChange={value =>
                    onTextChange(
                      question,
                      value.target.value,
                      subFormQuestion,
                      annotationId,
                      question.subFormName
                    )
                  }
                  disabled={readOnly}
                />
              )}
            </div>
          );
        } else {
          return (
            <div
              className={classnames([
                { 'study-form-seperator': !hideSeparator },
                { 'study-form-seperator-current-question': isCurrentQuestion },
              ])}
            >
              {subFormQuestion.type === 'content' && (
                <div
                  dangerouslySetInnerHTML={{
                    __html: cleanHTML(),
                  }}
                />
              )}
              {subFormQuestion.type === 'radio' &&
                subFormQuestion.values.map((option, i) => {
                  let checked = false;
                  if (notesForSubForm === undefined) {
                    checked = false;
                  } else {
                    checked =
                      notesForSubForm[subFormQuestion.key] === option.value;
                  }
                  const disabled =
                    readOnly ||
                    !isOptionAvailable(
                      option,
                      measurementLabels,
                      projectConfig,
                      slice,
                      true,
                      studyFormState
                    );
                  return (
                    <Label
                      key={`${option.value}_${i}_${index}_${subFormQuestion.key}_${question.key}`}
                      className={classnames(
                        subFormQuestion.style === 'buttons'
                          ? 'study-form-radio-button'
                          : 'study-form-radio',
                        { checked, disabled }
                      )}
                    >
                      <Radio
                        option={option}
                        question={subFormQuestion}
                        onClick={() => radioOnClick(option)}
                        radioChecked={checked}
                        radioDisabled={disabled}
                        radioReadOnly={readOnly}
                        subFormIndex={index}
                      />
                      {option.label || option.value}
                    </Label>
                  );
                })}
              {subFormQuestion.type === 'dropdown' && (
                <div className="drop-down-container">
                  <SelectBox
                    value={getValueSubForm(
                      slice,
                      subFormQuestion,
                      question.key,
                      question.subFormName,
                      annotationId,
                      studyFormState
                    )}
                    onChange={e => selectBoxOnClick(e)}
                    onClick={e => !isCurrentQuestion && selectBoxOnClick(e)}
                    options={formatOptions(subFormQuestion.values)}
                  />
                </div>
              )}
              {subFormQuestion.type === 'selectboxes' &&
                subFormQuestion.values.map((option, i) => {
                  let stateValue = [];
                  if (notesForSubForm === undefined) {
                    stateValue = [];
                  } else {
                    stateValue = notesForSubForm[subFormQuestion.key];
                  }
                  const checked =
                    (stateValue && stateValue.includes(option.value)) || false;
                  const disabled =
                    readOnly ||
                    !isOptionAvailable(
                      option,
                      measurementLabels,
                      projectConfig,
                      slice,
                      true,
                      studyFormState
                    );
                  return (
                    <Label
                      key={`${option.value}_${i}_${index}_${subFormQuestion.key}_${question.key}`}
                      className={classnames(
                        subFormQuestion.style === 'buttons'
                          ? 'study-form-radio-button'
                          : 'study-form-radio',
                        { checked, disabled }
                      )}
                    >
                      <Checkbox
                        onClick={() => onClickQuestionValues(question, option)}
                        question={subFormQuestion}
                        option={option}
                        checked={checked}
                        disabled={disabled}
                        readOnly={readOnly}
                      />
                      {option.label || option.value}
                    </Label>
                  );
                })}
              {['text', 'textarea'].includes(subFormQuestion.type) && (
                <TextArea
                  question={subFormQuestion}
                  value={getValueSubForm(
                    slice,
                    subFormQuestion,
                    question.key,
                    question.subFormName,
                    annotationId
                  )}
                  onChange={value =>
                    onTextChange(
                      question,
                      value.target.value,
                      subFormQuestion,
                      annotationId,
                      question.subFormName
                    )
                  }
                  onClick={() => onClick(subFormQuestion)}
                  disabled={readOnly}
                />
              )}
              {subFormQuestion.type === 'textfield' && (
                <TextBox
                  question={subFormQuestion}
                  value={getValueSubForm(
                    slice,
                    subFormQuestion,
                    question.key,
                    question.subFormName,
                    annotationId
                  )}
                  onChange={value =>
                    onTextChange(
                      question,
                      value.target.value,
                      subFormQuestion,
                      annotationId,
                      question.subFormName
                    )
                  }
                  onClick={() => onClick(subFormQuestion)}
                  disabled={readOnly}
                />
              )}
            </div>
          );
        }
      })()}
    </div>
  );
}

export default SubFormTabContent;
