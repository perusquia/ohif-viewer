import React, { memo } from 'react';
import classnames from 'classnames';
import store from '@ohif/viewer/src/store';
import _ from 'lodash';
import CircleChart from '../../../CircleChart/CircleChart.js';
import { getPercentColor } from '../../utils/index.js';
import GroupedCheckBox from '../GroupedCheckBox/GroupedCheckBox.js';
import { getInverted } from '../../utils/AgreementIndicatorUtils.js';
import './AgreementIndicator.styl';

function AgreementIndicatorForSelectBox(props) {
  const {
    slice,
    notes,
    onClick,
    readOnly,
    question,
    projectConfig,
    isOptionAvailable,
    measurementLabels,
    studyFormState,
    multipleTaskNotes,
    taskIds,
    index,
    measurements,
    notesForSubForm,
  } = props;

  const state = store.getState();

  return (
    <div className="agreement-container">
      <div className="component-container">
        {taskIds.length > 1 &&
          question.values?.map((option, i) => {
            let checked = false;
            const viewerAvatar = state.viewerAvatar;
            let isSingleUser = viewerAvatar.singleAvatar;
            if (isSingleUser) {
              if (notesForSubForm === undefined) {
                let note = getQuestionNoteFromStudyFormState(
                  notes,
                  slice,
                  question
                );
                note = !note ? [] : note;
                checked = note.includes(option.value);
              } else {
                checked = notesForSubForm?.[question.key]?.includes(
                  option.value
                );
              }
            } else {
              taskIds.map(taskId => {
                let note;
                if (notesForSubForm === undefined) {
                  note = getNoteFromMultipleTask(
                    multipleTaskNotes,
                    taskId,
                    slice,
                    question
                  );
                } else {
                  note = notesForSubForm?.[question.key];
                }
                if (!checked) {
                  note = !note ? [] : note;
                  checked = note.includes(option.value) || false;
                }
              });
            }

            let disabled =
              readOnly ||
              !isOptionAvailable(
                option,
                measurementLabels,
                projectConfig,
                slice,
                false,
                studyFormState
              );

            const taskCount = taskIds.length;

            let newPercentColors = [];
            question.values?.forEach((option, i) => {
              let obj = { count: 0, value: '' };
              taskIds.forEach(taskId => {
                if (multipleTaskNotes?.[taskId]) {
                  let note;
                  if (isSingleUser) {
                    note = getQuestionNoteFromStudyFormState(
                      notes,
                      slice,
                      question
                    );
                  } else {
                    note = getNoteFromMultipleTask(
                      multipleTaskNotes,
                      taskId,
                      slice,
                      question
                    );
                  }
                  note = !note ? [] : note;
                  obj.count = note.includes(option.value)
                    ? obj.count + 1
                    : obj.count;
                  obj.value = note.includes(option.value)
                    ? option.value
                    : option.value;
                }
              });
              newPercentColors.push(obj);
            });
            const total = taskCount;
            const percentColor = newPercentColors?.[i]
              ? getPercentColor(newPercentColors, total, option.value)
              : '';

            if (taskCount > 1) {
              disabled = !isSingleUser;
            }

            return (
              <div
                key={option.value + i}
                className="radio-button-wrapper-multi-task"
              >
                <GroupedCheckBox
                  className={classnames(
                    question.style === 'buttons'
                      ? 'study-form-radio-button'
                      : 'study-form-radio',
                    { checked, disabled }
                  )}
                  percentColor={percentColor}
                  option={option}
                  question={question}
                  onClick={e => {
                    onClick(option, question);
                  }}
                  checked={checked}
                  disabled={disabled}
                  readOnly={readOnly}
                  label={option.label || option.value}
                  multipleTaskNotes={multipleTaskNotes}
                  taskIds={taskIds}
                  index={index}
                  indexValue={i}
                  measurements={measurements}
                  slice={slice}
                  isSingleUser={isSingleUser}
                  notes={notes}
                />
              </div>
            );
          })}
      </div>
      {(() => {
        const taskCount = taskIds.length;
        const newPercentColors = [];
        const viewerAvatar = state.viewerAvatar;
        const isSingleUser = viewerAvatar.singleAvatar;
        const singleTaskId = viewerAvatar.taskId;
        const obj = { count: 1, value: '' };
        let userNote = [];
        Object.keys(multipleTaskNotes)?.forEach((taskId, i) => {
          let note;

          if (isSingleUser && singleTaskId === taskId) {
            note = getQuestionNoteFromStudyFormState(notes, slice, question);
          } else {
            note = getNoteFromMultipleTask(
              multipleTaskNotes,
              taskId,
              slice,
              question
            );
          }

          note = !note ? [] : note;

          if (userNote.length === 0) {
            userNote = note;
          } else {
            const diff = _.xor(userNote, note);
            if (!diff.length) {
              obj.count += 1;
            }
          }
        });
        newPercentColors.push(obj);

        const inverted = getInverted(
          multipleTaskNotes,
          slice,
          question,
          taskCount,
          newPercentColors
        );

        if (Object.keys(multipleTaskNotes).length && question.values?.length) {
          return (
            <div className="chart-container">
              <CircleChart
                options={newPercentColors}
                taskCount={taskCount}
                invert={inverted}
              />
            </div>
          );
        }
      })()}
    </div>
  );
}

const getQuestionNoteFromStudyFormState = (notes, slice, question) => {
  let note = notes.slices
    ? notes.slices?.[slice]?.[question.key]
    : notes?.[question.key];
  return note;
};

const getNoteFromMultipleTask = (
  multipleTaskNotes,
  taskId,
  slice,
  question
) => {
  let note = multipleTaskNotes?.[taskId]
    ? multipleTaskNotes?.[taskId]?.notes?.[0]?.slices
      ? multipleTaskNotes[taskId].notes[0].slices[slice]?.[question.key]
      : multipleTaskNotes?.[taskId]?.notes?.[0]?.[question.key]
    : [];
  return note;
};

export default memo(AgreementIndicatorForSelectBox);
