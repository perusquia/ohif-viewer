import React from 'react';
import store from '@ohif/viewer/src/store';

export default function ViewerFormAvatar(props) {
  const {
    initials,
    taskId,
    avatar,
    avatarStyle,
    avatarClick,
    userAvatarClickedDetails,
    onAvatarMouseEnter,
    dispatchActions,
    measurements,
    user,
    questionKey,
    onAvatarMouseLeave,
  } = props;

  const state = store.getState();
  const isProtocolDeleted = state.flywheel.isProtocolDeleted;
  avatarStyle.pointerEvents = isProtocolDeleted ? 'none' : 'auto';
  avatarStyle.opacity = isProtocolDeleted ? 0.3 : 1;

  return (
    <div className="avatar-wrapper">
      <div
        className={`avatar viewerFormAvatar ${initials + '-' + taskId}`}
        style={avatarStyle}
        onClick={e =>
          avatarClick(
            userAvatarClickedDetails,
            initials,
            taskId,
            e,
            dispatchActions,
            measurements,
            user
          )
        }
        onMouseEnter={e =>
          onAvatarMouseEnter(
            e,
            dispatchActions,
            measurements,
            user,
            questionKey,
            taskId,
            initials
          )
        }
        onMouseLeave={e =>
          onAvatarMouseLeave(e, dispatchActions, measurements, taskId, initials)
        }
      >
        {avatar ? '' : initials}
      </div>
    </div>
  );
}
