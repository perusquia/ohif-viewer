import React from 'react';
import { Select } from '@ohif/ui';

function SelectBox(props) {
  const { value, onChange, onClick, options, displayKey } = props;

  return (
    <Select
      rootClass="drop-down-style"
      showIcon={true}
      value={value}
      onChange={onChange}
      onClick={onClick}
      options={options}
      displayKey={displayKey}
    />
  );
}

export default SelectBox;
