import React from 'react';

function TextArea(props) {
  const { question, value, onChange, onClick, disabled } = props;

  return (
    <textarea
      type="text"
      name={question.key}
      value={value}
      onChange={onChange}
      onClick={onClick}
      disabled={disabled}
      className="study-form-text study-form-textarea"
    ></textarea>
  );
}

export default TextArea;
