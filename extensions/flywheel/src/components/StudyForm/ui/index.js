import Radio from './Radio.js';
import SelectBox from './SelectBox.js';
import Checkbox from './Checkbox.js';
import TextBox from './TextBox.js';
import TextArea from './TextArea.js';
import Label from './Label.js';
import StudyFormContent from './StudyFormContent.js';
import SubFormPanel from './SubFormPanel.js';
import SubFormTabHeader from './SubFormTabHeader.js';
import SubFormTabContent from './SubFormTabContent.js';

const ui = {
  Radio,
  SelectBox,
  Checkbox,
  TextBox,
  TextArea,
  Label,
  StudyFormContent,
  SubFormPanel,
  SubFormTabHeader,
  SubFormTabContent,
};

export default ui;
