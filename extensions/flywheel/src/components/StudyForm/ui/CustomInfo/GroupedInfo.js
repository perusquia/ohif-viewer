import React, { Fragment, useState } from 'react';
import { Icon } from '@ohif/ui/src/elements/Icon';

import { getInfoValue } from '../../utils/CustomInfo';

const GroupedInfo = props => {
  const { info, containers } = props;
  const isObject = info.type === 'Object';
  const isString = info.type === 'String';
  const [isOpen, setIsOpen] = useState(false);
  const onClickHandler = () => {
    setIsOpen(!isOpen);
  };
  const data = isString ? getInfoValue(info, containers) : '';

  return (
    <Fragment>
      <div className="grouped-info">
        {isObject && (
          <Fragment>
            <div className="group-info-heading">
              <span onClick={onClickHandler}>
                <Icon
                  name={isOpen ? 'arrow_drop_down' : 'arrow_drop_up'}
                  className="material-icons"
                  width="14px"
                  height="14px"
                />
              </span>
              <div title={info.label}>{info.label}</div>
            </div>
            {isOpen && (
              <Fragment>
                {info.value.map(value => {
                  return <GroupedInfo info={value} containers={containers} />;
                })}
              </Fragment>
            )}
          </Fragment>
        )}
      </div>
      {isString && (
        <div className="single-info">
          <div title={info.label}>{info.label}</div>
          <div title={data}>{data}</div>
        </div>
      )}
    </Fragment>
  );
};

export default GroupedInfo;
