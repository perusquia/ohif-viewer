import React from 'react';

function Label(props) {
  const { className, children } = props;

  return <label className={className}>{children}</label>;
}

export default Label;
