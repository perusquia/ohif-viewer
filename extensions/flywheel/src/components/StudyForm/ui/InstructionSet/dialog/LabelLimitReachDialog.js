import React from 'react';

import { SimpleDialog } from '@ohif/ui';

import './LabelLimitReachDialog.css';

const LabelLimitReachDialog = ({
  label,
  subTitle,
  onCancel,
  onDelete,
  isDisabledDeleteButton,
}) => {
  return (
    <div>
      <SimpleDialog
        headerTitle={label}
        onClose={() => {}}
        onConfirm={() => {}}
        showFooterButtons={false}
        hideCloseButton={true}
      >
        {subTitle && <div className="labelStyle">{subTitle}</div>}

        <div className="btnContainer">
          <div onClick={onCancel} className="btnStyle">
            Cancel
          </div>

          <div
            className={'btnStyle'}
            style={{ opacity: isDisabledDeleteButton ? 0.5 : 1 }}
            onClick={!isDisabledDeleteButton ? onDelete : undefined}
          >
            Delete and Redraw
          </div>
        </div>
      </SimpleDialog>
    </div>
  );
};

export default LabelLimitReachDialog;
