import { connect } from 'react-redux';
import InstructionContainer from './InstructionContainer.js';
import { getSliceInfo } from '@ohif/viewer/src/appExtensions/MeasurementsPanel/Utils.js';

let dispatchActions = null;

const mapStateToProps = (state, ownProps) => {
  const measurements = state.timepointManager.measurements;
  const labels = state?.flywheel?.projectConfig?.labels || [];
  return {
    measurements,
    ...ownProps,
    labels,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  if (dispatch) {
    if (!dispatchActions) {
      dispatchActions = require('@ohif/viewer/src/appExtensions/MeasurementsPanel/dispatchActions.js')
        .default;
    }
    if (dispatchActions) {
      return {
        dispatchActions: dispatchActions,
        getSliceInfo: getSliceInfo,
      };
    }
  }
  return {};
};

const ConnectedInstructionContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(InstructionContainer);

export default ConnectedInstructionContainer;
