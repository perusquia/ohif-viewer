import React from 'react';
import SingleInfo from './CustomInfo/SingleInfo';
import GroupedInfo from './CustomInfo/GroupedInfo';
import { containersUtils } from '../../../utils';

const { getContainersIds, getContainers } = containersUtils;

function CustomInfoTable(props) {
  const { question } = props;
  const info = question.values;
  const containersIds = getContainersIds();
  const containers = getContainers(containersIds);

  return (
    <div className="custom-info-table">
      {info.map(value => {
        if (value.type === 'Object') {
          return <GroupedInfo info={value} containers={containers} />;
        } else if (value.type === 'String') {
          return <SingleInfo info={value} containers={containers} />;
        }
      })}
    </div>
  );
}

export default CustomInfoTable;
