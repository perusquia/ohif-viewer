import { Utils } from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';

const { isCurrentFile } = Utils;

const updateStudyFormNoteSubFormQuestionDeleted = (
  projectConfig,
  deletedMeasurement,
  lastDeletedMeasurementId,
  studyNotes
) => {
  let deletedValue = '';
  let subForm = '';
  let deletedIndex = -1;
  const state = store.getState();
  if (!isCurrentFile(state) && projectConfig.bSlices) {
    deletedValue =
      studyNotes.slices?.[deletedMeasurement.deletedMeasurementSlice]?.[
        deletedMeasurement?.deletedMeasurementKey
      ]?.value;
    subForm = projectConfig.studyForm.components
      .find(item => item.key === deletedMeasurement?.deletedMeasurementKey)
      ?.values?.find(value => value.value === deletedValue)?.subForm;
    deletedIndex = studyNotes.slices?.[
      deletedMeasurement.deletedMeasurementSlice
    ]?.[deletedMeasurement?.deletedMeasurementKey]?.[subForm]?.findIndex(
      item => item.annotationId === lastDeletedMeasurementId
    );
    if (deletedIndex !== -1) {
      studyNotes.slices?.[deletedMeasurement.deletedMeasurementSlice]?.[
        deletedMeasurement?.deletedMeasurementKey
      ]?.[subForm]?.splice(deletedIndex, 1);
    }
  } else {
    deletedValue = studyNotes[deletedMeasurement?.deletedMeasurementKey]?.value;
    subForm = projectConfig.studyForm.components
      .find(item => item.key === deletedMeasurement?.deletedMeasurementKey)
      ?.values?.find(value => value.value === deletedValue)?.subForm;
    deletedIndex = studyNotes?.[deletedMeasurement?.deletedMeasurementKey]?.[
      subForm
    ]?.findIndex(item => item.annotationId === lastDeletedMeasurementId);
    if (deletedIndex !== -1) {
      studyNotes?.[deletedMeasurement?.deletedMeasurementKey]?.[
        subForm
      ]?.splice(deletedIndex, 1);
    }
  }
  return studyNotes;
};

const setNotes = (
  validOptions,
  state,
  projectConfig,
  notes,
  slice,
  question,
  currentValue
) => {
  if (!isCurrentFile(state) && projectConfig.bSlices) {
    if (!notes.slices) {
      notes.slices = {};
    }
    if (!notes.slices[slice]) {
      notes.slices[slice] = {};
    }
    if (question?.type === 'selectboxes' && Array.isArray(currentValue)) {
      if (!notes.slices[slice][question.key]) {
        notes.slices[slice][question.key] = [];
      }
      notes.slices[slice][question.key] = currentValue || [];
    } else {
      notes.slices[slice][question.key] =
        typeof currentValue === 'object'
          ? {
              value: validOptions.length === 1 ? validOptions[0].value : '',
            }
          : validOptions.length === 1
          ? validOptions[0].value
          : '';
    }
    if (
      validOptions[0]?.subForm &&
      projectConfig.studyFormWorkflow === 'Form' &&
      typeof currentValue === 'object'
    ) {
      notes.slices[slice][question.key][validOptions[0]?.subForm] = [];
    }
  } else {
    if (question?.type === 'selectboxes' && Array.isArray(currentValue)) {
      if (!notes[question.key]) {
        notes[question.key] = [];
      }
      notes[question.key] = currentValue || [];
    } else {
      notes[question.key] =
        typeof currentValue === 'object'
          ? {
              value: validOptions.length === 1 ? validOptions[0].value : '',
            }
          : validOptions.length === 1
          ? validOptions[0].value
          : '';
    }
    if (
      validOptions[0]?.subForm &&
      projectConfig.studyFormWorkflow === 'Form' &&
      typeof currentValue === 'object'
    ) {
      notes[question.key][validOptions[0]?.subForm] = [];
    }
  }
  return notes;
};

// Todo: remove this function and replace with getSelectedAnswer
const getValue = (sliceNumber, question, state) => {
  const notes = state.notes;
  if (notes?.slices) {
    if (typeof notes?.slices?.[sliceNumber]?.[question.key] === 'object') {
      return notes?.slices?.[sliceNumber]?.[question.key].value;
    } else {
      return notes?.slices?.[sliceNumber]?.[question.key];
    }
  } else {
    if (typeof notes?.[question.key] === 'object') {
      return notes[question.key].value;
    } else {
      return notes?.[question.key];
    }
  }
};

// Todo: remove this function and replace with getSelectedAnswer
const getValueSubForm = (
  sliceNumber,
  subFormQuestion,
  questionKey,
  subFormName,
  annotationId,
  state
) => {
  const notes = state.notes;
  let subFormNote;
  if (notes?.slices) {
    subFormNote = notes?.slices?.[sliceNumber]?.[questionKey]?.[
      subFormName
    ]?.find(item => item.annotationId === annotationId);
  } else {
    subFormNote = notes[questionKey]?.[subFormName]?.find(
      item => item.annotationId === annotationId
    );
  }
  return subFormNote?.[subFormQuestion.key]
    ? subFormNote[subFormQuestion.key]
    : '';
};

export {
  updateStudyFormNoteSubFormQuestionDeleted,
  setNotes,
  getValue,
  getValueSubForm,
};
