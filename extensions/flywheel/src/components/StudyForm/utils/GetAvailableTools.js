import store from '@ohif/viewer/src/store';
import { Utils } from '@flywheel/extension-flywheel-common';

const { isCurrentFile } = Utils;

const getAvailableTools = (
  availableTools,
  projectConfig,
  toolsWithLabel,
  measurementLabels,
  slice,
  isSubForm = null
) => {
  let availableToolsList = [];
  const state = store.getState();
  const isFile = isCurrentFile(state);
  if (
    state.subForm?.isSubFormPopupEnabled ||
    (projectConfig.studyFormWorkflow === 'Mixed' && isSubForm)
  ) {
    availableToolsList =
      availableTools?.size > 0 ? Array.from(availableTools) : availableTools;
  } else {
    const labels = projectConfig.labels;
    const allAvailableTools =
      availableTools?.size > 0 ? Array.from(availableTools) : availableTools;
    allAvailableTools?.forEach(tool => {
      if (toolsWithLabel[tool]) {
        toolsWithLabel[tool].forEach(label => {
          let limit = labels.find(data => data.value === label)?.limit;
          limit = limit ? limit : 0;
          const item =
            !isFile && projectConfig.bSlices
              ? measurementLabels.slices?.[slice]?.component
              : measurementLabels?.component;
          let itemCount = item?.filter(value => value === label)?.length;
          itemCount = itemCount ? itemCount : 0;
          if (itemCount < limit && !availableToolsList.includes(tool)) {
            availableToolsList.push(tool);
          }
        });
      } else {
        !availableToolsList.includes(tool) ? availableToolsList.push(tool) : '';
      }
    });
  }
  return availableToolsList;
};

export { getAvailableTools };
