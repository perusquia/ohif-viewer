import { Utils } from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';
import { getAvailableTools } from './GetAvailableTools.js';
const {
  isCurrentFile,
  hasMultipleReaderTask,
  getMeasurementsBasedOnTask,
  isEditModeForMultipleTask,
} = Utils;

const getStudyFormActiveToolsForEnable = (
  projectConfig,
  props,
  subFormName = null,
  toolsWithLabel = null
) => {
  const state = store.getState();
  let studyFormComponents = [];
  if (subFormName) {
    studyFormComponents =
      projectConfig?.studyForm?.subForms?.[subFormName]?.components;
  } else {
    studyFormComponents = projectConfig?.studyForm?.components;
  }
  let studyFormQuestionTools = [];
  const labels = props.projectConfig?.labels;
  const slice = props.sliceInfo?.sliceNumber;
  const allMeasurements = props.measurements;
  const hasSingleTaskEditMode = isEditModeForMultipleTask();
  studyFormComponents?.forEach(item => {
    if (item?.values?.length) {
      return item?.values?.forEach(data => {
        if (
          data?.requireMeasurements?.length &&
          data?.measurementTools?.length
        ) {
          let isLabelLimitReached = false;
          let labelName = '';
          data?.measurementTools?.forEach(tool => {
            if (!isLabelLimitReached && labelName === '') {
              data?.requireMeasurements?.forEach(label => {
                const selectedLabel = labels.find(data => data.value === label);
                let filteredMeasurements;
                let measurements = allMeasurements[tool];
                const selectedTaskId = state.viewerAvatar?.taskId;
                if (hasSingleTaskEditMode) {
                  measurements = getMeasurementsBasedOnTask(
                    measurements,
                    selectedTaskId
                  );
                }
                if (!isCurrentFile(state) && projectConfig?.bSlices) {
                  filteredMeasurements = measurements?.filter(
                    measurement =>
                      measurement.sliceNumber === slice &&
                      measurement.location === label &&
                      measurement.toolType === tool
                  );
                } else {
                  filteredMeasurements = measurements?.filter(
                    measurement =>
                      measurement.location === label &&
                      measurement.toolType === tool
                  );
                }
                if (
                  !filteredMeasurements?.length ||
                  filteredMeasurements?.length < selectedLabel.limit
                ) {
                  isLabelLimitReached = false;
                  labelName = '';
                  studyFormQuestionTools.push(tool);
                  if (toolsWithLabel && Object.keys(toolsWithLabel).length) {
                    if (!toolsWithLabel[tool]) {
                      toolsWithLabel[tool] = [];
                      data?.requireMeasurements.forEach(label => {
                        toolsWithLabel[tool].push(label);
                      });
                    } else {
                      data?.requireMeasurements.forEach(label => {
                        if (!toolsWithLabel[tool].includes(label)) {
                          toolsWithLabel[tool].push(label);
                        }
                      });
                    }
                  }
                } else {
                  isLabelLimitReached = true;
                  labelName = label;
                }
              });
            }
          });
        }
      });
    }
  });
  studyFormQuestionTools = [...new Set(studyFormQuestionTools)];
  return studyFormQuestionTools;
};

const getStudyFormToolsForEnable = (props, toolsWithLabels) => {
  let questionToolsList = getStudyFormActiveToolsForEnable(
    props.projectConfig,
    props,
    null,
    toolsWithLabels
  );
  questionToolsList = getAvailableTools(
    questionToolsList,
    props.projectConfig,
    toolsWithLabels,
    props.measurementLabels,
    props.sliceInfo?.sliceNumber
  );
  return questionToolsList;
};

export { getStudyFormActiveToolsForEnable, getStudyFormToolsForEnable };
