import OHIF from '@ohif/core';
import {
  getCurrentMeasurements,
  getVisibleQuestions,
} from '../validation/ValidationEngine.js';

const measurementApi = OHIF.measurements.MeasurementApi;
const { MeasurementHandlers } = OHIF.measurements;

const selectQuestionValues = (
  question,
  props,
  subFormName,
  isSubForm,
  state
) => {
  if (question.contourVisibility) {
    const { projectConfig } = props;
    const prevMeasurements = [];
    let shownQuestions = [];
    let activeQuestionIndex = 0;
    if (isSubForm) {
      shownQuestions =
        projectConfig.studyForm?.subForms?.[subFormName]?.components;
      activeQuestionIndex = (shownQuestions || []).findIndex(
        curQuestion => curQuestion.label === question.label
      );
    } else {
      shownQuestions = getVisibleQuestions(state, props);
      activeQuestionIndex = (shownQuestions || []).findIndex(
        curQuestion => curQuestion.label === question.label
      );
    }
    const doneQuestions = shownQuestions.slice(0, activeQuestionIndex);
    const currentMeasurements = getCurrentMeasurements(
      projectConfig.bSlices?.settings ? state.currentSliceInfo : null
    );
    (doneQuestions || []).reverse().forEach(doneQuestion => {
      if (prevMeasurements.length > 0) {
        return;
      }
      for (const option of doneQuestion.values || []) {
        (option.requireMeasurements || []).forEach(measureLabel => {
          currentMeasurements.forEach(currentMeasurement => {
            if (currentMeasurement.location === measureLabel) {
              prevMeasurements.push(currentMeasurement);
            }
          });
        });
      }
    });
    prevMeasurements.forEach(prevMeasurement => {
      MeasurementHandlers.onVisible(
        {
          detail: {
            toolType: prevMeasurement.toolType,
            measurementData: {
              _id: prevMeasurement._id,
              lesionNamingNumber: prevMeasurement.lesionNamingNumber,
              measurementNumber: prevMeasurement.measurementNumber,
            },
          },
        },
        false,
        false
      );
    });
    measurementApi.Instance.syncMeasurementsAndToolData();
    measurementApi.Instance.onMeasurementsUpdated();
  }
};

export { selectQuestionValues };
