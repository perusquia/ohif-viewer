const getToolsWithLabels = (visibleQuestions, isSubFormEnabled) => {
  const toolsWithLabel = {};
  if (!isSubFormEnabled && !visibleQuestions.components) {
    for (const question of visibleQuestions) {
      for (const option of question.values || []) {
        for (const tool of option.measurementTools || []) {
          if (!toolsWithLabel[tool]) {
            toolsWithLabel[tool] = [];
          }

          option?.requireMeasurements?.forEach(label => {
            if (!toolsWithLabel[tool].includes(label)) {
              toolsWithLabel[tool].push(label);
            }
          });
        }

        option.instructionSet?.forEach(instruction => {
          instruction.measurementTools?.forEach(x => {
            if (!toolsWithLabel[x]) {
              toolsWithLabel[x] = [];
            }

            instruction?.requireMeasurements.forEach(label => {
              if (!toolsWithLabel[x].includes(label)) {
                toolsWithLabel[x].push(label);
              }
            });
          });
        });
      }
    }
  }
  return toolsWithLabel;
};

export { getToolsWithLabels };
