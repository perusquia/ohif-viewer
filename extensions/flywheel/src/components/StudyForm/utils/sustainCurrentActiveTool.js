import store from '@ohif/viewer/src/store';
import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';
import hasMeasurementTools from './hasMeasurementTools';

const { setActiveTool } = FlywheelCommonRedux.actions;
const { selectActiveTool } = FlywheelCommonRedux.selectors;

const sustainCurrentActiveTool = toolList => {
  const state = store.getState();
  const activeTool = selectActiveTool(state);
  const hasTool = hasMeasurementTools();
  if (!hasTool) {
    return;
  }
  const tool = toolList.includes(activeTool)
    ? activeTool
    : toolList?.[0] || activeTool;
  store.dispatch(setActiveTool(tool));
};

export default sustainCurrentActiveTool;
