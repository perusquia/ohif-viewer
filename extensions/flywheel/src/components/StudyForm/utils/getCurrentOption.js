export const getCurrentOption = currentSelectedQuestion => {
  const currentQuestionAnswer =
    currentSelectedQuestion?.answer?.value || currentSelectedQuestion?.answer;
  const values = currentSelectedQuestion?.values;
  if (values?.length > 0) {
    return values.find(value => value.value === currentQuestionAnswer);
  }
};
