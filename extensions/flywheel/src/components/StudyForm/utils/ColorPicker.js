const clickColorPicker = (
  window,
  event,
  measurementData,
  DEFAULT_COLOR,
  props
) => {
  const screenHeight = window.innerHeight;

  // Align dialog 20px above or below from clicked point to not overlap the picker dialog over the palette and other buttons.
  const offset = 20;

  const pickerSize = {
    height: 297, // Color selector dialog height
    width: 232, // Color selector dialog width
  };

  const colorPickCoordinates = {
    left: event.clientX - pickerSize.width / 2,
    top:
      screenHeight / 2 > event.clientY
        ? event.clientY + offset
        : event.clientY - offset - pickerSize.height,
  };

  let selectedColor = DEFAULT_COLOR;
  let currentData = {};

  if (Array.isArray(measurementData)) {
    currentData = measurementData[0];
  } else {
    currentData = measurementData;
  }

  const measure = props.measurements[currentData.toolType].find(
    x => x._id === currentData.measurementId
  );

  if (measure) {
    selectedColor = measure.color;
  }

  return {
    selectedColor: selectedColor,
    colorPickCoordinates: colorPickCoordinates,
  };
};

export { clickColorPicker };
