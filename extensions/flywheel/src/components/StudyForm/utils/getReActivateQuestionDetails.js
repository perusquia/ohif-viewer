import store from '@ohif/viewer/src/store';
import {
  Redux as FlywheelCommonRedux,
  Utils as FlywheelCommonUtils,
} from '@flywheel/extension-flywheel-common';

const { setActiveQuestion } = FlywheelCommonRedux.actions;
const {
  getQuestionAndAnswerForMeasurement,
  getToolsAndToolTypeForSelectedOption,
} = FlywheelCommonUtils;

export const getReActivateQuestionDetails = (
  deletedMeasurement,
  visibleQuestions,
  isFile,
  projectConfig,
  notes,
  isStudyFormContainsMeasurementTools
) => {
  let deletedQuestion;
  if (
    deletedMeasurement.isSubForm &&
    deletedMeasurement.location &&
    deletedMeasurement.question &&
    deletedMeasurement.questionKey &&
    isStudyFormContainsMeasurementTools
  ) {
    const subFormComponents = visibleQuestions?.find(
      component => component.subFormName === deletedMeasurement.subFormName
    )?.components;
    deletedQuestion = subFormComponents?.find(
      question => question.key === deletedMeasurement.questionKey
    );
    const answerValue =
      !isFile && projectConfig.bSlices
        ? notes.slices[deletedMeasurement?.sliceNumber]?.[
            deletedMeasurement.question
          ]?.[deletedMeasurement.subFormName]?.find(
            item => item.annotationId === deletedMeasurement.subFormAnnotationId
          )?.[deletedMeasurement?.questionKey]
        : notes[deletedMeasurement.question]?.[
            deletedMeasurement.subFormName
          ]?.find(
            item => item.annotationId === deletedMeasurement.subFormAnnotationId
          )?.[deletedMeasurement?.questionKey];

    store.dispatch(
      setActiveQuestion({
        ...deletedQuestion,
        answer: answerValue,
        questionKey: deletedMeasurement?.questionKey,
        isSubForm: true,
        subFormName: deletedMeasurement.subFormName,
        subFormAnnotationId: deletedMeasurement.subFormAnnotationId,
        question: deletedMeasurement.question,
      })
    );
  } else if (
    !deletedMeasurement.isSubForm &&
    deletedMeasurement.location &&
    deletedMeasurement.questionKey &&
    isStudyFormContainsMeasurementTools
  ) {
    if (deletedMeasurement.questionKey) {
      deletedQuestion = visibleQuestions?.find(
        question => question.key === deletedMeasurement.questionKey
      );
    } else {
      deletedQuestion = visibleQuestions?.find(question => {
        return question?.values?.some(v => {
          if (v.instructionSet?.length) {
            return v.instructionSet?.some(i =>
              i.requireMeasurements?.includes(deletedMeasurement.location)
            );
          } else {
            return v.requireMeasurements?.includes(deletedMeasurement.location);
          }
        });
      });
    }

    const answerValue =
      !isFile && projectConfig.bSlices
        ? notes.slices[deletedMeasurement?.sliceNumber]?.[
            deletedMeasurement?.questionKey
          ]
        : notes[deletedMeasurement?.questionKey];

    store.dispatch(
      setActiveQuestion({
        ...deletedQuestion,
        answer: answerValue,
        questionKey: deletedMeasurement?.questionKey,
        isSubForm: false,
        subFormName: '',
        subFormAnnotationId: '',
        question: '',
      })
    );
  }

  if (deletedQuestion) {
    const { selectedOption } = getQuestionAndAnswerForMeasurement(
      deletedMeasurement
    );
    const { tools, toolType } = getToolsAndToolTypeForSelectedOption(
      deletedMeasurement,
      selectedOption
    );

    return {
      deletedQuestion: deletedQuestion,
      location: [deletedMeasurement.location],
      tools: tools,
      toolType: toolType,
    };
  } else {
    return;
  }
};
