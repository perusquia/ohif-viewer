import store from '@ohif/viewer/src/store';
import {
  Utils as FlywheelCommonUtils,
  Redux as FlywheelCommonRedux,
} from '@flywheel/extension-flywheel-common';
import Redux from '../../../redux/index.js';
import { getAvailableLabels } from '../utils/StudyFormLabel.js';
import sustainCurrentActiveTool from './sustainCurrentActiveTool.js';
import hasMeasurementTools from './hasMeasurementTools.js';

const { selectSessionRead, selectSessionFileRead } = Redux.selectors;

const {
  isBSliceApplicable,
  getActiveSliceInfo,
  isCurrentFile,
} = FlywheelCommonUtils;
const { setAvailableLabels } = FlywheelCommonRedux.actions;
const { selectActiveTool, measurementTools } = FlywheelCommonRedux.selectors;

const updateStudyFormAvailableLabels = () => {
  const state = store.getState();
  const projectConfig = state.flywheel.projectConfig;
  const bSlices = projectConfig?.bSlices;

  if (isBSliceApplicable(state)) {
    const activeViewportIndex = state.viewports.activeViewportIndex;
    const currentSlice = getActiveSliceInfo(activeViewportIndex);
    const sliceNo = currentSlice?.sliceNumber;
    if (bSlices) {
      const isBSlice = Object.keys(bSlices?.settings).includes(`${sliceNo}`);
      if (!isBSlice || !isStudyFormAnsweringBSlice(sliceNo)) {
        store.dispatch(setAvailableLabels(['']));
      } else {
        setAvailableToolsAndAvailableLabels(projectConfig, sliceNo);
      }
    } else {
      setAvailableToolsAndAvailableLabels(projectConfig, sliceNo);
    }
  }
};

const hasRoiOrMixedWorkFlow = () => {
  const state = store.getState();
  const projectConfig = state.flywheel.projectConfig;
  const { studyFormWorkflow = null } = projectConfig || {};
  const hasWorkflow =
    studyFormWorkflow === 'ROI' || studyFormWorkflow === 'Mixed';
  if (hasWorkflow) {
    return true;
  }
  return false;
};

const setAvailableToolsAndAvailableLabels = (projectConfig, sliceNo) => {
  const state = store.getState();
  const bSlices = projectConfig?.bSlices || null;
  const infusions = state.infusions;
  const activeTool = infusions.activeTool;
  const currentSelectedQuestion = infusions.currentSelectedQuestion;
  const answer = currentSelectedQuestion?.answer;
  const currentQuestionAnswer = answer?.value || answer;
  const values = currentSelectedQuestion?.values;
  let currentTools = [];
  const requiredLabels = new Set();
  let availableLabels = [];
  let availableTools = [];

  const hasTool = hasMeasurementTools();
  const hasWorkflow = hasRoiOrMixedWorkFlow();
  if (!hasTool && hasWorkflow && !values?.length) {
    store.dispatch(setAvailableLabels([''], measurementTools));
    return;
  }

  if (values?.length > 0) {
    const currentAnswer = values.find(v => v.value === currentQuestionAnswer);
    if (currentAnswer?.instructionSet?.length) {
      currentAnswer?.instructionSet.forEach(instruction => {
        currentTools.push(...(instruction?.measurementTools || []));
      });
      currentAnswer?.instructionSet?.forEach(instruction => {
        const labels = instruction.requireMeasurements || [];
        labels.forEach(requiredLabels.add, requiredLabels);
      });
    } else {
      currentTools.push(...(currentAnswer?.measurementTools || []));
      if (
        bSlices?.[sliceNo]?.[currentSelectedQuestion?.key]?.measurementTools
          ?.length
      ) {
        currentTools.push(
          ...bSlices?.[sliceNo]?.[currentSelectedQuestion?.key].measurementTools
        );
      }
      for (const measure of currentAnswer?.requireMeasurements || []) {
        requiredLabels.add(measure);
      }
    }
    availableLabels = getAvailableLabels(
      projectConfig,
      requiredLabels,
      sliceNo,
      currentSelectedQuestion.subFormAnnotationId
    );

    availableTools = [...new Set([...currentTools])];
    if (!availableLabels.length) {
      availableLabels.push('');
    }
    const labelsCollection = hasTool ? availableLabels : [''];
    const toolsCollection = hasTool ? availableTools : measurementTools;
    store.dispatch(setAvailableLabels(labelsCollection, toolsCollection));
    if (
      selectActiveTool(store.getState()) !== 'Eraser' &&
      measurementTools.includes(activeTool) &&
      activeTool !== 'DragProbe'
    ) {
      sustainCurrentActiveTool(availableTools);
    }
  }
};

const getSessionRead = () => {
  const state = store.getState();
  let sessionRead;

  if (isCurrentFile(state)) {
    if (state.flywheel.readerTaskWithFormResponse) {
      sessionRead = state.flywheel.readerTaskWithFormResponse.info;
    } else {
      sessionRead = selectSessionFileRead(state);
    }
  } else {
    if (state.flywheel.readerTaskWithFormResponse) {
      sessionRead = state.flywheel.readerTaskWithFormResponse.info;
    } else {
      sessionRead = selectSessionRead(state);
    }
  }
  return sessionRead;
};

const isStudyFormAnsweringBSlice = sliceNo => {
  const sessionRead = getSessionRead();
  const notes = sessionRead?.notes;
  const slice = notes?.slices?.[sliceNo];

  if (!slice || !Object.keys(slice)?.length) {
    return false;
  }

  const infusions = store.getState().infusions;
  const currentSelectedQuestion = infusions.currentSelectedQuestion;
  const key = currentSelectedQuestion?.key;
  const currentQuestionAnswer =
    currentSelectedQuestion?.answer?.value || currentSelectedQuestion?.answer;

  let currentSliceValue = currentSelectedQuestion?.isSubForm
    ? slice?.[currentSelectedQuestion?.question]?.value
    : slice?.[key]?.value || slice?.[key];

  if (currentSelectedQuestion?.isSubForm) {
    currentSliceValue = slice?.[currentSelectedQuestion?.question][
      currentSelectedQuestion?.subFormName
    ].find(
      item => item.annotationId === currentSelectedQuestion?.subFormAnnotationId
    )?.[currentSelectedQuestion?.questionKey];
  }

  return currentQuestionAnswer === currentSliceValue;
};

export { updateStudyFormAvailableLabels };
