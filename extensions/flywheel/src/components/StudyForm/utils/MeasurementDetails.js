const getDeletedMeasurementDetail = (
  timePointManager,
  lastDeletedMeasurementId
) => {
  const lastDeletedMeasurement = timePointManager.deletedMeasurementIdsInStudyForm.find(
    measurement => measurement.uuid === lastDeletedMeasurementId
  )?.measurement;
  return { ...lastDeletedMeasurement };
};

export { getDeletedMeasurementDetail };
