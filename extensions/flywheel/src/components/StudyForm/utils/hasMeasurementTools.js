import store from '@ohif/viewer/src/store';

const hasMeasurementTools = () => {
  const state = store.getState();
  const projectConfig = state.flywheel.projectConfig;
  const hasTools = projectConfig?.studyForm?.components?.some(item => {
    if (item?.values) {
      return item.values.some(data => data?.measurementTools?.length > 0);
    }
  });
  return hasTools;
};

export default hasMeasurementTools;
