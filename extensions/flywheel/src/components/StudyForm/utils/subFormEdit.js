import {
  Redux as FlywheelCommonRedux,
  Utils,
} from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';
import OHIF from '@ohif/core';

const { subFormRoiState } = FlywheelCommonRedux.actions;
const measurementApi = OHIF.measurements.MeasurementApi;
const { isCurrentFile } = Utils;

const subFormEdit = (state, projectConfig, props, uuid) => {
  store.dispatch(subFormRoiState({ status: false, measurementData: {} }));
  const questionKey = state.subForm?.measurement?.questionKey;
  const toolType = state.subForm?.measurement?.toolType;
  const location = state.subForm?.measurement?.location;
  const sliceNumberEdit = state.subForm?.measurement?.sliceNumber;
  const lastAnswer =
    typeof state.subForm?.measurement?.answer === 'object'
      ? state.subForm?.measurement?.answer?.value
      : state.subForm?.measurement?.answer;
  const roiSelectedQuestion = projectConfig.studyForm?.components?.find(
    item => item.key === questionKey
  );
  const lastAnsweredValue = roiSelectedQuestion?.values.find(
    value => value.value === lastAnswer
  );
  const measurementToolList = [...props.measurements[toolType]];
  measurementToolList.forEach(item => {
    if (typeof item.answer === 'object') {
      item.answer = item.answer.value;
    }
  });
  const measurementEdit =
    !isCurrentFile(state) && projectConfig.bSlices
      ? measurementToolList.find(
          item =>
            item.toolType === toolType &&
            item.location === location &&
            item.questionKey === questionKey &&
            item.sliceNumber === sliceNumberEdit &&
            item.answer === lastAnswer &&
            item.uuid === uuid
        )
      : measurementToolList.find(
          item =>
            item.toolType === toolType &&
            item.location === location &&
            item.questionKey === questionKey &&
            item.answer === lastAnswer &&
            item.uuid === uuid
        );
  return {
    roiSelectedQuestion: roiSelectedQuestion,
    lastAnsweredValue: lastAnsweredValue,
    measurementEdit: measurementEdit,
  };
};

const updateAnnotationKeyAnswerAndNotes = (
  measurementList,
  roiQuestion,
  answer,
  roiAnswer,
  projectConfig,
  studyFormNotes,
  sliceInfo
) => {
  const state = store.getState();
  measurementList.forEach(measurement => {
    measurement.questionKey = roiQuestion.key;
    measurement.answer = answer;
    measurementApi.Instance.updateMeasurement(
      measurement.toolType,
      measurement
    );
  });
  measurementApi.Instance.syncMeasurementsAndToolData();
  const data = roiAnswer.find(value => value.value === answer);
  if (!isCurrentFile(state) && projectConfig.bSlices) {
    if (!studyFormNotes.slices) {
      studyFormNotes.slices = {};
    }
    if (!studyFormNotes.slices[sliceInfo?.sliceNumber]) {
      studyFormNotes.slices[sliceInfo?.sliceNumber] = {};
    }
    if (!studyFormNotes.slices[sliceInfo?.sliceNumber]?.[roiQuestion.key]) {
      studyFormNotes.slices[sliceInfo?.sliceNumber][roiQuestion.key] =
        roiQuestion.type === 'selectboxes'
          ? [data.value]
          : data.subForm
          ? {
              value: data.value,
              [data.subForm]: [],
            }
          : data.value;
    } else {
      if (roiQuestion.type === 'selectboxes') {
        if (
          !Array.isArray(
            studyFormNotes.slices[sliceInfo?.sliceNumber]?.[roiQuestion.key]
          )
        ) {
          studyFormNotes.slices[sliceInfo?.sliceNumber][roiQuestion.key] = [
            data.value,
          ];
        } else if (
          Array.isArray(
            studyFormNotes.slices[sliceInfo?.sliceNumber]?.[roiQuestion.key]
          ) &&
          !studyFormNotes.slices[sliceInfo?.sliceNumber]?.[
            roiQuestion.key
          ].includes(data.value)
        ) {
          studyFormNotes.slices[sliceInfo?.sliceNumber][roiQuestion.key].push(
            data.value
          );
        } else if (
          Array.isArray(
            studyFormNotes.slices[sliceInfo?.sliceNumber]?.[roiQuestion.key]
          ) &&
          studyFormNotes.slices[sliceInfo?.sliceNumber]?.[
            roiQuestion.key
          ].includes(data.value)
        ) {
          const index = studyFormNotes.slices[sliceInfo?.sliceNumber][
            roiQuestion.key
          ].indexOf(data.value);
          if (index > -1) {
            studyFormNotes.slices[sliceInfo?.sliceNumber][
              roiQuestion.key
            ].splice(index, 1);
          }
        }
      } else if (data.subForm) {
        if (
          typeof studyFormNotes.slices[sliceInfo?.sliceNumber][
            roiQuestion.key
          ] !== 'object'
        ) {
          studyFormNotes.slices[sliceInfo?.sliceNumber][roiQuestion.key] = {
            value: data.value,
            [data.subForm]: [],
          };
        } else {
          studyFormNotes.slices[sliceInfo?.sliceNumber][roiQuestion.key].value =
            data.value;
        }
      } else {
        studyFormNotes.slices[sliceInfo?.sliceNumber][roiQuestion.key] =
          data.value;
      }
    }
  } else {
    if (!studyFormNotes[roiQuestion.key]) {
      studyFormNotes[roiQuestion.key] =
        roiQuestion.type === 'selectboxes'
          ? [data.value]
          : data.subForm
          ? {
              value: data.value,
              [data.subForm]: [],
            }
          : data.value;
    } else {
      if (roiQuestion.type === 'selectboxes') {
        if (!Array.isArray(studyFormNotes[roiQuestion.key])) {
          studyFormNotes[roiQuestion.key] = [data.value];
        } else if (
          Array.isArray(studyFormNotes[roiQuestion.key]) &&
          !studyFormNotes[roiQuestion.key].includes(data.value)
        ) {
          studyFormNotes[roiQuestion.key].push(data.value);
        } else if (
          Array.isArray(studyFormNotes[roiQuestion.key]) &&
          studyFormNotes[roiQuestion.key].includes(data.value)
        ) {
          const index = studyFormNotes[roiQuestion.key].indexOf(data.value);
          if (index > -1) {
            studyFormNotes[roiQuestion.key].splice(index, 1);
          }
        }
      } else if (data.subForm) {
        if (typeof studyFormNotes[roiQuestion.key] !== 'object') {
          studyFormNotes[roiQuestion.key] = {
            value: data.value,
            [data.subForm]: [],
          };
        } else {
          studyFormNotes[roiQuestion.key].value = data.value;
        }
      } else {
        studyFormNotes[roiQuestion.key] = data.value;
      }
    }
  }
  return {
    data: data,
    studyFormNotes: studyFormNotes,
  };
};

const updateAnnotationKeyAnswer = (measurements, roiQuestion, answer) => {
  measurements.forEach(measurement => {
    measurement.questionKey = roiQuestion.key;
    measurement.answer = answer;
    measurementApi.Instance.updateMeasurement(
      measurement.toolType,
      measurement
    );
  });
  measurementApi.Instance.syncMeasurementsAndToolData();
};

const updateNoteInMixedOrROI = (
  projectConfig,
  studyFormNotes,
  sliceInfo,
  roiQuestion,
  roiAnswer
) => {
  const state = store.getState();
  if (!isCurrentFile(state) && projectConfig.bSlices) {
    if (!studyFormNotes.slices) {
      studyFormNotes.slices = {};
    }
    if (!studyFormNotes.slices[sliceInfo?.sliceNumber]) {
      studyFormNotes.slices[sliceInfo?.sliceNumber] = {};
    }
    if (!studyFormNotes.slices[sliceInfo?.sliceNumber]?.[roiQuestion.key]) {
      studyFormNotes.slices[sliceInfo?.sliceNumber][
        roiQuestion.key
      ] = roiAnswer[0].subForm
        ? {
            value: roiAnswer[0].value,
            [roiAnswer[0].subForm]: [],
          }
        : roiAnswer[0].value;
    } else {
      if (roiAnswer[0].subForm) {
        if (
          typeof studyFormNotes.slices[sliceInfo?.sliceNumber][
            roiQuestion.key
          ] !== 'object'
        ) {
          studyFormNotes.slices[sliceInfo?.sliceNumber][roiQuestion.key] = {
            value: roiAnswer[0].value,
            [roiAnswer[0].subForm]: [],
          };
        } else {
          studyFormNotes.slices[sliceInfo?.sliceNumber][roiQuestion.key].value =
            roiAnswer[0].value;
        }
      } else {
        studyFormNotes.slices[sliceInfo?.sliceNumber][roiQuestion.key] =
          roiAnswer[0].value;
      }
    }
  } else {
    if (!studyFormNotes[roiQuestion.key]) {
      studyFormNotes[roiQuestion.key] = roiAnswer[0].subForm
        ? {
            value: roiAnswer[0].value,
            [roiAnswer[0].subForm]: [],
          }
        : roiAnswer[0].value;
    } else {
      if (roiAnswer[0].subForm) {
        if (typeof studyFormNotes[roiQuestion.key] !== 'object') {
          studyFormNotes[roiQuestion.key] = {
            value: roiAnswer[0].value,
            [roiAnswer[0].subForm]: [],
          };
        } else {
          studyFormNotes[roiQuestion.key].value = roiAnswer[0].value;
        }
      } else {
        studyFormNotes[roiQuestion.key] = roiAnswer[0].value;
      }
    }
  }
  return studyFormNotes;
};

const getQuestionAnswerAndTabDetails = (
  projectConfig,
  studyFormState,
  slice,
  question
) => {
  const state = store.getState();
  const isFile = isCurrentFile(state);
  const subFormTabs =
    !isFile && projectConfig.bSlices
      ? studyFormState.subFormTabList[slice]
      : studyFormState.subFormTabList;
  const questionAnswer =
    !isFile && projectConfig.bSlices
      ? typeof studyFormState.notes?.slices?.[slice]?.[question.key] ===
        'object'
        ? studyFormState.notes?.slices?.[slice]?.[question.key]?.value
        : studyFormState.notes?.slices?.[slice]?.[question.key]
      : typeof studyFormState.notes?.[question.key] === 'object'
      ? studyFormState.notes?.[question.key]?.value
      : studyFormState.notes?.[question.key];
  return {
    subFormTabs: subFormTabs,
    questionAnswer: questionAnswer,
  };
};

const getValueFromNoteAndSubFormName = (
  state,
  slice,
  question,
  projectConfig,
  annotationId
) => {
  let notesForSubForm = {};
  let value = '';
  let subFormName = '';
  if (state.notes.slices) {
    value = state.notes.slices?.[slice]?.[question.key]?.value;
    subFormName = projectConfig.studyForm?.components
      ?.find(qs => qs.key === question.key)
      .values?.find(val => val.value === value)?.subForm;
    notesForSubForm = state.notes.slices?.[slice]?.[question.key]?.[
      subFormName
    ]?.find(SubFormTabNotes => SubFormTabNotes.annotationId === annotationId);
  } else {
    value = state.notes[question.key]?.value;
    subFormName = projectConfig.studyForm?.components
      ?.find(qs => qs.key === question.key)
      .values?.find(val => val.value === value)?.subForm;
    notesForSubForm = state.notes[question.key]?.[subFormName]?.find(
      SubFormTabNotes => SubFormTabNotes.annotationId === annotationId
    );
  }
  return {
    notesForSubForm: notesForSubForm,
    value: value,
    subFormName: subFormName,
  };
};

export {
  subFormEdit,
  updateAnnotationKeyAnswerAndNotes,
  updateAnnotationKeyAnswer,
  updateNoteInMixedOrROI,
  getQuestionAnswerAndTabDetails,
  getValueFromNoteAndSubFormName,
};
