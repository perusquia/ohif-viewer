import store from '@ohif/viewer/src/store';
import {
  HTTP as FlywheelCommonHTTP,
  Utils as FlywheelCommonUtils,
  Redux as FlywheelCommonRedux,
} from '@flywheel/extension-flywheel-common';
import Redux from '../../../redux';

const { selectUser, selectActiveSessions } = Redux.selectors;
const { setReaderTaskWithFormResponse, setActiveSessions } = Redux.actions;
const {
  shouldIgnoreUserSettings,
  isCurrentFile,
  getRouteParams,
  isEditModeForMultipleTask,
} = FlywheelCommonUtils;
const { setMultipleTaskNotes } = FlywheelCommonRedux.actions;
const { customInfoKey } = FlywheelCommonHTTP.utils.REQUEST_UTILS;
const readStatus = {
  Todo: 'Todo',
  Inprogress: 'In_progress',
  Complete: 'Complete',
};

function updateSessionNotes(notes) {
  const state = store.getState();
  const isFile = isCurrentFile(state);
  const readerTaskResponse = _.cloneDeep(
    state.flywheel.readerTaskWithFormResponse
  );
  const featureConfig = state.flywheel.featureConfig;
  const { taskId } = getRouteParams();

  if (featureConfig?.features?.reader_tasks && taskId) {
    if (readerTaskResponse) {
      updateReaderTaskResponse(readerTaskResponse, notes);
    }
  } else if (!isFile) {
    updateSession(notes);
  }
}

function getReadTime() {
  const state = store.getState();
  const adjustedReadStartTime = state.timer.adjustedReadStartTime;
  return (Date.now() - adjustedReadStartTime) / 1000;
}

function updateReaderTaskResponse(readerTaskResponse, notes) {
  const state = store.getState();
  const readTime = getReadTime();
  const shouldIgnore = shouldIgnoreUserSettings(state);
  readerTaskResponse.info = {
    orientations: readerTaskResponse?.info?.orientations || [],
    date: new Date().toISOString(),
    readTime: readTime,
    readStatus: readStatus.Inprogress,
    displayProperties: shouldIgnore
      ? readerTaskResponse.info.displayProperties
      : null,
  };
  readerTaskResponse.info.notes = notes;
  store.dispatch(setReaderTaskWithFormResponse(readerTaskResponse));
  if (isEditModeForMultipleTask()) {
    const { multipleTaskNotes, taskId } = state.viewerAvatar;
    if (multipleTaskNotes?.[taskId]?.notes?.[0]) {
      multipleTaskNotes[taskId].notes[0] = notes;
      store.dispatch(setMultipleTaskNotes(multipleTaskNotes));
    }
  }
}

function updateSession(notes) {
  const state = store.getState();
  const sessions = selectActiveSessions(state);
  let currentDataInfo;
  let isUpdateRequired = false;

  if (sessions) {
    for (let key in notes) {
      if (notes[key] === undefined) {
        delete notes[key];
      }
    }
    const updatedSessions = [];
    sessions.forEach((session, index) => {
      // Pushing all sessions to avoid missing session while updating to store
      updatedSessions.push({ ...session });
      currentDataInfo = session.info;
      if (!currentDataInfo) {
        return;
      }
      const requestData = {
        ...currentDataInfo.ohifViewer,
      };
      const user = selectUser(state);
      if (!user) {
        return;
      }
      if (requestData.read) {
        const userInfo = requestData.read[customInfoKey(user._id)];
        const readTime = getReadTime();
        requestData.read = {
          ...requestData.read,
          [customInfoKey(user._id)]: {
            date: new Date().toISOString(),
            readTime: userInfo ? userInfo.readTime : readTime,
            notes: notes,
            readStatus: userInfo ? userInfo.Inprogress : readStatus.Inprogress,
          },
        };
        updatedSessions[index] = {
          ...session,
          info: { ...currentDataInfo, ohifViewer: requestData },
        };
        // Update if modification in at least one session.
        isUpdateRequired = true;
      }
    });
    if (isUpdateRequired) {
      store.dispatch(setActiveSessions(updatedSessions));
    }
  }
}

export default updateSessionNotes;
