import {
  Utils,
  Redux as FlywheelCommonRedux,
} from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';
const { validateAllBSlice } = Utils;
const { studyFormBSliceValidatedInfo } = FlywheelCommonRedux.actions;

const getAndSetBSliceValidatedInfo = (props, stateNotes) => {
  const { projectConfig } = props;
  const allBSliceValidatedData = validateAllBSlice(projectConfig, stateNotes);
  if (Object.keys(allBSliceValidatedData).length) {
    store.dispatch(studyFormBSliceValidatedInfo(allBSliceValidatedData));
  }
};

export { getAndSetBSliceValidatedInfo };
