import LabelLimitReachDialog from '../ui/InstructionSet/dialog/LabelLimitReachDialog.js';
import store from '@ohif/viewer/src/store';
import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';

const { setLimitReachedDialog } = FlywheelCommonRedux.actions;

const getDailogIdAndDisplayLabelLimitReachedDailog = (
  e,
  servicesManager,
  dispatchActions,
  newMeasures
) => {
  const { UIDialogService } = servicesManager.services;
  const popupFromLeft = 175;
  const popupFromTop = 220;
  const labelLimitReachDialogId = UIDialogService.create({
    content: LabelLimitReachDialog,
    defaultPosition: {
      x: window.innerWidth / 2 - popupFromLeft,
      y: window.innerHeight / 2 - popupFromTop,
    },
    showOverlay: true,
    contentProps: {
      label: 'Label limit reached',
      subTitle:
        newMeasures.length !== 1
          ? 'There are multiple annotations for this question, Please manually delete any to redraw.'
          : 'Annotation label limit has reached for this question. Would you like to delete the annotation and redraw it?',
      measurements: newMeasures,
      isDisabledDeleteButton: newMeasures.length !== 1,
      onCancel: () => {
        UIDialogService.dismiss({
          id: labelLimitReachDialogId,
        });
        store.dispatch(setLimitReachedDialog(null));
      },
      onDelete: () => {
        UIDialogService.dismiss({
          id: labelLimitReachDialogId,
        });
        dispatchActions.onDeleteGroupMeasurementClick(e, newMeasures, {
          servicesManager,
        });
        store.dispatch(setLimitReachedDialog(null));
      },
    },
  });
  return labelLimitReachDialogId;
};

export { getDailogIdAndDisplayLabelLimitReachedDailog };
