import { Component } from 'react';
import React from 'react';
import PropTypes from 'prop-types';
import { SimpleDialog } from '@ohif/ui';
import store from '@ohif/viewer/src/store';

import bounding from '@ohif/viewer/src/lib/utils/bounding.js';
import PermissionDialog from '../PermissionDialog/PermissionDialog';

import './DeleteDialog.styl';

export default class DeleteDialog extends Component {
  static defaultProps = {
    componentRef: React.createRef(),
    componentStyle: {},
  };

  static propTypes = {
    measurements: PropTypes.array.isRequired,
    servicesManager: PropTypes.object,
    onClose: PropTypes.func.isRequired,
    onContinue: PropTypes.func,
    componentRef: PropTypes.object,
    componentStyle: PropTypes.object,
    toolActivationContext: PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.state = {};
    this.mainElement = React.createRef();
  }

  componentDidMount = () => {
    bounding(this.mainElement);
  };

  render() {
    return (
      <SimpleDialog
        headerTitle="Delete Measurements"
        onClose={() => {}}
        onConfirm={() => {}}
        componentRef={this.mainElement}
        showFooterButtons={false}
        hideCloseButton={true}
      >
        <div className="label-style">{this.props.label}</div>
        <div className="button-container">
          <div className="button-style" onClick={() => this.onDelete()}>
            Yes
          </div>

          <div onClick={this.props.onClose} className="button-style">
            No
          </div>
        </div>
      </SimpleDialog>
    );
  }

  onDelete = () => {
    const measurements = this.props.measurements;

    let subFormLabel = '';
    const studyForm = store.getState().flywheel.projectConfig?.studyForm;
    if (studyForm?.components?.length) {
      measurements.forEach(measurement => {
        if (!measurement.isSubForm) {
          const question = studyForm.components.find(
            component => component.key === measurement.questionKey
          );
          if (question?.values?.length) {
            const answer =
              typeof measurement.answer === 'object'
                ? measurement.answer?.value
                : measurement.answer;
            const subForm = question.values.find(
              value => value.value === answer
            )?.subForm;
            if (subForm) {
              subFormLabel =
                'Do you want to delete this annotation? This annotation contains a sub form response.';
            }
          }
        }
      });
    }
    if (subFormLabel !== '') {
      const { UIDialogService } = this.props.servicesManager.services;
      let helpDialogId = null;
      if (helpDialogId) {
        UIDialogService.dismiss({ id: helpDialogId });
        helpDialogId = null;
        return;
      }
      const popupFromLeft = 175;
      const popupFromTop = 220;

      const deleteDialogEvent = new CustomEvent('deleteDialog');
      document.dispatchEvent(deleteDialogEvent);

      helpDialogId = UIDialogService.create({
        content: PermissionDialog,
        defaultPosition: {
          x: window.innerWidth / 2 - popupFromLeft,
          y: window.innerHeight / 2 - popupFromTop,
        },
        showOverlay: true,
        contentProps: {
          measurements: measurements,
          label: subFormLabel,
          onClose: () => {
            UIDialogService.dismiss({ id: helpDialogId });
            helpDialogId = null;
          },
          onConfirm: () => {},
          onDelete: this.props.onDelete,
          toolActivationContext: this.props.contentProps.toolActivationContext,
        },
      });
    } else {
      this.props.onDelete(
        measurements,
        this.props.contentProps.toolActivationContext
      );
    }

    this.props.onClose();
  };
}
