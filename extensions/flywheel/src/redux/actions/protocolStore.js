import { SET_ACTIVE_PROTOCOL_STORE } from '../constants/ActionTypes';

export function setActiveProtocolStore(
  projectId,
  protocolStore,
  waitForProtocol
) {
  return {
    type: SET_ACTIVE_PROTOCOL_STORE,
    projectId,
    protocolStore,
    waitForProtocol,
  };
}
