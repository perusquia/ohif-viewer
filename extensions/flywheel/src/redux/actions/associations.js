import { CLEAR_ASSOCIATIONS, SET_ASSOCIATIONS } from '../constants/ActionTypes';

export function clearAssociations() {
  return {
    type: CLEAR_ASSOCIATIONS,
  };
}

export function setAssociations(associations) {
  return {
    type: SET_ASSOCIATIONS,
    associations,
  };
}
