import { redux } from '@ohif/core';

import { setCurrentNifti, setCurrentMetaImage } from './study';
import { setActiveSession } from './session';
import { HTTP as FlywheelCommonHTTP } from '@flywheel/extension-flywheel-common';

const { setFileInfo, setSessionInfo } = FlywheelCommonHTTP.services;

// Redux should be dry. This can cause potential side effects
// TODO to change it, potential side effect (action changing something else other store)
export function setSessionMeasurements(ohifMeasurements) {
  return (dispatch, getState) => {
    const {
      associations,
      currentNiftis,
      currentMetaImage,
      session,
      user,
    } = getState().flywheel;

    let StudyInstanceUIDs = [];
    if (currentNiftis.length > 0) {
      currentNiftis.forEach(currentNifti => {
        StudyInstanceUIDs.push(currentNifti.filename);
      });
    } else if (currentMetaImage) {
      StudyInstanceUIDs.push(currentMetaImage.filename);
    } else if (session) {
      const association = Object.values(associations).find(
        assoc => assoc.session_id === session._id
      );
      if (association && association.study_uid) {
        StudyInstanceUIDs.push(association.study_uid);
      }
    }
    if (!(user && StudyInstanceUIDs.length > 0)) {
      return;
    }

    const fwMeasurements = {};
    let modified = false;
    for (const [toolType, measurements] of Object.entries(ohifMeasurements)) {
      if (measurements.length > 0) {
        fwMeasurements[toolType] = measurements.filter(measure =>
          StudyInstanceUIDs.includes(measure.StudyInstanceUID)
        );
        for (const measure of measurements) {
          // compatibility with older measurements
          measure.patientId = measure.PatientID;
          measure.studyInstanceUid = measure.StudyInstanceUID;
          measure.seriesInstanceUid = measure.SeriesInstanceUID;
          measure.sopInstanceUid = measure.SOPInstanceUID;
          // add user origin
          if (!measure.flywheelOrigin) {
            modified = true;
            measure.flywheelOrigin = {
              type: 'user',
              id: user._id,
            };
          }
        }
      }
    }
    if (modified) {
      // send back through to persist user info
      dispatch(redux.actions.setMeasurements({ ...ohifMeasurements }));
    } else {
      // save to flywheel
      if (currentNiftis.length > 0) {
        currentNiftis.forEach(currentNifti => {
          const currentFile = currentNifti;
          const ohifInfo = {
            ...currentFile.info.ohifViewer,
            measurements: fwMeasurements,
          };
          setFileInfo(
            currentFile.containerId,
            currentFile.filename,
            ohifInfo
          ).then(() => {
            dispatch(
              setCurrentNifti(
                currentNifti.containerId,
                currentNifti.filename,
                currentNifti.file_id,
                {
                  ...currentNifti.info,
                  ohifViewer: ohifInfo,
                },
                currentNifti.parents
              )
            );
          });
        });
      } else if (currentMetaImage) {
        const currentFile = currentMetaImage;
        const ohifInfo = {
          ...currentFile.info.ohifViewer,
          measurements: fwMeasurements,
        };
        dispatch(
          setCurrentMetaImage(
            currentMetaImage.containerId,
            currentMetaImage.filename,
            currentMetaImage.file_id,
            {
              ...currentMetaImage.info,
              ohifViewer: ohifInfo,
            },
            currentMetaImage.parents
          )
        );
      } else {
        const ohifInfo = {
          ...session.info.ohifViewer,
          measurements: fwMeasurements,
        };
        setSessionInfo(session._id, ohifInfo).then(() => {
          dispatch(
            setActiveSession({
              ...session,
              info: { ...session.info, ohifViewer: ohifInfo },
            })
          );
        });
      }
    }
  };
}
