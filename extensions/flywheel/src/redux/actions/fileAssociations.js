import {
  CLEAR_FILE_ASSOCIATIONS,
  SET_FILE_ASSOCIATIONS,
} from '../constants/ActionTypes';

export function clearFileAssociations() {
  return {
    type: CLEAR_FILE_ASSOCIATIONS,
  };
}

export function setFileAssociations(fileAssociations) {
  return {
    type: SET_FILE_ASSOCIATIONS,
    fileAssociations,
  };
}
