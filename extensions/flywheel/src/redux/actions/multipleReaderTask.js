import {
  SET_MULTIPLE_READER_TASK,
  SET_MULTIPLE_READER_TASK_RESPONSE,
} from '../constants/ActionTypes';

export function setMultipleReaderTask(data) {
  return {
    type: SET_MULTIPLE_READER_TASK,
    data,
  };
}

export function setMultipleReaderTaskResponse(data) {
  return {
    type: SET_MULTIPLE_READER_TASK_RESPONSE,
    data,
  };
}
