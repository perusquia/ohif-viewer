export function selectFileAssociations(state) {
  return state.flywheel.fileAssociations;
}

export function selectAllFileAssociations(state) {
  return state.flywheel.allFileAssociations;
}
