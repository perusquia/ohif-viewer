import { createSelector } from 'reselect';

export function selectMeasurements(state) {
  return state.timepointManager.measurements;
}

export const selectMeasurementsByStudyUid = createSelector(
  selectMeasurements,
  allMeasurements => {
    const result = {};
    for (const [toolType, measureList] of Object.entries(allMeasurements)) {
      for (const measure of measureList) {
        result[measure.StudyInstanceUID] =
          result[measure.StudyInstanceUID] || {};
        result[measure.StudyInstanceUID][toolType] =
          result[measure.StudyInstanceUID][toolType] || [];
        result[measure.StudyInstanceUID][toolType].push(measure);
      }
    }
    return result;
  }
);
