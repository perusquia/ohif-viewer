import {
  SET_STUDY_SORT,
  SET_STUDY_FILTERS,
  SET_STUDY_ORDERING,
} from '../constants/ActionTypes';

const initialState = {
  sort: {
    fieldName: 'isRead',
    direction: 'asc',
  },
  // should match propTypes in platform/ui/src/components/studyList/StudyList.js
  filters: {
    PatientName: '',
    PatientID: '',
    AccessionNumber: '',
    StudyDate: '',
    modalities: '',
    StudyDescription: '',
    patientNameOrId: '',
    accessionOrModalityOrDescription: '',
    allFields: '',
    studyDateTo: null,
    studyDateFrom: null,
    subjectCode: '',
    sessionLabel: '',
    status: '',
  },
  ordering: [],
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case SET_STUDY_SORT:
      return { ...state, sort: action.sort };
    case SET_STUDY_FILTERS:
      return { ...state, filters: action.filters };
    case SET_STUDY_ORDERING:
      return { ...state, ordering: action.ordering };
    default:
      return state;
  }
}
