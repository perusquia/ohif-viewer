import {
  SET_MULTIPLE_READER_TASK,
  SET_MULTIPLE_READER_TASK_RESPONSE,
} from '../constants/ActionTypes';

const initialState = {
  TaskIds: [],
  multipleTaskResponse: [],
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case SET_MULTIPLE_READER_TASK:
      return { ...state, TaskIds: action.data.taskIds };
    case SET_MULTIPLE_READER_TASK_RESPONSE:
      let multipleTaskResponse = [...state.multipleTaskResponse];
      multipleTaskResponse.push(action.data);
      return { ...state, multipleTaskResponse: [...multipleTaskResponse] };
    default:
      return state;
  }
}
