const TOOL_NAMES = {
  INDICATOR_DISPLAY_TOOL: 'IndicatorDisplayTool',
  SCALE_INDICATOR_TOOL: 'ScaleIndicatorTool',
  RING_OVERLAY_TOOL: 'RingOverlayTool'
};

export default TOOL_NAMES;
