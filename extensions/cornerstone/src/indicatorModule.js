import cornerstone from 'cornerstone-core';

// We should put this as a helper somewhere as we are using it in mutliple places.
function refreshViewport() {
  cornerstone.getEnabledElements().forEach(enabledElement => {
    if (enabledElement.image) {
      cornerstone.updateImage(enabledElement.element);
    }
  });
}

const configuration = {
  lineWidth: 3,
  opacity: 0.75,
  highlightOpacity: 0.5,
};

const state = {
  IndicatorSets: [],
};

/**
 * Adds a IndicatorSet set to the module.
 * @param {Object} indicatorSet The indicator set data.
 */
function addIndicatorSet(indicatorSet) {
  state.IndicatorSets.push(indicatorSet);
}

/**
 * Set the IndicatorSet collection.
 * @param {Object} indicatorSets The IndicatorSet collection.
 */
function setIndicatorSets(indicatorSets) {
  state.IndicatorSets = indicatorSets;
}

/**
 * Clear IndicatorSet with the given indicatorSetLabel.
 * @param {string} label The indicatorSetLabel of the IndicatorSet.
 */
function clearIndicatorSetForLabel(label) {
  state.IndicatorSets = state.IndicatorSets.filter(
    indicatorSet => indicatorSet.indicatorSetLabel !== label
  );
}

/**
 * Returns the IndicatorSet with the given SeriesInstanceUID and indicatorSetLabel.
 * @param {string} label The indicatorSetLabel of the IndicatorSet.
 * @param {string} SeriesInstanceUID The SeriesInstanceUID of the IndicatorSet.
 *
 * @returns {Object} The IndicatorSet.
 */
function getIndicatorSet(label, SeriesInstanceUID) {
  return state.IndicatorSets.find(
    indicatorSet =>
      indicatorSet.SeriesInstanceUID === SeriesInstanceUID &&
      indicatorSet.indicatorSetLabel === label
  );
}

/**
 * Returns the IndicatorSet with the given SeriesInstanceUID and indicatorSetLabel.
 * @param {string} label The indicatorSetLabel of the IndicatorSet.
 * @param {string} SeriesInstanceUID The SeriesInstanceUID of the IndicatorSet.
 * @param {string} imageId Image ID
 * @param {Number} indicatorNumber The indicatorNumber of the IndicatorSet.
 *
 * @returns {Object} The IndicatorSet.
 */
function getIndicator(label, SeriesInstanceUID, imageId, indicatorNumber) {
  const indicatorSet = getIndicatorSet(label, SeriesInstanceUID);
  const indicators = indicatorSet.indicators.filter(indicator =>
    indicator.shouldValidateImageId ? indicator.imageId === imageId : true
  );

  return indicators.find(
    indicator => indicator.IndicatorNumber === indicatorNumber
  );
}

/**
 * Returns the IndicatorSet collection
 *
 * @returns {Object} The IndicatorSet collection.
 */
function getIndicatorSets() {
  return state.IndicatorSets;
}

/**
 * Shows the IndicatorSet.
 * @param {string} label The indicatorSetLabel of the IndicatorSet.
 * @param {string} SeriesInstanceUID The SeriesInstanceUID of the IndicatorSet.
 */
function setShowHideIndicatorSet(label, SeriesInstanceUID, showHideStatus) {
  _setIndicatorSetVisible(label, SeriesInstanceUID, showHideStatus);
}

/**
 * Sets the visibility of the IndicatorSet.
 * @param {string} label The indicatorSetLabel of the IndicatorSet.
 * @param {string} SeriesInstanceUID The SeriesInstanceUID of the IndicatorSet.
 * @param {boolean} visible Whether the IndicatorSet should visible or not.
 */
function _setIndicatorSetVisible(label, SeriesInstanceUID, visible = true) {
  const indicatorSet = getIndicatorSet(label, SeriesInstanceUID);

  if (indicatorSet) {
    indicatorSet.visible = visible;

    refreshViewport();
  }
}

/**
 * Toggles the visibility of the IndicatorSet.
 * @param {string} label The indicatorSetLabel of the IndicatorSet.
 * @param {string} SeriesInstanceUID The SeriesInstanceUID of the IndicatorSet.
 */
function setToggleIndicatorSet(label, SeriesInstanceUID) {
  const indicatorSet = getIndicatorSet(label, SeriesInstanceUID);

  if (indicatorSet) {
    indicatorSet.visible = !indicatorSet.visible;

    refreshViewport();
  }
}

/**
 * Remove the IndicatorSet.
 * @param {string} label The indicatorSetLabel of the IndicatorSet.
 * @param {string} SeriesInstanceUID The SeriesInstanceUID of the IndicatorSet.
 */
function removeIndicatorSet(label, SeriesInstanceUID) {
  state.IndicatorSets = state.IndicatorSets.filter(
    indicatorSet =>
      indicatorSet.SeriesInstanceUID !== SeriesInstanceUID &&
      indicatorSet.indicatorSetLabel !== label
  );
  refreshViewport();
}

export default {
  getters: {
    indicatorSet: getIndicatorSet,
    indicatorSets: getIndicatorSets,
    indicator: getIndicator,
  },
  setters: {
    indicatorSet: addIndicatorSet,
    indicatorSets: setIndicatorSets,
    removeIndicatorSet,
    clearIndicatorSetForLabel,
    showHideIndicatorSet: setShowHideIndicatorSet,
    toggleIndicatorSet: setToggleIndicatorSet,
  },
  state,
  configuration,
};
