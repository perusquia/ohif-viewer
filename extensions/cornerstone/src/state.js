const state = {
  enabledElements: {},
};

/**
 * Sets the enabled element `dom` reference for an active viewport.
 * @param {HTMLElement} dom Active viewport element.
 * @return void
 */
const setEnabledElement = (viewportIndex, element) =>
  (state.enabledElements[viewportIndex] = element);

/**
 * Clear the enabled element `dom` reference while unmounting the viewport.
 * @param {viewportIndex} viewportIndex Viewport index.
 * @return void
 */
const clearEnabledElement = (viewportIndex) =>
  (delete state.enabledElements[viewportIndex]);

/**
 * Grabs the enabled element `dom` reference of an active viewport.
 *
 * @return {HTMLElement} Active viewport element.
 */
const getEnabledElement = viewportIndex => state.enabledElements[viewportIndex];

export { setEnabledElement, clearEnabledElement, getEnabledElement };
