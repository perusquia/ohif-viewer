import React, { Component } from 'react';
import './SliderDirection.styl'

class SliderDirection extends Component {
  render() {
    const style = { top: this.props.position[1] };
    return (<div className='slider-direction container' style={style}>
      <div>&#8679;<span className='slider-up'>{this.props.scrollDirectionValue.up}</span></div>
      <div>	&#8681;<span className='slider-down'>{this.props.scrollDirectionValue.down}</span></div>
    </div >);
  }
}

export default SliderDirection;
