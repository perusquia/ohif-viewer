import React from 'react';
import asyncComponent from './asyncComponent.js';
import init from './init.js';
import commandsModule from './commandsModule.js';
import toolbarModule from './toolbarModule.js';
import CornerstoneViewportDownloadForm from './CornerstoneViewportDownloadForm';
import { version } from '../package.json';
import {
  Utils as FlywheelCommonUtils,
} from '@flywheel/extension-flywheel-common';

const { clearSubFormQuestion, closeLimitReachedDialog } = FlywheelCommonUtils;

// support for old extension version
const OHIFCornerstoneViewport = asyncComponent(() =>
  import(
    /* webpackChunkName: "OHIFCornerstoneViewport" */ './OHIFCornerstoneViewport.js'
  )
);

const Component = React.lazy(() => {
  return import('./OHIFCornerstoneViewport');
});

// using old load version
const _OHIFCornerstoneViewport = props => {
  return (
    <React.Suspense fallback={<div>Loading...</div>}>
      <Component {...props} />
    </React.Suspense>
  );
};

/**
 *
 */
export default {
  /**
   * Only required property. Should be a unique value across all extensions.
   */
  id: 'cornerstone',
  version,

  /**
   *
   *
   * @param {object} [configuration={}]
   * @param {object|array} [configuration.csToolsConfig] - Passed directly to `initCornerstoneTools`
   */
  preRegistration({ servicesManager, commandsManager, configuration = {} }) {
    init({ servicesManager, commandsManager, configuration });
  },
  getViewportModule({ commandsManager, servicesManager }) {
    const ExtendedOHIFCornerstoneViewport = props => {
      const onNewImageHandler = jumpData => {
        commandsManager.runCommand('jumpToImage', jumpData);
        clearSubFormQuestion();
        closeLimitReachedDialog(servicesManager);
      };
      return (
        <OHIFCornerstoneViewport {...props} onNewImage={onNewImageHandler} />
      );
    };

    return ExtendedOHIFCornerstoneViewport;
  },
  getToolbarModule() {
    return toolbarModule;
  },
  getCommandsModule({ servicesManager, commandsManager }) {
    return commandsModule({ servicesManager, commandsManager });
  },
};

export { CornerstoneViewportDownloadForm, toolbarModule };
