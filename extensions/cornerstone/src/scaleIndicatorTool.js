import cornerstone from 'cornerstone-core';
import cornerstoneMath from 'cornerstone-math';

import TOOL_NAMES from './toolNames';
import IndicatorDisplayTool from './IndicatorDisplayTool';
import { loadScaleIndicator } from '../src/utils/loadScaleIndicator';

const { pixelToCanvas } = cornerstone;
const getDistance = cornerstoneMath.point.distance;

/**
 * @class ScaleIndicatorTool - Render the scale indicators
 * @extends IndicatorDisplayTool
 */
export default class ScaleIndicatorTool extends IndicatorDisplayTool {
  constructor(props = {}) {
    const defaultProps = {
      name: TOOL_NAMES.SCALE_INDICATOR_TOOL,
    };
    const initialProps = Object.assign(defaultProps, props);
    super(initialProps);
    this.data = [];
  }
  // Overriding from parent class.
  getToolData(element) {
    return this.data;
  }
  clearToolData() {
    this.data.length = 0;
  }
  // Overriding from parent class.
  renderToolData(evt) {
    const eventData = evt.detail;
    const element = eventData.element;
    const imageTopLeft = { x: 0, y: 0 };
    const imageBottomLeft = { x: 0, y: eventData.image.rows };
    const canvasTopLeft = pixelToCanvas(element, imageTopLeft);
    const canvasBottomLeft = pixelToCanvas(element, imageBottomLeft);

    if (this.previousCanvasTopLeft && this.previousCanvasBottomLeft) {
      const changeInTopLeft = getDistance(
        canvasTopLeft,
        this.previousCanvasTopLeft
      );
      const changeInBottomLeft = getDistance(
        canvasBottomLeft,
        this.previousCanvasBottomLeft
      );

      // Recalculate scale points if image position in canvas is changed.
      if (changeInTopLeft > 0.5 || changeInBottomLeft > 0.5) {
        loadScaleIndicator(true);
      }
    }
    this.previousCanvasTopLeft = canvasTopLeft;
    this.previousCanvasBottomLeft = canvasBottomLeft;
    super.renderToolData(evt);
  }
}
