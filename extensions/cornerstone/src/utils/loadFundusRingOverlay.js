import store from '@ohif/viewer/src/store';
import csTools from 'cornerstone-tools';
import { Redux } from '@flywheel/extension-flywheel-common';

import TOOL_NAMES from '../toolNames';
import { setToolMode } from './setToolMode';
import {
  checkHasElement,
  getActiveElement,
  getOrCreateImageIdSpecificToolData,
} from './indicatorUtils/utils';
import {
  setRingOverlay,
  getRingData,
  restoreRingOverlay,
  getRestoredPositionFromStore,
  createToolState,
  getUpdatedVisibilityState,
} from './RingOverlay';

const ringOverlayTool = TOOL_NAMES.RING_OVERLAY_TOOL;
const { setFundusRingDisplay, setIsRingOverlayRestoreNeeded } = Redux.actions;
const INDICATOR_SET_LABEL = 'RingOverlay';
const globalImageIdSpecificToolStateManager =
  csTools.globalImageIdSpecificToolStateManager;

const loadFundusRingOverlay = (update = false) => {
  if (!checkHasElement()) {
    return;
  }
  const element = getActiveElement();
  if (!element) {
    return;
  }

  const state = store.getState();
  const { existingIndicator, seriesInstanceUID, enabledElement } = getRingData(
    element
  );
  const index = state.viewports.activeViewportIndex;
  const stackToolState = cornerstoneTools.getToolState(element, 'stack');
  const stackData = stackToolState.data[0];
  const toolState = globalImageIdSpecificToolStateManager.saveToolState();

  const fundusRingOverlay = state.infusions.fundusRingOverlay;
  const isFundusRingDisplayed = fundusRingOverlay[index]?.[seriesInstanceUID];
  const toggleVisibility = update
    ? isFundusRingDisplayed
    : !isFundusRingDisplayed;
  const indicatorSet = {
    indicatorSetLabel: INDICATOR_SET_LABEL,
    SeriesInstanceUID: seriesInstanceUID,
    indicators: [],
    visible: toggleVisibility,
  };
  const indicatorModule = csTools.getModule('indicator');
  if (existingIndicator && !update) {
    indicatorModule.setters.showHideIndicatorSet(
      INDICATOR_SET_LABEL,
      seriesInstanceUID,
      !isFundusRingDisplayed
    );
  }
  if (!existingIndicator) {
    indicatorModule.setters.indicatorSet(indicatorSet);
  }
  if (isFundusRingDisplayed && !update) {
    const visibilityState = getUpdatedVisibilityState(false, seriesInstanceUID);
    store.dispatch(setFundusRingDisplay(visibilityState));
  }
  if (!isFundusRingDisplayed || update) {
    //Restoring ringOverlay position on first hotKey press.
    const isRestoreNeeded = state.infusions.fundusRingOverlay.isRestoreNeeded;
    if (isRestoreNeeded) {
      const restoredPosition = getRestoredPositionFromStore();
      restoreRingOverlay(restoredPosition);
      store.dispatch(setIsRingOverlayRestoreNeeded(false));
    }
    const { toolData } = getRingData(element);

    setRingOverlay(enabledElement, toolData, indicatorSet);
    saveToolState(toolState, stackData, toolData, enabledElement);
    setToolMode(ringOverlayTool, 'passive');
    const visibilityState = getUpdatedVisibilityState(true, seriesInstanceUID);
    store.dispatch(setFundusRingDisplay(visibilityState));
  }
};

const saveToolState = (toolState, stackData, updatedData, enabledElement) => {
  const currentImageId = enabledElement.image.imageId;
  let ringData;

  stackData.imageIds.forEach(imageId => {
    const defaultData = createToolState(imageId, undefined, enabledElement);
    const toolStateData = toolState[imageId]?.RingOverlayTool?.data || [];
    const currentData = toolStateData.length && toolStateData;
    const isCurrentImage = currentImageId === imageId;

    ringData = isCurrentImage ? updatedData : currentData || defaultData;

    if (!toolState[imageId]) {
      toolState[imageId] = {};
    }
    if (toolState[imageId].hasOwnProperty(ringOverlayTool)) {
      delete toolState[imageId].RingOverlayTool;
    }
    const toolData = getOrCreateImageIdSpecificToolData(
      toolState,
      imageId,
      ringOverlayTool
    );
    ringData.forEach(data => {
      if (!data) {
        return;
      }
      data.active = false;
      toolData.push(data);
    });
  });
};

export default loadFundusRingOverlay;
