import store from '@ohif/viewer/src/store';
import cornerstone from 'cornerstone-core';
import csTools from 'cornerstone-tools';
import { utils } from '@ohif/core';
import {
  Utils as FlywheelCommonUtils,
  Redux,
} from '@flywheel/extension-flywheel-common';
import { Redux as FlywheelRedux } from '@flywheel/extension-flywheel';
import getCircleDimensions from '@flywheel/extension-flywheel-common/src/tools/utils/getCircleDimensions';
import {
  getContainersIds,
  findActiveSession,
} from '@flywheel/extension-flywheel/src/utils/containers';
import TOOL_NAMES from '../toolNames';

const ringOverlayTool = TOOL_NAMES.RING_OVERLAY_TOOL;
const globalImageIdSpecificToolStateManager =
  csTools.globalImageIdSpecificToolStateManager;
const getPixelSpacing = csTools.importInternal('util/getPixelSpacing');
const {
  selectCurrentNifti,
  selectCurrentVolumetricImage,
  selectCurrentWebImage,
} = Redux.selectors;
const {
  isCurrentNifti,
  isCurrentVolumetricImage,
  isCurrentWebImage,
} = FlywheelCommonUtils;
const { studyMetadataManager } = utils;

const setRingOverlay = (enabledElement, toolData, indicatorSet) => {
  const projectConfig = store.getState().flywheel.projectConfig;
  const ringOverlayConfig = projectConfig?.ringOverlay;
  const isDottedLine = ringOverlayConfig?.isDottedLine || false;
  const ringCount = getRingCount();

  let indicators = [];
  let incrementor = 1;

  for (let i = 0; i < ringCount; i++) {
    let currentPoints;
    if (toolData?.length && toolData[i]) {
      const point = toolData[i].handles;
      currentPoints = [point.start, point.end];
    } else {
      currentPoints = getDefaultPosition(enabledElement)[i];
    }
    if (!currentPoints) {
      return;
    }
    const data = {
      start: currentPoints[0],
      end: currentPoints[1],
      SeriesInstanceUID: indicatorSet.SeriesInstanceUID,
    };
    indicators.push(data);
  }

  indicators.forEach(data => {
    const ringData = {
      handles: { start: data.start, end: data.end },
      type: 'CIRCULAR',
      indicatorSeriesInstanceUid: data.SeriesInstanceUID,
      indicatorNumber: incrementor,
      isDottedLine,
    };
    const existingRing = toolData?.find(
      data => data.indicatorNumber === incrementor
    );
    if (existingRing) {
      existingRing.handles = ringData.handles;
    } else {
      toolData?.push(ringData);
    }
    const IndicatorData = {
      IndicatorNumber: incrementor,
      visible: true,
    };
    indicatorSet.indicators.push(IndicatorData);
    incrementor++;
  });
};

const getDefaultPosition = enabledElement => {
  const image = enabledElement.image;
  const center = getImageCenter(image);
  const pixelSpacing = getPixelSpacing(image);

  const defaultPosition = getPositionFromCenterPoint(center, pixelSpacing);

  return defaultPosition;
};

const getPositionFromCenterPoint = (center, pixelSpacing) => {
  const defaultDiameters = [0.5, 1.5, 3];
  const projectConfig = store.getState().flywheel.projectConfig;
  const ringOverlayConfig = projectConfig?.ringOverlay;
  const ringDiameters = ringOverlayConfig?.diameters || defaultDiameters;
  const position = [];

  for (let i = 0; i < ringDiameters.length; i++) {
    const circleWidthAndHeight = getCircleDimensions(
      pixelSpacing,
      ringDiameters[i]
    );
    const start = {
      x: center.x - circleWidthAndHeight.width / 2,
      y: center.y - circleWidthAndHeight.height / 2,
    };
    const end = {
      x: center.x + circleWidthAndHeight.width / 2,
      y: center.y + circleWidthAndHeight.height / 2,
    };
    position.push([start, end]);
  }

  return position;
};

const getImageCenter = image => {
  const { width, height } = image;

  return {
    x: width / 2,
    y: height / 2,
  };
};

const createToolState = (imageId, center, element) => {
  const pixelSpacing = getPixelSpacingFromImageId(imageId);
  let defaultPosition;

  if (center) {
    defaultPosition = getPositionFromCenterPoint(center, pixelSpacing);
  } else {
    defaultPosition = getDefaultPosition(element);
  }
  const projectConfig = store.getState().flywheel.projectConfig;
  const ringOverlayConfig = projectConfig?.ringOverlay;
  const isDottedLine = ringOverlayConfig?.isDottedLine || false;
  const activeInstance = cornerstone.metaData.get('instance', imageId);
  const seriesInstanceUID = activeInstance.SeriesInstanceUID;
  let indicators = [];
  let toolData = [];
  let incrementor = 1;

  defaultPosition.forEach(position => {
    const data = {
      start: position[0],
      end: position[1],
      SeriesInstanceUID: seriesInstanceUID,
    };
    indicators.push(data);
  });

  indicators.forEach(data => {
    const ringData = {
      handles: { start: data.start, end: data.end },
      type: 'CIRCULAR',
      indicatorSeriesInstanceUid: data.SeriesInstanceUID,
      indicatorNumber: incrementor,
      isDottedLine,
    };
    toolData.push(ringData);
    incrementor++;
  });

  return toolData;
};

const getRingOverlayPositions = (filterParams = {}) => {
  const restoredPosition = getRestoredPositionFromStore() || {};
  const ringOverlay = restoredPosition;
  const imageIds = getImageIds();
  const studyInstanceUid = getStudyInstanceUid();

  imageIds.forEach(imageId => {
    const imagePlaneModule = cornerstone.metaData.get(
      'imagePlaneModule',
      imageId
    );
    const { rows, columns } = imagePlaneModule;
    const imageCenter = { x: columns / 2, y: rows / 2 };
    const toolState = globalImageIdSpecificToolStateManager.saveToolState();
    if (toolState[imageId]?.hasOwnProperty(ringOverlayTool)) {
      const data = toolState[imageId].RingOverlayTool.data[0];
      const start = data.handles.start;
      const end = data.handles.end;
      const center = { x: (start.x + end.x) / 2, y: (start.y + end.y) / 2 };
      const isDefaultPosition =
        imageCenter.x === center.x && imageCenter.y === center.y;
      const shouldAddPosition =
        !filterParams?.studyInstanceUid ||
        filterParams?.studyInstanceUid === studyInstanceUid;

      if (!isDefaultPosition && shouldAddPosition) {
        const formattedImageId = imageId.replaceAll('.', '_');
        if (ringOverlay?.[formattedImageId]) {
          delete ringOverlay[formattedImageId];
        }
        ringOverlay[imageId] = center;
      }
    }
  });
  return ringOverlay;
};

const getImageIds = () => {
  const studyInstanceUid = getStudyInstanceUid();
  const studyMetadata = studyMetadataManager.get(studyInstanceUid);
  const displaySets = studyMetadata.getDisplaySets();
  let imageIds = [];

  displaySets.forEach(imageSet => {
    const images = imageSet.images;

    if (imageSet.isDerived || !images) {
      return;
    }
    const isMultiFrame = imageSet.isMultiFrame;
    const numImageFrames = imageSet.numImageFrames;

    if (!isMultiFrame) {
      images.forEach((image, index) => {
        const imageId = image._imageId || image.getImageId(index);
        imageIds.push(imageId);
      });
    } else if (numImageFrames > 1) {
      for (let i = 0; i < numImageFrames; ++i) {
        const imageId = images[0].getImageId(i);

        if (imageId) imageIds.push(imageId);
      }
    }
  });
  imageIds = [...new Set(imageIds)];
  return imageIds;
};

const getStudyInstanceUid = () => {
  const state = store.getState();
  const viewports = state.viewports;
  const index = viewports.activeViewportIndex;
  const studyInstanceUid =
    viewports.viewportSpecificData[index].StudyInstanceUID;

  return studyInstanceUid;
};

const restoreRingOverlay = ringOverlay => {
  const toolState = globalImageIdSpecificToolStateManager.saveToolState();
  const imageIds = getImageIds();

  imageIds.forEach(imageId => {
    if (!toolState[imageId]) {
      toolState[imageId] = {};
    }

    const formattedImageId = imageId.replaceAll('.', '_');
    if (ringOverlay?.[formattedImageId] || ringOverlay?.[imageId]) {
      const position = ringOverlay[formattedImageId] || ringOverlay[imageId];
      const toolStateData = createToolState(imageId, position);
      toolState[imageId][ringOverlayTool] = {
        data: toolStateData,
      };
    }
  });
};

const getRestoredPositionFromStore = () => {
  const state = store.getState();
  let currentData;

  if (isCurrentNifti(state)) {
    currentData = selectCurrentNifti(state);
  } else if (isCurrentVolumetricImage(state)) {
    currentData = selectCurrentVolumetricImage(state);
  } else if (isCurrentWebImage()) {
    currentData = selectCurrentWebImage(state);
  } else {
    const {
      selectAllAssociations,
      selectAllSessions,
    } = FlywheelRedux.selectors;
    const activeViewPortIndex = state?.viewports?.activeViewportIndex;
    const StudyInstanceUID =
      state?.viewports?.viewportSpecificData[activeViewPortIndex]
        ?.StudyInstanceUID;
    const associations = selectAllAssociations(state);
    let ringOverlay;
    const studyAssociations =
      (associations || []).filter(
        assoc => assoc.study_uid === StudyInstanceUID
      ) || [];
    studyAssociations.forEach(assoc => {
      const sessionId = assoc.session_id;
      if (!sessionId) {
        return;
      }
      const data =
        findActiveSession(sessionId) || selectAllSessions(state)[sessionId];
      currentData = data?.info?.ohifViewer?.ringOverlay || currentData;
      if (currentData) {
        ringOverlay = { ...currentData };
      }
    });
    return ringOverlay;
  }
  const ringOverlay = currentData?.info?.ohifViewer?.ringOverlay || {};
  return ringOverlay;
};

const getRingData = element => {
  const enabledElement = cornerstone.getEnabledElement(element);
  const indicatorModule = csTools.getModule('indicator');
  const tool = csTools.getToolForElement(element, ringOverlayTool);
  const toolData = tool.getToolData(element);
  const activeInstance = cornerstone.metaData.get(
    'instance',
    enabledElement.image.imageId
  );
  const seriesInstanceUID = activeInstance.SeriesInstanceUID;
  const existingIndicator = indicatorModule.getters.indicatorSet(
    'RingOverlay',
    seriesInstanceUID
  );

  return { toolData, existingIndicator, seriesInstanceUID, enabledElement };
};

const getRingCount = () => {
  const DEFAULT_COUNT = 3;

  const projectConfig = store.getState().flywheel.projectConfig;
  const ringOverlayConfig = projectConfig?.ringOverlay;
  const configuredLength = ringOverlayConfig?.diameters?.length;
  const count = configuredLength || DEFAULT_COUNT;

  return count;
};

const getUpdatedVisibilityState = (updatedState, seriesInstanceUID) => {
  const state = store.getState();
  const visibilityState = state.infusions.fundusRingOverlay || {};
  const viewportSpecificData = state.viewports.viewportSpecificData;

  for (let key in viewportSpecificData) {
    if (!visibilityState[key]) {
      visibilityState[key] = {};
    }
    visibilityState[key][seriesInstanceUID] = updatedState;
  }
  return visibilityState;
};

const getPixelSpacingFromImageId = imageId => {
  const imagePlane = cornerstone.metaData.get('imagePlaneModule', imageId);
  let rowPixelSpacing;
  let colPixelSpacing;

  if (imagePlane) {
    rowPixelSpacing =
      imagePlane.rowPixelSpacing || imagePlane.rowImagePixelSpacing;
    colPixelSpacing =
      imagePlane.columnPixelSpacing || imagePlane.colImagePixelSpacing;
  }

  return {
    rowPixelSpacing,
    colPixelSpacing,
  };
};

export {
  setRingOverlay,
  createToolState,
  getRingOverlayPositions,
  restoreRingOverlay,
  getImageCenter,
  getImageIds,
  getRingData,
  getRingCount,
  getRestoredPositionFromStore,
  getUpdatedVisibilityState,
  getPixelSpacingFromImageId,
};
