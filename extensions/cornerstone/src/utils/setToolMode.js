/**
 * Set the given tool mode if not already and updating image.
 * @param {string} toolName - tool name
 * @param {string} toolMode - tool mode
 * @return void
 */
function setToolMode(toolName, toolMode) {
  cornerstone.getEnabledElements().forEach(enabledElement => {
    const { element, image } = enabledElement;
    const tool = cornerstoneTools.getToolForElement(element, toolName);

    if (tool.mode !== toolMode) {
      switch (toolMode) {
        case 'enabled':
          cornerstoneTools.setToolEnabled(toolName);
          break;
        case 'disabled':
          cornerstoneTools.setToolDisabled(toolName);
          break;
        case 'passive':
          cornerstoneTools.setToolPassive(toolName);
          break;
      }
    }

    if (image) {
      cornerstone.updateImage(element);
    }
  });
}

export { setToolMode };
