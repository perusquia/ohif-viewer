import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';
import TOOL_NAMES from '../../toolNames';

/**
 * Get the label of indicator set for given tool name.
 * @param {string} toolName Name of the tool.
 * @return string - label of the indicator set.
 */
const getIndicatorLabel = toolName => {
  let label = '';
  switch (toolName) {
    case TOOL_NAMES.INDICATOR_DISPLAY_TOOL:
      label = 'FOV Indicator';
      break;
    case TOOL_NAMES.SCALE_INDICATOR_TOOL:
      label = 'Scale Indicator';
      break;
    case TOOL_NAMES.RING_OVERLAY_TOOL:
      label = 'RingOverlay';
      break;
    default:
      label = '';
  }
  return label;
};

/**
 * Get the image specific tool data, create if not already exist.
 * @param {Object} toolState cornerstone tool state
 * @param {string} imageId - image id
 * @param {string} toolName - tool name
 * @return Object - image specific tool data
 */
const getOrCreateImageIdSpecificToolData = (toolState, imageId, toolName) => {
  if (!toolState.hasOwnProperty(imageId)) {
    toolState[imageId] = {};
  }

  const imageIdToolState = toolState[imageId];

  // If we don't have tool state for this type of tool, add an empty object
  if (!imageIdToolState.hasOwnProperty(toolName)) {
    imageIdToolState[toolName] = {
      data: [],
    };
  }

  return imageIdToolState[toolName].data;
};

const checkHasElement = () => {
  const hasElement = cornerstone.getEnabledElements().some(enabledElement => {
    if (enabledElement.image) {
      return true;
    }
    return false;
  });
  return hasElement;
};

const getActiveElement = () => {
  const state = store.getState();
  const index = state.viewports.activeViewportIndex;

  return FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(index);
};

export {
  getIndicatorLabel,
  getOrCreateImageIdSpecificToolData,
  checkHasElement,
  getActiveElement,
};
