import store from '@ohif/viewer/src/store';
import cornerstone from 'cornerstone-core';
import cornerstoneMath from 'cornerstone-math';

const { pixelToCanvas } = cornerstone;
const getDistance = cornerstoneMath.point.distance;

function loadPercentData(enabledElement, toolData, indicatorSet) {
  const projectConfig = store.getState().flywheel.projectConfig;
  const scaleIndicatorConfig = projectConfig?.scaleIndicator;
  const { division, subdivision } = getDivisions(scaleIndicatorConfig);
  const color = scaleIndicatorConfig?.color || 'white';
  const showEndLinesValue = scaleIndicatorConfig?.showEndLinesValue || false;
  let incrementor = 1;

  const divisionOptions = {
    type: 'division',
    division,
    incrementor,
    enabledElement,
    lineHeight: 22,
    showEndLinesValue,
    color,
  };
  incrementor = addToolData(toolData, indicatorSet, divisionOptions);
  const subdivisionOptions = {
    type: 'subdivision',
    division: subdivision,
    incrementor,
    enabledElement,
    lineHeight: 10,
    showEndLinesValue,
    color,
  };
  addToolData(toolData, indicatorSet, subdivisionOptions);
}
function getLinePoints(canvasPoint, lineHeight) {
  const linePoints = [
    canvasPoint,
    {
      x: canvasPoint.x,
      y: canvasPoint.y + lineHeight,
    },
  ];
  return linePoints;
}

function addToolData(toolData, indicatorSet, options) {
  const {
    enabledElement,
    type,
    division,
    lineHeight,
    showEndLinesValue,
  } = options;
  if (!division) {
    return;
  }
  const element = enabledElement.element;
  const canvasHeight = element.clientHeight;
  const {
    scaleWidth,
    scaleTopLeft,
    scaleTopRight,
    scaleBottomRight,
  } = getScaleDataOnCanvas(enabledElement);
  const lineWidth = type === 'division' ? 4 : 2;
  const noOfDivisions = parseFloat(Math.floor(100 / division).toFixed(0));
  const maxIndex = parseFloat(Math.ceil(100 / division).toFixed(0));
  const TOP = 'top';
  const BOTTOM = 'bottom';
  let incrementor = options.incrementor || 1;
  let indicators = [];

  for (let i = 0; i <= noOfDivisions; i++) {
    const percentage = i * division;
    const locationX = (percentage / 100) * scaleWidth;
    let value = type === 'division' ? percentage : '';

    if (type === 'division' && (percentage === 0 || percentage === 100)) {
      value = showEndLinesValue ? percentage : '';
    }
    const imageTopPoint = { x: scaleTopLeft.x + locationX, y: 0 };
    const imageBottomPoint = { x: scaleTopLeft.x + locationX, y: canvasHeight };
    const topLinePoints = getLinePoints(imageTopPoint, lineHeight);
    const bottomLinePoints = getLinePoints(imageBottomPoint, -lineHeight);

    const topLine = {
      line: topLinePoints,
      value,
      SeriesInstanceUID: indicatorSet.SeriesInstanceUID,
      textPosition: getTextLocationOnCanvas(topLinePoints, value, TOP),
      pointPosition: TOP,
    };
    indicators.push(topLine);

    const isValueInBetweenScale = value !== '' && value !== 0 && value !== 100;
    let bottomValue = value;
    if (isValueInBetweenScale) {
      bottomValue = (maxIndex - i) * division;
    }
    if (value === 0 || value === 100) {
      bottomValue = 100 - value;
    }

    const bottomLine = {
      line: bottomLinePoints,
      value: bottomValue,
      SeriesInstanceUID: indicatorSet.SeriesInstanceUID,
      textPosition: getTextLocationOnCanvas(
        bottomLinePoints,
        bottomValue,
        BOTTOM
      ),
      pointPosition: BOTTOM,
    };
    indicators.push(bottomLine);
  }

  // In the case of decimal division, the line on 100% may not draw.
  // So, adding scale lines on 100% of image.
  if (type === 'division') {
    const topEndLinePoints = getLinePoints(scaleTopRight, lineHeight);
    const bottomEndLinePoints = getLinePoints(scaleBottomRight, -lineHeight);
    const topValue = showEndLinesValue ? 100 : '';
    const bottomValue = showEndLinesValue ? 0 : '';

    const topEndLine = {
      line: topEndLinePoints,
      value: topValue,
      SeriesInstanceUID: indicatorSet.SeriesInstanceUID,
      textPosition: getTextLocationOnCanvas(topEndLinePoints, topValue, TOP),
      pointPosition: TOP,
    };
    indicators.push(topEndLine);

    const bottomEndLine = {
      line: bottomEndLinePoints,
      value: bottomValue,
      SeriesInstanceUID: indicatorSet.SeriesInstanceUID,
      textPosition: getTextLocationOnCanvas(
        bottomEndLinePoints,
        bottomValue,
        BOTTOM
      ),
      pointPosition: BOTTOM,
    };
    indicators.push(bottomEndLine);
  }

  indicators.forEach(data => {
    const value = getFormattedValue(data.value);
    const topLineData = {
      handles: {
        points: [data.line[0], data.line[1]],
      },
      pointPosition: data.pointPosition,
      type: 'OPEN_PLANAR',
      indicatorSeriesInstanceUid: data.SeriesInstanceUID,
      lineWidth: data.width ? data.width : lineWidth,
      indicatorNumber: incrementor,
      textPosition: data.textPosition,
      color: options.color,
      value,
      coordSystem: 'canvas',
    };
    toolData.push(topLineData);
    createIndicatorData(indicatorSet, incrementor);
    incrementor++;
  });
  return incrementor;
}

function getScaleDataOnCanvas(enabledElement) {
  const { rows, columns } = enabledElement.image;
  const element = enabledElement.element;
  const canvasHeight = element.clientHeight;
  const canvasWidth = element.clientWidth;
  const imageCenter = { x: columns / 2, y: rows / 2 };
  const imageLeftFromCenter = { x: 0, y: rows / 2 };
  const imageCenterOnCanvas = pixelToCanvas(element, imageCenter);
  const imageLeftFromCenterOnCanvas = pixelToCanvas(
    element,
    imageLeftFromCenter
  );
  const scaleWidth =
    2 * getDistance(imageCenterOnCanvas, imageLeftFromCenterOnCanvas);
  let scaleStartX = canvasWidth / 2 - scaleWidth / 2;

  // By default, center of the scale is in the center of the viewport.
  // In the case of zoomed image, we need to change the start of the scale to the origin of the viewport.
  scaleStartX = scaleStartX < 0 ? 0 : scaleStartX;

  const scaleTopLeft = { x: scaleStartX, y: 0 };
  const scaleTopRight = { x: scaleStartX + scaleWidth, y: 0 };
  const scaleBottomLeft = { x: scaleStartX, y: canvasHeight };
  const scaleBottomRight = { x: scaleStartX + scaleWidth, y: canvasHeight };

  return {
    scaleWidth,
    scaleTopLeft,
    scaleTopRight,
    scaleBottomLeft,
    scaleBottomRight,
  };
}
function createIndicatorData(indicatorSet, index) {
  const IndicatorData = {
    IndicatorNumber: index,
    visible: true,
  };

  indicatorSet.indicators.push(IndicatorData);
}

function getDivisions(scaleConfig) {
  const defaultDivision = 25;
  const defaultSubDivision = 5;

  if (!scaleConfig) {
    return {
      division: defaultDivision,
      subdivision: defaultSubDivision,
    };
  }
  const configDivision = Math.abs(scaleConfig.division);
  const configSubDivision = Math.abs(scaleConfig.subdivision);

  return {
    division: Math.min(100, Math.max(0, configDivision)),
    subdivision: Math.min(100, Math.max(0, configSubDivision)),
  };
}

function getTextLocationOnCanvas(points, textValue, textLocation) {
  const isEndValue = textValue === 0 || textValue === 100;
  const offsets = {
    default: -12,
    topZero: -7,
    bottomZero: -13,
    topHundered: -30,
    bottomHundered: -8,
    height: -23,
  };
  let offsetX = 0;
  let offsetY = 0;

  if (isEndValue) {
    if (textLocation === 'top') {
      offsetX = textValue === 0 ? offsets.topZero : offsets.topHundered;
    } else {
      offsetX = textValue === 0 ? offsets.bottomZero : offsets.bottomHundered;
      offsetY = offsets.height;
    }
  } else {
    offsetX = offsets.default;
    offsetY = textLocation === 'top' ? 0 : offsets.height;
  }
  const canvasPoint2 = points[1];

  const textPosition = {
    x: canvasPoint2.x + offsetX,
    y: canvasPoint2.y + offsetY,
  };
  return textPosition;
}

function getFormattedValue(value) {
  const formattedValue =
    value === '' || value % 1 === 0 ? value : value.toFixed(1);

  return formattedValue;
}

export default loadPercentData;
