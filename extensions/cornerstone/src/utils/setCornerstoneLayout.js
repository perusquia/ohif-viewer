import OHIF, { redux } from '@ohif/core';
import store from '@ohif/viewer/src/store';

const {
  clearViewportSpecificData,
  setLayout,
  setViewportSpecificData,
} = redux.actions;
const { studyMetadataManager } = OHIF.utils;

/**
 * Update the current layout with a simple Cornerstone one
 *
 * @return void
 */
const setCornerstoneLayout = () => {
  const { viewportSpecificData } = store.getState().viewports;
  const viewportLayout = store.getState().viewports.layout;
  const layout = {
    numRows: 1,
    numColumns: 1,
    viewports: [{ plugin: 'cornerstone' }],
  };
  const isMultiSessionViewEnabled = store?.getState()?.multiSessionData
    ?.isMultiSessionViewEnabled;
  if (isMultiSessionViewEnabled) {
    layout.numColumns *= 2;
    layout.viewports = [{ plugin: 'cornerstone' }, { plugin: 'cornerstone' }];
    store.dispatch(clearViewportSpecificData());
  }

  const isNiftiProtocol =
    Object.keys(viewportSpecificData).length &&
    Object.keys(viewportSpecificData).findIndex(
      data => viewportSpecificData[data].dataProtocol === 'nifti'
    ) > -1;
  const isMetaIOProtocol =
    Object.keys(viewportSpecificData).length &&
    Object.keys(viewportSpecificData).findIndex(
      data => viewportSpecificData[data].dataProtocol === 'metaimage'
    ) > -1;
  if (isNiftiProtocol || isMetaIOProtocol) {
    store.dispatch(clearViewportSpecificData());
    const validIndex = [];
    Object.keys(viewportSpecificData).forEach((key, index) => {
      if (viewportSpecificData[key].StudyInstanceUID) {
        validIndex.push(index);
      }
    });
    const study = studyMetadataManager.get(
      viewportSpecificData[validIndex.length > 0 ? validIndex[0] : 0]
        ?.StudyInstanceUID
    );
    const layouts = [];
    const numberOfViewports = Math.min(3, study.displaySets.length);
    const displaySets = study.displaySets.slice(0, numberOfViewports) || [];
    displaySets.forEach((item, index) => {
      layouts.push({
        plugin: isMetaIOProtocol
          ? 'cornerstone::metaimage'
          : 'cornerstone::nifti',
      });
      store.dispatch(setViewportSpecificData(index, item));
    });
    layout.numColumns = numberOfViewports;
    layout.viewports = layouts;
  }

  store.dispatch(setLayout(layout));
};

export default setCornerstoneLayout;
