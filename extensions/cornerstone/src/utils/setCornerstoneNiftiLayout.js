import cornerstone from 'cornerstone-core';
import { cornerstoneUtils } from '@flywheel/extension-cornerstone-infusions';

export const updateNiftiViewports = (
  commandsManager,
  toolNames,
  activeViewport,
  element,
  isActiveImageFromStack
) => {
  const enabledElement = cornerstone.getEnabledElement(element);
  let propertiesToSync = {};
  let plugin = activeViewport.plugin;
  propertiesToSync.seriesInstanceUID = activeViewport.SeriesInstanceUID;
  propertiesToSync.studyInstanceUID = activeViewport.StudyInstanceUID;
  toolNames.forEach(toolName => {
    if (toolName === 'Wwwc') {
      propertiesToSync.windowLevel = {
        windowCenter: enabledElement.viewport.voi.windowCenter,
        windowWidth: enabledElement.viewport.voi.windowWidth,
      };
    } else if (
      toolName === 'StackScroll' ||
      toolName === 'Crosshairs' ||
      toolName === 'StackScrollMouseWheel'
    ) {
      let patientPos = cornerstoneUtils.getPatientPosition(enabledElement, isActiveImageFromStack);
      if (patientPos) {
        propertiesToSync.patientPos = patientPos;
      }
    } else if (toolName === 'Zoom' || toolName === 'ZoomMouseWheel') {
      const defaultViewport = cornerstone.getDefaultViewport(
        enabledElement.canvas,
        enabledElement.image
      );
      propertiesToSync.zoom = {
        type: 'relative',
        scale: enabledElement.viewport.scale - defaultViewport.scale,
      };
      propertiesToSync.pan = enabledElement.viewport.translation;
    } else if (toolName === 'Rotate') {
      propertiesToSync.rotate = enabledElement.viewport.rotation;
    }
  });

  commandsManager?.runCommand("updateNiftiSingleView", {
    plugin
  });
};
