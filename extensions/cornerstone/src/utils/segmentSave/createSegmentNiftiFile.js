import { Vector3 } from 'cornerstone-math';
import pako from 'pako';

/**
 * Create NiFTI compressed file(NiFTI - 1 data format) from segmentation info and label map
 * Note: The hard-coded bit position and default values
 * for filling properties are based on the corner-nifti-loader module.
 * @param {Object} seriesMetaData
 * @param {Object} segmentDetails
 * @param {Object} labelMaps2D
 * @param {boolean} shouldLpsToRasConversion
 * @param {boolean} niftiHeader
 * @returns compressed NiFTI - 1 data format file
 */
export default function createSegmentNiftiFile(
  seriesMetaData,
  segmentDetails,
  labelMaps2D,
  shouldLpsToRasConversion,
  niftiHeader,
  sourceDataProtocol,
  extension = '.nii.gz'
) {
  const { metaData, numberOfFrames } = seriesMetaData;
  const imageSize = metaData.columns * metaData.rows;
  const totalBufferSize = imageSize * numberOfFrames;
  let fileData = new Int8Array(352 + totalBufferSize);
  const rawData = new DataView(fileData.buffer);
  const header = niftiHeader?.header || null;

  rawData.setInt32(0, 348);
  rawData.setInt8(39, header?.dim_info || 54);

  const dims = header?.dims || [];
  rawData.setInt16(40, dims[0] || 3);
  rawData.setInt16(42, dims[1] || metaData.columns);
  rawData.setInt16(44, dims[2] || metaData.rows);
  rawData.setInt16(46, dims[3] || numberOfFrames);
  rawData.setInt16(48, dims[4] || 1);
  rawData.setInt16(50, dims[5] || 1);
  rawData.setInt16(52, dims[6] || 1);
  rawData.setInt16(54, dims[7] || 1);

  rawData.setFloat32(56, header?.intent_p1 || 0);
  rawData.setFloat32(60, header?.intent_p2 || 0);
  rawData.setFloat32(64, header?.intent_p3 || 0);
  rawData.setInt16(68, header?.intent_code || 0);

  rawData.setInt16(70, 2); // Data type code 2 as only expecting 1, 0 as values
  rawData.setInt16(72, 8); // Bits per pixel set as 8 as only expecting 1, 0 as values
  rawData.setInt16(74, header?.slice_start || 0);

  const pixDims = header?.pixDims || [];
  rawData.setFloat32(76, pixDims[0] || 1);
  rawData.setFloat32(80, pixDims[1] || metaData.columnPixelSpacing);
  rawData.setFloat32(84, pixDims[2] || metaData.rowPixelSpacing);
  rawData.setFloat32(88, pixDims[3] || metaData.sliceThickness || 1);
  rawData.setFloat32(92, pixDims[4] || 1);
  rawData.setFloat32(96, pixDims[5] || 0);
  rawData.setFloat32(100, pixDims[6] || 0);
  rawData.setFloat32(104, pixDims[7] || 0);

  rawData.setFloat32(108, header?.vox_offset || 352);

  rawData.setFloat32(112, header?.scl_slope || 1);
  rawData.setFloat32(116, header?.scl_inter || 0);
  rawData.setInt16(120, header?.slice_end || 0);
  rawData.setInt8(122, header?.slice_code || 0);

  rawData.setInt8(123, header?.xyzt_units || 10);

  rawData.setFloat32(124, header?.cal_max || 0);
  rawData.setFloat32(128, header?.cal_min || 0);
  rawData.setFloat32(132, header?.slice_duration || 0);
  rawData.setFloat32(136, header?.toffset || 0);

  const description = header?.description || 'MaskRoiSegment';
  const data = Int8Array.from(description, x => x.charCodeAt(0));
  for (let i = 148; i < 228; i += 1) {
    const val = data[i];
    if (val) {
      rawData.setInt8(i, val);
    }
  }

  rawData.setInt16(252, header?.qform_code || 0);
  rawData.setInt16(254, header?.sform_code || 1);

  const dirMultiplier = shouldLpsToRasConversion ? -1 : 1;

  rawData.setFloat32(268, dirMultiplier * metaData.imagePositionPatient[0]);
  rawData.setFloat32(272, dirMultiplier * metaData.imagePositionPatient[1]);
  rawData.setFloat32(276, metaData.imagePositionPatient[2]);

  let scanAxisNormal;
  if (sourceDataProtocol === 'dicom') {
    scanAxisNormal = new Vector3(1, 1, 1);
  } else {
    scanAxisNormal = new Vector3(
      metaData.imageOrientationPatient[0],
      metaData.imageOrientationPatient[1],
      metaData.imageOrientationPatient[2]
    ).cross(
      new Vector3(
        metaData.imageOrientationPatient[3],
        metaData.imageOrientationPatient[4],
        metaData.imageOrientationPatient[5]
      )
    );
  }

  rawData.setFloat32(
    280,
    dirMultiplier *
      metaData.imageOrientationPatient[0] *
      metaData.rowPixelSpacing
  );
  rawData.setFloat32(
    284,
    dirMultiplier *
      metaData.imageOrientationPatient[3] *
      metaData.columnPixelSpacing
  );
  rawData.setFloat32(
    288,
    dirMultiplier * scanAxisNormal.x * metaData.sliceSpacing[0]
  );
  rawData.setFloat32(292, dirMultiplier * metaData.imagePositionPatient[0]);
  rawData.setFloat32(
    296,
    dirMultiplier *
      metaData.imageOrientationPatient[1] *
      metaData.rowPixelSpacing
  );
  rawData.setFloat32(
    300,
    dirMultiplier *
      metaData.imageOrientationPatient[4] *
      metaData.columnPixelSpacing
  );
  rawData.setFloat32(
    304,
    dirMultiplier * scanAxisNormal.y * metaData.sliceSpacing[1]
  );
  rawData.setFloat32(308, dirMultiplier * metaData.imagePositionPatient[1]);
  rawData.setFloat32(
    312,
    metaData.imageOrientationPatient[2] * metaData.rowPixelSpacing
  );
  rawData.setFloat32(
    316,
    metaData.imageOrientationPatient[5] * metaData.columnPixelSpacing
  );
  rawData.setFloat32(320, scanAxisNormal.z * metaData.sliceSpacing[2]);
  rawData.setFloat32(324, metaData.imagePositionPatient[2]);

  // Setting "n+1" as magic field value.
  rawData.setUint8(344, 110);
  rawData.setUint8(345, 43);
  rawData.setUint8(346, 49);

  // If the data is in complete file data format, then set it directly.
  if (segmentDetails.fileData) {
    segmentDetails.fileData.forEach((value, index) => {
      const baseIndex = 352 + index;
      const pixelVal =
        value === segmentDetails.segmentIndex ? segmentDetails.segmentIndex : 0;
      rawData.setInt8(baseIndex, pixelVal);
    });
  } else {
    for (let i = segmentDetails.start; i <= segmentDetails.end; i++) {
      const baseIndex = 352 + i * imageSize;
      if (segmentDetails.slices.includes(i)) {
        labelMaps2D[i].pixelData.forEach((value, index) => {
          const pixelVal =
            value === segmentDetails.segmentIndex
              ? segmentDetails.segmentIndex
              : 0;
          rawData.setInt8(baseIndex + index, pixelVal);
        });
      }
    }
  }

  const gZipData = pako.gzip(fileData);
  const segmentationData = extension.includes('.gz') ? gZipData : fileData;
  const file = new File(
    [segmentationData],
    `${segmentDetails.name}${segmentDetails.fileExtension}`,
    { type: '' }
  );

  return file;
}
