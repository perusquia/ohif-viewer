import cornerstone from 'cornerstone-core';
import cornerstoneTools from 'cornerstone-tools';
import dcmjs from 'dcmjs';

export default async function createSegmentDicomSegFile(
  activeViewport,
  segmentDetails,
  labelmaps3D
) {
  const stackToolState = cornerstoneTools.getToolState(activeViewport, 'stack');
  const imageIds = stackToolState.data[0].imageIds;
  let imagePromises = [];

  for (let i = 0; i < imageIds.length; i++) {
    imagePromises.push(cornerstone.loadImage(imageIds[i]));
  }

  let segBlob;
  let labelmaps2D = labelmaps3D?.[0]?.labelmaps2D;

  return Promise.all(imagePromises)
    .then(async images => {
      if (images) {
        const dataSets = [];

        images.forEach((image, index) => {
          const instance = cornerstone.metaData.get('instance', image.imageId);
          dataSets.push({
            ...instance,
            PixelData: image.getPixelData(),
            _meta: {},
          });
        });

        const multiframe = dcmjs.normalizers.Normalizer.normalizeToDataset(
          dataSets
        );
        const options = {
          includeSliceSpacing: true,
          SeriesDescription: segmentDetails?.name || 'Mask Segmentation',
        };
        const segmentation = new dcmjs.derivations.Segmentation(
          [multiframe],
          options
        );

        const ImageNormalizer = dcmjs.normalizers.ImageNormalizer;
        const referenceDataset = dataSets[0];
        const referencePosition = referenceDataset.ImagePositionPatient;
        const rowVector = referenceDataset.ImageOrientationPatient.slice(0, 3);
        const columnVector = referenceDataset.ImageOrientationPatient.slice(
          3,
          6
        );
        const scanAxis = ImageNormalizer.vec3CrossProduct(
          rowVector,
          columnVector
        );
        const distanceDatasetPairs = [];

        dataSets.forEach(function(dataset) {
          const position = dataset.ImagePositionPatient.slice();
          const positionVector = ImageNormalizer.vec3Subtract(
            position,
            referencePosition
          );
          const distance = ImageNormalizer.vec3Dot(positionVector, scanAxis);
          distanceDatasetPairs.push([distance, dataset]);
        });

        distanceDatasetPairs.sort(function(a, b) {
          return b[0] - a[0];
        });

        const labelmaps2DReplacingDetails = [];
        let labelMaps2DData = [];

        labelmaps2D?.length &&
          labelmaps2D.forEach((item, index) => {
            if (item) {
              const SOPInstanceUID = dataSets[index].SOPInstanceUID;
              const replaceIndex = distanceDatasetPairs?.findIndex(
                pair => pair[1].SOPInstanceUID === SOPInstanceUID
              );
              labelmaps2DReplacingDetails.push({
                index: index,
                replaceIndex: replaceIndex,
              });
              labelMaps2DData[replaceIndex] = labelmaps2D[index];
            }
          });

        labelmaps2D?.forEach((data, index) => {
          if (data) {
            labelmaps2D[index] = undefined;
          }
        });

        labelMaps2DData?.forEach((item, index) => {
          if (item) {
            labelmaps2D[index] = labelMaps2DData[index];
          }
        });

        segBlob = dcmjs.adapters.Cornerstone.Segmentation.fillSegmentation(
          segmentation,
          labelmaps3D,
          options
        );

        const file = new File(
          [segBlob],
          `${segmentDetails?.name}${segmentDetails?.fileExtension}`,
          { type: '' }
        );
        return file;
      }
    })
    .catch(err => {
      throw new Error(
        err.statusText || 'Failed to create DICOM SEG for the mask'
      );
    });
}
