import cornerstoneTools from 'cornerstone-tools';
import createSegmentDicomSegFile from './createSegmentDicomSegFile';
import dcmjs from 'dcmjs';
import {
  Utils as FlywheelCommonUtils,
  HTTP as FlywheelCommonHTTP,
  Redux as FlywheelCommonRedux,
} from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';
import cloneDeep from 'lodash.clonedeep';

const { getters } = cornerstoneTools.getModule('segmentation');
const saveRequestTracker = {};
const { getActiveFile, getSmartCTRangeList } = FlywheelCommonUtils;
const {
  listSessionAnalyses,
  createAnalyses,
  uploadAnalysesFile,
} = FlywheelCommonHTTP.services;
const { setLoaderDisplayStatus } = FlywheelCommonRedux.actions;

export default function saveSegmentsAsDicomSegFiles(activeViewport) {
  const { labelmaps3D } = getters.labelmaps3D(activeViewport);
  store.dispatch(setLoaderDisplayStatus(true));

  const labelmap3D = labelmaps3D?.[0];
  const toolState = cornerstoneTools.getToolState(activeViewport, 'stack');
  const stackData = toolState.data[0];
  const imageId = stackData.imageIds[0];
  const labelmaps2D = labelmap3D?.labelmaps2D;

  return new Promise(async (resolve, reject) => {
    try {
      if (!labelmap3D || labelmaps2D.length === 0) {
        resolve({
          savedFiles: [],
          failedFiles: [],
          warningInfo: { title: 'Saving failed' },
        });
        return;
      }
      if (saveRequestTracker[imageId]) {
        resolve({
          savedFiles: [],
          failedFiles: [],
          warningInfo: { title: 'Save already in progress' },
        });
        return;
      }
      const activeFile = await getActiveFile(store.getState());
      if (!activeFile) {
        resolve({ savedFiles: [], failedFiles: [] });
        return;
      }

      saveRequestTracker[imageId] = true;

      const { file_id, parents } = activeFile;
      const analysesInputInfo = getAnalysesInputInfo(activeFile);
      const extension = '.dcm';

      const allAnalyses = await listSessionAnalyses(parents.session);
      let maskAnalysis = (allAnalyses || []).find(analysis =>
        (analysis.inputs || []).find(file => file.file_id === file_id)
      );

      if (!maskAnalysis) {
        maskAnalysis = await createAnalyses(parents.session, analysesInputInfo);
      }

      for (let i = 0; i < labelmaps2D.length; i++) {
        if (!labelmaps2D[i]) {
          continue;
        }

        const segmentsOnLabelmap = labelmaps2D[i].segmentsOnLabelmap;

        segmentsOnLabelmap.forEach(segmentIndex => {
          if (segmentIndex !== 0 && !labelmap3D.metadata[segmentIndex]) {
            labelmap3D.metadata[segmentIndex] = generateMockMetadata(
              segmentIndex
            );
          }
        });
      }

      const segmentsMap = getSegmentationDataMap(
        labelmap3D.labelmaps2D,
        `Mask`,
        extension,
        maskAnalysis
      );

      const savePromises = [];
      const savedFiles = [];
      const failedFiles = [];

      const segmentDetails = [];
      Object.keys(segmentsMap).forEach(async key => {
        const segmentDetail = segmentsMap[key];
        segmentDetails.push(segmentDetail);
      });

      for (let j = 0; j < segmentDetails.length; j++) {
        const modifyingLabelmaps3D = cloneDeep([labelmaps3D[0]]);
        const labelMaps2D = modifyingLabelmaps3D[0].labelmaps2D;
        for (let i = 0; i <= stackData.imageIds.length; i++) {
          if (labelMaps2D[i]) {
            if (
              labelMaps2D[i].segmentsOnLabelmap.find(
                segment => segment === segmentDetails[j].segmentIndex
              )
            ) {
              labelMaps2D[i].pixelData.forEach((value, index) => {
                labelMaps2D[i].pixelData[index] =
                  value === segmentDetails[j].segmentIndex
                    ? segmentDetails[j].segmentIndex
                    : 0;
              });
              labelMaps2D[i].segmentsOnLabelmap = labelMaps2D[
                i
              ].segmentsOnLabelmap.filter(
                segment =>
                  segment === 0 || segment === segmentDetails[j].segmentIndex
              );
            } else {
              delete labelMaps2D[i];
            }
            modifyingLabelmaps3D[0].metadata.forEach((data, index) => {
              modifyingLabelmaps3D[0].metadata[index] =
                index === segmentDetails[j].segmentIndex ? data : undefined;
            });
          }
        }

        const file = await createSegmentDicomSegFile(
          activeViewport,
          segmentDetails[j],
          modifyingLabelmaps3D
        );
        const promise = uploadFile(file, file.name, maskAnalysis._id);

        savePromises.push(promise);
        const response = await promise;
        if (response) {
          savedFiles.push(file.name);
        } else {
          failedFiles.push(file.name);
        }
      }

      await Promise.all(savePromises);
      delete saveRequestTracker[imageId];
      store.dispatch(setLoaderDisplayStatus(false));
      resolve({ savedFiles, failedFiles });
    } catch (err) {
      delete saveRequestTracker[imageId];
      reject(err);
    }
    return;
  });
}

// Upload the file to the mask analyses container
const uploadFile = (file, fileName, analysesId) => {
  const formData = new FormData();
  formData.append('input_data', file, fileName);
  return uploadAnalysesFile(analysesId, formData);
};

// Retrieve the segment information from the label map and create a map with each segment detailed information
const getSegmentationDataMap = (labelMaps2D, namePrefix, fileExtension) => {
  const segmentsMap = {};
  const smartCTRanges = getSmartCTRangeList();
  (labelMaps2D || []).forEach((labelMap, index) => {
    (labelMap.segmentsOnLabelmap || []).forEach(segmentIndex => {
      if (segmentIndex > 0) {
        const segmentName =
          smartCTRanges[segmentIndex - 1]?.label || `Tissue${segmentIndex}`;
        let fileName = `${namePrefix}_${segmentName}_${getCurrentDateTimeString()}`;
        segmentsMap[segmentIndex] = segmentsMap[segmentIndex] || {
          segmentIndex,
          start: Number.MAX_VALUE,
          end: -1,
          slices: [],
          name: fileName,
          fileExtension,
        };
      } else {
        return;
      }
      segmentsMap[segmentIndex].start =
        segmentsMap[segmentIndex].start > index
          ? index
          : segmentsMap[segmentIndex].start;
      segmentsMap[segmentIndex].end =
        segmentsMap[segmentIndex].end < index
          ? index
          : segmentsMap[segmentIndex].end;
      if (!segmentsMap[segmentIndex].slices.includes(index)) {
        segmentsMap[segmentIndex].slices.push(index);
      }
    });
  });
  return segmentsMap;
};

// Get the analyses input info with file details for creating analyses
const getAnalysesInputInfo = activeFile => {
  const { name, filename, parents, version } = activeFile;
  const containerId = activeFile.containerId || activeFile.parent_ref.id;
  const containerType = Object.keys(parents).find(
    key => parents[key] === containerId
  );
  const inputFileInfo = {
    type: containerType,
    id: containerId,
    name: name || filename,
    version: version,
  };

  return {
    label: 'Mask Analyses',
    inputs: [inputFileInfo],
    description: '',
    info: {},
  };
};

const generateMockMetadata = segmentIndex => {
  // TODO -> Use colors from the cornerstoneTools LUT.
  const RecommendedDisplayCIELabValue = dcmjs.data.Colors.rgb2DICOMLAB([
    1,
    0,
    0,
  ]);

  return {
    SegmentedPropertyCategoryCodeSequence: {
      CodeValue: 'T-D0050',
      CodingSchemeDesignator: 'SRT',
      CodeMeaning: 'Tissue',
    },
    SegmentNumber: (segmentIndex + 1).toString(),
    SegmentLabel: 'Tissue ' + (segmentIndex + 1).toString(),
    SegmentAlgorithmType: 'SEMIAUTOMATIC',
    SegmentAlgorithmName: 'Slicer Prototype',
    RecommendedDisplayCIELabValue,
    SegmentedPropertyTypeCodeSequence: {
      CodeValue: 'T-D0050',
      CodingSchemeDesignator: 'SRT',
      CodeMeaning: 'Tissue',
    },
  };
};

const getCurrentDateTimeString = () => {
  const format = val => String(val).padStart(2, '0');
  const d = new Date();
  const date = [
    d.getFullYear(),
    format(d.getMonth()),
    format(d.getDate()),
  ].join('');
  const time = [
    format(d.getHours()),
    format(d.getMinutes()),
    format(d.getSeconds()),
  ].join('');
  return `${date}_${time}`;
};
