# @flywheel/extension-cornerstone-meta-image

This extension is to support for parsing the meta-image files like MHD etc and load the corresponding image files(.raw etc).
This is implemented with the similar approach like conerstone-nifti-image-loader which is used to load the nifti files and created the volume data from it.
