import metaDataManager from './metaImage/metaData/metaDataManager.js';
import { metaDataProvider } from './metaImage/metaData/metaDataProvider.js';
import VolumeAcquisitionStreamer from './metaImage/VolumeAcquisitionStreamer.js';
import ImageId from './metaImage/ImageId.js';
import augmentPromise from './metaImage/augmentPromise.js';
import cornerstoneEvents from './metaImage/cornerstoneEvents.js';

const options = {
  headers: {}
};

// Manage simultaneous image load request count
class MaxRequestManager {
  static maxSimultaneousRequestsConfig = 0;

  static updateMaxRequestConfig(maxCount) {
    const config = cornerstoneTools.stackPrefetch.getConfiguration();
    if (config.maxSimultaneousRequests > maxCount) {
      this.maxSimultaneousRequestsConfig = config.maxSimultaneousRequests;
      config.maxSimultaneousRequests = maxCount;
      cornerstoneTools.stackPrefetch.setConfiguration(config);
    }
  }

  static restoreDefaultMaxRequestConfig() {
    const config = cornerstoneTools.stackPrefetch.getConfiguration();
    if (this.maxSimultaneousRequestsConfig > 0) {
      config.maxSimultaneousRequests = this.maxSimultaneousRequestsConfig;
      cornerstoneTools.stackPrefetch.setConfiguration(config);
      this.maxSimultaneousRequestsConfig = 0;
    }
  }
}

const metaImage = {
  totalCachedImages: {},
  loadVolumeTimepoint(imageId) {
    let promise;

    try {
      const imageIdObject = ImageId.fromURL(imageId);
      const streamer = VolumeAcquisitionStreamer.getInstance(options.headers);

      MaxRequestManager.updateMaxRequestConfig(2);
      cornerstoneEvents.imageLoadStart(imageIdObject);

      promise = streamer.acquireTimepoint(imageIdObject).
        then((volume) => volume.slice(imageIdObject)).
        then((slice) => {
          metaDataManager.add(imageIdObject.url, slice.compoundMetaData);
          cornerstoneEvents.imageLoadEnd(imageIdObject);
          if (!this.totalCachedImages[imageIdObject.filePath]) {
            this.totalCachedImages[imageIdObject.filePath] = 0;
          }
          this.totalCachedImages[imageIdObject.filePath]++;
          const totalSlices = slice.compoundMetaData.rows + slice.compoundMetaData.columns +
            slice.compoundMetaData.numberOfFrames;
          if (this.totalCachedImages[imageIdObject.filePath] >= totalSlices) {
            MaxRequestManager.restoreDefaultMaxRequestConfig();
            delete this.totalCachedImages[imageIdObject.filePath];
          }

          return slice.cornerstoneImageObject;
        }).catch(error => {
          MaxRequestManager.restoreDefaultMaxRequestConfig();
          promise = Promise.reject(error);
        });

    } catch (error) {
      MaxRequestManager.restoreDefaultMaxRequestConfig();
      promise = Promise.reject(error);
    }

    // Temporary 'hack' to make the loader work with applications that expect
    // jquery.deferred promises (such as the StudyPrefetcher in OHIF)
    promise = augmentPromise(promise);

    // Temporary 'hack' to make the loader work on both cornerstone@1 and @2
    // @1 expected a promise to be returned directly, whereas @2 expects an
    // object like { promise, cancelFn }
    promise.promise = promise;

    return promise;
  },

  loadMetaHeader(imageId) {
    let promise;

    try {
      const imageIdObject = ImageId.fromURL(imageId);
      const volumeAcquisition = VolumeAcquisitionStreamer.getInstance(options.headers);

      // Load the Meta-Image(mhd only supported now) file
      promise = volumeAcquisition.acquireMetaHeader(imageIdObject).
        then((data) => {

          return data.metaData.header;
        }).catch(error => {
          promise = Promise.reject(error);
          throw error;
        });

    } catch (error) {
      promise = Promise.reject(error);
    }

    // Temporary 'hack' to make the loader work with applications that expect
    // jquery.deferred promises (such as the StudyPrefetcher in OHIF)
    promise = augmentPromise(promise);

    // Temporary 'hack' to make the loader work on both cornerstone@1 and @2
    // @1 expected a promise to be returned directly, whereas @2 expects an
    // object like { promise, cancelFn }
    promise.promise = promise;

    return promise;
  },

  loadHeader(imageId) {
    let promise;

    try {
      const imageIdObject = ImageId.fromURL(imageId);
      const volumeAcquisition = VolumeAcquisitionStreamer.getInstance(options.headers);

      // Create the per slice meta data for the raw file from the loaded Meta-Image header data
      promise = volumeAcquisition.acquireHeaderOnly(imageIdObject).
        then((volume) => volume.slice(imageIdObject)).
        then((data) => {
          metaDataManager.add(imageIdObject.url, data.compoundMetaData || data.metaData);

          return data.metaData;
        }).catch(error => {
          promise = Promise.reject(error);
        });

    } catch (error) {
      promise = Promise.reject(error);
    }

    // Temporary 'hack' to make the loader work with applications that expect
    // jquery.deferred promises (such as the StudyPrefetcher in OHIF)
    promise = augmentPromise(promise);

    // temporary 'hack' to make the loader work on both cornerstone@1 and @2
    // @1 expected a promise to be returned directly, whereas @2 expects an
    // object like { promise, cancelFn }
    promise.promise = promise;

    return promise;
  },

  ImageId,

  register(cornerstone) {
    cornerstone.registerImageLoader('metaimage', (imageId) => this.loadVolumeTimepoint(imageId, this));
    // Consider meta-image provider to have higher priority (usually apps, as OHIF, has providers priority less than 9999)
    cornerstone.metaData.addProvider(metaDataProvider, 10000);
  },

  configure(loaderOptions) {
    Object.assign(options, loaderOptions);
  }
};

export default metaImage;
