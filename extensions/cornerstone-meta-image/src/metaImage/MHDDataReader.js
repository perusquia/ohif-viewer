import cornerstoneMath from 'cornerstone-math';
import decodeMetaImageBigEndian from '../shared/metaImageBigEndianDecoder.js';
import normalizeInvalid from '../shared/normalizeInvalid.js';

const { Vector3 } = cornerstoneMath;

/**
 * @module Read and parse MHD file and the corresponding ElementDataFile file
 */
export default class MHDDataReader {
  constructor(data) {
    this._url = data.url;
    this.header = {};
    this._buffer = null;

    // Parse header (mhd) data
    let lines = new TextDecoder().decode(data.mhdBuffer).split('\n');
    lines.forEach(line => {
      let keyvalue = line.split('=');
      if (keyvalue.length === 2) {
        this.header[keyvalue[0].trim()] = keyvalue[1].trim();
      }
    });
    this.validateHeader(this.header);

    this.header.DimSize = this.header.DimSize.split(' ');
    if (this.header.DimSize.length === 2) {
      this.header.DimSize.push('1');
    }
    this.header.ElementSpacing = this.header.ElementSpacing.split(' ');
    this.header.TransformMatrix = this.header.TransformMatrix
      ? this.header.TransformMatrix.split(' ')
      : ['1', '0', '0', '0', '1', '0', '0', '0', '1'];
    this.header.Offset =
      this.header.Offset !== undefined
        ? this.header.Offset.split(' ')
        : [0, 0, 0];
    this.header.timeSlices = 1;
    this.header.littleEndian =
      this.header.BinaryDataByteOrderMSB === 'TRUE' ? false : true;
    this.header.LayerNames =
      this.header.LayerNames && !Array.isArray(this.header.LayerNames)
        ? this.header.LayerNames.split(' ')
        : this.header.LayerNames;
    this.header.updatedDimSize = null;

    this.initializeDataType();
  }

  initializeDataType() {
    const dataType = {
      TypedArrayConstructor: Uint8Array,
      samplesPerPixel: 1,
      isDataInFloat: false,
      isDataInColors: false,
    };

    if (this.header.ElementType === 'MET_CHAR') {
      dataType.TypedArrayConstructor = Int8Array;
    } else if (this.header.ElementType === 'MET_UCHAR') {
      dataType.TypedArrayConstructor = Uint8Array;
    } else if (this.header.ElementType === 'MET_SHORT') {
      dataType.TypedArrayConstructor = Int16Array;
    } else if (this.header.ElementType === 'MET_USHORT') {
      dataType.TypedArrayConstructor = Uint16Array;
    } else if (this.header.ElementType === 'MET_INT') {
      dataType.TypedArrayConstructor = Int32Array;
    } else if (this.header.ElementType === 'MET_UINT') {
      dataType.TypedArrayConstructor = Uint32Array;
    } else if (this.header.ElementType === 'MET_FLOAT') {
      dataType.isDataInFloat = true;
      dataType.TypedArrayConstructor = Float32Array;
    }

    this.dataType = dataType;
  }

  updateDimensionSizeForLayeredFormatData(dims) {
    this.header.updatedDimSize = dims;
  }

  validateHeader(header) {
    const headerFields = [
      'DimSize',
      'ElementSpacing',
      'ElementDataFile',
      'AnatomicalOrientation',
    ];
    headerFields.forEach(field => {
      if (!header.hasOwnProperty(field)) {
        throw new Error(
          `MHD header file validation failed, ${field} field is missing in header`
        );
      }
    });
  }

  parseRawImage(rawFileData, headerData) {
    const TypedArrayConstructor = this.dataType.TypedArrayConstructor;
    const imageSize = this.getRawDataLength();
    const imageOffset = headerData.length;

    const arraybuffer = rawFileData.slice(imageOffset, imageOffset + imageSize);

    // Reads the image data and puts it in a typed array
    let imageData = normalizeInvalid(
      TypedArrayConstructor,
      new TypedArrayConstructor(arraybuffer)
    );

    if (!this.header.littleEndian) {
      imageData = decodeMetaImageBigEndian(TypedArrayConstructor, imageData);
    }

    return imageData;
  }

  getRawDataLength() {
    const dims = this.header.updatedDimSize || this.header.DimSize;
    const numBitsPerVoxel = this.bitsAllocated();
    const timeDim = 1;
    let statDim = 1;

    if (dims[5]) {
      statDim = dims[5];
    }

    return (
      dims[0] * dims[1] * dims[2] * timeDim * statDim * (numBitsPerVoxel / 8)
    );
  }

  rightHanded() {
    let anatomicalOrientation = this.header.AnatomicalOrientation;
    if (
      anatomicalOrientation === 'RAS' ||
      anatomicalOrientation === 'RPI' ||
      anatomicalOrientation === 'LPS' ||
      anatomicalOrientation === 'LAI'
    ) {
      this._rightHanded = true;
    } else {
      this._rightHanded = false;
    }

    return this._rightHanded;
  }

  seriesInstanceUID() {
    // Use filename + timestamp..?
    return this._url;
  }

  numberOfFrames(dimensionIndex = 2) {
    if (this.header.updatedDimSize) {
      return parseInt(this.header.updatedDimSize[dimensionIndex], 10);
    }
    return parseInt(this.header.DimSize[dimensionIndex], 10);
  }

  sopInstanceUID(frameIndex = 0) {
    return frameIndex;
  }

  rows(frameIndex = 0) {
    if (this.header.updatedDimSize) {
      return parseInt(this.header.updatedDimSize[1], 10);
    }
    return parseInt(this.header.DimSize[1], 10);
  }

  columns(frameIndex = 0) {
    return parseInt(this.header.DimSize[0], 10);
  }

  pixelType(frameIndex = 0) {
    // 0 - int
    // 1 - float
    let type = 0;
    if (
      this.header.ElementType === 'MET_UFLOAT' ||
      this.header.ElementType === 'MET_FLOAT'
    ) {
      type = 1;
    }
    return type;
  }

  bitsAllocated(frameIndex = 0) {
    let bitsAllocated = 1;

    if (
      this.header.ElementType === 'MET_UCHAR' ||
      this.header.ElementType === 'MET_CHAR'
    ) {
      bitsAllocated = 8;
    } else if (
      this.header.ElementType === 'MET_USHORT' ||
      this.header.ElementType === 'MET_SHORT'
    ) {
      bitsAllocated = 16;
    } else if (
      this.header.ElementType === 'MET_UINT' ||
      this.header.ElementType === 'MET_INT' ||
      this.header.ElementType === 'MET_UFLOAT' ||
      this.header.ElementType === 'MET_FLOAT'
    ) {
      bitsAllocated = 32;
    }

    return bitsAllocated;
  }

  /**
   * https://itk.org/Wiki/ITK/MetaIO/Documentation
   * ElementSpacing[0] spacing between elements along X axis (i.e. column spacing)
   * ElementSpacing[1] spacing between elements along Y axis (i.e. row spacing)
   *
   * @param {*} frameIndex
   */
  pixelSpacing(frameIndex = 0) {
    let x = parseFloat(this.header.ElementSpacing[0], 10);
    let y = parseFloat(this.header.ElementSpacing[1], 10);
    let z = parseFloat(this.header.ElementSpacing[2], 10);
    if (this.header.LayerNames?.length) {
      z = parseFloat(this.header.ElementSpacing[1], 10);
      y = parseFloat(this.header.ElementSpacing[2], 10);
    }
    return [x, y, z];
  }

  imageOrientation(sliceDimension = 'z', frameIndex = 0) {
    let invertX = null;
    let invertY = null;
    let invertZ = null;
    let x = null;
    let y = null;

    switch (sliceDimension) {
      case 'x':
        invertZ = this.header.AnatomicalOrientation.match(/S/) ? 1 : -1;
        invertY = this.header.AnatomicalOrientation.match(/P/) ? -1 : 1;

        x = new Vector3(
          parseFloat(this.header.TransformMatrix[3]),
          parseFloat(this.header.TransformMatrix[4]) * invertY,
          parseFloat(this.header.TransformMatrix[5]) * invertZ
        );
        x.normalize();

        y = new Vector3(
          parseFloat(this.header.TransformMatrix[6]),
          parseFloat(this.header.TransformMatrix[7]) * invertY,
          parseFloat(this.header.TransformMatrix[8]) * invertZ
        );
        y.normalize();
        break;

      case 'y':
        invertX = this.header.AnatomicalOrientation.match(/L/) ? -1 : 1;
        invertZ = this.header.AnatomicalOrientation.match(/S/) ? 1 : -1;

        x = new Vector3(
          parseFloat(this.header.TransformMatrix[0]) * invertX,
          parseFloat(this.header.TransformMatrix[1]),
          parseFloat(this.header.TransformMatrix[2]) * invertZ
        );
        x.normalize();

        y = new Vector3(
          parseFloat(this.header.TransformMatrix[6]) * invertX,
          parseFloat(this.header.TransformMatrix[7]),
          parseFloat(this.header.TransformMatrix[8]) * invertZ
        );
        y.normalize();
        break;

      case 'z':
        invertX = this.header.AnatomicalOrientation.match(/L/) ? -1 : 1;
        invertY = this.header.AnatomicalOrientation.match(/P/) ? -1 : 1;

        x = new Vector3(
          parseFloat(this.header.TransformMatrix[0]) * invertX,
          parseFloat(this.header.TransformMatrix[1]) * invertY,
          parseFloat(this.header.TransformMatrix[2])
        );
        x.normalize();

        y = new Vector3(
          parseFloat(this.header.TransformMatrix[3]) * invertX,
          parseFloat(this.header.TransformMatrix[4]) * invertY,
          parseFloat(this.header.TransformMatrix[5])
        );
        y.normalize();
        break;
    }

    return [x.x, x.y, x.z, y.x, y.y, y.z];
  }

  imagePosition(sliceIndex, dimensionIndex = 0) {
    const sliceThickness = this.pixelSpacing(dimensionIndex)[dimensionIndex];
    let slice = sliceIndex;
    if (!this.rightHanded()) {
      slice = this.numberOfFrames(dimensionIndex) - 1 - slice;
    }
    const position = [
      parseFloat(this.header.Offset[0]),
      parseFloat(this.header.Offset[1]),
      parseFloat(this.header.Offset[2]),
    ];
    position[dimensionIndex] += slice * sliceThickness;

    return position;
  }

  extractPixelData(frameIndex = 0) {
    return this._decompressUncompressed(frameIndex);
  }

  _decompressUncompressed(frameIndex = 0) {
    let buffer = this._buffer;
    let numberOfChannels = this.numberOfChannels();
    let numPixels =
      this.rows(frameIndex) * this.columns(frameIndex) * numberOfChannels;
    if (!this.rightHanded()) {
      frameIndex = this.numberOfFrames() - 1 - frameIndex;
    }
    let frameOffset = frameIndex * numPixels;

    if (this.header.ElementType === 'MET_CHAR') {
      return new Int8Array(buffer, frameOffset, numPixels);
    } else if (this.header.ElementType === 'MET_UCHAR') {
      return new Uint8Array(buffer, frameOffset, numPixels);
    } else if (this.header.ElementType === 'MET_SHORT') {
      frameOffset = frameOffset * 2;
      return new Int16Array(buffer, frameOffset, numPixels);
    } else if (this.header.ElementType === 'MET_USHORT') {
      frameOffset = frameOffset * 2;
      return new Uint16Array(buffer, frameOffset, numPixels);
    } else if (this.header.ElementType === 'MET_INT') {
      frameOffset = frameOffset * 4;
      return new Int32Array(buffer, frameOffset, numPixels);
    } else if (this.header.ElementType === 'MET_UINT') {
      frameOffset = frameOffset * 4;
      return new Uint32Array(buffer, frameOffset, numPixels);
    } else if (this.header.ElementType === 'MET_FLOAT') {
      frameOffset = frameOffset * 4;
      return new Float32Array(buffer, frameOffset, numPixels);
    }
  }
}
