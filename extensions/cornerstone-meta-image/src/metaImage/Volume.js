import Slice from './Slice.js';

export default class Volume {
  constructor(
    imageIdObject,
    metaData,
    imageDataNDarray,
    floatImageDataNDarray,
    isSingleTimepoint = false
  ) {
    this.imageIdObject = imageIdObject;
    this.metaData = metaData;
    this.imageDataNDarray = imageDataNDarray;
    this.floatImageDataNDarray = floatImageDataNDarray;
    this.isSingleTimepoint = isSingleTimepoint;
    this.convertRAStoLPSView();
  }

  /**
   * convertRAStoLPS - converts the orientation matrix from standard meta-image(ex: mhd)
   * right handled orientation(RAS) to dicom's LPS so it matches cornerstone expectation
   * of dicom's image orientation (i.e., row cosines, column cosines). That is
   * achieved by doing a 180deg rotation on the z axis, which is equivalent to
   * flipping the signs of the first 2 rows.
   *
   */
  convertRAStoLPSView() {
    // Flipping the first row is equivalent to doing a 180deg rotation on 'z',
    // which achieves going from RAS (mhd right handled orientation) to LPS (dicom's)
    const steps = [1, 1, 1];
    if (!this.metaData.rightHanded()) {
      steps[2] = -1;
    }

    if (this.hasImageData) {
      this.imageDataNDarray = this.imageDataNDarray.step(...steps, 1);
      if (this.floatImageDataNDarray) {
        this.floatImageDataNDarray = this.floatImageDataNDarray.step(
          ...steps,
          1
        );
      }
    }
  }

  slice(imageIdObject) {
    return new Slice(this, imageIdObject, this.isSingleTimepoint);
  }

  get hasImageData() {
    return (
      this.imageDataNDarray &&
      this.imageDataNDarray.data &&
      this.imageDataNDarray.data.byteLength > 0
    );
  }

  get sizeInBytes() {
    const integerArraySize = this.imageDataNDarray
      ? this.imageDataNDarray.data.byteLength
      : 0;
    const floatArraySize = this.floatImageDataView
      ? this.floatImageDataView.data.byteLength
      : 0;

    return integerArraySize + floatArraySize;
  }
}
