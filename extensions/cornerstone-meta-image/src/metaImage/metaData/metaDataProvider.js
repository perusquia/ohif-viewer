import metaDataManager from './metaDataManager.js';
import { decimalToFraction } from './decimalToFraction.js';
import ImageId from '../ImageId.js';

const dependencies = {
  metaDataManager,
  decimalToFraction,
};

export function metaDataProvider(type, imageId) {
  // Fetches injected dependencies
  const metaDataManager = dependencies.metaDataManager;
  const metaData = metaDataManager.get(imageId);

  if (!metaData) {
    return;
  }

  // VTK currently only handle 16 bit allocated images, so the image pixel data should be in 16 bit format.
  // As per the image loader implementation(cornerstone-meta-image), it will by default provide the 16 bit data even the original data is
  // Float pixel array, so updating the metadata numBitsPerVoxel attribute to 16.
  metaData.numBitsPerVoxel = 16;

  const imageIdObject = ImageId.fromURL(imageId);

  switch (type) {
    case 'functional': {
      return {
        frameOfReferenceUID: imageIdObject.filePath,
        timeSlices: metaData.timeSlices
      };
    }
    case 'imagePlane':
    case 'imagePlaneModule': {
      const frameOfReferenceUID = imageIdObject.filePath;

      return {
        frameOfReferenceUID,
        columns: metaData.columns,
        rows: metaData.rows,
        imageOrientationPatient: metaData.imageOrientationPatient,
        columnCosines: metaData.columnCosines,
        rowCosines: metaData.rowCosines,
        imagePositionPatient: metaData.imagePositionPatient,
        // Assuming slices contain no gaps between them (contiguous voxels),
        // as the meta-image file does not hold thickness/gap separately
        sliceThickness: metaData.slicePixelSpacing,
        columnPixelSpacing: metaData.columnPixelSpacing,
        rowPixelSpacing: metaData.rowPixelSpacing
      };
    }

    case 'imagePixel':
    case 'imagePixelModule': {
      return {
        samplesPerPixel: getSamplesPerPixel(metaData),
        photometricInterpretation: getPhotometricInterpretation(metaData),
        rows: metaData.rows,
        columns: metaData.columns,
        bitsAllocated: metaData.numBitsPerVoxel,
        bitsStored: metaData.numBitsPerVoxel,
        highBit: metaData.numBitsPerVoxel - 1,
        pixelRepresentation: getPixelRepresentation(metaData),
        planarConfiguration: getPlanarConfiguration(metaData),
        pixelAspectRatio: getPixelAspectRatio(metaData),
        smallestPixelValue: metaData.minPixelValue,
        largestPixelValue: metaData.maxPixelValue
      };
    }

    case 'modalityLut':
    case 'modalityLutModule':
      return {
        rescaleIntercept: 0,//metaData.intercept,
        rescaleSlope: 1,//metaData.slope,
        rescaleType: 'US',
        modalityLutSequence: undefined
      };

    case 'voiLut':
    case 'voiLutModule':
      return {
        windowCenter: metaData.windowCenter,
        windowWidth: metaData.windowWidth,
        voiLutSequence: undefined
      };

    case 'multiFrame':
    case 'multiFrameModule':
      return {
        numberOfFrames: metaData.numberOfFrames,
        frameIncrementPointer: undefined,
        stereoPairsPresent: 'NO'
      };

    default:
      return;
  }
}

function getSamplesPerPixel(metaData) {
  // The fourth dimension (metaData.header.dims[4]), if present assume to represents the
  // samples per voxel
  // TODO: Need to revisit if a data with samples per pixel is available
  if (metaData.header) {
    const hasFifthDimensionSpecified = metaData.header.NDims >= 4;
    const hasSamplesPerVoxelSpecified = hasFifthDimensionSpecified && (metaData.header.DimSize[3] > 1);

    return hasSamplesPerVoxelSpecified ? metaData.header.DimSize[3] : 1;
  }
  return 1;
}

function getPhotometricInterpretation(metaData) {
  const samplesPerPixel = getSamplesPerPixel(metaData);
  if ((samplesPerPixel === 3) || (samplesPerPixel === 4)) {
    return 'RGB';
  }

  // Or 'MONOCHROME2' otherwise, as its the most typical photometric interpretation
  return 'MONOCHROME2';
}

function getPixelRepresentation(metaData) {
  const dataTypeCode = metaData.header.ElementType;

  switch (dataTypeCode) {
    case 'MET_UCHAR':
    case 'MET_USHORT':
    case 'MET_UINT':
      //'0000H' means unsigned integer, by DICOM pixel representation value
      return '0000H';
    case 'MET_CHAR':
    case 'MET_SHORT':
    case 'MET_INT':
      // '0001H' means signed integer, 2-complement
      return '0001H';
    case 'MET_FLOAT':
      // As images using float or rgb(a) values are converted to Uint16, we
      // return the pixel representation as unsigned integer
      return '0000H';
  }
}

function getPlanarConfiguration(metaData) {
  // The planar configuration only applies if image has samplesPerPixel > 1
  // it determines how the samples are organized
  const samplesPerPixel = getSamplesPerPixel(metaData);

  return samplesPerPixel > 1 ? 0 : undefined;
}

function getPixelAspectRatio(metaData) {
  // TODO - Need to correct if aspect ratio applicable for meta-image file
  return 1;
}
