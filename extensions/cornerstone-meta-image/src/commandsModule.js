
const commandsModule = ({ servicesManager, commandsManager, appConfig }) => {
  const { store } = appConfig;
  const actions = {
  };

  const definitions = {
  };

  return {
    actions,
    definitions,
    defaultContext: 'ACTIVE_VIEWPORT::CORNERSTONE::METAIMAGE',
  };
};

export default commandsModule;
