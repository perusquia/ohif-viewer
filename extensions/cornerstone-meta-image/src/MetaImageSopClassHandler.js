import { MODULE_TYPES, utils } from '@ohif/core';
import { Utils } from '@flywheel/extension-flywheel-common';
import metaImage from './metaImageLoader';
import getSourceDisplaySet from './getSourceDisplaySet';

const { segmentationUtils, parseSliceIndex } = Utils;
export const SOP_CLASS_UIDS = {
  MR: '1.2.840.10008.5.1.4.1.1.4',
  ENHANCED_MR: '1.2.840.10008.5.1.4.1.1.4.1',
  MR_SPECTROSCOPY: '1.2.840.10008.5.1.4.1.1.4.2',
  SEG: '1.2.840.10008.5.1.4.1.1.66.4',
};

const MetaImageSopClassHandler = {
  id: 'MetaImageSopClassHandlerPlugin',
  type: MODULE_TYPES.SOP_CLASS_HANDLER,
  sopClassUIDs: [
    SOP_CLASS_UIDS.MR,
    SOP_CLASS_UIDS.ENHANCED_MR,
    SOP_CLASS_UIDS.MR_SPECTROSCOPY,
    SOP_CLASS_UIDS.SEG,
  ],
  dataProtocol: 'metaimage',
  getDisplaySetFromSeries(
    series,
    study,
    webClient,
    authorizationHeaders,
    displaySetDefaultBuilder
  ) {
    const defaultDisplaySet = displaySetDefaultBuilder(
      series,
      series._instances
    );

    const middleFrameIndex = Math.floor(
      (series.getInstanceCount() - 1 || 0) / 2
    );
    const seriesData = series.getData();
    const seriesMatches = [
      seriesData.SeriesInstanceUID,
      seriesData.SeriesDescription,
    ];
    const launchFrameIndex = parseSliceIndex(
      seriesMatches,
      middleFrameIndex,
      seriesData.instances.length
    );
    const instance = series.getInstanceByIndex(launchFrameIndex);

    const metadata = instance.getData().metadata;
    const { temporalSeries = false } = series._data || {};
    const { timeSlices = false } = study._data || {};
    const SOPInstanceUID =
      typeof instance.SOPInstanceUID === 'function'
        ? instance.SOPInstanceUID()
        : instance.SOPInstanceUID;

    const imageId = instance.getImageId();
    const isSeg = metadata.SOPClassUID === SOP_CLASS_UIDS.SEG;
    defaultDisplaySet.setAttributes({
      plugin: 'cornerstone::metaimage',
      dataProtocol: MetaImageSopClassHandler.dataProtocol,
      Modality: isSeg ? 'SEG' : 'MR',
      displaySetInstanceUID: utils.guid(),
      imageId,
      isDerived: isSeg,
      referencedDisplaySetUID: null, // Assigned when loaded.
      labelmapIndexes: [], // Assigned when loaded.
      isLoaded: false,
      frameIndex: launchFrameIndex,
      metadata,
      SOPInstanceUID: SOPInstanceUID,
      SeriesInstanceUID: series.getSeriesInstanceUID(),
      StudyInstanceUID: study.getStudyInstanceUID(),
      authorizationHeaders: authorizationHeaders,
      temporalSet: temporalSeries,
      timeSlices: timeSlices,
      sopClassUIDs: MetaImageSopClassHandler.sopClassUIDs,
    });

    if (isSeg) {
      defaultDisplaySet.getSourceDisplaySet = function(
        studies,
        skipUpdate = false
      ) {
        return getSourceDisplaySet(studies, defaultDisplaySet, skipUpdate);
      };
      defaultDisplaySet.load = function(
        referencedDisplaySet,
        studies,
        segmentationIndex
      ) {
        return segmentationUtils.loadSegmentation(
          defaultDisplaySet,
          referencedDisplaySet,
          studies,
          metaImage.loadVolumeTimepoint.bind(metaImage),
          segmentationIndex
        );
      };
      defaultDisplaySet.setSegmentVisibility = function(
        isVisible,
        referencedDisplaySet,
        itemIndex = 0
      ) {
        return segmentationUtils.setSegmentVisibility(
          defaultDisplaySet.labelmapIndexes[itemIndex],
          defaultDisplaySet.segmentationIndexes[itemIndex],
          isVisible
        );
      };
      defaultDisplaySet.removeSegment = function(
        referencedDisplaySet,
        studies,
        currentSegIndex,
        newSegIndex = 0,
        newLabelmapIndex = -1
      ) {
        return segmentationUtils.removeSegment(
          defaultDisplaySet,
          referencedDisplaySet,
          studies,
          currentSegIndex,
          newSegIndex,
          newLabelmapIndex
        );
      };
      defaultDisplaySet.rePositionSegment = function(
        referencedDisplaySet,
        studies,
        newLabelmapIndex
      ) {
        return segmentationUtils.rePositionSegment(
          defaultDisplaySet,
          referencedDisplaySet,
          studies,
          newLabelmapIndex
        );
      };
    }

    return defaultDisplaySet;
  },
};

export default MetaImageSopClassHandler;
