import cornerstone from 'cornerstone-core';
import metaImage from '../metaImageLoader';

export default function initImageLoader({ appConfig }) {
  metaImage.register(cornerstone);
}
