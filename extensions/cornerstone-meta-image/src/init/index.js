import initImageLoader from './initImageLoader';
import metaImage from '../metaImageLoader';
import {
  HTTP as FlywheelCommonHTTP,
} from '@flywheel/extension-flywheel-common';

const { getAuthToken } = FlywheelCommonHTTP.utils.AUTH_UTILS;

export default function initExtension({ appConfig }) {
  initImageLoader(store);
  setMetaImageLoaderConfig();
}


function setMetaImageLoaderConfig() {
  metaImage.configure({
    headers: {
      Authorization: getAuthToken(),
    },
  });
}
