const infusions = [];

export default function toolbarModule({ appConfig, commandsManager }) {
  const { store } = appConfig;
  const toolbarModule = infusions.reduce(
    (accumulator, getDefinition) => {
      const infusionDefinitions = getDefinition(store, commandsManager) || [];
      return {
        definitions: [...accumulator.definitions, ...infusionDefinitions],
        defaultContext: 'ACTIVE_VIEWPORT::CORNERSTONE',
        defaultPrecedence: 99,
      };
    },
    { definitions: [] }
  );

  return toolbarModule;
}
