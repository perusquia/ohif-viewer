import initReactive from './initReactive';
import { Reactive as FlywheelCommonReactive } from '@flywheel/extension-flywheel-common';

export default function initExtension() {
  initReactive(FlywheelCommonReactive.store.observables.storeObservable$);
}
