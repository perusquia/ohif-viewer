import Reactive from '../reactive';
// import { CrosshairsTool, LinkSeriesTool } from '../tools';

/**
 * It initializes some reactive components. On extension initialization
 * @param {object} storeObservable$ store observable
 */
function initReactive(storeObservable$)
{
  const { viewportElementsObservable$ } = Reactive.viewport.createObservables(
    storeObservable$
  );

  // Reactive.crosshairs.subscribe(CrosshairsTool, viewportElementsObservable$);

  // Reactive.linkSeries.subscribe(
  //   LinkSeriesTool.tools,
  //   viewportElementsObservable$
  // );
}

export default initReactive;
