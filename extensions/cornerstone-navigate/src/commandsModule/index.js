const infusions = [];

export default function commandsModule({
  commandsManager,
  servicesManager,
  hotkeysManager,
  appConfig,
}) {
  const { store } = appConfig;
  const commandsModule = infusions.reduce(
    (accumulator, getCommandsModule) => {
      const infusionCommandsModule =
        getCommandsModule({
          commandsManager,
          servicesManager,
          hotkeysManager,
          appConfig,
        }) || [];

      return {
        definitions: {
          ...accumulator.definitions,
          ...infusionCommandsModule.definitions,
        },
        actions: {
          ...accumulator.actions,
          ...infusionCommandsModule.actions,
        },
      };
    },
    { definitions: {} }
  );

  return commandsModule;
}
