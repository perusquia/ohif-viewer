/**
 * Select SmartCTRanges object from redux state store
 * @param {Object} App redux state
 * @return {Object | undefined} SmartCTRanges object or undefined in case not having ranges
 */
export function selectAvailableSmartCTRanges(state = {}) {
  return state?.flywheel?.projectConfig?.smartCTRanges;
}
/**
 * Select AvailableSmartCTRanges array from redux state store
 * @param {Object} App redux state
 * @return {Array | undefined} AvailableSmartCTRanges array or undefined in case not having ranges
 */
export function getAvailableSmartCTRanges(state = {}) {
  return state?.smartCT?.smartCTRanges;
}
