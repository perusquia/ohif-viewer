/**
 * Select currentNifti object from redux state store
 * @param {Object} App redux state
 * @return {Object | undefined} currentNifti object or undefined in case not nifti
 */
export function selectCurrentNifti(state = {}, index = 0) {
  return state?.flywheel?.currentNiftis?.[index];
}

export function selectCurrentNiftis(state = {}) {
  return state?.flywheel?.currentNiftis;
}

/**
 * Select currentMetaImage object from redux state store
 * @param {Object} App redux state
 * @return {Object | undefined} currentMetaImage object or undefined in case not mhd or etc
 */
export function selectCurrentMetaImage(state = {}) {
  return state.flywheel?.currentMetaImage;
}

/**
 * Select currentNifti/currentMetaImage object from redux state store
 * @param {Object} App redux state
 * @return {Object | undefined} currentNifti/currentMetaImage object or undefined in case not nifti and mhd like meta-image
 */
export function selectCurrentVolumetricImage(state = {}, index = 0) {
  return (
    state.flywheel?.currentNiftis[index] || state.flywheel?.currentMetaImage
  );
}

/**
 * Select currentWebImage object from redux state store
 * @param {Object} App redux state
 * @param {Number} index index of current webImage
 * @return {Object | undefined} currentWebImage Object or undefined in case not webImage
 */
export function selectCurrentWebImage(state = {}, index = 0) {
  return state.flywheel && state.flywheel.currentWebImage[index];
}

/**
 * Select all currentWebImage array from redux state store
 * @param {Object} App redux state
 * @return {Array} currentWebImage Array or empty array in case not webImage
 */
export function selectCurrentWebImages(state = {}) {
  return state.flywheel && state.flywheel.currentWebImage;
}
