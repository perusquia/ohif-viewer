import { SET_LOADER_DISPLAY_STATUS } from '../constants/ActionTypes';

export function setLoaderDisplayStatus(status) {
  return { type: SET_LOADER_DISPLAY_STATUS, status };
}
