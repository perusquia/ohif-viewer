import {
  SAVE_ACTIONS_STATES,
  UNDO_LAST_ACTION_STATE,
  REDO_NEXT_ACTION_STATE,
  CLEAR_ALL_ACTION_STATES,
} from '../constants/ActionTypes';

export function saveActionsStates(actionsStates) {
  return {
    type: SAVE_ACTIONS_STATES,
    actionsStates,
  };
}

export function undoLastActionState() {
  return {
    type: UNDO_LAST_ACTION_STATE,
  };
}

export function redoNextActionState() {
  return {
    type: REDO_NEXT_ACTION_STATE,
  };
}

export function clearAllActionStates() {
  return {
    type: CLEAR_ALL_ACTION_STATES,
  };
}
