import {
  ADD_MULTI_SESSION_DATA,
  CLEAR_MULTI_SESSION_DATA,
} from '../constants/ActionTypes';

export function addMultiSessionData(
  projectId,
  subjectId,
  sessionId,
  acquisitionId,
  studyUid,
  seriesUid,
  containerId,
  index
) {
  return {
    type: ADD_MULTI_SESSION_DATA,
    projectId,
    subjectId,
    sessionId,
    acquisitionId,
    studyUid,
    seriesUid,
    containerId,
    index,
  };
}

export function clearMultiSessionData() {
  return {
    type: CLEAR_MULTI_SESSION_DATA,
  };
}
