import {
  SET_HOVERED_MEASUREMENTS,
  SET_MEASUREMENT_COLOR_INTENSITY,
} from '../constants/ActionTypes';

export const setMeasurementColorIntensity = status => {
  return {
    type: SET_MEASUREMENT_COLOR_INTENSITY,
    status,
  };
};

export const setHoveredMeasurement = measurements => {
  return {
    type: SET_HOVERED_MEASUREMENTS,
    measurements,
  };
};
