import { EXPAND_RIGHT_HAND_PANEL } from '../constants/ActionTypes';

export function changeRightHandPanelMode(panelMode) {
  return {
    type: EXPAND_RIGHT_HAND_PANEL,
    panelMode,
  };
}
