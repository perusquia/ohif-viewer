export const setSmartCTRange = smartCTRange => {
  return {
    type: 'SET_SELECTED_RANGE',
    smartCTRange,
  };
};

export const setAvailableSmartRanges = smartCTRanges => {
  return {
    type: 'SET_AVAILABLE_RANGES',
    smartCTRanges,
  };
};

export const addAvailableSmartRange = smartCTRange => {
  return {
    type: 'ADD_AVAILABLE_RANGE',
    smartCTRange,
  };
};
