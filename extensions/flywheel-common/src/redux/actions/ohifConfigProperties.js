import { SET_STUDY_FORM_AVAILABILITY } from '../constants/ActionTypes';

export const setStudyFormAvailableStatus = status => {
  return {
    type: SET_STUDY_FORM_AVAILABILITY,
    status,
  };
};
