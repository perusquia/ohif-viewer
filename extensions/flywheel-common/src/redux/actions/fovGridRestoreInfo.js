import { SET_FOV_GRID_RESTORE_INFO } from '../constants/ActionTypes';

export const setFovGridRestoreInfo = restoreInfo => ({
  type: SET_FOV_GRID_RESTORE_INFO,
  restoreInfo,
});
