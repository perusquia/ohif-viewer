import { SET_PREVIOUS_SESSION_READ_TIME } from '../constants/ActionTypes';

export const setPreviousSessionReadTime = readTime => ({
  type: SET_PREVIOUS_SESSION_READ_TIME,
  readTime,
});
