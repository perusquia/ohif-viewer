import { SET_STUDYFORM_BSLICE_VALIDATED_INFO } from '../constants/ActionTypes';

export const studyFormBSliceValidatedInfo = data => {
  return {
    type: SET_STUDYFORM_BSLICE_VALIDATED_INFO,
    data,
  };
};
