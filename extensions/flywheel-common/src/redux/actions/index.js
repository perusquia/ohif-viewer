import * as labels from './labels';
import * as tools from './tools';
import * as segmentationFileChooser from './segmentationFileChooser';
import * as colorPalette from './colorPalette';
import * as smartCT from './smartCT';
import * as multiSessionData from './multiSessionData';
import * as loader from './loader';
import * as contourProperties from './contourProperties';
import * as layout from './layout';
import * as viewportSyncCommands from './viewportSyncCommands';
import * as undoRedoActionState from './undoRedoActionState';
import * as measurementPanel from './measurementPanel';
import * as measurementColorIntensity from './measurementColorIntensity';
import * as ohifConfigProperties from './ohifConfigProperties';
import * as timer from './timer';
import * as subFormROI from './subFormROI';
import * as scissorsActionState from './scissorsActionState';
import * as studyFormBSliceValidatedInfo from './studyFormBSliceValidatedInfo';
import * as fovGridRestoreInfo from './fovGridRestoreInfo';
import * as rightHandPanel from './rightHandPanel';
import * as viewerAvatar from './viewerAvatar';
import * as indicators from './indicators';

const actions = {
  ...labels,
  ...tools,
  ...segmentationFileChooser,
  ...colorPalette,
  ...multiSessionData,
  ...loader,
  ...contourProperties,
  ...layout,
  ...viewportSyncCommands,
  ...undoRedoActionState,
  ...measurementPanel,
  ...measurementColorIntensity,
  ...ohifConfigProperties,
  ...timer,
  ...subFormROI,
  ...smartCT,
  ...scissorsActionState,
  ...studyFormBSliceValidatedInfo,
  ...fovGridRestoreInfo,
  ...rightHandPanel,
  ...viewerAvatar,
  ...indicators,
};
export default actions;
