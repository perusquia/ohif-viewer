export const setViewportNotifyCommand = (plugin, commandName) => {
  return {
    type: 'SET_VIEWPORT_NOTIFY_COMMAND',
    plugin,
    commandName,
  };
};
export const removeViewportNotifyCommand = plugin => {
  return {
    type: 'REMOVE_VIEWPORT_NOTIFY_COMMAND',
    plugin,
  };
};
