import {
  UPDATE_ROI_COLORS,
  SET_NEXT_PALETTE_COLOR,
  UPDATE_PALETTE_COLORS,
  SET_PALETTE_COLORS_FROM_CONFIG,
} from '../constants/ActionTypes';

export const updateRoiColors = roiColor => {
  return {
    type: UPDATE_ROI_COLORS,
    roiColor,
  };
};

export const setNextPalette = () => {
  return {
    type: SET_NEXT_PALETTE_COLOR,
  };
};

export const updatePaletteColors = measurements => {
  return {
    type: UPDATE_PALETTE_COLORS,
    measurements,
  };
};

export const setPaletteColorsFromConfig = colors => {
  return {
    type: SET_PALETTE_COLORS_FROM_CONFIG,
    colors,
  };
};
