import {
  SET_VIEWER_AVATAR,
  SET_SINGLE_AVATAR,
  SET_STUDY_STATE_NOTE,
  SET_MULTIPLE_TASK_NOTE,
  SET_MULTIPLE_TASK_USER,
  SET_MULTIPLE_TASK_NOTES_USER_FORM_RESPONSE,
  SET_MULTIPLE_TASK_FORM_RESPONSE,
} from '../constants/ActionTypes';

export const setViewerAvatar = data => {
  return {
    type: SET_VIEWER_AVATAR,
    data,
  };
};

export const setSingleAvatar = data => {
  return {
    type: SET_SINGLE_AVATAR,
    data,
  };
};

export const setStudyStateNote = status => {
  return {
    type: SET_STUDY_STATE_NOTE,
    status,
  };
};

export const setMultipleTaskNotes = data => {
  return {
    type: SET_MULTIPLE_TASK_NOTE,
    data,
  };
};

export const setMultipleTaskUser = data => {
  return {
    type: SET_MULTIPLE_TASK_USER,
    data,
  };
};

export const setMultipleTaskFormResponse = data => {
  return {
    type: SET_MULTIPLE_TASK_FORM_RESPONSE,
    data,
  };
};

export const setMultipleTaskNotesUserAndFormResponse = data => {
  return {
    type: SET_MULTIPLE_TASK_NOTES_USER_FORM_RESPONSE,
    data,
  };
};
