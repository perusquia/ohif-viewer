import { SET_STUDYFORM_BSLICE_VALIDATED_INFO } from '../constants/ActionTypes';

const defaultState = {
  bSliceValidation: {},
};

export default function studyFormBSliceValidatedInfo(
  state = defaultState,
  action
) {
  switch (action.type) {
    case SET_STUDYFORM_BSLICE_VALIDATED_INFO: {
      return { ...state, bSliceValidation: action.data };
    }
    default:
      return state;
  }
}
