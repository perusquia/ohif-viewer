import {
  EXPAND_RIGHT_HAND_PANEL,
  RightHandPanelModes,
} from '../constants/ActionTypes';

const initialState = {
  panelMode: RightHandPanelModes.NORMAL,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case EXPAND_RIGHT_HAND_PANEL:
      if (state.panelMode !== action.panelMode) {
        return {
          ...state,
          panelMode: action.panelMode,
        };
      }
      return state;
    default:
      return state;
  }
}
