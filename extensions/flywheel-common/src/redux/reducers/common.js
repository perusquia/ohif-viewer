import omit from 'lodash/omit';
import { redux as OhifRedux } from '@ohif/core';
import {
  SET_ACTIVE_PROJECT,
  SET_ACTIVE_TOOL,
  SET_AVAILABLE_LABELS,
  SET_AVAILABLE_TOOLS,
  SET_PROTOCOL_LAYOUT,
  SET_SCALE_INDICATOR_STATUS,
  SET_FUNDUS_RING,
  SET_IS_RESTORE_NEEDED,
  SET_LOADER_DISPLAY_STATUS,
  SET_ACTIVE_VTK_TOOL,
  SET_ACTIVE_QUESTION,
  REMOVE_AVAILABLE_TOOLS,
  SET_WORKFLOW_TOOLS,
  SET_PANEL_SWITCHED,
  SET_TOOL_WHILE_PANEL_SWITCH,
  SET_RELABEL,
  SET_LIMIT_REACHED_DIALOG,
} from '../constants/ActionTypes';
import { setActiveTool } from '../actions/tools';

const { SET_VIEWPORT_LAYOUT } = OhifRedux.constants;

const defaultState = {
  activeTool: 'Wwwc',
  activeVtkTool: 'WWWC',
  availableLabels: null,
  availableTools: null,
  selectedLabels: [],
  protocolLayout: null,
  enableLoader: false,
  currentSelectedQuestion: null,
  isScaleDisplayed: false,
  fundusRingOverlay: {},
  workFlowTools: null,
  isPanelSwitched: false,
  toolWhilePanelSwitch: false,
  confirmationDialog: false,
  subFormDialog: false,
  confirmationDialogData: {},
  subFormDialogData: {},
  isRelabel: false,
  limitReachedDialog: null,
};
const initialState = loadState() || defaultState;

function reducer(state = initialState, action) {
  switch (action.type) {
    case SET_ACTIVE_TOOL:
      return { ...state, activeTool: action.tool };
    case SET_ACTIVE_VTK_TOOL:
      return { ...state, activeVtkTool: action.tool };
    case SET_AVAILABLE_LABELS:
      let selectedLabels = [...state.selectedLabels];
      action.labels &&
        action.labels.forEach(x => {
          if (!selectedLabels.includes(x)) {
            selectedLabels.push(x);
          }
        });
      setTimeout(setActiveTool); // Ensure activeTool is available.
      return {
        ...state,
        availableLabels: action.labels,
        availableTools: action.tools || null,
        selectedLabels: [...selectedLabels],
      };
    case SET_AVAILABLE_TOOLS:
      return { ...state, availableTools: action.tools };
    case SET_ACTIVE_QUESTION:
      return { ...state, currentSelectedQuestion: action.question };
    case SET_PROTOCOL_LAYOUT:
      return { ...state, protocolLayout: action.layout };
    case SET_VIEWPORT_LAYOUT:
      return state.protocolLayout ? { ...state, protocolLayout: null } : state;
    case SET_ACTIVE_PROJECT:
      return { ...state, availableLabels: null, availableTools: null };
    case SET_LOADER_DISPLAY_STATUS:
      return {
        ...state,
        enableLoader: action.status,
      };
    case SET_SCALE_INDICATOR_STATUS:
      return {
        ...state,
        isScaleDisplayed: action.isDisplayed,
      };
    case SET_FUNDUS_RING:
      return {
        ...state,
        fundusRingOverlay: action.ringOverlay,
      };
    case SET_IS_RESTORE_NEEDED:
      return {
        ...state,
        fundusRingOverlay: {
          ...state.fundusRingOverlay,
          isRestoreNeeded: action.isRestoreNeeded,
        },
      };
    case REMOVE_AVAILABLE_TOOLS:
      return {
        ...state,
        availableTools: action.tools,
      };
    case SET_WORKFLOW_TOOLS:
      return {
        ...state,
        workFlowTools: action.tools,
      };
    case SET_PANEL_SWITCHED:
      return {
        ...state,
        isPanelSwitched: action.data.isPanelSwitched,
        confirmationDialog: action.data.confirmationDialog,
        subFormDialog: action.data.subFormDialog,
        confirmationDialogData: action.data.confirmationDialogData,
        subFormDialogData: action.data.subFormDialogData,
        toolWhilePanelSwitch: true,
      };
    case SET_LIMIT_REACHED_DIALOG:
      return {
        ...state,
        limitReachedDialog: action.data,
      };
    case SET_TOOL_WHILE_PANEL_SWITCH:
      return {
        ...state,
        toolWhilePanelSwitch: action.toolWhilePanelSwitch,
      };
    case SET_RELABEL:
      return {
        ...state,
        isRelabel: action.status,
      };
    default:
      return state;
  }
}

export default function persistingReducer(oldState, action) {
  const newState = reducer(oldState, action);
  if (newState !== oldState) {
    // Stash state in localStorage
    saveState(newState);
  }
  return newState;
}

function loadState() {
  try {
    const serializedState = self.localStorage.getItem('state.infusions');
    if (!serializedState) {
      return undefined;
    }
    return { ...defaultState, ...JSON.parse(serializedState) };
  } catch (e) {
    return undefined;
  }
}

function saveState(state) {
  try {
    const serializedState = JSON.stringify(
      omit(
        state,
        'availableLabels',
        'availableTools',
        'selectedLabels',
        'activeVtkTool',
        'currentSelectedQuestion',
        'enableLoader',
        'isScaleDisplayed',
        'fundusRingOverlay'
      )
    );
    self.localStorage.setItem('state.infusions', serializedState);
  } catch (e) {}
}
