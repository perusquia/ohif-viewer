import { SET_PREVIOUS_SESSION_READ_TIME } from '../constants/ActionTypes';

const defaultState = {
  sessionReadTime: 0,
  adjustedReadStartTime: Date.now(),
};

export default function timer(state = defaultState, action) {
  switch (action.type) {
    case SET_PREVIOUS_SESSION_READ_TIME: {
      const adjustedReadStartTime = Date.now() - (action.readTime || 0) * 1000;
      return {
        ...state,
        sessionReadTime: action.readTime || 0,
        adjustedReadStartTime,
      };
    }
    default:
      return state;
  }
}
