import cloneDeep from 'lodash.clonedeep';

import {
  ADD_MULTI_SESSION_DATA,
  CLEAR_MULTI_SESSION_DATA,
} from '../constants/ActionTypes';

const defaultState = {
  isMultiSessionViewEnabled: false,
  acquisitionData: [],
};

export default function multiSessionData(state = defaultState, action) {
  switch (action.type) {
    case ADD_MULTI_SESSION_DATA: {
      const acquisitionData = cloneDeep(state.acquisitionData);
      let acquisition = {};
      acquisition.projectId = action.projectId;
      acquisition.subjectId = action.subjectId;
      acquisition.sessionId = action.sessionId;
      acquisition.acquisitionId = action.acquisitionId;
      acquisition.studyUid = action.studyUid;
      acquisition.seriesUid = action.seriesUid;
      acquisition.containerId = action.containerId || '';
      if (action.index === 0) {
        acquisitionData.splice(0, acquisitionData.length);
      }
      if (action.index >= acquisitionData.length) {
        acquisitionData.push(acquisition);
      } else {
        acquisitionData[action.index] = acquisition;
      }
      const isMultiSessionViewEnabled =
        acquisitionData.length > 1 ? true : false;
      return Object.assign({}, state, {
        acquisitionData: acquisitionData,
        isMultiSessionViewEnabled: isMultiSessionViewEnabled,
      });
    }
    case CLEAR_MULTI_SESSION_DATA: {
      return { ...defaultState };
    }
    default:
      return state;
  }
}
