import {
  SET_EDIT_SUBFORM_ROI_LAYOUT,
  SET_SUBFORM_POPUP_VISIBILITY,
  SET_LABEL_FOR_QUESTION,
  SET_SUBFORM_QUESTION_ANSWERED,
  SET_STUDYFORM_ANSWER_IN_MIXED_MODE,
  SET_SUBFORM_POPUP_CLOSE,
} from '../constants/ActionTypes';

const defaultState = {
  isROIEdit: false,
  measurement: {},
  isSubFormPopupEnabled: false,
  isLabelApplied: false,
  isSubFormQuestion: false,
  studyFormLastAnsweredQuestion: {},
  isSubFormPopupCloseFromOverlappingDelete: false,
  isSubFormPopupCloseFromOverlappingAutoAdjust: false,
};

export default function setSubFormRoiState(state = defaultState, action) {
  switch (action.type) {
    case SET_EDIT_SUBFORM_ROI_LAYOUT: {
      return {
        ...state,
        isROIEdit: action.data.status,
        measurement: action.data.measurementData,
      };
    }
    case SET_SUBFORM_POPUP_VISIBILITY: {
      return {
        ...state,
        isSubFormPopupEnabled: action.data,
      };
    }
    case SET_LABEL_FOR_QUESTION: {
      return {
        ...state,
        isLabelApplied: action.data,
      };
    }
    case SET_SUBFORM_QUESTION_ANSWERED: {
      return {
        ...state,
        isSubFormQuestion: action.data,
      };
    }
    case SET_STUDYFORM_ANSWER_IN_MIXED_MODE: {
      return {
        ...state,
        studyFormLastAnsweredQuestion: action.data,
      };
    }
    case SET_SUBFORM_POPUP_CLOSE: {
      return {
        ...state,
        isSubFormPopupCloseFromOverlappingDelete: action.data.delete,
        isSubFormPopupCloseFromOverlappingAutoAdjust: action.data.autoAdjust,
      };
    }
    default:
      return state;
  }
}
