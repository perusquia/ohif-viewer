import timer from './timer';
import common from './common';
import segmentation from './segmentation';
import colorPalette from './colorPalette';
import multiSessionData from './multiSessionData';
import contourProperties from './contourProperties';
import viewportSyncCommands from './viewportSyncCommands';
import undoRedoActionState from './undoRedoActionState';
import ohifConfigProperties from './ohifConfigProperties';
import measurementColorIntensity from './measurementColorIntensity';
import measurementPanelProperties from './measurementPanelProperties';
import setSubFormRoiState from './subFormROI';
import smartCT from './smartCT';
import scissorsActionState from './scissorsActionState';
import studyFormBSliceValidatedInfo from './studyFormBSliceValidatedInfo';
import fovGridRestoreInfo from './fovGridRestoreInfo';
import rightHandPanel from './rightHandPanel';
import viewerAvatar from './viewerAvatar';

const reducers = {
  common,
  segmentation,
  colorPalette,
  multiSessionData,
  contourProperties,
  viewportSyncCommands,
  undoRedoActionState,
  measurementColorIntensity,
  timer,
  ohifConfigProperties,
  measurementPanelProperties,
  setSubFormRoiState,
  smartCT,
  scissorsActionState,
  studyFormBSliceValidatedInfo,
  fovGridRestoreInfo,
  rightHandPanel,
  viewerAvatar,
};

export default reducers;
