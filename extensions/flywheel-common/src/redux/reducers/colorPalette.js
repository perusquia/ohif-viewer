import _, { cloneDeep } from 'lodash';
import {
  UPDATE_ROI_COLORS,
  SET_NEXT_PALETTE_COLOR,
  UPDATE_PALETTE_COLORS,
  SET_PALETTE_COLORS_FROM_CONFIG,
} from '../constants/ActionTypes';

const colouredAnnotationTypes = [
  'RectangleRoi',
  'EllipticalRoi',
  'FreehandRoi',
  'Angle',
  'CircleRoi',
  'Bidirectional',
  'Length',
  'OpenFreehandRoi',
  'ArrowAnnotate',
  'ContourRoi',
];

const defaultState = {
  roiColor: {
    current: 'rgba(255, 255, 0, 0.2)',
    next: 'rgba(244, 67, 54, 0.2)',
  },
  palette: [
    'rgba(244, 67, 54, 0.2)',
    'rgba(63, 81, 181, 0.2)',
    'rgba(76, 175, 80, 0.2)',
    'rgba(205, 220, 57, 0.2)',
    'rgba(255, 87, 34, 0.2)',
    'rgba(33, 150, 243, 0.2)',
    'rgba(139, 195, 74, 0.2)',
    'rgba(18, 125, 139, 0.2)',
    'rgba(255, 152, 0, 0.2)',
    'rgba(156, 39, 176, 0.2)',
    'rgba(3, 169, 244, 0.2)',
    'rgba(255, 235, 59, 0.2)',
    'rgba(121, 85, 72, 0.2)',
    'rgba(103, 58, 183, 0.2)',
    'rgba(0, 188, 212, 0.2)',
    'rgba(255, 193, 7, 0.2)',
    'rgba(233, 30, 99, 0.2)',
    'rgba(0, 150, 136, 0.2)',
  ],
  paletteIndex: 0,
};

export default function colorPalette(state = defaultState, action) {
  switch (action.type) {
    case UPDATE_ROI_COLORS: {
      return Object.assign({}, state, { roiColor: action.roiColor });
    }
    case SET_NEXT_PALETTE_COLOR: {
      const nextPaletteIndex = (state.paletteIndex + 1) % state.palette.length;

      const roiColor = {
        current: state.roiColor.next,
        next: state.palette[nextPaletteIndex],
      };

      return Object.assign({}, state, {
        roiColor,
        paletteIndex: nextPaletteIndex,
      });
    }
    case UPDATE_PALETTE_COLORS: {
      let colors = [];

      for (let i = 0; i < colouredAnnotationTypes.length; i++) {
        const rois = action.measurements[colouredAnnotationTypes[i]];
        if (rois && rois.length) {
          colors = [...colors, ...rois.map(measurement => measurement.color)];
        }
      }
      colors = _.difference(colors, state.palette);
      colors = _.uniq(colors);
      const palette = [...state.palette, ...colors];
      return Object.assign({}, state, { palette });
    }
    case SET_PALETTE_COLORS_FROM_CONFIG: {
      let paletteColors = cloneDeep(state.palette);
      let roiColor = cloneDeep(state.roiColor);
      if (action.colors.length) {
        paletteColors = action.colors;
        roiColor.current = action.colors[0];
        roiColor.next = action.colors[0];
      }
      return { ...state, palette: paletteColors, roiColor };
    }
    default:
      return state;
  }
}
