import { SET_STUDY_FORM_AVAILABILITY } from '../constants/ActionTypes';

const defaultState = {
  isStudyFormExist: false,
};

export default function ohifConfigProperties(state = defaultState, action) {
  switch (action.type) {
    case SET_STUDY_FORM_AVAILABILITY: {
      return { ...state, isStudyFormExist: action.status };
    }
    default:
      return state;
  }
}
