import { SET_FOV_GRID_RESTORE_INFO } from '../constants/ActionTypes';

const defaultState = {
  centerPos: null,
  isCenterEnabled: false,
  selectedFovIndex: -1,
};

export default function fovGridRestoreInfo(state = defaultState, action) {
  switch (action.type) {
    case SET_FOV_GRID_RESTORE_INFO: {
      if (!action.restoreInfo) {
        return Object.assign({}, { ...defaultState });
      }
      return {
        ...state,
        centerPos: action.restoreInfo.centerPos || null,
        isCenterEnabled: action.restoreInfo.isCenterEnabled || false,
        selectedFovIndex:
          action.restoreInfo.selectedFovIndex >= 0
            ? action.restoreInfo.selectedFovIndex
            : -1,
      };
    }
    default:
      return state;
  }
}
