import { SET_SELECTED_SCISSORS_STRATEGY } from '../constants/ActionTypes';

const defaultState = { selectedStrategy: '' };

export default function smartCT(state = defaultState, action) {
  switch (action.type) {
    case SET_SELECTED_SCISSORS_STRATEGY: {
      return { ...state, selectedStrategy: action.selectedStrategy };
    }
    default:
      return state;
  }
}
