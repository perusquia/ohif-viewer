import actions from './actions';
import reducers from './reducers';
import selectors from './selectors';
import * as constants from './constants/ActionTypes';
import * as smartCTRanges from './constants/SmartCTRanges';

const redux = {
  actions,
  constants,
  reducers,
  selectors,
  smartCTRanges,
};

export default redux;
