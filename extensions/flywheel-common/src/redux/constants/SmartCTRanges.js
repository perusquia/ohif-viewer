import csTools from 'cornerstone-tools';
import { addOrUpdateSmartCTRange } from '../../utils/addOrUpdateSmartCTRange';

let smartCTRanges = [
  {
    label: 'Lung',
    range: {
      min: -700,
      max: -600,
    },
    color: '',
    description: '',
  },
  {
    label: 'Fat',
    range: {
      min: -120,
      max: -90,
    },
    color: '',
    description: '',
  },
  {
    label: 'Cerebrospinal Fluids',
    range: {
      min: 15,
      max: 16,
    },
    color: '',
    description: '',
  },
  {
    label: 'Fluids',
    range: {
      min: -30,
      max: 45,
    },
    color: '',
    description: '',
  },
  {
    label: 'Lymph Nodes',
    range: {
      min: 10,
      max: 20,
    },
    color: '',
    description: '',
  },
  {
    label: 'Kidney',
    range: {
      min: 20,
      max: 45,
    },
    color: '',
    description: '',
  },
  {
    label: 'Muscle',
    range: {
      min: 35,
      max: 55,
    },
    color: '',
    description: '',
  },
  {
    label: 'Liver',
    range: {
      min: 54,
      max: 66,
    },
    color: '',
    description: '',
  },
  {
    label: 'Gallstone',
    range: {
      min: 30,
      max: 120,
    },
    color: '',
    description: '',
  },
  {
    label: 'Soft Tissues',
    range: {
      min: 100,
      max: 300,
    },
    color: '',
    description: '',
  },
  {
    label: 'Bone (Cancellous)',
    range: {
      min: 300,
      max: 400,
    },
    color: '',
    description: '',
  },
  {
    label: 'Bone (Cortical)',
    range: {
      min: 500,
      max: 1900,
    },
    color: '',
    description: '',
  },
  {
    label: 'Foreign Body',
    range: {
      min: 2100,
      max: 31000,
    },
    color: '',
    description: '',
  },
];

function getSmartCTRanges() {
  const segmentationColor =
    csTools.store.modules.segmentation.state.colorLutTables;

  smartCTRanges = smartCTRanges.map((x, index) => {
    x.color = segmentationColor[0][index + 1];
    return x;
  });

  return smartCTRanges;
}

function addSmartCTRange(entry) {
  addOrUpdateSmartCTRange(smartCTRanges, entry);
  return smartCTRanges;
}

export { getSmartCTRanges, addSmartCTRange };
