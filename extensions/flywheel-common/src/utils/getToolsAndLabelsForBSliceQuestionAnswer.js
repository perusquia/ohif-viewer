import { getToolsAndLabelsForQuestionAnswer } from '.';

function getToolsAndLabelsForBSliceQuestionAnswer(
  question,
  notes,
  sliceNumber
) {
  const selectedBSliceAnswer = notes?.slices?.[sliceNumber]?.[question?.key];
  const selectedAnswer =
    selectedBSliceAnswer?.value || selectedBSliceAnswer || question?.answer;
  const selectedOption = question?.values?.find(
    x => x.value === selectedAnswer
  );
  const { questionTools, questionLabels } = getToolsAndLabelsForQuestionAnswer(
    question,
    selectedOption,
    sliceNumber
  );
  return { questionTools, questionLabels };
}

export default getToolsAndLabelsForBSliceQuestionAnswer;
