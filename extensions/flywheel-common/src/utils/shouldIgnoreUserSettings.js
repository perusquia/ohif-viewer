import getRouteParams from './getRouteParams';
import hasMultipleReaderTask from './hasMultipleReaderTask';

const regExPattern = /^true$/i;

function shouldIgnoreUserSettings(state) {
  const { ignoreUserSettings } = getRouteParams();
  const isTrueSet = regExPattern.test(ignoreUserSettings);
  if (isTrueSet) {
    return true;
  }
  return hasMultipleReaderTask(state);
}

export default shouldIgnoreUserSettings;
