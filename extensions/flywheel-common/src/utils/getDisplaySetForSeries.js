export const getDisplaySetForSeries = (study, SeriesInstanceUID) => {
  const displaySet = study.findDisplaySet(displaySet =>
    displaySet.images?.find(
      image => image.getSeriesInstanceUID() === SeriesInstanceUID
    )
  );
  return displaySet;
};
