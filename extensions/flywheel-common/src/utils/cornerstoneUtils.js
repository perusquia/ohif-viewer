import cornerstone from 'cornerstone-core';
import cornerstoneTools from 'cornerstone-tools';
import cornerstoneMath from 'cornerstone-math';
import OHIF from '@ohif/core';

import { getEnabledElement } from '@ohif/extension-cornerstone/src/state'; // by viewport index

const BaseAnnotationTool = cornerstoneTools.importInternal(
  'base/BaseAnnotationTool'
);
const convertToVector3 = cornerstoneTools.import('util/convertToVector3');
export { getEnabledElement };

export function getEnabledElements() {
  return new Set(
    cornerstone.getEnabledElements().map(enabled => enabled.element)
  );
}

/**
 * It returns every tool instance of BaseAnnotation
 * @return {Array} Array of annotation tools
 */
export const getCurrentAnnotationTools = () => {
  try {
    return cornerstoneTools.store.state.tools.filter(toolInstance => {
      return toolInstance.__proto__ instanceof BaseAnnotationTool;
    });
  } catch (e) {
    return [];
  }
};

// primitive cache strategy to prevent calling api to set a mode already set.
// It prevents duplicate data on tool change history
let cacheToolMode = {};
const toolModeFn = {
  passive: cornerstoneTools.setToolPassive,
  enabled: cornerstoneTools.setToolEnabled,
  active: cornerstoneTools.setToolActive,
  disabled: cornerstoneTools.setToolDisabled,
};
/**
 * Wrapper method to set cs tool mode. It caches current state to avoid any unnecessary set.
 * @param {string} toolName tool name
 * @param {'passive' | 'enabled' | 'active' | 'disabled'} toolMode tool mode
 */
export const setToolMode = (toolName, toolMode, viewportIndex = null) => {
  let firstElement = null;
  let tool = null;
  try {
    const enabledElements = cornerstone.getEnabledElements();
    // Safe guard to skip setting the tool mode when no cornerstone viewports available
    if (enabledElements.length) {
      firstElement = enabledElements[0].element;
      const element =
        viewportIndex !== null && viewportIndex >= 0
          ? getEnabledElement(viewportIndex)
          : firstElement;
      const enabledElement = element
        ? cornerstone.getEnabledElement(element)
        : null;
      tool = cornerstoneTools.getToolForElement(firstElement, toolName);
      if (enabledElement?.image) {
        tool?.initializeDefaultCursor?.({ detail: enabledElement });
      }
      if (tool.mode === toolMode && tool._options.mouseButtonMask.includes(1)) {
        return;
      }
    }
  } catch (error) {}
  const fn = toolModeFn[toolMode];
  if (fn) {
    fn(toolName, { mouseButtonMask: 1 });
    cacheToolMode[toolName] = toolMode;
  }
};

/**
 * It clears local cache data
 */
export const clearCacheToolMode = () => {
  cacheToolMode = {};
};
/**
 * It clears cs tools state history for all items or only given items.
 * @param {Array<string>} onlyFor array of tools name to be cleared out
 * @return {boolean} True in case of success
 */
export const clearToolChangeHistory = onlyFor => {
  try {
    if (onlyFor && Array.isArray(onlyFor)) {
      cornerstoneTools.store.state.globalToolChangeHistory = cornerstoneTools.store.state.globalToolChangeHistory.filter(
        historyItem => {
          const { args } = historyItem;
          const toolType = args[0];
          return !onlyFor.includes(toolType);
        }
      );
    } else {
      cornerstoneTools.store.state.globalToolChangeHistory = [];
    }
    return true;
  } catch (e) {
    OHIF.warn('Something went wrong when clearing tool state change history');
  }

  return false;
};

/**
 * Find the nearest image index to the patient position passed
 * @param {String[]} imageIds stack image ids
 * @param {Object} patientPos position in patient co-ordinate
 * @returns {Number} nearest slice index
 */
export function getNearestSliceFromImagesIds(imageIds, patientPos) {
  let minDistance = Number.MAX_VALUE;
  let newImageIdIndex = -1;
  const sourceImagePosition = convertToVector3(patientPos);

  imageIds.forEach((imageId, index) => {
    const imagePlane = cornerstone.metaData.get('imagePlaneModule', imageId);

    if (!imagePlane?.imagePositionPatient) {
      return;
    }

    const imagePosition = convertToVector3(imagePlane.imagePositionPatient);
    const distance = imagePosition.distanceToSquared(sourceImagePosition);

    if (distance < minDistance) {
      minDistance = distance;
      newImageIdIndex = index;
    }
  });
  return newImageIdIndex;
}

export function getCurrentDateTimeString() {
  const format = val => String(val).padStart(2, '0');
  const d = new Date();
  const date = [
    d.getFullYear(),
    format(d.getMonth()),
    format(d.getDate()),
  ].join('');
  const time = [
    format(d.getHours()),
    format(d.getMinutes()),
    format(d.getSeconds()),
  ].join('');
  return `${date}_${time}`;
}

export function getPlaneMetadata(imageId) {
  const getPatientDetails = (
    metadataKey,
    imageId,
    imageOrientationPatientKey,
    imagePositionPatientKey
  ) => {
    const entity = cornerstone.metaData.get(metadataKey, imageId);
    const {
      [imageOrientationPatientKey]: imageOrientation,
      [imagePositionPatientKey]: imagePosition,
    } = entity || {};

    if (!imageOrientation || !imagePosition) {
      return undefined;
    }

    return {
      rowCosines: new cornerstoneMath.Vector3(
        parseFloat(imageOrientation[0]),
        parseFloat(imageOrientation[1]),
        parseFloat(imageOrientation[2])
      ),
      columnCosines: new cornerstoneMath.Vector3(
        parseFloat(imageOrientation[3]),
        parseFloat(imageOrientation[4]),
        parseFloat(imageOrientation[5])
      ),
      imagePositionPatient: new cornerstoneMath.Vector3(
        parseFloat(imagePosition[0]),
        parseFloat(imagePosition[1]),
        parseFloat(imagePosition[2])
      ),
    };
  };

  // get from metadata property 'imagePlaneModule' or fallback to get it from 'instance'
  // nifti provider does not support metadata property 'instance'.
  return (
    getPatientDetails(
      'imagePlaneModule',
      imageId,
      'imageOrientationPatient',
      'imagePositionPatient'
    ) ||
    getPatientDetails(
      'instance',
      imageId,
      'ImageOrientationPatient',
      'ImagePositionPatient'
    )
  );
}

/**
 * Determine the axis most closely-aligned to the normal vector of the slice planes, based on slice cosine values.
 */
const XYZ = 'xyz'.split('');

export function getNormalAxisFromImageId(imageId) {
  const metadata = getPlaneMetadata(imageId);
  if (!metadata || !metadata.columnCosines || !metadata.rowCosines) {
    return undefined;
  }
  const columnAxis = XYZ.reduce((result, axis) =>
    Math.abs(metadata.columnCosines[axis]) >
    Math.abs(metadata.columnCosines[result])
      ? axis
      : result
  );
  const rowAxis = XYZ.reduce((result, axis) =>
    Math.abs(metadata.rowCosines[axis]) > Math.abs(metadata.rowCosines[result])
      ? axis
      : result
  );
  return XYZ.find(axis => axis !== columnAxis && axis !== rowAxis);
}

/**
 * To get normal axis orientation from imageId
 * @param {string} imageId
 * @returns
 */
export const getNormalAxisOrientation = imageId => {
  const axis = getNormalAxisFromImageId(imageId);
  if (axis === 'z') {
    return 'Axial';
  } else if (axis === 'y') {
    return 'Coronal';
  } else if (axis === 'x') {
    return 'Sagittal';
  }

  return '';
};

export const getFileName = imageId => {
  const selectedRange = store.getState()?.smartCT?.selectedRange;
  const orientation = getNormalAxisOrientation(imageId);
  if (selectedRange) {
    const fileName = `Mask_${orientation}_${
      selectedRange.label
    }_${getCurrentDateTimeString()}.nii.gz`;

    return fileName;
  }
};

/**
 * To get imagePlane from imageId.
 * If imagePlane values are undefined or not Real non-zero it will return with default values.
 * @param {string} imageId
 * @returns {Object} image plane metadata
 */
export const getFallbackImagePlane = imageId => {
  const imagePlane = cornerstone.metaData.get('imagePlaneModule', imageId);
  const defaultColumnCosines = [0, 1, 0];
  const defaultRowCosines = [1, 0, 0];
  const defaultImagePositionPatient = [1, 0, 0, 0, 1, 0];

  imagePlane.columnPixelSpacing = imagePlane.columnPixelSpacing || 1;
  imagePlane.rowPixelSpacing = imagePlane.rowPixelSpacing || 1;
  imagePlane.columnCosines = imagePlane.columnCosines || defaultColumnCosines;
  imagePlane.rowCosines = imagePlane.rowCosines || defaultRowCosines;
  imagePlane.imagePositionPatient =
    imagePlane.imagePositionPatient || defaultImagePositionPatient;

  return imagePlane;
};
