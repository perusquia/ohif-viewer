import redux from '../redux';
import http from '../http';
import {
  isCurrentFile,
  isCurrentNifti,
  isCurrentMetaImage,
} from './isCurrentFile';

const {
  selectCurrentNiftis,
  selectCurrentMetaImage,
  selectCurrentWebImage,
} = redux.selectors;

// Get the active viewport file info
const getActiveFile = async state => {
  const viewportSpecificData =
    state.viewports.viewportSpecificData[state.viewports.activeViewportIndex] ||
    {};

  let activeFiles = [];
  let activeFile = null;
  if (isCurrentFile(state)) {
    activeFiles = isCurrentNifti(state)
      ? selectCurrentNiftis(state)
      : isCurrentMetaImage(state)
      ? selectCurrentMetaImage(state)
      : selectCurrentWebImage(state) || [];
    activeFiles = !Array.isArray(activeFiles) ? [activeFiles] : activeFiles;
    activeFile = activeFiles.find(
      image => viewportSpecificData.StudyInstanceUID === image.filename
    );
  } else {
    const activeAssociation = state.flywheel.allAssociations?.find(
      association =>
        association.study_uid === viewportSpecificData.StudyInstanceUID &&
        association.series_uid === viewportSpecificData.SeriesInstanceUID &&
        association.acquisition_id !== 'None'
    );
    const acquisitions = state.flywheel.acquisitions || {};
    const keys = Object.keys(acquisitions) || [];
    if (!keys?.length) {
      const acq = await http.services.getAcquisition(
        activeAssociation.acquisition_id
      );
      if (acq) {
        acquisitions[activeAssociation.acquisition_id] = acq;
        keys.push(activeAssociation.acquisition_id);
      }
    }
    const activeAcquisitionKey =
      keys.find(key => activeAssociation.acquisition_id === key) || '';
    activeFile = acquisitions[activeAcquisitionKey]?.files?.find(
      file => file._id === activeAssociation.file_id
    );
  }
  return activeFile;
};

export default getActiveFile;
