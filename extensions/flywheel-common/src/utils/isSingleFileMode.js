import getRouteParams from './getRouteParams';
import { isCurrentFile } from './isCurrentFile';

/**
 * It checks if is a single study or not, based on store state or url path
 * @param {Object} state app redux store
 * @param {Object} location application location
 * @param {boolean} skipForTaskMode whether to skip application URL checking for task mode
 * @return {boolean} true in case its a single file mode
 */
const isSingleFileMode = (state, location, skipForTaskMode = false) => {
  const { StudyInstanceUID, SeriesInstanceUID, taskId } = getRouteParams(
    location
  );
  let filteringStudyOnURL = StudyInstanceUID && SeriesInstanceUID;
  if (skipForTaskMode && taskId) {
    // Whether to skip application URL checking for task mode with task id in the application URL
    filteringStudyOnURL = null;
  }

  return isCurrentFile(state) || filteringStudyOnURL;
};

export default isSingleFileMode;
