import redux from '../redux';
import _ from 'lodash';
import getRouteParams from './getRouteParams';
import { utils } from '@ohif/core';

const { selectCurrentNifti, selectCurrentMetaImage } = redux.selectors;
const { ZIP_IMAGES } = utils.regularExpressions;

const PATH_FILE_TYPES = {
  nifti: 'nifti',
  metaimage: 'metaimage',
  webimage: 'image',
  tiff: 'tiff',
};

/**
 * It checks if current path is related to given type param
 * @param {string} type file type to be compared with
 * @return {boolean} true in case its a study of given type
 */
const isCurrentFileType = type => {
  const location = window.location;
  const {
    fileContainerId,
    filename,
    fileType,
    imageSessionId,
  } = getRouteParams(location);

  const matchURLPath =
    fileType === type &&
    ((!!fileContainerId && !!filename) || !!imageSessionId);
  return matchURLPath;
};

/**
 * It checks if its a nifti study or not, based on store state or url path
 * @param {Object} state app redux store
 * @return {boolean} true in case its a nifti study
 */
const isCurrentNifti = state => {
  const stateNifti = selectCurrentNifti(state);
  const urlNifti = isCurrentFileType(PATH_FILE_TYPES.nifti);

  return !_.isEmpty(stateNifti) || urlNifti;
};

/**
 * It checks if its a meta-image(mhd only supporting now) study or not, based on store state or url path
 * @param {Object} state app redux store
 * @return {boolean} true in case its a mhd study
 */
const isCurrentMetaImage = state => {
  const stateMetaImage = selectCurrentMetaImage(state);
  const urlMetaImage = isCurrentFileType(PATH_FILE_TYPES.metaimage);

  return !_.isEmpty(stateMetaImage) || urlMetaImage;
};

/**
 * It checks if its a webImage file or not, based url path
 * @return {boolean} true in case its a webimage
 */
const isCurrentWebImage = () => {
  return (
    isCurrentFileType(PATH_FILE_TYPES.webimage) ||
    isCurrentFileType(PATH_FILE_TYPES.tiff)
  );
};

const isCurrentVolumetricImage = state =>
  isCurrentNifti(state) || isCurrentMetaImage(state);

const isCurrentFile = state =>
  isCurrentVolumetricImage(state) || isCurrentWebImage();

const isCurrentZipFile = fileName => ZIP_IMAGES.test(fileName);

export {
  isCurrentNifti,
  isCurrentMetaImage,
  isCurrentWebImage,
  isCurrentVolumetricImage,
  isCurrentFile,
  isCurrentZipFile,
};
