import getRouteParams from './getRouteParams';
import isSingleFileMode from './isSingleFileMode';

/**
 * To allow forms assigned in task to be shown in UI.
 * @param {Object} projectConfig projectConfig from store
 * @param {Object} state app redux store
 * @param {Object} location application location
 * @return {boolean} true in case its not a single file mode or has study form questions
 */

const shouldShowStudyForm = (projectConfig, state, location) => {
  const { taskId } = getRouteParams(location);
  const hasQuestions =
    projectConfig && (projectConfig.questions || projectConfig.studyForm);
  const isSingleFile = isSingleFileMode(state, location);

  return (!isSingleFile || taskId) && hasQuestions;
};
export default shouldShowStudyForm;
