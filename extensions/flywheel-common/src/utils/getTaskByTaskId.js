function getTaskByTaskID(taskId) {
  const state = store.getState();
  const multipleReaderTask = state.multipleReaderTask;
  if (multipleReaderTask?.TaskIds?.length) {
    const readerTask = multipleReaderTask.multipleTaskResponse.find(
      response => response._id === taskId
    );
    return readerTask;
  } else {
    const readerTask = state.flywheel.readerTaskWithFormResponse;
    return readerTask;
  }
}

export default getTaskByTaskID;
