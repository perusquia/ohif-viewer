import store from '@ohif/viewer/src/store';
import { isCurrentFile } from './isCurrentFile';
import { getSessionRead } from './bSliceColorUtils';
import cloneDeep from 'lodash.clonedeep';

const QUESTION_TYPE_DEFAULT_VALUE = {
  radio: '',
  dropdown: '',
  selectboxes: [],
  textfield: '',
  text: '',
  textarea: '',
  content: '',
  info: {},
};

function getSelectedAnswer(question, notes, sliceNumber, subFormAnnotationId) {
  const state = store.getState();
  const isFile = isCurrentFile(state);
  const projectConfig = state.flywheel.projectConfig;
  let answerValue = QUESTION_TYPE_DEFAULT_VALUE[question.type];

  if (subFormAnnotationId) {
    const subFormInfo = getSubFormInfoFromMeasurement(
      subFormAnnotationId,
      projectConfig
    );
    if (subFormInfo) {
      answerValue =
        !isFile && projectConfig?.bSlices
          ? notes.slices?.[sliceNumber]?.[subFormInfo.questionKey]?.[
              subFormInfo.subFormName
            ]?.find(item => item.annotationId === subFormAnnotationId)?.[
              question.key
            ]
          : notes?.[subFormInfo.questionKey]?.[subFormInfo.subFormName]?.find(
              item => item.annotationId === subFormAnnotationId
            )?.[question.key];
    }
  } else {
    answerValue =
      !isFile && projectConfig?.bSlices
        ? notes.slices?.[sliceNumber]?.[question.key]
        : notes[question.key];
  }

  return answerValue?.value ? answerValue?.value : answerValue;
}

function getSelectedOptionForQuestion(
  question,
  sliceNumber,
  subFormAnnotationId
) {
  const notes = getSessionRead()?.notes || {};
  const answer = getSelectedAnswer(
    question,
    notes,
    sliceNumber,
    subFormAnnotationId
  );
  const selectedOption = question?.values?.find(x => x.value === answer);
  return selectedOption;
}

function getSubFormInfoFromMeasurement(subFormAnnotationId, projectConfig) {
  const annotation = getAnnotationById(subFormAnnotationId);
  const questions = projectConfig?.studyForm?.components || [];
  if (questions.length && annotation) {
    const question = questions.find(x => x.key === annotation.questionKey);
    const questionOption = question.values.find(
      x => x.value === annotation.answer
    );
    return {
      subFormName: questionOption.subForm,
      questionKey: annotation.questionKey,
    };
  }
}

function getAnnotationById(annotationId) {
  const state = store.getState();
  const measurements = state.timepointManager.measurements;
  let annotation = null;
  Object.keys(measurements).some(toolType => {
    annotation = measurements[toolType].find(
      measurement => measurement.uuid === annotationId
    );
    return annotation;
  });
  return annotation;
}

function getSelectedOptionTools(deletedMeasurement, selectedOption) {
  const state = store.getState();
  const isFile = isCurrentFile(state);
  const projectConfig = state.flywheel?.projectConfig;
  const { bSlices } = projectConfig;
  const sliceSettings = bSlices?.settings;
  let selectedOptionTools = [];
  const sliceSpecificMeasurementTools =
    sliceSettings?.[deletedMeasurement.sliceNumber]?.measurementTools?.[
      deletedMeasurement.questionKey
    ];

  if (!isFile && sliceSpecificMeasurementTools) {
    selectedOptionTools = cloneDeep(sliceSpecificMeasurementTools);
    return selectedOptionTools;
  }

  if (selectedOption?.instructionSet) {
    selectedOption.instructionSet.forEach(x => {
      if (x.measurementTools) {
        selectedOptionTools = cloneDeep(
          selectedOptionTools.concat(x.measurementTools)
        );
      }
    });
    return selectedOptionTools;
  }

  if (selectedOption?.measurementTools) {
    selectedOptionTools = cloneDeep(selectedOption?.measurementTools);
  }

  return selectedOptionTools;
}

function getToolsAndToolTypeForSelectedOption(
  deletedMeasurement,
  selectedOption
) {
  const selectedOptionTools = getSelectedOptionTools(
    deletedMeasurement,
    selectedOption
  );
  const isToolAvailableForMeasurement = selectedOptionTools.includes(
    deletedMeasurement.toolType
  );
  const tools = isToolAvailableForMeasurement
    ? [deletedMeasurement.toolType]
    : selectedOptionTools;
  const toolType = isToolAvailableForMeasurement
    ? deletedMeasurement.toolType
    : selectedOptionTools?.[0];
  return { tools, toolType };
}

export {
  getSelectedOptionForQuestion,
  getSelectedAnswer,
  getToolsAndToolTypeForSelectedOption,
};
