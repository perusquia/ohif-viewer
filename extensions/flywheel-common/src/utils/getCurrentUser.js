import store from '@ohif/viewer/src/store';

export const getCurrentUser = () => {
  const state = store.getState();
  return state.flywheel.user;
};

export default getCurrentUser;
