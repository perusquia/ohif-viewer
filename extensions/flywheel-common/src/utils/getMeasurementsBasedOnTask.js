function getMeasurementsBasedOnTask(measurements, taskId) {
  const filteredMeasurements = measurements?.filter(
    measurement => measurement.task_id === taskId
  );
  return filteredMeasurements;
}
export default getMeasurementsBasedOnTask;
