import { getCountForLabel } from './hasToolLimitReached';

function getLabelCount(
  label,
  question,
  selectedOption,
  sliceNumber,
  bSliceSettings,
  subFormAnnotationId
) {
  let count = 0;
  if (!question) {
    return count;
  }

  count = getCountForLabel(
    label,
    question,
    sliceNumber,
    bSliceSettings,
    subFormAnnotationId
  );

  return count;
}

export default getLabelCount;
