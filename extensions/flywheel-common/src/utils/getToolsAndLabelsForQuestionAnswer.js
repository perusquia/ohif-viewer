import {
  getBSliceTools,
  getLabelsForTool,
  getMeasurementToolsForAnswer,
  getBSliceSettings,
} from './hasToolLimitReached';

function getToolsAndLabelsForQuestionAnswer(
  question,
  selectedOption,
  sliceNumber,
  selectedInstruction
) {
  const bSliceSettings = getBSliceSettings();
  const questionTools = getMeasurementToolsForAnswer(
    selectedOption,
    null,
    question,
    bSliceSettings,
    sliceNumber,
    selectedInstruction
  );
  const questionLabels = new Set();
  const bSliceTools = getBSliceTools(question, bSliceSettings, sliceNumber);
  questionTools.forEach(tool => {
    const labelsForTools = getLabelsForTool(tool, selectedOption, bSliceTools);
    labelsForTools.forEach(t => {
      questionLabels.add(t);
    });
  });

  return { questionTools, questionLabels: Array.from(questionLabels) };
}

export default getToolsAndLabelsForQuestionAnswer;
