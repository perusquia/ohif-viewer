import store from '@ohif/viewer/src/store';

export const getDefaultROILabelFromHPLayout = () => {
  const state = store.getState();
  let defaultRoiLabelDetails = {};
  const layoutViewports = state.infusions.protocolLayout?.viewports;

  if (!layoutViewports) {
    return defaultRoiLabelDetails;
  }

  Object.keys(layoutViewports).forEach(key => {
    Object.keys(layoutViewports[key])?.forEach(index => {
      if (layoutViewports[key][index]?.defaultRoiLabel) {
        if (
          !defaultRoiLabelDetails[layoutViewports[key][index].SeriesInstanceUID]
        ) {
          defaultRoiLabelDetails[
            layoutViewports[key][index].SeriesInstanceUID
          ] = [];
        }
        defaultRoiLabelDetails[
          layoutViewports[key][index].SeriesInstanceUID
        ].push(layoutViewports[key][index].defaultRoiLabel);
      }
    });
  });

  return defaultRoiLabelDetails;
};

export const getAllFovealRois = defaultRoiLabels => {
  const state = store.getState();
  const annotations = [];
  const measurements = state.timepointManager?.measurements;
  const fovGridSupportTools = [
    'CircleRoi',
    'FreehandRoi',
    'ContourRoi',
    'RectangleRoi',
    'EllipticalRoi',
  ];

  Object.keys(measurements).forEach(tool => {
    if (fovGridSupportTools.includes(tool)) {
      measurements[tool]?.forEach(annotation => {
        if (defaultRoiLabels?.includes(annotation?.location)) {
          annotations.push(annotation);
        }
      });
    }
  });

  return annotations;
};
