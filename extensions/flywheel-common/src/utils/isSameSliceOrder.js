import cornerstone from 'cornerstone-core';
import { Vector3 } from 'cornerstone-math';

const isSliceOrderSame = (baseImageIds, segImageIds) => {
  if (baseImageIds?.length < 2 || segImageIds?.length < 2) {
    return;
  }
  let isSameSliceOrder = true;

  const {
    imagePositionPatient: baseImagePositionPatient1,
    imageOrientationPatient,
  } = cornerstone.metaData.get('imagePlaneModule', baseImageIds[0]);
  const {
    imagePositionPatient: baseImagePositionPatient2,
  } = cornerstone.metaData.get('imagePlaneModule', baseImageIds[1]);
  const {
    imagePositionPatient: segImagePositionPatient1,
  } = cornerstone.metaData.get('imagePlaneModule', segImageIds[0]);
  const {
    imagePositionPatient: segImagePositionPatient2,
  } = cornerstone.metaData.get('imagePlaneModule', segImageIds[1]);

  const baseSliceDirection = [
    baseImagePositionPatient2[0] - baseImagePositionPatient1[0],
    baseImagePositionPatient2[1] - baseImagePositionPatient1[1],
    baseImagePositionPatient2[2] - baseImagePositionPatient1[2],
  ];

  const segSliceDirection = [
    segImagePositionPatient2[0] - segImagePositionPatient1[0],
    segImagePositionPatient2[1] - segImagePositionPatient1[1],
    segImagePositionPatient2[2] - segImagePositionPatient1[2],
  ];

  const majorAxisNormalIndex = getNormalAxisIndex(imageOrientationPatient);

  isSameSliceOrder =
    baseSliceDirection[majorAxisNormalIndex] /
      Math.abs(baseSliceDirection[majorAxisNormalIndex]) ===
    segSliceDirection[majorAxisNormalIndex] /
      Math.abs(segSliceDirection[majorAxisNormalIndex]);

  return isSameSliceOrder;
};

const getNormalAxisIndex = imageOrientation => {
  const axisNormal = new Vector3(
    imageOrientation[0],
    imageOrientation[1],
    imageOrientation[2]
  )
    .cross(
      new Vector3(imageOrientation[3], imageOrientation[4], imageOrientation[5])
    )
    .toArray();
  const majorAxisNormal = axisNormal.reduce((majorComponent, component) =>
    Math.abs(majorComponent) > Math.abs(component) ? majorComponent : component
  );
  return axisNormal.indexOf(majorAxisNormal);
};

export default isSliceOrderSame;
