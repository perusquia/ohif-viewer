import store from '@ohif/viewer/src/store';
import {
  isCurrentFile,
  isCurrentVolumetricImage,
  isCurrentWebImage,
} from '../utils';
import { validateAllBSlice } from '../utils/validateBSlice.js';
import { customInfoKey } from '../http/client/utils/requestUtils.js';
import Redux from '../redux';

const { selectCurrentVolumetricImage, selectCurrentWebImage } = Redux.selectors;
const { studyFormBSliceValidatedInfo } = Redux.actions;

const greenColor = 'rgba(60, 179, 113, 0.2)';
const redColor = 'rgba(255, 0, 0, 0.2)';

/**
 * Finds B Slice indicator color from the color palette
 * @param {string} sliceNumberKey - slice number key
 * @param {Object} bSliceSettings
 * @returns {string} hexadecimal color code string
 */
const getBSliceIndicatorColor = (sliceNumberKey, bSliceSettings) => {
  const state = store.getState();
  const sessionRead = getSessionRead();
  const allBSliceValidatedInfo =
    state.studyFormBSliceValidatedInfo.bSliceValidation;
  const projectConfig = state.flywheel.projectConfig;

  if (Object.keys(allBSliceValidatedInfo).length) {
    const allBSliceValidatedData = validateAllBSlice(
      projectConfig,
      sessionRead?.notes
    );
    if (allBSliceValidatedData?.[sliceNumberKey]) {
      return getHexadecimalCodeFromRGBA(greenColor);
    } else if (allBSliceValidatedData?.[sliceNumberKey] === false) {
      return getHexadecimalCodeFromRGBA(redColor);
    }
  } else if (
    !Object.keys(allBSliceValidatedInfo).length &&
    sessionRead?.notes
  ) {
    const allBSliceValidatedData = validateAllBSlice(
      projectConfig,
      sessionRead?.notes
    );
    store.dispatch(studyFormBSliceValidatedInfo(allBSliceValidatedData));
    if (allBSliceValidatedData?.[sliceNumberKey]) {
      return getHexadecimalCodeFromRGBA(greenColor);
    } else if (allBSliceValidatedData?.[sliceNumberKey] === false) {
      return getHexadecimalCodeFromRGBA(redColor);
    }
  }

  const paletteColors = window.store.getState()?.colorPalette?.palette;
  if (paletteColors?.length) {
    const keys = Object.keys(bSliceSettings);
    const bsliceIndex = keys.findIndex(key => sliceNumberKey === key);
    const colorIndex = bsliceIndex % paletteColors.length;
    return getHexadecimalCodeFromRGBA(paletteColors[colorIndex]);
  }

  return null;
};

/**
 * Finds the hexadecimal value of color from the RGBA color string
 * @param {string} color
 * @returns {string} Hexadecimal value
 */
const getHexadecimalCodeFromRGBA = color => {
  if (!color) {
    return null;
  }
  const rgba = color.replace(/^rgba?\(|\s+|\)$/g, '').split(',');
  const hex = `#${(
    (1 << 24) +
    (parseInt(rgba[0]) << 16) +
    (parseInt(rgba[1]) << 8) +
    parseInt(rgba[2])
  )
    .toString(16)
    .slice(1)}`;

  return hex;
};

const getSessionRead = () => {
  const state = store.getState();
  let sessionRead = {};
  if (state.flywheel.readerTaskWithFormResponse) {
    sessionRead = state.flywheel.readerTaskWithFormResponse.info;
  } else {
    if (isCurrentFile(state)) {
      const user = state.flywheel.user;
      if (!user) {
        return null;
      }
      let currentFile = null;
      if (isCurrentVolumetricImage(state)) {
        currentFile = selectCurrentVolumetricImage(state, 0);
      } else if (isCurrentWebImage()) {
        currentFile = selectCurrentWebImage(state);
      }
      const data = currentFile?.info?.ohifViewer || {};
      if (!data) {
        return null;
      }
      sessionRead = data.read?.[customInfoKey(user._id)];
    } else {
      const user = state.flywheel.user;
      if (!user) {
        return null;
      }
      const session =
        state.flywheel.sessions !== null ? state.flywheel.sessions[0] : null;
      const data = session?.info?.ohifViewer || {};
      if (!data) {
        return null;
      }
      sessionRead = data.read?.[customInfoKey(user._id)];
    }
  }
  return sessionRead;
};

export { getBSliceIndicatorColor, getSessionRead };
