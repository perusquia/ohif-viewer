function hasMultipleReaderTask(state) {
  if (state?.multipleReaderTask?.TaskIds?.length) {
    return true;
  }
  return false;
}

export default hasMultipleReaderTask;
