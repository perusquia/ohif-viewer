import cornerstone from 'cornerstone-core';
import store from '@ohif/viewer/src/store';

const waitForViewportUpdate = async viewportSpecificDataArray => {
  return new Promise((resolve, reject) => {
    const unsubscribeFn = store.subscribe(() => {
      const state = store.getState();
      const viewports = state.viewports;
      const viewport = viewportSpecificDataArray.find(
        viewportData =>
          !Object.keys(viewports.viewportSpecificData).find(
            key =>
              viewports.viewportSpecificData[key].SeriesInstanceUID ===
              viewportData.displaySet.SeriesInstanceUID
          )
      );
      if (!viewport) {
        let isViewportStackUpdated = true;
        cornerstone.getEnabledElements().forEach((enabledElement, index) => {
          const { displaySet } =
            viewportSpecificDataArray.find(
              ({ viewportIndex }) => viewportIndex === index
            ) || {};
          if (!displaySet) {
            return;
          }
          const seriesModule = cornerstone.metaData.get(
            'generalSeriesModule',
            enabledElement.image.imageId
          );
          if (
            displaySet?.SeriesInstanceUID !== seriesModule?.seriesInstanceUID
          ) {
            isViewportStackUpdated = false;
          }
        });
        if (isViewportStackUpdated) {
          unsubscribeFn();
          resolve();
        }
      }
    });
  });
};

export default waitForViewportUpdate;
