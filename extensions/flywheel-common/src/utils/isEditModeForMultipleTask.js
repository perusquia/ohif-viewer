import store from '@ohif/viewer/src/store';
import hasMultipleReaderTask from './hasMultipleReaderTask';

function isEditModeForMultipleTask() {
  const state = store.getState();
  const hasMultipleTask = hasMultipleReaderTask(state);
  const selectedTaskId = state.viewerAvatar?.taskId;
  const singleAvatar = state.viewerAvatar?.singleAvatar;
  return hasMultipleTask && selectedTaskId && singleAvatar;
}

export default isEditModeForMultipleTask;
