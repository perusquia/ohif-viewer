const ranges = ['min', 'max', 'exact'];

function hasInstructionRange(instruction) {
  return ranges.some(x => instruction.hasOwnProperty(x));
}

export default hasInstructionRange;
