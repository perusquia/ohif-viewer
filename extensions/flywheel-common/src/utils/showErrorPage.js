const showErrorPage = (history, errorMessage, projectId) => {
  history.push({
    pathname: '/error',
    state: { errorMessage, projectId },
  });
};

export default showErrorPage;
