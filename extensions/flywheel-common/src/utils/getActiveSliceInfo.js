import * as cornerstoneUtils from './cornerstoneUtils';
import cornerstoneTools from 'cornerstone-tools';

const getActiveSliceInfo = (index, element) => {
  try {
    if (!element) {
      element = cornerstoneUtils.getEnabledElement(index);
    }
    if (element) {
      const toolState = cornerstoneTools.getToolState(element, 'stack');
      const stackData = toolState.data[0];
      const imageId = stackData.imageIds[stackData.currentImageIdIndex];
      return {
        imageId: imageId,
        sliceNumber: stackData.currentImageIdIndex + 1,
        viewportIndex: index,
      };
    } else {
      return null;
    }
  } catch (error) {
    return undefined;
  }
};

export default getActiveSliceInfo;
