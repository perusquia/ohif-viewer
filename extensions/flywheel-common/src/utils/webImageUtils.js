import { utils } from '@ohif/core';

const { ALL_IMAGE_TYPE } = utils.regularExpressions;

export const isHpForWebImages = hpLayouts => {
  const viewports = hpLayouts?.[0].viewports.flat() || [];
  const isWebImageProtocol = viewports.some(
    viewport => viewport.fileType === 'image'
  );

  return isWebImageProtocol;
};

export const isImageOpenedFromSession = pathName => {
  if (!pathName) {
    return false;
  }
  const index = pathName.lastIndexOf('/') + 1;
  const lastUrlPart = pathName.substring(index);

  return !ALL_IMAGE_TYPE.test(lastUrlPart);
};
