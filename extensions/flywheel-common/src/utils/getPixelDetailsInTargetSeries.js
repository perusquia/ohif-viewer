import cornerstone from 'cornerstone-core';
import csTools from 'cornerstone-tools';
import * as cornerstoneUtils from './cornerstoneUtils';
const imagePointToPatientPoint = csTools.import(
  'util/imagePointToPatientPoint'
);
const projectPatientPointToImagePlane = csTools.import(
  'util/projectPatientPointToImagePlane'
);

/**
 * Get the matching pixel position in image co-ordinate and the slice index
 * in one plane corresponding to the input pixel index and image plane passed
 * @param {*} pixelIndex
 * @param {*} imagePlane
 * @param {*} targetImageIds
 * @returns {Object} - the object with slice index and image point
 */
const getPixelDetailsInTargetSeries = (
  pixelIndex,
  imagePlane,
  targetImageIds
) => {
  const rows = Math.floor(pixelIndex / imagePlane.columns);
  const columns = pixelIndex - rows * imagePlane.columns;
  if (imagePlane?.imagePositionPatient) {
    const patientPos = imagePointToPatientPoint(
      {
        x: columns,
        y: rows,
      },
      imagePlane
    );
    const targetSliceIndex = cornerstoneUtils.getNearestSliceFromImagesIds(
      targetImageIds,
      patientPos
    );
    const targetImagePlane = cornerstone.metaData.get(
      'imagePlaneModule',
      targetImageIds[targetSliceIndex]
    );
    const targetPoint = projectPatientPointToImagePlane(
      patientPos,
      targetImagePlane
    );
    return {
      sliceIndex: targetSliceIndex,
      imagePoint: targetPoint,
    };
  }
  return null;
};

export default getPixelDetailsInTargetSeries;
