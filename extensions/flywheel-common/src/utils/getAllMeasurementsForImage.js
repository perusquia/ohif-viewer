/**
 * It retrieve all the measurements from the measurement collection of the image
 * @param {string} imagePath image path
 * @param {Object} measurements measurement collection map
 * @return {Array} filterToolTypes collection of tool types to filter, if provided then skip all other tool types
 */
const getAllMeasurementsForImage = (
  imagePath,
  measurements,
  filterToolTypes = []
) => {
  let imageMeasurements = [];
  Object.keys(measurements).forEach(measurementKey => {
    if (filterToolTypes.length && !filterToolTypes.includes(measurementKey)) {
      return;
    }
    const measurement = measurements[measurementKey];
    if (!measurement || !measurement.length) {
      return;
    }
    const measure = measurement.filter(x => x.imagePath === imagePath);
    if (measure) {
      imageMeasurements = imageMeasurements.concat(measure);
    }
  });
  return imageMeasurements;
};

export default getAllMeasurementsForImage;
