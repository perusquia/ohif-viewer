import cloneDeep from 'lodash.clonedeep';

import redux from '../redux';
const { staticTools } = redux.selectors;

/**
 * Filter the toolbar button list based on the project configuration(ohif-config.json)
 * @param {Array} toolbarButtons
 * @param {Object} projectConfig
 * @returns
 */
function filterToolbarButtonList(toolbarButtons, projectConfig) {
  if (
    !projectConfig ||
    !projectConfig.toolbar ||
    (projectConfig.toolbar && !Object.keys(projectConfig.toolbar).length)
  ) {
    return toolbarButtons;
  }
  const toolbarConfig = projectConfig.toolbar;
  let iconList = cloneDeep(toolbarButtons);

  toolbarButtons.forEach(button => {
    const label = button.label;
    if (toolbarConfig[label]) {
      let obj = toolbarConfig[label];
      if (obj) {
        let buttons_ = null;
        let filteredButton = [];
        if (obj.except) {
          buttons_ = obj.except;
          if (button.buttons) {
            filteredButton = button.buttons.filter(
              x => !buttons_.includes(x.label) && !staticTools.includes(x.label)
            );
            filteredButton = filterExtraSeparators(filteredButton);
            let toolIndex = iconList.findIndex(
              i => i.label === label && !staticTools.includes(i.label)
            );
            if (toolIndex > -1) {
              iconList[toolIndex] =
                filteredButton.length === 1
                  ? { ...filteredButton[0] }
                  : { ...button, buttons: filteredButton };
            }
          } else if (obj.except.includes(label)) {
            iconList = iconList.filter(
              x => x.label != label && !staticTools.includes(label)
            );
          }
        } else if (obj.only) {
          buttons_ = obj.only;
          if (button.buttons) {
            filteredButton = button.buttons.filter(
              x =>
                !staticTools.includes(button.label) &&
                buttons_.includes(x.label)
            );
            let toolIndex = iconList.findIndex(
              i => i.label === label && !staticTools.includes(i.label)
            );
            const icons = buttons_.filter(
              key =>
                !staticTools.includes(key) &&
                !filteredButton.some(i => i.label === key)
            );
            icons.forEach(key => {
              let buttonIndex = toolbarButtons.findIndex(
                item =>
                  !staticTools.includes(item.label) &&
                  item.buttons &&
                  item.buttons.some(t => t.label === key)
              );
              // if tool is in the child button list
              if (buttonIndex > -1) {
                const tool = toolbarButtons[buttonIndex];
                const itemIndex = tool.buttons.findIndex(
                  item =>
                    !staticTools.includes(tool.label) && item.label === key
                );
                if (itemIndex > -1) {
                  let _filteredButton = tool.buttons.filter(
                    item => item.label === key
                  );
                  if (_filteredButton.length) {
                    filteredButton.push(_filteredButton[0]);
                  }
                  iconList[toolIndex]['buttons'] = [...filteredButton];
                }
              } else {
                buttonIndex = iconList.findIndex(
                  i => i.label === key && !staticTools.includes(i.label)
                );
                if (buttonIndex > -1) {
                  const buttonObj = iconList[buttonIndex];
                  iconList = iconList.filter((item, i) => i != buttonIndex);
                  filteredButton.push({ ...buttonObj });
                }
              }
            });
            if (toolIndex > -1) {
              iconList[toolIndex] =
                filteredButton.length === 1
                  ? { ...filteredButton[0] }
                  : { ...button, buttons: filteredButton };
            }
          }
        }
      }
    }
  });

  iconList = iconList.filter(x => {
    if (!x.buttons) {
      return x;
    }
    if (x.buttons && x.buttons.length) {
      return x;
    }
  });

  iconList = filterExtraSeparators(iconList);
  iconList.forEach(icon => {
    if (icon.buttons) {
      icon.buttons = filterExtraSeparators(icon.buttons);
    }
  });
  return iconList;
}

const filterExtraSeparators = buttons => {
  const filterButtons = removeSeparator(buttons);
  const newFilteredButtons = [];
  filterButtons.reduce((newButtons, x) => {
    if (
      newButtons.length > 0 &&
      newButtons[newButtons.length - 1].label === x.label
    ) {
      return newButtons;
    }
    newButtons.push(x);
    return newButtons;
  }, newFilteredButtons);
  return newFilteredButtons;
};

const removeSeparator = buttons => {
  let filterButtons = [];
  if (
    buttons?.[0]?.label === 'Separator' ||
    (buttons.length - 1 > 0 &&
      buttons?.[buttons.length - 1]?.label === 'Separator')
  ) {
    filterButtons = buttons.filter((x, index) => {
      if (index === 0 && x.label === 'Separator') {
        return false;
      } else if (buttons.length - 1 === index && x.label === 'Separator') {
        return false;
      }
      return true;
    });
    return removeSeparator(filterButtons);
  }
  return buttons;
};

export { filterToolbarButtonList };
