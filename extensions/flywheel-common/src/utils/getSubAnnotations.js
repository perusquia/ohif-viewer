import store from '@ohif/viewer/src/store';
import { getBSliceSettings } from './hasToolLimitReached';

const getSubAnnotations = measurementData => {
  const state = store.getState();
  const bSliceSettings = getBSliceSettings();
  const studyForm = state.flywheel.projectConfig?.studyForm;
  let subAnnotations = [];
  if (!measurementData.isSubForm && studyForm?.components?.length) {
    const question = studyForm.components.find(
      component => component.key === measurementData.questionKey
    );
    if (question?.values?.length > 0) {
      const answer =
        typeof measurementData.answer === 'object'
          ? measurementData.answer.value
          : measurementData.answer;
      const subForm = question.values.find(value => value.value === answer)
        ?.subForm;
      if (subForm) {
        const measurements = state.timepointManager.measurements;
        Object.keys(measurements).forEach(key => {
          measurements[key].forEach(tool => {
            if (
              tool.question === measurementData.questionKey &&
              (!bSliceSettings ||
                tool.sliceNumber === measurementData.sliceNumber) &&
              tool.subFormAnnotationId === measurementData.uuid
            ) {
              tool.measurementId = tool._id;
              subAnnotations.push(tool);
            }
          });
        });
      }
    }
  }
  return subAnnotations;
};

export default getSubAnnotations;
