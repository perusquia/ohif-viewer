import cornerstone from 'cornerstone-core';
import cornerstoneTools from 'cornerstone-tools';
import dcmjs from 'dcmjs';

const { setters, getters } = cornerstoneTools.getModule('segmentation');

export function makeColorLUTAndGetIndex(segMetadata) {
  const { state } = cornerstoneTools.getModule('segmentation');
  const { colorLutTables } = state;
  const colorLUTIndex = getNextColorLUTIndex();

  const { data } = segMetadata;

  if (
    !data.some(
      segment =>
        segment &&
        (segment.ROIDisplayColor || segment.RecommendedDisplayCIELabValue)
    )
  ) {
    // Use default cornerstoneTools colorLUT.
    return 0;
  }

  const colorLUT = [];

  for (let i = 0; i < data.length; i++) {
    const segment = data[i];
    if (!segment) {
      continue;
    }

    const { ROIDisplayColor, RecommendedDisplayCIELabValue } = segment;

    if (RecommendedDisplayCIELabValue) {
      const rgb = dcmjs.data.Colors.dicomlab2RGB(
        RecommendedDisplayCIELabValue
      ).map(x => Math.round(x * 255));

      colorLUT[i] = [...rgb, 255];
    } else if (ROIDisplayColor) {
      colorLUT[i] = [...ROIDisplayColor, 255];
    } else {
      colorLUT[i] = [...colorLutTables[0][i]];
    }
  }

  colorLUT.shift();
  setters.colorLUT(colorLUTIndex, colorLUT);

  return colorLUTIndex;
}

export function getNextLabelmapIndex(firstImageId) {
  const { state } = cornerstoneTools.getModule('segmentation');
  const brushStackState = state.series[firstImageId];

  let labelmapIndex = 1;

  if (brushStackState) {
    const { labelmaps3D } = brushStackState;
    labelmapIndex = labelmaps3D.length || 1;

    for (let i = 1; i < labelmaps3D.length; i++) {
      if (!labelmaps3D[i]) {
        labelmapIndex = i;
        break;
      }
    }
  }

  return labelmapIndex;
}

export function getNextColorLUTIndex() {
  const { state } = cornerstoneTools.getModule('segmentation');
  const { colorLutTables } = state;

  let colorLUTIndex = colorLutTables.length;

  for (let i = 0; i < colorLutTables.length; i++) {
    if (!colorLutTables[i]) {
      colorLUTIndex = i;
      break;
    }
  }

  return colorLUTIndex;
}

export function getImageIdsForDisplaySet(
  studies,
  StudyInstanceUID,
  SeriesInstanceUID,
  displaySetsSource = 'displaySets'
) {
  const study = studies.find(
    study => study.StudyInstanceUID === StudyInstanceUID
  );

  const displaySets = study[displaySetsSource].filter(displaySet => {
    return displaySet.SeriesInstanceUID === SeriesInstanceUID;
  });

  if (displaySets.length > 1) {
    console.warn(
      'More than one display set with the same SeriesInstanceUID. This is not supported yet...'
    );
    // TODO -> We could make check the instance list and see if any match?
    // Do we split the segmentation into two cornerstoneTools segmentations if there are images in both series?
    // ^ Will that even happen?
  }

  const referencedDisplaySet = displaySets[0];

  if (
    referencedDisplaySet.isMultiFrame &&
    referencedDisplaySet.numImageFrames > 1
  ) {
    const imageIds = [];
    for (let i = 0; i < referencedDisplaySet.numImageFrames; i++) {
      imageIds.push(referencedDisplaySet.images[0].getImageId(i));
    }
    return imageIds;
  }

  return referencedDisplaySet.images.map(image => image.getImageId());
}

/**
 * Get the first image id
 * @param {Object} referencedDisplaySet - source display set
 * @param {Array} [studies] - study list
 */
export function getFirstImageId(referencedDisplaySet, studies) {
  const { StudyInstanceUID } = referencedDisplaySet;

  const imageIds = getImageIdsForDisplaySet(
    studies,
    StudyInstanceUID,
    referencedDisplaySet.SeriesInstanceUID
  );
  return imageIds[0];
}

/**
 * Reposition the segments in the label map collections
 * @param {Object} segDisplaySet - segment display set
 * @param {Object} referencedDisplaySet - source display set
 * @param {Array} [studies] - study list
 * @param {number} [labelmapIndex] - label map index
 */
export function rePositionSegment(
  segDisplaySet,
  referencedDisplaySet,
  studies,
  labelmapIndex,
  itemIndex = 0
) {
  const firstImageId = getFirstImageId(referencedDisplaySet, studies);

  const { state } = cornerstoneTools.getModule('segmentation');
  const brushStackState = state.series[firstImageId];
  if (!brushStackState || labelmapIndex === undefined) {
    return;
  }
  const [reOrderedItems] = brushStackState.labelmaps3D.splice(
    segDisplaySet.labelmapIndexes[itemIndex],
    1
  );
  brushStackState.labelmaps3D.splice(labelmapIndex, 0, reOrderedItems);
}

/**
 * Remove the segment data inside the label map on reposition
 * @param {Object} segDisplaySet - segment display set
 * @param {Object} referencedDisplaySet - source display set
 * @param {Array} [studies] - study list
 * @param {number} [curSegIndex] - current position of the segment
 */
export function removeSegment(
  segDisplaySet,
  referencedDisplaySet,
  studies,
  curSegIndex
) {
  let itemIndex = (segDisplaySet.segmentationIndexes || []).findIndex(
    segIndex => segIndex === curSegIndex
  );
  itemIndex = itemIndex < 0 ? 0 : itemIndex;
  const firstImageId = getFirstImageId(referencedDisplaySet, studies);
  const { state } = cornerstoneTools.getModule('segmentation');
  const brushStackState = state.series[firstImageId];
  const labelmapIndex = segDisplaySet.labelmapIndexes[itemIndex];
  const segmentationIndex = segDisplaySet.segmentationIndexes[itemIndex];
  if (!brushStackState || labelmapIndex === undefined) {
    return;
  }
  const labelmap3D = brushStackState.labelmaps3D[labelmapIndex];

  if (!labelmap3D) {
    return;
  } // Delete metadata if present.

  delete labelmap3D.metadata[segmentationIndex];
  const labelmaps2D = labelmap3D.labelmaps2D; // Clear segment's voxels.

  for (let i = 0; i < labelmaps2D.length; i++) {
    const labelmap2D = labelmaps2D[i]; // If the labelmap2D has data, and it contains the segment, delete it.

    if (
      labelmap2D &&
      labelmap2D.segmentsOnLabelmap.includes(segmentationIndex)
    ) {
      const pixelData = labelmap2D.pixelData; // Remove this segment from the list.
      const indexOfSegment = labelmap2D.segmentsOnLabelmap.indexOf(
        segmentationIndex
      );
      labelmap2D.segmentsOnLabelmap.splice(indexOfSegment, 1); // Delete the label for this segment.
      for (let p = 0; p < pixelData.length; p++) {
        if (pixelData[p] === segmentationIndex) {
          pixelData[p] = 0;
        }
      }
    }
  }
  labelmap3D.segmentsHidden.splice(segmentationIndex, 1); // Delete the label for this segment.
  brushStackState.labelmaps3D.splice(labelmapIndex, 1);
  cornerstone.getEnabledElements().forEach(enabledElement => {
    setters.activeLabelmapIndex(enabledElement.element, 0);
  });
  segDisplaySet.segmentationIndexes.splice(itemIndex, 1);
  segDisplaySet.labelmapIndexes.splice(itemIndex, 1);
}

/**
 * Update the segmentation visible status by checking the existing status
 * @param {number} [labelmapIndex] - label map index
 * @param {number} [segmentationIndex] - segmentation index
 * @param {boolean} [isVisible] - segmentation to be visible or not
 */
export async function setSegmentVisibility(
  labelmapIndex,
  segmentationIndex,
  isVisible,
  firstImageId = null
) {
  if (firstImageId) {
    // Offline update
    if (
      isVisible !==
      getters.isSegmentVisibleOffline(
        firstImageId,
        segmentationIndex,
        labelmapIndex
      )
    ) {
      setters.toggleSegmentVisibilityOffline(
        firstImageId,
        segmentationIndex,
        labelmapIndex
      );
    }
  } else {
    cornerstone.getEnabledElements().forEach(enabledElement => {
      if (
        isVisible !==
        getters.isSegmentVisible(
          enabledElement.element,
          segmentationIndex,
          labelmapIndex
        )
      ) {
        setters.toggleSegmentVisibility(
          enabledElement.element,
          segmentationIndex,
          labelmapIndex
        );
      }
    });
  }
}
