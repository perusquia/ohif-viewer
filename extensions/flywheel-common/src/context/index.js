import withAllowedActiveTools from './withAllowedActiveTools';

const contexts = {
  withAllowedActiveTools,
};

export default contexts;
