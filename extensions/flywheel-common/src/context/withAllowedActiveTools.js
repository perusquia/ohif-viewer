import React from 'react';
import { connect } from 'react-redux';
import redux from '../redux';
import _ from 'lodash';

const { selectAllowedActiveTools, selectActiveViewportIndex } = redux.selectors;

const mapStateToProps = (state, props) => {
  const allowedActiveTools =
    state && selectAllowedActiveTools(state, selectActiveViewportIndex(state));
  return {
    allowedActiveTools,
  };
};

const areStatesEqual = (next, prev) => {
  const prevAllowedTools =
    prev && selectAllowedActiveTools(prev, selectActiveViewportIndex(prev));
  const nextAllowedTools =
    next && selectAllowedActiveTools(next, selectActiveViewportIndex(next));
  return _.isEqual(prevAllowedTools, nextAllowedTools);
};

export default function withAllowedActiveTools(Component) {
  return connect(mapStateToProps, null, null, { areStatesEqual })(Component);
}
