const AUTH_LOCAL_STORAGE_TOKEN = 'satellizer_token';

function getAuthToken() {
  const item = localStorage.getItem(AUTH_LOCAL_STORAGE_TOKEN);
  if (!item || !item.startsWith('v1:')) {
    return item || '';
  }
  let [, value, padding] = item.split(':');
  for (let i = 0; i < parseInt(padding, 10); i++) {
    value = '=' + value;
  }
  return _reverse(atob(_reverse(value)));
}

function _reverse(str) {
  return Array.from(str)
    .reverse()
    .join('');
}
/**
 * It returns auth schema string
 */
function getAuthSchema() {
  const schemaAndToken = getAuthToken() || '';
  const parts = schemaAndToken.split(' ');
  return parts.length === 2 ? parts[0] : undefined;
}

/**
 * It returns token string without schema string
 */
function getAuthTokenOnly() {
  const schemaAndToken = getAuthToken() || '';
  const parts = schemaAndToken.split(' ');
  return parts[1] || parts[0];
}

export { getAuthToken, getAuthSchema, getAuthTokenOnly };
