import { DataAdapters } from '../../../utils';
import { log } from '@ohif/core';

export function parseToJson(response) {
  return response.json().then(json => {
    if (response.status >= 400) {
      const error = new Error(json.detail || `HTTP ${response.status}`);
      error.code = response.status;
      throw error;
    }
    return json;
  });
}

export function parseToText(response) {
  return response.text();
}

// move this to common place in case we start using a more robust interceptor strategy
export const getResponseInterceptors = (config = {}) => {
  const responseInterceptors = [];
  if (config.parseTo) {
    let parserMethod;
    switch (config.parseTo) {
      case 'text':
        parserMethod = parseToText;
        break;
      case 'json':
        parserMethod = parseToJson;
        break;
    }

    if (parserMethod) {
      responseInterceptors.push({ responseSuccess: parserMethod });
    } else {
      log.warn(`Api fetch:: There is no parser for type: ${config.parseTo}`);
    }
  } else if (config.parseToJson) {
    responseInterceptors.push({ responseSuccess: parseToJson });
  }

  if (config.adaptToCase) {
    responseInterceptors.push({ responseSuccess: DataAdapters.adaptToCase });
  }

  return responseInterceptors;
};
