import services from './services';
import ApiClient, { utils } from './client';
import * as urls from './urls';

const http = {
  services,
  utils,
  urls,
  ApiClient,
};

export default http;
