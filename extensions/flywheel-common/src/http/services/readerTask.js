import ApiClient from '../client';
import { getUser } from './user';

import {
  GET_ANNOTATIONS_BY_TASK,
  GET_ANNOTATIONS_BY_FILE,
  CREATE_ANNOTATION,
  MODIFY_DELETE_ANNOTATION,
  UPDATE_READER_TASK,
  GET_ROLES,
} from '../urls';

export function getMeasurementsByTaskId(taskId) {
  const urlOptions = {
    url: GET_ANNOTATIONS_BY_TASK,
    urlParams: {
      pathParams: {
        taskId,
      },
    },
  };

  const config = {
    parseToJson: false,
    adaptToCase: true,
  };

  return ApiClient.get(urlOptions, config).then(response => response.json());
}

export function getMeasurementsByFileId(fileId) {
  const urlOptions = {
    url: GET_ANNOTATIONS_BY_FILE,
    urlParams: {
      pathParams: {
        fileId,
      },
    },
  };

  const config = {
    parseToJson: false,
    adaptToCase: true,
  };

  return ApiClient.get(urlOptions, config).then(response => response.json());
}

export function createAnnotation(data) {
  const urlOptions = {
    url: CREATE_ANNOTATION,
  };

  const config = {
    parseToJson: true,
    adaptToCase: true,
  };

  return ApiClient.post(urlOptions, data, config);
}

export function modifyAnnotation(id, data) {
  const urlOptions = {
    url: MODIFY_DELETE_ANNOTATION,
    urlParams: {
      pathParams: {
        id,
      },
    },
  };

  const config = {
    parseToJson: true,
    adaptToCase: true,
  };

  return ApiClient.put(urlOptions, data, config);
}

export function deleteAnnotation(id) {
  const urlOptions = {
    url: MODIFY_DELETE_ANNOTATION,
    urlParams: {
      pathParams: {
        id,
      },
    },
  };

  const config = {
    parseToJson: true,
    adaptToCase: true,
  };

  return ApiClient.delete(urlOptions, config);
}

export function updateReaderTask(taskId, status, info) {
  return getUser().then(user => {
    const urlOptions = {
      url: UPDATE_READER_TASK,
      urlParams: {
        pathParams: {
          taskId,
        },
      },
    };

    const config = {
      parseToJson: true,
      adaptToCase: true,
    };

    if (user.roles.includes('site_admin')) {
      config.isAdmin = true;
    }
    const data = { status, info };

    return ApiClient.put(urlOptions, data, config);
  });
}

export function getRoles() {
  const urlOptions = {
    url: GET_ROLES,
    urlParams: {},
  };

  const config = {
    parseToJson: true,
    adaptToCase: true,
  };

  return ApiClient.get(urlOptions, config);
}
