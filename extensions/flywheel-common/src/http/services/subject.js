import ApiClient from '../client';
import { GET_SUBJECTS, GET_SUBJECTS_ANALYSES } from '../urls';
import { getUser } from './user';

export function listSubjects(projectId, filters = []) {
  return getUser().then(user => {
    const subjectFilters = [`project=${projectId}`, ...filters];

    const urlOptions = {
      url: GET_SUBJECTS,
      urlParams: {
        queryParams: {
          filter: subjectFilters.join(','),
        },
      },
    };

    const config = {
      parseToJson: true, // return response as json
    };

    if (user.roles.includes('site_admin')) {
      config.isAdmin = true;
    }

    return ApiClient.get(urlOptions, config);
  });
}

export function listSubjectAnalyses(subjectId) {
  return getUser().then(user => {
    const urlOptions = {
      url: GET_SUBJECTS_ANALYSES.replace(':subjectId', subjectId),
      urlParams: {
        queryParams: {
          subjectId,
        },
      },
    };

    const config = {
      parseToJson: true, // return response as json
      queryAll: true, // request all records
    };

    if (user.roles.includes('site_admin')) {
      config.isAdmin = true;
    }

    return ApiClient.get(urlOptions, config);
  });
}
