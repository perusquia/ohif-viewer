import ApiClient from '../client';
import { GET_INSTANCE_TAGS } from '../urls';

export function getInstanceTags(projectId, studyId, seriesId, instanceId) {
  const urlOptions = {
    url: GET_INSTANCE_TAGS,
    urlParams: {
      pathParams: {
        projectId,
        studyId,
        seriesId,
        instanceId,
      },
    },
  };

  const config = {
    parseToJson: true, // return response as json
  };

  return ApiClient.get(urlOptions, config);
}
