import csTools from 'cornerstone-tools';

const CornerstonePanTool = csTools.PanTool;

const { state } = csTools.store;
/**
 * Improvement on csTool Pan tool to use FLYW custom implementation
 *
 * @public
 * @class PanTool
 * @memberof Tools
 * @classdesc  Tool for panning the image.
 * @extends CornerstonePanTool
 */
export default class PanTool extends CornerstonePanTool {
  static toolName = 'Pan';

  initializeDefaultCursor(evt, isAdd = true) {
    const cursorTools =
      state?.tools.filter(item => 'BaseCursorTool' === item.name) || [];

    if (cursorTools.length) {
      // Start or finish drawing based on isAdd parameter
      cursorTools.forEach(cursorTool =>
        isAdd
          ? cursorTool.addNewMeasurement?.(evt)
          : cursorTool.finishDrawing?.(evt.detail.element)
      );
    }
  }
}
