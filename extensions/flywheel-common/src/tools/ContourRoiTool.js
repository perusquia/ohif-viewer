import { getEnabledElement } from 'cornerstone-core';
import { getToolState, store } from 'cornerstone-tools';
import { utils } from '@ohif/core';

import TOOL_NAMES from './utils/contourUtils/toolNames';
import interpolate from './utils/contourUtils/freehandInterpolate/interpolate';
import FreehandRoiTool from './FreehandRoiTool';
import updateRelatedContours from './utils/contourUtils/updateRelatedContours';
import getROIContourCollection from './utils/contourUtils/getROIContourCollection.js';
import commonToolUtils from './commonUtils';
const { state } = store;

const { saveActionState } = commonToolUtils;

export default class ContourRoiTool extends FreehandRoiTool {
  constructor(props = {}) {
    const defaultProps = {
      configuration: defaultFreehandConfiguration(),
      name: TOOL_NAMES.CONTOUR_ROI_TOOL,
    };

    const initialProps = Object.assign(defaultProps, props);

    super(initialProps);

    this.configuration.alwaysShowHandles = false;

    cornerstone.events.removeEventListener(
      cornerstone.EVENTS.ELEMENT_ENABLED,
      registerEvents
    );
    cornerstone.events.addEventListener(
      cornerstone.EVENTS.ELEMENT_ENABLED,
      registerEvents
    );
    cornerstone.events.removeEventListener(
      cornerstone.EVENTS.ELEMENT_DISABLED,
      unRegisterEvents
    );
    cornerstone.events.addEventListener(
      cornerstone.EVENTS.ELEMENT_DISABLED,
      unRegisterEvents
    );
  }

  /**
   * Create the measurement data for this tool.
   * @override @public @method
   *
   * @param {object} eventData
   * @returns {object} measurementData
   */
  createNewMeasurement(eventData) {
    const data = super.createNewMeasurement(eventData);
    const enabledElement = getEnabledElement(this.element);
    const { StudyInstanceUID, SeriesInstanceUID } = cornerstone.metaData.get(
      'instance',
      enabledElement.image.imageId
    );

    const measurementData = {
      ...data,
      StudyInstanceUID,
      SeriesInstanceUID,
      ROIContourUid: null,
    };

    return measurementData;
  }

  /**
   * To check the tool name match to this class or an extended tool.
   * @returns {Boolean} true if matching else false
   */
  isToolNameMatching() {
    return this.name === TOOL_NAMES.CONTOUR_ROI_TOOL
      ? true
      : super.isToolNameMatching();
  }

  /**
   * Custom callback for when toolData property is changed.
   *
   * @param  {Object} eventData
   */
  _OnPropertyChange(eventData) {
    const { eventType } = eventData.detail;
    if (eventType === 'Initialize') {
      this.OnInitializeLabel(eventData);
    } else {
      updateRelatedContours(eventData);
    }
  }

  /**
   * Custom callback for when labelling done for roi.
   *
   * @param  {Object} eventData
   */
  OnInitializeLabel(eventData) {
    const { measurementData, operationData } = eventData.detail;
    const element = eventData.currentTarget;
    const toolState = getToolState(element, this.name);
    const isUndoRedoOperation =
      operationData?.type === 'Undo' || operationData?.type === 'Redo';

    if (!toolState) {
      return;
    }
    let referenceContours = [];
    if (!measurementData.ROIContourUid) {
      referenceContours = getROIContourCollection(
        element,
        { key: 'location', value: measurementData.location },
        true
      );
      // Skip other type of annotations with same location
      referenceContours = referenceContours.filter(
        contour => contour.ROIContourUid
      );
      for (let i = 0; i < toolState.data.length; i++) {
        const data = toolState.data[i];

        if (measurementData._id !== data._id) {
          continue;
        }
        if (!measurementData.ROIContourUid) {
          data.ROIContourUid =
            referenceContours[0]?.ROIContourUid || utils.guid();
          measurementData.ROIContourUid = data.ROIContourUid;
          saveActionState(measurementData, 'measurements', 'Add');
        }
        break;
      }
      interpolate([measurementData], element);
    } else if (isUndoRedoOperation) {
      referenceContours = getROIContourCollection(
        element,
        { key: 'ROIContourUid', value: measurementData.ROIContourUid },
        true
      );
      // Skip other type of annotations with same location
      referenceContours = referenceContours.filter(
        contour => !contour.interpolated
      );
      // If undo-redo operation, then find all the non-interpolated one to trigger interpolation.
      const contours =
        referenceContours.length >= 2
          ? referenceContours.slice(0, referenceContours.length - 1)
          : referenceContours;
      interpolate(contours, element);
    }
  }
}

/**
 * Register viewport events for contour roi tool
 * @param {Object} evt - The event.
 */
function registerEvents(evt) {
  if (!evt?.detail?.element) {
    return;
  }
  const eventName =
    TOOL_NAMES.CONTOUR_ROI_TOOL + 'measurementpropertychangeevent';
  evt.detail.element.removeEventListener(eventName, OnPropertyChange);
  evt.detail.element.addEventListener(eventName, OnPropertyChange);
}

/**
 * Un-register viewport events for contour roi tool
 * @param {Object} evt - The event.
 */
function unRegisterEvents(evt) {
  if (!evt?.detail?.element) {
    return;
  }
  const eventName =
    TOOL_NAMES.CONTOUR_ROI_TOOL + 'measurementpropertychangeevent';
  evt.detail.element.removeEventListener(eventName, OnPropertyChange);
}

/**
 * Custom callback for when toolData property is changed.
 *
 * @param  {Object} eventData
 */
function OnPropertyChange(eventData) {
  const { eventType } = eventData.detail;
  const activeTool = state.tools.find(
    tool => tool.name === TOOL_NAMES.CONTOUR_ROI_TOOL
  );
  activeTool?._OnPropertyChange(eventData);
}

function defaultFreehandConfiguration() {
  return {
    mouseLocation: {
      handles: {
        start: {
          highlight: true,
          active: true,
        },
      },
    },
    spacing: 1,
    interpolatedHandleRadius: 0.5,
    interpolatedAlpha: 0.5,
    activeHandleRadius: 3,
    completeHandleRadius: 6,
    completeHandleRadiusTouch: 28,
    alwaysShowHandles: false,
    invalidColor: 'crimson',
    currentHandle: 0,
    currentTool: -1,
  };
}
