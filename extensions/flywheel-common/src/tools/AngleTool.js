import cornerstone from 'cornerstone-core';
import csTools from 'cornerstone-tools';
import * as unitUtils from './unit';

import commonToolUtils from './commonUtils';

import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';
import measurementToolUtils from './utils';
import { getDefaultBoundingBox } from './textBoxContent';
import { shouldEnableAnnotationTool } from './utils/shouldEnableAnnotationTool';

const { setNextPalette } = FlywheelCommonRedux.actions;

const {
  findToolIndex,
  saveActionState,
  upOrEndEventsManager,
} = commonToolUtils;

// set an alias
const CornerstoneAngleTool = csTools.AngleTool;

// Drawing
const getNewContext = csTools.importInternal('drawing/getNewContext');
const draw = csTools.importInternal('drawing/draw');
const setShadow = csTools.importInternal('drawing/setShadow');
const drawHandles = csTools.importInternal('drawing/drawHandles');
const drawLinkedTextBox = csTools.importInternal('drawing/drawLinkedTextBox');
const drawJoinedLines = csTools.importInternal('drawing/drawJoinedLines');

// State
const getToolState = csTools.getToolState;
const toolColors = csTools.toolColors;
const toolStyle = csTools.toolStyle;

// manipulators
const moveHandleNearImagePoint = csTools.importInternal(
  'manipulators/moveHandleNearImagePoint'
);

// Util
const getPixelSpacing = csTools.importInternal('util/getPixelSpacing');

// State
const getModule = csTools.getModule;

/**
 * Improvement on csTool Angle Tool to use FLYW custom measurement label implementation
 * renderToolData method is similar to its original version (csTools)
 *
 * @public
 * @class AngleTool
 * @memberof Tools.Annotation
 * @classdesc Tool for measuring Angle
 * @extends CornerstoneAngleTool
 */
export default class AngleTool extends CornerstoneAngleTool {
  static toolName = 'AngleTool';

  constructor(props = {}) {
    super(props);
    this._endDrawing = this._endDrawing.bind(this);
  }

  createNewMeasurement(eventData) {
    const colorPalette = store.getState().colorPalette;
    const data = super.createNewMeasurement(eventData);
    const color = colorPalette.roiColor.next;
    store.dispatch(setNextPalette());
    return Object.assign({}, data, { color: color });
  }

  pointNearTool(element, data, coords) {
    const isAnnotationToolEnabled = shouldEnableAnnotationTool(element);

    if (data.readonly || !data.visible || !isAnnotationToolEnabled) {
      return false;
    }
    return super.pointNearTool(element, data, coords);
  }

  renderToolData(evt) {
    const eventData = evt.detail;
    const enabledElement = eventData.enabledElement;
    const {
      handleRadius,
      drawHandlesOnHover,
      hideHandlesIfMoving,
      renderDashed,
    } = this.configuration;
    // If we have no toolData for this element, return immediately as there is nothing to do
    const toolData = getToolState(evt.currentTarget, this.name);
    const lineDash = getModule('globalConfiguration').configuration.lineDash;

    if (!toolData) {
      return;
    }

    // We have tool data for this element - iterate over each one and draw it
    const context = getNewContext(eventData.canvasContext.canvas);
    const { image, element } = eventData;
    const { rowPixelSpacing, colPixelSpacing } = getPixelSpacing(image);

    const lineWidth = toolStyle.getToolWidth();

    for (let i = 0; i < toolData.data.length; i++) {
      const data = toolData.data[i];

      if (data.visible === false) {
        continue;
      }

      draw(context, context => {
        setShadow(context, this.configuration);

        // Differentiate the color of activation tool
        let color = data.active
          ? toolColors.getActiveColor()
          : data.color || toolColors.getColorIfActive(data);
        color = unitUtils.convertRgbToRgba(color, data.color);
        color = color.substring(0, color.lastIndexOf(',')) + ', 1)';
        if (measurementToolUtils.hasColorIntensity(data)) {
          color = measurementToolUtils.reduceColorOpacity(color);
        }
        const handleStartCanvas = cornerstone.pixelToCanvas(
          eventData.element,
          data.handles.start
        );
        const handleMiddleCanvas = cornerstone.pixelToCanvas(
          eventData.element,
          data.handles.middle
        );

        const lineOptions = { color };

        if (renderDashed) {
          lineOptions.lineDash = lineDash;
        }

        drawJoinedLines(
          context,
          eventData.element,
          data.handles.start,
          [data.handles.middle, data.handles.end],
          lineOptions
        );

        // Draw the handles
        const handleOptions = {
          color,
          handleRadius,
          drawHandlesIfActive: drawHandlesOnHover,
          hideHandlesIfMoving,
        };

        if (this.configuration.drawHandles) {
          drawHandles(context, eventData, data.handles, handleOptions);
        }

        // Update textbox stats
        if (data.invalidated || data.forceInvalidated) {
          data.forceInvalidated = false;
          if (data.rAngle) {
            this.throttledUpdateCachedStats(image, element, data);
          } else {
            this.updateCachedStats(image, element, data);
          }
        }

        if (data.rAngle) {
          const text = this.textBoxText(data, rowPixelSpacing, colPixelSpacing);

          const distance = 15;

          let textCoords;

          if (!data.handles.textBox.hasMoved) {
            textCoords = {
              x: handleMiddleCanvas.x,
              y: handleMiddleCanvas.y,
            };

            const padding = 5;
            const textWidth = textBoxWidth(context, text, padding);

            if (handleMiddleCanvas.x < handleStartCanvas.x) {
              textCoords.x -= distance + textWidth + 10;
            } else {
              textCoords.x += distance;
            }

            const transform = cornerstone.internal.getTransform(enabledElement);

            transform.invert();

            const coords = transform.transformPoint(textCoords.x, textCoords.y);

            data.handles.textBox.x = coords.x;
            data.handles.textBox.y = coords.y;
          }
          if (unitUtils.isMeasurementOutputHidden()) {
            // Temporary status flag to indicate measurement visibility in
            // viewport, not to be saved in server.
            data.handles.textBox.isVisible = false;
            data.handles.textBox.boundingBox = getDefaultBoundingBox(
              data.handles.textBox
            );
          } else {
            drawLinkedTextBox(
              context,
              eventData.element,
              data.handles.textBox,
              text,
              data.handles,
              textBoxAnchorPoints,
              color,
              lineWidth,
              0,
              true
            );
          }
        }
      });
    }

    function textBoxAnchorPoints(handles) {
      return [handles.start, handles.middle, handles.end];
    }

    function textBoxWidth(context, text, padding) {
      const font = csTools.textStyle.getFont();
      const origFont = context.font;

      if (font && font !== origFont) {
        context.font = font;
      }
      const width = context.measureText(text).width;

      if (font && font !== origFont) {
        context.font = origFont;
      }

      return width + 2 * padding;
    }
  }

  /**
   * Custom callback for when a handle is selected.
   * @method handleSelectedCallback
   * @memberof Tools.Base.BaseAnnotationTool
   *
   * @param  {*} evt    -
   * @param  {*} toolData   -
   * @param  {*} handle - The selected handle.
   * @param  {String} interactionType -
   * @returns {void}
   */
  handleSelectedCallback(evt, toolData, handle, interactionType = 'mouse') {
    const eventData = evt.detail;
    const { element } = eventData;
    const isAnnotationToolEnabled = shouldEnableAnnotationTool(element);

    if (!toolData.readonly && toolData.visible && isAnnotationToolEnabled) {
      moveHandleNearImagePoint(evt, this, toolData, handle, interactionType);
      this._initializeDrawing(evt, handle, interactionType);
    }
  }

  /**
   * Custom callback for when a tool is selected.
   *
   * @method toolSelectedCallback
   * @memberof Tools.Base.BaseAnnotationTool
   *
   * @param  {*} evt
   * @param  {*} annotation
   * @param  {string} [interactionType=mouse]
   * @returns {void}
   */
  toolSelectedCallback(evt, annotation, interactionType = 'mouse') {
    if (!annotation.readonly && annotation.visible) {
      super.toolSelectedCallback(evt, annotation, interactionType);
      this._initializeDrawing(evt, annotation.handles?.start, interactionType);
    }
  }

  /**
   * Initialize the drawing loop when tool is active and a click event happens.
   *
   * @private
   * @param {Object} evt - The event.
   * @param  {*} handle - Selected handle or any of the handle.
   * @param  {string} [interactionType=mouse]
   * @returns {void}
   */
  _initializeDrawing(evt, handle, interactionType = 'mouse') {
    const { element } = evt.detail;
    const currentTool = findToolIndex(element, this.name, handle);
    if (currentTool >= 0) {
      this.configuration.currentTool = currentTool;
      this._startDrawing(evt);
      upOrEndEventsManager.registerHandler(
        element,
        interactionType,
        this._endDrawing
      );
    }
  }

  /**
   * Beginning of drawing loop when tool is active and a click event happens.
   *
   * @private
   * @param {Object} evt - The event.
   * @returns {void}
   */
  _startDrawing(evt) {
    const { element } = evt.detail;
    const toolState = getToolState(element, this.name);
    const data = toolState.data[this.configuration.currentTool];
    saveActionState(data, 'measurements', 'Modify', true, false);
  }

  /**
   * Ends the active drawing loop and completes the polygon.
   *
   * @private
   * @param {Object} evt - The event.
   * @returns {void}
   */
  _endDrawing(evt) {
    const { element } = evt.detail;
    const interactionType = upOrEndEventsManager.getCurrentInteractionType(evt);
    upOrEndEventsManager.removeHandler(
      element,
      interactionType,
      this._endDrawing
    );
    const toolState = getToolState(element, this.name);
    const data = toolState.data[this.configuration.currentTool];
    saveActionState(data, 'measurements', 'Modify', false, true);
    this.configuration.currentTool = -1;
  }

  /** Ends the active drawing loop forcefully.
   *
   * @public
   * @param {Object} element - The element on which the roi is being drawn.
   * @returns {null}
   */
  finishDrawing(element) {
    const evt = { detail: { element } };
    if (this.configuration.currentTool >= 0) {
      this._endDrawing(evt);
    }
  }

  textBoxText(data, rowPixelSpacing, colPixelSpacing) {
    const suffix = !rowPixelSpacing || !colPixelSpacing ? ' (isotropic)' : '';
    const str = '00B0'; // Degrees symbol
    const rAngle = data.rAngle || '';
    return rAngle.toString() + String.fromCharCode(parseInt(str, 16)) + suffix;
  }

  getTextInfo(measurementData, data = null) {
    if (measurementData && data) {
      const { rowPixelSpacing, colPixelSpacing } = getPixelSpacing(data.image);
      const text = this.textBoxText(
        measurementData,
        rowPixelSpacing,
        colPixelSpacing
      );
      if (text) {
        return [`rAngle : ${text}`];
      }
    }
    return [];
  }
}
