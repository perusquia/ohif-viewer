import cornerstone from 'cornerstone-core';
import csTools from 'cornerstone-tools';
import SmartBrushTool from './SmartBrushTool';
import Redux from '../redux';
import store from '@ohif/viewer/src/store';
import { getSmartCTRangeList } from '../utils';

const { setSmartCTRange } = Redux.actions;
const segmentationModule = csTools.getModule('segmentation');
/**
 * @public
 * @class AutoSmartBrushTool
 * @memberof Tools.Brush
 * @classdesc Tool for drawing segmentations on an image.
 * @extends SmartBrush
 */
export default class AutoSmartBrushTool extends SmartBrushTool {
  static toolName = 'AutoSmartBrushTool';

  constructor(props = {}) {
    super(props);
    this.name = 'AutoSmartBrush';
  }

  /**
   *
   * @param {Object} evt
   * @param {Array} pointerArray - contains x, y coords under brush
   * @param {Object} brushCenter
   * @param {Number} radius
   * @returns {min: Number, max: Number} - density range covering density value under the brush center
   */
  getWindowRange(evt, pointerArray, brushCenter, radius) {
    const eventData = evt.detail;
    const activeLabelmapIndex = segmentationModule.getters.activeLabelmapIndex(
      eventData.element
    );

    if (activeLabelmapIndex > 0) {
      return super.getWindowRange(evt, pointerArray, brushCenter, radius);
    }

    const density = this.getDensity(
      eventData.image,
      eventData.element,
      pointerArray,
      brushCenter,
      radius
    );
    const selectedRange = this.getRangeFromDensity(density);
    if (!selectedRange) {
      return super.getWindowRange(evt, pointerArray, brushCenter, radius);
    }

    return selectedRange;
  }

  /**
   * to get the density based on first click
   * @param {Object} image
   * @param {HTMLElement} element
   * @param {Array} pointerArray
   * @param {Object} brushCenter
   * @param {Number} radius
   * @param {Object} windowRange
   * @returns density
   */
  getDensity(image, element, pointerArray, brushCenter, radius) {
    let density = null;
    if (!image.color) {
      const brushCenterCoords = {
        x: Math.floor(brushCenter.x),
        y: Math.floor(brushCenter.y),
      };
      const storedPixels = cornerstone.getStoredPixels(
        element,
        brushCenterCoords.x,
        brushCenterCoords.y,
        1,
        1
      );

      if (!density) {
        density = storedPixels[0] * image.slope + image.intercept;
      }
    }

    return density;
  }

  /**
   * to get the density range from store
   * @param {Number} density
   * @returns {Object}
   */
  getRangeFromDensity(density) {
    const smartCTRanges = getSmartCTRangeList();

    if (smartCTRanges && smartCTRanges.length) {
      let range = smartCTRanges.find(
        item => item.range.min <= density && item.range.max > density
      );
      if (range) {
        this.setSelectedRange(range);
        return range;
      }
      // If density does not fall in any of the available ranges, use the closest one
      const minDistances = smartCTRanges.map(item =>
        Math.min(
          Math.abs(item.range.min - density),
          Math.abs(item.range.max - density)
        )
      );
      const minDistance = Math.min.apply(null, minDistances);
      const index = minDistances.indexOf(minDistance);
      if (index > -1) {
        // to update smartCT range selection in UI.
        this.setSelectedRange(smartCTRanges[index]);
        return smartCTRanges[index];
      }
    }
    return null;
  }

  setSelectedRange(obj) {
    store.dispatch(setSmartCTRange(obj));
  }
}
