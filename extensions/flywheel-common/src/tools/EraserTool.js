import csTools from 'cornerstone-tools';
import upOrEndEventsManager from './commonUtils/manageUpOrEndEvents';
import { shouldEnablePreMouseDownCallback } from './utils/shouldOverrideActiveToolMouseDown';

// set an alias
const CornerstoneEraserTool = csTools.EraserTool;
const triggerEvent = csTools.importInternal('util/triggerEvent');
/**
 * @public
 * @class EraserTool
 * @memberof Tools
 *
 * @classdesc Tool for deleting the data of other Annotation Tools.
 * @extends CornerstoneEraserTool
 */
export default class EraserTool extends CornerstoneEraserTool {
  _deleteAllNearbyTools(evt) {
    if (shouldEnablePreMouseDownCallback(evt)) {
      return;
    }
    const element = evt.detail.element;
    const { state } = csTools.store;
    const interactionType = upOrEndEventsManager.getCurrentInteractionType(evt);
    let data = [];
    state.tools.forEach(tool => {
      if (tool.element === element) {
        const toolState = csTools.getToolState(element, tool.name);
        if (toolState) {
          const annotations = toolState.data.slice().reverse();
          annotations.forEach(annotation => {
            const shouldRemoveToolState = shouldRemoveTool(
              evt,
              tool,
              annotation,
              interactionType
            );
            if (shouldRemoveToolState) {
              data.push({
                ...annotation,
                ...{ measurementId: annotation?._id },
              });
            }
          });
        }
      }
    });
    if (data?.length) {
      triggerEvent(element, `fwCornerstoneMeasurementDeleting`, {
        measurement: data,
      });
    }

    evt.preventDefault();
    evt.stopPropagation();
    return true;
  }
}

function shouldRemoveTool(event, tool, data, interactionType) {
  const coords = event.detail.currentPoints.canvas;
  const element = event.detail.element;
  const shouldRemoveToolState =
    (typeof tool.pointNearTool === 'function' &&
      tool.pointNearTool(element, data, coords, interactionType)) ||
    (typeof tool.pointInsideTool === 'function' &&
      tool.pointInsideTool(element, data, coords, interactionType));
  return shouldRemoveToolState;
}
