import cornerstone from 'cornerstone-core';
import cornerstoneMath from 'cornerstone-math';
import csTools from 'cornerstone-tools';
import OHIF from '@ohif/core';
import store from '@ohif/viewer/src/store';
import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';

import commonToolUtils from './commonUtils';
import * as TextBoxContentUtils from './textBoxContent';
import * as unitUtils from './unit';
import measurementToolUtils from './utils';
import {
  isPointInsideRect,
  getRectangleData,
} from './utils/RectangleIntersect';
import { shouldEnableAnnotationTool } from './utils/shouldEnableAnnotationTool';

const { setNextPalette } = FlywheelCommonRedux.actions;
const { selectActiveTool } = FlywheelCommonRedux.selectors;

const {
  findToolIndex,
  saveActionState,
  upOrEndEventsManager,
} = commonToolUtils;

const { state } = csTools.store;

// set an alias
const CornerstoneRectangleRoiTool = csTools.RectangleRoiTool;

// Drawing
const getNewContext = csTools.importInternal('drawing/getNewContext');
const draw = csTools.importInternal('drawing/draw');
const setShadow = csTools.importInternal('drawing/setShadow');
const drawRect = csTools.importInternal('drawing/drawRect');
const drawHandles = csTools.importInternal('drawing/drawHandles');
const drawLinkedTextBox = csTools.importInternal('drawing/drawLinkedTextBox');

// manipulators
const triggerEvent = csTools.importInternal('util/triggerEvent');
const moveHandleNearImagePoint = csTools.importInternal(
  'manipulators/moveHandleNearImagePoint'
);
const getHandleNearImagePoint = csTools.importInternal(
  'manipulators/getHandleNearImagePoint'
);

// State
const getToolState = csTools.getToolState;
const toolColors = csTools.toolColors;
const toolStyle = csTools.toolStyle;

// Util
const getROITextBoxCoords = csTools.importInternal('util/getROITextBoxCoords');
const getPixelSpacing = csTools.importInternal('util/getPixelSpacing');

const { rectangleRoiCursor } = csTools.importInternal('tools/cursors');

// State
const getModule = csTools.getModule;

/**
 * Improvement on csTool ROI tool to use FLYW custom textBox implementation
 * renderToolData method is similar to its original version (csTools)
 *
 * @public
 * @class RectangleRoiTool
 * @memberof Tools.Annotation
 * @classdesc Tool for selecting a region of an image to crop and create a new one
 * @extends CornerstoneRectangleRoiTool
 */
export default class RectangleRoiTool extends CornerstoneRectangleRoiTool {
  static toolName = 'RectangleRoiTool';

  constructor(props = {}) {
    super(props);
    this._endDrawing = this._endDrawing.bind(this);
  }

  createNewMeasurement(eventData) {
    const colorPalette = store.getState().colorPalette;
    const data = super.createNewMeasurement(eventData);
    const color = colorPalette.roiColor.next;
    store.dispatch(setNextPalette());
    return Object.assign({}, data, { color: color });
  }

  initializeDefaultCursor(evt, isAdd = true) {
    const cursorTools =
      state?.tools.filter(item => 'BaseCursorTool' === item.name) || [];

    if (cursorTools.length) {
      // Start or finish drawing based on isAdd parameter
      cursorTools.forEach(cursorTool =>
        isAdd
          ? cursorTool.addNewMeasurement?.(evt)
          : cursorTool.finishDrawing?.(evt.detail.element)
      );
    }
  }

  // Overwrite method to use custom Unit strategy
  renderToolData(evt) {
    const toolData = getToolState(evt.currentTarget, this.name);

    if (!toolData) {
      return;
    }

    const eventData = evt.detail;
    const { image, element } = eventData;
    const lineWidth = toolStyle.getToolWidth();
    const lineDash = getModule('globalConfiguration').configuration.lineDash;
    const {
      handleRadius,
      drawHandlesOnHover,
      renderDashed,
    } = this.configuration;
    const context = getNewContext(eventData.canvasContext.canvas);
    const { rowPixelSpacing, colPixelSpacing } = getPixelSpacing(image);

    // Meta
    const seriesModule =
      cornerstone.metaData.get('generalSeriesModule', image.imageId) || {};

    // Pixel Spacing
    const modality = seriesModule.modality;
    const hasPixelSpacing = rowPixelSpacing && colPixelSpacing;

    const isAnyActiveData = toolData.data.find(data => data.active) || false;
    this.initializeDefaultCursor(evt, !isAnyActiveData);

    draw(context, context => {
      // If we have tool data for this element - iterate over each set and draw it
      for (let i = 0; i < toolData.data.length; i++) {
        const data = toolData.data[i];

        if (data.visible === false) {
          continue;
        }

        let color = data.active
          ? toolColors.getActiveColor()
          : data.color || toolColors.getColorIfActive(data);
        color = unitUtils.convertRgbToRgba(color, data.color);

        let fillStyle = color;

        color = color.substring(0, color.lastIndexOf(',')) + ', 1)';
        if (measurementToolUtils.hasColorIntensity(data)) {
          color = measurementToolUtils.reduceColorOpacity(color);
          fillStyle = measurementToolUtils.reduceColorOpacity(fillStyle);
        }

        const handleOptions = {
          color,
          handleRadius,
          drawHandlesIfActive: drawHandlesOnHover,
        };

        setShadow(context, this.configuration);

        const rectOptions = { color, fillStyle };

        if (renderDashed) {
          rectOptions.lineDash = lineDash;
        }

        // Draw
        drawRect(
          context,
          element,
          data.handles.start,
          data.handles.end,
          rectOptions,
          'pixel',
          data.handles.initialRotation
        );

        if (this.configuration.drawHandles) {
          drawHandles(context, eventData, data.handles, handleOptions);
        }

        // Update textbox stats
        if (data.invalidated || data.forceInvalidated) {
          data.forceInvalidated = false;
          if (data.cachedStats) {
            this.throttledUpdateCachedStats(image, element, data);
          } else {
            this.updateCachedStats(image, element, data);
          }
        }

        // Default to textbox on right side of ROI
        if (!data.handles.textBox.hasMoved) {
          const defaultCoords = getROITextBoxCoords(
            eventData.viewport,
            data.handles
          );

          Object.assign(data.handles.textBox, defaultCoords);
        }

        const textBoxAnchorPoints = handles =>
          TextBoxContentUtils.findTextBoxAnchorPoints(
            handles.start,
            handles.end
          );

        const textBoxContent = TextBoxContentUtils.createTextBoxContent(
          context,
          image.color,
          data.cachedStats,
          modality,
          hasPixelSpacing,
          this.configuration
        );

        data.unit = unitUtils.getUnitByModality(
          modality,
          this.configuration.showHounsfieldUnits
        );

        if (unitUtils.isRoiOutputHidden()) {
          // Temporary status flag to indicate measurement visibility in
          // viewport, not to be saved in server.
          data.handles.textBox.isVisible = false;
          data.handles.textBox.boundingBox = TextBoxContentUtils.getDefaultBoundingBox(
            data.handles.textBox
          );
        } else {
          drawLinkedTextBox(
            context,
            element,
            data.handles.textBox,
            textBoxContent,
            data.handles,
            textBoxAnchorPoints,
            color,
            lineWidth,
            10,
            true
          );
        }
      }
    });
  }

  pointNearTool(
    element,
    data,
    coords,
    interactionType,
    mouseProximity = state.handleRadius || 6
  ) {
    const isAnnotationToolEnabled = shouldEnableAnnotationTool(element);

    if (data.readonly || !data.visible || !isAnnotationToolEnabled) {
      return;
    }

    const hasStartAndEndHandles =
      data && data.handles && data.handles.start && data.handles.end;
    const validParameters = hasStartAndEndHandles;

    if (!validParameters) {
      logger.warn(
        `invalid parameters supplied to tool ${this.name}'s pointNearTool`
      );
    }

    if (!validParameters || data.visible === false) {
      return false;
    }

    const handleNearImagePoint = getHandleNearImagePoint(
      element,
      data.handles,
      coords,
      mouseProximity
    );

    if (handleNearImagePoint) {
      return true;
    }

    const distance = interactionType === 'mouse' ? mouseProximity : 25;
    const startCanvas = cornerstone.pixelToCanvas(element, data.handles.start);
    const endCanvas = cornerstone.pixelToCanvas(element, data.handles.end);

    const rect = {
      left: Math.min(startCanvas.x, endCanvas.x),
      top: Math.min(startCanvas.y, endCanvas.y),
      width: Math.abs(startCanvas.x - endCanvas.x),
      height: Math.abs(startCanvas.y - endCanvas.y),
    };

    const distanceToPoint = cornerstoneMath.rect.distanceToPoint(rect, coords);

    const isHighlight = distanceToPoint <= distance;
    const currentTool = selectActiveTool(store.getState());
    return !isHighlight && currentTool === 'Eraser'
      ? this.pointInsideTool(element, data, coords)
      : isHighlight;
  }

  pointInsideTool(element, data, coords) {
    const isAnnotationToolEnabled = shouldEnableAnnotationTool(element);

    if (data.readonly || !data.visible || !isAnnotationToolEnabled) {
      return;
    }
    const hasStartAndEndHandles =
      data && data.handles && data.handles.start && data.handles.end;

    if (!hasStartAndEndHandles || data.visible === false) {
      return false;
    }

    const { start, end, initialRotation } = data.handles;
    const point = cornerstone.canvasToPixel(element, coords);
    const imageId = OHIF.measurements.getImageIdForImagePath(data.imagePath);
    const imagePlane = cornerstone.metaData.get('imagePlaneModule', imageId);
    const imageDimension = { x: imagePlane.columns, y: imagePlane.rows };
    const { points } = getRectangleData(
      start,
      end,
      imageDimension,
      initialRotation
    );
    const isPointInsideTool = isPointInsideRect(
      points,
      point,
      imageDimension,
      initialRotation
    );
    return isPointInsideTool;
  }
  /**
   * Custom callback for when a handle is selected.
   * @method handleSelectedCallback
   * @memberof Tools.Base.BaseAnnotationTool
   *
   * @param  {*} evt    -
   * @param  {*} toolData   -
   * @param  {*} handle - The selected handle.
   * @param  {String} interactionType -
   * @returns {void}
   */
  handleSelectedCallback(evt, toolData, handle, interactionType = 'mouse') {
    const eventData = evt.detail;
    const { element } = eventData;
    const isAnnotationToolEnabled = shouldEnableAnnotationTool(element);

    if (!toolData.readonly && toolData.visible && isAnnotationToolEnabled) {
      moveHandleNearImagePoint(evt, this, toolData, handle, interactionType);
      this._initializeDrawing(evt, handle, interactionType);
    }
  }

  /**
   * Custom callback for when a tool is selected.
   *
   * @method toolSelectedCallback
   * @memberof Tools.Base.BaseAnnotationTool
   *
   * @param  {*} evt
   * @param  {*} annotation
   * @param  {string} [interactionType=mouse]
   * @returns {void}
   */
  toolSelectedCallback(evt, annotation, interactionType = 'mouse') {
    if (!annotation.readonly && annotation.visible) {
      super.toolSelectedCallback(evt, annotation, interactionType);
      this._initializeDrawing(evt, annotation.handles?.start, interactionType);
    }
  }

  /**
   * Initialize the drawing loop when tool is active and a click event happens.
   *
   * @private
   * @param {Object} evt - The event.
   * @param  {*} handle - Selected handle or any of the handle.
   * @param  {string} [interactionType=mouse]
   * @returns {void}
   */
  _initializeDrawing(evt, handle, interactionType = 'mouse') {
    const { element } = evt.detail;
    const currentTool = findToolIndex(element, this.name, handle);
    if (currentTool >= 0) {
      this.configuration.currentTool = currentTool;
      this._startDrawing(evt);
      upOrEndEventsManager.registerHandler(
        element,
        interactionType,
        this._endDrawing
      );
    }
  }

  /**
   * Beginning of drawing loop when tool is active and a click event happens.
   *
   * @private
   * @param {Object} evt - The event.
   * @returns {void}
   */
  _startDrawing(evt) {
    const { element } = evt.detail;
    const toolState = getToolState(element, this.name);
    const data = toolState.data[this.configuration.currentTool];
    saveActionState(data, 'measurements', 'Modify', true, false);
  }

  /**
   * Ends the active drawing loop and completes the polygon.
   *
   * @private
   * @param {Object} evt - The event.
   * @returns {void}
   */
  _endDrawing(evt) {
    const { element } = evt.detail;
    const interactionType = upOrEndEventsManager.getCurrentInteractionType(evt);
    upOrEndEventsManager.removeHandler(
      element,
      interactionType,
      this._endDrawing
    );
    const toolState = getToolState(element, this.name);
    const data = toolState.data[this.configuration.currentTool];
    saveActionState(data, 'measurements', 'Modify', false, true);
    this.configuration.currentTool = -1;

    this.fireModifiedEvent(element, data);
  }

  /** Ends the active drawing loop forcefully.
   *
   * @public
   * @param {Object} element - The element on which the roi is being drawn.
   * @returns {null}
   */
  finishDrawing(element) {
    const evt = { detail: { element } };
    if (this.configuration.currentTool >= 0) {
      this._endDrawing(evt);
    }
  }

  /**
   * Fire MEASUREMENT_MODIFIED event on provided element
   * @param {any} element which freehand data has been modified
   * @param {any} measurementData the measurment data
   * @returns {void}
   */
  fireModifiedEvent(element, measurementData) {
    const eventType = csTools.EVENTS.MEASUREMENT_MODIFIED;
    const eventData = {
      toolName: this.name,
      toolType: this.name, // Deprecation notice: toolType will be replaced by toolName
      element,
      measurementData,
    };

    triggerEvent(element, eventType, eventData);
  }

  getTextInfo(measurementData, data = null) {
    if (data) {
      // Meta
      const seriesModule =
        cornerstone.metaData.get('generalSeriesModule', data.image.imageId) ||
        {};

      // Pixel Spacing
      const modality = seriesModule.modality;

      const { rowPixelSpacing, colPixelSpacing } = getPixelSpacing(data.image);

      const hasPixelSpacing = rowPixelSpacing && colPixelSpacing;
      return TextBoxContentUtils.getTextBoxContent(
        data.image.color,
        measurementData.cachedStats,
        modality,
        hasPixelSpacing,
        this.configuration
      );
    }
    return [];
  }
}
