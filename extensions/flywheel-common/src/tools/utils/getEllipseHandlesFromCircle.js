import csTools from 'cornerstone-tools';
import cornerstoneMath from 'cornerstone-math';
import { getFallbackImagePlane } from '../../utils/cornerstoneUtils';

const imagePointToPatientPoint = csTools.import(
  'util/imagePointToPatientPoint'
);
const projectPatientPointToImagePlane = csTools.import(
  'util/projectPatientPointToImagePlane'
);

/**
 * Return the top left and bottom right corner points of circle in image coordinate system
 * similar to handles of an ellipse.
 *
 * @param {String} imageId Image id
 * @param {Object} handles Center and circumference points of circle in image coordinates
 *
 * @returns {Object} Return top left and bottom right corner points of circle in image coordinate.
 */
export default function getEllipseHandlesFromCircle(imageId, handles) {
  const imagePlane = getFallbackImagePlane(imageId);
  const { Vector3 } = cornerstoneMath;
  const centerPatientPoint = imagePointToPatientPoint(
    handles.start,
    imagePlane
  );
  const endPatientPoint = imagePointToPatientPoint(handles.end, imagePlane);
  const patientPointRadius = centerPatientPoint.distanceTo(endPatientPoint);
  const horizAxis = new Vector3(
    imagePlane.rowCosines[0],
    imagePlane.rowCosines[1],
    imagePlane.rowCosines[2]
  ).normalize();
  const vertAxis = new Vector3(
    imagePlane.columnCosines[0],
    imagePlane.columnCosines[1],
    imagePlane.columnCosines[2]
  ).normalize();
  let topLeftDelta = horizAxis.clone().multiplyScalar(-patientPointRadius);
  let bottomRightDelta = horizAxis.clone().multiplyScalar(patientPointRadius);

  topLeftDelta = topLeftDelta.add(
    vertAxis.clone().multiplyScalar(-patientPointRadius)
  );
  bottomRightDelta = bottomRightDelta.add(
    vertAxis.clone().multiplyScalar(patientPointRadius)
  );

  const topLeftPatientPoint = centerPatientPoint.clone().add(topLeftDelta);
  const bottomRightPatientPoint = centerPatientPoint
    .clone()
    .add(bottomRightDelta);

  const topLeftImagePoint = projectPatientPointToImagePlane(
    topLeftPatientPoint,
    imagePlane
  );
  const bottomRightImagePoint = projectPatientPointToImagePlane(
    bottomRightPatientPoint,
    imagePlane
  );
  return { topLeftImagePoint, bottomRightImagePoint };
}
