export function clipToBoxInSameOrientation(point, pointOnLine, box) {
  let newPoint = _.cloneDeep(point);
  newPoint.x = Math.min(Math.max(point.x, 0), box.width);

  if (newPoint.x !== point.x) {
    newPoint.y =
      pointOnLine.y +
      ((point.y - pointOnLine.y) / (point.x - pointOnLine.x)) *
        (newPoint.x - pointOnLine.x);
    point.x = newPoint.x;
    point.y = newPoint.y;
  }

  newPoint.y = Math.min(Math.max(point.y, 0), box.height);
  if (newPoint.y !== point.y) {
    newPoint.x =
      pointOnLine.x +
      ((point.x - pointOnLine.x) / (point.y - pointOnLine.y)) *
        (newPoint.y - pointOnLine.y);
    point.x = newPoint.x;
    point.y = newPoint.y;
  }
}

export function shiftToBoxInSameOrientation(pointOut, points, box) {
  let adjustX = 0;
  let adjustY = 0;
  const handleX = pointOut.x;
  const handleY = pointOut.y;
  if (handleX > box.width) {
    adjustX = box.width - handleX;
  } else if (handleX < 0) {
    adjustX = 0 - handleX;
  }
  if (handleY > box.height) {
    adjustY = box.height - handleY;
  } else if (handleY < 0) {
    adjustY = 0 - handleY;
  }
  shiftPoints(points, adjustX, adjustY);
}

export function shiftPoints(points, adjustX, adjustY) {
  Object.keys(points).forEach(key => {
    points[key].x += adjustX;
    points[key].y += adjustY;
  });
}
