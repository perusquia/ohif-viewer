import store from '@ohif/viewer/src/store';
import { getReadOnlyStatus } from './checkReadOnlyStatus';
import getActiveSliceInfo from '../../utils/getActiveSliceInfo';
import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';

export const shouldEnableAnnotationTool = element => {
  const projectConfig = FlywheelCommonUtils.getCondensedProjectConfig();
  const sliceSettings = projectConfig?.bSlices?.settings;
  const isReadOnlyViewport = getReadOnlyStatus(element);
  let enableAnnotationTool = true;
  const state = store.getState();
  const showStudyForm = FlywheelCommonUtils.shouldShowStudyForm(
    state?.flywheel?.projectConfig,
    state,
    window.location
  );

  if (sliceSettings && showStudyForm) {
    const sliceNumberKey = getActiveSliceInfo(-1, element).sliceNumber;
    if (
      (sliceSettings?.[sliceNumberKey] && isReadOnlyViewport) ||
      !sliceSettings?.[sliceNumberKey]
    ) {
      enableAnnotationTool = false;
    }
  } else if (isReadOnlyViewport) {
    enableAnnotationTool = false;
  }

  return enableAnnotationTool;
};
