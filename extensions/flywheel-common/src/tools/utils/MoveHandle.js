// Reused existing logic in cornerstone tools[MoveHandle.js].
// Changes made to restrict orientation change of length tool once it has parallels.

import cornerstone from 'cornerstone-core';
import csTools from 'cornerstone-tools';
import getActiveTool from './getActiveTool';
import { clipToBoxInSameOrientation } from './adjustPoints';

const EVENTS = csTools.EVENTS;
const anyHandlesOutsideImage = csTools.importInternal(
  'manipulators/anyHandlesOutsideImage'
);
const removeToolState = csTools.removeToolState;
const triggerEvent = csTools.importInternal('util/triggerEvent');
const clipToBox = csTools.importInternal('util/clipToBox');
const BaseAnnotationTool = csTools.importInternal('base/BaseAnnotationTool');
const { state } = csTools.store;

const manipulatorStateModule = csTools.getModule('manipulatorState');

const runAnimation = {
  value: false,
};

const _dragEvents = {
  mouse: [EVENTS.MOUSE_DRAG],
  touch: [EVENTS.TOUCH_DRAG],
};

const _upOrEndEvents = {
  mouse: [EVENTS.MOUSE_UP, EVENTS.MOUSE_CLICK],
  touch: [
    EVENTS.TOUCH_END,
    EVENTS.TOUCH_DRAG_END,
    EVENTS.TOUCH_PINCH,
    EVENTS.TOUCH_PRESS,
    EVENTS.TAP,
  ],
};

// Average pixel width of index finger is 45-57 pixels
// https://www.smashingmagazine.com/2012/02/finger-friendly-design-ideal-mobile-touchscreen-target-sizes/
const FINGER_DISTANCE = -57;

/**
 * Move the provided handle
 *
 * @public
 * @method moveHandle
 * @memberof Manipulators
 *
 * @param {*} evtDetail
 * @param {*} toolName
 * @param {*} annotation
 * @param {*} handle
 * @param {*} [options={}]
 * @param {Boolean}  [options.deleteIfHandleOutsideImage]
 * @param {Boolean}  [options.preventHandleOutsideImage]
 * @param {*} [interactionType=mouse]
 * @param {function} doneMovingCallback
 * @returns {undefined}
 */
export default function moveHandle(
  evtDetail,
  toolName,
  annotation,
  handle,
  options = {},
  interactionType = 'mouse',
  doneMovingCallback
) {
  // Use global defaults, unless overidden by provided options
  options = Object.assign(
    {
      deleteIfHandleOutsideImage: state.deleteIfHandleOutsideImage,
      preventHandleOutsideImage: state.preventHandleOutsideImage,
    },
    options
  );

  const element = evtDetail.element;
  const dragHandler = _dragHandler.bind(
    this,
    toolName,
    annotation,
    handle,
    options,
    interactionType
  );
  // So we don't need to inline the entire `upOrEndHandler` function
  const upOrEndHandler = () => {
    _upOrEndHandler(
      toolName,
      evtDetail,
      annotation,
      handle,
      options,
      interactionType,
      {
        dragHandler,
        upOrEndHandler,
      },
      doneMovingCallback
    );
  };

  manipulatorStateModule.setters.addActiveManipulatorForElement(
    element,
    _cancelEventHandler.bind(
      null,
      toolName,
      evtDetail,
      annotation,
      handle,
      options,
      interactionType,
      {
        dragHandler,
        upOrEndHandler,
      },
      doneMovingCallback
    )
  );

  handle.active = true;
  handle.moving = true;
  annotation.active = true;
  state.isToolLocked = true;

  // Add Event Listeners
  _dragEvents[interactionType].forEach(eventType => {
    element.addEventListener(eventType, dragHandler);
  });
  _upOrEndEvents[interactionType].forEach(eventType => {
    element.addEventListener(eventType, upOrEndHandler);
  });

  // ==========================
  // ========  TOUCH ==========
  // ==========================
  if (interactionType === 'touch') {
    runAnimation.value = true;
    const enabledElement = cornerstone.getEnabledElement(element);

    const aboveFinger = {
      x: evtDetail.currentPoints.page.x,
      y: evtDetail.currentPoints.page.y + FINGER_DISTANCE,
    };

    const targetLocation = cornerstone.pageToPixel(
      element,
      aboveFinger.x,
      aboveFinger.y
    );

    _animate(handle, runAnimation, enabledElement, targetLocation);
  }
}

function length(start, end) {
  return Math.sqrt(Math.pow(end.x - start.x, 2) + Math.pow(end.y - start.y, 2));
}

function _dragHandler(
  toolName,
  annotation,
  handle,
  options,
  interactionType,
  evt
) {
  const { image, currentPoints, element, buttons } = evt.detail;
  const page = currentPoints.page;
  const targetLocation = cornerstone.pageToPixel(
    element,
    page.x,
    interactionType === 'touch' ? page.y + FINGER_DISTANCE : page.y
  );

  // If the length measurement has parallel lines, allow handle movement in parallel direction only.
  let newTarget = targetLocation;
  let startLocation;
  if (
    options.restrictOrientationChange &&
    handle !== annotation.handles.textBox
  ) {
    const lastLocation = { x: handle.x, y: handle.y };
    startLocation =
      handle === annotation.handles.start
        ? annotation.handles.end
        : annotation.handles.start;
    const firstLine = length(lastLocation, startLocation);
    const currentLine = length(targetLocation, startLocation);
    const joinLine = length(targetLocation, lastLocation);
    // Cosine law
    let angle = Math.acos(
      (Math.pow(firstLine, 2) +
        Math.pow(currentLine, 2) -
        Math.pow(joinLine, 2)) /
        (2 * firstLine * currentLine)
    );

    const reqDistance = Math.cos(angle) * currentLine;
    const ratio = reqDistance / firstLine;
    newTarget.x = (1 - ratio) * startLocation.x + ratio * lastLocation.x;
    newTarget.y = (1 - ratio) * startLocation.y + ratio * lastLocation.y;
  }

  runAnimation.value = false;
  handle.active = true;
  handle.hasMoved = true;
  handle.x = newTarget.x;
  handle.y = newTarget.y;
  // TODO: A way to not flip this for textboxes on annotations
  annotation.invalidated = true;

  if (options.preventHandleOutsideImage) {
    if (options.restrictOrientationChange) {
      clipToBoxInSameOrientation(handle, startLocation, image);
    }
    clipToBox(handle, image);
  }

  cornerstone.updateImage(element);

  const activeTool = getActiveTool(element, buttons, interactionType);

  if (activeTool instanceof BaseAnnotationTool) {
    activeTool.updateCachedStats(image, element, annotation);
  }

  const eventType = EVENTS.MEASUREMENT_MODIFIED;
  const modifiedEventData = {
    toolName,
    toolType: toolName, // Deprecation notice: toolType will be replaced by toolName
    element,
    measurementData: annotation,
  };

  triggerEvent(element, eventType, modifiedEventData);
}

function _cancelEventHandler(
  toolName,
  evtDetail,
  annotation,
  handle,
  options = {},
  interactionType,
  { dragHandler, upOrEndHandler },
  doneMovingCallback
) {
  _endHandler(
    toolName,
    evtDetail,
    annotation,
    handle,
    options,
    interactionType,
    {
      dragHandler,
      upOrEndHandler,
    },
    doneMovingCallback,
    false
  );
}

function _upOrEndHandler(
  toolName,
  evtDetail,
  annotation,
  handle,
  options = {},
  interactionType,
  { dragHandler, upOrEndHandler },
  doneMovingCallback
) {
  const { element } = evtDetail;

  manipulatorStateModule.setters.removeActiveManipulatorForElement(element);

  _endHandler(
    toolName,
    evtDetail,
    annotation,
    handle,
    options,
    interactionType,
    {
      dragHandler,
      upOrEndHandler,
    },
    doneMovingCallback,
    true
  );
}

function _endHandler(
  toolName,
  evtDetail,
  annotation,
  handle,
  options = {},
  interactionType,
  { dragHandler, upOrEndHandler },
  doneMovingCallback,
  success = true
) {
  const element = evtDetail.element;

  handle.active = false;
  handle.moving = false;
  annotation.active = false;
  annotation.invalidated = true;
  runAnimation.value = false;
  state.isToolLocked = false;

  // Remove Event Listeners
  _dragEvents[interactionType].forEach(eventType => {
    element.removeEventListener(eventType, dragHandler);
  });
  _upOrEndEvents[interactionType].forEach(eventType => {
    element.removeEventListener(eventType, upOrEndHandler);
  });

  // If any handle is outside the image, delete the tool data
  if (
    options.deleteIfHandleOutsideImage &&
    anyHandlesOutsideImage(evtDetail, annotation.handles)
  ) {
    removeToolState(element, toolName, annotation);
  }

  // // TODO: What dark magic makes us want to handle TOUCH_PRESS differently?
  // if (evt.type === EVENTS.TOUCH_PRESS) {
  //   evt.detail.handlePressed = annotation;
  //   handle.x = image.x; // Original Event
  //   handle.y = image.y;
  // }

  if (typeof options.doneMovingCallback === 'function') {
    options.doneMovingCallback(success);
  }

  if (typeof doneMovingCallback === 'function') {
    doneMovingCallback(success);
  }

  cornerstone.updateImage(element);
}

/**
 * Animates the provided handle using `requestAnimationFrame`
 * @private
 * @method _animate
 *
 * @param {*} handle
 * @param {*} runAnimation
 * @param {*} enabledElement
 * @param {*} targetLocation
 * @returns {undefined}
 */
function _animate(handle, runAnimation, enabledElement, targetLocation) {
  if (!runAnimation.value) {
    return;
  }

  // Pixels / second
  const distanceRemaining = Math.abs(handle.y - targetLocation.y);
  const linearDistEachFrame = distanceRemaining / 10;

  if (distanceRemaining < 1) {
    handle.y = targetLocation.y;
    runAnimation.value = false;

    return;
  }

  if (handle.y > targetLocation.y) {
    handle.y -= linearDistEachFrame;
  } else if (handle.y < targetLocation.y) {
    handle.y += linearDistEachFrame;
  }

  // Update the image
  cornerstone.updateImage(enabledElement.element);

  // Request a new frame
  cornerstone.requestAnimationFrame(function() {
    _animate(handle, runAnimation, enabledElement, targetLocation);
  });
}
