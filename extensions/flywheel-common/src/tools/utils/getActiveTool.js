import csTools from 'cornerstone-tools';

const BaseAnnotationTool = csTools.importInternal('base/BaseAnnotationTool');
const BaseBrushTool = csTools.importInternal('base/BaseBrushTool');

const { getters, state } = csTools.store;

// Todo: We could simplify this if we only allow one active
// Tool per mouse button mask?
export default function getActiveTool(
  element,
  buttons,
  interactionType = 'mouse'
) {
  let tools;

  if (interactionType === 'touch') {
    tools = getActiveToolsForElement(element, getters.touchTools());
    tools = tools.filter(tool => tool.options.isTouchActive);
  } else {
    // Filter out disabled, enabled, and passive
    tools = getActiveToolsForElement(element, getters.mouseTools());

    // Filter out tools that do not match mouseButtonMask
    tools = tools.filter(
      tool =>
        Array.isArray(tool.options.mouseButtonMask) &&
        buttons &&
        tool.options.mouseButtonMask.includes(buttons) &&
        tool.options.isMouseActive
    );

    if (state.isMultiPartToolActive) {
      tools = filterToolsUseableWithMultiPartTools(tools);
    }
  }

  if (tools.length === 0) {
    return;
  }

  return tools[0];
}

/**
 * Filters an array of tools, returning only tools which are active.
 * @export
 * @public
 * @method
 * @name getActiveToolsForElement
 *
 * @param  {HTMLElement} element The element.
 * @param  {Object[]} tools      The input tool array.
 * @param  {string} handlerType  The input type being queried.
 * @returns {Object[]}            The filtered array.
 */
function getActiveToolsForElement(element, tools, handlerType) {
  return tools.filter(
    tool =>
      tool.element === element &&
      tool.mode === 'active' &&
      (handlerType === undefined || tool.options[`is${handlerType}Active`])
  );
}

/**
 * Filters an array of tools, returning only tools which are active or passive.
 * @export
 * @public
 * @method
 * @name filterToolsUseableWithMultiPartTools
 *
 * @param  {Object[]} tools      The input tool array.
 * @returns {Object[]}            The filtered array.
 */
function filterToolsUseableWithMultiPartTools(tools) {
  return tools.filter(
    tool =>
      !tool.isMultiPartTool &&
      !(tool instanceof BaseAnnotationTool) &&
      !(tool instanceof BaseBrushTool)
  );
}
