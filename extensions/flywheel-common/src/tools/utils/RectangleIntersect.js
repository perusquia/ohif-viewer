import cornerstoneMath from 'cornerstone-math';
import rotatePoint from './rotatePoint.js';
import { checkLinesIntersect } from './LineIntersect.js';

/**
 * Is point is inside the rectangle
 * @param {[Object]} points rectangle measurement points
 * @param {Object} point point to check
 * @param {Object} imageDimension image dimension on which rectangle drawn
 * @param {number} rotation initial image rotation at the time of drawing rectangle
 * @return {boolean} true if inside else false
 */
export function isPointInsideRect(points, point, imageDimension, rotation) {
  if (points.length < 4) {
    return false;
  }
  const centerPoint = {
    x: imageDimension.x / 2,
    y: imageDimension.y / 2,
  };
  let corner1 = points[0];
  let corner3 = points[2];
  let srcPoint = point;

  // Rotate the points with initial drawn rotation to make the rectangle non-rotated
  if (Math.abs(rotation) > 0.05) {
    corner1 = rotatePoint(corner1, centerPoint, rotation);
    corner3 = rotatePoint(corner3, centerPoint, rotation);
    srcPoint = rotatePoint(srcPoint, centerPoint, rotation);
  }
  const topLeft = {
    x: Math.min(corner1.x, corner3.x),
    y: Math.min(corner1.y, corner3.y),
  };
  const bottomRight = {
    x: Math.max(corner1.x, corner3.x),
    y: Math.max(corner1.y, corner3.y),
  };
  corner1 = topLeft;
  corner3 = bottomRight;
  const rect = {
    left: corner1.x,
    top: corner1.y,
    width: corner3.x - corner1.x,
    height: corner3.y - corner1.y,
  };
  return cornerstoneMath.point.insideRect(srcPoint, rect);
}

/**
 * To retrieve the rectangle data
 * @param {Object} start rectangle start point
 * @param {Object} end rectangle end point
 * @param {Object} imageDimension image dimension on which rectangle drawn
 * @param {number} rotation initial image rotation at the time of drawing rectangle
 * @return {Object} rectangle points, line points covering the entire rectangle
 */
export function getRectangleData(start, end, imageDimension, rotation) {
  const centerPoint = {
    x: imageDimension.x / 2,
    y: imageDimension.y / 2,
  };
  let corner1 = start;
  let corner3 = end;

  // Rotate the points with initial drawn rotation to make the rectangle non-rotated
  if (Math.abs(rotation) > 0.05) {
    corner1 = rotatePoint(start, centerPoint, rotation);
    corner3 = rotatePoint(end, centerPoint, rotation);
  }

  let corner2 = {
    x: corner3.x,
    y: corner1.y,
  };
  let corner4 = {
    x: corner1.x,
    y: corner3.y,
  };

  // Rotate back the points to the image pixel co-ordinate.
  if (Math.abs(rotation) > 0.05) {
    corner1 = rotatePoint(corner1, centerPoint, -rotation);
    corner2 = rotatePoint(corner2, centerPoint, -rotation);
    corner3 = rotatePoint(corner3, centerPoint, -rotation);
    corner4 = rotatePoint(corner4, centerPoint, -rotation);
  }

  // Adding the corner1 twice in linePoints in the last to complete the rectangle
  // and to make the number of points for connecting lines even
  return {
    points: [corner1, corner2, corner3, corner4],
    linePoints: [corner1, corner2, corner3, corner4, corner1, corner1],
  };
}

/**
 * To retrieve the rectangle data
 * @param {[Object]} boxPoints1 first ROI bounding box points
 * @param {[Object]} boxPoints2 second ROI bounding box points
 * @param {Object} imageDimension image dimension on which rectangle drawn
 * @return {boolean} return true if bounding box intersects
 */
export function isBoundingBoxIntersect(boxPoints1, boxPoints2, imageDimension) {
  let boundBoxOverlapping = !!boxPoints1.find(point =>
    isPointInsideRect(boxPoints2, point, imageDimension, 0)
  );

  boundBoxOverlapping =
    boundBoxOverlapping ||
    !!boxPoints2.find(point =>
      isPointInsideRect(boxPoints1, point, imageDimension, 0)
    );

  boundBoxOverlapping =
    boundBoxOverlapping || checkLinesIntersect(boxPoints1, boxPoints2);

  return boundBoxOverlapping;
}

/**
 * To check the box vertical positions
 * @param {[Object]} boxPoints1 first ROI bounding box points
 * @param {[Object]} boxPoints2 second ROI bounding box points
 * @return {boolean} return true if bounding box are left/right each other
 */
export function isBoundingBoxLeftRight(boxPoints1, boxPoints2) {
  let left1 = Number.MAX_VALUE;
  let right1 = 0;
  boxPoints1.forEach(point => {
    left1 = point.x < left1 ? point.x : left1;
    right1 = point.x > right1 ? point.x : right1;
  });
  let left2 = Number.MAX_VALUE;
  let right2 = 0;
  boxPoints2.forEach(point => {
    left2 = point.x < left2 ? point.x : left2;
    right2 = point.x > right2 ? point.x : right2;
  });
  let isLeftRight = false;
  if (right1 < left2 || left1 > right2) {
    isLeftRight = true;
  }
  return isLeftRight;
}

/**
 * To check the box vertical positions
 * @param {[Object]} boxPoints1 first ROI bounding box points
 * @param {[Object]} boxPoints2 second ROI bounding box points
 * @return {boolean} return true if bounding box are top/bottom each other
 */
export function isBoundingBoxTopBottom(boxPoints1, boxPoints2) {
  let top1 = Number.MAX_VALUE;
  let bottom1 = 0;
  boxPoints1.forEach(point => {
    top1 = point.y < top1 ? point.y : top1;
    bottom1 = point.y > bottom1 ? point.y : bottom1;
  });
  let top2 = Number.MAX_VALUE;
  let bottom2 = 0;
  boxPoints2.forEach(point => {
    top2 = point.y < top2 ? point.y : top2;
    bottom2 = point.y > bottom2 ? point.y : bottom2;
  });
  let isTopBottom = false;
  if (bottom1 < top2 || top1 > bottom2) {
    isTopBottom = true;
  }
  return isTopBottom;
}
