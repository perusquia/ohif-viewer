import store from '@ohif/viewer/src/store';
import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';

export const getReadOnlyStatus = element => {
  const state = store.getState();
  const viewports = state?.viewports?.layout?.viewports;
  let readOnly = false;

  viewports.some((viewport, index) => {
    const iteratedElement = FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
      index
    );
    if (iteratedElement === element) {
      readOnly = viewport.readOnly || false;
      return true;
    }
  });
  return readOnly;
};
