const TOOL_NAMES = {
  CONTOUR_ROI_TOOL: 'ContourRoi',
};

export default TOOL_NAMES;
