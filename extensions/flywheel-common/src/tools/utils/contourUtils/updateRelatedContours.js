import cornerstoneTools from 'cornerstone-tools';
import OHIF from '@ohif/core';
import getROIContourData from './getROIContourData.js';
import getROIContourCollection from './getROIContourCollection.js';
import interpolate from './freehandInterpolate/interpolate';
import saveActionState from '../../commonUtils/saveActionState';

const triggerEvent = cornerstoneTools.importInternal('util/triggerEvent');
const { EVENTS } = cornerstoneTools;

/**
 * updatedRelatedContours - Update the same roi contour id contours on modifying
 * one of them .
 *
 * @param  {Object} eventData.
 * @return {Nil}
 */
export default function(eventData) {
  const { eventType, measurementData, operationData } = eventData.detail;
  const isUndoRedoOperation =
    operationData?.type === 'Undo' || operationData?.type === 'Redo';
  const element = eventData.currentTarget;
  const referenceContours = getROIContourCollection(
    element,
    { key: 'ROIContourUid', value: measurementData.ROIContourUid },
    true
  );
  if (isUndoRedoOperation) {
    handleUndoRedoContourUpdate(eventData, referenceContours);
    return;
  }
  const actionGroup = 'measurements';
  const measurementApi = OHIF.measurements.MeasurementApi.Instance;

  (referenceContours || []).forEach(contour => {
    const detail = {
      toolType: contour.toolType,
      element,
      measurementData: contour,
      isSubAction: true,
    };
    if (measurementData._id !== contour._id) {
      if (eventType === 'Delete') {
        const measurementTypeId =
          measurementApi.toolsGroupsMap[contour.toolType];
        const { lesionNamingNumber, timepointId } = contour;
        measurementApi.deleteMeasurements(contour.toolType, measurementTypeId, {
          lesionNamingNumber,
          timepointId,
        });
        if (!contour.interpolated) {
          saveActionState(contour, actionGroup, eventType, false, false, true);
        }
      } else if (eventType === 'Modify') {
        const measurement = measurementApi.tools[measurementData.toolType].find(
          m => m._id === contour._id
        );
        measurement.location = measurementData.location;
        measurement.description = measurementData.description;
        measurement.color = measurementData.color;
        measurement.visible = measurementData.visible;
        measurement.dirty = true;
        measurementApi.updateMeasurement(measurement.toolType, measurement);
        OHIF.measurements.MeasurementApi.syncToolStateWithMeasurementData(
          measurement
        );
      }
    }
  });
}

const handleUndoRedoContourUpdate = (eventData, referenceContours) => {
  const { eventType, measurementData, operationData } = eventData.detail;
  const element = eventData.currentTarget;
  const interpolateItems = [];
  const measurementApi = OHIF.measurements.MeasurementApi.Instance;

  // Special handling for contour roi measurement for redo of interpolated roi change
  // to key roi by editing/sculpting, this is because the auto creation action of the
  // interpolated rois will not be available in the undo-redo-state
  if (operationData?.modifiedProperty === 'interpolated') {
    const imageId = OHIF.measurements.getImageIdForImagePath(
      measurementData.imagePath
    );
    const roiImageContours = getROIContourData(
      [imageId],
      { key: 'ROIContourUid', value: measurementData.ROIContourUid },
      true
    );
    const interpolatedContour = (roiImageContours?.[0]?.contours || []).find(
      contour => contour.interpolated
    );
    if (
      interpolatedContour &&
      interpolatedContour._id !== measurementData._id
    ) {
      const measurementTypeId =
        measurementApi.toolsGroupsMap[interpolatedContour.toolType];
      measurementApi.deleteMeasurements(
        interpolatedContour.toolType,
        measurementTypeId,
        {
          lesionNamingNumber: interpolatedContour.lesionNamingNumber,
          timepointId: interpolatedContour.timepointId,
        }
      );
      delete measurementData.lesionNamingNumber;
      delete measurementData.measurementNumber;
      measurementApi.addMeasurement(
        measurementData.toolType,
        measurementData,
        false
      );
      measurementApi.syncMeasurementsAndToolData();
      OHIF.measurements.MeasurementApi.syncToolStateWithMeasurementData(
        measurementData
      );
    }
    interpolate([measurementData], element);
    return;
  }

  // Save state for undo-redo only for non-interpolated measurements, recreate or update
  // interpolated ones while undo-redo operation.

  (referenceContours || []).forEach(contour => {
    const detail = {
      toolType: contour.toolType,
      element,
      measurementData: contour,
      isSubAction: true,
    };
    if (measurementData._id !== contour._id) {
      if (eventType === 'Delete') {
        // No need to remove all the non-interpolated contour for the type 'Delete' as part of 'Undo'(Add operation)
        // but, only the specific measurementdata and all the interpolated contours.
        // Interpolated contours will be identified again freshly.
        if (contour.interpolated || operationData?.type !== 'Undo') {
          const measurementTypeId =
            measurementApi.toolsGroupsMap[contour.toolType];
          const { lesionNamingNumber, timepointId } = contour;
          measurementApi.deleteMeasurements(
            contour.toolType,
            measurementTypeId,
            {
              lesionNamingNumber,
              timepointId,
            }
          );
        } else if (!contour.interpolated && operationData?.type === 'Undo') {
          interpolateItems.push(contour);
        }
      } else if (eventType === 'Modify') {
        const measurement = measurementApi.tools[measurementData.toolType].find(
          m => m._id === contour._id
        );
        const isPropertyChange =
          measurement.location !== measurementData.location ||
          measurement.description !== measurementData.description ||
          measurement.color !== measurementData.color ||
          measurement.visible !== measurementData.visible;
        if (isPropertyChange) {
          measurement.location = measurementData.location;
          measurement.description = measurementData.description;
          measurement.color = measurementData.color;
          measurement.visible = measurementData.visible;
          measurement.dirty = true;
        } else if (!measurement.interpolated) {
          interpolateItems.push(measurement);
        }
        measurementApi.updateMeasurement(
          measurement.toolType,
          measurement,
          true
        );
        OHIF.measurements.MeasurementApi.syncToolStateWithMeasurementData(
          measurement
        );
      }
    }
  });
  if (eventType === 'Modify') {
    const modifiedEventData = {
      toolName: measurementData.toolType,
      toolType: measurementData.toolType, // Deprecation notice: toolType will be replaced by toolName
      element,
      measurementData,
    };
    triggerEvent(element, EVENTS.MEASUREMENT_MODIFIED, modifiedEventData);
  }
  if (interpolateItems.length > 0) {
    interpolate(interpolateItems, element);
  }
};
