import Polygon from './classes/Polygon.js';
import interpolate from './freehandInterpolate/interpolate.js';

export { Polygon, interpolate };
