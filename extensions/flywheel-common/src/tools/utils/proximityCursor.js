import csTools from 'cornerstone-tools';
import { calculateDimensions } from './getCircleDimensions';

const { importInternal } = csTools;

// Cornerstone 3rd party dev kit imports
const drawRect = importInternal('drawing/drawRect');
const drawEllipse = importInternal('drawing/drawEllipse');
const getPixelSpacing = importInternal('util/getPixelSpacing');

function getProximityCursor() {
  const { shape, display, size } = getProximityCursorProperties();
  const cursorShape = shape.toLowerCase();
  const defaultSize = cursorShape === 'circle' ? 0.125 : 0;
  const number = 'number';

  switch (cursorShape) {
    case 'square':
      const squareSize = typeof size === number ? size : defaultSize;
      return {
        display,
        height: squareSize,
        width: squareSize,
        drawCursor: drawRect,
      };
    case 'rectangle':
      const rectangleSize = getFormattedSize(size, defaultSize);
      return {
        display,
        height: rectangleSize.height,
        width: rectangleSize.width,
        drawCursor: drawRect,
      };
    case 'ellipse':
      const ellipseSize = getFormattedSize(size, defaultSize);
      return {
        display,
        height: ellipseSize.height,
        width: ellipseSize.width,
        drawCursor: drawEllipse,
      };
    case 'circle':
      const circleSize = typeof size === number ? size : defaultSize;
      return {
        display,
        height: circleSize,
        width: circleSize,
        drawCursor: drawEllipse,
      };
    default:
      break;
  }
}

function getFormattedSize(size, defaultSize) {
  const height = typeof size?.height === 'number' ? size.height : defaultSize;
  const width = typeof size?.width === 'number' ? size.width : defaultSize;
  const isZeroSize = height === 0 || width === 0;

  return {
    height: isZeroSize ? 0 : height,
    width: isZeroSize ? 0 : width,
  };
}

function getProximityCursorProperties() {
  return store.getState()?.contourProperties.proximityCursorProperties;
}

/**
 *
 * @param {Object} evt - event data
 * @param {Object} cursorTool - proximity cursor properties.
 * @returns {Object} - width and height of proximity cursor
 */
function getProximityCursorDetails(evt, cursorTool) {
  const eventData = evt.detail;
  const { image } = eventData;

  const { rowPixelSpacing, colPixelSpacing } = getPixelSpacing(image);
  const hasPixelSpacing = rowPixelSpacing && colPixelSpacing;
  if (!hasPixelSpacing) {
    return 0;
  }

  return {
    width: calculateDimensions(cursorTool.width, colPixelSpacing, 'x'),
    height: calculateDimensions(cursorTool.height, rowPixelSpacing, 'y'),
  };
}

export default {
  getProximityCursor,
  getProximityCursorDetails,
  getProximityCursorProperties,
};
