import store from '@ohif/viewer/src/store';
import { getReadOnlyStatus } from './checkReadOnlyStatus';
import {
  Redux as FlywheelCommonRedux,
  Utils,
} from '@flywheel/extension-flywheel-common';
import getActiveSliceInfo from '../../utils/getActiveSliceInfo';

export const enableToolsForBSlice = (bSlicesConfig, element) => {
  let enableToolsForBSlice = true;
  const state = store.getState();
  const isReadOnlyViewport = getReadOnlyStatus(element);
  const { selectedQuestion } = FlywheelCommonRedux.selectors;

  const activeQuestion = selectedQuestion(state);
  const questionKey = activeQuestion?.questionKey || '';
  const answer = activeQuestion?.answer || '';
  const values = activeQuestion?.values || [];

  const sliceSettings = bSlicesConfig?.settings;
  const sliceRequirements = bSlicesConfig?.required;
  const sliceNumberKey = getActiveSliceInfo(-1, element)?.sliceNumber;

  if (
    !sliceSettings[sliceNumberKey] &&
    !isReadOnlyViewport &&
    !Utils.isCurrentFile(state)
  ) {
    enableToolsForBSlice = false;
  } else if (
    sliceSettings[sliceNumberKey] &&
    sliceRequirements &&
    questionKey.length !== 0
  ) {
    if (sliceRequirements?.one?.includes(questionKey)) {
      if (
        sliceRequirements?.specific?.[sliceNumberKey]?.includes(questionKey)
      ) {
        enableToolsForBSlice = true;
      } else if (values.length > 0) {
        const value = values.find(v => v.value == answer);
        const measurementTools = value?.measurementTools;
        const requireMeasurements = value?.requireMeasurements;
        if (measurementTools && requireMeasurements) {
          const measurements = state.timepointManager.measurements;
          measurementTools.forEach(toolName => {
            measurements[toolName]?.find(measurement => {
              if (
                measurement.sliceNumber === Number(sliceNumberKey) &&
                requireMeasurements?.includes(measurement.location)
              ) {
                enableToolsForBSlice = false;
              }
            });
          });
        }
      } else {
        enableToolsForBSlice = true;
      }
    }
  }
  return enableToolsForBSlice;
};
