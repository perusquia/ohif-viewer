import cornerstoneMath from 'cornerstone-math';

/**
 *
 * @param {Object} image - image data
 * @param {Number} diameter - Circle diameter
 * @returns {Object} Diameter in X and Y axis of circle
 */
function getCircleDimensions(pixelSpacing, diameter) {
  const { rowPixelSpacing, colPixelSpacing } = pixelSpacing;
  const hasPixelSpacing = rowPixelSpacing && colPixelSpacing;
  if (!hasPixelSpacing) {
    return 0;
  }

  return {
    width: calculateDimensions(diameter, colPixelSpacing, 'x'),
    height: calculateDimensions(diameter, rowPixelSpacing, 'y'),
  };
}

export function calculateDimensions(diameter, pixelSpacing, axis) {
  const coords = { x: 0, y: 0 };

  const imageDimension = diameter / pixelSpacing;
  let coordsInCircumferenceImage;
  if (axis === 'x') {
    coordsInCircumferenceImage = {
      x: coords.x + imageDimension,
      y: coords.y,
    };
  } else {
    coordsInCircumferenceImage = {
      x: coords.x,
      y: coords.y + imageDimension,
    };
  }

  return Math.abs(
    cornerstoneMath.point.distance(coordsInCircumferenceImage, coords)
  );
}

export default getCircleDimensions;
