import cornerstone from 'cornerstone-core';
import OHIF from '@ohif/core';

import getEllipseHandlesFromCircle from './getEllipseHandlesFromCircle';
import { getEllipseData } from './EllipseIntersect.js';

const canvasToPixel = cornerstone.canvasToPixel;

/**
 *
 * @param {Object} centerOnCanvas centre of circle in canvas units.
 * @param {Object} radiusOnCanvas radius of circle in canvas units.
 * @param {Object} element DOM Element in which circle drawn.
 * @param {number} angleResolution angle between two adjacent points along the circle circumference in degrees.
 * @return {Array} Array of end points of the polygon along the circle circumference in image coordinates.
 */
export function getPointsOnCircleCircumference(
  centerOnCanvas,
  radiusOnCanvas,
  element,
  angleResolution = 5
) {
  let linePoints = [];

  for (let i = 0; i <= 360; i = i + angleResolution) {
    const angleInRadiansFromXAxis = (i * Math.PI) / 180;
    const point = {
      x: radiusOnCanvas * Math.cos(angleInRadiansFromXAxis) + centerOnCanvas.x,
      y: radiusOnCanvas * Math.sin(angleInRadiansFromXAxis) + centerOnCanvas.y,
    };
    const pointPixel = canvasToPixel(element, point);
    linePoints.push(pointPixel);
  }

  return linePoints;
}

export function getEllipseDataforCircle(measurement, imageDimension) {
  const { getImageIdForImagePath } = OHIF.measurements;
  const imageId = getImageIdForImagePath(measurement.imagePath);
  const ellipsePointsForCircle = getEllipseHandlesFromCircle(
    imageId,
    measurement.handles
  );
  return getEllipseData(
    ellipsePointsForCircle.topLeftImagePoint,
    ellipsePointsForCircle.bottomRightImagePoint,
    imageDimension,
    measurement.handles.initialRotation
  );
}
