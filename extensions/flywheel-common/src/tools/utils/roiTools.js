const roiTools = {
  RectangleRoi: 'RectangleRoi',
  EllipticalRoi: 'EllipticalRoi',
  FreehandRoi: 'FreehandRoi',
  OpenFreehandRoi: 'OpenFreehandRoi',
  CircleRoi: 'CircleRoi',
};

export default roiTools;
