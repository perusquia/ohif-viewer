import cornerstoneMath from 'cornerstone-math';

const { intersectLine } = cornerstoneMath.lineSegment;

/**
 * Is point is inside the freehand
 * @param {[Object]} points freehand measurement points
 * @param {Object} point point to check
 * @return {boolean} true if inside else false
 */
export function isPointInsideFreehand(points, point) {
  return pointInFreehand(points, point);
}

/**
 * Get the freehand bounding box points
 * @param {[Object]} points freehand points.
 * @returns {[Object]} return the bounding box points.
 */
function getFreehandBoundingBoxPts(points) {
  // Retrieve the bounds of the ROI in image coordinates
  const bounds = {
    left: points[0].x,
    right: points[0].x,
    bottom: points[0].y,
    top: points[0].x,
  };

  for (let i = 0; i < points.length; i++) {
    bounds.left = Math.min(bounds.left, points[i].x);
    bounds.right = Math.max(bounds.right, points[i].x);
    bounds.bottom = Math.min(bounds.bottom, points[i].y);
    bounds.top = Math.max(bounds.top, points[i].y);
  }
  return [
    { x: bounds.left, y: bounds.top },
    { x: bounds.right, y: bounds.top },
    { x: bounds.right, y: bounds.bottom },
    { x: bounds.left, y: bounds.bottom },
  ];
}

/**
 * Calculates whether "location" is inside the freehand
 * @param {[Object]} points freehand points.
 * @param {Object} point point being queried.
 * @returns {boolean} true if the point is inside the freehand defined by points.
 */
function pointInFreehand(points, point) {
  const x = point.x;
  const y = point.y;
  let inside = false;
  const start = 0;
  const end = points.length;
  const len = end - start;
  for (let i = 0, j = len - 1; i < len; j = i++) {
    const xi = points[i + start].x;
    const yi = points[i + start].y;
    const xj = points[j + start].x;
    const yj = points[j + start].y;

    const intersect =
      yi > y !== yj > y && x < ((xj - xi) * (y - yi)) / (yj - yi) + xi;
    if (intersect) inside = !inside;
  }
  return inside;
}

/**
 * To retrieve the freehand data
 * @param {[Object]} points freehand measurement points
 * @param {boolean} isClosed is closed or open freehand, true if closed
 * @return {Object} freehand points
 */
export function getFreehandData(points, isClosed) {
  let freehandPoints = [];
  freehandPoints = freehandPoints.concat(points);
  if (freehandPoints.length < 1) {
    return {
      points: [],
      boundingBoxPoints: [],
    };
  }
  if (isClosed) {
    // Append the first point in last if not already there
    if (
      freehandPoints[0].x !== freehandPoints[points.length - 1].x ||
      freehandPoints[0].y !== freehandPoints[points.length - 1].y
    ) {
      freehandPoints.push(freehandPoints[0]);
    }
    if (freehandPoints.length % 2 !== 0) {
      freehandPoints.push(freehandPoints[0]); // Make the number of points even
    }
  } else {
    if (freehandPoints.length % 2 !== 0) {
      freehandPoints.push(freehandPoints[freehandPoints.length - 1]); // Make the number of points even
    }
  }
  const boundingBoxPoints = getFreehandBoundingBoxPts(points);
  return {
    points: freehandPoints,
    boundingBoxPoints: boundingBoxPoints,
  };
}
