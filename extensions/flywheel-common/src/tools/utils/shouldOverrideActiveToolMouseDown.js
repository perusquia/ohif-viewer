import csTools from 'cornerstone-tools';
import TOOL_NAMES from '@ohif/extension-cornerstone/src/toolNames';
const ringOverlayTool = TOOL_NAMES.RING_OVERLAY_TOOL;

/**
 * Prevent preMOuseDownCallback if ring overlay tool is active
 *
 * @param {Object} evt - The event.
 * @returns {boolean}
 */
export function shouldEnablePreMouseDownCallback(evt) {
  const { element } = evt.detail;
  const toolState = csTools.getToolState(element, ringOverlayTool);
  if (toolState) {
    const isRingOverlayActive = toolState.data?.find(
      handle => handle?.active === true
    );
    return isRingOverlayActive;
  }
  return false;
}
