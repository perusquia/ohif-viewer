import { doesIntersect } from './LineIntersect.js';

/**
 * Checks whether the modification of a handle's position causes intersection of the lines of the polygon.
 * @public
 * @method modify
 *
 * @param {Object} dataHandles Data object associated with the tool.
 * @param {number} modifiedHandleId The id of the handle being modified.
 * @returns {boolean} Whether the modfication causes any intersections.
 */
function modify(dataHandles, modifiedHandleId) {
  // Check if the modifiedHandle's previous and next lines will intersect any other line in the polygon
  const modifiedHandle = getCoords(dataHandles[modifiedHandleId]);

  // Previous neightbor handle
  let neighborHandleId = modifiedHandleId - 1;

  if (modifiedHandleId === 0) {
    neighborHandleId = dataHandles.length - 1;
  }

  let neighborHandle = getCoords(dataHandles[neighborHandleId]);

  if (
    doesIntersectOtherLines(dataHandles, modifiedHandle, neighborHandle, [
      modifiedHandleId,
      neighborHandleId,
    ])
  ) {
    return true;
  }

  // Next neightbor handle
  if (modifiedHandleId === dataHandles.length - 1) {
    neighborHandleId = 0;
  } else {
    neighborHandleId = modifiedHandleId + 1;
  }

  neighborHandle = getCoords(dataHandles[neighborHandleId]);

  return doesIntersectOtherLines(dataHandles, modifiedHandle, neighborHandle, [
    modifiedHandleId,
    neighborHandleId,
  ]);
}

/**
 * Checks whether the line (p1,q1) intersects any of the other lines in the polygon.
 * @private
 * @function doesIntersectOtherLines
 *
 * @param {Object} dataHandles Data object associated with the tool.
 * @param {Object} p1 Coordinates of the start of the line.
 * @param {Object} q1 Coordinates of the end of the line.
 * @param {Object} ignoredHandleIds Ids of handles to ignore (i.e. lines that share a vertex with the line being tested).
 * @returns {boolean} Whether the line intersects any of the other lines in the polygon.
 */
function doesIntersectOtherLines(dataHandles, p1, q1, ignoredHandleIds) {
  let j = dataHandles.length - 1;
  const lastIndex = j;
  if (
    ignoredHandleIds.indexOf(0) !== -1 &&
    ignoredHandleIds.indexOf(lastIndex) !== -1
  ) {
    return false;
  }

  for (let i = 0; i < dataHandles.length; i++) {
    if (
      ignoredHandleIds.indexOf(i) !== -1 ||
      ignoredHandleIds.indexOf(j) !== -1
    ) {
      j = i;
      continue;
    }

    const p2 = getCoords(dataHandles[j]);
    const q2 = getCoords(dataHandles[i]);

    if (j !== lastIndex && i !== 0 && doesIntersect(p1, q1, p2, q2)) {
      return true;
    }

    j = i;
  }

  return false;
}

/**
 * Returns an object with two properties, x and y, from a heavier FreehandHandleData object.
 * @private
 * @function getCoords
 *
 * @param {Object} dataHandle Data object associated with a single handle in the freehand tool.
 * @returns {Object} An object containing position propeties x and y.
 */
function getCoords(dataHandle) {
  return {
    x: dataHandle.x,
    y: dataHandle.y,
  };
}

/**
 * To compare two measurement lines.
 * @param {Array} refPoints - reference measurement to be used as boundary.
 * @param {Array} curPoints - currently drawn measurement.
 * @param {Object} imageDimension - image dimension
 * @param {Boolean} isHorizontal - to specify which direction to be validated ("up/down" = true, "left/right" = false)
 * @returns Boolean - true if measurement overlaps or if measurement is not comparable as on given side
 */
export function compareContourPosition(
  refPoints,
  curPoints,
  imageDimension,
  isHorizontal = false
) {
  let isValidPosition;

  if (isArrayOfArray(refPoints) || isArrayOfArray(curPoints)) {
    let direction = [];
    let status = false;
    status = compareOpenFreehandPosition(
      refPoints,
      curPoints,
      imageDimension,
      isHorizontal,
      true,
      direction
    );
    isValidPosition = checkLineDirection(direction);
    if (isValidPosition) {
      status = compareOpenFreehandPosition(
        curPoints,
        refPoints,
        imageDimension,
        isHorizontal,
        true,
        direction
      );
      isValidPosition = checkLineDirection(direction);
    }
    if (!direction.length && status === true) {
      return status;
    }
    if (!direction.length && status === false) {
      return status;
    }

    return isValidPosition;
  }

  isValidPosition = validateContourPosition(
    refPoints,
    curPoints,
    imageDimension,
    isHorizontal
  );
  if (isValidPosition) {
    isValidPosition = validateContourPosition(
      curPoints,
      refPoints,
      imageDimension,
      isHorizontal
    );
  }
  return isValidPosition;
}

/**
 * To check open-freehand position
 * @param {Array} refPoints
 * @param {Array} curPoints
 * @param {Object} imageDimension
 * @param {Boolean} isHorizontal
 * @param {Boolean} showPosition
 * @returns {Boolean}
 */
function compareOpenFreehandPosition(
  refPoints,
  curPoints,
  imageDimension,
  isHorizontal,
  showPosition = false,
  direction
) {
  const isOpenFreehand1 = isArrayOfArray(refPoints);
  const isOpenFreehand2 = isArrayOfArray(curPoints);
  const openFreehandPoints = isOpenFreehand1 ? refPoints : curPoints;
  const refMeasurementPoints = !isOpenFreehand1 ? refPoints : curPoints;
  let status = false;
  let l = 0;
  let p = 0;
  let position;
  if (isOpenFreehand1 && isOpenFreehand2) {
    if (openFreehandPoints.length === 1 && refMeasurementPoints.length === 1) {
      status = validateContourPosition(
        openFreehandPoints[0],
        refMeasurementPoints[0],
        imageDimension,
        isHorizontal
      );
      return status;
    } else {
      for (l = 0; l < openFreehandPoints.length; l++) {
        for (p = 0; p < refMeasurementPoints.length; p++) {
          position = validateContourPosition(
            openFreehandPoints[l],
            refMeasurementPoints[p],
            imageDimension,
            isHorizontal,
            showPosition
          );
          direction.push({ ...position });
        }
      }
    }
  } else if (isOpenFreehand1 || isOpenFreehand2) {
    for (l = 0; l < openFreehandPoints.length; l++) {
      position = validateContourPosition(
        openFreehandPoints[l],
        refMeasurementPoints,
        imageDimension,
        isHorizontal,
        showPosition
      );
      direction.push({ ...position });
    }
  }
  return direction;
}

/**
 * To check the line position
 * @param {Array} direction
 * @returns
 */
function checkLineDirection(direction) {
  if (direction.length) {
    const posValue = direction[0].positions;
    if (!posValue) {
      let count = 0;
      for (let i = 0; i < direction.length; i++) {
        if (!direction[i].positions) {
          count += 1;
        }
      }
      if (count === direction.length) {
        return true;
      }
    }
    let val = null;
    for (let i = 0; i < direction.length; i++) {
      if (direction[i].positions === 'OVERLAPS') {
        return true;
      } else if (direction[i].positions === 'NOT_OVERLAPED') {
        const dir = direction[i].lineDirection.find(x => x !== 0 && x !== val);
        if (dir) {
          if (val && val !== dir) {
            return true;
          }
          if (!val) {
            val = dir;
          }
        }
      }
    }
  }

  return false;
}

/**
 * To check the points are of array of array format
 * @param {Array} points
 * @returns {Boolean}
 */
function isArrayOfArray(points) {
  if (points?.length && Array.isArray(points[0])) {
    return true;
  }
  return false;
}

/**
 * Validate the position of 2 open freehand/polygon measurement points whether both maintained the same position.
 * Here considering only the comparable points for validation by skipping the other points.
 * ex: For horizontal validation, either first set of comparable points(comparable means points coming in same vertical range)
 * should be completely left or completely right of second set of comparable points.
 * For vertical validation, either first set of comparable points(comparable means points coming in same horizontal range)
 * should be completely up or completely down of second set of comparable points.
 * @param {Array} refPoints - reference measurement to be used as boundary.
 * @param {Array} curPoints - currently drawn measurement.
 * @param {Object} imageDimension - image dimension
 * @param {Boolean} isHorizontal - to specify which direction to be validated ("up/down" = true, "left/right" = false)
 * @returns Boolean - true if measurement overlaps or if measurement is not comparable as on given side
 */
export function validateContourPosition(
  refPoints,
  curPoints,
  imageDimension,
  isHorizontal = false,
  showPosition = false
) {
  if (refPoints.length < 2 || curPoints.length < 2) {
    return false;
  }
  let ref = [...refPoints];
  ref.push({ ...ref[ref.length - 1] });
  let cur = [...curPoints];
  cur.push({ ...cur[cur.length - 1] });

  let refDir = 0;
  let dir = 0;
  for (let i = 0; i < cur.length; i++) {
    const point = cur[i];
    const startPoint = {
      x: isHorizontal ? point.x : 0,
      y: isHorizontal ? 0 : point.y,
    };
    const endPoint = {
      x: isHorizontal ? point.x : imageDimension.x,
      y: isHorizontal ? imageDimension.y : point.y,
    };
    for (let j = 0; j + 1 < ref.length; j++) {
      let isIntersect = doesIntersect(startPoint, point, ref[j], ref[j + 1]);
      if (isIntersect) {
        dir = 1;
        if (refDir === 0) {
          refDir = dir;
          dir = 0;
        }
        if (refDir !== dir && dir !== 0) {
          if (showPosition) {
            return {
              isValidPosition: true,
              positions: 'OVERLAPS',
              lineDirection: [refDir, dir],
            };
          }
          return true;
        }
      }

      isIntersect = doesIntersect(point, endPoint, ref[j], ref[j + 1]);
      dir = isIntersect ? -1 : 0;
      if (refDir === 0) {
        refDir = dir;
      }
      if (refDir !== dir && dir !== 0) {
        if (showPosition) {
          return {
            isValidPosition: true,
            positions: 'OVERLAPS',
            lineDirection: [refDir, dir],
          };
        }
        return true;
      }
    }
  }
  if (showPosition) {
    return {
      isValidPosition: refDir === 0,
      positions: refDir === 0 ? null : 'NOT_OVERLAPED',
      lineDirection: refDir === 0 ? [refDir] : [0, refDir],
    };
  }
  return refDir === 0;
}

export default { modify, compareContourPosition, doesIntersectOtherLines };
