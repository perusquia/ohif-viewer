import csTools from 'cornerstone-tools';
import store from '@ohif/viewer/src/store';
import commonToolUtils from './commonUtils';
import cornerstone from 'cornerstone-core';
import { getSmartCTRangeList } from '../utils';
import { shouldEnableAnnotationTool } from './utils/shouldEnableAnnotationTool';

const levelTrace = csTools.importInternal(
  'tools/segmentation/strategies/levelTrace'
);
const cursors = csTools.importInternal('tools/cursors');
const BaseTool = csTools.importInternal('base/BaseTool');
const segmentationModule = csTools.getModule('segmentation');
const { getCircle } = csTools.importInternal('util/segmentationUtils');
const { saveActionState, segmentationRangeManager } = commonToolUtils;
const {
  syncInCrossSectional2DViewports,
  getViewportIndex,
} = commonToolUtils.syncInCrossSectional2DViewports;

/**
 * @public
 * @class ThresholdTool
 * @classdesc Tool for drawing segmentations on an image.
 */
export default class ThresholdTool extends BaseTool {
  static eventListeners = [];

  constructor(props = {}) {
    const defaultProps = {
      name: 'ThresholdTool',
      strategies: {
        LEVELTRACE: levelTrace,
      },
      supportedInteractionTypes: ['Mouse', 'Touch'],
      svgCursor: cursors.freehandEraseInsideCursor,
      mixins: ['multiFreehandSegmentationMixin'],
    };
    const initialProps = Object.assign(defaultProps, props);

    super(initialProps);
    this.updateOnMouseMove = true;
    this.lastMousePoint = {};
    this.thresholdValue = 0;
    this.previousImageId = '';
    setTimeout(this.bindToElements.bind(this), 100);
  }

  bindToElements() {
    try {
      this.unbindFromElements();
    } catch (err) {
      // Potentially tool activated before viewport elements got enabled
      console.warn(err);
    }
    const viewportSpecificData = store.getState().viewports
      ?.viewportSpecificData;
    const allowedPlugins = [
      'cornerstone',
      'cornerstone::nifti',
      'cornerstone::metaimage',
    ];
    cornerstone.getEnabledElements().forEach((enabledElement, elementIndex) => {
      // Global ThresholdTool will be used even if a temporary tool will be created by csTools
      // by calling new APITool() in addTool.js to check a global tool already exists
      // So register events of all elements for the active global tool
      const globalThresholdTool =
        csTools.getToolForElement(enabledElement.element, this.name) || this;
      const viewportData = viewportSpecificData[elementIndex];
      globalThresholdTool.setToleranceFunction(
        globalThresholdTool._getTolerance
      );
      globalThresholdTool.setPreMouseMoveCallback(
        globalThresholdTool._preMouseMoveCallback.bind(globalThresholdTool)
      );
      // Wait till image loaded and then setToolState
      if (
        enabledElement &&
        // cornerstone.getImage(enabledElement.element) &&
        allowedPlugins.includes(viewportData.plugin)
      ) {
        globalThresholdTool.setToolState(enabledElement.element);
        ThresholdTool.eventListeners[elementIndex] = {
          enabledElement: enabledElement,
          newImageCallback: globalThresholdTool._onNewImageListener.bind(
            globalThresholdTool
          ),
        };

        enabledElement.element.addEventListener(
          cornerstone.EVENTS.NEW_IMAGE,
          ThresholdTool.eventListeners[elementIndex].newImageCallback
        );
      } else if (allowedPlugins.includes(viewportData.plugin)) {
        setTimeout(
          globalThresholdTool.bindToElements.bind(globalThresholdTool),
          100
        );
      }
    });
  }

  unbindFromElements() {
    if (ThresholdTool.eventListeners) {
      ThresholdTool.eventListeners.forEach(listener => {
        listener.enabledElement.element.removeEventListener(
          cornerstone.EVENTS.NEW_IMAGE,
          listener.newImageCallback
        );
        this.handles.points.length = 0;
        cornerstone.getEnabledElements().forEach(enabledElement => {
          if (enabledElement.element === listener.enabledElement.element) {
            cornerstone.updateImage(listener.enabledElement.element);
          }
        });
      });
      ThresholdTool.eventListeners.length = 0;
    }
  }

  _onNewImageListener(evt) {
    this.setToolState(evt.detail.element);

    // Prevent updates in other viewports on scrolling in activeViewport
    if (
      !evt.detail.oldImage ||
      this.previousImageId !== evt.detail.oldImage.imageId ||
      !this.lastMousePoint
    ) {
      return;
    }

    if (this.previousImageId !== evt.detail.image.imageId) {
      const detail = {
        ...evt.detail,
        currentPoints: this.lastMousePoint,
      };
      const eventData = { ...evt, detail };
      this.previousImageId = eventData.detail.image.imageId;

      this.findSegmentIndex(eventData);
      this.mouseMoveCallback(eventData);
      cornerstone.updateImage(detail.element);
    }
  }

  setToolState(element) {
    const toolState = csTools.getToolState(element, this.name);
    if (!toolState) {
      csTools.addToolState(element, this.name, { data: [0] });
    }
  }

  clearPreview(elementToSkip) {
    cornerstone.getEnabledElements().forEach(enabledElement => {
      if (enabledElement.element === elementToSkip) {
        cornerstone.updateImage(enabledElement.element);
        return;
      }
      const globalThresholdTool =
        csTools.getToolForElement(enabledElement.element, this.name) || this;
      // Clear previously drawn outline
      if (globalThresholdTool.handles.points.length) {
        globalThresholdTool.handles.points.length = 0;
      }
      cornerstone.updateImage(enabledElement.element);
    });
  }

  setToleranceFunction(toleranceFn) {
    // This will be overridden by the mixin function
  }
  _getTolerance(ctPixelValue) {
    const state = store.getState();
    const selectedRange = state.smartCT?.selectedRange?.range;
    let minTolerance = 0;
    if (
      ctPixelValue > selectedRange?.min &&
      ctPixelValue < selectedRange?.max
    ) {
      minTolerance = Math.min(
        ctPixelValue - selectedRange.min,
        selectedRange.max - ctPixelValue
      );
    }
    const relativeToleranceToSelectedRange =
      minTolerance + (state.segmentation?.sliderRange || 0);

    return relativeToleranceToSelectedRange;
  }

  _getOperationData(evt, opType) {}

  findSegmentIndex(evt) {
    const { configuration } = segmentationModule;
    const radius = configuration.radius;
    const eventData = evt.detail;
    if (eventData) {
      this.thresholdValue = store.getState()?.segmentation?.sliderRange;
      this.lastMousePoint = eventData.currentPoints;
      this.previousImageId = eventData.image.imageId;
      const { rows, columns } = eventData.image;
      const brushCenter = {
        x: Math.floor(eventData.currentPoints.image.x),
        y: Math.floor(eventData.currentPoints.image.y),
      };

      const pointerArray = getCircle(
        radius,
        rows,
        columns,
        brushCenter.x,
        brushCenter.y
      );
      const allRanges = getSmartCTRangeList();
      const smartCTRange = segmentationRangeManager.getWindowRange(
        evt,
        pointerArray,
        brushCenter,
        radius
      );
      const segmentationIndex = allRanges.findIndex(
        r => r.label === smartCTRange.label
      );
      segmentationModule.getters.labelmap2D(eventData.element);

      if (segmentationIndex !== -1) {
        segmentationModule.setters.activeSegmentIndex(
          eventData.element,
          segmentationIndex + 1
        );
      }
      segmentationRangeManager.setSelectedRange(smartCTRange);
    }
    return evt;
  }

  _isMouseOutsideImage({ image, currentPoints }) {
    return (
      currentPoints.image.x <= 0 ||
      currentPoints.image.y <= 0 ||
      currentPoints.image.x >= image.width ||
      currentPoints.image.y >= image.height
    );
  }

  _preMouseMoveCallback(evt) {
    const activeTool = store.getState().infusions?.activeTool;
    if (activeTool !== this.name) {
      this.unbindFromElements();
      this.clearPreview();
      return false;
    }

    const isAnnotationToolEnabled = shouldEnableAnnotationTool(
      evt.detail.element
    );

    if (!isAnnotationToolEnabled) {
      return false;
    }

    // remove lines from other viewports
    this.clearPreview(evt.detail.element);
    if (this._isMouseOutsideImage(evt.detail)) {
      return false;
    }
    this.findSegmentIndex(evt);
  }

  preMouseUpCallback = evt => {
    const { element } = evt.detail;
    const toolState = csTools.getToolState(element, 'stack');
    const toolData = toolState.data[0];
    const index = segmentationModule.getters.activeLabelmapIndex(element);
    const viewportIndex = getViewportIndex(element);
    const activeViewport = store.getState().viewports.viewportSpecificData[
      viewportIndex
    ];
    const studyUid = activeViewport.StudyInstanceUID;
    const seriesUid = activeViewport.SeriesInstanceUID;

    const isAnnotationToolEnabled = shouldEnableAnnotationTool(
      evt.detail.element
    );

    if (!isAnnotationToolEnabled) {
      return false;
    }

    const data = {
      toolType: this.name,
      StudyInstanceUID: studyUid,
      SeriesInstanceUID: seriesUid,
      toolData: toolData,
      element: element,
      labelmapIndex: index,
    };
    saveActionState(data, 'segments', 'Add', false, false);
    // schedule updates on all viewports after processing to reflect labelMap3D changes
    setTimeout(() => {
      cornerstone.getEnabledElements().forEach(enabledElement => {
        cornerstone.updateImage(enabledElement.element);
      });
    }, 0);
  };

  mouseClickCallback(evt) {
    this.mouseUpCallback?.(evt);
  }

  mouseUpCallback = evt => {
    this._mouseUpCallback(evt);
    const { element } = evt.detail;
    const {
      labelmap2D,
      labelmap3D,
      activeLabelmapIndex,
    } = segmentationModule.getters.labelmap2D(element);
    const activeLabelmap = { labelmap2D, labelmap3D, activeLabelmapIndex };
    syncInCrossSectional2DViewports(this.name, evt.detail, activeLabelmap);
  };
}
