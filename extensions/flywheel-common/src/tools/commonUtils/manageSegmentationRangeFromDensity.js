import store from '@ohif/viewer/src/store';
import Redux from '../../redux';
import cornerstone from 'cornerstone-core';
import csTools from 'cornerstone-tools';
import { getDefaultSmartCTRangeList } from '../../utils';
import { getEnabledElement } from '@ohif/extension-cornerstone/src/state';
const { getSmartCTRanges } = Redux.smartCTRanges;
const { setSmartCTRange } = Redux.actions;
const segmentationModule = csTools.getModule('segmentation');

/**
 *
 * @param {Object} evt
 * @param {Array} pointerArray - contains x, y coords under brush
 * @param {Object} brushCenter
 * @param {Number} radius
 * @returns {min: Number, max: Number} - density range covering density value under the brush center
 */
const getDefaultWindowRange = (evt, pointerArray, data, radius) => {
  const state = store.getState();
  const smartCT = state.smartCT;
  const selectedRange = smartCT?.selectedRange;
  if (selectedRange) {
    return selectedRange;
  }
  const defaultRange = getDefaultSmartCTRangeList();
  return defaultRange;
};

/**
 *
 * @param {Object} evt
 * @param {Array} pointerArray - contains x, y coords under brush
 * @param {Object} brushCenter
 * @param {Number} radius
 * @returns {min: Number, max: Number} - density range covering density value under the brush center
 */
const getWindowRange = (evt, pointerArray, brushCenter, radius) => {
  const eventData = evt.detail;
  const activeLabelmapIndex = segmentationModule.getters.activeLabelmapIndex(
    eventData.element
  );

  if (activeLabelmapIndex > 0) {
    return getDefaultWindowRange(evt, pointerArray, brushCenter, radius);
  }

  const density = getDensity(
    evt.detail.image,
    evt.detail.element,
    pointerArray,
    brushCenter,
    radius
  );
  const selectedRange = getRangeFromDensity(density);
  if (!selectedRange) {
    return getDefaultWindowRange(evt, pointerArray, brushCenter, radius);
  }
  return selectedRange;
};

/**
 * to get the density based on first click
 * @param {Object} image
 * @param {HTMLElement} element
 * @param {Array} pointerArray
 * @param {Object} brushCenter
 * @param {Number} radius
 * @param {Object} windowRange
 * @returns density
 */
const getDensity = (image, element, pointerArray, brushCenter, radius) => {
  let density = null;
  if (!image.color) {
    const brushCenterCoords = {
      x: Math.floor(brushCenter.x),
      y: Math.floor(brushCenter.y),
    };
    const storedPixels = cornerstone.getStoredPixels(
      element,
      brushCenterCoords.x,
      brushCenterCoords.y,
      1,
      1
    );

    if (!density) {
      density = storedPixels[0] * image.slope + image.intercept;
    }
  }

  return density;
};

/**
 * to get the density range from store
 * @param {Number} density
 * @returns {Object}
 */
const getRangeFromDensity = density => {
  const smartCTRanges = getSmartCTRanges();
  if (smartCTRanges && smartCTRanges.length) {
    let range = smartCTRanges.find(
      item => item.range.min <= density && item.range.max > density
    );
    if (range) {
      setSelectedRange(range);
      return range;
    }
    // If density does not fall in any of the available ranges, use the closest one
    const minDistances = smartCTRanges.map(item =>
      Math.min(
        Math.abs(item.range.min - density),
        Math.abs(item.range.max - density)
      )
    );
    const minDistance = Math.min.apply(null, minDistances);
    const index = minDistances.indexOf(minDistance);
    if (index > -1) {
      // to update smartCT range selection in UI.
      setSelectedRange(smartCTRanges[index]);
      return smartCTRanges[index];
    }
  }
  return null;
};

const setSelectedRange = CtObj => {
  store.dispatch(setSmartCTRange(CtObj));
};

const activateDefaultSegmentation = element => {
  if (!element) {
    const state = store.getState();
    const activeViewportIndex = state.viewports.activeViewportIndex;
    element = getEnabledElement(activeViewportIndex);
  }

  const activeLabelmapIndex = segmentationModule.getters.activeLabelmapIndex(
    element
  );
  const enabledElement = cornerstone.getEnabledElement(element);
  if (enabledElement.image) {
    if (activeLabelmapIndex === undefined) {
      segmentationModule.getters.labelmap2D(element);
      segmentationModule.setters.activeLabelmapIndex(element, 0);
    }
  }
};

const segmentationRangeManager = {
  getDefaultWindowRange,
  getWindowRange,
  getDensity,
  getRangeFromDensity,
  setSelectedRange,
  activateDefaultSegmentation,
};

export default segmentationRangeManager;
