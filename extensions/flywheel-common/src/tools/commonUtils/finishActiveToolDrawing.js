import csTools from 'cornerstone-tools';

const { state } = csTools.store;

export default function finishActiveToolDrawing(toolName) {
  const activeTools =
    state?.tools.filter(
      item => item.mode === 'active' && toolName === item.name
    ) || [];

  if (activeTools.length) {
    // Forcefully finish drawing if not already finished
    activeTools.forEach(activeTool =>
      activeTool.finishDrawing?.(activeTool.element)
    );
  }
}
