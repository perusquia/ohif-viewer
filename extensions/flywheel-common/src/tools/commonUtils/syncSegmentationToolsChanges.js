import csTools from 'cornerstone-tools';
import saveActionState from './saveActionState';
import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';
import { getSmartCTRangeList } from '../../utils';
import { utils } from '@ohif/core';

const { studyMetadataManager } = utils;

const { drawBrushPixels, getDiffBetweenPixelData } = csTools.importInternal(
  'util/segmentationUtils'
);
const imagePointToPatientPoint = csTools.import(
  'util/imagePointToPatientPoint'
);
const projectPatientPointToImagePlane = csTools.import(
  'util/projectPatientPointToImagePlane'
);
const segmentationModule = csTools.getModule('segmentation');
const targetPixelCache = {};

function getImageDetailsInTarget(pixelIndex, imagePlane, targetImageIds) {
  const rows = Math.floor(pixelIndex / imagePlane.columns);
  const columns = pixelIndex - rows * imagePlane.columns;

  if (imagePlane?.imagePositionPatient) {
    const patientPos = imagePointToPatientPoint(
      {
        x: columns,
        y: rows,
      },
      imagePlane
    );
    const targetSliceIndex = FlywheelCommonUtils.cornerstoneUtils.getNearestSliceFromImagesIds(
      targetImageIds,
      patientPos
    );
    const targetImagePlane = cornerstone.metaData.get(
      'imagePlaneModule',
      targetImageIds[targetSliceIndex]
    );
    const targetPoint = projectPatientPointToImagePlane(
      patientPos,
      targetImagePlane
    );
    return {
      sliceIndex: targetSliceIndex,
      imagePoint: targetPoint,
    };
  }
  return null;
}

function initializeLabelMap(element, displaySet, offline) {
  const state = store.getState();
  let imageIds = [];
  if (displaySet && offline) {
    imageIds = displaySet.images.map(image => image._imageId);
    const activeLabelmapIndex = segmentationModule.getters.activeLabelmapIndexOffline(
      imageIds
    );
    if (activeLabelmapIndex === undefined) {
      segmentationModule.setters.activeLabelmapIndexOffline(imageIds, 0);
    }
  } else {
    const activeLabelmapIndex = segmentationModule.getters.activeLabelmapIndex(
      element
    );
    if (activeLabelmapIndex === undefined) {
      segmentationModule.getters.labelmap2D(element);
      segmentationModule.setters.activeLabelmapIndex(element, 0);
    }
  }

  const allRanges = getSmartCTRangeList();
  const smartCTRange = state.smartCT.selectedRange;
  const segmentationIndex = allRanges.findIndex(
    r => r.label === smartCTRange?.label
  );

  if (segmentationIndex !== -1) {
    if (displaySet && offline) {
      segmentationModule.setters.activeSegmentIndexOffline(
        imageIds[0],
        segmentationIndex + 1
      );
    } else {
      segmentationModule.setters.activeSegmentIndex(
        element,
        segmentationIndex + 1
      );
    }
  }
}

export function getViewportIndex(element) {
  const state = store.getState();
  const viewports = state?.viewports?.layout?.viewports;
  let viewportIndex = 0;

  viewports.some((viewport, index) => {
    const iteratedElement = FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
      index
    );
    if (iteratedElement === element) {
      viewportIndex = index;
      return true;
    }
  });
  return viewportIndex;
}

function syncLabelmap2D(
  labelmap2D,
  previousPixelData,
  labelmapIndex,
  srcActiveSegmentIndex,
  tool,
  srcImage,
  imagePlane,
  targetElements,
  targetDisplaySets = []
) {
  const pixelDifferences = [];
  if (previousPixelData) {
    const diffInPixels = getDiffBetweenPixelData(
      previousPixelData,
      labelmap2D.pixelData
    );
    diffInPixels.forEach(diff => {
      pixelDifferences[diff[0]] = [diff[1], diff[2]];
    });
  }
  const targetsDetails = {};
  if (targetDisplaySets.length) {
    targetElements = targetDisplaySets;
  }
  targetElements.forEach(targetElement => {
    let imageIds = [];
    if (!targetElement.element && targetElement.dataProtocol) {
      imageIds = targetElement.images.map(image => image._imageId);
    } else {
      const toolState = cornerstoneTools.getToolState(
        targetElement.element,
        'stack'
      );
      const stackData = toolState.data[0];
      imageIds = stackData.imageIds;
    }
    const targetInstance = cornerstone.metaData.get('instance', imageIds[0]);
    const seriesInstanceUID = targetInstance.SeriesInstanceUID;

    if (!targetPixelCache[seriesInstanceUID]) {
      targetPixelCache[seriesInstanceUID] = {};
    }
    if (!targetPixelCache[seriesInstanceUID][srcImage.imageId]) {
      targetPixelCache[seriesInstanceUID][srcImage.imageId] = [];
    }

    let targetImageDetails =
      targetPixelCache[seriesInstanceUID][srcImage.imageId];

    labelmap2D.pixelData.forEach((segmentIndex, index) => {
      if (previousPixelData && !pixelDifferences[index]) {
        return;
      }
      let targetPixelDetail = targetImageDetails[index];

      if (!targetPixelDetail) {
        targetPixelDetail = getImageDetailsInTarget(
          index,
          imagePlane,
          imageIds
        );
        targetImageDetails[index] = targetPixelDetail;
      }

      const { sliceIndex, imagePoint } = targetPixelDetail;

      if (!targetsDetails[seriesInstanceUID]) {
        targetsDetails[seriesInstanceUID] = [];
      }
      if (!targetsDetails[seriesInstanceUID][sliceIndex]) {
        targetsDetails[seriesInstanceUID][sliceIndex] = [];
      }
      if (!targetsDetails[seriesInstanceUID][sliceIndex][segmentIndex]) {
        targetsDetails[seriesInstanceUID][sliceIndex][segmentIndex] = [];
      }
      targetsDetails[seriesInstanceUID][sliceIndex][segmentIndex].push([
        Math.round(imagePoint.x),
        Math.round(imagePoint.y),
      ]);
    });
  });

  targetElements.forEach(targetElement => {
    let labelmaps3D = [];
    let imageIds = [];
    if (!targetElement.element && targetElement.dataProtocol) {
      imageIds = targetElement.images.map(image => image._imageId);
      const brushStackState = state.series[imageIds[0]];
      labelmaps3D = brushStackState.labelmaps3D;
    } else {
      const toolState = cornerstoneTools.getToolState(
        targetElement.element,
        'stack'
      );
      const stackData = toolState.data[0];
      imageIds = stackData.imageIds;
      labelmaps3D = segmentationModule.getters.labelmaps3D(
        targetElement.element
      )?.labelmaps3D;
    }
    if (!labelmaps3D) {
      return;
    }
    const targetLabelmap3D = labelmaps3D[labelmapIndex];
    targetLabelmap3D.activeSegmentIndex = srcActiveSegmentIndex;
    if (!targetElement.element && targetElement.dataProtocol) {
      segmentationModule.setters.activeSegmentIndexOffline(
        imageIds[0],
        srcActiveSegmentIndex
      );
    } else {
      segmentationModule.setters.activeSegmentIndex(
        targetElement.element,
        srcActiveSegmentIndex
      );
    }

    const firstInstance = cornerstone.metaData.get('instance', imageIds[0]);
    const targetPointerArray = targetsDetails[firstInstance.SeriesInstanceUID];
    targetPointerArray?.forEach((pointerPerSlice, index) => {
      if (!pointerPerSlice) {
        return;
      }
      const targetImagePlane = cornerstone.metaData.get(
        'imagePlaneModule',
        imageIds[index]
      );
      const targetLabelmap2D = segmentationModule.getters.labelmap2DByImageIdIndex(
        targetLabelmap3D,
        index,
        targetImagePlane.rows,
        targetImagePlane.columns
      );
      pointerPerSlice.forEach((pointerPerSegment, segmentIndex) => {
        drawBrushPixels(
          pointerPerSegment,
          targetLabelmap2D.pixelData,
          segmentIndex,
          targetImagePlane.columns,
          false
        );
        if (!targetLabelmap2D.segmentsOnLabelmap.includes(segmentIndex)) {
          targetLabelmap2D.segmentsOnLabelmap.push(segmentIndex);
        }
      });
    });
  });
  targetElements.forEach(targetElement => {
    if (targetElement?.element) {
      cornerstone.updateImage(targetElement.element);
    }
  });
}

const { state } = segmentationModule;

export function syncInCrossSectional2DViewports(
  tool,
  eventData,
  activeLabelmap
) {
  const {
    labelmap2D,
    labelmap3D,
    activeLabelmapIndex,
    previousPixelData,
  } = activeLabelmap;
  const { element, image } = eventData;

  const imagePlane = cornerstone.metaData.get(
    'imagePlaneModule',
    image.imageId
  );
  const instance = cornerstone.metaData.get('instance', image.imageId);
  const studyMetadata = studyMetadataManager.get(instance.StudyInstanceUID);
  const displaySets = studyMetadata.getDisplaySets();
  const activeDisplaySet = displaySets.find(
    ds => ds.SeriesInstanceUID === instance.SeriesInstanceUID
  );
  const dataProtocol = activeDisplaySet?.dataProtocol;
  const targetElements = [];
  cornerstone.getEnabledElements().forEach(enabledElement => {
    if (enabledElement.element !== element && enabledElement.image) {
      const targetInstance = cornerstone.metaData.get(
        'instance',
        enabledElement.image.imageId
      );
      if (dataProtocol) {
        if (targetInstance.StudyInstanceUID === instance.StudyInstanceUID) {
          targetElements.push(enabledElement);
        }
      }
    }
  });

  let targetDisplaySets = [];
  if (targetElements.length === 0) {
    if (dataProtocol) {
      targetDisplaySets = displaySets.filter(
        ds =>
          ds.StudyInstanceUID === activeDisplaySet.StudyInstanceUID &&
          ds.displaySetInstanceUID !== activeDisplaySet.displaySetInstanceUID &&
          !ds.isDerived
      );
    } else {
      return;
    }
  }

  if (targetDisplaySets.length && !targetElements.length) {
    targetDisplaySets.forEach(targetDisplaySet => {
      initializeLabelMap(null, targetDisplaySet, true);
    });
  } else {
    targetElements.forEach(targetElement => {
      initializeLabelMap(targetElement.element);
    });
  }

  syncLabelmap2D(
    labelmap2D,
    previousPixelData,
    activeLabelmapIndex,
    labelmap3D.activeSegmentIndex,
    tool,
    image,
    imagePlane,
    targetElements,
    targetDisplaySets
  );
}

export default {
  syncInCrossSectional2DViewports,
  getViewportIndex,
};
