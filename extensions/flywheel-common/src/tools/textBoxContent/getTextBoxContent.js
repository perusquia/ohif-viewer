import lineBuilders from './lineBuilders';

const { areaLineBuilder, meanStdLineBuilder, minMaxLineBuilder } = lineBuilders;

/**
 * It iterates on lineBuilders iterator and mount an array of lines
 *
 * @param {Array} iterators Array of line builders
 * @param {Object} context App context
 * @param {boolean} justify to justify lines or not
 * @return {Array<string>} lines
 */
const _iterateLines = (iterators = [], justify = false) => {
  let lines = [];

  iterators.forEach(lineBuilder => {
    const result = lineBuilder();
    if (typeof result === 'string') {
      lines.push(result);
    } else if (Array.isArray(result)) {
      lines.push(...result);
    }
  });
  return lines;
};

/**
 * Copied from csTools/annotation/Elliptical with some Flywheel adjustments
 * It returns a list of strings representing the text box content.
 * @param {boolean} isColorImage
 * @param {Object} { area, mean, stdDev, min, max, meanStdDevSUV }
 * @param {string} modality Study modality
 * @param {boolean} hasPixelSpacing if Study has pixel spacing value
 * @param {*} [options={}] - { showMinMax, showHounsfieldUnits }
 * @returns {string[]}
 */
export default function getTextBoxContent(
  isColorImage,
  {
    area,
    mean = 0,
    stdDev = 0,
    median = 0,
    interQuartileRange = 0,
    min = 0,
    max = 0,
    meanStdDev = {},
    meanStdDevSUV,
  } = {},
  modality,
  hasPixelSpacing,
  options = {}
) {
  const showMinMax = options.showMinMax || false;

  const _mean = meanStdDev.mean || mean;
  const _stdDev = meanStdDev.stdDev || stdDev;
  const _median = meanStdDev.median || median;
  const _interQuartileRange =
    meanStdDev.interQuartileRange || interQuartileRange;
  const _min = meanStdDev.min || min;
  const _max = meanStdDev.max || max;

  const linesIterator = [];
  // get line builders
  const areaBuilder = areaLineBuilder(area, hasPixelSpacing);
  const meanStdBuilder = meanStdLineBuilder(
    _mean,
    _stdDev,
    _median,
    _interQuartileRange,
    meanStdDevSUV,
    modality,
    !isColorImage
  );
  const minMaxBuilder = minMaxLineBuilder(
    _min,
    _max,
    modality,
    !isColorImage && showMinMax
  );

  // default display order
  linesIterator.push(areaBuilder);
  linesIterator.push(meanStdBuilder);
  linesIterator.push(minMaxBuilder);

  return _iterateLines(linesIterator);
}
