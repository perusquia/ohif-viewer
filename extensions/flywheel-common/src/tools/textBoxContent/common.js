import csTools from 'cornerstone-tools';
const numbersWithCommas = csTools.importInternal('util/numbersWithCommas');

export const formatValue = value => value.toFixed(2);

export const formatValueToOneDecimalPlace = value => Number(value).toFixed(1);

export const LENGTH_SPACE_BETWEEN_STR_BLOCKS = 6;
export const MINIMUM_SPACE_BETWEEN_STR_BLOCKS = '  ';

/**
 * It returns a string with given spaceNum spacing added to the right;
 * @param {string} str
 * @return {string}
 */
export const addExtraSpacing = (str = '', spaceNum = 0) =>
  str.padEnd(str.length + spaceNum);

/**
 * It returns a string with default spacing added to the right;
 * @param {string} str
 * @return {string}
 */
export const addDefaultSpacing = str =>
  addExtraSpacing(str, LENGTH_SPACE_BETWEEN_STR_BLOCKS);

/**
 * It creates a string from a list of string with spaces within.
 * @param  {[string]} strings list of strings (blocks)
 * @return {string}
 */
export const inlineSpace = (...strings) => {
  return strings.reduce((acc, value, index, array) => {
    let result = acc;

    if (index < array.length - 1) {
      // not last and valid value (add space)
      if (typeof value === 'string' && value) {
        result += addExtraSpacing(value, LENGTH_SPACE_BETWEEN_STR_BLOCKS);
      }
    } else {
      result += value || '';
    }

    return result;
  }, '');
};

/**
 * Get integer length for given str param. It consider csTools text size to calculate its length.
 *
 * @param {string} str string value
 * @param {Object} context App Context
 * @return {number} string length
 */
export const getStringLength = (str = '', context) =>
  Math.floor(context.measureText(str).width);

/**
 * It gets a max non negative number between source param string and target param string.
 * In case target is smaller than source it returns 0.
 *
 * @param {string} source string source
 * @param {string} target string target to compare with
 * @return {number} Length difference or 0 in case target smaller than source
 */
export const maxStringLengthDifference = (source, target) =>
  Math.max(0, getStringLength(target) - getStringLength(source));

/**
 * It finds the max length among given maxLength param and a set of strings
 * @param {number} maxLength Actual max length to be used on comparison process
 * @param {Object} context App context
 * @param  {[string]} strings List of strings
 * @return {number} Max string length
 */
export const getMaxLength = (maxLength, context, ...strings) => {
  let max = maxLength;

  if (!strings) {
    return max;
  }

  strings.forEach(string => {
    const strLength = getStringLength(string, context);
    max = Math.max(strLength, max);
  });

  return max;
};

/**
 * It mounts a line for textBoxContent. It combines prefix, value(formatted or not), posfix into a string result.
 * @param {?number} value
 * @param {string} [prefix] prefix string
 * @param {string} [posfix] posfix string
 * @param {boolean} [conditional = true] It prevents processing string
 * @param {function} [formatter=numbersWithCommas] value formatter function
 * @return {string | undefined} Line string or undefined (in case invalid params)
 */
export const getLineItem = (
  value,
  prefix,
  posfix,
  conditional = true,
  formatter = value => numbersWithCommas(formatValue(value))
) => {
  if (!conditional || typeof value !== 'number') {
    return;
  }

  const formattedValue = formatter(value);

  if (typeof formattedValue !== 'string') {
    return;
  }

  return (
    `${prefix} `.trimLeft() + formattedValue.trim() + ` ${posfix}`.trimRight()
  );
};
