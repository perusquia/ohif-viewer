import { getLineItem } from '../common';
import * as unitUtils from '../../unit';

/**
 * * It returns a method line builder for given line type.
 * @param {number} min min value
 * @param {number} max max value
 * @param {string} modality Study modality
 * @param {boolean} showMinMax flag to show or not current line
 * @return {function} Area line builder
 */
const getMinMaxBuilder = (min, max, modality, showMinMax) => {
  return () => {
    if (!showMinMax) {
      return;
    }
    const unit = unitUtils.getUnitByModality(modality);
    const minString = getLineItem(min, 'Minimum:', unit);
    const maxString = getLineItem(max, 'Maximum:', unit);

    return [minString, maxString];
  };
};

export default getMinMaxBuilder;
