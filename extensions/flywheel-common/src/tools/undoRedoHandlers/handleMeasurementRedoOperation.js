import OHIF from '@ohif/core';
import csTools from 'cornerstone-tools';
import store from '@ohif/viewer/src/store';

const { getters } = csTools.store;

export default function handleMeasurementRedoOperation(nextAction) {
  const measurementApi = OHIF.measurements.MeasurementApi.Instance;

  const deletedMeasurementIds = store.getState().timepointManager
    .deletedMeasurementIds;
  const tools = getters.mouseTools();
  const actionTool = tools.find(tool => tool.name === nextAction.toolType);

  // Cancel if any debounced function callback
  if (actionTool?.throttledUpdateCachedStats) {
    actionTool.throttledUpdateCachedStats.cancel();
  }

  nextAction.data.active = false;
  nextAction.data.invalidated = false;
  delete nextAction.data.isCreationInProgress; // Omit the attribute while restoring

  if (nextAction?.actionType === 'Add') {
    nextAction.data.lesionNamingNumber = null;
    nextAction.data.measurementNumber = null;

    // If adding measurement as part of redo is there in the list of items pending to delete from server,
    // then remove it from that list.
    // If already deleted from server then add it as new measurement by removing the server "id" attribute.
    const deletedMeasurementId = deletedMeasurementIds.find(
      measurementId => measurementId._id === nextAction.data._id
    );
    let addMeasurement = nextAction.data;
    if (deletedMeasurementId) {
      addMeasurement = {
        ...addMeasurement,
        ...{ id: deletedMeasurementId.id },
      };
      store.dispatch(
        OHIF.redux.actions.removeDeletedMeasurementId(deletedMeasurementId.id)
      );
    } else {
      delete addMeasurement.id;
    }

    measurementApi.addMeasurement(
      nextAction.toolType,
      { ...addMeasurement, ...{ dirty: true } },
      false
    );
    measurementApi.syncMeasurementsAndToolData();
  } else if (nextAction?.actionType === 'Modify') {
    measurementApi.updateMeasurement(
      nextAction.toolType,
      { ...nextAction.data, ...{ dirty: true } },
      true
    );
    OHIF.measurements.MeasurementApi.syncToolStateWithMeasurementData(
      nextAction.data
    );
  } else if (nextAction?.actionType === 'Delete') {
    const { _id } = nextAction.data;
    const measurementTypeId =
      measurementApi.toolsGroupsMap[nextAction.toolType];

    measurementApi.deleteMeasurements(nextAction.toolType, measurementTypeId, {
      toolItemId: _id,
    });
  }

  (nextAction.sub || []).forEach(subAction => {
    handleMeasurementRedoOperation(subAction);
  });
}
