import OHIF from '@ohif/core';
import csTools from 'cornerstone-tools';
import store from '@ohif/viewer/src/store';

const { getters } = csTools.store;

export default function handleMeasurementUndoOperation(lastAction) {
  const measurementApi = OHIF.measurements.MeasurementApi.Instance;

  const deletedMeasurementIds = store.getState().timepointManager
    .deletedMeasurementIds;
  const tools = getters.mouseTools();
  const actionTool = tools.find(tool => tool.name === lastAction.toolType);

  // Cancel if any debounced function callback
  if (actionTool?.throttledUpdateCachedStats) {
    actionTool.throttledUpdateCachedStats.cancel();
  }
  (lastAction.sub || [])
    .slice()
    .reverse()
    .forEach(subAction => {
      handleMeasurementUndoOperation(subAction);
    });

  lastAction.data.active = false;
  lastAction.data.invalidated = false;
  delete lastAction.data.isCreationInProgress; // Omit the attribute while restoring

  if (lastAction.actionType === 'Add') {
    const { _id } = lastAction.data;
    const measurementTypeId =
      measurementApi.toolsGroupsMap[lastAction.toolType];

    measurementApi.deleteMeasurements(lastAction.toolType, measurementTypeId, {
      toolItemId: _id,
    });
  } else if (lastAction.actionType === 'Modify') {
    measurementApi.updateMeasurement(
      lastAction.toolType,
      { ...lastAction.data, ...{ dirty: true } },
      true
    );
    OHIF.measurements.MeasurementApi.syncToolStateWithMeasurementData(
      lastAction.data
    );
  } else if (lastAction.actionType === 'Delete') {
    delete lastAction.data.lesionNamingNumber;
    delete lastAction.data.measurementNumber;

    // If measurement is already saved in server with valid server "id",
    // then adding measurement as part of undo is there in the list of items pending to delete from server,
    // then remove it from that list,
    // if already deleted from server then add it as new measurement by removing the server "id" attribute.
    if (lastAction.data.id) {
      if (
        deletedMeasurementIds.find(
          measurementId => measurementId.id === lastAction.data.id
        )
      ) {
        store.dispatch(
          OHIF.redux.actions.removeDeletedMeasurementId(lastAction.data.id)
        );
      } else {
        delete lastAction.data.id;
      }
    }

    measurementApi.addMeasurement(
      lastAction.toolType,
      { ...lastAction.data, ...{ dirty: true } },
      false
    );
    measurementApi.syncMeasurementsAndToolData();
  }
}
