// TODO nifti segmentation maybe this not really necessary as orientation must be explicity for nifti
/**
 * getValidOrientations - returns an array of valid orientations.
 *
 * @param  {Number[6]} iop The row (0..2) an column (3..5) direction cosines.
 * @return {Number[8][6]} An array of valid orientations.
 */
function getPossibleOrientations(iop) {
  const orientations = [];

  // [0,  1,  2]: 0,   0hf,   0vf
  // [3,  4,  5]: 90,  90hf,  90vf
  // [6, 7]:      180, 270

  orientations[0] = iop;
  orientations[1] = flipImageOrientationPatient.h(iop);
  orientations[2] = flipImageOrientationPatient.v(iop);

  const iop90 = rotateDirectionCosinesInPlane(iop, Math.PI / 2);

  orientations[3] = iop90;
  orientations[4] = flipImageOrientationPatient.h(iop90);
  orientations[5] = flipImageOrientationPatient.v(iop90);

  orientations[6] = rotateDirectionCosinesInPlane(iop, Math.PI);
  orientations[7] = rotateDirectionCosinesInPlane(iop, 1.5 * Math.PI);

  return orientations;
}

/**
 * rotateDirectionCosinesInPlane - rotates the row and column cosines around
 * their normal by angle theta.
 *
 * @param  {Number[6]} iop   The row (0..2) an column (3..5) direction cosines.
 * @param  {Number} theta The rotation magnitude in radians.
 * @return {Number[6]}       The rotate row (0..2) and column (3..5) direction cosines.
 */
const rotateDirectionCosinesInPlane = (iop, theta) => {
  const r = [iop[0], iop[1], iop[2]];
  const c = [iop[3], iop[4], iop[5]];
  const rxc = crossProduct3D(r, c);

  const rRot = rotateVectorAroundUnitVector(r, rxc, theta);
  const cRot = rotateVectorAroundUnitVector(c, rxc, theta);

  return [...rRot, ...cRot];
};

const flipImageOrientationPatient = {
  /**
   * h: Flips ImageOrientationPatient in the horizontal direction.
   * @param {Number[6]} iop - ImageOrientationPatient
   * @returns {Number[6]} The transformed ImageOrientationPatient
   */
  h: iop => {
    return [iop[0], iop[1], iop[2], -iop[3], -iop[4], -iop[5]];
  },
  /**
   * v: Flips ImageOrientationPatient in the vertical direction.
   * @param {Number[6]} iop - ImageOrientationPatient
   * @returns {Number[6]} The transformed ImageOrientationPatient
   */
  v: iop => {
    return [-iop[0], -iop[1], -iop[2], iop[3], iop[4], iop[5]];
  },
  /**
   * hv: Flips ImageOrientationPatient in the horizontal and vertical directions.
   * @param {Number[6]} iop - ImageOrientationPatient
   * @returns {Number[6]} The transformed ImageOrientationPatient
   */
  hv: iop => {
    return [-iop[0], -iop[1], -iop[2], -iop[3], -iop[4], -iop[5]];
  },
};

// TODO nifti segmentation use third party lib
/**
 * crossProduct3D - Returns the cross product of a and b.
 *
 * @param  {Number[3]} a Vector a.
 * @param  {Number[3]} b Vector b.
 * @return {Number[3]}   The cross product.
 */
function crossProduct3D(a, b) {
  return [
    a[1] * b[2] - a[2] * b[1],
    a[2] * b[0] - a[0] * b[2],
    a[0] * b[1] - a[1] * b[0],
  ];
}

// TODO nifti segmentation use third party lib
/**
 * rotateVectorAroundUnitVector - Rotates vector v around unit vector k using
 *                                Rodrigues' rotation formula.
 *
 * @param  {Number[3]} v     The vector to rotate.
 * @param  {Number[3]} k     The unit vector of the axis of rotation.
 * @param  {Number} theta    The rotation magnitude in radians.
 * @return {Number[3]}       The rotated v vector.
 */
function rotateVectorAroundUnitVector(v, k, theta) {
  const cosTheta = Math.cos(theta);
  const sinTheta = Math.sin(theta);
  const oneMinusCosTheta = 1.0 - cosTheta;
  const kdotv = k[0] * v[0] + k[1] * v[1] + k[2] * v[2];
  const vRot = [];
  const kxv = crossProduct3D(k, v);

  for (let i = 0; i <= 2; i++) {
    vRot[i] =
      v[i] * cosTheta + kxv[i] * sinTheta + k[i] * kdotv * oneMinusCosTheta;

    vRot[i] *= -1;
  }

  return vRot;
}

export { getPossibleOrientations, flipImageOrientationPatient };
