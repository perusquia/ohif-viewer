import { getPossibleOrientations } from './orientation';
import cornerstoneTools from 'cornerstone-tools';

const SEG_ERROR = {
  NO_INSERT_METHOD: 'There is no insert method for given data',
  OVERLAPPING_GENERAL_ERROR: 'Can not check overlapping',
};

const SEG_ORIENTATION = {
  PLANAR: 'PLANAR',
  PERPENDICULAR: 'PERPENDICULAR',
  OBLIQUE: 'OBLIQUE',
};

const getNaturalizedSegImageModule = (
  imageFrameDataSegments,
  metadataProvider,
  segmentationIndex = 1,
  isSourceMultiFrame = false
) => {
  const naturalizedSegImageModule = {
    Columns: imageFrameDataSegments[0].columns,
    Rows: imageFrameDataSegments[0].rows,
    PerFrameFunctionalGroupsSequence: [],
    SharedFunctionalGroupsSequence: {
      PlaneOrientationSequence: {
        ImageOrientationPatient: [0, 0, 0, 0, 0, 0],
      },
    },
    SegmentSequence: [],
  };
  const { state } = cornerstoneTools.getModule('segmentation');
  const { colorLutTables } = state;
  for (let i = 0; i < segmentationIndex; i++) {
    naturalizedSegImageModule.SegmentSequence.push({
      ROIDisplayColor: colorLutTables[0][i + 1],
    });
  }
  imageFrameDataSegments.forEach((dataSegment, index) => {
    const { imageId } = dataSegment;

    const instanceData = metadataProvider.get('instance', imageId);

    // TODO volumetric segmentation check ReferencedSOPClassUID and default value
    naturalizedSegImageModule.PerFrameFunctionalGroupsSequence.push({
      DerivationImageSequence: {
        SourceImageSequence: {
          ReferencedSOPClassUID:
            instanceData.ReferencedSOPClassUID || '1.2.840.10008.5.1.4.1.1.4',
          ReferencedSOPInstanceUID: instanceData.ReferencedSOPInstanceUID,
          ReferencedFrameNumber: isSourceMultiFrame ? index + 1 : null,
        },
        DerivationCodeSequence: {
          CodeMeaning: 'Segmentation',
          CodeValue: '',
          CodingSchemeDesignator: '',
        },
      },
      SegmentIdentificationSequence: {
        ReferencedSegmentNumber: segmentationIndex, // only one
      },
    });
  });

  return naturalizedSegImageModule;
};

const getSegmentsPixelData = imageFrameDataSegments => {
  return imageFrameDataSegments.map(dataSegment => {
    const { getPixelData } = dataSegment;
    return getPixelData();
  });
};

const getSourceModules = (imageIds = [], metadataProvider) => {
  const imagePlaneModule = metadataProvider.get(
    'imagePlaneModule',
    imageIds[0]
  );

  const generalSeriesModule = metadataProvider.get(
    'generalSeriesModule',
    imageIds[0]
  );

  return {
    imagePlaneModule,
    generalSeriesModule,
  };
};

const getOrientations = sourceImagePlaneModule => {
  const ImageOrientationPatient = Array.isArray(
    sourceImagePlaneModule.rowCosines
  )
    ? [
        ...sourceImagePlaneModule.rowCosines,
        ...sourceImagePlaneModule.columnCosines,
      ]
    : [
        sourceImagePlaneModule.rowCosines.x,
        sourceImagePlaneModule.rowCosines.y,
        sourceImagePlaneModule.rowCosines.z,
        sourceImagePlaneModule.columnCosines.x,
        sourceImagePlaneModule.columnCosines.y,
        sourceImagePlaneModule.columnCosines.z,
      ];

  // Get IOP from ref series, compute supported orientations:
  return getPossibleOrientations(ImageOrientationPatient);
};

const getSegmentMetadata = (naturalizedSegImageModule, seriesInstanceUid) => {
  const segmentSequence = naturalizedSegImageModule.SegmentSequence;
  let data = [];

  if (Array.isArray(segmentSequence)) {
    data = [undefined, ...segmentSequence];
  } else {
    // Only one segment, will be stored as an object.
    data = [undefined, segmentSequence];
  }

  return {
    seriesInstanceUid,
    data,
  };
};

/**
 * findIndexOfSourceImage - Returns the Cornerstone imageId of the source image.
 *
 * @param  {Object} SourceImageSequence Sequence describing the source image.
 * @param  {String[]} imageIds          A list of imageIds.
 * @param  {Object} metadataProvider    A Cornerstone metadataProvider to query
 *                                      metadata from imageIds.
 * @return {String}                     The corresponding imageId.
 */
function findIndexOfSourceImage(
  SourceImageSequence,
  imageIds,
  metadataProvider
) {
  const {
    ReferencedSOPInstanceUID,
    ReferencedFrameNumber,
  } = SourceImageSequence;

  const isSameFrame = imageId => {
    if (!ReferencedFrameNumber) {
      return true;
    }
    const imageIdFrameNumber = Number(imageId.split('frames/')[1]);
    return imageIdFrameNumber === ReferencedFrameNumber;
  };

  return imageIds.findIndex(imageId => {
    const sopCommonModule = metadataProvider.get('sopCommonModule', imageId);
    if (!sopCommonModule) {
      return;
    }

    return (
      sopCommonModule.sopInstanceUID === ReferencedSOPInstanceUID &&
      isSameFrame(imageId)
    );
  });
}

export default class VolumetricImageSegmentation {
  constructor(
    imageIds,
    imageFrameDataSegments,
    metadataProvider,
    segmentationIndex,
    isSourceMultiFrame = false
  ) {
    this.imageIds = imageIds;
    this.naturalizedSegImageModule = getNaturalizedSegImageModule(
      imageFrameDataSegments,
      metadataProvider,
      segmentationIndex,
      isSourceMultiFrame
    );
    this.sourceModules = getSourceModules(imageIds, metadataProvider);
    // Volumetric(Nifti, Meta-image like MHD) segmentation defaults to planar
    this.orientation = SEG_ORIENTATION.PLANAR;
    this.config = {
      skipOverlapping: true,
      bytesPerLabel: 2, // 2 bytes per label voxel in cst4.
    };

    this.validOrientations = getOrientations(
      this.sourceModules.imagePlaneModule
    );
    this.sharedRows = this.naturalizedSegImageModule.Rows;
    this.sharedColumns = this.naturalizedSegImageModule.Columns;
    this.sliceLength = this.sharedColumns * this.sharedRows;
  }

  isOverlapping() {
    throw Error(SEG_ERROR.OVERLAPPING_GENERAL_ERROR);
  }

  decoratePlanarAndNonOverlapping(
    segmentsOnFrame,
    segmentsOnFrameArray,
    labelmapBufferArray,
    imageSegmentsPixelDataArray,
    metadataProvider
  ) {
    const {
      PerFrameFunctionalGroupsSequence,
      SourceImageSequence,
    } = this.naturalizedSegImageModule;

    // TODO segmentation.  check PerFrameFunctionalGroups data shape
    const getSourceImageSequence = (PerFrameFunctionalGroups = {}) => {
      let response = SourceImageSequence && SourceImageSequence[i];

      if (!response) {
        response = (PerFrameFunctionalGroups.DerivationImageSequence || {})
          .SourceImageSequence;
      }

      return response;
    };

    for (
      let i = 0, groupsLen = PerFrameFunctionalGroupsSequence.length;
      i < groupsLen;
      ++i
    ) {
      const PerFrameFunctionalGroups = PerFrameFunctionalGroupsSequence[i];

      const pixelData = imageSegmentsPixelDataArray[i];
      const segmentIndex =
        PerFrameFunctionalGroups.SegmentIdentificationSequence
          .ReferencedSegmentNumber;

      let _sourceImageSequence = getSourceImageSequence(
        PerFrameFunctionalGroups
      );

      const imageIdIndex = findIndexOfSourceImage(
        _sourceImageSequence,
        this.imageIds,
        metadataProvider
      );

      if (imageIdIndex < 0) {
        // Image not present in stack, can't import this frame.
        continue;
      }

      const byteOffset =
        this.sliceLength * this.config.bytesPerLabel * imageIdIndex; // 2 bytes/pixel
      const labelmap2DView = new Uint16Array(
        labelmapBufferArray[0],
        byteOffset,
        this.sliceLength
      );

      // populate labelmap2DView and segmentsOnFrame based on pixelData pixels
      for (let j = 0, len = pixelData.length; j < len; ++j) {
        if (pixelData[j]) {
          for (let x = j + 1; x < len; ++x) {
            if (pixelData[x]) {
              labelmap2DView[x] = segmentIndex;
            }
          }

          if (!segmentsOnFrame[imageIdIndex]) {
            segmentsOnFrame[imageIdIndex] = [];
          }

          segmentsOnFrame[imageIdIndex].push(segmentIndex);

          break;
        }
      }
    }
  }

  getToolStateDecorateFunction(metadataProvider) {
    let overlapping = false;
    if (!this.config.skipOverlapping) {
      overlapping = this.isOverlapping(
        pixelData,
        this.naturalizedSegImageModule,
        this.imageIds,
        this.validOrientations,
        metadataProvider
      );
    }

    let decorateFunction;

    switch (this.orientation) {
      case SEG_ORIENTATION.PLANAR:
        if (overlapping) {
          throw Error(SEG_ERROR.NO_INSERT_METHOD);
        } else {
          decorateFunction = this.decoratePlanarAndNonOverlapping;
        }
        break;
      default:
        throw Error(SEG_ERROR.NO_INSERT_METHOD);
    }
    return decorateFunction.bind(this);
  }

  generateToolState(imageFrameDataSegment, metadataProvider) {
    const segmentsOnFrameArray = [];
    segmentsOnFrameArray[0] = [];
    const segmentsOnFrame = [];

    const arrayBufferLength =
      this.sliceLength * this.imageIds.length * this.config.bytesPerLabel;
    const labelmapBufferArray = [];
    labelmapBufferArray[0] = new ArrayBuffer(arrayBufferLength);
    const SeriesInstanceUID = this.sourceModules.generalSeriesModule
      .seriesInstanceUID;
    const segMetadata = getSegmentMetadata(
      this.naturalizedSegImageModule,
      SeriesInstanceUID
    );
    const segFramesPixelDataArray = getSegmentsPixelData(imageFrameDataSegment);
    const decorateResponse = this.getToolStateDecorateFunction(
      metadataProvider
    );

    decorateResponse(
      segmentsOnFrame,
      segmentsOnFrameArray,
      labelmapBufferArray,
      segFramesPixelDataArray,
      metadataProvider
    );

    return {
      labelmapBufferArray,
      segMetadata,
      segmentsOnFrame,
      segmentsOnFrameArray,
    };
  }
}
