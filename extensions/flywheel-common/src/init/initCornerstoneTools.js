import cornerstone from 'cornerstone-core';
import _ from 'lodash';
import Redux from '../redux';

const { setActiveTool } = Redux.actions;

export default function initCornerstoneTools(store) {
  cornerstone.events.addEventListener(
    cornerstone.EVENTS.IMAGE_LOADED,
    _.debounce(
      () => {
        store.dispatch(setActiveTool());
      },
      1000,
      { leading: true }
    )
  );

  // After all images loaded, switching between different layouts should enable tools and measurements.
  cornerstone.events.addEventListener(
    cornerstone.EVENTS.ELEMENT_ENABLED,
    _.debounce(() => {
      store.dispatch(setActiveTool());
    }, 1000)
  );
}
