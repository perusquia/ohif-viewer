import Reactive from '../reactive';
import Api from '../http';

/**
 * It initializes some reactive components. On extension initialization
 * @param {object} store app store
 */
function initReactive(store) {
  Reactive.store.createObservable(store);

  Reactive.user.createObservable(Api.services.requestUser);
}

export default initReactive;
