import cornerstone from 'cornerstone-core';
import MultiFrameWadors from '../imageLoader/wadors';

export default function initImageLoader({ appConfig }) {
  MultiFrameWadors.register(cornerstone);
  MultiFrameWadors.clearAllPixelData();
}
