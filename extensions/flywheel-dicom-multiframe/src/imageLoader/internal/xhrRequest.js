import { getOptions } from './options.js';
import cornerstoneWadoImageLoader from 'cornerstone-wado-image-loader';

const { findIndexOfString } = cornerstoneWadoImageLoader.wadors;
import cornerstone from 'cornerstone-core';

function unInt8ArrayConcat(first, second) {
  const firstLength = first.length;
  const result = new Uint8Array(firstLength + second.length);

  result.set(first);
  result.set(second, firstLength);

  return result;
}

/**
 * Convert data to string
 * Original from wadors image loader
 * @param {*} data
 * @param {*} offset
 * @param {*} length
 */
function stringToUInt8Array(str) {
  const data = Uint8Array.from(str, x => x.charCodeAt(0));

  return data;
}

/**
 * Method to find header
 * Original from wadors image loader
 * @param {string} header
 */
function findBoundary(header) {
  for (let i = 0; i < header.length; i++) {
    if (header[i].substr(0, 2) === '--') {
      return header[i];
    }
  }
}

/**
 * Method to find content type
 * Original from wadors image loader
 * @param {string} header
 */
function findContentType(header) {
  for (let i = 0; i < header.length; i++) {
    if (header[i].substr(0, 13) === 'Content-Type:') {
      return header[i].substr(13).trim();
    }
  }
}

/**
 * Convert data to string
 * Original from wadors image loader
 * @param {*} data
 * @param {*} offset
 * @param {*} length
 */
function uint8ArrayToString(data, offset, length) {
  offset = offset || 0;
  length = length || data.length - offset;
  let str = '';

  for (let i = offset; i < offset + length; i++) {
    str += String.fromCharCode(data[i]);
  }

  return str;
}

/**
 * Evaluate the loaded buffer, parse for boundary and split the frames from it for displaying.
 * @param {Object} bufferData
 * @param {Array} frames
 * @param {fn} handleFrameData
 */
function evaluteLoadedBytes(bufferData, frames, handleFrameData) {
  while (
    (bufferData.originalData.bytesRead >
      bufferData.originalData.nextReadOffset +
        bufferData.originalData.frameTotalLength ||
      bufferData.originalData.isDownloaded) &&
    frames.length > 0
  ) {
    let endIndex = 0;
    if (bufferData.originalData.frameTotalLength === 0) {
      // First look for the multipart mime header
      const tokenIndex = findIndexOfString(
        bufferData.originalData.buffer,
        '\r\n\r\n',
        bufferData.originalData.nextReadOffset
      );

      if (tokenIndex === -1) {
        break;
      }
      const header = uint8ArrayToString(
        bufferData.originalData.buffer,
        bufferData.originalData.nextReadOffset,
        tokenIndex
      );
      // Now find the boundary  marker
      const split = header.split('\r\n');

      if (!bufferData.frameInfo.boundary) {
        bufferData.frameInfo.boundary = findBoundary(split);
        if (!bufferData.frameInfo.boundary) {
          break;
        }
      }
      let offset = tokenIndex + 4; // skip over the \r\n\r\n
      bufferData.frameInfo.startOffset =
        offset - bufferData.originalData.nextReadOffset;
      bufferData.imageInfo.contentType = findContentType(split);
    }

    // find the terminal boundary marker
    endIndex = findIndexOfString(
      bufferData.originalData.buffer,
      bufferData.frameInfo.boundary,
      bufferData.frameInfo.startOffset + bufferData.originalData.nextReadOffset
    );
    if (endIndex === -1) {
      break;
    }
    bufferData.originalData.frameTotalLength =
      endIndex - bufferData.originalData.nextReadOffset;
    // Remove \r\n from the length
    bufferData.frameInfo.imageLength =
      endIndex -
      2 -
      (bufferData.frameInfo.startOffset +
        bufferData.originalData.nextReadOffset);

    const frameInfo = bufferData.frameInfo;
    const bufferLength =
      frameInfo.startOffset +
      frameInfo.imageLength +
      frameInfo.boundaryBuffer.length;
    const length = bufferLength % 2 !== 0 ? bufferLength + 1 : bufferLength;

    const frameBuffer = new Uint8Array(length);
    frameBuffer.set(
      bufferData.originalData.buffer.slice(
        bufferData.originalData.nextReadOffset,
        endIndex
      )
    );

    frameBuffer.set(
      frameInfo.boundaryBuffer,
      frameInfo.startOffset + frameInfo.imageLength
    );

    if (length > bufferLength) {
      frameBuffer.set([0], length - 1);
    }

    bufferData.originalData.nextReadOffset = endIndex;
    const currentFrame = parseInt(frames.splice(0, 1)[0], 10);
    handleFrameData(
      currentFrame,
      frameBuffer.buffer,
      frameInfo,
      bufferData.imageInfo
    );
  }
}

function xhrRequest(
  url,
  imageId,
  bufferData,
  triggerBufferEvaluation,
  headers = {},
  params = {}
) {
  const options = getOptions();

  headers.authorization = options.beforeFetch().Authorization;

  return new Promise((resolve, reject) => {
    fetch(url, {
      method: 'GET',
      headers,
    })
      .then(response => {
        if (response.ok) {
          // Parse the boundary from header for splitting the frames
          const components =
            response.headers.get('content-type')?.split(';') || [];
          const boundaryComponent =
            components.find(component => component.includes('boundary=')) || '';
          const boundary =
            (boundaryComponent.split('boundary=') || [])[1] || null;
          bufferData.frameInfo.boundary =
            boundary && !boundary.includes('--') ? '--' + boundary : boundary;
          if (bufferData.frameInfo.boundary) {
            // Keep the boundary buffer for appending to the splitted frames buffer
            // to make it consistent with the single frame buffer format.
            bufferData.frameInfo.boundaryBuffer = stringToUInt8Array(
              '\r\n' + bufferData.frameInfo.boundary + '--'
            );
          }
          const reader = response.body.getReader();

          let bytesRead = 0;

          params.deferred = {
            resolve,
            reject,
          };
          params.url = url;
          params.imageId = imageId;

          if (options.onloadstart) {
            options.onloadstart(event, params);
          } // Event

          const eventData = {
            url,
            imageId,
          };

          cornerstone.triggerEvent(
            cornerstone.events,
            'cornerstoneimageloadstart',
            eventData
          );

          reader
            .read()
            .then(
              function processData({ done, value }) {
                if (done) {
                  bufferData.originalData.isDownloaded = true;
                  reader.cancel();
                  if (options.onloadend) {
                    options.onloadend(event, params);
                  }

                  const eventData = {
                    url,
                    imageId,
                  };

                  // Event
                  cornerstone.triggerEvent(
                    cornerstone.events,
                    'cornerstoneimageloadend',
                    eventData
                  );

                  resolve();
                  return;
                }
                if (bytesRead === 0) {
                  triggerBufferEvaluation();
                }
                bytesRead += value.length;
                let length =
                  bufferData.originalData.frameTotalLength *
                    bufferData.totalFrames +
                  100;
                if (
                  bufferData.originalData.frameTotalLength > 0 &&
                  length > bufferData.originalData.buffer.length
                ) {
                  const result = new Uint8Array(length);
                  result.set(bufferData.originalData.buffer);
                  bufferData.originalData.buffer = result;
                }
                if (bytesRead > bufferData.originalData.buffer.length) {
                  const result = new Uint8Array(bytesRead + value.length); // Assigning some additional value length buffer
                  result.set(bufferData.originalData.buffer);
                  bufferData.originalData.buffer = result;
                }
                bufferData.originalData.buffer.set(
                  value,
                  bytesRead - value.length
                );
                bufferData.originalData.bytesRead = bytesRead;

                return reader.read().then(processData);
              },
              function(reason) {
                reader.cancel();
                reject(reason);
              }
            )
            .catch(error => {
              const errorMessage = error?.message || error;
              const errorDescription = `Failed fetch the image(s) with api '${url}' due to '${errorMessage}'`;

              reject(new Error(errorDescription));
            });
        } else {
          const errorDescription = `Failed fetch the image(s) with api '${url}' due to '${response.statusText}'`;
          reject(new Error(errorDescription));
        }
      })
      .catch(error => {
        const errorMessage = error?.message || error;
        const errorDescription = `Failed fetch the image(s) with api '${url}' due to '${errorMessage}'`;

        reject(new Error(errorDescription));
      });
  });
}

export { xhrRequest, evaluteLoadedBytes };
