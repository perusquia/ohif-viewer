import { xhrRequest, evaluteLoadedBytes } from './xhrRequest.js';
import { setOptions, getOptions } from './options.js';

const internal = {
  xhrRequest,
  evaluteLoadedBytes,
  setOptions,
  getOptions,
};

export { setOptions, getOptions, xhrRequest, evaluteLoadedBytes, internal };
