import { xhrRequest, evaluteLoadedBytes } from '../internal/index.js';

const pixelDataManager = {};
const requestInPlace = {};
const queueRequests = [];
const MAX_ALLOWED_FRAMES = 500;
const FRAMES_SPLIT_REGEX = '/frames/';

/**
 * Resolve the frame buffer promise with the data received and update the cache
 * @param {number} frame
 * @param {Object} imageBuffer
 * @param {Object} frameInfo
 * @param {Object} imageInfo
 */
const handleFrameData = (frame, imageBuffer, frameInfo, imageInfo) => {
  let offset = frameInfo.startOffset;

  if (frameInfo.imageLength <= 0) {
    pixelDataManager[imageInfo.sopInstanceUID][frame].reject(
      new Error(
        'Invalid response for frame ' +
          frame +
          ' - failed due to missing the terminating boundary'
      )
    );
  }

  let length = frameInfo.imageLength;

  let source = new Uint8Array(imageBuffer, offset, length);

  offset = source.byteOffset;
  length = source.length;

  // If the original data was 8 bit and the bit allocated metadata forcefully updated to 16
  // bit, then do the conversion of 8 bit to 16 bit buffer.
  // If the buffer is not compressed, again converting it to 8-bit array for making the output image frame data(returned from
  //cornerstoneWadoImageLoader.createImage) as properly in the 16-bit format.
  if (
    imageInfo.bitsAllocated === 16 &&
    length === imageInfo.rows * imageInfo.columns
  ) {
    let modifiedData = Uint16Array.from(source);
    modifiedData = new Uint8Array(modifiedData.buffer);
    source = modifiedData;
  }
  pixelDataManager[imageInfo.sopInstanceUID][frame].isFetching = false;

  // return the info for this pixel data
  pixelDataManager[imageInfo.sopInstanceUID][frame].resolve({
    contentType: imageInfo.contentType,
    imageFrame: {
      pixelData: source,
    },
  });
};

/**
 * Create multi-frame bulk request url from the original request by considering the number of frames
 * and maximum allowed frames.
 * @param {string} originalUrl
 * @param {string} sopInstanceUID
 * @param {number} currentFrame
 * @param {number} numberOfFrames
 * @returns the multi-frame request url.
 */
const createMultiFrameUrl = (
  originalUrl,
  sopInstanceUID,
  currentFrame,
  numberOfFrames
) => {
  const maxFrame = Math.min(MAX_ALLOWED_FRAMES, numberOfFrames);
  const minFrame = 1;
  // Frames are arranging in the request url based on adjacent frame first.
  const frames = [];

  // If any ascending/descending frame already there, then take adjacent descending/ascending frame
  // to consider maximum possible number of frames in each single request.
  for (let i = 1; i < numberOfFrames; i++) {
    const descendingFrame = currentFrame - i;
    const ascendingFrame = currentFrame + i;
    if (descendingFrame >= minFrame) {
      if (!pixelDataManager[sopInstanceUID][descendingFrame]) {
        frames.push(descendingFrame);
      }
    }
    if (ascendingFrame <= numberOfFrames) {
      if (!pixelDataManager[sopInstanceUID][ascendingFrame]) {
        frames.push(ascendingFrame);
      }
    }
    if (frames.length >= maxFrame) {
      break;
    }
  }

  const multiFrameUrl = frames.length
    ? `${originalUrl},${frames.join(',')}`
    : originalUrl;

  return multiFrameUrl;
};

/**
 * Evaluate the buffer loaded and split per frame
 * @param {Object} bufferData
 * @param {Array} evaluateFrames
 * @param {boolean} isPrefetch
 */
const evaluateBuffer = (bufferData, evaluateFrames, isPrefetch) => {
  setTimeout(
    () => {
      evaluteLoadedBytes(bufferData, evaluateFrames, handleFrameData);
      if (evaluateFrames.length > 0) {
        evaluateBuffer(bufferData, evaluateFrames, true);
      } else {
        bufferData.originalData.buffer = null;
      }
    },
    isPrefetch ? 500 : 10
  );
};

/**
 * Load the image with the multi-frame bulk request if already not triggered for the same sop image.
 * @param {Object} bufferData
 * @param {Array} evaluateFrames
 * @param {string} multiFrameUrl
 * @param {string} imageId
 * @param {Object} headers
 * @param {number} currentFrame
 */
const loadImage = (
  bufferData,
  evaluateFrames,
  multiFrameUrl,
  imageId,
  headers,
  currentFrame
) => {
  const triggerBufferEvaluation = (isPrefetch = false) => {
    evaluateBuffer(bufferData, evaluateFrames, isPrefetch);
  };
  // If already a request is in progress, then put it in queue and trigger once the current is done.
  if (requestInPlace[bufferData.imageInfo.sopInstanceUID]) {
    queueRequests.push({
      multiFrameUrl,
      imageId,
      bufferData,
      headers,
      currentFrame,
    });
  } else {
    requestInPlace[bufferData.imageInfo.sopInstanceUID] = true;
    const loadPromise = xhrRequest(
      multiFrameUrl,
      imageId,
      bufferData,
      triggerBufferEvaluation,
      headers
    );
    loadPromise.then(
      function() {
        requestInPlace[bufferData.imageInfo.sopInstanceUID] = false;
        if (queueRequests.length) {
          const nextRequest = queueRequests.splice(0, 1)[0];
          let uriParams =
            nextRequest.multiFrameUrl.split(FRAMES_SPLIT_REGEX) || [];
          const nextFramesStr = uriParams[1];
          const nextFrames = nextFramesStr.split(',');
          loadImage(
            nextRequest.bufferData,
            nextFrames,
            nextRequest.multiFrameUrl,
            nextRequest.imageId,
            nextRequest.headers,
            nextRequest.currentFrame
          );
        }
      },
      error =>
        pixelDataManager[bufferData.imageInfo.sopInstanceUID][
          currentFrame
        ].reject(error)
    );
  }
};

/**
 * This is a copy from wadoImageLoader with some adjustments.
 * To keep original implementation it was added two strategies into it:
 * 1. One to store given series promise
 * 2. Converting from 8-bit to 16-bit buffer allocation( MPR re-construction
 *    with VTK js will not supports 8-bit data). For this case to prevent calling server many times we use strategy 1.
 * @param {*} uri
 * @param {*} imageId
 * @param {*} mediaType
 * @param {boolean} isBulkLoad
 */

function getPixelData(
  uri,
  imageId,
  mediaType = 'application/octet-stream',
  isBulkLoad = true
) {
  const headers = {
    accept: mediaType,
  };

  const instance = cornerstone.metaData.get('instance', imageId) || {};
  const { BitsAllocated, Rows, Columns } = instance;
  const numberOfFrames = instance.NumberOfFrames || 1;
  const sopInstanceUID = instance.SOPInstanceUID;
  if (!pixelDataManager[sopInstanceUID]) {
    pixelDataManager[sopInstanceUID] = [];
  }

  const uriParams = uri.split(FRAMES_SPLIT_REGEX) || [];
  let multiFrameUrl = '';
  const currentFrame = parseInt(uriParams[1], 10);

  const frameFetchInfo = pixelDataManager[sopInstanceUID][currentFrame];
  if (!frameFetchInfo) {
    if (numberOfFrames >= currentFrame) {
      multiFrameUrl = createMultiFrameUrl(
        uri,
        sopInstanceUID,
        currentFrame,
        numberOfFrames
      );
    }
  }

  // chain already existing promise to this new request
  if (frameFetchInfo) {
    return frameFetchInfo.promise;
  }

  const bufferData = {
    originalData: {
      buffer: new Uint8Array(),
      nextReadOffset: 0,
      frameTotalLength: 0,
      bytesRead: 0,
      isDownloaded: false,
    },
    frameInfo: {
      startOffset: 0,
      imageLength: 0,
      boundaryBuffer: null,
    },
    imageInfo: {
      bitsAllocated: BitsAllocated,
      rows: Rows,
      columns: Columns,
      sopInstanceUID,
    },
    totalFrames: numberOfFrames,
  };

  const framesStr = multiFrameUrl.split(FRAMES_SPLIT_REGEX)[1];
  const frames = framesStr.split(',');
  for (let i = 0; i < frames.length; i++) {
    const frame = parseInt(frames[i], 10);
    if (pixelDataManager[sopInstanceUID][frame]) {
      continue;
    }
    const promise = new Promise((resolve, reject) => {
      if (frame === currentFrame) {
        const evaluateFrames = frames.slice(0);
        loadImage(
          bufferData,
          evaluateFrames,
          multiFrameUrl,
          imageId,
          headers,
          reject
        );
      }
      if (!pixelDataManager[sopInstanceUID][frame]) {
        pixelDataManager[sopInstanceUID][frame] = {
          isFetching: true,
          promise: null,
          resolve,
          reject,
        };
      }
    });
    pixelDataManager[sopInstanceUID][frame].promise = promise;
  }

  return pixelDataManager[sopInstanceUID][currentFrame].promise;
}

/**
 * Clear all existing data into pixelDataManager
 */
const clearAllPixelData = () => {
  Object.keys(pixelDataManager).forEach(key => {
    delete pixelDataManager[key];
  });
};

export { getPixelData, clearAllPixelData };
