import { getPixelData, clearAllPixelData } from './getPixelData.js';
import loadImage from './loadImage.js';
import register from './register.js';

export default {
  getPixelData,
  clearAllPixelData,
  loadImage,
  register,
};
