import makeSopInstanceList from './makeSopInstanceList';

const adapters = {
  makeSopInstanceList,
};

export default adapters;
