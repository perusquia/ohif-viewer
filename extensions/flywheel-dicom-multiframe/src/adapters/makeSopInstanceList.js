import OHIF from '@ohif/core';
import Utils from '../utils';

/**
 * It creates the separate SOP instances for each frame based data.
 * @param {Array<Object>} originalSopInstanceList list of instance (tags not naturalized)
 * @param {Object} study given study
 */
export default function makeSopInstanceList(
  originalSopInstanceList = [],
  study
) {
  const instances = [];
  const { NumberOfFrames } =
    originalSopInstanceList.length > 0 ? originalSopInstanceList[0] : {};
  if (NumberOfFrames > 1) {
    originalSopInstanceList.forEach(sopInstance => {
      const instance = {
        ...sopInstance,
        getImageId: function(frame) {
          const wadorsId = OHIF.utils.getWADORSImageId(
            this,
            frame >= 0 ? frame : 0
          );
          return Utils.ImageIdUtils.replaceToMultiFrameIdScheme(wadorsId);
        },
      };
      instances.push(instance);
    });
  } else {
    instances.push(...originalSopInstanceList);
  }

  return instances;
}
