import cloneDeep from 'lodash.clonedeep';

function getOpenLayersStyle(feature, styleOptions) {
  const style = feature.getStyle();
  if (typeof (style) !== 'object') {
    return style;
  }

  if ('stroke' in styleOptions) {
    const stroke = style.getStroke();
    stroke.setColor(styleOptions.stroke.color);
    stroke.setWidth(styleOptions.stroke.width);
    style.setStroke(stroke);
  }

  if ('fill' in styleOptions) {
    const fill = style.getFill();
    fill.setColor(styleOptions.fill.color);
    style.setFill(fill);
  }
  return style;
}

const featureOperations = {
  setFeatureColor(feature, color) {
    let strokeColor = cloneDeep(color);
    strokeColor = strokeColor.substring(0, strokeColor.lastIndexOf(',')) + ', 1)';

    const styleOptions = {
      stroke: {
        color: strokeColor,
        width: 2,
      },
      fill: {
        color: color,
      },
    };

    feature.setStyle(getOpenLayersStyle(feature, styleOptions));
    feature.set('styleOptions', styleOptions);
  }
}

export default featureOperations;
