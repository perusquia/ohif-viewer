import { toolProp } from '../toolUtils';

import { microscopyEventRegister } from '../../viewportInstance';

export default function(lastAction) {
  lastAction.data.active = false;
  lastAction.data.invalidated = false;
  delete lastAction.data.isCreationInProgress; // Omit the attribute while restoring

  if (lastAction.data.data) {
    const uid = lastAction.data.data.uid;

    if (lastAction.actionType === 'Add') {
      microscopyEventRegister.viewportInstance.removeROI(uid);
    } else if (lastAction.actionType === 'Modify') {
      // TODO - Need to update when the measurement modification support enabled
    } else if (lastAction.actionType === 'Delete') {
      microscopyEventRegister.viewportInstance.addROI(
        lastAction.data.data,
        toolProp().styleOptions
      );
    }
  }
}
