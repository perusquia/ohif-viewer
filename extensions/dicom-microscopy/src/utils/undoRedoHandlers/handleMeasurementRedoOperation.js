import { toolProp } from '../toolUtils';

import { microscopyEventRegister } from '../../viewportInstance';

export default function(nextAction) {
  nextAction.data.active = false;
  nextAction.data.invalidated = false;
  delete nextAction.data.isCreationInProgress; // Omit the attribute while restoring

  if (nextAction.data.data) {
    const uid = nextAction.data.data.uid;

    if (nextAction?.actionType === 'Add') {
      microscopyEventRegister.viewportInstance.addROI(
        nextAction.data.data,
        toolProp().styleOptions
      );
    } else if (nextAction?.actionType === 'Modify') {
      // TODO - Need to update when the measurement modification support enabled
    } else if (nextAction?.actionType === 'Delete') {
      microscopyEventRegister.viewportInstance.removeROI(uid);
    }
  }
}
