import handleUndoRedoOperation from './handleUndoRedoOperation';

const undoRedoHandlers = {
  handleUndoRedoOperation,
};

export default undoRedoHandlers;
