import OHIF from '@ohif/core';


const getMeasurement = (measurementDetails, searchByUuid = false) => {
  const { MeasurementApi } = OHIF.measurements;
  const measurementApi = MeasurementApi.Instance;
  if (!measurementApi) {
    console.warn('Measurement API is not initialized');
    return;
  }

  const { measurementData, toolType, uuid } = measurementDetails;

  const collection = measurementApi.tools[toolType];

  // Stop here if the tool data shall not be persisted (e.g. temp tools)
  if (!collection) return;

  if (searchByUuid) {
    return collection.find(t => t.uuid === uuid);
  }
  return collection.find(t => t._id === measurementData._id);
}

const getMeasurementByUuid = (uuid) => {
  const { MeasurementApi } = OHIF.measurements;
  const measurementApi = MeasurementApi.Instance;
  if (!measurementApi) {
    console.warn('Measurement API is not initialized');
    return;
  }
  return measurementApi.getAllMeasurements().find(measurement => measurement.uuid === uuid);
}

const getFilteredMeasurements = (filterIds, includes = true) => {
  const { MeasurementApi } = OHIF.measurements;
  const measurementApi = MeasurementApi.Instance;
  if (!measurementApi) {
    console.warn('Measurement API is not initialized');
    return [];
  }

  return measurementApi.getAllMeasurements()
    .filter(measurement => includes ? filterIds.includes(measurement._id)
      : !filterIds.includes(measurement._id));
}

export { getMeasurement, getMeasurementByUuid, getFilteredMeasurements };
