import cloneDeep from 'lodash.clonedeep';
import { measurements } from '@ohif/core';
import store from '@ohif/viewer/src/store';
import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';
import { addLabel } from '../annotations/annotationLabellingHandler';
import { getToolInstance } from '../tools/utils';
import { globalDicomMicroscopyToolStateManager } from '../toolStateManager';
import { getAllViewports } from '../activeMicroscopyViewports';
import { getMeasurementByUuid } from './measurementUtils';
import featureOperations from './FeatureOperations';

const { setNextPalette } = FlywheelCommonRedux.actions;
const { selectActiveViewportIndex, selectViewportSpecificData } = FlywheelCommonRedux.selectors;
const { MeasurementHandlers, getImageIdForImagePath } = measurements;


/**
 * Callback on adding a measurement
 * @param {Object} event - custom event
 */
const onMeasurementAdded = event => {
  const geometryData = cloneDeep(event.detail.payload);

  const fwMeasurement = getMeasurementByUuid(geometryData.uid);
  if (fwMeasurement) {
    const imageId = getImageIdForImagePath(fwMeasurement.imagePath);
    const smMeasurement = globalDicomMicroscopyToolStateManager.getToolStateForMeasurement(
      imageId,
      fwMeasurement.toolType,
      fwMeasurement.uuid
    );
    if (!smMeasurement) {
      return;
    }

    // Measurement visibility is handled by adding/removing ROI. This checking is
    // done so that measurement is not added when it is made visible from measurement panel.
    if (
      smMeasurement.updateFWMeasurement !== false ||
      !smMeasurement.geometryData
    ) {
      const currentViewport = getAllViewports().find(
        viewport => viewport.container === event.currentTarget
      );
      if (
        getSignificantViewportIndex(smMeasurement) === currentViewport.index
      ) {
        updateMeasurementData(imageId, fwMeasurement, geometryData);
        if (!fwMeasurement.location) {
          addLabel(event, fwMeasurement);
        }
      }

      const toolInstance = getToolInstance(smMeasurement.toolType);
      getAllViewports().forEach(viewport => {
        const viewportData = store.getState().viewports.viewportSpecificData[
          viewport.index
        ];
        if (
          viewportData?.SeriesInstanceUID === fwMeasurement.SeriesInstanceUID
        ) {
          if (!viewport.viewerInstance.getROI(fwMeasurement.uuid)) {
            toolInstance.createRoi(
              { ...smMeasurement, geometryData },
              viewport.viewerInstance
            );
          }
        }
      });
    }

    smMeasurement.updateFWMeasurement = true;
  }
};

/**
 * Callback on removing a measurement
 * @param {Object} event - custom event
 */
const onMeasurementRemoved = event => {
  // Remove measurement corresponding to deleted ROI from measurement collection
  const geometryData = event.detail.payload;
  const fwMeasurement = getMeasurementByUuid(geometryData.uid);

  if (fwMeasurement) {
    const imageId = getImageIdForImagePath(fwMeasurement.imagePath);
    const smMeasurement = globalDicomMicroscopyToolStateManager.getToolStateForMeasurement(
      imageId,
      fwMeasurement.toolType,
      fwMeasurement.uuid
    );
    if (!smMeasurement) {
      return;
    }

    // Delete only if updateFWMeasurement is not false. updateFWMeasurement is
    // set as false for operations from measurement panel as measurement
    // collection is already updated for those operations.
    if (smMeasurement.updateFWMeasurement !== false) {
      MeasurementHandlers.onRemoved({
        detail: {
          toolName: fwMeasurement.toolType,
          measurementData: fwMeasurement,
        },
      });
    }
    smMeasurement.updateFWMeasurement = true;
  }
};

/**
 * Callback on modifying a measurement
 * @param {Object} event - custom event
 */
const onMeasurementModified = event => {
  // Update measurement corresponding to deleted ROI from measurement collection
  const geometryData = cloneDeep(event.detail.payload);
  const fwMeasurement = getMeasurementByUuid(geometryData.uid);

  if (fwMeasurement) {
    const imageId = getImageIdForImagePath(fwMeasurement.imagePath);
    const smMeasurement = globalDicomMicroscopyToolStateManager.getToolStateForMeasurement(
      imageId,
      fwMeasurement.toolType,
      fwMeasurement.uuid
    );
    if (!smMeasurement) {
      return;
    }

    // Update only if updateFWMeasurement is not false. updateFWMeasurement is
    // set as false for operations from measurement panel as measurement
    // collection is already updated for those operations.
    if (smMeasurement.updateFWMeasurement !== false) {
      const currentViewport = getAllViewports().find(
        viewport => viewport.container === event.currentTarget
      );
      if (
        getSignificantViewportIndex(smMeasurement) === currentViewport.index
      ) {
        updateMeasurementData(imageId, fwMeasurement, geometryData);
      }
    }
    smMeasurement.updateFWMeasurement = true;
  }
};

const getSignificantViewportIndex = (smMeasurement) => {
  let viewportIndex = -1;

  const activeViewportIndex = selectActiveViewportIndex(store.getState());
  const viewportSpecificData = selectViewportSpecificData(store.getState());

  // If measurement exists in active viewport, activeViewportIndex is returned,
  // else index of first viewport having the measurement is returned.
  if (viewportSpecificData[activeViewportIndex].SeriesInstanceUID ===
    smMeasurement.SeriesInstanceUID) {
    viewportIndex = activeViewportIndex;
  } else {
    viewportIndex = Object.keys(viewportSpecificData).findIndex(key =>
      viewportSpecificData[key].SeriesInstanceUID === smMeasurement.SeriesInstanceUID);
  }
  return viewportIndex;
}

const updateMeasurementData = (imageId, fwMeasurement, geometryData) => {
  const measurementData = { ...fwMeasurement, active: false };

  const toolInstance = getToolInstance(measurementData.toolType);
  toolInstance?.updateFWMeasurementHandles(measurementData,
    geometryData);

  // Persist geometry data to interact with ROI later.
  globalDicomMicroscopyToolStateManager.updateToolState(imageId, fwMeasurement.toolType,
    { ...measurementData, geometryData })

  MeasurementHandlers.onModified({
    detail: {
      toolName: fwMeasurement.toolType,
      measurementData
    }
  })
}

/**
 * Callback on starting annotation draw
 * @param {Object} event - custom event
 */
const onDrawStarted = event => {
  const toolInstance = getToolInstance();
  if (!toolInstance) {
    return;
  }
  const measurementData = toolInstance.createMeasurementData(event.feature);

  const colorPalette = store.getState().colorPalette;
  const color = colorPalette.roiColor.next;
  store.dispatch(setNextPalette());

  featureOperations.setFeatureColor(event.feature, color);
  measurementData.color = color;

  MeasurementHandlers.onAdded({
    detail: {
      toolName: measurementData.toolType,
      measurementData
    }
  })

  // Persist FW measurement details along with geometry data (will be added on
  // ROI added event ) to interact with ROI later.
  const imageId = getImageIdForImagePath(measurementData.imagePath);
  globalDicomMicroscopyToolStateManager.addToolState(imageId, measurementData.toolType,
    measurementData);

}

const onDrawAborted = event => {
  const fwMeasurement = getMeasurementByUuid(event.feature.id_);

  // If the draw operation is aborted in between, remove corresponding measurement
  // from measurement panel as we are adding the measurement on draw start.
  MeasurementHandlers.onRemoved({
    detail: {
      toolName: fwMeasurement.toolType,
      measurementData: fwMeasurement
    }
  })
}

export {
  onMeasurementAdded, onMeasurementRemoved, onMeasurementModified,
  onDrawStarted, onDrawAborted
};
