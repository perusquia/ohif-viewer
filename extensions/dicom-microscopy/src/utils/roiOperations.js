import { globalDicomMicroscopyToolStateManager } from '../toolStateManager';

export const removeAllOpenLayerROIs = (imageId, viewerInstance) => {
  const imageToolState = globalDicomMicroscopyToolStateManager.getImageToolState(imageId) || {};

  // Update 'updateFWMeasurement' flag so that stored measurement data is not
  // removed on removing ROis in viewport.
  Object.keys(imageToolState).forEach(tool => {
    imageToolState[tool]?.data.forEach(data => {
      data.updateFWMeasurement = false;
    })
  });
  viewerInstance.removeAllROIs();
}
