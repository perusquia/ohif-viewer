export const toolProp = () => {
  const data = {
    styleOptions: {
      stroke: {
        color: '#ffff00',
        width: 2,
      },
      fill: {
        color: 'rgba(255,255,255,0.0)',
      },
    },
  };
  return data;
};

const emptyFill = {
  color: 'rgba(255,255,255,0.0)',
};

const defaultStroke = {
  color: '#ffff00',
  width: 1,
};

export const emptyStyle = {
  image: {
    circle: {
      fill: emptyFill,
      stroke: defaultStroke,
      radius: 5,
    },
  },
  fill: emptyFill,
  stroke: defaultStroke,
};

export const isRoiTool = (tool) => {
  const ROI_TOOLS = ['EllipticalRoi', "CircleRoi", 'RectangleRoi', 'FreehandRoi'];
  return ROI_TOOLS.includes(tool);
}

export const isMeasurementTool = (tool) => {
  const MEASUREMENT_TOOLS = ['Length'];
  return MEASUREMENT_TOOLS.includes(tool);
}
