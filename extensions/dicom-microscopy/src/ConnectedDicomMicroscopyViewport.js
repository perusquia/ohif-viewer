import OHIF from '@ohif/core';
import { connect } from 'react-redux';
import asyncComponent from './asyncComponent.js';

// support for old extension version
const DicomMicroscopyViewport = asyncComponent(() =>
  import('./DicomMicroscopyViewport')
);

const { setViewportActive } = OHIF.redux.actions;

const mapStateToProps = (state) => {
  const { activeViewportIndex, layout } = state.viewports;
  return { activeViewportIndex, layout };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  const { viewportIndex } = ownProps;

  return {
    setViewportActive: () => {
      dispatch(setViewportActive(viewportIndex));
    },
  };
};

const ConnectedDicomMicroscopyViewport = connect(
  mapStateToProps,
  mapDispatchToProps
)(DicomMicroscopyViewport);

export default ConnectedDicomMicroscopyViewport;
