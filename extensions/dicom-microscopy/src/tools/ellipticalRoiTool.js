import { measurements } from '@ohif/core';
import { FeatureGeometryEvents, FeatureEvents } from 'dicom-microscopy-viewer/src/enums';
import { getEllipseData } from "@flywheel/extension-flywheel-common/src/tools/utils/EllipseIntersect";
import { globalDicomMicroscopyToolStateManager } from '../toolStateManager';
import baseAnnotationTool from "./baseAnnotationTool";
import { getSymbol, getGeometryName } from "./utils";

export default class ellipticalRoiTool extends baseAnnotationTool {

  updateFWMeasurementHandles(measurementData, geometryData) {
    const graphicData = geometryData.scoord3d.graphicData;
    const xCoordinates = graphicData.map(data => data?.[0]);
    const yCoordinates = graphicData.map(data => data?.[1]);

    // Set the start and end points as top-left, top-right,
    // bottom-left and bottom-right points of bounding
    // rectangle based on ellipse points.
    const start = {
      x: Math.min(...xCoordinates),
      y: Math.min(...yCoordinates),
      highlight: true,
      active: false,
    };
    const end = {
      x: Math.max(...xCoordinates),
      y: Math.max(...yCoordinates),
      highlight: true,
      active: false,
    };
    if (graphicData[0][1] > graphicData[1][1]) {
      [start.y, end.y] = [end.y, start.y];
    }
    if (graphicData[0][0] > graphicData[1][0]) {
      [start.x, end.x] = [end.x, start.x];
    }

    measurementData.handles = {
      start,
      end,
      textBox: this.createMeasurementTextBox(graphicData),
    }
  }

  createRoi(measurementData, viewerInstance) {
    const imageId = measurements.getImageIdForImagePath(measurementData.imagePath)
    const smMeasurementData = globalDicomMicroscopyToolStateManager.getToolStateForMeasurement(
      imageId, measurementData.toolType, measurementData.uuid);

    if (smMeasurementData?.geometryData) {
      super.createRoi(smMeasurementData, viewerInstance);
    } else {
      const roi = {};
      const styleOptions = {};

      roi.uid = measurementData.uuid;
      roi.properties = { markup: 'measurement' };
      roi.geometryName = getGeometryName(measurementData.toolType);
      roi.scoord3d = { graphicType: 'POLYGON' };

      const { handles } = measurementData;
      let graphicData = [];

      const ellipseData = getEllipseData(
        handles.start,
        handles.end,
        { x: 512, y: 512 },
        0,
        16
      );

      const ellipsePoints = ellipseData.linePoints;
      // Rearrange ellipse points so that drawing starts form left towards right.
      if (handles.start.x < handles.end.x) {
        const middleIndex = ellipsePoints.length / 2;
        const upperHalfPoints = ellipsePoints.splice(0, middleIndex);
        ellipsePoints.push(...upperHalfPoints);
      }

      ellipsePoints.forEach(point => {
        graphicData.push([point.x, point.y, 0]);
      });
      graphicData.push([ellipsePoints[0].x, ellipsePoints[0].y, 0]);

      roi.scoord3d.graphicData = graphicData;

      styleOptions.fill = { color: measurementData.color };
      styleOptions.stroke = {
        color:
          measurementData.color.substring(
            0,
            measurementData.color.lastIndexOf(',')
          ) + ', 1)',
        width: 2,
      };

      roi.properties.styleOptions = styleOptions;
      viewerInstance.addROI(roi, styleOptions);
    }

    const drawingSource = viewerInstance[getSymbol(viewerInstance, 'drawingSource')];
    const annotationManager = viewerInstance[getSymbol(viewerInstance, 'annotationManager')];

    const feature = drawingSource.getFeatureById(measurementData.uuid);
    feature.set('markup', 'measurement');
    feature.getGeometry().dispatchEvent(FeatureGeometryEvents.CHANGE);
    annotationManager.onAdd(feature);
    feature.dispatchEvent(FeatureEvents.PROPERTY_CHANGE);
  }
}
