import baseAnnotationTool from "./baseAnnotationTool";
import rectangleRoiTool from "./rectangleRoiTool";
import CircleRoiTool from "./circleRoiTool";
import ellipticalRoiTool from "./ellipticalRoiTool";
import freehandRoiTool from "./freehandRoiTool";
import arrowAnnotateTool from "./arrowAnnotateTool";
import lengthTool from "./lengthTool";

const getSymbol = (viewer, property) => {
  return Object.getOwnPropertySymbols(viewer)?.find(
    x => String(x) === `Symbol(${property})`
  );
};

const getFWToolType = (feature) => {
  let toolType = "";
  switch (feature.geometryName_) {
    case 'Circle': {
      toolType = 'CircleRoi';
      break;
    }
    case 'Ellipse': {
      toolType = "EllipticalRoi";
      break;
    }
    case 'Box': {
      toolType = "RectangleRoi";
      break;
    }
    case 'FreeHandPolygon': {
      toolType = "FreehandRoi";
      break;
    }
    case 'Line': {
      toolType =
        feature.values_?.marker === 'arrow' ? 'ArrowAnnotate' : 'Length';
      break;
    }
    default: {
      break;
    }
  }
  return toolType;
}

const getToolInstance = (toolType) => {
  let toolInstance;
  switch (toolType) {
    case 'RectangleRoi': {
      toolInstance = new rectangleRoiTool();
      break;
    }
    case 'CircleRoi': {
      toolInstance = new CircleRoiTool();
      break;
    }
    case 'EllipticalRoi': {
      toolInstance = new ellipticalRoiTool();
      break;
    }
    case 'FreehandRoi': {
      toolInstance = new freehandRoiTool();
      break;
    }
    case 'ArrowAnnotate': {
      toolInstance = new arrowAnnotateTool();
      break;
    }
    case 'Length': {
      toolInstance = new lengthTool();
      break;
    }
    default: {
      toolInstance = new baseAnnotationTool();
      break;
    }
  }
  return toolInstance;
}

const getTextBoxCoords = (graphicData) => {
  const length = graphicData.length;
  return { x: graphicData[length - 1][0], y: graphicData[length - 1][1] };
}

const getPolyBoundingBox = (minX, minY, maxX, maxY) => {
  return {
    left: minX,
    top: minY,
    width: maxX - minX,
    height: maxY - minY,
  };
}

const getGeometryName = (geometryType) => {
  let geometryName;
  switch (geometryType) {
    case 'CircleRoi': {
      geometryName = 'Circle';
      break;
    }
    case 'RectangleRoi': {
      geometryName = 'Box';
      break;
    }
    case 'EllipticalRoi': {
      geometryName = 'Ellipse';
      break;
    }
    case 'FreehandRoi': {
      geometryName = 'FreeHandPolygon';
      break;
    }
    case 'ArrowAnnotate': {
      geometryName = 'Line';
      break;
    }
    case 'Length': {
      geometryName = 'Line';
      break;
    }
    default: {
      break;
    }
  }
  return geometryName;
}


export {
  getSymbol, getFWToolType, getToolInstance, getTextBoxCoords,
  getPolyBoundingBox, getGeometryName
}
