import { measurements } from '@ohif/core';
import { distance as getDistance } from 'ol/coordinate';
import { FeatureGeometryEvents, FeatureEvents } from 'dicom-microscopy-viewer/src/enums';
import { globalDicomMicroscopyToolStateManager } from '../toolStateManager';
import baseAnnotationTool from './baseAnnotationTool';
import { getSymbol, getGeometryName } from './utils';

export default class CircleRoiTool extends baseAnnotationTool {
  updateFWMeasurementHandles(measurementData, geometryData) {
    const graphicData = geometryData.scoord3d.graphicData;
    const diametricallyOpposedCoordinates = graphicData.slice(0, 2);
    const circumferencePoint1 = diametricallyOpposedCoordinates[0];
    const circumferencePoint2 = diametricallyOpposedCoordinates[1];

    // Circle is defined by two points: the center point and a point on the
    // circumference.
    const centre = [
      (circumferencePoint1[0] + circumferencePoint2[0]) / parseFloat(2),
      (circumferencePoint1[1] + circumferencePoint2[1]) / parseFloat(2),
      0,
    ];

    measurementData.handles = {
      start: {
        x: centre[0],
        y: centre[1],
        highlight: true,
        active: false,
      },
      end: {
        x: circumferencePoint1[0],
        y: circumferencePoint1[1],
        highlight: true,
        active: false,
      },
      textBox: this.createMeasurementTextBox(graphicData),
    };
  }

  createRoi(measurementData, viewerInstance) {
    const imageId = measurements.getImageIdForImagePath(measurementData.imagePath)
    const smMeasurementData = globalDicomMicroscopyToolStateManager.getToolStateForMeasurement(
      imageId, measurementData.toolType, measurementData.uuid);

    if (smMeasurementData.geometryData) {
      super.createRoi(smMeasurementData, viewerInstance);
    } else {
      const roi = {};
      const styleOptions = {};

      roi.uid = measurementData.uuid;
      roi.properties = { markup: 'measurement' };
      roi.geometryName = getGeometryName(measurementData.toolType);

      // Circle is created as type ellipse in dicom-microscopy-viewer.
      roi.scoord3d = { graphicType: 'ELLIPSE' };

      const { handles } = measurementData;
      let graphicData = [];

      const center = [handles.start.x, handles.start.y];
      const circumferencePoint = [handles.end.x, handles.end.y];
      const radius = getDistance(center, circumferencePoint);

      // Points on circumference equivalent to major and  minor axis of the ellipse.
      graphicData = [
        [center[0] - radius, center[1], 0],
        [center[0] + radius, center[1], 0],
        [center[0], center[1] - radius, 0],
        [center[0], center[1] + radius, 0],
      ];

      roi.scoord3d.graphicData = graphicData;

      styleOptions.fill = { color: measurementData.color };
      styleOptions.stroke = {
        color:
          measurementData.color.substring(
            0,
            measurementData.color.lastIndexOf(',')
          ) + ', 1)',
        width: 2,
      };

      roi.properties.styleOptions = styleOptions;
      viewerInstance.addROI(roi, styleOptions);
    }

    const drawingSource =
      viewerInstance[getSymbol(viewerInstance, 'drawingSource')];
    const annotationManager =
      viewerInstance[getSymbol(viewerInstance, 'annotationManager')];

    const feature = drawingSource.getFeatureById(measurementData.uuid);
    feature.set('markup', 'measurement');
    feature.getGeometry().dispatchEvent(FeatureGeometryEvents.CHANGE);
    annotationManager.onAdd(feature);
    feature.dispatchEvent(FeatureEvents.PROPERTY_CHANGE);
  }
}
