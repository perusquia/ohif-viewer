
import { measurements } from '@ohif/core';
import { FeatureGeometryEvents, FeatureEvents } from 'dicom-microscopy-viewer/src/enums';
import { globalDicomMicroscopyToolStateManager } from '../toolStateManager';
import baseAnnotationTool from "./baseAnnotationTool";
import { getSymbol, getGeometryName } from "./utils";

export default class rectangleRoiTool extends baseAnnotationTool {

  updateFWMeasurementHandles(measurementData, geometryData) {
    const graphicData = geometryData.scoord3d.graphicData;
    measurementData.handles = {
      start: {
        x: graphicData?.[0]?.[0],
        y: graphicData?.[0]?.[1],
        highlight: true,
        active: false,
      },
      end: {
        x: graphicData?.[2]?.[0],
        y: graphicData?.[2]?.[1],
        highlight: true,
        active: false,
      },
      textBox: this.createMeasurementTextBox(graphicData),
    }
  }

  createRoi(measurementData, viewerInstance) {
    const imageId = measurements.getImageIdForImagePath(measurementData.imagePath)
    const smMeasurementData = globalDicomMicroscopyToolStateManager.getToolStateForMeasurement(
      imageId, measurementData.toolType, measurementData.uuid);

    if (smMeasurementData?.geometryData) {
      super.createRoi(smMeasurementData, viewerInstance);
    } else {
      const roi = {};
      const styleOptions = {};

      roi.uid = measurementData.uuid;
      roi.properties = { markup: 'measurement' };
      roi.geometryName = getGeometryName(measurementData.toolType);
      roi.scoord3d = { graphicType: 'POLYGON' };

      const { handles } = measurementData;
      let graphicData = [];

      if (handles?.start && handles?.end) {
        graphicData.push([handles.start.x, handles.start.y, 0]);
        graphicData.push([handles.end.x, handles.start.y, 0]);
        graphicData.push([handles.end.x, handles.end.y, 0]);
        graphicData.push([handles.start.x, handles.end.y, 0]);
        graphicData.push([handles.start.x, handles.start.y, 0]);
      }
      roi.scoord3d.graphicData = graphicData;

      styleOptions.fill = { color: measurementData.color };
      styleOptions.stroke = {
        color:
          measurementData.color.substring(
            0,
            measurementData.color.lastIndexOf(',')
          ) + ', 1)',
        width: 2,
      };
      roi.properties.styleOptions = styleOptions;
      viewerInstance.addROI(roi, styleOptions);
    }

    const drawingSource = viewerInstance[getSymbol(viewerInstance, 'drawingSource')];
    const annotationManager = viewerInstance[getSymbol(viewerInstance, 'annotationManager')];

    const feature = drawingSource.getFeatureById(measurementData.uuid);
    feature.set('markup', 'measurement');
    feature.getGeometry().dispatchEvent(FeatureGeometryEvents.CHANGE);
    annotationManager.onAdd(feature);
    feature.dispatchEvent(FeatureEvents.PROPERTY_CHANGE);
  }
}
