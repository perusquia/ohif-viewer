
// Maintains details about geometries/measurements existing in microscopy viewports.
// It includes the details present in FW measurement data and the information
// required to interact with open layer features. The mapping is done against
// imageId so that there is a sync with coorenerstone tools 'tool state' and
// measurement API operations can be done with minimal changes.
function toolStateManager() {
  let toolState = {};

  function getImageToolState(imageId) {
    return toolState[imageId];
  }

  function restoreImageToolState(imageId, imageToolState) {
    toolState[imageId] = imageToolState;
  }

  function getToolState() {
    return toolState;
  }

  function restoreToolState(modifiedToolState) {
    toolState = modifiedToolState;
  }

  function addToolState(imageId, toolName, data) {
    // If we don't have any tool state for this imageId, add an empty object
    if (!toolState.hasOwnProperty(imageId)) {
      toolState[imageId] = {};
    }

    const imageToolState = toolState[imageId];

    // If we don't have tool state for this tool name, add an empty object
    if (!imageToolState.hasOwnProperty(toolName)) {
      imageToolState[toolName] = {
        data: [],
      };
    }

    const toolData = imageToolState[toolName];

    // Finally, add this new tool to the state
    toolData.data.push(data);
  }

  function updateToolState(imageId, toolName, data) {
    // If we don't have any tool state for this imageId, return
    if (!toolState.hasOwnProperty(imageId)) {
      return;
    }

    const imageToolState = toolState[imageId];

    // If we don't have tool state for this tool name, return
    if (!imageToolState.hasOwnProperty(toolName)) {
      return;
    }

    const toolData = imageToolState[toolName];

    // If we don't have tool data with this uuid, return
    const dataIndex = toolData.data?.findIndex(tData => tData.uuid === data.uuid);
    if (dataIndex === undefined || dataIndex === -1) {
      return;
    }

    toolData.data.splice(dataIndex, 1, data);
  }

  function getImageToolStateForTool(imageId, toolName) {
    // If we don't have any tool state for this imageId, return undefined
    if (!toolState.hasOwnProperty(imageId)) {
      return;
    }

    const imageToolState = toolState[imageId];

    // If we don't have tool state for this tool name, return undefined
    if (!imageToolState.hasOwnProperty(toolName)) {
      return;
    }

    return imageToolState[toolName];
  }

  function getToolStateForMeasurement(imageId, toolName, measurementUuid) {
    // If we don't have any tool state for this imageId, return undefined
    if (!toolState.hasOwnProperty(imageId)) {
      return;
    }

    const imageToolState = toolState[imageId];

    // If we don't have tool state for this tool name, return undefined
    if (!imageToolState.hasOwnProperty(toolName)) {
      return;
    }

    const toolStateForTool = imageToolState[toolName];
    return toolStateForTool.data.find(toolData => toolData.uuid === measurementUuid);
  }

  function clearToolState() {
    toolState = {};
  }

  return {
    getImageToolState,
    addToolState,
    updateToolState,
    restoreToolState,
    getToolState,
    restoreImageToolState,
    getImageToolStateForTool,
    getToolStateForMeasurement,
    clearToolState,
    toolState,
  };
}

const globalDicomMicroscopyToolStateManager = toolStateManager();

export {
  toolStateManager,
  globalDicomMicroscopyToolStateManager,
};
