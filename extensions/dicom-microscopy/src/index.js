import React from 'react';
import DicomMicroscopySopClassHandler from './DicomMicroscopySopClassHandler.js';
import { version } from '../package.json';
import toolbarModule from './toolbarModule.js';
import commandsModule from './commandsModule.js';
import Redux from './redux';
import initExtension from './init';
import Reactive from './reactive';
import { smAnnotationHandler } from './annotations/index.js';
import { globalDicomMicroscopyToolStateManager } from './toolStateManager.js';
import ConnectedDicomMicroscopyViewport from './ConnectedDicomMicroscopyViewport';

export default {
  /**
   * Only required property. Should be a unique value across all extensions.
   */
  id: 'microscopy',
  version,

  preRegistration({ commandsManager, servicesManager, appConfig }) {
    initExtension({ commandsManager, servicesManager, appConfig });
  },
  getToolbarModule() {
    return {
      definitions: [...toolbarModule.definitions],
      defaultContext: 'ACTIVE_VIEWPORT::MICROSCOPY',
      shouldDisplayLayoutButton: true,
    };
  },
  getViewportModule({ servicesManager }) {
    const ExtendedDicomMicroscopyViewport = props => {
      return (
        <ConnectedDicomMicroscopyViewport {...props}
          servicesManager={servicesManager}
        />
      );
    };

    return ExtendedDicomMicroscopyViewport;
  },
  getSopClassHandlerModule() {
    return DicomMicroscopySopClassHandler;
  },
  getCommandsModule({ servicesManager }) {
    return commandsModule({ servicesManager });
  },
};

export { Redux, Reactive, toolbarModule, smAnnotationHandler, globalDicomMicroscopyToolStateManager };
