import { combineReducers } from 'redux';
import { redux as OhifRedux } from '@ohif/core';
import Redux from '../redux';

export default function initStore(store) {
  // TODO OHIF to provide a API to consume reducers from extensions
  // add microscopy reducer
  OhifRedux.reducers.microscopy = Redux.reducers.microscopyData;
  store.replaceReducer(combineReducers(OhifRedux.reducers));
}
