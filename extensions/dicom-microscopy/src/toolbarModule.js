import SupportToolbarComponent from "@flywheel/extension-cornerstone-infusions/src/components/toolbar/SupportToolbarComponent";
import StudyListComponent from '@flywheel/extension-flywheel/src/components/StudyList';

const TOOLBAR_BUTTON_TYPES = {
  SEPARATOR: 'separator',
};

const definitions = [
  {
    id: 'StudyList',
    label: 'Study List',
    icon: 'contact_page',
    class: 'material-icons',
    CustomComponent: StudyListComponent,
  },
  {
    id: 'ZoomMenu',
    label: 'Zoom',
    icon: 'search-plus',
    commandName: 'setZoomActive',
    commandOptions: { toolName: 'Zoom' },
  },
  {
    id: 'Pan',
    label: 'Pan',
    icon: 'arrows',
    commandName: 'setPanActive',
    commandOptions: { toolName: 'Pan' },
  },
  {
    id: 'Separator',
    label: 'Separator',
    type: TOOLBAR_BUTTON_TYPES.SEPARATOR,
  },
  {
    id: 'AnnotateMenu',
    label: 'Annotate',
    icon: 'draw',
    class: 'material-icons',
    buttons: [
      {
        id: 'EllipticalRoi',
        label: 'Ellipse',
        icon: 'circle-o',
        commandName: 'setEllipticalActive',
        commandOptions: { toolName: 'EllipticalRoi' },
        options: {
          group: 'ROI',
        }
      },
      {
        id: 'CircleRoi',
        label: 'Circle',
        icon: 'circle-o',
        commandName: 'setCircleActive',
        commandOptions: { toolName: 'CircleRoi' },
        options: {
          group: 'ROI',
        }
      },
      {
        id: 'RectangleRoi',
        label: 'Rectangle',
        icon: 'square-o',
        commandName: 'setRectangleActive',
        commandOptions: { toolName: 'RectangleRoi' },
        options: {
          group: 'ROI',
        }
      },
      {
        id: 'FreehandRoi',
        label: 'Freehand',
        icon: 'freehand',
        commandName: 'setFreehandActive',
        commandOptions: { toolName: 'FreehandRoi' },
        options: {
          group: 'ROI',
        }
      },
      {
        id: 'Separator',
        label: 'Separator',
        type: TOOLBAR_BUTTON_TYPES.SEPARATOR,
      },
      {
        id: 'Length',
        label: 'Length',
        icon: 'measure-temp',
        commandName: 'setLengthActive',
        commandOptions: { toolName: 'Length' },
        options: {
          group: 'Measure',
        }
      },
      {
        id: 'Separator',
        label: 'Separator',
        type: TOOLBAR_BUTTON_TYPES.SEPARATOR,
      },
      {
        id: 'ArrowAnnotate',
        label: 'Annotate',
        icon: 'measure-non-target',
        commandName: 'setAnnotateActive',
        commandOptions: { toolName: 'ArrowAnnotate' },
        options: {
          group: 'SinglePoint',
        }
      },
    ],
  },
  {
    id: 'SupportButton',
    label: 'Support',
    icon: 'support_agent',
    class: 'material-icons',
    CustomComponent: SupportToolbarComponent,
    context: ['ACTIVE_VIEWPORT::MICROSCOPY'],
    options: { pull: 'right' },
  }
];

export default {
  definitions,
  defaultContext: 'ACTIVE_VIEWPORT::MICROSCOPY',
};
