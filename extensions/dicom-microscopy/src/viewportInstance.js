import * as eventHandlers from './utils/handleMeasurementEvents';
import { getViewport } from './activeMicroscopyViewports';
import { onMoveEnd } from './handleViewerMapEvents';


const microscopyEventRegister = {
  microscopyEvents: null
};

/**
 * Register handler for the required microscopy events
 */
microscopyEventRegister.bindEvents = (viewportIndex) => {
  const { container, viewerInstance } = getViewport(viewportIndex) || {};
  if (container && viewerInstance) {
    container.addEventListener(
      microscopyEventRegister.microscopyEvents.ROI_ADDED, eventHandlers.onMeasurementAdded
    );
    container.addEventListener(
      microscopyEventRegister.microscopyEvents.ROI_REMOVED, eventHandlers.onMeasurementRemoved
    );
    container.addEventListener(
      microscopyEventRegister.microscopyEvents.ROI_MODIFIED, eventHandlers.onMeasurementModified
    );

    getViewerMap(viewerInstance)?.on('moveend', onMoveEnd);
  }
};

/**
 * Unregister the registered handlers
 */
microscopyEventRegister.unBindEvents = (viewportIndex) => {
  const { container, viewerInstance } = getViewport(viewportIndex) || {};
  if (container && viewerInstance) {
    container.removeEventListener(
      microscopyEventRegister.microscopyEvents.ROI_ADDED, eventHandlers.onMeasurementAdded
    );
    container.removeEventListener(
      microscopyEventRegister.microscopyEvents.ROI_REMOVED, eventHandlers.onMeasurementRemoved
    );
    container.removeEventListener(
      microscopyEventRegister.microscopyEvents.ROI_MODIFIED, eventHandlers.onMeasurementModified
    );

    getViewerMap(viewerInstance)?.un('moveend', onMoveEnd);
  }
};


const getViewerMap = (viewerInstance) => {
  const viewerSymbols = Object.getOwnPropertySymbols(viewerInstance);
  const mapSymbol = viewerSymbols?.find(x => String(x) === 'Symbol(map)');
  return viewerInstance[mapSymbol];
}

export { microscopyEventRegister };
