import { FeatureGeometryEvents, FeatureEvents } from 'dicom-microscopy-viewer/src/enums';
import { getAllViewports } from './activeMicroscopyViewports';

const onMoveEnd = (event) => {
  // When viewport scale changes on zooming, updates annotation's measurement
  // unit accordingly.

  const viewport = getAllViewports().find(viewport => {
    const viewportInstance = viewport.viewerInstance;
    const viewerSymbols = Object.getOwnPropertySymbols(viewportInstance);
    const mapSymbol = viewerSymbols?.find(x => String(x) === 'Symbol(map)');
    if (viewportInstance[mapSymbol] === event.target) {
      return true;
    }
  });

  if (!viewport) {
    return;
  }
  const viewportInstance = viewport.viewerInstance;
  const viewerSymbols = Object.getOwnPropertySymbols(viewportInstance);
  const featureSymbol = viewerSymbols?.find(x => String(x) === 'Symbol(features)');
  const features = viewportInstance[featureSymbol].getArray();

  features.forEach(feature => {
    const geometry = feature.getGeometry();
    geometry.dispatchEvent(FeatureGeometryEvents.CHANGE);
    feature.dispatchEvent(FeatureEvents.PROPERTY_CHANGE);
  });
};

export { onMoveEnd };
