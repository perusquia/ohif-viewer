
// Keeps an array of microscopy viewer instances with the viewport index
// so that it can be used to get the viewer instance and corresponding
// container element later while doing operations on viewport.
const microscopyViewports = [];

export const addViewport = (index, viewerInstance, container) => {
  const viewportIndex = microscopyViewports.findIndex(viewport => viewport.index === index);
  if (viewportIndex !== -1) {
    microscopyViewports.splice(viewportIndex);
  }
  microscopyViewports.push({ index, viewerInstance, container });
}

export const getViewport = (index) => {
  return microscopyViewports.find(viewport => viewport.index === index);
}

export const getAllViewports = () => {
  return microscopyViewports;
}

export const removeViewport = (index, viewerInstance, container) => {
  const viewportIndex = microscopyViewports.findIndex(viewport => viewport.index === index);
  if (viewportIndex !== -1) {
    microscopyViewports.splice(viewportIndex);
  }
}
