import { handleAnnotationChange } from "./annotationHandler";
import { annotationOperations } from "./annotationOperations";
import { addLabel, editLabel } from "./annotationLabellingHandler";
import getAnnotationText from "./getAnnotationText";

export const smAnnotationHandler = {
  annotationOperations,
  handleAnnotationChange,
  addLabel,
  editLabel,
  getAnnotationText
}
