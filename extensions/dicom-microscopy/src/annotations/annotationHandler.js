import cloneDeep from 'lodash.clonedeep';
import { measurements } from '@ohif/core';
import store from '@ohif/viewer/src/store';
import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';
import { globalDicomMicroscopyToolStateManager } from '../toolStateManager';
import { getMeasurement, getFilteredMeasurements } from '../utils/measurementUtils';
import { annotationOperations } from './annotationOperations';
import { getViewport } from '../activeMicroscopyViewports';
import { getToolInstance } from '../tools/utils';

const { getImageIdForImagePath } = measurements;
const { measurementToolUtils } = FlywheelCommonUtils;

export function handleAnnotationChange(type, measurement) {
  switch (type) {
    case annotationOperations.DELETE_ANNOTATION: {
      deleteAnnotation(measurement);
      break;
    }
    case annotationOperations.CHANGE_COLOR: {
      changeColor(measurement);
      break;
    }
    case annotationOperations.TOGGLE_VISIBILITY: {
      toggleVisibility(measurement);
      break;
    }
    case annotationOperations.HiGHLIGHT_HOVERED_ANNOTATIONS: {
      highlightHoveredAnnotations(measurement);
      break;
    }
    default: {
      break;
    }
  }
}

function toggleVisibility(measurementData) {
  const fwMeasurement = getMeasurement(measurementData);
  if (fwMeasurement) {
    const imageId = getImageIdForImagePath(fwMeasurement.imagePath)
    const smMeasurementData = globalDicomMicroscopyToolStateManager.getToolStateForMeasurement(
      imageId, fwMeasurement.toolType, fwMeasurement.uuid);

    if (smMeasurementData) {
      const toolInstance = getToolInstance(smMeasurementData.toolType);

      getViewerInstances(smMeasurementData).forEach((viewerInstance) => {
        smMeasurementData.updateFWMeasurement = false;
        const roi = viewerInstance.getROI(smMeasurementData.uuid);
        if (roi) {
          viewerInstance.removeROI(smMeasurementData.uuid);
        } else {
          toolInstance.createRoi(fwMeasurement, viewerInstance);
        }
      });
    }
  }
}

function deleteAnnotation(measurementData) {
  const fwMeasurement = getMeasurement(measurementData);
  if (fwMeasurement) {
    const imageId = getImageIdForImagePath(fwMeasurement.imagePath)
    const smMeasurementData = globalDicomMicroscopyToolStateManager.getToolStateForMeasurement(
      imageId, fwMeasurement.toolType, fwMeasurement.uuid);

    if (smMeasurementData) {
      getViewerInstances(smMeasurementData).forEach((viewerInstance) => {
        smMeasurementData.updateFWMeasurement = false;
        const viewerSymbols = Object.getOwnPropertySymbols(
          viewerInstance);
        const interactionsSymbol = viewerSymbols?.find(x => String(x) === 'Symbol(interactions)');
        const interactions = viewerInstance[interactionsSymbol];
        interactions?.draw?.finishDrawing();

        viewerInstance.removeROI(fwMeasurement.uuid);
      });
      highlightHoveredAnnotations([]);
    }
  }
}


function changeColor(measurementData) {
  const fwMeasurement = getMeasurement(measurementData);
  if (fwMeasurement) {
    const { color } = measurementData.measurementData;
    const strokeColor = color.substring(0, color.lastIndexOf(',')) + ', 1)';
    changeROIColor(fwMeasurement, strokeColor, color);
  }
}

function highlightHoveredAnnotations(measurements) {
  const hoveredIds = measurements.map(measurement => measurement.measurementId);
  const highlightedMeasurements = hoveredIds.length > 0 ?
    getFilteredMeasurements(hoveredIds, true)
    : getFilteredMeasurements(hoveredIds, false);

  highlightedMeasurements.forEach(measurementData => {
    const strokeColor = measurementData.color.substring(0, measurementData.color.lastIndexOf(',')) + ', 1)';
    changeROIColor(measurementData, strokeColor, measurementData.color);
  });

  const dimmedMeasurements = hoveredIds.length > 0 ? getFilteredMeasurements(hoveredIds, false)
    : [];
  dimmedMeasurements.forEach(measurementData => {
    const reducedOpacityColor = measurementToolUtils.reduceColorOpacity(measurementData.color);
    changeROIColor(measurementData, reducedOpacityColor, reducedOpacityColor);
  });
}

const changeROIColor = (measurementData, strokeColor, fillColor) => {
  const imageId = getImageIdForImagePath(measurementData.imagePath)

  const smMeasurementData = globalDicomMicroscopyToolStateManager.getToolStateForMeasurement(
    imageId, measurementData.toolType, measurementData.uuid);
  if (smMeasurementData) {

    getViewerInstances(smMeasurementData).forEach((viewerInstance, viewerIndex) => {
      smMeasurementData.updateFWMeasurement = false;
      const roi = viewerInstance.getROI(measurementData.uuid);
      if (roi) {
        const styleOptions = cloneDeep(roi.properties.styleOptions);

        styleOptions.stroke.color = strokeColor;
        styleOptions.fill.color = fillColor;
        viewerInstance.setROIStyle(measurementData.uuid, styleOptions);

        if (viewerIndex === 0 && smMeasurementData.geometryData) {
          smMeasurementData.geometryData.properties.styleOptions = styleOptions;
        }
      }
    });
  }
}

const getViewerInstances = (smMeasurementData) => {
  const viewerInstances = [];
  const viewportSpecificData = store.getState().viewports.viewportSpecificData;
  Object.keys(viewportSpecificData).forEach((dataKey, viewportIndex) => {
    const viewportData = viewportSpecificData[dataKey];
    if (viewportData.SeriesInstanceUID === smMeasurementData.SeriesInstanceUID) {
      const viewerInstance = getViewport(viewportIndex)?.viewerInstance;
      if (viewerInstance) {
        viewerInstances.push(viewerInstance);
      }
    }
  });
  return viewerInstances;
}
