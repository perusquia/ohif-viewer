import { measurements } from '@ohif/core';
import { common } from '@flywheel/extension-flywheel-common/src/tools/textBoxContent';
import { globalDicomMicroscopyToolStateManager } from '../toolStateManager';

const { getImageIdForImagePath } = measurements;
const { getLineItem } = common;

const getAnnotationText = (measurement) => {
  let textBoxContent = [];

  const imageId = getImageIdForImagePath(measurement.imagePath)
  const measurementData = globalDicomMicroscopyToolStateManager.getToolStateForMeasurement(
    imageId, measurement.toolType, measurement.uuid);
  if (measurementData) {
    // Measurement details are extracted with respect to the structure
    // measurements are stored by 'dicom-microscopy-viewer'.
    const measurements = measurementData.geometryData?.measurements;
    if (measurements?.length > 0) {
      const measurementName = measurements[0].ConceptNameCodeSequence[0].CodeMeaning;
      let measurementValue = measurements[0].MeasuredValueSequence[0].NumericValue;
      let measurementUnit = measurements[0].MeasuredValueSequence[0].MeasurementUnitsCodeSequence[0].CodeValue;
      // Corrections on area and length values are done similar to 'dicom-microscopy-viewer'.
      if (measurementName === 'Area') {
        measurementValue = convertAreaToMilliMeterOrMicroMeter(measurementValue, measurementUnit);
        measurementUnit = squareUnit(measurementUnit);
      } else if (measurementName === 'Length') {
        measurementValue = convertLengthToMilliMeter(measurementValue, measurementUnit);
      }
      textBoxContent = [getLineItem(measurementValue, `${measurementName}: `, measurementUnit)];
  }
  }
  return textBoxContent;
}

const squareUnit = unit => `${unit}${String.fromCharCode(178)}`;

const convertAreaToMilliMeterOrMicroMeter = (area, units) => {
  if (area) {
    switch (units) {
      case "μm":
        area = area * 1000;
        break;
      case "mm":
        area = area / 1000;
        break;
      default:
        break;
    }
  }
  return area;
}

const convertLengthToMilliMeter = (length, units) => {
  if (length) {
    switch (units) {
      case "mm":
        length = length / 1000;
        break;
      default:
        break;
    }
  }
  return length;
}

export default getAnnotationText;
