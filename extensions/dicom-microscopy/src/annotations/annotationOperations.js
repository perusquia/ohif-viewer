export const annotationOperations = {
  DELETE_ANNOTATION: 'deleteAnnotation',
  TOGGLE_VISIBILITY: 'toggleVisibility',
  CHANGE_COLOR: 'changeColor',
  HiGHLIGHT_HOVERED_ANNOTATIONS: 'highlightHoveredAnnotations',
}
