import OHIF from '@ohif/core';
import { utils as microscopyUtils } from 'dicom-microscopy-viewer';

const { utils } = OHIF;

const SOP_CLASS_UIDS = {
  VL_WHOLE_SLIDE_MICROSCOPY_IMAGE_STORAGE: '1.2.840.10008.5.1.4.1.1.77.1.6',
};

const DicomMicroscopySopClassHandler = {
  id: 'DicomMicroscopySopClassHandlerPlugin',
  sopClassUIDs: [SOP_CLASS_UIDS.VL_WHOLE_SLIDE_MICROSCOPY_IMAGE_STORAGE],
  getDisplaySetFromSeries(series, study, dicomWebClient) {
    let instance;
    instance = series._instances.find(instance => instance.getData().metadata.ImageType[2] === 'LABEL');

    if (instance === undefined) {
      const volumeInstances = series._instances.filter(instance => instance.getData().metadata.ImageType[2] === 'VOLUME')
      instance = volumeInstances.reduce((prev, current) => {
        return (prev.getData().metadata.TotalPixelMatrixColumns
          > current.getData().metadata.TotalPixelMatrixColumns)
          ? current : prev;
      });
    }
    const metadata = instance.getData().metadata;
    const imageId = instance.getImageId();
    // Dicom-microscopy-viewport applies this rotation on the image when
    // displayed in the viewport. Same is applied on the thumbnail to
    // match with the viewport image.
    const imageRotation = getImageRotation(metadata);

    const {
      SeriesDescription,
      SeriesNumber,
      ContentDate,
      ContentTime,
    } = metadata;

    // Note: We are passing the dicomweb client into each viewport!

    return {
      plugin: 'microscopy',
      Modality: 'SM',
      displaySetInstanceUID: utils.guid(),
      dicomWebClient,
      SOPInstanceUID: instance.getSOPInstanceUID(),
      SeriesInstanceUID: series.getSeriesInstanceUID(),
      StudyInstanceUID: study.getStudyInstanceUID(),
      SeriesDescription,
      SeriesDate: ContentDate, // Map ContentDate/Time to SeriesTime for series list sorting.
      SeriesTime: ContentTime,
      SeriesNumber,
      metadata,
      imageId,
      imageRotation
    };
  },
};

const getImageRotation = (metadata) => {
  const rotation = microscopyUtils.computeRotation({ orientation: metadata.ImageOrientationSlide, inDegrees: true });
  // To orient the slide horizontally with the label on the right side.
  const correction = metadata.ImageType[2] === 'LABEL' ? 0 : 90;
  return rotation + correction;
}
export default DicomMicroscopySopClassHandler;
