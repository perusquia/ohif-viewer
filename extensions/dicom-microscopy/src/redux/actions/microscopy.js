export const addMicroscopyData = data => {
  return {
    type: 'ADD_MICROSCOPY_DATA',
    data,
  };
};
