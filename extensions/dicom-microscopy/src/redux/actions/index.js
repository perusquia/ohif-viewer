import * as microscopy from './microscopy';

const actions = {
  ...microscopy,
};
export default actions;
