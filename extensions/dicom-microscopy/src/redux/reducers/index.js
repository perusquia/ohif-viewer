import microscopyData from './microscopy';

const reducers = {
  microscopyData,
};

export default reducers;
