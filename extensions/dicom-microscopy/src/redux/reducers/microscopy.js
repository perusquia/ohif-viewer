import cloneDeep from 'lodash.clonedeep';

const defaultState = {
  isEnabledMicroscopy: false,
};

export default function microscopyData(state = defaultState, action) {
  switch (action.type) {
    case 'ADD_MICROSCOPY_DATA': {
      const data = cloneDeep(state.isEnabledMicroscopy);
      let obj = cloneDeep(action.data);
      return Object.assign({}, state, {
        isEnabledMicroscopy: true,
      });
    }

    default:
      return state;
  }
}
