/*
Copyright (c) MONAI Consortium
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import cornerstoneTools from 'cornerstone-tools';
import cornerstone from 'cornerstone-core';
import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';
const {
  setActiveTool,
  setSmartCTRange,
  addAvailableSmartRange,
} = FlywheelCommonRedux.actions;
const {
  segmentationTools,
  getAvailableSmartCTRanges,
  selectAllowedActiveTools,
  selectActiveViewportIndex,
} = FlywheelCommonRedux.selectors;

const { addSmartCTRange, getSmartCTRanges } = FlywheelCommonRedux.smartCTRanges;
const { setters, getters } = cornerstoneTools.getModule('segmentation');

const autoSelectionRestrictedTools = ['AutoSmartBrush'];
function getImageIdsForDisplaySet(
  studies,
  StudyInstanceUID,
  SeriesInstanceUID
) {
  const study = studies.find(
    study => study.StudyInstanceUID === StudyInstanceUID
  );

  const displaySets = study.displaySets.filter(displaySet => {
    return displaySet.SeriesInstanceUID === SeriesInstanceUID;
  });

  const referencedDisplaySet = displaySets[0];
  return referencedDisplaySet.images.map(image => image.getImageId());
}

/**
 * Gets an array of LabelMap.
 * Each LabelMap is an array of segments.
 *
 * Note that this LabelMap we have here is different from cornerstone's.
 *
 * @param element
 */
function getLabelMaps(element) {
  let labelmaps = [];
  if (!element) {
    return labelmaps;
  }

  const segmentationModule = cornerstoneTools.getModule('segmentation');
  const { labelmaps3D } = getters.labelmaps3D(element);

  if (!labelmaps3D) {
    return labelmaps;
  }

  for (let i = 0; i < labelmaps3D.length; i++) {
    let segments = [];
    const labelmap3D = labelmaps3D[i];

    // TODO:: which one is standard metadata.data[] or metadata[] ???
    const metadata =
      labelmap3D && labelmap3D.metadata && labelmap3D.metadata.data
        ? labelmap3D.metadata.data
        : null;
    const colorLutTable =
      segmentationModule.state.colorLutTables[labelmap3D.colorLUTIndex];

    if (!metadata) {
    } else {
      for (let j = 1; j < metadata.length; j++) {
        const meta = metadata[j];
        if (!meta) {
          continue;
        }
        const segmentIndex = meta.SegmentNumber - 1;
        const id = i + '+' + segmentIndex;
        const color = colorLutTable[meta.SegmentNumber];
        const segmentItem = {
          id: id,
          labelmapIndex: i,
          segmentIndex,
          color: color,
          meta: meta,
          name: meta.SegmentLabel,
          description: meta.SegmentDescription,
        };
        segments.push(segmentItem);
      }
    }

    labelmaps.push(segments);
  }

  return labelmaps;
}

function flattenLabelmaps(labelmaps) {
  return [].concat.apply([], labelmaps);
}

function getFirstSegmentId(element) {
  const labelmaps = getLabelMaps(element);
  const segments = flattenLabelmaps(labelmaps);
  return segments && segments.length ? segments[0].id : null;
}

/**
 * Creates a segment.
 *
 * @param {Object} element A cornerstone element
 * @param {string} label Name of the segment
 * @param description
 * @param color
 * @param {boolean} newLabelMap Whether to put this segment in a new labelmap3D or not
 * @param labelMeta
 * @returns {{labelmapIndex: *, segmentIndex: number}}
 */
function createSegment(
  element,
  label,
  description = '',
  color = null,
  newLabelMap = false,
  labelMeta = null
) {
  const newRangeItem = {
    label,
    color: color ? [color.r, color.g, color.b, 1] : '',
    range: { min: Infinity, max: -Infinity },
    description: '',
  };
  const stateValue = store.getState();
  let smartRanges = getAllSmartCTRanges();
  let existingRange = smartRanges.find(range => range.label === label);
  if (!existingRange) {
    addSmartRanges(stateValue, newRangeItem);
    smartRanges = getAllSmartCTRanges();
  }

  const SegmentNumber =
    smartRanges.findIndex(range => range.label === label) + 1 || 1;
  labelMeta = labelMeta
    ? labelMeta
    : {
        SegmentedPropertyCategoryCodeSequence: {
          CodeValue: 'T-D0050',
          CodingSchemeDesignator: 'SRT',
          CodeMeaning: 'Tissue',
        },
        SegmentNumber: SegmentNumber,
        SegmentLabel: label ? label : 'label-0-1',
        SegmentDescription: description,
        SegmentAlgorithmType: 'SEMIAUTOMATIC',
        SegmentAlgorithmName: 'MONAI',
        SegmentedPropertyTypeCodeSequence: {
          CodeValue: 'T-D0050',
          CodingSchemeDesignator: 'SRT',
          CodeMeaning: 'Tissue',
        },
      };

  setters.activeLabelmapIndex(element, 0);

  const { labelmap3D, activeLabelmapIndex } = getters.labelmap2D(element);

  // Add new colorLUT if required for new labelmapIndex
  const { state } = cornerstoneTools.getModule('segmentation');
  if (state.colorLutTables.length <= activeLabelmapIndex) {
    setters.colorLUT(activeLabelmapIndex);
    labelmap3D.colorLUTIndex = activeLabelmapIndex;
  }

  // TODO:: which one is standard metadata.data[] or metadata[] ???
  if (!labelmap3D.metadata || !labelmap3D.metadata.data) {
    labelmap3D.metadata = { data: [undefined] };
  }

  const { metadata } = labelmap3D;
  let nextSegmentId = labelMeta.SegmentNumber || 1;

  labelMeta.SegmentLabel = label
    ? label
    : 'label_' + activeLabelmapIndex + '-' + nextSegmentId;

  if (nextSegmentId === metadata.data.length) {
    metadata.data.push(labelMeta);
  } else if (
    !metadata.data.find(meta => meta?.SegmentNumber === nextSegmentId)
  ) {
    metadata.data.splice(nextSegmentId, 0, labelMeta);
  }
  setters.activeSegmentIndex(element, nextSegmentId);

  if (existingRange) {
    if (color && existingRange.color === '') {
      existingRange.color = color
        ? [color.r, color.g, color.b, 1]
        : existingRange.color;
      addSmartRanges(stateValue, existingRange);
    }
  } else {
    existingRange = smartRanges.find(range => range.label === label);
  }
  // setting selected smartCTRange item as given label
  store.dispatch(setSmartCTRange(existingRange));
  return {
    id: activeLabelmapIndex + '+' + nextSegmentId,
    labelmapIndex: activeLabelmapIndex,
    segmentIndex: nextSegmentId,
  };
}

function createSegmentItem(ranges, segmentIndex) {
  const color =
    cornerstoneTools.store.modules.segmentation.state.colorLutTables[0][
      segmentIndex
    ];
  const meta = {
    SegmentedPropertyCategoryCodeSequence: {
      CodeValue: 'T-D0050',
      CodingSchemeDesignator: 'SRT',
      CodeMeaning: 'Tissue',
    },
    SegmentNumber: segmentIndex,
    SegmentLabel: ranges.label || 'label-0-1',
    SegmentDescription: ranges.description,
    SegmentAlgorithmType: 'SEMIAUTOMATIC',
    SegmentAlgorithmName: 'MONAI',
    SegmentedPropertyTypeCodeSequence: {
      CodeValue: 'T-D0050',
      CodingSchemeDesignator: 'SRT',
      CodeMeaning: 'Tissue',
    },
  };
  const id = `0+${segmentIndex}`;
  const segmentItem = {
    id: id,
    labelmapIndex: 0,
    segmentIndex: segmentIndex,
    color,
    meta,
    name: ranges.label,
    description: ranges.description,
    range: ranges.range,
  };
  return segmentItem;
}

function getSegmentInfo(element, labelmapIndex, segmentIndex) {
  var name = '';
  var description = '';
  var color = '';

  const labelmap3D = getters.labelmap3D(element, labelmapIndex);
  if (!labelmap3D) {
    return { name, description, color };
  }

  const metadata = labelmap3D.metadata.data
    ? labelmap3D.metadata.data
    : labelmap3D.metadata;
  if (!metadata) {
    return { name, description, color };
  }

  const segData =
    metadata.find(data => data?.SegmentNumber === segmentIndex) ||
    metadata[segmentIndex];
  name = segData?.SegmentLabel;
  description = segData?.SegmentDescription;

  const segmentationModule = cornerstoneTools.getModule('segmentation');
  const colorLutTable =
    segmentationModule.state.colorLutTables[labelmap3D.colorLUTIndex];
  color = colorLutTable[segmentIndex];

  return { name, description, color };
}

function updateSegment(
  element,
  labelmapIndex,
  segmentIndex,
  buffer,
  numberOfFrames,
  operation,
  slice = -1
) {
  getters.labelmap2D(element, 0);
  setters.activeLabelmapIndex(element, 0);

  const labelmap3D = getters.labelmap3D(element, 0);
  if (!labelmap3D) {
    return;
  }

  const metadata = labelmap3D.metadata.data
    ? labelmap3D.metadata.data
    : labelmap3D.metadata;
  if (!metadata) {
    return;
  }
  // Segments on LabelMap
  const segmentsOnLabelmap = metadata
    .filter(x => x && x.SegmentNumber)
    .map(x => x.SegmentNumber);
  segmentsOnLabelmap.unshift(0);

  const labelmaps2D = labelmap3D.labelmaps2D;
  const slicelengthInBytes = buffer.byteLength / (numberOfFrames * 2);

  if (!labelmaps2D.length || labelmaps2D.length < 1) {
    operation = undefined;
  }
  const ranges = {};
  let useSourceBufferForAnyImage = false;
  const toolState = cornerstoneTools.getToolState(element, 'stack');
  const stackData = toolState.data[0];
  const imageIds = stackData.imageIds;
  const promises = [];
  for (let i = 0; i < numberOfFrames; i++) {
    if (slice >= 0 && i !== slice) {
      // do only one slice (in case of 3D Volume but 2D result e.g. Deeprow2D)
      continue;
    }

    let promise = cornerstone.loadImage(imageIds[i]).then(image => {
      let { operation, useSourceBuffer } = assignSegmentIndexToLabelmap(
        i,
        labelmap3D,
        operation,
        slicelengthInBytes,
        buffer,
        segmentIndex,
        ranges,
        image,
        segmentsOnLabelmap
      );
      useSourceBufferForAnyImage =
        useSourceBufferForAnyImage || useSourceBuffer;
    });
    promises.push(promise);
  }
  return Promise.all(promises).then(() => {
    let { name, color } = getSegmentInfo(element, labelmapIndex, segmentIndex);
    const stateValue = store.getState();
    let colorRanges = {};
    Object.keys(ranges).map(range => {
      colorRanges = {
        label: name,
        color,
        range: ranges[range],
      };
      addSmartRanges(stateValue, colorRanges);
    });
    const smartRanges = getAllSmartCTRanges();
    const existingRange = smartRanges.find(range => range.label === name);
    if (existingRange) {
      existingRange.range.min =
        existingRange?.range?.min || colorRanges?.range?.min || Infinity;
      existingRange.range.max =
        existingRange?.range?.max || colorRanges?.range?.max || -Infinity;
      store.dispatch(setSmartCTRange({ ...existingRange }));
    }

    let srcBuffer = labelmap3D.buffer;
    labelmap3D.buffer = useSourceBufferForAnyImage ? srcBuffer : buffer;
    cornerstone.updateImage(element);
  });
}
function getAllSmartCTRanges() {
  const stateValue = store.getState();
  const availableSmartRanges = getAvailableSmartCTRanges(stateValue);
  const smartRanges = availableSmartRanges || getSmartCTRanges();
  return smartRanges;
}

function addSmartRanges(state, rangeItem = null) {
  const availableSmartRanges = getAvailableSmartCTRanges(state);
  if (rangeItem) {
    if (availableSmartRanges) {
      store.dispatch(addAvailableSmartRange(rangeItem));
    } else {
      addSmartCTRange(rangeItem);
    }
  }
}

function assignSegmentIndexToLabelmap(
  i,
  labelmap3D,
  operation,
  slicelengthInBytes,
  buffer,
  segmentIndex,
  ranges,
  image,
  segmentsOnLabelmap
) {
  const labelmaps2D = labelmap3D.labelmaps2D;
  // Update Buffer (2D/3D)
  let srcBuffer = labelmap3D.buffer;
  let useSourceBuffer = false;
  const segmentOffset = segmentIndex; // - 1;
  const storedPixelData = image.getPixelData();

  // no segments in this slice
  if (
    !labelmaps2D[i] ||
    !labelmaps2D[i].segmentsOnLabelmap ||
    !labelmaps2D[i].segmentsOnLabelmap.length
  ) {
    operation = 'override';
  } else {
    operation = 'overlap';
    labelmaps2D[i].segmentsOnLabelmap.forEach(segment => {
      if (segmentsOnLabelmap.findIndex(seg => seg === segment) < 0) {
        segmentsOnLabelmap.push(segment);
      }
    });
    segmentsOnLabelmap.sort((a, b) => a - b);
  }

  const sliceOffset = slicelengthInBytes * i;
  const sliceLength = slicelengthInBytes / 2;
  let pixelData = new Uint16Array(buffer, sliceOffset, sliceLength);
  let srcPixelData = new Uint16Array(srcBuffer, sliceOffset, sliceLength);

  if (operation === 'overlap' || operation === 'override') {
    useSourceBuffer = true;
  }
  for (let j = 0; j < pixelData.length; j++) {
    if (operation === 'overlap') {
      if (pixelData[j] > 0) {
        srcPixelData[j] = segmentOffset;
      }
    } else if (operation === 'override') {
      if (srcPixelData[j] === segmentIndex) {
        srcPixelData[j] = 0;
      }
      if (pixelData[j] > 0) {
        srcPixelData[j] = segmentOffset;
      }
    } else {
      if (pixelData[j] > 0) {
        pixelData[j] = segmentOffset;
      }
    }
    if (pixelData[j] > 0) {
      ranges[pixelData[j]] = ranges[pixelData[j]] || {
        min: Infinity,
        max: -Infinity,
      };
      const storedValue = storedPixelData[j] * image.slope + image.intercept;
      if (ranges[pixelData[j]].min > storedValue) {
        ranges[pixelData[j]].min = storedValue;
      }
      if (ranges[pixelData[j]].max < storedValue) {
        ranges[pixelData[j]].max = storedValue;
      }
    }
  }

  pixelData = useSourceBuffer ? srcPixelData : pixelData;
  labelmaps2D[i] = { pixelData, segmentsOnLabelmap };
  return { operation, useSourceBuffer };
}

function updateSegmentMeta(
  element,
  labelmapIndex,
  segmentIndex,
  label = undefined,
  desc = undefined,
  color = undefined
) {
  const labelmap3D = getters.labelmap3D(element, labelmapIndex);
  if (!labelmap3D) {
    return;
  }

  const metadata = labelmap3D.metadata.data
    ? labelmap3D.metadata.data
    : labelmap3D.metadata;
  if (!metadata) {
    return;
  }

  if (label) {
    metadata[segmentIndex].SegmentLabel = label;
  }
  if (desc) {
    metadata[segmentIndex].SegmentDescription = desc;
  }

  if (color) {
    const segmentationModule = cornerstoneTools.getModule('segmentation');
    const colorLutTable =
      segmentationModule.state.colorLutTables[labelmap3D.colorLUTIndex];

    colorLutTable[segmentIndex][0] = color.r;
    colorLutTable[segmentIndex][1] = color.g;
    colorLutTable[segmentIndex][2] = color.b;
  }
}

function deleteSegment(element, labelmapIndex, segmentIndex, header) {
  if (!element || !segmentIndex) {
    return;
  }

  const labelmap3D = getters.labelmap3D(element, labelmapIndex);
  if (!labelmap3D) {
    return;
  }

  // TODO:: which one is standard metadata.data[] or metadata[] ???
  if (labelmap3D.metadata && labelmap3D.metadata.data) {
    let newData = [undefined];
    for (let i = 1; i < labelmap3D.metadata.data.length; i++) {
      const meta = labelmap3D.metadata.data[i];
      if (segmentIndex !== meta.SegmentNumber) {
        newData.push(meta);
      }
    }
    labelmap3D.metadata.data = newData;
  }

  // remove segments mapping
  const labelmaps2D = labelmap3D.labelmaps2D;
  for (let i = 0; i < labelmaps2D.length; i++) {
    const labelmap2D = labelmaps2D[i];
    if (labelmap2D && labelmap2D.segmentsOnLabelmap.includes(segmentIndex)) {
      const indexOfSegment = labelmap2D.segmentsOnLabelmap.indexOf(
        segmentIndex
      );
      labelmap2D.segmentsOnLabelmap.splice(indexOfSegment, 1);
    }
  }

  // cleanup buffer
  let z = new Uint16Array(labelmap3D.buffer);
  for (let i = 0; i < z.length; i++) {
    if (z[i] === segmentIndex) {
      z[i] = 0;
    }
  }
  cornerstone.updateImage(element);
}

function clearSegment(element, labelmapIndex, segmentIndex, header) {
  if (!element || !segmentIndex) {
    return;
  }

  const labelmap3D = getters.labelmap3D(element, labelmapIndex);
  if (!labelmap3D) {
    return;
  }

  // cleanup buffer
  let z = new Uint16Array(labelmap3D.buffer);
  for (let i = 0; i < z.length; i++) {
    if (z[i] === segmentIndex) {
      z[i] = 0;
    }
  }
  cornerstone.updateImage(element);
}

function setSegmentToolActive() {
  const state = store.getState();
  const allowedTools = selectAllowedActiveTools(
    state,
    selectActiveViewportIndex(state)
  );
  const activeTool = state.infusions?.activeTool;
  const isAnySegmentationToolActive =
    segmentationTools.includes(activeTool) &&
    !autoSelectionRestrictedTools.includes(activeTool);
  if (!isAnySegmentationToolActive) {
    if (allowedTools) {
      const tool = segmentationTools.find(
        toolName => !autoSelectionRestrictedTools.includes(toolName)
      );
      if (tool) {
        store.dispatch(setActiveTool(tool));
      }
    } else {
      store.dispatch(setActiveTool('Brush'));
    }
  }
}

export {
  getImageIdsForDisplaySet,
  getLabelMaps,
  flattenLabelmaps,
  createSegment,
  clearSegment,
  getSegmentInfo,
  updateSegment,
  deleteSegment,
  updateSegmentMeta,
  getFirstSegmentId,
  createSegmentItem,
  setSegmentToolActive,
};
