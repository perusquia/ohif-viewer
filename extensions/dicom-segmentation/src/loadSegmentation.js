import cornerstoneTools from 'cornerstone-tools';
import { Utils } from '@flywheel/extension-flywheel-common';

const { getters, setters } = cornerstoneTools.getModule('segmentation');
const { segmentationUtils } = Utils;

export default async function loadSegmentation(
  segDisplaySet,
  referencedDisplaySet,
  studies,
  labelmapBuffer,
  segMetadata,
  segmentsOnFrame,
  labelmapSegments,
  segmentationIndex = 1,
  skipLabelMapReadjust = false
) {
  const { StudyInstanceUID } = referencedDisplaySet;

  const imageIds = segmentationUtils.getImageIdsForDisplaySet(
    studies,
    StudyInstanceUID,
    referencedDisplaySet.SeriesInstanceUID
  );

  // TODO: Could define a color LUT based on colors in the SEG.
  const labelmapIndex = segmentationUtils.getNextLabelmapIndex(imageIds[0]);
  const colorLUTIndex = 0; //segmentationUtils.makeColorLUTAndGetIndex(segMetadata);

  setters.labelmap3DByFirstImageId(
    imageIds[0],
    labelmapBuffer,
    labelmapIndex,
    segMetadata.data,
    imageIds.length,
    segmentsOnFrame,
    colorLUTIndex
  );

  if (!segDisplaySet.labelmapSegments) {
    segDisplaySet.labelmapSegments = {};
  }

  segDisplaySet.labelmapIndexes.push(labelmapIndex);
  segDisplaySet.segmentationIndexes.push(segmentationIndex);
  /**
   * Cache each labelmap segments.
   * This data is used to determine the active label map when a given segment is activated/clicked.
   */
  segDisplaySet.labelmapSegments[labelmapIndex] = labelmapSegments.length
    ? Array.from(
        new Set(labelmapSegments.filter(a => !!a).reduce((a, b) => a.concat(b)))
      )
    : [];
  const { state } = cornerstoneTools.getModule('segmentation');
  const brushStackState = state.series[imageIds[0]];
  // Update active label map later when image loaded in viewport
  brushStackState.activeLabelmapIndex = undefined;
  const labelmaps3D = brushStackState.labelmaps3D[labelmapIndex];
  labelmaps3D.activeSegmentIndex = segmentationIndex;
  correctSegmentIndexOnLabelMap2D(labelmaps3D, segmentationIndex);
  if (labelmapIndex > 1 && !skipLabelMapReadjust) {
    // Push the new segment to front to manage the rendering order.
    const itemIndex = segDisplaySet.labelmapIndexes.length - 1;
    segmentationUtils.rePositionSegment(
      segDisplaySet,
      referencedDisplaySet,
      studies,
      1,
      itemIndex
    );
    segDisplaySet.labelmapIndexes[segDisplaySet.labelmapIndexes.length - 1] = 1;
    segDisplaySet.labelmapIndexes.forEach((mapIndex, index) => {
      if (segDisplaySet.labelmapIndexes.length - 1 === index) {
        return;
      }
      segDisplaySet.labelmapIndexes[index] = mapIndex + 1;
    });
  }

  // Segment editing is not supported now for DICOM SEG, resetting label map index to 0 always.
  if (0 !== getters.activeLabelmapIndexOffline(imageIds)) {
    setters.activeLabelmapIndexOffline(imageIds, 0);
  }
  /*
   * TODO: Improve the way we notify parts of the app that depends on segs to be loaded.
   *
   * Currently we are using a non-ideal implementation through a custom event to notify the segmentation panel
   * or other components that could rely on loaded segmentations that
   * the segments were loaded so that e.g. when the user opens the panel
   * before the segments are fully loaded, the panel can subscribe to this custom event
   * and update itself with the new segments.
   *
   * This limitation is due to the fact that the cs segmentation module is an object (which will be
   * updated after the segments are loaded) that React its not aware of its changes
   * because the module object its not passed in to the panel component as prop but accessed externally.
   *
   * Improving this event approach to something reactive that can be tracked inside the react lifecycle,
   * allows us to easily watch the module or the segmentations loading process in any other component
   * without subscribing to external events.
   */
  console.log('Segmentation loaded.');
  const event = new CustomEvent('extensiondicomsegmentationsegloaded');
  document.dispatchEvent(event);

  return labelmapIndex;
}

function correctSegmentIndexOnLabelMap2D(labelmaps3D, segmentationIndex) {
  const labelmaps2D = labelmaps3D.labelmaps2D;
  labelmaps2D.forEach(labelMap2D => {
    if (labelMap2D) {
      labelMap2D.pixelData.forEach((pixel, index) => {
        if (pixel > 0 && pixel !== segmentationIndex) {
          labelMap2D.pixelData[index] = segmentationIndex;
        }
      });
    }
  });
}
