const tiffSchema = 'tiff:';
const configApiRoot = window.config.apiRoot;
const rootUrl = configApiRoot || window.origin;
const protocol = rootUrl.split('//')[0];

export function toTiffSchema(imageId) {
  return imageId.replace(protocol, tiffSchema);
}

export function getImageUrl(imageUrl) {
  const host = window.location.host;
  const apiRoot = configApiRoot || '';
  if (imageUrl.includes(host)) {
    const urlWithoutHost = imageUrl.split(host)[1] || '';
    // Return the file url with configured api root if any without the host.
    if (urlWithoutHost.includes(apiRoot)) {
      return urlWithoutHost;
    }
    return apiRoot + urlWithoutHost;
  }
  return imageUrl.replace(tiffSchema, protocol);
}
