import { getImageUrl } from './getImageId.js';
import Tiff from 'tiff.js';
import createImage from './createImage.js';

let options = {};

export function configure(opts) {
  options = opts;
}

export function getDataUrl(buffer) {
  const MAX_MEMORY = 256 * 1024 * 1024;

  Tiff.initialize({
    TOTAL_MEMORY: MAX_MEMORY,
  });
  const tiff = new Tiff({
    buffer,
  });
  return tiff.toDataURL();
}

/**
 * This is a copy from tiffImageLoader with some adjustments.
 * @param {string} imageId
 */
function loadImage(imageId) {
  let url = getImageUrl(imageId);
  const xhr = new XMLHttpRequest();

  xhr.open('GET', url);
  xhr.responseType = 'arraybuffer';
  options.beforeSend(xhr);

  const promise = new Promise((resolve, reject) => {
    xhr.onload = function() {
      try {
        const arrayBuffer = xhr.response;
        const image = new Image();
        image.onload = function() {
          const imageData = createImage(image, imageId);
          resolve(imageData);
        };
        image.src = getDataUrl(arrayBuffer);
      } catch (error) {
        const fileName = imageId.substring(imageId.lastIndexOf('/') + 1);
        reject(new Error(`${error.message}. Couldn't open file '${fileName}'`));
      }
    };
    xhr.onerror = function(error) {
      reject(error);
    };
    xhr.send();
  });

  const cancelFn = () => {
    xhr.abort();
  };

  return {
    promise,
    cancelFn,
  };
}

export default loadImage;
