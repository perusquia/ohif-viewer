import initImageLoader from './initImageLoader';

export default function initExtension() {
  initImageLoader();
}
