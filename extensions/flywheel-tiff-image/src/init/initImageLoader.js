import cornerstone from 'cornerstone-core';
import imageLoader from '../imageLoader';
import { HTTP as FlywheelCommonHTTP } from '@flywheel/extension-flywheel-common';
import { configure } from '../imageLoader/loadImage';
const { getAuthToken } = FlywheelCommonHTTP.utils.AUTH_UTILS;

export default function initImageLoader() {
  imageLoader.register(cornerstone);
  configure({
    // Send the auth token for image loader requests
    beforeSend: xhr => {
      xhr.setRequestHeader('authorization', getAuthToken());
    },
  });
}
