import loadImage from './loadImage.js';
import register from './register.js';

export default {
  loadImage,
  register,
};
