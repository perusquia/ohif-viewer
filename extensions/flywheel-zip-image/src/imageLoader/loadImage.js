import store from '@ohif/viewer/src/store';
import { utils } from '@ohif/core';
import {
  Redux as FlywheelCommonRedux,
  HTTP as FlywheelCommonHTTP,
} from '@flywheel/extension-flywheel-common';
import createImage from '@flywheel/extension-flywheel-tiff-image/src/imageLoader/createImage';
import { getDataUrl } from '@flywheel/extension-flywheel-tiff-image/src/imageLoader/loadImage.js';
import { getImageDataFromUrl } from './getImageData.js';

const { selectCurrentWebImages } = FlywheelCommonRedux.selectors;
const { getZipMember } = FlywheelCommonHTTP.services;
const { TIFF_IMAGES, WEB_IMAGES } = utils.regularExpressions;

function loadImage(imageurl) {
  const { path: imagePath, id, zipName } = getImageDataFromUrl(imageurl);

  const promise = new Promise(async (resolve, reject) => {
    const fileId = getFileId(zipName);
    const zipMemberPromise = await getZipMember(fileId, imagePath);
    const buffer = await zipMemberPromise.arrayBuffer();
    const image = new Image();

    image.onload = () => {
      const imageData = createImage(image, id);
      resolve(imageData);
    };

    image.onerror = error => {
      reject(error);
    };

    try {
      const src = createImageSrc(buffer, imagePath);
      image.src = src;
    } catch (error) {
      reject(error);
    }
  });

  return {
    promise,
  };
}

const createImageSrc = (buffer, imagePath) => {
  let src;

  try {
    if (TIFF_IMAGES.test(imagePath)) {
      src = getDataUrl(buffer);
    } else if (WEB_IMAGES.test(imagePath)) {
      const arrayBufferView = new Uint8Array(buffer);
      const blob = new Blob([arrayBufferView]);
      const urlCreator = window.URL || window.webkitURL;
      const imageUrl = urlCreator.createObjectURL(blob);
      src = imageUrl;
    } else {
      throw new Error();
    }
    return src;
  } catch (error) {
    throw new Error(`Could not open file from path '${imagePath}'`);
  }
};

const getFileId = fileName => {
  const state = store.getState();
  const currentWebImages = selectCurrentWebImages(state);
  const currentImage = currentWebImages.find(
    image => image.filename === fileName
  );
  return currentImage.file_id;
};

export default loadImage;
