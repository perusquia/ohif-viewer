import cornerstone from 'cornerstone-core';
import imageLoader from '../imageLoader';

export default function initImageLoader() {
  imageLoader.register(cornerstone);
}
