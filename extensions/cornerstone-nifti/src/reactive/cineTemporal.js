import { distinctUntilChanged, map, switchMap } from 'rxjs/operators';
import { Subscription, EMPTY, interval } from 'rxjs';
import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';

let cineSubscription$ = Subscription.EMPTY;

const CINE_PLAYING_PROPERTY = 'isPlaying';
const CINE_FRAME_RATE_PROPERTY = 'cineFrameRate';
const PLAY_ON_CURRENT_ONLY = true; // original cine only plays when on activeViewport

/**
 * Return value of given prop key from viewport cine content. Otherwise return undefined.
 * @param {object} viewportData Viewerport data to get cine property
 * @param {string} prop Cine key property to look for.
 * @return {any | undefined} Property value or undefined if not existing
 */
const _getCineProperty = (viewportData = {}, prop) => {
  const cineObject = viewportData.cine || {};
  return cineObject[prop];
};

/**
 * Get existing viewport data in case cine is Playing. Otherwise returns undefined
 * @param {Object} Object containing viewports data (each display viewport will produce a object entry)
 * @return {object | undefined}
 */
const _getCurrentViewportCine = (
  viewportsSpecificData = {},
  activeViewportIndex
) => {
  if (PLAY_ON_CURRENT_ONLY) {
    return viewportsSpecificData[activeViewportIndex];
  }

  // keep playing event if active viewport is not the original playing one
  const current = Object.values(viewportsSpecificData).find(data => {
    const isPlaying = _getCineProperty(data, CINE_PLAYING_PROPERTY);
    return !!isPlaying;
  });

  return current;
};

/**
 * Set up subscription which will call operationCallback based on a time interval and current store state. In case store changes it evaluates/update emitter
 * @param {function} operationCallback Callback method to be consumed whenever cineAutoPlay should tick
 */
export async function subscribeCineTemporalSync(
  operationCallback = () => {},
  storeObservable$
) {
  unsubscribeCineTemporalSync();
  // subscription to handle state change/update
  cineSubscription$ = storeObservable$
    .pipe(
      // map to object with current viewport index, actual frame rate and if its playing
      map(state => {
        const viewportSpecificData = FlywheelCommonRedux.selectors.selectViewportSpecificData(
          state
        );
        const activeViewportIndex = FlywheelCommonRedux.selectors.selectActiveViewportIndex(
          state
        );
        const viewportCine =
          _getCurrentViewportCine(viewportSpecificData, activeViewportIndex) ||
          {};
        const actualFrameRate = _getCineProperty(
          viewportCine,
          CINE_FRAME_RATE_PROPERTY
        );

        const isPlaying = _getCineProperty(viewportCine, CINE_PLAYING_PROPERTY);

        return {
          activeViewportIndex,
          isPlaying,
          actualFrameRate,
        };
      }),
      // distinct when isPlaying or actualFrameRate has changed
      distinctUntilChanged((prev = {}, curr = {}) => {
        const {
          actualFrameRate: currFrameRate,
          isPlaying: currIsPlaying,
        } = curr;

        const {
          actualFrameRate: prevFrameRate,
          isPlaying: prevIsPlaying,
        } = prev;

        return (
          prevIsPlaying === currIsPlaying && prevFrameRate === currFrameRate
        );
      }),
      // switch map to Observable interval (in case playing) or complete it
      switchMap(({ actualFrameRate, isPlaying }) => {
        if (actualFrameRate && isPlaying) {
          // how many ms to tick a new frame
          const intervalValue = 1000 / actualFrameRate;
          const _intervalValue = Number(intervalValue.toFixed(0));

          return interval(_intervalValue);
        }
        return EMPTY;
      })
    )
    .subscribe(() => {
      operationCallback();
    });
}

/**
 * Stop cine temporal autoplay and store state subscription.
 */
function unsubscribeCineTemporalSync() {
  cineSubscription$.unsubscribe();
}

/** Reactive object structure */

// other methods
const subscribers = {
  subscribeCineTemporalSync,
};

const unsubscribers = {
  unsubscribeCineTemporalSync,
};

// define package object
const reactive = { subscribers, unsubscribers };

export default reactive;
