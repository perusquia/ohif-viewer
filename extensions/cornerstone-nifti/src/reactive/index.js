import cineTemporal from './cineTemporal';

const reactive = {
  cineTemporal,
};

export default reactive;
