import cornerstone from 'cornerstone-core';
import navigateTimeInViewports from './utils/navigateTimeInViewport';
import isTemporalCommandDisabled from './utils/isTemporalDisabled';
import Reactive from './reactive';
import {
  Reactive as FlywheelCommonReactive,
  Utils as FlywheelCommonUtils,
  Redux as FlywheelCommonRedux,
} from '@flywheel/extension-flywheel-common';
import { getEnabledElement } from '../../cornerstone/src/state';
import scrollToCenter from '../../cornerstone-infusions/src/utils/scrollToCenter';
import {
  cornerstoneUtils,
  updateOtherViewports,
} from '@flywheel/extension-cornerstone-infusions';
import OHIF from '@ohif/core';
import {
  Redux as FlywheelRedux,
  Utils as FlywheelUtils,
} from '@flywheel/extension-flywheel';
import cornerstoneTools from 'cornerstone-tools';
import { LinkSeriesTool } from '../../cornerstone-infusions/src/tools';

const { resetLayoutToHP } = FlywheelUtils;
const { selectActiveProtocolStore } = FlywheelRedux.selectors;
const { setProtocolLayout } = FlywheelCommonRedux.actions;
const { setViewportActive, setViewportLayoutAndData } = OHIF.redux.actions;
const { studyMetadataManager } = OHIF.utils;
const scroll = cornerstoneTools.import('util/scroll');
const scrollToIndex = cornerstoneTools.import('util/scrollToIndex');

const commandsModule = ({ servicesManager, commandsManager, appConfig }) => {
  const { store } = appConfig;
  let isInSingleView = false;
  let viewportSpecificDatas = {};
  let viewportRestoreProperties = {
    propertiesToSync: [],
    numRows: 1,
    numColumns: 1,
    clickedViewportIndex: 0,
  };
  const actions = {
    updateTemporalViewportsSeries: ({
      viewports: viewportsStore,
      direction,
    }) => {
      const { viewportSpecificData = {} } = viewportsStore || {};
      navigateTimeInViewports(viewportSpecificData, direction, store);
    },
    playTemporalViewportsSeries: ({
      viewports: viewportsStore,
      direction,
      playing,
    }) => {
      const runUpdate = () => {
        commandsManager.runCommand('nextTemporalSeries');
      };

      if (playing) {
        Reactive.cineTemporal.subscribers.subscribeCineTemporalSync(
          runUpdate,
          FlywheelCommonReactive.store.observables.storeObservable$
        );
      } else {
        Reactive.cineTemporal.unsubscribers.unsubscribeCineTemporalSync();
      }
    },
    resetViewport: ({ viewports }) => {
      Object.keys(viewports.viewportSpecificData).forEach(viewportIndex => {
        const enabledElement = getEnabledElement(viewportIndex);
        if (enabledElement) {
          cornerstone.reset(enabledElement);
          scrollToCenter([enabledElement]);

          const state = store.getState();
          const protocolStore = selectActiveProtocolStore(state);
          // Reset to hanging protocol layout if a valid protocol already applied and if the current layout
          // or the viewport displaySet is different
          if (protocolStore) {
            resetLayoutToHP(
              commandsManager,
              store,
              protocolStore,
              isAlreadyMatching => {
                if (isAlreadyMatching) {
                  const viewportSpecificData =
                    viewports.viewportSpecificData[
                      viewports.activeViewportIndex
                    ];
                  updateOtherViewports(
                    commandsManager,
                    ['Wwwc', 'StackScroll', 'Zoom'],
                    viewportSpecificData,
                    enabledElement,
                    true
                  );
                }
              }
            );
            isInSingleView = false;
          } else {
            const viewportSpecificData =
              viewports.viewportSpecificData[viewports.activeViewportIndex];
            updateOtherViewports(
              commandsManager,
              ['Wwwc', 'StackScroll', 'Zoom'],
              viewportSpecificData,
              enabledElement,
              true
            );
          }
        }
      });
      store.dispatch(setViewportActive(0));
    },
    updateNiftiSingleView: ({ plugin }) => {
      let viewports = store.getState().viewports || {};
      if (!isInSingleView) {
        isInSingleView = true;
        const activeViewportIndex = viewports.activeViewportIndex;
        viewportRestoreProperties = {
          propertiesToSync: [],
          numRows: viewports.numRows,
          numColumns: viewports.numColumns,
          clickedViewportIndex: activeViewportIndex,
        };
        viewportSpecificDatas = viewports.viewportSpecificData;

        const enabledElements = cornerstone.getEnabledElements();
        enabledElements.forEach((element, index) => {
          const toolState = cornerstoneTools.getToolState(
            element.element,
            'stack'
          );
          const stackData = toolState.data[0];

          viewportRestoreProperties.propertiesToSync.push({
            seriesInstanceUID: viewportSpecificDatas[index].SeriesInstanceUID,
            studyInstanceUID: viewportSpecificDatas[index].StudyInstanceUID,
            windowLevel: element.viewport.voi,
            zoom: { type: 'exact', scale: element.viewport.scale },
            rotate: element.viewport.rotation,
            scrollIndex: stackData.currentImageIdIndex,
            viewportIndex: index,
          });
        });

        let currentDisplaySet = {};
        currentDisplaySet[0] = viewportSpecificDatas[activeViewportIndex];
        const viewport = [{ height: '100%', width: '100%', plugin: plugin }];
        store.dispatch(setProtocolLayout(null));
        store.dispatch(
          setViewportLayoutAndData(
            {
              numRows: 1,
              numColumns: 1,
              viewports: viewport,
            },
            currentDisplaySet
          )
        );

        let interval = setInterval(() => {
          viewports = store.getState().viewports || {};
          if (viewports.numColumns * viewports.numRows === 1) {
            const enabledElements = cornerstone.getEnabledElements();
            if (!enabledElements.find(element => !element.image)) {
              clearInterval(interval);
              interval = null;
              actions.updateNiftiViewportProperties({
                elementIndex: 0,
                viewportData:
                  viewportRestoreProperties.propertiesToSync[
                    activeViewportIndex
                  ],
                activeViewportIndex: 0,
              });
            }
          }
        }, 5);
      } else {
        isInSingleView = false;
        const enabledElement = cornerstone.getEnabledElements()[0];
        const numRows = viewportRestoreProperties.numRows;
        const numColumns = viewportRestoreProperties.numColumns;
        const singleViewIndex = viewportRestoreProperties.clickedViewportIndex;

        const scrollLink = LinkSeriesTool.tools.scrollLink.isEnabled;
        const wwwcLink = LinkSeriesTool.tools.wwwcLink.isEnabled;
        const zoomLink = LinkSeriesTool.tools.zoomLink.isEnabled;
        const orientLink = LinkSeriesTool.tools.orientLink.isEnabled;

        // Merge the single view property changes to restore properties array
        if (singleViewIndex >= 0) {
          const study = studyMetadataManager.get(
            viewportSpecificDatas[singleViewIndex].StudyInstanceUID
          );
          const displaySet = study.displaySets.find(
            image =>
              image.SOPInstanceUID ===
              viewportSpecificDatas[singleViewIndex].SOPInstanceUID
          );
          Object.keys(viewportSpecificDatas).forEach((key, index) => {
            if (index != singleViewIndex) {
              let view = viewportSpecificDatas[key];
              if (
                view.SeriesInstanceUID ===
                  viewportSpecificDatas[singleViewIndex].SeriesInstanceUID &&
                view.StudyInstanceUID ===
                  viewportSpecificDatas[singleViewIndex].StudyInstanceUID
              ) {
                viewportRestoreProperties.propertiesToSync[
                  index
                ] = Object.assign(
                  viewportRestoreProperties.propertiesToSync[index],
                  {
                    windowLevel: wwwcLink
                      ? enabledElement.viewport.voi
                      : viewportRestoreProperties.propertiesToSync[index]
                          .windowLevel,
                    zoom: zoomLink
                      ? { type: 'exact', scale: enabledElement.viewport.scale }
                      : viewportRestoreProperties.propertiesToSync[index].zoom,
                    rotate: orientLink
                      ? enabledElement.viewport.rotation
                      : viewportRestoreProperties.propertiesToSync[index]
                          .rotate,
                    scrollIndex: scrollLink
                      ? displaySet?.frameIndex
                      : viewportRestoreProperties.propertiesToSync[index]
                          .scrollIndex,
                  }
                );
              }
            }
          });
          viewportRestoreProperties.propertiesToSync[
            singleViewIndex
          ] = Object.assign(
            viewportRestoreProperties.propertiesToSync[singleViewIndex],
            {
              windowLevel: enabledElement.viewport.voi,
              zoom: { type: 'exact', scale: enabledElement.viewport.scale },
              rotate: enabledElement.viewport.rotation,
              scrollIndex: displaySet?.frameIndex,
            }
          );
        }

        const layouts = [];
        Object.keys(viewportSpecificDatas).forEach(key => {
          layouts.push({ plugin: 'cornerstone::nifti' });
        });

        store.dispatch(setProtocolLayout(null));
        store.dispatch(
          setViewportLayoutAndData(
            {
              numRows,
              numColumns,
              viewports: layouts,
            },
            viewportSpecificDatas
          )
        );

        let interval = setInterval(() => {
          viewports = store.getState().viewports || {};
          if (
            viewports.numColumns * viewports.numRows ===
            numRows * numColumns
          ) {
            const enabledElements = cornerstone.getEnabledElements();
            if (!enabledElements.find(element => !element.image)) {
              clearInterval(interval);
              interval = null;
              viewportRestoreProperties.propertiesToSync.forEach(
                (propertiesToSync, index) => {
                  actions.updateNiftiViewportProperties({
                    elementIndex: index,
                    viewportData: propertiesToSync,
                    activeViewportIndex: singleViewIndex,
                  });
                }
              );
              store.dispatch(setViewportActive(0));
            }
          }
        }, 5);
      }
    },
    updateNiftiViewportProperties: ({
      elementIndex,
      viewportData,
      activeViewportIndex,
    }) => {
      const enabledElement = cornerstone.getEnabledElements()[elementIndex];
      if (!enabledElement.image) {
        return;
      }
      const imageId = enabledElement.image.imageId;
      const instance = cornerstone.metaData.get('instance', imageId);
      if (!instance) {
        return;
      }
      const { StudyInstanceUID, SeriesInstanceUID } = instance;
      const propertiesToSync = viewportData;

      const study = studyMetadataManager.get(StudyInstanceUID);
      if (
        StudyInstanceUID === propertiesToSync.studyInstanceUID &&
        (SeriesInstanceUID === propertiesToSync.seriesInstanceUID ||
          (study.displaySets.length &&
            study.displaySets[0].plugin === 'cornerstone::nifti'))
      ) {
        let viewport = cornerstone.getViewport(enabledElement.element);

        if (propertiesToSync?.windowLevel) {
          viewport.voi = propertiesToSync.windowLevel;
        }
        if (propertiesToSync?.zoom) {
          cornerstone.resize(enabledElement.element, true);
          const defaultViewport = cornerstone.getDefaultViewport(
            enabledElement.canvas,
            enabledElement.image
          );
          let updatedScale = viewport.scale;
          if (propertiesToSync.zoom.type === 'exact') {
            updatedScale = propertiesToSync.zoom.scale;
          } else if (propertiesToSync.zoom.type === 'rescale') {
            if (
              propertiesToSync.zoom.viewportDim &&
              propertiesToSync.zoom.scale
            ) {
              const canvasWidth = enabledElement.canvas.width;
              const canvasHeight = enabledElement.canvas.height;
              const relWidthChange =
                canvasWidth / propertiesToSync.zoom.viewportDim.width;
              const relHeightChange =
                canvasHeight / propertiesToSync.zoom.viewportDim.height;
              const relChange = Math.sqrt(relWidthChange * relHeightChange);
              updatedScale = relChange * propertiesToSync.zoom.scale;
            } else {
              updatedScale =
                defaultViewport.scale + propertiesToSync.zoom.scale;
            }
          } else {
            updatedScale = defaultViewport.scale + propertiesToSync.zoom.scale;
          }
          viewport.scale = updatedScale;
        }
        if (propertiesToSync?.rotate) {
          viewport.rotation = propertiesToSync.rotate;
        }
        cornerstone.setViewport(enabledElement.element, viewport);
        if (propertiesToSync?.patientPos) {
          const toolState = cornerstoneTools.getToolState(
            enabledElement.element,
            'stack'
          );
          const stackData = toolState.data[0];
          const newImageIdIndex = cornerstoneUtils.getNearestSliceIndex(
            enabledElement,
            propertiesToSync.patientPos
          );
          scroll(
            enabledElement.element,
            newImageIdIndex - stackData.currentImageIdIndex
          );
        }
        if (propertiesToSync?.scrollIndex) {
          scrollToIndex(enabledElement.element, propertiesToSync.scrollIndex);
        }
      }
      if (activeViewportIndex >= 0) {
        store.dispatch(setViewportActive(activeViewportIndex));
      }
    },
  };

  const definitions = {
    nextTemporalSeries: {
      commandFn: actions.updateTemporalViewportsSeries,
      storeContexts: ['viewports'],
      options: { direction: 1 },
      isDisabled: args => {
        const { viewports = {} } = args || {};
        return isTemporalCommandDisabled(viewports);
      },
      disabledStoreContexts: ['viewports'],
    },
    lastTemporalSeries: {
      commandFn: actions.updateTemporalViewportsSeries,
      storeContexts: ['viewports'],
      options: { direction: Number.POSITIVE_INFINITY },
      isDisabled: args => {
        const { viewports = {} } = args || {};
        return isTemporalCommandDisabled(viewports);
      },
      disabledStoreContexts: ['viewports'],
    },
    firstTemporalSeries: {
      commandFn: actions.updateTemporalViewportsSeries,
      storeContexts: ['viewports'],
      options: { direction: Number.NEGATIVE_INFINITY },
      isDisabled: args => {
        const { viewports = {} } = args || {};
        return isTemporalCommandDisabled(viewports);
      },
      disabledStoreContexts: ['viewports'],
    },
    previousTemporalSeries: {
      commandFn: actions.updateTemporalViewportsSeries,
      storeContexts: ['viewports'],
      options: { direction: -1 },
      isDisabled: args => {
        const { viewports = {} } = args || {};
        return isTemporalCommandDisabled(viewports);
      },
      disabledStoreContexts: ['viewports'],
    },
    playTemporalSeries: {
      commandFn: actions.playTemporalViewportsSeries,
      storeContexts: ['viewports'],
      options: {},
      isDisabled: args => {
        const { viewports = {} } = args || {};
        return isTemporalCommandDisabled(viewports);
      },
      disabledStoreContexts: ['viewports'],
    },
    resetViewport: {
      commandFn: actions.resetViewport,
      storeContexts: ['viewports'],
      options: {},
    },
    updateNiftiSingleView: {
      commandFn: actions.updateNiftiSingleView,
      context: 'VIEWER',
      options: {},
    },
    update2DNiftiViewport: {
      commandFn: actions.updateNiftiViewportProperties,
      storeContexts: [],
      options: {},
    },
  };

  return {
    actions,
    definitions,
    defaultContext: 'ACTIVE_VIEWPORT::CORNERSTONE::NIFTI',
  };
};

export default commandsModule;
