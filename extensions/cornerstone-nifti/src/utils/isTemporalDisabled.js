export default function isTemporalCommandDisabled(viewportsStore = {}) {
  const { viewportSpecificData = {}, activeViewportIndex } = viewportsStore;

  const displaySet = viewportSpecificData[activeViewportIndex];
  if (displaySet) {
    const enabled = isNaN(Number(displaySet.timeSlices))
      ? true
      : displaySet.timeSlices > 1;
    return !enabled;
  }
  return false;
}
