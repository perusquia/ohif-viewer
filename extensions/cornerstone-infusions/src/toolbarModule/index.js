import colorMap from './colorMap';
import segmentation from './segmentation';
import common from './common';
import crosshairs from './crosshairs';
import linkSeries from './linkSeries';
import protocols from './protocols';
import colorChannel from './colorChannel';

const infusions = [
  colorMap,
  colorChannel,
  segmentation,
  linkSeries,
  crosshairs,
  common,
  protocols,
];

export default function toolbarModule({
  appConfig,
  commandsManager,
  servicesManager,
}) {
  const { store } = appConfig;
  const toolbarModule = infusions.reduce(
    (accumulator, getDefinition) => {
      const infusionDefinitions =
        getDefinition(store, commandsManager, servicesManager) || [];
      return {
        definitions: [...accumulator.definitions, ...infusionDefinitions],
        defaultContext: [
          'ACTIVE_VIEWPORT::CORNERSTONE',
          'ACTIVE_VIEWPORT::CORNERSTONE::NIFTI',
          'ACTIVE_VIEWPORT::CORNERSTONE::METAIMAGE',
        ],
        defaultPrecedence: 99,
      };
    },
    { definitions: [] }
  );

  return toolbarModule;
}
