import SupportToolbarComponent from '../components/toolbar/SupportToolbarComponent';

const toolbarDefinitions = [
  {
    id: 'HelpButton',
    label: 'Help',
    icon: 'question-circle',
    commandName: 'toggleHelp',
    context: [
      'ACTIVE_VIEWPORT::CORNERSTONE',
      'ACTIVE_VIEWPORT::CORNERSTONE::NIFTI',
      'ACTIVE_VIEWPORT::CORNERSTONE::METAIMAGE',
    ],
    options: { pull: 'right' },
  },
  {
    id: 'SupportButton',
    label: 'Support',
    icon: 'support_agent',
    class: 'material-icons',
    CustomComponent: SupportToolbarComponent,
    context: [
      'ACTIVE_VIEWPORT::CORNERSTONE',
      'ACTIVE_VIEWPORT::CORNERSTONE::NIFTI',
      'ACTIVE_VIEWPORT::VTK',
    ],
    options: { pull: 'right' },
  },
];

const getDefinitions = () => {
  return toolbarDefinitions;
};

export default getDefinitions;
