import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';

const getDefinitions = store => {
  const toolbarDefinitions = [
    {
      id: 'Crosshairs',
      label: 'Crosshairs',
      icon: 'crosshairs',
      type: 'setToolActive',
      getState: () => {
        const state = store.getState();
        return {
          visible:
            (FlywheelCommonRedux.selectors.selectViewports(state) || [])
              .length > 1,
        };
      },
      commandName: 'toggleCrosshairs',
      commandOptions: {},
      context: [
        'ACTIVE_VIEWPORT::CORNERSTONE',
        'ACTIVE_VIEWPORT::CORNERSTONE::NIFTI',
        'ACTIVE_VIEWPORT::CORNERSTONE::METAIMAGE',
      ],
    },
  ];

  return toolbarDefinitions;
};

export default getDefinitions;
