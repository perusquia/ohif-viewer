import Colormap from '../components/toolbar/colorMap';
import withCommandsManager from '../components/withCommandsManager';
import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';

const TOOLBAR_BUTTON_TYPES = {
  COMMAND: 'command',
  SET_TOOL_ACTIVE: 'setToolActive',
};

const getDefinitions = (store, commandsManager) => {
  const toolbarDefinitions = [
    {
      id: 'toggleColormap',
      label: 'Color Map',
      icon: 'color-map',
      CustomComponent: withCommandsManager(Colormap, commandsManager),
      commandName: 'changeColormap',
      resetButton: {
        id: 'resetColormap',
        label: 'Reset',
        icon: 'reset',
        type: TOOLBAR_BUTTON_TYPES.COMMAND,
        commandName: 'resetColormap',
      },
      context: [
        'ACTIVE_VIEWPORT::CORNERSTONE',
        'ACTIVE_VIEWPORT::CORNERSTONE::NIFTI',
        'ACTIVE_VIEWPORT::CORNERSTONE::METAIMAGE',
      ],
      getState: () => {
        return {
          visible: !FlywheelCommonUtils.isCurrentWebImage(),
        };
      },
    },
  ];
  return toolbarDefinitions;
};

export default getDefinitions;
