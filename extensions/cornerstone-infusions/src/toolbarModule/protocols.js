import { Redux as FlywheelRedux } from '@flywheel/extension-flywheel';
const { selectActiveProtocolStore } = FlywheelRedux.selectors;

const getProtocolsButtonState = store => {
  return () => {
    let protocolStore = selectActiveProtocolStore(store.getState()) || {};
    return {
      visible: protocolStore != null,
    };
  };
};

const getDefinitions = store => {
  const toolbarDefinitions = [
    {
      id: 'Protocols',
      label: 'Protocols',
      icon: 'th-list',
      getState: getProtocolsButtonState(store),
      commandName: 'switchProtocolMode',
      commandOptions: {},
      context: [
        'ACTIVE_VIEWPORT::CORNERSTONE',
        'ACTIVE_VIEWPORT::CORNERSTONE::NIFTI',
        'ACTIVE_VIEWPORT::CORNERSTONE::METAIMAGE',
        'ACTIVE_VIEWPORT::VTK',
      ],
    },
  ];

  return toolbarDefinitions;
};

export default getDefinitions;
