import cornerstoneTools from 'cornerstone-tools';
import {
  cornerstoneUtils,
  orientSynchronizer,
  zoomSynchronizer,
} from '../utils';
import {
  Utils as FlywheelCommonUtils,
  Redux as FlywheelCommonRedux,
} from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';

// OHIF import utility
const scrollToIndex = cornerstoneTools.import('util/scrollToIndex');
const { bucketByOrientation, getStackData } = cornerstoneUtils;
const { setActiveTool } = FlywheelCommonRedux.actions;
class SeriesLink {
  synchronizers = [];
  cornerstoneEvent = null;
  syncFunction = null;

  get sourceElements() {
    return this.synchronizers.reduce((elements, synchronizer) => {
      return elements.concat(synchronizer.getSourceElements());
    }, []);
  }

  get isEnabled() {
    const enabledElements = FlywheelCommonUtils.cornerstoneUtils.getEnabledElements();
    return this.sourceElements.some(element => enabledElements.has(element));
  }

  destroy() {
    for (const synchronizer of this.synchronizers) {
      synchronizer.destroy();
    }
    this.synchronizers = [];
  }

  toggle(toolType = '') {
    const enabled = this.isEnabled;
    this.destroy();
    if (enabled) {
      return false;
    }
    const enabledElements = FlywheelCommonUtils.cornerstoneUtils.getEnabledElements();
    const bucketedElements = bucketByOrientation([...enabledElements]);
    for (const bucket of bucketedElements) {
      const syncFunction =
        bucket?.syncFunctions?.[toolType] || this.syncFunction;
      const synchronizer = new cornerstoneTools.Synchronizer(
        this.cornerstoneEvent,
        syncFunction
      );
      for (const element of bucket?.element) {
        synchronizer.add(element);
      }
      this.synchronizers.push(synchronizer);
    }
    return true;
  }
}

class ScrollLink extends SeriesLink {
  cornerstoneEvent = 'cornerstonenewimage';
  syncFunction = cornerstoneTools.stackImagePositionSynchronizer;

  toggle() {
    store.dispatch(setActiveTool('StackScroll'));
    if (!super.toggle('scroll')) {
      return false;
    }
    for (const synchronizer of this.synchronizers) {
      const syncElements = synchronizer.getSourceElements();
      const stackData = getStackData(syncElements[0]);
      const currentPos = stackData.currentImageIdIndex || 0;
      for (const element of syncElements) {
        try {
          scrollToIndex(element, currentPos);
        } catch (error) {
          // out of range
          const stack = getStackData(element);
          scrollToIndex(element, stack.imageIds.length - 1);
        }
      }
    }
  }
}

class WwwcLink extends SeriesLink {
  cornerstoneEvent = 'cornerstoneimagerendered';
  syncFunction = cornerstoneTools.wwwcSynchronizer;

  toggle() {
    store.dispatch(setActiveTool('Wwwc'));
    if (!super.toggle()) {
      return false;
    }
  }
}

class ZoomLink extends SeriesLink {
  cornerstoneEvent = 'cornerstoneimagerendered';
  syncFunction = zoomSynchronizer;

  toggle() {
    store.dispatch(setActiveTool('Zoom'));
    if (!super.toggle()) {
      return false;
    }
  }
}

class OrientLink extends SeriesLink {
  cornerstoneEvent = 'cornerstoneimagerendered';
  syncFunction = orientSynchronizer;

  toggle() {
    store.dispatch(setActiveTool('Rotate'));
    if (!super.toggle()) {
      return false;
    }
  }
}
const scrollLink = new ScrollLink();
const wwwcLink = new WwwcLink();
const zoomLink = new ZoomLink();
const orientLink = new OrientLink();
const tools = { scrollLink, wwwcLink, zoomLink, orientLink };

export default { tools };
