import cornerstoneTools from 'cornerstone-tools';
import { cornerstoneUtils } from '../utils';
import Reactive from '../reactive';
import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';
import store from '../../../../platform/viewer/src/store';
import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';

const { getStackData } = cornerstoneUtils;
const { setActiveTool } = FlywheelCommonRedux.actions;

let synchronizer = null;

function isEnabled() {
  const enabledElements = FlywheelCommonUtils.cornerstoneUtils.getEnabledElements();
  return (
    synchronizer &&
    synchronizer
      .getSourceElements()
      .some(element => enabledElements.has(element))
  );
}
const toggleCrosshairs = () => {
  const enabled = isEnabled();
  if (synchronizer) {
    synchronizer.destroy();
    synchronizer = null;
  }

  // call to unsubscribe for viewport changed
  Reactive.crosshairs.unsubscribers.unsubscribeViewportChanged();

  cornerstoneTools.setToolDisabled('ReferenceLines');

  synchronizer = new cornerstoneTools.Synchronizer(
    'cornerstonenewimage',
    cornerstoneTools.updateImageSynchronizer
  );

  const enabledElements = FlywheelCommonUtils.cornerstoneUtils.getEnabledElements();
  for (const element of enabledElements) {
    synchronizer.add(element);
    const stackData = getStackData(element);
    cornerstoneTools.addStackStateManager(element, ['stack', 'Crosshairs']);
    cornerstoneTools.addToolState(element, 'stack', stackData);
  }
  cornerstoneTools.addTool(cornerstoneTools.ReferenceLinesTool);
  const tool = cornerstoneTools.CrosshairsTool;

  cornerstoneTools.addTool(tool);
  cornerstoneTools.setToolActive('Crosshairs', {
    mouseButtonMask: 1,
    synchronizationContext: synchronizer,
  });
  cornerstoneTools.setToolEnabled('ReferenceLines', {
    synchronizationContext: synchronizer,
  });

  Reactive.crosshairs.subscribers.subscribeViewportChanged(
    crosshairsTool,
    Reactive.viewport.observables.activeViewportIndexObservable$
  );

  store.dispatch(setActiveTool('Crosshairs'));
};

const crosshairsTool = { toggleCrosshairs, isEnabled };
Object.defineProperty(crosshairsTool, 'synchronizer', {
  get: () => {
    return synchronizer;
  },
  configurable: false,
  enumerable: false,
});

export default crosshairsTool;
