import cornerstoneTools from 'cornerstone-tools';
import shouldPreventScroll from '../utils/shouldPreventScroll';

const setScrollValidator = cornerstoneTools.import('util/scrollValidator');

export default function() {
  setScrollValidator(shouldPreventScroll);
}
