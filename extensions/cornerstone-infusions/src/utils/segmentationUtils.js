import cornerstoneTools from 'cornerstone-tools';
import {
  Redux as FlywheelCommonRedux,
  Utils as FlywheelCommonUtils,
} from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';
import { utils } from '@ohif/core';
import cloneDeep from 'lodash.clonedeep';

const {
  setActiveTool,
  setSmartCTRange,
  setSegmentationData,
} = FlywheelCommonRedux.actions;
const { segmentationTools } = FlywheelCommonRedux.selectors;

const { setters, getters } = cornerstoneTools.getModule('segmentation');
const { studyMetadataManager } = utils;

const getSelectedSegmentationData = selectedSegmentData => {
  const state = store.getState();
  const { dataProtocol } = getActiveViewport();
  const activeSegmentationDisplaySets = getActiveSegmentationDisplaySets(
    selectedSegmentData
  );
  const activeLabelmapIndices = [];
  const elements = [];
  const firstImageIds = [];
  let activeLabelmapIndex;

  if (dataProtocol === 'nifti') {
    const viewportSpecificData = store.getState().viewports
      .viewportSpecificData;

    activeSegmentationDisplaySets.forEach(displaySet => {
      if (displaySet?.labelmapIndexes?.[0]) {
        const originalSeriesUID = getOriginalSeriesUID(
          displaySet.SeriesInstanceUID
        );
        const viewportIndex = Object.keys(viewportSpecificData).findIndex(
          key =>
            viewportSpecificData[key].SeriesInstanceUID === originalSeriesUID
        );
        if (viewportIndex > -1) {
          const element = FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
            viewportIndex
          );
          const studyMetadata = studyMetadataManager.get(
            viewportSpecificData[viewportIndex].StudyInstanceUID
          );
          firstImageIds.push(
            studyMetadata.getFirstImageId(
              viewportSpecificData[viewportIndex].displaySetInstanceUID
            )
          );
          elements.push(element);
          activeLabelmapIndices.push(getters.activeLabelmapIndex(element));
          activeLabelmapIndex = getters.activeLabelmapIndex(element);
        } else {
          const studyMetadata = studyMetadataManager.get(
            displaySet.StudyInstanceUID
          );
          const referenceDisplaySet = (
            studyMetadata.getDisplaySets() || []
          ).find(ds => ds.SeriesInstanceUID === originalSeriesUID);
          const imageIds = referenceDisplaySet.images.map(
            image => image._imageId
          );
          const firstImageId = imageIds[0];
          firstImageIds.push(firstImageId);
          const activeLabelmapIndex = getters.activeLabelmapIndexOffline(
            imageIds
          );
          activeLabelmapIndices.push(activeLabelmapIndex);
        }
      }
    });
  } else {
    const element = FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
      state.viewports.activeViewportIndex
    );
    let activeLabelmap = {};
    activeSegmentationDisplaySets.forEach(activeSegmentationDisplaySet => {
      if (activeSegmentationDisplaySet?.labelmapIndexes?.[0]) {
        const { labelmaps3D } = getters.labelmaps3D(element);
        if (labelmaps3D) {
          activeLabelmap = getters.labelmap3D(
            element,
            activeSegmentationDisplaySet.labelmapIndexes[0]
          );
        }
      }
    });
    activeLabelmapIndex = getters.activeLabelmapIndex(element);
    firstImageIds.push(getFirstImageId());
    activeLabelmapIndices.push(activeLabelmapIndex);
    elements.push(element);
  }
  return {
    activeSegmentationDisplaySets,
    activeLabelmapIndices,
    elements,
    firstImageIds,
    activeLabelmapIndex,
  };
};

const getActiveLabelmapIndex = () => {
  const state = store.getState();
  const element = FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
    state.viewports.activeViewportIndex
  );
  const activeLabelmapIndex = getters.activeLabelmapIndex(element);

  return activeLabelmapIndex;
};

const editSegmentationMask = selectedSegmentData => {
  const {
    activeLabelmapIndex,
    elements,
    activeSegmentationDisplaySets,
  } = getSelectedSegmentationData(selectedSegmentData);
  activeSegmentationDisplaySets.forEach((displaySet, index) => {
    if (!_.isEmpty(displaySet)) {
      applyEditingToDisplaysetAndUpdateColor(
        activeLabelmapIndex,
        displaySet,
        elements[index]
      );
    }
  });
};

const applyEditingToDisplaysetAndUpdateColor = (
  activeLabelmapIndex,
  activeSegmentationDisplaySet,
  element
) => {
  const { dataProtocol } = getActiveViewport();
  if (activeLabelmapIndex === 0) {
    if (dataProtocol === 'nifti') {
      const originalSeriesUID = getOriginalSeriesUID(
        activeSegmentationDisplaySet.SeriesInstanceUID
      );
      const studyMetadata = studyMetadataManager.get(
        activeSegmentationDisplaySet.StudyInstanceUID
      );
      const referenceDisplaySet = (studyMetadata.getDisplaySets() || []).find(
        ds => ds.SeriesInstanceUID === originalSeriesUID
      );
      const imageIds = referenceDisplaySet.images.map(img => img._imageId);
      setters.activeLabelmapIndexOffline(
        imageIds,
        activeSegmentationDisplaySet.labelmapIndexes?.[0]
      );
      const activeSegmentIndex = getters.activeSegmentIndexOffline(
        imageIds[0],
        activeSegmentationDisplaySet.labelmapIndexes?.[0]
      );
      const smartCTRangeList = FlywheelCommonUtils.getSmartCTRangeList();
      const selectedSegmentationColor =
        smartCTRangeList?.[activeSegmentIndex - 1];

      if (selectedSegmentationColor) {
        store.dispatch(setSmartCTRange(selectedSegmentationColor));
      }
    } else {
      setters.activeLabelmapIndex(
        element,
        activeSegmentationDisplaySet.labelmapIndexes?.[0]
      );
      const activeSegmentIndex = getters.activeSegmentIndex(
        element,
        activeSegmentationDisplaySet.labelmapIndexes?.[0]
      );
      const smartCTRangeList = FlywheelCommonUtils.getSmartCTRangeList();
      const selectedSegmentationColor =
        smartCTRangeList?.[activeSegmentIndex - 1];

      if (selectedSegmentationColor) {
        store.dispatch(setSmartCTRange(selectedSegmentationColor));
      }
    }
  }
};

const exitEditSegmentationMode = selectedSegmentData => {
  if (!selectedSegmentData) {
    return;
  }
  const { dataProtocol } = getActiveViewport();
  const {
    activeLabelmapIndex,
    elements,
    activeSegmentationDisplaySets,
  } = getSelectedSegmentationData(selectedSegmentData);

  activeSegmentationDisplaySets.forEach((displaySet, index) => {
    if (displaySet?.labelmapIndexes[0]) {
      if (activeLabelmapIndex === displaySet.labelmapIndexes[0]) {
        if (dataProtocol === 'nifti') {
          const originalSeriesUID = getOriginalSeriesUID(
            displaySet.SeriesInstanceUID
          );
          const studyMetadata = studyMetadataManager.get(
            displaySet.StudyInstanceUID
          );
          const referenceDisplaySet = (
            studyMetadata.getDisplaySets() || []
          ).find(ds => ds.SeriesInstanceUID === originalSeriesUID);
          const imageIds = referenceDisplaySet.images.map(img => img._imageId);
          setters.activeLabelmapIndexOffline(imageIds, 0);
        } else {
          setters.activeLabelmapIndex(elements[index], 0);
        }
      }
    }
  });
};

const getDerivedDisplaySetArrayByReferencedSeries = (
  studyMetadata,
  SeriesInstanceUID,
  modality
) => {
  const derivedDisplaySets = studyMetadata.getDerivedDatasets({
    referencedSeriesInstanceUID: SeriesInstanceUID,
    Modality: modality,
  });
  return derivedDisplaySets;
};

const getActiveSegmentationDisplaySets = selectedSegmentData => {
  const activeViewport = getActiveViewport();
  const studyInstanceUid = activeViewport.StudyInstanceUID;
  const studyMetadata = studyMetadataManager.get(studyInstanceUid);
  const segDisplaySets = [];

  if (activeViewport.dataProtocol === 'nifti') {
    const displaySets = studyMetadata.getDisplaySets();
    displaySets.forEach(displaySet => {
      const { SeriesInstanceUID } = displaySet;
      const derivedDisplaySets = getDerivedDisplaySetArrayByReferencedSeries(
        studyMetadata,
        SeriesInstanceUID,
        selectedSegmentData.modality
      );

      if (!_.isEmpty(derivedDisplaySets)) {
        const segDisplayset = getDerivedDisplaySet(
          derivedDisplaySets,
          selectedSegmentData.displaySetInstanceUIDs
        );
        segDisplaySets.push(segDisplayset);
      }
    });
  } else {
    segDisplaySets.push(getActiveSegmentationDisplaySet(selectedSegmentData));
  }

  return segDisplaySets;
};

const getActiveSegmentationDisplaySet = selectedSegmentData => {
  const activeViewport = getActiveViewport();
  const studyInstanceUid = activeViewport.StudyInstanceUID;
  const studyMetadata = studyMetadataManager.get(studyInstanceUid);
  const derivedDisplaySets = studyMetadata.getDerivedDatasets({
    referencedSeriesInstanceUID: selectedSegmentData.referenceUID,
    Modality: selectedSegmentData.modality,
  });
  const segDisplayset = getDerivedDisplaySet(
    derivedDisplaySets,
    selectedSegmentData.displaySetInstanceUIDs
  );

  return segDisplayset;
};

const isSelectedSegmentMaskInEditing = selectedSegmentData => {
  let isSelectedMaskInEditing;
  const {
    activeLabelmapIndex,
    activeSegmentationDisplaySets,
    elements,
  } = getSelectedSegmentationData(selectedSegmentData);
  activeSegmentationDisplaySets.forEach((displaySet, index) => {
    if (!_.isEmpty(displaySet) && elements[index])
      isSelectedMaskInEditing =
        activeLabelmapIndex > 0 &&
        displaySet.labelmapIndexes[0] === activeLabelmapIndex;
  });
  return isSelectedMaskInEditing;
};

const getActiveViewport = () => {
  const state = store.getState();
  let viewport = null;

  if (state.viewports) {
    const { viewportSpecificData, activeViewportIndex } = state.viewports;
    if (viewportSpecificData[activeViewportIndex]) {
      viewport = viewportSpecificData[activeViewportIndex];
    }
  }

  return viewport;
};

const getDerivedDisplaySet = (derivedDisplaySets, displaySetInstanceUIDs) => {
  let displaySet = {};
  if (!derivedDisplaySets.length) {
    return displaySet;
  }
  derivedDisplaySets.every(segDisplaySet => {
    if (
      !segDisplaySet.isDeleted &&
      displaySetInstanceUIDs?.includes(segDisplaySet.displaySetInstanceUID)
    ) {
      displaySet = segDisplaySet;
      return false;
    }
    return true;
  });
  return displaySet;
};

const setSegmentToolActive = () => {
  const state = store.getState();
  const activeTool = state.infusions?.activeTool;
  const isAnySegmentationToolActive = segmentationTools.includes(activeTool);

  if (!isAnySegmentationToolActive) {
    store.dispatch(setActiveTool(segmentationTools[0]));
  }
};

const getFirstImageId = () => {
  const { StudyInstanceUID, displaySetInstanceUID } = getActiveViewport();
  const studyMetadata = studyMetadataManager.get(StudyInstanceUID);
  return studyMetadata.getFirstImageId(displaySetInstanceUID);
};

function getActiveReferenceUID() {
  const state = store.getState();
  const currentVolumetricImage =
    state.flywheel?.currentNiftis[0] || state.flywheel?.currentMetaImage;
  let referenceUID = null;
  if (state.viewports) {
    const { viewportSpecificData, activeViewportIndex } = state.viewports;
    if (viewportSpecificData[activeViewportIndex]) {
      referenceUID = currentVolumetricImage
        ? viewportSpecificData[activeViewportIndex].StudyInstanceUID
        : viewportSpecificData[activeViewportIndex].SeriesInstanceUID;
    }
  }
  return referenceUID;
}

const getOriginalSeriesUID = derivedSeriesInstanceUID => {
  if (derivedSeriesInstanceUID.indexOf('-derived') >= 0) {
    return derivedSeriesInstanceUID.split('-derived')[0];
  }
  return derivedSeriesInstanceUID;
};

const getSegmentationDataFromStore = referenceUID => {
  const state = store.getState();
  const segmentationData = state.segmentation.segmentationData?.[referenceUID];
  return segmentationData;
};

const exitSegmentationMaskFromEditMode = displaySet => {
  const referenceUID =
    displaySet.dataProtocol === 'nifti'
      ? displaySet.StudyInstanceUID
      : displaySet.SeriesInstanceUID;
  const segmentationData = getSegmentationDataFromStore(referenceUID);
  if (segmentationData) {
    const { index, activelyEditingMask } = getActivelyEditingMaskDetails(
      segmentationData
    );
    if (index > -1 && activelyEditingMask !== undefined) {
      exitEditSegmentationMode(activelyEditingMask);
      updateSegmentationData(
        index,
        activelyEditingMask,
        segmentationData,
        referenceUID
      );
    }
  }
};

const getActivelyEditingMaskDetails = segmentationData => {
  const index = segmentationData?.findIndex(data => data.isEditing);
  const activelyEditingMask = cloneDeep(segmentationData[index]);
  return { index, activelyEditingMask };
};

const updateSegmentationData = (
  index,
  activelyEditingMask,
  segmentationData,
  referenceUID
) => {
  activelyEditingMask.isEditing = false;
  let newMaskArray = [...segmentationData];
  newMaskArray[index] = activelyEditingMask;
  store.dispatch(setSegmentationData([...newMaskArray], referenceUID));
};

export {
  editSegmentationMask,
  exitEditSegmentationMode,
  getActiveLabelmapIndex,
  isSelectedSegmentMaskInEditing,
  getSelectedSegmentationData,
  setSegmentToolActive,
  getActiveReferenceUID,
  getActiveSegmentationDisplaySet,
  getActiveViewport,
  exitSegmentationMaskFromEditMode,
};
