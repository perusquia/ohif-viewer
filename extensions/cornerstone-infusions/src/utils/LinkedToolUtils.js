import { LinkSeriesTool } from '../tools';

export function getLinkButtonStatus(button) {
  let enabled = false;
  if (button === 'Orient') {
    enabled = LinkSeriesTool.tools.orientLink.isEnabled;
  }
  return enabled;
}
