/**
 * Synchronize the target zoom and pan to match the source
 * @export
 * @public
 * @method
 * @name zoomSynchronizer
 *
 * @param {Object} synchronizer - The Synchronizer instance that attaches this
 * handler to an event
 * @param {HTMLElement} sourceElement - The source element for the zoom and pan values
 * @param {HTMLElement} targetElement - The target element
 * @returns {void}
 */
export default function(synchronizer, sourceElement, targetElement) {
  // Ignore the case where the source and target are the same enabled element
  if (targetElement === sourceElement) {
    return;
  }

  // Get the source and target viewports
  const sourceViewport = cornerstone.getViewport(sourceElement);
  const targetViewport = cornerstone.getViewport(targetElement);

  // Do nothing if the scale and translation are the same
  if (targetViewport.scale === sourceViewport.scale) {
    return;
  }

  // Scale is different, sync them
  targetViewport.scale = sourceViewport.scale;
  synchronizer.setViewport(targetElement, targetViewport);
}
