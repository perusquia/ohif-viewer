import { redux } from '@ohif/core';

const { setChannelId: storeAction } = redux.actions;

export default function setChannelId(channelId, store) {
  const action = storeAction({ channelId });
  store.dispatch(action);
}
