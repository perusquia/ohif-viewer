import csTools from 'cornerstone-tools';
import {
  Utils as FlywheelCommonUtils,
  Redux as FlywheelCommonRedux,
} from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';

const { state } = csTools.store;
const { measurementTools } = FlywheelCommonRedux.selectors;

export default function() {
  const element = FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
    store.getState().viewports.activeViewportIndex
  );
  const activeTools = state.tools.filter(
    tool =>
      tool.element === element &&
      (tool.mode === 'active' || tool.mode === 'passive') &&
      measurementTools.includes(tool.name)
  );

  if (activeTools.length) {
    const activeTool = store.getState().infusions?.activeTool;
    const currentActiveTool = activeTools.find(
      tool => tool.name === activeTool
    );

    if (currentActiveTool?.mode === 'active' && currentActiveTool?._drawing) {
      return true;
    }
  }
  return false;
}
