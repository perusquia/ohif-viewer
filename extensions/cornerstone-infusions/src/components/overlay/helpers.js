import { parse, format } from 'date-fns';
/**
 * Compare equality of two string arrays.
 *
 * @param {string[]} [arr1] - String array #1
 * @param {string[]} [arr2] - String array #2
 * @returns {boolean}
 */
export function areStringArraysEqual(arr1, arr2) {
  if (arr1 === arr2) return true; // Identity
  if (!arr1 || !arr2) return false; // One is undef/null
  if (arr1.length !== arr2.length) return false; // Diff length

  for (let i = 0; i < arr1.length; i++) {
    if (arr1[i] !== arr2[i]) return false;
  }

  return true;
}

export function formatDA(date, strFormat = 'MMM d, yyyy') {
  if (!date) {
    return '';
  }

  // Goal: 'Apr 5, 1999'
  try {
    const parsedDateTime = parse(date, 'yyyyMMdd', new Date());
    const formattedDateTime = format(parsedDateTime, strFormat);
    return formattedDateTime;
  } catch (err) {
    return '';
  }
}

export function formatNumberPrecision(number, precision) {
  if (number !== null) {
    return parseFloat(number).toFixed(precision);
  }
  return '';
}

/**
 * Formats a patient name for display purposes
 */
export function formatPN(name) {
  if (!name) {
    return '';
  }

  // Convert the first ^ to a ', '. String.replace() only affects
  // the first appearance of the character.
  const commaBetweenFirstAndLast = name.replace('^', ', ');

  // Replace any remaining '^' characters with spaces
  const cleaned = commaBetweenFirstAndLast.replace(/\^/g, ' ');

  // Trim any extraneous whitespace
  return cleaned.trim();
}

export function formatTM(time, strFormat = 'HH:mm:ss') {
  if (!time) {
    return '';
  }

  // DICOM Time is stored as HHmmss.SSS, where:
  //      HH 24 hour time:
  //      m mm    0..59   Minutes
  //      s ss    0..59   Seconds
  //      S SS SSS    0..999  Fractional seconds
  //
  // Goal: '24:12:12'
  try {
    const inputFormat = time.length >= 10 ? 'HHmmss.SSS' : 'HHmmss';
    const strTime = time.toString().substring(0, inputFormat.length);
    const parsedDateTime = parse(strTime, inputFormat, new Date(0));
    const formattedDateTime = format(parsedDateTime, strFormat);
    return formattedDateTime;
  } catch (err) {
    return '';
  }
}

export function isValidNumber(value) {
  return typeof value === 'number' && !isNaN(value);
}
