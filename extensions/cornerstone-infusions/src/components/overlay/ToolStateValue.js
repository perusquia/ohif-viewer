import cornerstone from 'cornerstone-core';
import cornerstoneTools from 'cornerstone-tools';
import isNil from 'lodash/isNil';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { evaluateTemplate } from './template';

class ToolStateValue extends Component {
  static propTypes = {
    attribute: PropTypes.oneOf([
      'imageX',
      'imageY',
      'pixelValue',
      'modalityPixelValue',
      'rotation',
      'stretch',
    ]).isRequired,
    element: PropTypes.object,
    template: PropTypes.string,
  };

  state = {
    value: 0,
  };

  constructor(props) {
    super(props);
    this.bindToElement();
  }

  componentDidUpdate(prevProps) {
    if (this.props.element !== prevProps.element) {
      this.bindToElement();
    }
  }

  componentWillUnmount() {
    this.unbindFromElement();
  }

  bindToElement() {
    this.unbindFromElement();
    if (this.props.element) {
      this.props.element.addEventListener(
        cornerstoneTools.EVENTS.MOUSE_MOVE,
        this.onMouseMove
      );
      this.props.element.addEventListener(
        cornerstone.EVENTS.IMAGE_RENDERED,
        this.onImageRendered
      );
    }
  }

  unbindFromElement() {
    if (this.props.element) {
      this.props.element.removeEventListener(
        cornerstoneTools.EVENTS.MOUSE_MOVE,
        this.onMouseMove
      );
      this.props.element.removeEventListener(
        cornerstone.EVENTS.IMAGE_RENDERED,
        this.onImageRendered
      );
    }
  }

  onMouseMove = event => {
    const { attribute } = this.props;
    const { image } = event.detail.currentPoints;
    let value;
    switch (attribute) {
      case 'pixelValue':
      case 'modalityPixelValue':
        value = cornerstone.getStoredPixels(
          this.props.element,
          image.x,
          image.y,
          1,
          1
        )[0];
        if (attribute === 'modalityPixelValue') {
          // https://github.com/cornerstonejs/cornerstoneTools/issues/278
          const { intercept, slope } = event.detail.image;
          value = value * slope + intercept;
        }
        break;
      case 'imageX':
      case 'imageY':
        value = image[attribute === 'imageY' ? 'y' : 'x'].toFixed(0);
        break;
    }
    if (!isNil(value)) {
      this.setState({ value: value || 0 });
    }
  };

  onImageRendered = event => {
    const { attribute, template } = this.props;
    const { viewport } = event.detail;
    let value;

    switch (attribute) {
      case 'rotation':
        value = viewport.rotation || 0;
        break;
      case 'stretch':
        const stretchX = viewport.stretch?.x;
        const stretchY = viewport.stretch?.y;
        const isX_Axis = stretchX && stretchX !== 1;
        const isY_Axis = stretchY && stretchY !== 1;
        const data = {
          stretchAxis: isX_Axis ? 'X' : isY_Axis ? 'Y' : undefined,
          stretchValue: isX_Axis ? stretchX : isY_Axis ? stretchY : undefined,
        };
        const isValid = data.stretchAxis && data.stretchValue;

        value = isValid ? evaluateTemplate(template, data) : '';
        break;
    }
    if (!isNil(value)) {
      this.setState({ value });
    }
  };

  render() {
    const stateValue = this.state.value;
    const isInvalidForStretch =
      this.props.attribute === 'stretch' && stateValue === 0;
    return isInvalidForStretch ? '' : stateValue;
  }
}

export default ToolStateValue;
