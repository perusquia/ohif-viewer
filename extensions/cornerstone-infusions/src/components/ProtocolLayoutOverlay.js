import { SelectTree } from '@ohif/ui';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { Redux as FlywheelRedux } from '@flywheel/extension-flywheel';

const { selectProjectConfig } = FlywheelRedux.selectors;
class ProtocolLayoutOverlay extends Component {
  static propTypes = {
    projectConfig: PropTypes.shape({
      layouts: PropTypes.arrayOf(
        PropTypes.shape({
          name: PropTypes.string,
          viewports: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string))
            .isRequired,
        })
      ),
    }),
    selectCallback: PropTypes.func,
  };

  selectTreeSelectCalback = (event, itemSelected) => {
    if (this.props.contentProps.selectCallback) {
      let selected = this.props.projectConfig.layouts.find(
        layout => layout.name === itemSelected.label
      );
      this.props.contentProps.selectCallback(selected);
    }
  };

  render() {
    const treeLayouts = this.props.projectConfig.layouts.map(layout => {
      return { label: layout.name, value: layout.name };
    });
    return (
      <SelectTree
        searchEnabled={false}
        items={treeLayouts}
        columns={1}
        onSelected={this.selectTreeSelectCalback}
        selectTreeFirstTitle="Select a layout"
      />
    );
  }
}

const mapStateToProps = state => ({
  projectConfig: selectProjectConfig(state),
});

export default connect(mapStateToProps)(ProtocolLayoutOverlay);
