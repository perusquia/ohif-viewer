import React from 'react';

export default function withCommandsManager(
  Component,
  commandsManager = {},
  servicesManager = {}
) {
  return class WithCommandsManager extends React.Component {
    render() {
      return (
        <Component
          {...this.props}
          commandsManager={commandsManager}
          servicesManager={servicesManager}
        />
      );
    }
  };
}
