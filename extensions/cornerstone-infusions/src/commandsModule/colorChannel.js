import cornerstone from 'cornerstone-core';
import setChannelId from '../utils/setChannelId';
import OHIF from '@ohif/core';
import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';
import { DEFAULT_COLOR_ID } from '../components/toolbar/colorMap';

const { utils } = OHIF;
const { CornerstoneColorChannelService } = utils;

const _updateViewportsColorChannel = (
  viewportIndex,
  nextColorChannel,
  nextColorChannelName
) => {
  const element = FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
    viewportIndex
  );

  CornerstoneColorChannelService.onChangeColorChannel(
    element,
    nextColorChannel,
    nextColorChannelName
  );
};

const commandsModule = ({ appConfig, commandsManager }) => {
  const { store } = appConfig;
  const actions = {
    changeColorChannel: ({ viewports: viewportsStore, channelId, reset }) => {
      const channel = cornerstone.colors.getColorChannelList();
      if (reset) {
        channelId = channel[0].id;
      }
      if (!channelId) {
        const viewports = store.getState().viewports || {};
        const currentChannel = viewports.channelId;
        const channelIndex = channel.findIndex(x => x.id == currentChannel);
        const nextChannelIndex =
          channelIndex + 1 < channel.length ? channelIndex + 1 : 0;
        if (channel[nextChannelIndex]) {
          channelId = channel[nextChannelIndex].id;
        }
        if (store.getState().viewports.colormapId !== DEFAULT_COLOR_ID) {
          commandsManager.runCommand('resetColormap', {
            isReset: true,
          });
        }
      }
      const { viewportSpecificData = {} } = viewportsStore || {};
      const {
        colorChannelName,
        colorChannel,
      } = CornerstoneColorChannelService.getNextColorChannel(channelId);
      // iterate viewports and update each of them
      for (let keyIndex in viewportSpecificData) {
        if (
          viewportSpecificData[keyIndex].plugin === 'cornerstone' ||
          viewportSpecificData[keyIndex].plugin === 'cornerstone::nifti' ||
          viewportSpecificData[keyIndex].plugin === 'cornerstone::metaimage'
        ) {
          _updateViewportsColorChannel(
            keyIndex,
            colorChannel,
            colorChannelName
          );
        }
      }
      // set to store
      setChannelId(channelId, store);
    },
  };
  const definitions = {
    changeColorChannel: {
      commandFn: actions.changeColorChannel,
      storeContexts: ['viewports'],
      options: {},
    },
    resetColorChannel: {
      commandFn: actions.changeColorChannel,
      storeContexts: ['viewports'],
      options: { reset: true },
    },
  };
  return {
    actions,
    definitions,
  };
};
export default commandsModule;
