import cornerstoneTools from 'cornerstone-tools';
import { Components as FlywheelComponents } from '@flywheel/extension-flywheel';
import {
  Redux as FlywheelCommonRedux,
  Utils as FlywheelCommonUtils,
} from '@flywheel/extension-flywheel-common';

const { setActiveTool } = FlywheelCommonRedux.actions;
const { measurementTools, selectActiveTool } = FlywheelCommonRedux.selectors;

const scrollToIndex = cornerstoneTools.import('util/scrollToIndex');

let helpDialogId = null;
const { HelpDialog } = FlywheelComponents;

const commandsModule = ({ appConfig, servicesManager, hotkeysManager }) => {
  const { store } = appConfig;
  const actions = {
    firstImage: ({ viewports }) => {
      scrollToIndex(
        FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
          viewports.activeViewportIndex
        ),
        0
      );
    },
    lastImage: ({ viewports }) => {
      const viewport =
        viewports.viewportSpecificData[viewports.activeViewportIndex] || {};
      scrollToIndex(
        FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
          viewports.activeViewportIndex
        ),
        viewport.numImageFrames - 1
      );
    },
    setActiveToolHotkey: ({ toolName }) => {
      if (!toolName) {
        console.warn('No toolname provided to setActiveToolHotkey command');
      }
      const viewports = store.getState().viewports;
      const activeViewportIndex = viewports.activeViewportIndex;
      const activeViewportPluginName =
        viewports.viewportSpecificData[activeViewportIndex]?.plugin;
      if (activeViewportPluginName === 'vtk') {
        switch (toolName) {
          case 'Rotate': {
            window.vtkActions.enableRotateTool();
            break;
          }
          case 'StackScroll': {
            window.vtkActions.enableStackScrollTool();
            break;
          }
          case 'Zoom': {
            window.vtkActions.enableZoomTool();
            break;
          }
          case 'Wwwc': {
            window.vtkActions.enableLevelTool();
            break;
          }
          case 'Crosshair': {
            window.vtkActions.enableCrosshairsTool(false);
            break;
          }
          case 'Pan': {
            window.vtkActions.enablePanTool();
            break;
          }
        }
      } else {
        store.dispatch(setActiveTool(toolName));
      }
    },
    toggleHelp: state => {
      const { UIDialogService } = servicesManager.services;

      if (helpDialogId) {
        UIDialogService.dismiss({ id: helpDialogId });
        helpDialogId = null;
        return;
      }

      helpDialogId = UIDialogService.create({
        content: HelpDialog,
        defaultPosition: { x: 20, y: 120 },
        showOverlay: false,
        contentProps: {
          onClose: () => {
            UIDialogService.dismiss({ id: helpDialogId });
            helpDialogId = null;
          },
          onConfirm: () => {},
          hotkeysManager,
        },
      });
    },
    prevMeasureTool: state => {
      const activeTool = selectActiveTool(state);
      const index = measurementTools.findIndex(
        toolName => toolName === activeTool
      );
      let prevTool = measurementTools[0];
      if (index >= 0) {
        const mod = measurementTools.length;
        prevTool = measurementTools[(index - 1 + mod) % mod];
      }
      store.dispatch(setActiveTool(prevTool));
    },
    nextMeasureTool: state => {
      const activeTool = selectActiveTool(state);
      const index = measurementTools.findIndex(
        toolName => toolName === activeTool
      );
      let nextTool = measurementTools[0];
      if (index >= 0) {
        const mod = measurementTools.length;
        nextTool = measurementTools[(index + 1) % mod];
      }
      store.dispatch(setActiveTool(nextTool));
    },
  };

  const definitions = {
    firstImage: {
      commandFn: actions.firstImage,
      storeContexts: ['viewports'],
      options: {},
    },
    lastImage: {
      commandFn: actions.lastImage,
      storeContexts: ['viewports'],
      options: {},
    },
    setActiveToolHotkey: {
      commandFn: actions.setActiveToolHotkey,
      storeContexts: [],
      options: {},
    },
    toggleHelp: {
      commandFn: actions.toggleHelp,
      storeContexts: [],
      options: {},
    },
    prevMeasureTool: {
      commandFn: actions.prevMeasureTool,
      storeContexts: ['infusions'],
      options: {},
    },
    nextMeasureTool: {
      commandFn: actions.nextMeasureTool,
      storeContexts: ['infusions'],
      options: {},
    },
  };

  return {
    actions,
    definitions,
  };
};

export default commandsModule;
