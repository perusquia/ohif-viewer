import { Components as FlywheelComponents } from '@flywheel/extension-flywheel';

let labelChangeDialogId = null;
const { EditSegmentationLabelDialog } = FlywheelComponents;

const commandsModule = ({ servicesManager, hotkeysManager }) => {
  const actions = {
    toggleChangeSegmentationLabel: segmentationData => {
      const { UIDialogService } = servicesManager.services;
      const popupFromLeft = 175;
      const popupFromTop = 220;

      if (labelChangeDialogId) {
        return;
      }
      labelChangeDialogId = UIDialogService.create({
        content: EditSegmentationLabelDialog,
        defaultPosition: {
          x: window.innerWidth / 2 - popupFromLeft,
          y: window.innerHeight / 2 - popupFromTop,
        },
        showOverlay: false,
        contentProps: {
          onClose: () => {
            UIDialogService.dismiss({ id: labelChangeDialogId });
            labelChangeDialogId = null;
          },
          onConfirm: () => {},
          segmentationData,
          hotkeysManager,
        },
      });
    },
  };

  const definitions = {
    toggleChangeSegmentationLabel: {
      commandFn: actions.toggleChangeSegmentationLabel,
      storeContexts: [],
      options: {},
    },
  };

  return {
    actions,
    definitions,
  };
};

export default commandsModule;
