import { Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { log } from '@ohif/core';
import { cornerstoneUtils } from '../utils';

import {
  Redux as FlywheelCommonRedux,
  Utils as FlywheelCommonUtils,
} from '@flywheel/extension-flywheel-common';

const { getOrientation } = cornerstoneUtils;

let viewportElementsSubscription$ = Subscription.EMPTY;
let activeViewportChangedSubscription$ = Subscription.EMPTY;
// create a stream to apply crosshairs
const subscribeCrosshairs = (CrosshairsTool, viewportElementsObservable$) => {
  unsubscribeCrosshairs();
  // recreate synchronizer if viewports are changed
  viewportElementsSubscription$ = viewportElementsObservable$
    .pipe(debounceTime(1000))
    .subscribe(viewportElements => {
      if (!CrosshairsTool.synchronizer) {
        return;
      }
      const syncElements = CrosshairsTool.synchronizer.getSourceElements();
      if (
        viewportElements.length !== syncElements.length ||
        !syncElements.every(element => viewportElements.has(element))
      ) {
        CrosshairsTool.toggleCrosshairs(); // disable
        CrosshairsTool.toggleCrosshairs(); // enable
      }
    });
};

const unsubscribeCrosshairs = () => {
  if (viewportElementsSubscription$) {
    viewportElementsSubscription$.unsubscribe();
  }
};

const subscribeViewportChanged = (
  CrosshairsTool,
  activeViewportIndexObservable$
) => {
  activeViewportChangedSubscription$ = activeViewportIndexObservable$.subscribe(
    index => {
      if (!CrosshairsTool.isEnabled()) {
        return;
      }
      const activeElement = FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(
        index
      );
      const activeOrientation = getOrientation(activeElement) || index;
      for (const element of CrosshairsTool.synchronizer.getTargetElements()) {
        if (
          element === activeElement ||
          getOrientation(element) === activeOrientation
        ) {
          cornerstoneTools.setToolDisabledForElement(element, 'ReferenceLines');
        } else {
          cornerstoneTools.setToolEnabledForElement(element, 'ReferenceLines');
        }
      }
    }
  );
};

const unsubscribeViewportChanged = () => {
  if (activeViewportChangedSubscription$) {
    activeViewportChangedSubscription$.unsubscribe();
  }
};

/** Reactive object structure */
// default methods
const subscribe = subscribeCrosshairs;
const unsubscribe = () => {
  log.warn('Unsubscribe not implemented yet');
};

// other methods
const subscribers = {
  subscribeViewportChanged,
};

const unsubscribers = {
  unsubscribeViewportChanged,
};

// define package object
const reactive = { subscribers, unsubscribers, subscribe, unsubscribe };

export default reactive;
