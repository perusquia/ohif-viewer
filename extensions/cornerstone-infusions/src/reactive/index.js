import crosshairs from './crosshairs';
import linkSeries from './linkSeries';
import viewport from './viewport';

const reactive = {
  crosshairs,
  linkSeries,
  viewport,
};

export default reactive;
