import { Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import _ from 'lodash';
import store from '@ohif/viewer/src/store';
import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';

const { setActiveTool } = FlywheelCommonRedux.actions;

let viewportDataSubscription$ = Subscription.EMPTY;
// create a stream to apply crosshairs
const subscribeLinkSeries = (linkSeriesTools = {}, viewportDataObservable$) => {
  unsubscribeLinkSeries();
  // recreate synchronizers if viewports are changed
  viewportDataSubscription$ = viewportDataObservable$
    .pipe(debounceTime(1000))
    .subscribe(viewportData => {
      const activeTool = store.getState().infusions.activeTool;
      let linkingEnabled = false;
      const tools = _.values(linkSeriesTools);
      for (const link of tools) {
        if (link.isEnabled) {
          link.toggle(); // disable
          link.toggle(); // enable
          linkingEnabled = true;
        }
      }
      // Restore the active tool that existed before recreating synchronizers.
      if (
        linkingEnabled &&
        store.getState().infusions.activeTool !== activeTool
      ) {
        store.dispatch(setActiveTool(activeTool));
      }
    });
};

const unsubscribeLinkSeries = () => {
  if (viewportDataSubscription$) {
    viewportDataSubscription$.unsubscribe();
  }
};

/** Reactive object structure */

// default methods
const subscribe = subscribeLinkSeries;
const unsubscribe = unsubscribeLinkSeries;

// define package object
const reactive = { subscribe, unsubscribe };
Object.defineProperty(reactive, 'subscriptions', {
  get: () => {
    return {
      viewportDataSubscription$,
    };
  },
  configurable: false,
  enumerable: false,
});

export default reactive;
