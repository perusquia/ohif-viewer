import { distinctUntilChanged, map, shareReplay } from 'rxjs/operators';
import {
  Redux as FlywheelCommonRedux,
  Utils as FlywheelCommonUtils,
} from '@flywheel/extension-flywheel-common';

let viewportsObservable$;
let viewportElementsObservable$;
let activeViewportIndexObservable$;
let viewportDataObservable$;

function createObservables(storeObservable$) {
  if (!storeObservable$) {
    return;
  }

  viewportsObservable$ = storeObservable$.pipe(
    map(state => FlywheelCommonRedux.selectors.selectViewports(state)),
    distinctUntilChanged()
  );

  viewportElementsObservable$ = viewportsObservable$.pipe(
    map(
      viewports =>
        new Set(
          viewports.map((vp, index) =>
            FlywheelCommonUtils.cornerstoneUtils.getEnabledElement(index)
          )
        )
    )
  );

  activeViewportIndexObservable$ = storeObservable$.pipe(
    map(state =>
      FlywheelCommonRedux.selectors.selectActiveViewportIndex(state)
    ),
    distinctUntilChanged()
  );

  viewportDataObservable$ = storeObservable$.pipe(
    map(state =>
      FlywheelCommonRedux.selectors.selectViewportSpecificData(state)
    ),
    distinctUntilChanged()
  );

  return {
    viewportsObservable$,
    viewportElementsObservable$,
    activeViewportIndexObservable$,
    viewportDataObservable$,
  };
}

/** Reactive object structure */

// define package object
const reactive = { createObservables };
Object.defineProperty(reactive, 'observables', {
  get: () => {
    return {
      viewportsObservable$,
      viewportElementsObservable$,
      activeViewportIndexObservable$,
      viewportDataObservable$,
    };
  },
  configurable: false,
  enumerable: false,
});

export default reactive;
