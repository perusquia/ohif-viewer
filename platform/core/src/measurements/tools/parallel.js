const displayFunction = data => {
  return "";
};

export const parallel = {
  id: 'Parallel',
  name: 'Parallel',
  toolGroup: 'allTools',
  cornerstoneToolType: 'Length',
  options: {
    measurementTable: {
      displayFunction,
    },
    caseProgress: {
      include: true,
      evaluate: true,
    },
  },
};
