import { ToolsUtils as FlywheelCommonToolsUtils } from '@flywheel/extension-flywheel-common';
import getImagePropertyForImagePath from '../lib/getImagePropertyForImagePath';

const displayFunction = data => {
  if (data.shortestDiameter) {
    // TODO: Make this check criteria again to see if we should display shortest x longest

    const pixelSpacing = getImagePropertyForImagePath(
      data.imagePath,
      'PixelSpacing'
    );
    const unit = FlywheelCommonToolsUtils.unitUtils.getLengthUnit(pixelSpacing);

    return `${data.longestDiameter} ${unit} x ${data.shortestDiameter} ${unit}`;
  }

  return data.longestDiameter;
};

export const bidirectional = {
  id: 'Bidirectional',
  name: 'Target',
  toolGroup: 'allTools',
  cornerstoneToolType: 'Bidirectional',
  options: {
    measurementTable: {
      displayFunction,
    },
    caseProgress: {
      include: true,
      evaluate: true,
    },
  },
};
