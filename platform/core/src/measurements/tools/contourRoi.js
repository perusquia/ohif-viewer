const displayFunction = data => {
  return '';
};

export const contourRoi = {
  id: 'ContourRoi',
  name: 'Contour',
  toolGroup: 'allTools',
  cornerstoneToolType: 'FreehandMouse',
  options: {
    measurementTable: {
      displayFunction,
    },
    caseProgress: {
      include: true,
      evaluate: true,
    },
  },
};
