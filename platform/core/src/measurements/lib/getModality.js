export default function (state) {
  const { viewportSpecificData, activeViewportIndex } = state.viewports;
  return viewportSpecificData && viewportSpecificData[activeViewportIndex]?.Modality;
}
