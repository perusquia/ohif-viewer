import cornerstone from 'cornerstone-core';
import getImageIdForImagePath from './getImageIdForImagePath';

export default function(imagePath, property) {
  if (!property || !imagePath) {
    return;
  }
  const imageId = getImageIdForImagePath(imagePath);
  const instance = cornerstone.metaData.get('instance', imageId);

  return (instance || {})[property];
}
