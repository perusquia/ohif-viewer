import cornerstone from 'cornerstone-core';

/**
 * Image attributes collection
 * @typedef {Object} ImageAttributes
 * @property {string | undefined} PatientID PatientID value for given instance.
 * @property {string} StudyInstanceUID StudyInstanceUID value for given instance.
 * @property {string} SeriesInstanceUID SeriesInstanceUID value for given instance.
 * @property {string} SOPInstanceUID  SeriesInstanceUID value for given instance.
 * @property {string} frameIndex  Frame index value for given instance.
 * @property {string} imagePath Union of other properties separated by $$$.
 */

/**
 * Get a collection of image study attributes based on given element
 * @param {*} element
 * @return {(ImageAttributes | {})} Image attributes or empty object in case not existing instance
 */
export default function (element, imageId) {
  if (!imageId) {
  // Get the Cornerstone imageId
  const enabledElement = cornerstone.getEnabledElement(element);
    imageId = enabledElement?.image?.imageId;
  }

  const instance = cornerstone.metaData.get('instance', imageId);

  if (!instance) {
    return {}
  }
  // Get StudyInstanceUID & PatientID
  const {
    StudyInstanceUID,
    PatientID,
    SeriesInstanceUID,
    SOPInstanceUID,
  } = instance;

  const splitImageId = imageId.split('&frame');
  let frameIndex =
    splitImageId[1] !== undefined ? Number(splitImageId[1]) : 0;

  if ((splitImageId[1] === undefined) && instance.NumberOfFrames > 1) {
    // Handle the multi-frame cases(ex: OPT) where each image share the same SOP Instance UID
    const url = splitImageId[0];
    const splittedUrl = url.split('frames/');
    if (splittedUrl[1] && Number(splittedUrl[1])) {
      frameIndex = Number(splittedUrl[1]);
    }
  }

  const imagePath = [
    StudyInstanceUID,
    SeriesInstanceUID,
    SOPInstanceUID,
    frameIndex,
  ].join('$$$');

  return {
    PatientID,
    StudyInstanceUID,
    SeriesInstanceUID,
    SOPInstanceUID,
    frameIndex,
    imagePath,
  };
}
