import cornerstone from 'cornerstone-core';
import * as FlywheelCommon from '@flywheel/extension-flywheel-common';

const FlywheelUtils = FlywheelCommon.Utils;
const { compareForInsideOutside } = FlywheelUtils.measurementToolUtils.checkMeasurementInsideOutside;
const { compareForPosition } = FlywheelUtils.measurementToolUtils.checkMeasurementPosition;
const { compareForOverlapping } = FlywheelUtils.measurementToolUtils.checkMeasurementOverlap;

const handleContourBoundary = (
  measurementData,
  imageId,
  labels,
  measurements = [],
  element
) => {
  if (!imageId) {
    return;
  }
  const imagePlane = cornerstone.metaData.get('imagePlaneModule', imageId);
  const imageDimension = { x: imagePlane.columns, y: imagePlane.rows };
  const labelObj = labels.find(x => x.value === measurementData.location);
  let boundaryMeasurements = [];

  if (labelObj) {
    if (labelObj.boundary && labelObj.boundary.length > 0) {
      checkBoundaryCondition(
        labelObj,
        measurementData,
        labels,
        measurements,
        imageDimension,
        boundaryMeasurements,
        element
      );
      if (boundaryMeasurements.length === 0) {
        removeBoundarySatisfiedMeasurements(labelObj, labels, measurements);
      }
    }
    checkBoundaryReferences(
      labelObj,
      measurementData,
      labels,
      measurements,
      imageDimension,
      boundaryMeasurements,
      element
    );
    if (boundaryMeasurements.length === 0) {
      removeBoundaryReferenceSatisfiedMeasurements(labelObj, labels, measurements);
    }
  }
  return boundaryMeasurements;
}

function checkBoundaryCondition(
  label,
  measurement,
  labelList,
  measurements,
  imageDimension,
  overlappedMeasurements,
  element
) {
  let status = true;
  if (label.boundary && label.boundary.length > 0) {
    label.boundary.forEach(boundary => {
      const obj = labelList.find(x => x.value === boundary.label || x.label === boundary.label);
      measurements.forEach((measure, index) => {
        if (
          obj &&
          measure.location === obj.value &&
          measure._id !== measurement._id
        ) {
          if (boundary.direction === 'inward') {
            if (measure.toolType !== 'OpenFreehandRoi') {
              status = compareForInsideOutside(
                measurement,
                measurement.toolType,
                measure,
                measure.toolType,
                imageDimension,
                true,
                element
              );
            } else {
              status = !compareForOverlapping(
                measurement,
                measurement.toolType,
                measure,
                measure.toolType,
                imageDimension,
                element
              );
            }
          } else if (boundary.direction === 'outward') {
            if (measure.toolType !== 'OpenFreehandRoi') {
              status = compareForInsideOutside(
                measurement,
                measurement.toolType,
                measure,
                measure.toolType,
                imageDimension,
                false,
                element
              );
            } else {
              status = !compareForOverlapping(
                measurement,
                measurement.toolType,
                measure,
                measure.toolType,
                imageDimension,
                element
              );
            }
          } else {
            const isHorizontal =
              boundary.direction === 'up/down';
            status = compareForPosition(
              measure,
              measure.toolType,
              measurement,
              measurement.toolType,
              imageDimension,
              isHorizontal,
              element
            );
          }
          if (!status) {
            overlappedMeasurements.push(measure);
          }
        }
      });
    });
  }
  return status;
}

function checkBoundaryReferences(
  label,
  measurement,
  labelList,
  measurements,
  imageDimension,
  overlappedMeasurements,
  element
) {
  let status = true;
  labelList.forEach(item => {
    if (item.boundary && item.boundary.length > 0) {
      item.boundary.forEach(boundary => {
        if (boundary.label === label.value || boundary.label === label.label) {
          measurements.forEach((measure, index) => {
            if (
              measure.location === item.value &&
              measure._id !== measurement._id
            ) {
              if (boundary.direction === 'inward') {
                if (measure.toolType !== 'OpenFreehandRoi') {
                  status = compareForInsideOutside(
                    measure,
                    measure.toolType,
                    measurement,
                    measurement.toolType,
                    imageDimension,
                    true,
                    element
                  );
                } else {
                  status = !compareForOverlapping(
                    measurement,
                    measurement.toolType,
                    measure,
                    measure.toolType,
                    imageDimension,
                    element
                  );
                }
              } else if (boundary.direction === 'outward') {
                if (measure.toolType !== 'OpenFreehandRoi') {
                  status = compareForInsideOutside(
                    measure,
                    measure.toolType,
                    measurement,
                    measurement.toolType,
                    imageDimension,
                    false,
                    element
                  );
                } else {
                  status = !compareForOverlapping(
                    measurement,
                    measurement.toolType,
                    measure,
                    measure.toolType,
                    imageDimension,
                    element
                  );
                }
              } else {
                const isHorizontal =
                  boundary.direction === 'up/down';
                status = compareForPosition(
                  measure,
                  measure.toolType,
                  measurement,
                  measurement.toolType,
                  imageDimension,
                  isHorizontal,
                  element
                );
              }
              if (!status) {
                overlappedMeasurements.push(measure);
              }
            }
          });
        }
      });
    }
  });
  return status;
}

function removeBoundarySatisfiedMeasurements(label, labelList, measurements) {
  if (label.boundary && label.boundary.length > 0) {
    const removeIndexes = [];
    label.boundary.forEach((boundary) => {
      const obj = labelList.find(x => (x.value === boundary.label) || (x.label === boundary.label));
      measurements.forEach((measure, index) => {
        if (obj && measure.location === obj.value) {
          if (!removeIndexes.includes(index)) {
            removeIndexes.push(index);
          }
        }
      });
      if (removeIndexes.length > 0) {
        removeIndexes.forEach(removedIndex => {
          if (measurements.length > removedIndex) {
            measurements.splice(removedIndex, 1);
          }
        });
        removeBoundarySatisfiedMeasurements(obj, labelList, measurements)
      }
    });
  }
}

function removeBoundaryReferenceSatisfiedMeasurements(label, labelList, measurements) {
  labelList.forEach(item => {
    const removeIndexes = [];
    if (item.boundary && item.boundary.length > 0) {
      item.boundary.forEach((boundary) => {
        if ((boundary.label === label.value) || (boundary.label === label.label)) {
          measurements.forEach((measure, index) => {
            if (measure.location === item.value) {
              if (!removeIndexes.includes(index)) {
                removeIndexes.push(index);
              }
            }
          });
          if (removeIndexes.length > 0) {
            removeIndexes.forEach(removedIndex => {
              if (measurements.length > removedIndex) {
                measurements.splice(removedIndex, 1);
              }
            });
            removeBoundaryReferenceSatisfiedMeasurements(item, labelList, measurements);
          }
        }
      });
    }
  });
}

export default handleContourBoundary;
