import cornerstone from 'cornerstone-core';
import cornerstoneTools from 'cornerstone-tools';
import store from '@ohif/viewer/src/store';
import jumpToRowItem from './jumpToRowItem.js';
import redux from '../../redux';
import { MeasurementApi } from '../classes';
import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';

const { setSpecificData } = redux.actions;

const scrollToIndex = cornerstoneTools.import('util/scrollToIndex');

const navigateToMeasurementImage = async (
  measurementData,
  viewportsState,
  timepointManagerState,
  options
) => {
  // TODO: invertViewportTimepointsOrder should be stored in / read from user preferences
  // TODO: childToolKey should come from the measurement table when it supports child tools
  options = options || {
    invertViewportTimepointsOrder: false,
    childToolKey: null,
  };

  const actionData = jumpToRowItem(
    measurementData,
    viewportsState,
    timepointManagerState,
    options
  );

  actionData.viewportSpecificData.forEach(viewportSpecificData => {
    const { viewportIndex, displaySet } = viewportSpecificData;

    store.dispatch(setSpecificData(viewportIndex, displaySet));
  });

  await FlywheelCommonUtils.waitForViewportUpdate(
    actionData.viewportSpecificData
  );

  const { toolType, measurementNumber } = measurementData;
  const measurementApi = MeasurementApi.Instance;

  Object.keys(measurementApi.tools).forEach(toolType => {
    const measurements = measurementApi.tools[toolType];

    measurements.forEach(measurement => {
      measurement.active = false;
    });
  });

  const measurementsToActive = measurementApi.tools[toolType].filter(
    measurement => {
      return measurement.measurementNumber === measurementNumber;
    }
  );

  measurementsToActive.forEach(measurementToActive => {
    measurementToActive.active = true;
  });

  measurementApi.syncMeasurementsAndToolData();

  cornerstone.getEnabledElements().forEach((enabledElement, index) => {
    const { displaySet } =
      actionData.viewportSpecificData.find(
        ({ viewportIndex }) => viewportIndex === index
      ) || {};
    if (displaySet && displaySet.images) {
      let scrollIndex = -1;

      // Handle the multi-frame cases(ex: OPT) where each image share the same SOP Instance UID
      if (
        displaySet.isMultiFrame &&
        displaySet.numImageFrames > 1 &&
        displaySet.frameIndex >= 1
      ) {
        scrollIndex = displaySet.frameIndex;
      }
      if (scrollIndex < 0) {
        scrollIndex = displaySet.images.findIndex(
          image =>
            image.SOPInstanceUID.toLowerCase() ===
            displaySet.SOPInstanceUID.toLowerCase()
        );
      }
      if (scrollIndex >= 0) {
        scrollToIndex(enabledElement.element, scrollIndex);
      }
    }
    cornerstone.updateImage(enabledElement.element);
  });
};

export default navigateToMeasurementImage;
