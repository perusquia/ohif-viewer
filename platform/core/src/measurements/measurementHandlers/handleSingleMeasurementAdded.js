import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';
import { MeasurementApi } from '../classes';
import log from '../../log';
import user from '../../user';
import getImageAttributes from '../lib/getImageAttributes';
import getLabel from '../lib/getLabel';
import refreshCornerstoneViewports from '../lib/refreshCornerstoneViewports';
import getImageIdForImagePath from '../lib/getImageIdForImagePath';
import getModality from '../lib/getModality';

export default function handleSingleMeasurementAdded({ eventData, tool }) {
  const measurementApi = MeasurementApi.Instance;
  if (!measurementApi) {
    log.warn('Measurement API is not initialized');
  }

  const { measurementData, toolType } = eventData;

  const collection = measurementApi.tools[toolType];

  // Stop here if the tool data shall not be persisted (e.g. temp tools)
  if (!collection) return;

  // Stop here if there's no measurement data or if it was cancelled
  if (!measurementData || measurementData.cancelled) return;

  log.info('CornerstoneToolsMeasurementAdded');

  const modality = getModality(store.getState());
  let imageId =
    modality === 'SM'
      ? (imageId = getImageIdForImagePath(measurementData.imagePath))
      : eventData.imageId || undefined;

  const imageAttributes = getImageAttributes(eventData.element, imageId);
  const measurement = Object.assign({}, measurementData, imageAttributes, {
    lesionNamingNumber: measurementData.lesionNamingNumber,
    userId: user.getUserId(),
    toolType,
  });

  const viewports = window.store.getState().viewports || {};
  const { viewportSpecificData, activeViewportIndex } = viewports;
  let isActiveViewportIndex2D = false;
  const activeViewportIndexKey =
    viewportSpecificData[activeViewportIndex].StudyInstanceUID ===
      measurement.StudyInstanceUID &&
    viewportSpecificData[activeViewportIndex].SeriesInstanceUID ===
      measurement.SeriesInstanceUID &&
    viewportSpecificData[activeViewportIndex].plugin === 'cornerstone'
      ? activeViewportIndex
      : undefined;
  if (activeViewportIndexKey) {
    isActiveViewportIndex2D = true;
  }
  let index = activeViewportIndex;
  if (!isActiveViewportIndex2D) {
    index = Object.keys(viewportSpecificData).find(
      key =>
        viewportSpecificData[key].StudyInstanceUID ===
          measurement.StudyInstanceUID &&
        viewportSpecificData[key].SeriesInstanceUID ===
          measurement.SeriesInstanceUID &&
        viewportSpecificData[key].plugin.includes('cornerstone')
    );
    if (index < 0 || isNaN(index)) {
      index = activeViewportIndex;
    }
  }
  const slice = FlywheelCommonUtils.getActiveSliceInfo(index)?.sliceNumber;
  measurement.sliceNumber = measurement.sliceNumber || slice;

  const addedMeasurement = measurementApi.addMeasurement(toolType, measurement);
  Object.assign(measurementData, addedMeasurement);

  const measurementLabel = getLabel(measurementData);
  if (measurementLabel) {
    measurementData.labels = [measurementLabel];
  }

  if (modality !== 'SM') {
    // TODO: This is very hacky, but will work for now
    refreshCornerstoneViewports();
  }

  // TODO: Notify about the last activated measurement

  if (MeasurementApi.isToolIncluded(tool)) {
    // TODO: Notify that viewer suffered changes
  }
}
