import { Channel, RemoteEvent, RemoteMethod } from '@flywheel-io/ohif-remote';
import { NEVER } from 'rxjs';
import { filter, first, map } from 'rxjs/operators';

class RemoteService {
  channel = null
  config = null
  enabled = false
  methods = RemoteMethod
  projectId = null

  _window = window

  constructor({ window } = {}) {
    if (window) {
      this._window = window;
    }
    const params = new URLSearchParams(this._window.location.search);
    // enable all functionality if a ?remote parameter exists in the current URL
    // otherwise no-op most functionality
    if (params.has('remote')) {
      this.enabled = true;
      this._connect();
    }
  }

  /* private methods */

  _connect() {
    this.channel = new Channel({
      receiveWindow: this._window,
    });
    this.channel.messages$.subscribe(this._handleMessage);
  }

  _handleMessage = message => {
    switch (message.method) {
      case RemoteMethod.SetConfiguration:
        this.config = message.params.config;
        break;
      case RemoteMethod.SetProject:
        this.projectId = message.params.projectId;
        break;
    }
  }

  /* public methods */

  waitForConfiguration() {
    if (this.config) {
      return Promise.resolve(this.config);
    }
    if (!this.enabled) {
      return Promise.resolve({});
    }
    return this.channel.messages$.pipe(
      first(message => message.method === RemoteMethod.SetConfiguration),
      map(message => {
        if (message.params.error) {
          throw deserializeError(message.params.error);
        }
        return message.params.config;
      }),
    ).toPromise();
  }

  /* receive methods */
  listenForMethod(methodName) {
    if (!this.enabled) {
      return NEVER; // emit nothing and complete
    }
    return this.channel.messages$.pipe(
      filter(message => message.method === methodName),
      map(message => message.params),
    );
  }

  /* send events */

  signalInitialization() {
    if (this.enabled) {
      this.channel.send(RemoteEvent.Init);
    }
  }

  signalTaskOpened(task) {
    if (this.enabled) {
      this.channel.send(RemoteEvent.TaskOpened, { task });
    }
  }

  signalTaskStatusChanged(task, previous, current) {
    if (this.enabled) {
      this.channel.send(RemoteEvent.TaskStatusChanged, { task, previous, current });
    }
  }

  signalTaskSaved(task) {
    if (this.enabled) {
      this.channel.send(RemoteEvent.TaskSaved, { task });
    }
  }

  signalTaskClosed(task) {
    if (this.enabled) {
      this.channel.send(RemoteEvent.TaskClosed, { task });
    }
  }
}

export default RemoteService;
