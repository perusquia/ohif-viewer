import log from '../log.js';
import cornerstone from 'cornerstone-core';
import cornerstoneTools from 'cornerstone-tools';
import getImageId from '../utils/getImageId.js';

export class StudyPrefetcher {
  activeViewport = null;
  viewports = null;
  studies = null;

  constructor(studies) {
    this.studies = studies || [];
    this.prefetchDisplaySetsTimeout = 300;
    this.lastActiveViewportElement = null;
    cornerstone.events.addEventListener(
      'cornerstoneimagecachefull.StudyPrefetcher',
      this.cacheFullHandler
    );
  }

  destroy() {
    this.stopPrefetching();
    cornerstone.events.removeEventListener(
      'cornerstoneimagecachefull.StudyPrefetcher',
      this.cacheFullHandler
    );
  }

  static getInstance() {
    if (!StudyPrefetcher.instance) {
      StudyPrefetcher.instance = new StudyPrefetcher([]);
    }

    return StudyPrefetcher.instance;
  }

  setStudies(studies, cancelCurrentPrefetching = true) {
    const stackPrefetchConfig = cornerstoneTools.stackPrefetch.getConfiguration();
    if (cancelCurrentPrefetching && !stackPrefetchConfig.preserveExistingPool) {
      this.stopPrefetching();
    }
    this.studies = studies;
  }

  setViewports(viewports, activeViewportIndex = 0) {
    if (Object.keys(viewports).length === 0) {
      return;
    }
    this.viewports = viewports;
    this.activeViewport = viewports[activeViewportIndex];
  }

  prefetch(config, cancelCurrentPrefetching = true) {
    if (
      !this.studies ||
      !this.studies.length ||
      !this.viewports ||
      !Object.keys(this.viewports).length
    ) {
      return;
    }

    if (cancelCurrentPrefetching) {
      this.stopPrefetching();
    }
    this.prefetchDisplaySets(config);
  }

  /**
   * Note: Calling this causes currently pending requests to be marked as complete
   * That can be a problem if your study list has been updated with additional studies
   */
  stopPrefetching() {
    cornerstoneTools.requestPoolManager.clearRequestStack('prefetch');
  }

  prefetchDisplaySetsAsync(timeout, config) {
    timeout = timeout || this.prefetchDisplaySetsTimeout;

    clearTimeout(this.prefetchDisplaySetsHandler);
    this.prefetchDisplaySetsHandler = setTimeout(() => {
      this.prefetchDisplaySets(config);
    }, timeout);
  }

  prefetchDisplaySets(config) {
    const displaySetsToPrefetch = this.getDisplaySetsToPrefetch(config);
    const imageIds = this.getImageIdsFromDisplaySets(displaySetsToPrefetch);

    this.prefetchImageIds(imageIds);
  }

  prefetchImageIds(imageIds) {
    const nonCachedImageIds = this.filterCachedImageIds(imageIds);
    const requestPoolManager = cornerstoneTools.requestPoolManager;
    const requestType = 'prefetch';
    const preventCache = false;
    const noop = () => { };

    nonCachedImageIds.forEach(imageId => {
      requestPoolManager.addRequest(
        {},
        imageId,
        requestType,
        preventCache,
        noop,
        noop
      );
    });

    requestPoolManager.startGrabbing();
  }

  getAllDisplaySets(studies) {
    return [].concat(...studies.map(study => study.displaySets));
  }

  getStudy(image) {
    const StudyInstanceUID = cornerstone.metaData.get(
      'StudyInstanceUID',
      image.imageId
    );
    return OHIF.viewer.Studies.find(
      study => study.StudyInstanceUID === StudyInstanceUID
    );
  }

  getSeries(study, image) {
    const SeriesInstanceUID = cornerstone.metaData.get(
      'SeriesInstanceUID',
      image.imageId
    );
    const studyMetadata = OHIF.viewerbase.getStudyMetadata(study);

    return studyMetadata.getSeriesByUID(SeriesInstanceUID);
  }

  getInstance(series, image) {
    const instanceMetadata = cornerstone.metaData.get(
      'instance',
      image.imageId
    );
    return series.getInstanceByUID(instanceMetadata.SOPInstanceUID);
  }

  getActiveDisplaySet(displaySets, displaySetInstanceUID) {
    return displaySets.find(
      displaySet => displaySet.displaySetInstanceUID === displaySetInstanceUID
    );
  }

  getDisplaySetsToPrefetch(config) {
    const displaySetCount = this.getAllDisplaySets(this.studies).length;

    config = config || {
      order: 'closest',
      displaySetCount,
    };

    if (!config || !config.displaySetCount) {
      return [];
    }

    const displaySets = this.getAllDisplaySets(this.studies);
    // Prefetch even if we don't have an available viewport
    const activeDisplaySet = this.activeViewport
      ? this.getActiveDisplaySet(
        displaySets,
        this.activeViewport.displaySetInstanceUID
      )
      : displaySets[0];
    const prefetchMethodMap = {
      topdown: 'getFirstDisplaySets',
      downward: 'getNextDisplaySets',
      closest: 'getClosestDisplaySets',
    };

    const prefetchOrder = config.order;
    const methodName = prefetchMethodMap[prefetchOrder];
    const getDisplaySets = this[methodName];

    if (!getDisplaySets) {
      if (prefetchOrder) {
        log.warn(`Invalid prefetch order configuration (${prefetchOrder})`);
      }

      return [];
    }

    return getDisplaySets.call(
      this,
      displaySets,
      activeDisplaySet,
      config.displaySetCount
    );
  }

  getFirstDisplaySets(displaySets, activeDisplaySet, displaySetCount) {
    const length = displaySets.length;
    const selectedDisplaySets = [];

    for (let i = 0; i < length && displaySetCount; i++) {
      const displaySet = displaySets[i];

      if (displaySet !== activeDisplaySet) {
        selectedDisplaySets.push(displaySet);
        displaySetCount--;
      }
    }

    return selectedDisplaySets;
  }

  getNextDisplaySets(displaySets, activeDisplaySet, displaySetCount) {
    const activeDisplaySetIndex = displaySets.indexOf(activeDisplaySet);
    const begin = activeDisplaySetIndex + 1;
    const end = Math.min(begin + displaySetCount, displaySets.length);

    return displaySets.slice(begin, end);
  }

  getClosestDisplaySets(displaySets, activeDisplaySet, displaySetCount) {
    const activeDisplaySetIndex = displaySets.indexOf(activeDisplaySet);
    const length = displaySets.length;
    const selectedDisplaySets = [activeDisplaySet];
    let left = activeDisplaySetIndex - 1;
    let right = activeDisplaySetIndex + 1;

    while ((left >= 0 || right < length) && displaySetCount) {
      if (left >= 0) {
        selectedDisplaySets.push(displaySets[left]);
        displaySetCount--;
        left--;
      }

      if (right < length && displaySetCount) {
        selectedDisplaySets.push(displaySets[right]);
        displaySetCount--;
        right++;
      }
    }

    return selectedDisplaySets;
  }

  getImageIdsFromDisplaySets(displaySets) {
    let imageIds = [];

    displaySets.forEach(displaySet => {
      imageIds = imageIds.concat(this.getImageIdsFromDisplaySet(displaySet));
    });

    return imageIds;
  }

  getImageIdsFromDisplaySet(displaySet) {
    const imageIds = [];

    // TODO: This duplicates work done by the stack manager
    (displaySet.images || []).forEach(image => {
      const numFrames = image.numFrames;
      if (numFrames > 1) {
        for (let i = 0; i < numFrames; i++) {
          let imageId = getImageId(image, i);
          imageIds.push(imageId);
        }
      } else {
        let imageId = getImageId(image);
        imageIds.push(imageId);
      }
    });

    return imageIds;
  }

  filterCachedImageIds(imageIds) {
    return imageIds.filter(imageId => !this.isImageCached(imageId));
  }

  isImageCached(imageId) {
    const image = cornerstone.imageCache.imageCache[imageId];
    return image && image.sizeInBytes;
  }

  cacheFullHandler = () => {
    log.warn('Cache full');
    this.stopPrefetching();
  };
}
