const DICOM_TAG_FOR_CORECTION = {
  PerFrameFunctionalGroupsSequence: '52009230',
};

/**
 * Special handling for correcting the reported meta data specific issues
 * Note: This will correct the specific tag issues identified only.
 * @param {*} sopInstanceList
 */
const correctMetaData = sopInstanceList => {
  sopInstanceList.forEach(sopInstance => {
    const perFrameTag =
      DICOM_TAG_FOR_CORECTION.PerFrameFunctionalGroupsSequence;
    if (sopInstance[perFrameTag]?.Value?.[0]?.[perFrameTag]) {
      sopInstance[perFrameTag] = sopInstance[perFrameTag].Value[0][perFrameTag];
    }
  });
};

export default correctMetaData;
