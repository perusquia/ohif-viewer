import store from '@ohif/viewer/src/store';
import { HTTP as FlywheelCommonHTTP } from '@flywheel/extension-flywheel-common';
import { setCurrentWebImage } from '@flywheel/extension-flywheel/src/redux/actions/study';
import { ALL_IMAGE_TYPE } from '../../../../utils/regularExpressions';

const { getFileMetadata } = FlywheelCommonHTTP.services;

const decorateSOPInstance = (
  instance,
  SeriesInstanceUID,
  SeriesDescription,
  getSOPInstanceUIDFromUrl
) => {
  // into SOPInstanceUID method ensure inner context to be instance var
  Object.defineProperty(instance, 'SOPInstanceUID', {
    enumerable: true,
    configurable: true,
    get: function() {
      return getSOPInstanceUIDFromUrl(this.url);
    }.bind(instance),
  });

  Object.defineProperty(instance, 'getImageId', {
    enumerable: true,
    configurable: true,
    value: function() {
      return this.url;
    }.bind(instance),
  });

  // expose seriesInstanceUID
  Object.defineProperty(instance, 'SeriesInstanceUID', {
    enumerable: true,
    configurable: true,
    writable: true,
    value: SeriesInstanceUID,
  });

  // expose seriesInstanceUID
  Object.defineProperty(instance, 'SeriesDescription', {
    enumerable: true,
    configurable: true,
    writable: true,
    value: SeriesDescription,
  });

  // into SOPInstanceUID method ensure inner context to be instance var
  Object.defineProperty(instance, 'metadata', {
    enumerable: true,
    configurable: true,
    writable: true,
    value: instance,
  });
};

const getAndStoreFileMetadata = async (containerId, fileName) => {
  const fileMetadata = await getFileMetadata(containerId, fileName);
  const fileId = fileMetadata.file_id;

  store.dispatch(
    setCurrentWebImage(
      fileMetadata.parent_ref.id,
      fileMetadata.name,
      fileId,
      fileMetadata.parents,
      fileMetadata.info
    )
  );
  return fileMetadata;
};

const getContainerIdFromUrl = baseUrl => {
  const containers = 'containers';
  const startIndex = baseUrl.indexOf(containers) + containers.length + 1;
  const endIndex = baseUrl.indexOf('files') - 1;
  const containerId = baseUrl.substring(startIndex, endIndex);

  return containerId;
};

const getFilteredZipInfo = zipInfo => {
  const filteredZipInfo = zipInfo.filter(info => {
    const isWebImageRegex = ALL_IMAGE_TYPE;
    return isWebImageRegex.test(info.path);
  });

  return filteredZipInfo;
};

export {
  decorateSOPInstance,
  getAndStoreFileMetadata,
  getContainerIdFromUrl,
  getFilteredZipInfo,
};
