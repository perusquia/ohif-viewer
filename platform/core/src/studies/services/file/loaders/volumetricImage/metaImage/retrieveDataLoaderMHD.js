// Note: This file is the moved and renamed version of file/loaders/metaImage/retrieveDataLoader.js.

import MetaImageStudyPreparer from './MetaImageStudyPreparer';
import VolumetricImageDataLoaderSync from '../retrieveDataLoader';
import { META_IMAGE_DATA_PROTOCOL } from '../LoaderConstants';

export default class MhdDataLoaderSync extends VolumetricImageDataLoaderSync {
  static extensionRegExp = /\.[mhd]+$/i;

  constructor(server, containerId, fileName, segmentationFileNames) {
    super(server, containerId, fileName, segmentationFileNames);
    this.DATA_PROTOCOL = META_IMAGE_DATA_PROTOCOL;
  }

  getStudyPreparer() {
    return new MetaImageStudyPreparer(this.DATA_PROTOCOL);
  }

  isViewQueryParamRequired(fileName) {
    // The query param will be added at the url creation inside the http services for MHD
    return !MhdDataLoaderSync.extensionRegExp.test(fileName);
  }
}
