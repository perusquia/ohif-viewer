// Note: This file is the moved and renamed version of file/loaders/metaImage/prepareMetaImageStudies.js.
// It is the refactored version of previous functional component changed to class structure with common
// functionalities moved to parent class

import _ from 'lodash';
import * as metaImageLoader from '@flywheel/extension-cornerstone-meta-image';
import {
  Utils as FlywheelCommonUtils,
  HTTP as FlywheelCommonHTTP,
} from '@flywheel/extension-flywheel-common';
import VolumetricImageStudyPreparer from '../VolumetricImageStudyPreparer';

const { getParamsFromContainerFileUrl } = FlywheelCommonUtils;

const { listAcquisitions, getContainer } = FlywheelCommonHTTP.services;

const metaImage = metaImageLoader.metaImage;
const ImageId = metaImage.ImageId;

const SOP_CLASS_UIDS = {
  MR: '1.2.840.10008.5.1.4.1.1.4',
  SEG: '1.2.840.10008.5.1.4.1.1.66.4',
};

async function getRawFileURL(study, header, baseURL) {
  const url = baseURL || study.baseURL;
  const { fileContainerId, fileName } = getParamsFromContainerFileUrl(url);
  const containerInfo = await getContainer(fileContainerId);
  let rawFile = null;
  if (containerInfo.session) {
    const sessionAcquisitions = await listAcquisitions(containerInfo.session);
    sessionAcquisitions.every(acq => {
      rawFile = acq.files.find(file => file.name === header.ElementDataFile);
      return !rawFile ? true : false;
    });
  } else {
    // Handle meta-image files in analysis
    rawFile = containerInfo.files.find(
      file => file.name === header.ElementDataFile
    );
    if (!rawFile) {
      rawFile = containerInfo.inputs.find(
        file => file.name === header.ElementDataFile
      );
    }
  }
  const params = url.split('/');
  let containerIndex = params.findIndex(param => param === fileContainerId);
  if (containerIndex >= 0) {
    params[containerIndex] = rawFile?.parent_ref?.id || fileContainerId;
  }
  let nameIndex = params.findIndex(param => param === fileName);
  if (nameIndex >= 0) {
    params[nameIndex] = header.ElementDataFile || rawFile.name;
  }
  return params.join('/');
}

class MetaImageStudyPreparer extends VolumetricImageStudyPreparer {
  constructor(dataProtocol) {
    super();
    this.dataProtocol = dataProtocol;
    this.rawFileUrl = '';
  }

  getDataProtocol() {
    return this.dataProtocol;
  }

  async getFileUrl(study, header, baseURL, isSource = false) {
    const url = baseURL || study.baseURL || '';
    let fileUrl = '';
    if (isSource) {
      fileUrl = await getRawFileURL(study, header, baseURL);
    } else {
      const fileNameIndexStart = url.lastIndexOf('/');
      fileUrl =
        this.rawFileUrl ||
        url.slice(0, fileNameIndexStart) + '/' + header.ElementDataFile;
    }

    return fileUrl;
  }

  createImageId(url) {
    return new ImageId.fromURL(url);
  }

  getNumberOfSpatialSlices(header, sliceDimensionIndex) {
    if (header.updatedDimSize) {
      return parseInt(header.updatedDimSize[sliceDimensionIndex], 10);
    }
    return header.DimSize[sliceDimensionIndex];
  }

  async setImageFileURlFromHeader(study, header, baseURL) {
    this.rawFileUrl = await getRawFileURL(study, header, baseURL);

    return header;
  }

  async createSpatialSeries(
    study,
    seriesUid,
    header,
    metadata = {},
    dataProtocol,
    isDerived = false,
    derivedIndex = -1
  ) {
    // If the MHD file is a representation of elevation map, then wait for the entire and
    // raw data to be downloaded to reconstruct the required pixel data corresponding to slices.
    if (header.LayerNames?.length) {
      const fileUrl = await this.getFileUrl(study, header, '');
      const imageId = this.createImageId(fileUrl);
      const imageObject = metaImage.loadVolumeTimepoint(imageId.url);
      await imageObject.promise;
    }
    return super.createSpatialSeries(
      study,
      seriesUid,
      header,
      metadata,
      dataProtocol,
      isDerived,
      derivedIndex
    );
  }

  /**
   * createSpatialInstances - For each spatial series (x, y, z) in the study,
   * creates the instances for each spatial slice.
   *
   * @param  {Object} study The volumetric study whose series will receive instances for each spatial slice.
   * @param  {Object} header The file header.
   * @param  {Boolean} supportsRangeRead Whether the server hosting the volumetric file supports range
   * read requests (to fetch just the initial bytes).
   * @param  {Object} metadata  Metadata for given volumetric study.
   * @param  {boolean} [isSeg=false] It defines if series is segmentation or not
   * @param  {string} [baseURL=false] external baseURL to be used
   * @param  {string} [sourceBaseURL=false] external sourceBaseURL to be used in case isSeg true
   * @return {Promise}                    A promise that is fulfilled when
   * all the spatial slices for all series in the study have been created.
   */
  createSpatialInstances(
    study,
    header,
    supportsRangeRead,
    metadata = {},
    isSeg = false,
    baseURL,
    sourceBaseURL = ''
  ) {
    const spatialInstancesCreationPromise = new Promise(
      async (resolve, reject) => {
        const loadSliceMetaInfoTasks = [];
        let firstImageLoadTask = null;

        const fileUrl = await this.getFileUrl(study, header, baseURL);
        let sourceFileUrl = '';
        if (
          study.series.find(
            series => sourceBaseURL.indexOf(series.dataProtocol) >= 0
          )
        ) {
          const sourceHeader = await this.loadHeader(
            sourceBaseURL,
            supportsRangeRead,
            true
          );
          sourceFileUrl = await this.getFileUrl(
            study,
            sourceHeader,
            sourceBaseURL,
            true
          );
        }
        study.series.forEach(async series => {
          if (series.instances.length > 0) {
            return;
          }
          const sliceDimensionIndex = 'xyz'.indexOf(series.dimension);
          const { SeriesInstanceUID, SeriesDescription, SeriesNumber } = series;
          const numberOfSpatialSlices = this.getNumberOfSpatialSlices(
            header,
            sliceDimensionIndex
          );

          // creates one instance in the series for each spatial slice
          for (
            let sliceIndex = 0;
            sliceIndex < numberOfSpatialSlices;
            sliceIndex++
          ) {
            const imageIdFormat = this.createImageId(fileUrl);
            let sourceImageIdFormat = {};
            imageIdFormat.slice.dimension = series.dimension;
            imageIdFormat.slice.index = sliceIndex;
            let sourceImageId = '';

            if (sourceBaseURL) {
              if (sourceFileUrl) {
                sourceImageIdFormat = this.createImageId(sourceFileUrl);
                sourceImageIdFormat.slice.dimension = series.dimension;
                sourceImageIdFormat.slice.index = sliceIndex;
                sourceImageId = sourceImageIdFormat.url;
              } else {
                const originalSeriesUID = this.getOriginalSeriesUID(
                  series.SeriesInstanceUID
                );
                const originalSeries = !!study.seriesMap
                  ? study.seriesMap[originalSeriesUID]
                  : null;
                let isSourceMultiFrame = false;
                if (originalSeries?.instances?.length) {
                  isSourceMultiFrame =
                    originalSeries.instances[0].metadata.NumberOfFrames > 1;
                }
                if (isSourceMultiFrame) {
                  sourceImageId =
                    originalSeries.instances[0].metadata.SOPInstanceUID;
                } else {
                  sourceImageId =
                    originalSeries?.instances?.length > sliceIndex
                      ? originalSeries.instances[sliceIndex].metadata
                          .SOPInstanceUID
                      : '';
                }
              }
            }

            // Query the slice metadata by using the image loader(nifti/mhd ...) to get
            // info like the number of rows/columns, pixel spacing etc.
            const loadSliceMetaInfoPromise = this.loadHeader(
              imageIdFormat.url,
              supportsRangeRead
            );

            loadSliceMetaInfoTasks.push(loadSliceMetaInfoPromise);

            // Load the first image to ensure the image downloaded and loaded successfully before starting
            // further images background loading and caching, if load fails handle with proper error message
            // Note: This is not doing while loading segmentation file to avoid disturbing the base image display.
            if (!firstImageLoadTask && !isSeg) {
              const imageObject = metaImage.loadVolumeTimepoint(
                imageIdFormat.url
              );
              firstImageLoadTask = {
                promise: imageObject.promise,
                url: imageIdFormat.filePath,
              };
            }

            loadSliceMetaInfoPromise.then(metaInfo => {
              const sopClassUID = isSeg
                ? SOP_CLASS_UIDS.SEG
                : metadata.sopClassUID || SOP_CLASS_UIDS.MR;
              const instance = {
                url: imageIdFormat.url,
                Columns: metaInfo.columns,
                frame: sliceIndex,
                Rows: metaInfo.rows,
                SOPClassUID: sopClassUID,
                frameOfReferenceUID: metaInfo.frameOfReferenceUID,
                ImagePositionPatient: metaInfo.imagePositionPatient,
                ImageOrientationPatient: metaInfo.imageOrientationPatient,
                PixelSpacing: [
                  metaInfo.rowPixelSpacing,
                  metaInfo.columnPixelSpacing,
                ],
                timeSlice: 0,
                Modality: isSeg ? 'SEG' : 'MR', // MR is dicom tag related for fMRI
                StudyInstanceUID: study.StudyInstanceUID,
              };

              this._decorateSOPInstance(
                instance,
                SeriesInstanceUID,
                SeriesDescription,
                SeriesNumber,
                sourceImageId
              );

              series.instances.push(instance);
            }, reject);
          }
        });

        // only resolves the promise returned by this function when all
        // spatial slices have been created
        Promise.all(loadSliceMetaInfoTasks)
          .then(() => {
            if (!firstImageLoadTask) {
              // If first image not initialized, continue without throwing error
              resolve();
              return;
            }
            firstImageLoadTask.promise.then(
              function(image) {
                if (!image) {
                  reject(
                    'Failed to fetch the image: ' + firstImageLoadTask.url
                  );
                } else {
                  resolve();
                }
              },
              function(error) {
                reject(error.message);
              }
            );
          })
          .catch(error => {
            reject(error);
          });
      }
    );

    return spatialInstancesCreationPromise;
  }

  loadHeader(url, supportsRangeRead, isMetaHeader = false) {
    if (isMetaHeader) {
      return metaImage.loadMetaHeader(url, supportsRangeRead);
    }
    return metaImage.loadHeader(url, supportsRangeRead);
  }

  _getSOPInstanceUIDFromUrl(url) {
    if (url.indexOf('metaimage:') >= 0) {
      return url.substr('metaimage:'.length);
    } else {
      return url;
    }
  }
}

export default MetaImageStudyPreparer;
