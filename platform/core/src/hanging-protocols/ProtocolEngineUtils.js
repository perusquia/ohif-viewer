import { listAcquisitions } from '@flywheel/extension-flywheel-common/src/http/services/acquisition';
import ProtocolEngine from './ProtocolEngine.js';
import utils from '../utils';
import log from '../log';
import classes from '../classes';

const { studyMetadataManager } = utils;
const { OHIFStudyMetadataSource } = classes;

/**
 * Check if the current layout is matching with the hanging protocol layout and viewport data
 * @param {Object} state - store state
 * @param {Object} protocolStore - protocol data
 * @param {function} statusCallback - matching status callback
 * @param {boolean} shouldCompareLayoutOnly - if true then compare the layout and plugin only, not the series inside the viewport
 * @param {Array[StudyMetaData]} studyMetaDataCollection = study data collection
 * @return void - Matching status will be returnes to the caller as callback
 */
const isCurrentLayoutMatchingWithHP = (
  state,
  protocolStore,
  statusCallback,
  shouldCompareLayoutOnly = false,
  studyMetaDataCollection = null
) => {
  if (!studyMetaDataCollection || !studyMetaDataCollection.length) {
    studyMetaDataCollection = getStudyMetaDataCollection(state);
  }

  function setAllViewportSpecificData(
    layout,
    displaySets,
    viewportOrientations = null,
    viewportSpanData = null
  ) {
    const { viewports } = state;
    let isMatching = true;

    if (layout) {
      const viewportKeys = Object.keys(viewports.viewportSpecificData);
      const dsKeys = Object.keys(displaySets);

      if (viewportKeys.length !== dsKeys.length) {
        isMatching = false;
      }

      viewportKeys.forEach(key => {
        const displaySet = viewports.viewportSpecificData[key];

        if (displaySet?.plugin !== displaySets[key]?.plugin) {
          isMatching = false;
        }

        if (
          !shouldCompareLayoutOnly &&
          (displaySet?.StudyInstanceUID !==
            displaySets[key]?.StudyInstanceUID ||
            displaySet?.SeriesInstanceUID !==
              displaySets[key]?.SeriesInstanceUID)
        ) {
          isMatching = false;
        }
      });
    } else {
      isMatching = false;
    }

    statusCallback(isMatching);
  }

  const options = {
    setViewportSpecificData: () => {},
    clearViewportSpecificData: () => {},
    setAllViewportSpecificData: setAllViewportSpecificData,
  };
  findMathingProtocolLayout(protocolStore, studyMetaDataCollection, options);
};

/**
 * Find the matching hanging protocol layout and viewport data
 * @param {Object} protocolStore - protocol data
 * @param {Array[StudyMetaData]} studyMetaDataCollection = study data collection
 * @param {Object} options - contains the callbacks for notifying the matching layout
 * @return void - Matching layout will be returnes to the caller as callback in options
 */
const findMathingProtocolLayout = (
  protocolStore,
  studyMetaDataCollection,
  options
) => {
  try {
    new ProtocolEngine(
      protocolStore,
      studyMetaDataCollection,
      {},
      new OHIFStudyMetadataSource(),
      options
    );
  } catch (error) {
    // FLYW-8500 this sometimes races series metadata and causes an error
    log.error(error);
  }
};

/**
 * Get the active study meta data collection
 * @param {Object} state - store state
 * @return {Array[StudyMetaData]} studyMetaDataCollection = study data collection
 */
const getStudyMetaDataCollection = state => {
  const { viewports, multiSessionData } = state;
  const studyInstanceUIDs = [];
  if (multiSessionData?.isMultiSessionViewEnabled) {
    const acquisitionsData = multiSessionData.acquisitionData;
    acquisitionsData.forEach(acquisition => {
      if (!studyInstanceUIDs.includes(acquisition.studyUid)) {
        studyInstanceUIDs.push(acquisition.studyUid);
      }
    });
  } else {
    Object.keys(viewports.viewportSpecificData).forEach(key => {
      const displaySet = viewports.viewportSpecificData[key];
      if (
        displaySet?.StudyInstanceUID &&
        !studyInstanceUIDs.includes(displaySet.StudyInstanceUID)
      ) {
        studyInstanceUIDs.push(displaySet.StudyInstanceUID);
      }
    });
  }
  const studyMetaDataCollection = [];
  studyInstanceUIDs.forEach(studyInstanceUID => {
    const studyMetaData = studyMetadataManager.get(studyInstanceUID);
    studyMetaDataCollection.push(studyMetaData);
  });
  return studyMetaDataCollection;
};

const filteredFiles = async (projectConfig, containerId) => {
  let selectedFiles = [];
  if (!projectConfig) {
    return selectedFiles;
  }
  const layout = projectConfig.layouts;

  if (layout) {
    const viewports = layout[0]?.viewports.flat() || [];
    const isImageType = viewports.some(
      viewport => viewport.fileType === 'image'
    );

    if (isImageType) {
      const acquisitions = await listAcquisitions(containerId);
      let files = [];

      acquisitions.forEach(acquisition => {
        if (!files.includes(acquisition.files)) {
          files.push(acquisition.files);
        }
      });
      const fileNameRegex = viewports.map(
        viewport => new RegExp(viewport.fileName, 'i')
      );
      fileNameRegex.forEach(regex => {
        files.flat().some(file => {
          const name = file.name;
          const isMatching =
            regex.test(name) &&
            !selectedFiles.includes(file) &&
            file.type === 'image';
          if (isMatching) {
            selectedFiles.push(file);
          }
          return isMatching;
        });
      });
    }
  }
  return selectedFiles;
};

const getProcessesFileData = files => {
  let fileNames = [];
  let containerIds = [];

  files.forEach(file => {
    const name = file.name || '';
    fileNames.push(name);
    containerIds.push(file.parent_ref.id);
  });
  return { fileNames, containerIds };
};

export {
  isCurrentLayoutMatchingWithHP,
  findMathingProtocolLayout,
  filteredFiles,
  getProcessesFileData,
};
