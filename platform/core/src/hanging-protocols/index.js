import ProtocolEngine from './ProtocolEngine.js';
import * as ProtocolEngineUtils from './ProtocolEngineUtils';
import { ProtocolStore, ProtocolStrategy, loadProtocols } from './protocolStore';
import { addCustomAttribute } from './customAttributes';
import { addCustomViewportSetting } from './customViewportSettings';

const hangingProtocols = {
  ProtocolEngine,
  ProtocolStore,
  ProtocolStrategy,
  loadProtocols,
  addCustomAttribute,
  addCustomViewportSetting,
  ProtocolEngineUtils,
};

export default hangingProtocols;
