import cornerstone from 'cornerstone-core';

class CornerstoneColorChannelService {
  constructor() {
    this.listeners = {};
    this.viewportsAdapted = [];
    this.currentChannelId;
  }
  loadAndUpdateImage(imageId, colorChannel, colorChannelName) {
    return cornerstone.loadImage(imageId).then(image => {
      if (!colorChannelName) {
        this._recoverOriginalImage(image);
      } else {
        this._adaptImage(image, colorChannel);
      }
    });
  }
  getNextColorChannel(channelId) {
    const _colorChannel = cornerstone.colors.getColorChannel(channelId);
    const colorChannelName =
      _colorChannel.getColorChannelSchemeName() || undefined;
    const colorChannel = colorChannelName ? _colorChannel : undefined;
    return {
      colorChannel,
      colorChannelName,
    };
  }
  setChannelId(channelId) {
    this.currentChannelId = channelId;
  }

  bindOnNewImage(viewportIndex, element) {
    if (!element) {
      return;
    }
    if (this.listeners[viewportIndex]) {
      element.removeEventListener(
        cornerstone.EVENTS.NEW_IMAGE,
        this.listeners[viewportIndex]
      );
    }

    this.listeners[viewportIndex] = this._onNewImageListener.bind(
      this,
      viewportIndex
    );

    element.addEventListener(
      cornerstone.EVENTS.NEW_IMAGE,
      this.listeners[viewportIndex]
    );
  }

  onChangeColorChannel(element, nextColorChannel, nextColorChannelName) {
    const updateImagesRenderMethod = element => {
      const promises = [];
      const enabledElement = cornerstone.getEnabledElement(element);
      if (enabledElement.image?.imageId) {
        if (!nextColorChannelName) {
          this._recoverOriginalImage(enabledElement.image);
        }
        promises.push(
          this.loadAndUpdateImage(
            enabledElement.image.imageId,
            nextColorChannel,
            nextColorChannelName
          ).catch(error => {
            console.log(error);
          })
        );
      }
      return promises;
    };

    Promise.all(updateImagesRenderMethod(element)).then(() => {
      const enabledElement = (cornerstone.getEnabledElements() || []).find(
        enabledElement => enabledElement.element === element
      );
      if (enabledElement?.image?.imageId) {
        const viewport = cornerstone.getViewport(element);
        this._updateViewportColorChannel(element, viewport, nextColorChannel);
      }
    });
  }

  _onNewImageListener(viewportIndex, event = {}) {
    const { image, viewport, element } = event.detail;
    if (
      this.currentChannelId &&
      viewport.colorChannel !== this.currentChannelId
    ) {
      const { colorChannel } = this.getNextColorChannel(this.currentChannelId);
      this._adaptImage(image, colorChannel, viewportIndex);
      this._updateViewportColorChannel(element, viewport, colorChannel);
      this.viewportsAdapted[viewportIndex] = true;
    }
  }

  _recoverOriginalImage(image) {
    image.render = image.originalRender;
    delete image.originalRender;
    image.colorChannel = image.originalcolorChannel;
    delete image.originalcolorChannel;
  }

  _updateViewportColorChannel(element, viewport, colorChannel) {
    if (viewport.colorChannel && !colorChannel) {
      cornerstone.changeColorChannel(element);
    }
    viewport.colorChannel = colorChannel;
    cornerstone.setViewport(element, viewport);
  }

  _adaptImage(image, colorChannel, viewportIndex) {
    //store original render method
    if (typeof image.originalRender !== 'function') {
      image.originalRender = image.render;
    }

    if (this.viewportsAdapted[viewportIndex] && colorChannel) {
      if (typeof image.originalcolorChannel !== 'object') {
        image.originalcolorChannel = image.colorChannel;
      }
      image.colorChannel = colorChannel;
    }

    image.render = null;
  }
}
const cornerstoneColorChannelService = new CornerstoneColorChannelService();

export default cornerstoneColorChannelService;
