export const TIFF_IMAGES = /.+\.tiff?$/i;
export const ZIP_IMAGES = /.+\.zip$/i;
export const WEB_IMAGES = /.+\.(png|jpe?g)$/i;
export const ALL_IMAGE_TYPE = /.+\.(zip|tif?f|png|jpe?g)$/i;
