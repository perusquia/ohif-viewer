import { Utils } from '@flywheel/extension-flywheel-common';

/**
* Returns the task id of the task enabled in edit mode for multiple reader tasks
* or the task id of single reader task,
* or null if no task is enabled in edit mode in the case of  multiple reader tasks
* or no value is returned if not a reader task.
*
* @returns {string|null}
*/
const getActiveTaskId = () => {
  const { getRouteParams, hasMultipleReaderTask } = Utils;
  const { taskId } = getRouteParams();
  const state = store.getState();
  const featureConfig = state.flywheel.featureConfig;
  const reader_tasks = featureConfig?.features?.reader_tasks;
  const isReaderTask = taskId && reader_tasks;

  if (isReaderTask) {
    const hasMultipleTask = hasMultipleReaderTask(state);

    return hasMultipleTask ? state.viewerAvatar.taskId : taskId;
  }
};

export default getActiveTaskId;
