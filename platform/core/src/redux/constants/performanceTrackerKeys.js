export default {
  INITIAL_IMAGE_LOAD: 'IMAGE: Initial Loading',
  IMAGE_SCROLL: 'IMAGE: Scrolling',
  SEGMENTATION_LOAD: 'SEGMENTATION: Loading',
};
