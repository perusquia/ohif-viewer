import actions from './actions.js';
import reducers from './reducers';
import localStorage from './localStorage.js';
import sessionStorage from './sessionStorage.js';
import * as constants from './constants/ActionTypes';
import performanceTrackerKeys from './constants/performanceTrackerKeys.js';

const redux = {
  reducers,
  actions,
  constants,
  localStorage,
  sessionStorage,
  performanceTrackerKeys,
};

export default redux;
