export const defaultState = {
  unfinished: {},
  performanceTracker: {},
};

const performanceTracker = (state = defaultState, action) => {
  switch (action.type) {
    case 'START_ENTRY':
      state.unfinished[action.key] = state.unfinished?.[action.key] || {};
      state.unfinished[action.key][action.id] = action.startTime;
      return { ...state };
    case 'END_ENTRY':
      let startTime = state.unfinished?.[action.key]?.[action.id];
      if (startTime) {
        state.unfinished?.[action.key]?.[action.id] &&
        Object.keys(state.unfinished?.[action.key]).length === 1
          ? delete state.unfinished?.[action.key]
          : delete state.unfinished?.[action.key]?.[action.id];
        state.performanceTracker[action.key] =
          state.performanceTracker?.[action.key] || [];
        state.performanceTracker[action.key].push({
          startTime,
          endTime: action.endTime,
        });
      }
      return { ...state };
    case 'ADD_ENTRY':
      state.performanceTracker[action.key] =
        state.performanceTracker?.[action.key] || [];
      state.performanceTracker[action.key].push({
        startTime: action.startTime,
        endTime: action.endTime,
      });
      return { ...state };
    case 'REMOVE_ENTRY':
      state.unfinished?.[action.key]?.[action.id] &&
      Object.keys(state.unfinished?.[action.key]).length === 1
        ? delete state.unfinished?.[action.key]
        : delete state.unfinished?.[action.key]?.[action.id];
      return { ...state };
    case 'CLEAR_LOG':
      state.unfinished = {};
      state.performanceTracker = {};
      return { ...state };
    default:
      return state;
  }
};

export default performanceTracker;
