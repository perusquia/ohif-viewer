import extensions from './extensions';
import loading from './loading';
import preferences from './preferences';
import servers from './servers';
import studies from './studies';
import timepointManager from './timepointManager';
import viewports from './viewports';
import performanceTracker from './performanceTracker';

const reducers = {
  extensions,
  loading,
  preferences,
  servers,
  studies,
  timepointManager,
  viewports,
  performanceTracker,
};

export default reducers;
