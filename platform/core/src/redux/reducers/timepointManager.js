import cloneDeep from 'lodash.clonedeep';

const defaultState = {
  timepoints: [],
  measurements: [],
  orientations: [],
  isRotating: false,
  deletedMeasurementIds: [],
  isSaving: false,
  deletedMeasurementIdsInStudyForm: [],
  isMeasurementDeletedForStudyFormQuestion: false,
  lastDeletedMeasurementId: null,
};

const timepointManager = (state = defaultState, action) => {
  switch (action.type) {
    case 'SET_TIMEPOINTS':
      return Object.assign({}, state, { timepoints: action.state });
    case 'SET_MEASUREMENTS':
      return Object.assign({}, state, { measurements: action.state });
    case 'SET_ORIENTATION':
      let orientations = cloneDeep(state.orientations)
      if (action.isSessionData) {
        return Object.assign({}, state, { orientations: cloneDeep(action.state) });
      }
      let index = orientations.findIndex((item) => {
        if (item.SeriesInstanceUID == action.state.SeriesInstanceUID &&
          item.StudyInstanceUID == action.state.StudyInstanceUID) {
          return item
        }
      })
      if (index > -1) orientations[index] = cloneDeep(action.state)
      else orientations.push(action.state)
      return Object.assign({}, state, { orientations: orientations });
    case 'SET_ROTATION_STATUS':
      return Object.assign({}, state, { isRotating: action.state });
    case 'SET_MEASUREMENT_DELETED':
      return Object.assign({}, state, {
        deletedMeasurementIdsInStudyForm: [],
        isMeasurementDeletedForStudyFormQuestion: action.state
      });
    case 'ADD_DELETED_MEASUREMENT_ID': {
      let deletedMeasurementIds = cloneDeep(state.deletedMeasurementIds);
      deletedMeasurementIds = [...deletedMeasurementIds, { id: action.id, _id: action._id }];
      return Object.assign({}, state, { deletedMeasurementIds: deletedMeasurementIds });
    }
    case 'ADD_DELETED_MEASUREMENT_ID_STUDY_FORM': {
      let deletedMeasurementIdsInStudyForm = cloneDeep(state.deletedMeasurementIdsInStudyForm);
      deletedMeasurementIdsInStudyForm = [...deletedMeasurementIdsInStudyForm, { id: action.id, _id: action._id, uuid: action.uuid, measurement: action.measurement }];
      return Object.assign({}, state, {
        deletedMeasurementIdsInStudyForm: deletedMeasurementIdsInStudyForm,
        isMeasurementDeletedForStudyFormQuestion: true,
        lastDeletedMeasurementId: action.uuid
       });
    }
    case 'REMOVE_DELETED_MEASUREMENT_ID': {
      let deletedMeasurementIds = cloneDeep(state.deletedMeasurementIds);
      const measurementIndex = deletedMeasurementIds.findIndex(measurementId => measurementId.id === action.id)
      if (measurementIndex !== -1) {
        deletedMeasurementIds.splice(measurementIndex, 1);
      }
      return Object.assign({}, state, { deletedMeasurementIds: deletedMeasurementIds });
    }
    case 'SET_SAVE_STATUS': {
      return Object.assign({}, state, { isSaving: action.status });
    }
    default:
      return state;
  }
};

export default timepointManager;
