import React, { Component, useState } from 'react';
import PropTypes from 'prop-types';
import { TextInput } from '@ohif/ui';

import './SimpleDialog.styl';

class SimpleDialog extends Component {
  static propTypes = {
    children: PropTypes.node,
    componentRef: PropTypes.any,
    componentStyle: PropTypes.object,
    rootClass: PropTypes.string,
    isOpen: PropTypes.bool,
    headerTitle: PropTypes.string.isRequired,
    onClose: PropTypes.func.isRequired,
    onConfirm: PropTypes.func.isRequired,
    showFooterButtons: PropTypes.bool,
    confirmButtonTitle: PropTypes.string,
    enableConfirm: PropTypes.bool,
    hideCloseButton: PropTypes.bool,
    hideHeaderSection: PropTypes.bool,
  };

  static defaultProps = {
    isOpen: true,
    componentStyle: {},
    rootClass: '',
    showFooterButtons: true,
  };

  static InputDialog = ({ onSubmit, defaultValue, title, label, onClose }) => {
    const [value, setValue] = useState(defaultValue);

    const onSubmitHandler = () => {
      onSubmit(value);
    };

    return (
      <div className="InputDialog">
        <SimpleDialog
          headerTitle={title}
          onClose={onClose}
          onConfirm={onSubmitHandler}
        >
          <TextInput
            type="text"
            value={value}
            onChange={event => setValue(event.target.value)}
            label={label}
          />
        </SimpleDialog>
      </div>
    );
  };

  render() {
    return (
      <React.Fragment>
        {this.props.isOpen && (
          <div
            className={`simpleDialog ${this.props.rootClass} `}
            ref={this.props.componentRef}
            style={this.props.componentStyle}
          >
            <form>
              {!this.props.hideHeaderSection && <div className="header">
                {!this.props.hideCloseButton && <span className="closeBtn" onClick={this.onClose}>
                  <span className="closeIcon">x</span>
                </span>}
                <h4 className="title">{this.props.headerTitle}</h4>
              </div>
              }
              <div className="content">{this.props.children}</div>
              {this.props.showFooterButtons && (
                <div className="footer">
                  <button className="btn btn-default" onClick={this.onClose}>
                    Cancel
                  </button>
                  <button className={(this.props.confirmButtonTitle && this.props.enableConfirm) ? 'choose-btn' : (this.props.confirmButtonTitle && !this.props.enableConfirm) ? 'disable-choose-btn' : 'btn btn-primary'} onClick={this.onConfirm}>
                    {this.props.confirmButtonTitle ? this.props.confirmButtonTitle : "Confirm"}
                  </button>
                </div>
              )}
            </form>
          </div>
        )}
      </React.Fragment>
    );
  }

  onClose = event => {
    event.preventDefault();
    event.stopPropagation();
    this.props.onClose();
  };

  onConfirm = event => {
    event.preventDefault();
    event.stopPropagation();
    this.props.onConfirm();
  };
}

export { SimpleDialog };
