import React from 'react';
import cornerstone from 'cornerstone-core';
import csTools from 'cornerstone-tools';
import { isEmpty } from 'lodash';

import * as FlywheelCommon from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';
import OHIF from '@ohif/core';
import { Icon } from '@ohif/ui';
import componentTypes from './ComponentTypeFactory';
import { ComponentTypes } from './AnnotationTableComponents';

import './AnnotationTableComponents/CommonStyle.styl';
import getAnnotationText from '@ohif/extension-dicom-microscopy/src/annotations/getAnnotationText';
import { getItemVisibilityStatus } from './AnnotationTableComponents/Utils';

const { getters } = csTools.store;
const {
  getImageIdForImagePath,
  getModality,
  MeasurementApi,
} = OHIF.measurements;
const FlywheelCommonUtils = FlywheelCommon.Utils;
const {
  isCurrentFile,
  hasAnonymousReaderTask,
} = FlywheelCommonUtils;
const FlywheelCommonUnitUtils = FlywheelCommon.ToolsUtils.unitUtils;
const getPixelSpacing = csTools.importInternal('util/getPixelSpacing');

export const createChildComponents = (childList, props = {}) => {
  if (!childList?.length) {
    return null;
  }
  return childList.map((item, index) => {
    const { parentProps } = props;
    const TableComponent = componentTypes[item.typeOfComponent];
    return (
      <TableComponent
        key={item.key + index}
        metadata={{ ...item, ...props }}
        parentProps={parentProps}
      />
    );
  });
};

export const getAvatarStyle = data => {
  const { _id: userId, avatar, firstname, lastname } = data || {};
  const state = store.getState();
  const uniqueId = data.task_id || userId;
  const showAvatar = !hasAnonymousReaderTask(state);
  const initials =
    !showAvatar && data.task_id
      ? ''
      : !isEmpty(firstname) && !isEmpty(lastname)
      ? firstname[0] + lastname[0]
      : '';
  let avatarStyle = {};
  if (uniqueId) {
    avatarStyle = {
      backgroundSize: 'cover',
    };
    const state = store.getState();
    if (avatar && !hasAnonymousReaderTask(state)) {
      avatarStyle.backgroundImage = `url(${avatar})`;
    } else {
      avatarStyle.backgroundColor = getAvatarColor(uniqueId);
    }
  }
  return { avatarStyle, initials, avatar };
};

export const getAvatarColor = userId => {
  let hash = 0;
  for (let i = 0; i < userId.length; i++) {
    hash = userId.charCodeAt(i) + ((hash << 5) - hash);
  }
  const c = (hash & 0x00ffffff).toString(16).toUpperCase();
  return '#' + '00000'.substring(0, 6 - c.length) + c;
};

export const getActionButton = (
  icon,
  onClickCallback,
  isClosed = false,
  title = '',
  disablePointerEvents = false
) => {
  const pointerEvent = disablePointerEvents ? 'pointer-events-none' : '';
  return (
    <span
      className={`icon-size action-button-container ${pointerEvent}`}
      style={{ opacity: isClosed ? 0.5 : 1 }}
      onClick={onClickCallback}
    >
      <svg>
        <title>{title || ''}</title>
        <Icon name={icon} className={icon} width="14px" height="14px" />
      </svg>
    </span>
  );
};

export const preventPropagation = evt => {
  evt.stopPropagation();
  evt.preventDefault();
};

export const getAnnotationClass = toolType => {
  return getters.mouseTools().find(x => x.name === toolType);
};

export const getTextInfo = measurement => {
  const tool = getAnnotationClass(measurement.toolType);
  const storeMeasurement = getMeasurementFromStore(measurement);
  let textBoxContent = [];
  if (storeMeasurement) {
    if (getModality(store.getState()) === 'SM') {
      textBoxContent = getAnnotationText(storeMeasurement);
    } else {
      cornerstone.getEnabledElements().forEach(enabledElement => {
        const element = enabledElement.element;
        const imageId = getImageIdForImagePath(storeMeasurement.imagePath);
        if (
          enabledElement &&
          enabledElement.image &&
          enabledElement.image.imageId === imageId
        ) {
          if (tool.getTextInfo) {
            textBoxContent = tool.getTextInfo(storeMeasurement, {
              imageId,
              element,
              image: enabledElement.image,
            });
          }
        }
      });
    }
  }

  return textBoxContent;
};

export const getTotalArea = (measurements, showMeasurementsUnit) => {
  let totalArea = 0;
  let unit;
  measurements.forEach(measurement => {
    const storeMeasurement = getMeasurementFromStore(measurement);
    if (
      measurement.toolType !== 'ContourRoi' &&
      (storeMeasurement?.cachedStats?.area || storeMeasurement?.area)
    ) {
      totalArea +=
        storeMeasurement?.cachedStats?.area || storeMeasurement?.area;
    }
  });
  if (showMeasurementsUnit) {
    cornerstone.getEnabledElements().forEach(enabledElement => {
      if (enabledElement?.image) {
        const { rowPixelSpacing, colPixelSpacing } = getPixelSpacing(
          enabledElement.image
        );
        const hasPixelSpacing = rowPixelSpacing && colPixelSpacing;
        unit = FlywheelCommonUnitUtils.squareUnit(
          FlywheelCommonUnitUtils.getLengthUnit(hasPixelSpacing, totalArea)
        );
      }
    });

    const mmToMicronConversionFactor = 1000;
    const shouldConvertToMicro = FlywheelCommonUnitUtils.shouldConvertToMicrometer(
      totalArea
    );
    const convertedArea = shouldConvertToMicro
      ? totalArea * mmToMicronConversionFactor * mmToMicronConversionFactor
      : totalArea;
    totalArea = convertedArea;

    return unit && totalArea > 0
      ? `${Number(totalArea).toFixed(2)} ${unit}`
      : null;
  } else {
    return totalArea;
  }
};

export const getMeasurementFromStore = measurement => {
  if (!measurement?.measurementId) {
    measurement = { ...measurement, measurementId: measurement._id };
  }
  const state = store.getState();
  if (state.timepointManager && state.timepointManager.measurements) {
    const measurements = state.timepointManager.measurements;
    const toolData = measurements[measurement.toolType];
    return toolData && toolData.find(x => x._id === measurement.measurementId);
  }
  return null;
};

export const getMeasurementsBySlice = (measureGroup, sliceNumber) => {
  const state = store.getState();
  const projectConfig = state.flywheel.projectConfig;
  let sessionMeasurements = measureGroup.measurements;
  if (!isCurrentFile(state) && projectConfig?.bSlices) {
    sessionMeasurements = sessionMeasurements.filter(
      item => item.sliceNumber === sliceNumber
    );
  }
  return sessionMeasurements;
};

export const getQuestionKeyFromStudyForm = toolType => {
  const state = store.getState();
  const projectConfig = state.flywheel.projectConfig;
  const studyForm = projectConfig?.studyForm?.components || [];
  const question = studyForm.find(x => {
    const option =
      x.values &&
      x.values.find(val => {
        if ((val['measurementTools'] || []).includes(toolType)) {
          return val;
        }
      });
    if (option) {
      return x;
    }
  });

  if (question) {
    return question.key;
  }

  return '';
};

export const getQuestionKeysByToolTypes = () => {
  const state = store.getState();
  const projectConfig = state.flywheel.projectConfig;
  const studyForm = projectConfig?.studyForm?.components || [];
  const labels = {};
  studyForm.forEach(component => {
    component.values &&
      component.values.forEach(item => {
        item?.requireMeasurements?.forEach(label => {
          if (!labels[label]) {
            labels[label] = {};
          }
          item?.measurementTools?.forEach(toolType => {
            if (!labels[label][toolType]) {
              labels[label][toolType] = [];
            }
            if (!labels[label][toolType].includes(component.key)) {
              labels[label][toolType].push(component.key);
            }
          });
        });
      });
  });
  return labels;
};

export const isMeasurementsHidden = measurements => {
  return measurements.some(measure => {
    return !getItemVisibilityStatus(measure);
  });
};

export const updateMeasurementVisibility = (
  evt,
  measurements,
  updateSingleMeasurementVisibility
) => {
  preventPropagation(evt);
  const isVisible = !isMeasurementsHidden(measurements);
  measurements.forEach(measure => {
    const isItemVisible = !getItemVisibilityStatus(measure);
    if (isVisible != isItemVisible) {
      updateSingleMeasurementVisibility(evt, measure, false);
    }
  });
  MeasurementApi.Instance.syncMeasurementsAndToolData();
  MeasurementApi.Instance.onMeasurementsUpdated();
};

export const getSeriesData = (
  allSeries,
  studyInstanceUid,
  seriesInstanceUID
) => {
  const series = allSeries
    .find(s => s?.getData?.().SeriesInstanceUID === seriesInstanceUID)
    ?.getData();
  const seriesId = studyInstanceUid + '_' + seriesInstanceUID;

  if (!series) {
    const returnObj = {};
    returnObj.label = seriesInstanceUID;
    returnObj.key = seriesId;
    returnObj.typeOfComponent = ComponentTypes.ContainerTile;
    returnObj.children = [];
    return returnObj;
  }

  const seriesLabel = series.SeriesDescription
    ? series.SeriesDescription
    : series.SeriesNumber !== undefined
    ? series.SeriesNumber
    : series.SeriesInstanceUID;
  const seriesObj = {};
  seriesObj.label = seriesLabel;
  seriesObj.key = seriesId;
  seriesObj.typeOfComponent = ComponentTypes.ContainerTile;
  seriesObj.children = [];

  return seriesObj;
};
