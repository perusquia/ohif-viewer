import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { isEmpty } from 'lodash';

import { withTranslation } from '../../contextProviders';
import { Icon } from './../../elements/Icon';
import { OverlayTrigger } from './../overlayTrigger';
import { Tooltip } from './../tooltip';
import { TableListItem } from './../tableList/TableListItem.js';

import './MeasurementTableItem.styl';

class MeasurementTableItem extends Component {
  static propTypes = {
    measurementData: PropTypes.object.isRequired,
    onItemClick: PropTypes.func.isRequired,
    onRelabel: PropTypes.func,
    onDelete: PropTypes.func,
    onVisible: PropTypes.func,
    onEditDescription: PropTypes.func,
    itemClass: PropTypes.string,
    itemIndex: PropTypes.number,
    t: PropTypes.func,
    onClickColorPicker: PropTypes.func,
    selectedColor: PropTypes.string,
    closeColorPicker: PropTypes.func,
    readonly: PropTypes.bool,
  };

  render() {
    const { warningTitle = '', hasWarnings } = this.props.measurementData;

    return (
      <React.Fragment>
        {hasWarnings ? (
          <OverlayTrigger
            key={this.props.itemIndex}
            placement="left"
            overlay={
              <Tooltip
                placement="left"
                className="in tooltip-warning"
                id="tooltip-left"
              >
                <div className="warningTitle">{this.props.t(warningTitle)}</div>
                <div className="warningContent">{this.getWarningContent()}</div>
              </Tooltip>
            }
          >
            <div>{this.getTableListItem()}</div>
          </OverlayTrigger>
        ) : (
          <React.Fragment>{this.getTableListItem()}</React.Fragment>
        )}
      </React.Fragment>
    );
  }

  getActionButton = (icon, onClickCallback) => {
    return (
      <button key={icon} className="btnAction" onClick={onClickCallback}>
        <span style={{ marginRight: '4px' }}>
          <Icon name={icon} className={icon} width="14px" height="14px" />
        </span>
      </button>
    );
  };

  getTableListItem = () => {
    const hasWarningClass = this.props.measurementData.hasWarnings
      ? 'hasWarnings'
      : '';

    const actionButtons = [];
    const toolType = this.props.measurementData.toolType;

    if (typeof this.props.onRelabel === 'function'&& !this.props.readonly) {
      const relabelButton = this.getActionButton('edit', this.onRelabelClick);
      actionButtons.push(relabelButton);
    }
    if (typeof this.props.onEditDescription === 'function' && !this.props.readonly) {
      const descriptionButton = this.getActionButton(
        'exclamation-circle',
        this.onEditDescriptionClick
      );
      actionButtons.push(descriptionButton);
    }
    if (typeof this.props.onDelete === 'function' && !this.props.readonly) {
      const deleteButton = this.getActionButton('trash', this.onDeleteClick);
      actionButtons.push(deleteButton);
    }
    if (
      typeof this.props.onClickColorPicker === 'function'
    ) {
      const colorPickButton = this.getActionButton(
        'palette',
        this.onColorPickClick.bind(this)
      );
      actionButtons.push(colorPickButton);
    }
    if (typeof this.props.onVisible === 'function') {
      const measurementData = this.props.measurementData;
      const isVisible = this.getItemVisibilityStatus(measurementData)
      const visibleButton = this.getActionButton(
        isVisible ? 'eye' : 'eye-closed',
        this.onVisibleClick
      );
      actionButtons.push(visibleButton);
    }

    const { _id: userId, avatar, firstname, lastname } =
      this.props.measurementData.user || {};
    const initials =
      !isEmpty(firstname) && !isEmpty(lastname)
        ? firstname[0] + lastname[0]
        : '';
    let avatarStyle = {};
    if (userId) {
      avatarStyle = {
        backgroundSize: 'cover',
      };
      if (avatar) {
        avatarStyle.backgroundImage = `url(${avatar})`;
      } else {
        avatarStyle.backgroundColor = getAvatarColor(userId);
      }
    }
    const { selectedColor } = this.props;
    const color =
      selectedColor.substring(0, selectedColor.lastIndexOf(',')) + ', 1)';

    return (
      <TableListItem
        itemKey={this.props.measurementData.measurementNumber}
        itemClass={`measurementItem ${this.props.itemClass} ${hasWarningClass}`}
        itemIndex={this.props.itemIndex}
        onItemClick={this.onItemClick.bind(this)}
        itemColor={color}
      >
        <div>
          <div className="measurementLocation">
            <div className="avatar" style={avatarStyle}>
              {avatar ? '' : initials}
            </div>
            {this.props.t(this.props.measurementData.label, {
              keySeparator: '>',
              nsSeparator: '|',
            })}
          </div>
          <div className="displayTexts">{this.getDataDisplayText()}</div>
          <div className="rowActions">{actionButtons}</div>
        </div>
      </TableListItem>
    );
  };

  getItemVisibilityStatus(eventData) {
    const store = window.store.getState();
    if (!store.timepointManager) {
      return false;
    }
    const measurements = store.timepointManager.measurements;
    if (measurements[eventData.toolType]) {
      const measurement = measurements[eventData.toolType].find(item => item._id === eventData.measurementId);
      if (measurement) {
        return measurement.visible;
      }
    }
    return false;
  }

  onItemClick = event => {
    this.props.onItemClick(event, this.props.measurementData);
  };

  onRelabelClick = event => {
    // Prevent onItemClick from firing
    event.stopPropagation();
    this.props.closeColorPicker();
    this.props.onRelabel(event, this.props.measurementData);
  };

  onEditDescriptionClick = event => {
    // Prevent onItemClick from firing
    event.stopPropagation();
    this.props.closeColorPicker();
    this.props.onEditDescription(event, this.props.measurementData);
  };

  onDeleteClick = event => {
    // Prevent onItemClick from firing
    event.stopPropagation();
    this.props.closeColorPicker();
    this.props.onDelete(event, this.props.measurementData);
  };

  onColorPickClick = event => {
    // Prevent onItemClick from firing
    event.stopPropagation();
    this.props.onClickColorPicker(event, this.props.measurementData);
  };

  onVisibleClick = event => {
    // Prevent onItemClick from firing
    event.stopPropagation();
    this.props.onVisible(event, this.props.measurementData);
  };

  getDataDisplayText = () => {
    return this.props.measurementData.data.map((data, index) => {
      return (
        <div key={`displayText_${index}`} className="measurementDisplayText">
          {data.displayText ? data.displayText : '...'}
        </div>
      );
    });
  };

  getWarningContent = () => {
    const { warningList = '' } = this.props.measurementData;

    if (Array.isArray(warningList)) {
      const listedWarnings = warningList.map((warn, index) => {
        return <li key={index}>{warn}</li>;
      });

      return <ol>{listedWarnings}</ol>;
    } else {
      return <React.Fragment>{warningList}</React.Fragment>;
    }
  };
}

const getAvatarColor = userId => {
  let hash = 0;
  for (let i = 0; i < userId.length; i++) {
    hash = userId.charCodeAt(i) + ((hash << 5) - hash);
  }
  const c = (hash & 0x00ffffff).toString(16).toUpperCase();
  return '#' + '00000'.substring(0, 6 - c.length) + c;
};

const connectedComponent = withTranslation('MeasurementTable')(
  MeasurementTableItem
);
export { connectedComponent as MeasurementTableItem };
export default connectedComponent;
