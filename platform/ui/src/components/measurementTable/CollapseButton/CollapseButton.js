import React from 'react';

import ToolbarButton from '../../../viewer/ToolbarButton';
import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';
import store from '@ohif/viewer/src/store';

import './CollapseButton.styl';

const {
  changeRightHandPanelMode
} = FlywheelCommonRedux.actions;

const { RightHandPanelModes } = FlywheelCommonRedux.constants;

export function CollapseButton() {

  function getRightHandPanelState() {
    const state = store.getState();
    const panelMode = state.rightHandPanel.panelMode;
    return panelMode;
  }

  function onClick(e) {
    const panelMode = getRightHandPanelState();
    if (panelMode === RightHandPanelModes.NORMAL) {
      return store.dispatch(changeRightHandPanelMode(RightHandPanelModes.EXPAND));
    } else if (panelMode === RightHandPanelModes.EXPAND) {
      return store.dispatch(changeRightHandPanelMode(RightHandPanelModes.NORMAL));
    }
    store.dispatch(changeRightHandPanelMode(RightHandPanelModes.NORMAL));
  }

  function isRightPanelExpanded() {
    const panelMode = getRightHandPanelState();
    return panelMode === RightHandPanelModes.NORMAL;
  }
  const isExpanded = isRightPanelExpanded();
  return (<div className='collapse-button-style margin-right-10' onClick={onClick}>
    <ToolbarButton
      key=''
      label=''
      icon={isExpanded ? 'arrow_back_ios' : 'arrow_forward_ios'}
      class={isExpanded ? 'material-icons expand-icon' : 'material-icons collapse-icon'}
    />
  </div>);
}
