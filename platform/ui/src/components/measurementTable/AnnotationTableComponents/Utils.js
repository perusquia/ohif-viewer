import React from 'react';

import store from '@ohif/viewer/src/store';
import { toolbarModule } from '@ohif/extension-cornerstone';
import { ToolbarButton } from '../../../viewer/ToolbarButton';
import { Icon } from '@ohif/ui';

const DEFAULT_COLOR = 'rgba(255, 255, 0, 0.2)';

const getItemVisibilityStatus = (eventData) => {
  const state = store.getState();
  if (!state.timepointManager) {
    return false;
  }
  const measurements = state.timepointManager.measurements;
  if (measurements[eventData.toolType]) {
    const measurement = measurements[eventData.toolType].find(item => item._id === eventData.measurementId);
    if (measurement) {
      return measurement.visible;
    }
  }
  return false;
}

const getToolIcon = (data, props) => {
  const { toolIcon, toolObj, tool } = getToolDetailsFromToolbarDefinition(data);
  let toolIconComponent = null;
  if (toolObj && toolObj.class) {
    toolIconComponent = <ToolbarButton
      {...props}
      className={`${toolObj.class} icon-size`}
      icon={toolIcon}
      label=''
    />
  } else {
    toolIconComponent = getActionButton(toolObj.icon, () => { }, false, toolObj.label, true);
  }
  return toolIconComponent;
}

const getActionButton = (icon, onClickCallback, isClosed = false, title = '', disablePointerEvents = false) => {
  const pointerEvent = disablePointerEvents ? 'pointer-events-none' : '';
  return (<span className={`icon-size action-button-container ${pointerEvent}`} style={{ opacity: isClosed ? .5 : 1 }} onClick={onClickCallback}>
    <svg>
      <title>{title || ''}</title>
      <Icon name={icon} className={icon} width='14px' height='14px' />
    </svg>
  </span>
  );
};

const getToolDetailsFromToolbarDefinition = (measure) => {
  let toolbarDefinitions = toolbarModule.definitions || [];
  let toolIcon = '';
  let toolObj;
  const tool = toolbarDefinitions.find(x => {
    if (x.commandOptions && x.commandOptions.toolName) {
      return x.commandOptions.toolName === measure.toolType;
    }
    if (x.buttons && x.buttons.some(button => button.commandOptions && button.commandOptions.toolName === measure.toolType)) {
      return x;
    }
  });
  if (tool) {
    if (tool.buttons && Array.isArray(tool.buttons)) {
      toolObj = tool.buttons.find(button => button.commandOptions && button.commandOptions.toolName === measure.toolType);
      if (toolObj && Object.keys(toolObj)) {
        toolIcon = toolObj.icon;
      }
    } else {
      toolIcon = tool.icon;
      toolObj = tool;
    }
  }
  return { toolIcon, toolObj, tool };
}

const getMeasurementColor = (measurement, measurements) => {
  if (measurements && measurements[measurement.toolType]) {
    const _measurement = measurements[measurement.toolType].find(x => x._id === measurement.measurementId);
    if (_measurement && _measurement.color) {
      return _measurement.color;
    }
  }
  return DEFAULT_COLOR;
}

const getQuestionKeyFromStudyForm = (toolType) => {
  const state = store.getState();
  const projectConfig = state.flywheel.projectConfig;
  const studyForm = projectConfig?.studyForm?.components || [];
  const question = studyForm.find((x) => {
    const option = x.values && x.values.find(val => {
      if ((val['measurementTools'] || []).includes(toolType)) {
        return val;
      }
    });
    if (option) {
      return x;
    }
  });

  if (question) {
    return question.key;
  }

  return '';
}


export {
  getItemVisibilityStatus,
  getToolDetailsFromToolbarDefinition,
  getMeasurementColor,
  getQuestionKeyFromStudyForm,
  getToolIcon,
  getActionButton,
}
