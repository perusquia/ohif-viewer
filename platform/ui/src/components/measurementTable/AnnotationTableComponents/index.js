import UserTile from './UserTile';
import ContainerTile from './ContainerTile';
import LabelTile from './LabelTile';
import HeaderTile from './HeaderTile';

const ComponentTypes = {
  'HeaderTile': 'HeaderTile',
  'ContainerTile': 'ContainerTile',
  'UserTile': 'UserTile',
  'LabelTile': 'LabelTile',
}

export {
  UserTile,
  ContainerTile,
  LabelTile,
  HeaderTile,
  ComponentTypes
};
