import React from 'react';
import { ContainerTile, UserTile, HeaderTile, LabelTile } from './AnnotationTableComponents';

const componentTypes = {
  ContainerTile: (newProps) => <ContainerTile metadata={{}} {...newProps} />,
  HeaderTile: (newProps) => <HeaderTile metadata={{}} {...newProps} />,
  UserTile: (newProps) => <UserTile metadata={{}} {...newProps} />,
  LabelTile: (newProps) => <LabelTile metadata={{}} {...newProps} />,
}

export default componentTypes;
