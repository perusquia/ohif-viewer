import './StudyList.styl';

import React from 'react';
import classNames from 'classnames';
import TableSearchFilter from './TableSearchFilter.js';
import PropTypes from 'prop-types';
import { StudyListLoadingText } from './StudyListLoadingText.js';
import { useTranslation } from 'react-i18next';
import store from '@ohif/viewer/src/store';

const getContentFromUseMediaValue = (
  displaySize,
  contentArrayMap,
  defaultContent
) => {
  const content =
    displaySize in contentArrayMap
      ? contentArrayMap[displaySize]
      : defaultContent;

  return content;
};

const readStatus = {
  Todo: 'Todo',
  Inprogress: 'In_progress',
  Complete: 'Complete',
};

/**
 *
 *
 * @param {*} props
 * @returns
 */
function StudyList(props) {
  const {
    isLoading,
    hasError,
    studies,
    sort,
    onSort: handleSort,
    filterValues,
    onFilterChange: handleFilterChange,
    onSelectItem: handleSelectItem,
    studyListDateFilterNumDays,
    displaySize,
  } = props;

  const studyTaskStatus = store.getState().flywheel.featureConfig?.features?.study_task_status;

  const { t, ready: translationsAreReady } = useTranslation([
    'StudyList',
    'Flywheel',
  ]);

  const largeTableMeta = [
    {
      displayText: t('Flywheel:Subject Code'),
      fieldName: 'subjectCode',
      inputType: 'text',
      size: 300,
    },
    {
      displayText: t('Flywheel:Session Label'),
      fieldName: 'sessionLabel',
      inputType: 'text',
      size: 300,
    },
    {
      displayText: t('StudyDate'),
      fieldName: 'StudyDate',
      inputType: 'date-range',
      size: 300,
    },
    {
      displayText: t('Modality'),
      fieldName: 'modalities',
      inputType: 'text',
      size: 100,
    },
  ];

  const mediumTableMeta = [
    {
      displayText: t('Flywheel:Subject Code'),
      fieldName: 'subjectCode',
      inputType: 'text',
      size: 280,
    },
    {
      displayText: t('Flywheel:Session Label'),
      fieldName: 'sessionLabel',
      inputType: 'text',
      size: 280,
    },
    {
      displayText: t('StudyDate'),
      fieldName: 'StudyDate',
      inputType: 'date-range',
      size: 440,
    },
    {
      displayText: t('Modality'),
      fieldName: 'modalities',
      inputType: 'text',
      size: 100,
    },
  ];

  const smallTableMeta = [
    {
      displayText: t('Search'),
      fieldName: 'allFields',
      inputType: 'text',
      size: 100,
    },
  ];

  if (studyTaskStatus) {
    const studyStatus = {
      displayText: t('Flywheel:Study Status'),
      fieldName: 'status',
      inputType: 'select',
      size: 300,
    };
    largeTableMeta.push(studyStatus);
    mediumTableMeta.push(studyStatus);
  } else {
    const read = {
      displayText: t('Flywheel:Read'),
      fieldName: 'isRead',
      inputType: null,
      size: 50,
    };
    largeTableMeta.push(read);
    mediumTableMeta.push(read);
  }

  const tableMeta = getContentFromUseMediaValue(
    displaySize,
    { large: largeTableMeta, medium: mediumTableMeta, small: smallTableMeta },
    smallTableMeta
  );

  return translationsAreReady ? (
    <table className="table table--striped table--hoverable">
      <thead className="table-head">
        <tr className="filters">
          <TableSearchFilter
            meta={tableMeta}
            values={filterValues}
            onSort={handleSort}
            onValueChange={handleFilterChange}
            sortFieldName={sort.fieldName}
            sortDirection={sort.direction}
            studyListDateFilterNumDays={studyListDateFilterNumDays}
          />
        </tr>
      </thead>
      <tbody className="table-body" data-cy="study-list-results">
        {/* I'm not in love with this approach, but it's the quickest way for now
         *
         * - Display different content based on loading, empty, results state
         *
         * This is not ideal because it create a jump in focus. For loading especially,
         * We should keep our current results visible while we load the new ones.
         */}
        {/* LOADING */}
        {isLoading && (
          <tr className="no-hover">
            <td colSpan={tableMeta.length}>
              <StudyListLoadingText />
            </td>
          </tr>
        )}
        {!isLoading && hasError && (
          <tr className="no-hover">
            <td colSpan={tableMeta.length}>
              <div className="notFound">
                {t('There was an error fetching studies')}
              </div>
            </td>
          </tr>
        )}
        {/* EMPTY */}
        {!isLoading && !studies.length && (
          <tr className="no-hover">
            <td colSpan={tableMeta.length}>
              <div className="notFound">{t('No matching results')}</div>
            </td>
          </tr>
        )}
        {!isLoading &&
          studies.map((study, index) => (
            <TableRow
              key={`${study.StudyInstanceUID}-${index}`}
              onClick={StudyInstanceUID => handleSelectItem(StudyInstanceUID)}
              AccessionNumber={study.AccessionNumber || ''}
              modalities={study.modalities}
              PatientID={study.PatientID || ''}
              PatientName={study.PatientName || ''}
              StudyDate={study.StudyDate}
              StudyDescription={study.StudyDescription || ''}
              StudyInstanceUID={study.StudyInstanceUID}
              displaySize={displaySize}
              subjectCode={study.subjectCode}
              sessionLabel={study.sessionLabel}
              isRead={study.isRead}
              status={study.status}
              t={t}
            />
          ))}
      </tbody>
    </table>
  ) : null;
}

StudyList.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  hasError: PropTypes.bool.isRequired,
  studies: PropTypes.array.isRequired,
  onSelectItem: PropTypes.func.isRequired,
  // ~~ SORT
  sort: PropTypes.shape({
    fieldName: PropTypes.string,
    direction: PropTypes.oneOf(['desc', 'asc', null]),
  }).isRequired,
  onSort: PropTypes.func.isRequired,
  // ~~ FILTERS
  filterValues: PropTypes.shape({
    PatientName: PropTypes.string.isRequired,
    PatientID: PropTypes.string.isRequired,
    AccessionNumber: PropTypes.string.isRequired,
    StudyDate: PropTypes.string.isRequired,
    modalities: PropTypes.string.isRequired,
    StudyDescription: PropTypes.string.isRequired,
    patientNameOrId: PropTypes.string.isRequired,
    accessionOrModalityOrDescription: PropTypes.string.isRequired,
    allFields: PropTypes.string.isRequired,
    studyDateTo: PropTypes.any,
    studyDateFrom: PropTypes.any,
    subjectCode: PropTypes.string.isRequired,
    sessionLabel: PropTypes.string.isRequired,
    status: PropTypes.string,
  }).isRequired,
  onFilterChange: PropTypes.func.isRequired,
  studyListDateFilterNumDays: PropTypes.number,
  displaySize: PropTypes.string,
};

StudyList.defaultProps = {};

function TableRow(props) {
  const {
    isHighlighted,
    modalities,
    StudyDate,
    StudyInstanceUID,
    subjectCode,
    sessionLabel,
    isRead,
    status,
    onClick: handleClick,
    displaySize,
  } = props;
  const modality = modalities
    ? modalities
        .split('\\')
        .filter(m => m)
        .join(' ')
    : undefined;

  const { t } = useTranslation('StudyList');

  const studyTaskStatus = store.getState().flywheel.featureConfig?.features?.study_task_status;

  const largeRowTemplate = (
    <tr
      onClick={() => handleClick(StudyInstanceUID)}
      className={classNames({ active: isHighlighted })}
    >
      <td className={classNames({ 'empty-value': !subjectCode })}>
        {subjectCode || `(${t('Empty')})`}
      </td>
      <td>{sessionLabel}</td>
      <td>{StudyDate}</td>
      <td className={classNames({ 'empty-value': !modalities })}>
        {modality || `(${t('Empty')})`}
      </td>
      {studyTaskStatus ? (
        <td
          style={{
            textAlign: 'left',
            color:
              status === readStatus.Complete
                ? 'green'
                : status === readStatus.Inprogress
                ? 'yellow'
                : status === readStatus.Todo
                ? 'blue'
                : 'white',
          }}
        >
          {status === readStatus.Complete ? '✓ ' + t(status) : t(status)}
        </td>
      ) : (
        <td style={{ textAlign: 'center' }}>{isRead ? '✓' : ''}</td>
      )}
    </tr>
  );

  const mediumRowTemplate = (
    <tr
      onClick={() => handleClick(StudyInstanceUID)}
      className={classNames({ active: isHighlighted })}
    >
      <td
        className={classNames('overflow-ellipsis', {
          'empty-value': !subjectCode,
        })}
        title={subjectCode}
      >
        {subjectCode || `(${t('Empty')})`}
      </td>
      <td>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          {/* DESCRIPTION */}
          <div
            className="overflow-ellipsis hide-xs"
            style={{ flexGrow: 1 }}
            title={sessionLabel}
          >
            {sessionLabel}
          </div>
        </div>
      </td>
      {/* DATE */}
      <td>{StudyDate}</td>
      <td>
        {/* MODALITY & ACCESSION */}
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            maxWidth: '80px',
            width: '80px',
          }}
        >
          <div
            className={classNames({
              modalities: modalities,
              'empty-value': !modalities,
            })}
            aria-label={modality}
            title={modality}
          >
            {modality || `(${t('Empty')})`}
          </div>
        </div>
      </td>
      {studyTaskStatus ? (
        <td
          style={{
            textAlign: 'left',
            color:
              status === readStatus.Complete
                ? 'green'
                : status === readStatus.Inprogress
                ? 'yellow'
                : status === readStatus.Todo
                ? 'blue'
                : 'white',
          }}
        >
          {status === readStatus.Complete ? '✓ ' + t(status) : t(status)}
        </td>
      ) : (
        <td style={{ textAlign: 'center' }}>{isRead ? '✓' : ''}</td>
      )}
    </tr>
  );

  const smallRowTemplate = (
    <tr
      onClick={() => handleClick(StudyInstanceUID)}
      className={classNames({ active: isHighlighted })}
    >
      <td style={{ position: 'relative', overflow: 'hidden' }}>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          {/* NAME AND ID */}
          <div
            className={classNames('overflow-ellipsis', {
              'empty-value': !subjectCode,
            })}
            style={{ width: '150px', minWidth: '150px' }}
          >
            <div
              style={{ fontWeight: 500, paddingTop: '3px' }}
              title={subjectCode}
            >
              {subjectCode || `(${t('Empty')})`}
            </div>
          </div>

          {/* DESCRIPTION */}
          <div
            className="overflow-ellipsis hide-xs"
            style={{ flexGrow: 1, paddingLeft: '35px' }}
            title={sessionLabel}
          >
            {sessionLabel}
          </div>

          {/* MODALITY & DATE */}
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              maxWidth: '80px',
              width: '80px',
            }}
          >
            <div
              className={classNames({
                modalities: modalities,
                'empty-value': !modalities,
              })}
              aria-label={modality}
              title={modality}
            >
              {modality || `(${t('Empty')})`}
            </div>
            <div>{StudyDate}</div>
          </div>
        </div>
      </td>
    </tr>
  );

  const rowTemplate = getContentFromUseMediaValue(
    displaySize,
    {
      large: largeRowTemplate,
      medium: mediumRowTemplate,
      small: smallRowTemplate,
    },
    smallRowTemplate
  );

  return rowTemplate;
}

TableRow.propTypes = {
  AccessionNumber: PropTypes.string.isRequired,
  isHighlighted: PropTypes.bool,
  modalities: PropTypes.string,
  PatientID: PropTypes.string.isRequired,
  PatientName: PropTypes.string.isRequired,
  StudyDate: PropTypes.string.isRequired,
  StudyDescription: PropTypes.string.isRequired,
  StudyInstanceUID: PropTypes.string.isRequired,
  subjectCode: PropTypes.string,
  sessionLabel: PropTypes.string,
  isRead: PropTypes.bool,
  status: PropTypes.string,
  displaySize: PropTypes.string,
};

TableRow.defaultProps = {
  isHighlighted: false,
};

export { StudyList };
