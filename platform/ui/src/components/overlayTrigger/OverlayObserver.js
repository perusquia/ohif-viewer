import { Subject } from 'rxjs';

const subject = new Subject();

export const OverlayObserver = {
  setToggleState: () => subject.next(),
  getToggleState: () => subject.asObservable()
};
