window.config = {
  routerBasename: '/pwa',
  showStudyList: true,
  servers: {
    dicomWeb: [
      {
        name: 'DCM4CHEE',
        wadoUriRoot: 'https://server.dcmjs.org/dcm4chee-arc/aets/DCM4CHEE/wado',
        qidoRoot: 'https://server.dcmjs.org/dcm4chee-arc/aets/DCM4CHEE/rs',
        wadoRoot: 'https://server.dcmjs.org/dcm4chee-arc/aets/DCM4CHEE/rs',
        qidoSupportsIncludeField: true,
        imageRendering: 'wadors',
        thumbnailRendering: 'wadors',
        enableStudyLazyLoad: true,
        supportsFuzzyMatching: true,
      },
    ],
  },
  hotkeys: [
    // ~ Global
    {
      commandName: 'incrementActiveViewport',
      label: 'Next Viewport',
      keys: ['right'],
    },
    {
      commandName: 'decrementActiveViewport',
      label: 'Previous Viewport',
      keys: ['left'],
    },
    { commandName: 'performUndo', label: 'Undo operation', keys: ['ctrl+z'] },
    {
      commandName: 'performRedo',
      label: 'Redo operation',
      keys: ['ctrl+shift+z'],
    },
    // Performance tracker
    {
      commandName: 'performanceTracker',
      label: 'Log performance',
      keys: ['ctrl+shift+l'],
    },
    {
      commandName: 'clearPerformanceLog',
      label: 'Clear log',
      keys: ['ctrl+shift+e'],
    },
    // Supported Keys: https://craig.is/killing/mice
    // ~ Cornerstone Extension
    { commandName: 'rotateViewportCW', label: 'Rotate Right', keys: ['r'] },
    { commandName: 'rotateViewportCCW', label: 'Rotate Left', keys: ['l'] },
    {
      commandName: 'changeColorChannel',
      label: 'Color Channel',
      keys: ['shift+r'],
    },
    {
      commandName: 'toggleScaleDisplay',
      label: 'Display Scale',
      keys: ['shift+s'],
    },
    {
      commandName: 'toggleFundusRing',
      label: 'Display Fundus Ring',
      keys: ['shift+f'],
    },
    { commandName: 'invertViewport', label: 'Invert', keys: ['i'] },
    {
      commandName: 'flipViewportHorizontal',
      label: 'Flip Horizontally',
      keys: ['h'],
    },
    {
      commandName: 'flipViewportVertical',
      label: 'Flip Vertically',
      keys: ['v'],
    },
    {
      commandName: 'incrementVerticalStretch',
      commandOptions: { isVertical: true, isIncrement: true },
      label: 'Increment Viewport Stretch Vertically',
      keys: ['y'],
    },
    {
      commandName: 'decrementVerticalStretch',
      commandOptions: { isVertical: true, isIncrement: false },
      label: 'Decrement Viewport Stretch Vertically',
      keys: ['ctrl+y'],
    },
    {
      commandName: 'incrementHorizontalStretch',
      commandOptions: { isVertical: false, isIncrement: true },
      label: 'Increment Viewport Stretch Horizontally',
      keys: ['x'],
    },
    {
      commandName: 'decrementHorizontalStretch',
      commandOptions: { isVertical: false, isIncrement: false },
      label: 'Decrement Viewport Stretch Horizontally',
      keys: ['ctrl+x'],
    },
    { commandName: 'scaleUpViewport', label: 'Zoom In', keys: ['+'] },
    { commandName: 'scaleDownViewport', label: 'Zoom Out', keys: ['-'] },
    { commandName: 'fitViewportToWindow', label: 'Zoom to Fit', keys: ['='] },
    { commandName: 'resetViewport', label: 'Reset', keys: ['space'] },
    // clearAnnotations
    { commandName: 'nextImage', label: 'Next Image', keys: ['down'] },
    { commandName: 'previousImage', label: 'Previous Image', keys: ['up'] },
    { commandName: 'nextBSlice', label: 'Next B Slice', keys: ['b'] },
    // firstImage
    // lastImage
    {
      commandName: 'nextViewportDisplaySet',
      label: 'Previous Series',
      keys: ['pagedown'],
    },
    {
      commandName: 'previousViewportDisplaySet',
      label: 'Next Series',
      keys: ['pageup'],
    },
    // ~ Cornerstone Tools
    { commandName: 'setZoomTool', label: 'Zoom', keys: ['z'] },
    {
      commandName: 'setActiveToolHotkey',
      commandOptions: { toolName: 'Zoom' },
      label: 'Zoom',
      keys: ['z'],
    },
    {
      commandName: 'setActiveToolHotkey',
      commandOptions: { toolName: 'Wwwc' },
      label: 'Wwwc',
      keys: ['w', 'esc'],
    },
    {
      commandName: 'setActiveToolHotkey',
      commandOptions: { toolName: 'Magnify' },
      label: 'Magnify',
      keys: ['m'],
    },
    {
      commandName: 'setActiveToolHotkey',
      commandOptions: { toolName: 'Pan' },
      label: 'Pan',
      keys: ['p'],
    },
    {
      commandName: 'setActiveToolHotkey',
      commandOptions: { toolName: 'StackScroll' },
      label: 'Stack Scroll',
      keys: ['s'],
    },
    {
      commandName: 'setActiveToolHotkey',
      commandOptions: { toolName: 'Angle' },
      label: 'Angle',
      keys: ['a'],
    },
    {
      commandName: 'prevMeasureTool',
      label: 'Previous Measurement Tool',
      keys: ['['],
    },
    {
      commandName: 'nextMeasureTool',
      label: 'Next Measurement Tool',
      keys: [']'],
    },
    { commandName: 'firstImage', label: 'First Image', keys: ['home'] },
    { commandName: 'lastImage', label: 'Last Image', keys: ['end'] },
    // ~ NIfTI only
    {
      commandName: 'nextTemporalSeries',
      label: 'Next Time',
      keys: ['.'],
    },
    {
      commandName: 'previousTemporalSeries',
      label: 'Previous Time',
      keys: [','],
    },
    // ~ Window level presets
    {
      commandName: 'windowLevelPreset1',
      label: 'W/L Preset 1',
      keys: ['1'],
    },
    {
      commandName: 'windowLevelPreset2',
      label: 'W/L Preset 2',
      keys: ['2'],
    },
    {
      commandName: 'windowLevelPreset3',
      label: 'W/L Preset 3',
      keys: ['3'],
    },
    {
      commandName: 'windowLevelPreset4',
      label: 'W/L Preset 4',
      keys: ['4'],
    },
    {
      commandName: 'windowLevelPreset5',
      label: 'W/L Preset 5',
      keys: ['5'],
    },
    {
      commandName: 'windowLevelPreset6',
      label: 'W/L Preset 6',
      keys: ['6'],
    },
    {
      commandName: 'windowLevelPreset7',
      label: 'W/L Preset 7',
      keys: ['7'],
    },
    {
      commandName: 'windowLevelPreset8',
      label: 'W/L Preset 8',
      keys: ['8'],
    },
    {
      commandName: 'windowLevelPreset9',
      label: 'W/L Preset 9',
      keys: ['9'],
    },
    { commandName: 'mpr2d', label: '2D MPR', keys: ['d'] },
    {
      commandName: 'toggleProximityRoiCursorStatus',
      label: 'Toggle Proximity ROI Cursor Display',
      keys: ['c'],
    },
  ],
  i18n: {
    LOCIZE_PROJECTID: 'a8da3f9a-e467-4dd6-af33-474d582a0294',
    LOCIZE_API_KEY: null, // Developers can use this to do in-context editing. DO NOT COMMIT THIS KEY!
    USE_LOCIZE: true,
  },
};
