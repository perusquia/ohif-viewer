/**
 * Entry point for development and production PWA builds.
 * Packaged (NPM) builds go through `index-umd.js`
 */

import 'regenerator-runtime/runtime';
import * as Sentry from '@sentry/react';

import App from './App.js';
import React from 'react';
import ReactDOM from 'react-dom';
// test

/**
 * EXTENSIONS
 * =================
 *
 * Importing and modifying the extensions our app uses HERE allows us to leverage
 * tree shaking and a few other niceties. However, by including them here they become
 * "baked in" to the published application.
 *
 * Depending on your use case/needs, you may want to consider not adding any extensions
 * by default HERE, and instead provide them via the extensions configuration key or
 * by using the exported `App` component, and passing in your extensions as props using
 * the defaultExtensions property.
 */
import OHIFVTKExtension from '@ohif/extension-vtk';
import OHIFDicomHtmlExtension from '@ohif/extension-dicom-html';
import OHIFDicomSegmentationExtension from '@ohif/extension-dicom-segmentation';
import OHIFDicomRtExtension from '@ohif/extension-dicom-rt';
import OHIFDicomMicroscopyExtension from '@ohif/extension-dicom-microscopy';
import OHIFDicomPDFExtension from '@ohif/extension-dicom-pdf';
//import OHIFDicomTagBrowserExtension from '@ohif/extension-dicom-tag-browser';
// Add this for Debugging purposes:
//import OHIFDebuggingExtension from '@ohif/extension-debugging';
import { version } from '../package.json';

import FlywheelExtension from '@flywheel/extension-flywheel';
import FlywheelCommonExtension from '@flywheel/extension-flywheel-common';
import CornerstoneInfusions from '@flywheel/extension-cornerstone-infusions';
import CornerstoneNavigate from '@flywheel/extension-cornerstone-navigate';
import CornerstoneNifti from '@flywheel/extension-cornerstone-nifti';
import CornerstoneMetaImage from '@flywheel/extension-cornerstone-meta-image';
import FlywheelDicomSiemens from '@flywheel/extension-flywheel-dicom-siemens';
import FlywheelDicomMultiFrame from '@flywheel/extension-flywheel-dicom-multiframe';
import FlywheelTiffImage from '@flywheel/extension-flywheel-tiff-image';
import FlywheelZipImage from '@flywheel/extension-flywheel-zip-image';
import MonaiLabel from '@ohif/extension-monai-label';

if (process.env.NODE_ENV === 'production' && process.env.SENTRY_RELEASE) {
  const rateLimit = 30 * 1000; // send same error at most every 30 seconds
  const eventLimiter = {};
  Sentry.init({
    dsn:
      'https://e3d9c7f23b4c4bd3a2ee3ebae8e8700b@o347616.ingest.sentry.io/5395603',
    release: process.env.SENTRY_RELEASE,
    environment: window.location.hostname,
    // rate limit https://github.com/getsentry/sentry-javascript/issues/435#issuecomment-605634518
    beforeSend: (event, hint) => {
      const msg = hint?.originalException?.message;
      if (msg && msg in eventLimiter) {
        return null; // do not send
      }
      eventLimiter[msg] = true;
      setTimeout(() => {
        delete eventLimiter[msg];
      }, rateLimit);
      return event;
    },
  });
}

// Default Settings
let config = {};
if (window) {
  config = window.config || {};

  window.version = version;
}

const appDefaults = {
  config,
  defaultExtensions: [
    OHIFVTKExtension,
    OHIFDicomHtmlExtension,
    OHIFDicomMicroscopyExtension,
    OHIFDicomPDFExtension,
    FlywheelDicomSiemens,
    FlywheelDicomMultiFrame,
    FlywheelCommonExtension, // commmon must come first (to initialize some commons for the others)
    FlywheelExtension,
    CornerstoneInfusions,
    CornerstoneNavigate,
    CornerstoneNifti,
    CornerstoneMetaImage,
    OHIFDicomSegmentationExtension,
    OHIFDicomRtExtension,
    FlywheelTiffImage,
    FlywheelZipImage,
    MonaiLabel,
    //OHIFDebuggingExtension,
    //OHIFDicomTagBrowserExtension,
  ],
};

const appProps = Object.assign({}, appDefaults, config);

/** Create App */
const app = React.createElement(App, appProps, null);

/** Render */
ReactDOM.render(app, document.getElementById('root'));
