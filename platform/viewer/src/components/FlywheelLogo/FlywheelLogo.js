import './FlywheelLogo.css';
import { Tooltip, OverlayTrigger, Icon } from '@ohif/ui';
import React from 'react';

class FlywheelLogo extends React.Component {
  render() {
    return (
      <div className="logo-panel">
        <div className="brand-name">
          <Icon name="flywheel-logo" className="flywheel-logo-image" />
        </div>
        <div className="research-use">
          <span> FOR INVESTIGATIONAL USE ONLY </span>
          <OverlayTrigger
            key='research-use'
            placement='top'
            delayHide={1500}
            overlay={
              <Tooltip
                placement='top'
                className='in tooltip-info'
                id='tooltip-left'
              >
                <div className='tooltip-content'>
                  <span>
                    This imaging viewer is based on an open source,
                    zero-footprint DICOM viewer that is licensed by MIT.
                    For additional details please visit the link provided below.
                  </span>
                  <span style={{ marginTop: '10px', textDecoration: 'underline' }}>
                    <a href="https://github.com/ohif" target="_blank"
                      className="github">
                      github.com/ohif
                    </a>
                  </span>
                </div>
              </Tooltip>
            }
          >
            <span name="info" style={{ fontSize: '14px', paddingLeft: '10px' }}
              className="material-icons">
              info
            </span>
          </OverlayTrigger>
        </div >
      </div >
    );
  }

}

export default FlywheelLogo;
