import { Icon, SelectTree } from '@ohif/ui';
import React, { Component } from 'react';

import LabellingTransition from './LabellingTransition.js';
import OHIFLabellingData from './OHIFLabellingData.js';
import PropTypes from 'prop-types';
import bounding from '../../lib/utils/bounding.js';
import cloneDeep from 'lodash.clonedeep';
import { getAddLabelButtonStyle } from './labellingPositionUtils.js';
import OHIF from '@ohif/core';
import store from '@ohif/viewer/src/store';
import {
  Redux as FlywheelCommonRedux,
  Utils as FLywheelCommonUtils,
} from '@flywheel/extension-flywheel-common';
import { setAutoLabellingOnce } from '@ohif/viewer/src/appExtensions/MeasurementsPanel/actions';
import { getMeasurementLabels } from '@flywheel/extension-flywheel/src/components/StudyForm/validation/ValidationEngine.js';

const { MeasurementApi } = OHIF.measurements;
const { setLabelForQuestion, setRelabel } = FlywheelCommonRedux.actions;
const {
  isCurrentFile,
  hasMultipleReaderTask,
  getMeasurementsBasedOnTask,
  getLabelsForToolWorkFlow,
  isEditModeForMultipleTask,
} = FLywheelCommonUtils;

class LabellingFlow extends Component {
  static propTypes = {
    eventData: PropTypes.object.isRequired,
    measurementData: PropTypes.object.isRequired,
    labellingDoneCallback: PropTypes.func.isRequired,
    updateLabelling: PropTypes.func.isRequired,

    initialTopDistance: PropTypes.number,
    skipAddLabelButton: PropTypes.bool,
    editLocation: PropTypes.bool,
    editDescription: PropTypes.bool,
    availableLabels: PropTypes.arrayOf(PropTypes.string),
    selectedQuestion: PropTypes.object,
    selectedLabels: PropTypes.arrayOf(PropTypes.string),
    projectConfig: PropTypes.shape({
      labels: PropTypes.arrayOf(
        PropTypes.shape({
          label: PropTypes.string,
          value: PropTypes.string.isRequired,
        })
      ),
      studyFormWorkflow: PropTypes.string,
      studyForm: PropTypes.shape({
        // https://github.com/formio/formio.js/wiki/Components-JSON-Schema
        components: PropTypes.arrayOf(
          PropTypes.shape({
            key: PropTypes.string.isRequired,
            label: PropTypes.string,
            defaultValue: PropTypes.string,
            values: PropTypes.arrayOf(
              PropTypes.shape({
                directive: PropTypes.string,
                instructionSet: PropTypes.array,
                label: PropTypes.string,
                value: PropTypes.string,
                excludeMeasurements: PropTypes.arrayOf(PropTypes.string),
                requireMeasurements: PropTypes.arrayOf(PropTypes.string),
              })
            ),
            validate: PropTypes.shape({
              required: PropTypes.bool,
              custom: PropTypes.object,
              minSelectedCount: PropTypes.number,
              maxSelectedCount: PropTypes.number,
            }),
            type: PropTypes.oneOf([
              'content',
              'radio',
              'text',
              'selectboxes',
              'textarea',
              'textfield',
              'dropdown',
            ]).isRequired,
            // This is for conditionally rendering this question.
            conditional: PropTypes.shape({
              // https://github.com/jwadhams/json-logic-js/
              json: PropTypes.object,
            }),
          })
        ),
      }),
    }),
    measurementPanelData: PropTypes.bool,
  };

  constructor(props) {
    super(props);

    const { location, locationLabel, description } = props.measurementData;

    let style = props.componentStyle;
    if (!props.skipAddLabelButton) {
      style = getAddLabelButtonStyle(props.measurementData, props.eventData);
    }

    this.state = {
      location,
      locationLabel,
      description,
      skipAddLabelButton: props.skipAddLabelButton,
      editDescription: props.editDescription,
      editLocation: props.editLocation,
      componentStyle: style,
      confirmationState: false,
      displayComponent: true,
    };

    this.mainElement = React.createRef();
    this.descriptionInput = React.createRef();

    this.initialItems = OHIFLabellingData;
    this.currentItems = cloneDeep(this.initialItems);
    this.hasSingleTaskEditMode = isEditModeForMultipleTask();
  }

  componentDidMount() {
    const hasAutoLabelling = this.isEnabledAutomatedLabelling();
    const autoLabellingOnce = this.isEnabledAutomatedLabellingOnce();
    if (autoLabellingOnce) {
      this.selectTreeSelectCalback(undefined, autoLabellingOnce);
      store.dispatch(setAutoLabellingOnce(false));
    }
    if (
      hasAutoLabelling &&
      !this.props.measurementPanelData &&
      this.currentItems.length === 1
    ) {
      this.selectTreeSelectCalback(undefined, this.currentItems[0]);
    }
  }

  isEnabledAutomatedLabelling() {
    let isEnabledAutomatedLabelling = true;
    const projectConfig = this.props.projectConfig;
    if (projectConfig && projectConfig.hasOwnProperty('automatedLabelling')) {
      isEnabledAutomatedLabelling = projectConfig.automatedLabelling;
    }
    return isEnabledAutomatedLabelling;
  }

  // Store will be updated with value in autoLabellingOnce when measurement is converted to freehand
  // as part of automatic adjustment
  isEnabledAutomatedLabellingOnce() {
    let isEnabledAutomatedLabelling = false;
    const state = store.getState();
    if (
      state.ui.autoLabellingOnce &&
      Object.keys(state.ui.autoLabellingOnce).length !== 0
    ) {
      isEnabledAutomatedLabelling = state.ui.autoLabellingOnce;
    }
    return isEnabledAutomatedLabelling;
  }

  componentDidUpdate = () => {
    this.repositionComponent();
    if (this.state.editDescription) {
      this.descriptionInput.current.focus();
    }
  };

  render() {
    let mainElementClassName = 'labellingComponent';
    if (this.state.editDescription) {
      mainElementClassName += ' editDescription';
    }

    const style = Object.assign({}, this.state.componentStyle);
    if (this.state.skipAddLabelButton) {
      if (style.left - 160 < 0) {
        style.left = 0;
      } else {
        style.left -= 160;
      }
    }

    if (this.state.editLocation) {
      style.top = '50%';
      style.left = '50%';
    }

    const state = store.getState();

    if (this.props.projectConfig && this.props.projectConfig.labels) {
      this.initialItems = this.props.projectConfig.labels;
      const availableLabels = this.props.availableLabels;
      const activeTool = state.infusions.activeTool;

      if (availableLabels?.length > 0 && availableLabels?.[0] !== '') {
        if (this.props.measurementPanelData) {
          let measurements = [];
          const measures = state.timepointManager.measurements;
          const toolTypes = Object.keys(measures);
          toolTypes.forEach(toolType => {
            measures[toolType].forEach(item => {
              if (
                this.props.selectedLabels.includes(item.location) &&
                item.imagePath === this.props.measurementData.imagePath
              ) {
                measurements.push(item);
              }
            });
          });
          this.initialItems = this.initialItems.filter(
            x =>
              !measurements.some(item => item.location === x.value) ||
              x.value === this.props.measurementData.location
          );
        } else {
          this.initialItems = this.initialItems.filter(
            x =>
              availableLabels.includes(x.label) ||
              availableLabels.includes(x.value)
          );
        }
      }

      const isFile = isCurrentFile(state);

      if (
        (this.props.projectConfig?.studyFormWorkflow === 'ROI' ||
          this.props.projectConfig?.studyFormWorkflow === 'Mixed') &&
        availableLabels.length > 0 &&
        this.props.showStudyForm
      ) {
        const tool = this.props.measurementData.toolType;
        let labels = [];
        const isSubForm = state.subForm?.isSubFormPopupEnabled;
        let questionList = [];
        const selectedTaskId = state.viewerAvatar?.taskId;

        if (!isSubForm) {
          questionList = [...this.props.projectConfig?.studyForm?.components];
        }

        const measurements = state.timepointManager.measurements;
        const slice = this.props.measurementData?.sliceNumber;
        const measurementLabels = getMeasurementLabels(
          measurements,
          this.props.projectConfig,
          slice
        );
        let filteredMeasurements =
          !isFile && this.props.projectConfig?.bSlices
            ? measurements[tool].filter(data => data.sliceNumber === slice)
            : measurements[tool];
        let questionKeyList = [];
        let labelName = [];
        if (filteredMeasurements?.length) {
          questionKeyList = filteredMeasurements.map(item => item.questionKey);
          if (this.hasSingleTaskEditMode) {
            filteredMeasurements = getMeasurementsBasedOnTask(
              filteredMeasurements,
              selectedTaskId
            );
          }
          labelName = filteredMeasurements.map(item => item.location);
        }
        const selectedQuestion = this.props.selectedQuestion;
        const subFormAnswer =
          typeof selectedQuestion?.answer === 'object'
            ? selectedQuestion?.answer?.value
            : selectedQuestion?.answer;
        const option = selectedQuestion?.values?.find(
          value => value.value === subFormAnswer
        );
        const selectedQuestionKey = selectedQuestion?.isSubForm
          ? null
          : selectedQuestion?.key;
        const isMixedWorkflow =
          this.props.projectConfig?.studyFormWorkflow === 'Mixed';
        if (
          option?.measurementTools?.includes(tool) &&
          option?.requireMeasurements?.length
        ) {
          if (isMixedWorkflow) {
            if (
              state.subForm?.isSubFormQuestion ||
              selectedQuestion?.isSubForm
            ) {
              option.requireMeasurements?.forEach(label => {
                const selectedLabel = this.props.projectConfig?.labels?.find(
                  data => data.value === label
                );
                labels.push(selectedLabel);
              });
            } else if (
              state.subForm?.studyFormLastAnsweredQuestion?.key &&
              state.subForm?.studyFormLastAnsweredQuestion?.value ===
                subFormAnswer &&
              !state.subForm?.isSubFormQuestion &&
              !selectedQuestion.isSubForm
            ) {
              option.requireMeasurements?.forEach(label => {
                let labelCount = 0;
                let allMeasurements = measurements[tool];
                if (this.hasSingleTaskEditMode) {
                  allMeasurements = getMeasurementsBasedOnTask(
                    allMeasurements,
                    selectedTaskId
                  );
                }
                allMeasurements.forEach(item => {
                  if (
                    !isFile &&
                    this.props.projectConfig?.bSlices &&
                    item.sliceNumber === slice &&
                    item.toolType === tool &&
                    item.location === label &&
                    this.props.measurementData?._id !== item._id
                  ) {
                    labelCount += 1;
                  } else if (
                    item.toolType === tool &&
                    item.location === label &&
                    this.props.measurementData?._id !== item._id
                  ) {
                    labelCount += 1;
                  }
                });
                const selectedLabel = this.props.projectConfig?.labels?.find(
                  data => data.value === label
                );
                if (labelCount < selectedLabel.limit) {
                  labels.push(selectedLabel);
                }
              });
            } else {
              this.getLabels(
                questionList,
                tool,
                labels,
                questionKeyList,
                labelName,
                measurementLabels,
                slice,
                this.props.projectConfig,
                selectedQuestionKey
              );
            }
          } else {
            if (isSubForm) {
              option.requireMeasurements?.forEach(label => {
                const selectedLabel = this.props.projectConfig?.labels?.find(
                  data => data.value === label
                );
                labels.push(selectedLabel);
              });
            } else {
              this.getLabels(
                questionList,
                tool,
                labels,
                questionKeyList,
                labelName,
                measurementLabels,
                slice,
                this.props.projectConfig,
                selectedQuestionKey
              );
            }
          }
        } else {
          if (this.props.measurementData.isSubForm) {
            const subFormName = this.props.measurementData?.subFormName;
            questionList = this.props.projectConfig.studyForm.subForms[
              subFormName
            ].components;
          }

          this.getLabels(
            questionList,
            tool,
            labels,
            questionKeyList,
            labelName,
            measurementLabels,
            slice,
            this.props.projectConfig,
            null
          );
        }

        if (
          isMixedWorkflow &&
          (!state.subForm?.isSubFormQuestion || !selectedQuestion?.isSubForm)
        ) {
          const labelsForTool = getLabelsForToolWorkFlow(activeTool, slice, '');
          labels =
            this.props.projectConfig?.labels?.filter(data =>
              labelsForTool.includes(data.value)
            ) || [];
        }
        this.initialItems = [...new Set(labels)];
      }

      if (
        (((state.infusions.isRelabel || !availableLabels?.length) &&
          this.initialItems?.length) ||
          !this.initialItems?.length ||
          (availableLabels?.length > 0 &&
            availableLabels?.[0] === '' &&
            (this.props.projectConfig?.studyFormWorkflow === 'Form' ||
              !this.props.projectConfig?.studyFormWorkflow))) &&
        this.props.projectConfig?.studyForm?.components &&
        this.props.showStudyForm
      ) {
        const allMeasurements = state.timepointManager.measurements;
        const sliceNo = this.props.measurementData?.sliceNumber;
        const allMeasurementLabels = getMeasurementLabels(
          allMeasurements,
          this.props.projectConfig,
          sliceNo
        );
        const items = [];
        const isSubFormMeasurement = this.props.measurementData?.isSubForm;
        let componentLabels = [];
        let subFormName, question, subFormQuestion, subFormAnnotationId;
        if (isSubFormMeasurement) {
          subFormName = this.props.measurementData?.subFormName;
          subFormAnnotationId = this.props.measurementData?.subFormAnnotationId;
          question = this.props.measurementData?.question;
          subFormQuestion = this.props.measurementData?.questionKey;
          const subFormComponents = this.props.projectConfig.studyForm.subForms[
            subFormName
          ].components;
          subFormComponents.forEach(item => {
            item.values?.forEach(value => {
              if (value?.requireMeasurements?.length) {
                value?.requireMeasurements?.forEach(label => {
                  componentLabels.push(label);
                });
              }
            });
          });
        } else {
          const studyFormComponents = this.props.projectConfig.studyForm
            .components;
          studyFormComponents.forEach(item => {
            item.values?.forEach(value => {
              if (value?.requireMeasurements?.length) {
                value?.requireMeasurements?.forEach(label => {
                  componentLabels.push(label);
                });
              }
            });
          });
        }

        componentLabels = [...new Set(componentLabels)];

        if (isSubFormMeasurement && componentLabels.length) {
          const subFormLabels = this.props.projectConfig?.labels?.filter(data =>
            componentLabels.includes(data.value)
          );
          this.initialItems = subFormLabels;
        }

        if (!this.initialItems?.length) {
          if (this.props.projectConfig?.labels?.length) {
            this.initialItems = this.props.projectConfig?.labels;
          } else {
            this.initialItems = OHIFLabellingData;
          }
        }

        const location = this.props.measurementData?.location;

        this.initialItems.forEach(item => {
          if (componentLabels?.includes(item.value)) {
            const components = !isSubFormMeasurement
              ? !isFile && this.props.projectConfig?.bSlices
                ? allMeasurementLabels.slices?.[sliceNo]?.component
                : allMeasurementLabels.component
              : !isFile && this.props.projectConfig?.bSlices
              ? allMeasurementLabels.slices?.[sliceNo]?.[question]?.[
                  subFormName
                ]?.[subFormAnnotationId]
              : allMeasurementLabels?.[question]?.[subFormName]?.[
                  subFormAnnotationId
                ];

            if (components?.includes(item.value)) {
              const labelLength = components.filter(data => data === item.value)
                .length;
              if (
                labelLength < item?.limit ||
                (state.infusions.isRelabel && location === item.value) ||
                !item?.limit
              ) {
                items.push(item);
              }
            } else {
              items.push(item);
            }
          }
        });
        this.initialItems = items;
      }

      this.currentItems = cloneDeep(this.initialItems);

      if (!this.currentItems.length) {
        if (this.props.projectConfig?.labels?.length) {
          this.currentItems = this.props.projectConfig?.labels;
        } else {
          this.currentItems = OHIFLabellingData;
        }
      }
    }

    return (
      <LabellingTransition
        displayComponent={this.state.displayComponent}
        onTransitionExit={this.props.labellingDoneCallback}
      >
        <>
          <div className="labellingComponent-overlay"></div>
          <div
            className={mainElementClassName}
            style={style}
            ref={this.mainElement}
          >
            {this.labellingStateFragment()}
          </div>
        </>
      </LabellingTransition>
    );
  }

  getLabels = (
    questionList,
    tool,
    labels,
    questionKeyList,
    labelName,
    measurementLabels,
    slice,
    projectConfig,
    selectedQuestionKey
  ) => {
    questionList?.forEach(item => {
      if (!selectedQuestionKey || selectedQuestionKey === item.key) {
        item.values?.forEach(value => {
          if (value.measurementTools?.includes(tool)) {
            value.requireMeasurements?.forEach(data => {
              const labelList = this.props.projectConfig?.labels.filter(
                label => label.value === data
              );
              labelList?.forEach(label => {
                if (
                  !questionKeyList.includes(item.key) ||
                  labelName?.filter(location => location === label.value)
                    ?.length < label?.limit
                ) {
                  let toolCount = measurementLabels.slices
                    ? measurementLabels.slices?.[slice]?.component?.filter(
                        value => value === label.value
                      )?.length
                    : measurementLabels.component?.filter(
                        value => value === label.value
                      )?.length;
                  toolCount = !toolCount ? 0 : toolCount;
                  if (
                    projectConfig?.bSlices &&
                    projectConfig?.bSlices?.settings?.[slice] &&
                    toolCount < label?.limit
                  ) {
                    labels.push(label);
                  } else if (toolCount < label?.limit) {
                    labels.push(label);
                  }
                }
              });
            });
          }
        });
      }
    });
  };

  labellingStateFragment = () => {
    const {
      skipAddLabelButton,
      editLocation,
      description,
      locationLabel,
    } = this.state;

    if (!skipAddLabelButton) {
      return (
        <>
          <button
            type="button"
            className="addLabelButton"
            onClick={this.showLabelling}
          >
            {this.state.location ? 'Edit' : 'Add'} Label
          </button>
        </>
      );
    } else {
      if (editLocation) {
        return (
          <SelectTree
            items={this.currentItems}
            columns={1}
            onSelected={this.selectTreeSelectCalback}
            selectTreeFirstTitle="Assign Label"
            onComponentChange={this.repositionComponent}
          />
        );
      } else {
        return (
          <>
            <div
              className="checkIconWrapper"
              onClick={this.fadeOutAndLeaveFast}
            >
              <Icon name="check" className="checkIcon" />
            </div>
            <div className="locationDescriptionWrapper">
              <div className="location">{locationLabel}</div>
              <div className="description">
                <input
                  id="descriptionInput"
                  ref={this.descriptionInput}
                  defaultValue={description || ''}
                  autoComplete="off"
                  onKeyPress={this.handleKeyPress}
                />
              </div>
            </div>
            <div className="commonButtons">
              <button
                type="button"
                className="commonButton left"
                onClick={this.relabel}
              >
                Relabel
              </button>
              <button
                type="button"
                className="commonButton right"
                onClick={this.setDescriptionUpdateMode}
              >
                {description ? 'Edit ' : 'Add '}
                Description
              </button>
            </div>
            <div className="editDescriptionButtons">
              <button
                type="button"
                className="commonButton left"
                onClick={this.descriptionCancel}
              >
                Cancel
              </button>
              <button
                type="button"
                className="commonButton right"
                onClick={this.descriptionSave}
              >
                Save
              </button>
            </div>
          </>
        );
      }
    }
  };

  relabel = event => {
    const viewportTopPosition = this.mainElement.current.offsetParent.offsetTop;
    const componentStyle = {
      top: event.nativeEvent.y - viewportTopPosition - 55,
      left: event.nativeEvent.x,
    };
    this.setState({
      editLocation: true,
      componentStyle,
    });
  };

  setDescriptionUpdateMode = () => {
    this.descriptionInput.current.focus();

    this.setState({
      editDescription: true,
    });
  };

  descriptionCancel = () => {
    const { description = '' } = cloneDeep(this.state);
    this.descriptionInput.current.value = description;

    this.setState({
      editDescription: false,
    });
  };

  handleKeyPress = e => {
    if (e.key === 'Enter') {
      this.descriptionSave();
    }
  };

  descriptionSave = () => {
    const description = this.descriptionInput.current.value;
    this.props.updateLabelling({ description });

    this.setState({
      description,
      editDescription: false,
    });
  };

  selectTreeSelectCalback = (event, itemSelected) => {
    const location = itemSelected.value;
    const {
      questionKey,
      answer,
      isSubForm,
      subFormName,
      subFormAnnotationId,
      question,
    } = this.props?.selectedQuestion || {};
    const { projectConfig } = this.props;
    store.dispatch(setRelabel(false));
    if (
      projectConfig.studyFormWorkflow === 'ROI' ||
      projectConfig.studyFormWorkflow === 'Mixed'
    ) {
      const state = store.getState();
      const isSubFormPopup = state.subForm?.isSubFormPopupEnabled;
      if (
        ((state.subForm?.isSubFormQuestion || isSubForm) &&
          projectConfig.studyFormWorkflow === 'Mixed') ||
        (isSubFormPopup && projectConfig.studyFormWorkflow === 'ROI')
      ) {
        this.props.updateLabelling({
          location,
          questionKey,
          answer,
          isSubForm,
          subFormName,
          subFormAnnotationId,
          question,
        });
      } else {
        this.props.updateLabelling({
          location,
          questionKey,
          answer,
          isSubForm: false,
          subFormName: '',
          subFormAnnotationId: '',
          question: '',
        });
      }
      store.dispatch(setLabelForQuestion(true));
    } else {
      this.props.updateLabelling({
        location,
        questionKey,
        answer,
        isSubForm,
        subFormName,
        subFormAnnotationId,
        question,
      });
    }

    const eventData = {
      detail: {
        measurementData: this.props.measurementData,
      },
    };
    const customEvent = new CustomEvent('labelApplied', eventData);
    document.dispatchEvent(customEvent);

    const viewportTopPosition = this.mainElement.current.offsetParent.offsetTop;
    const componentStyle = !event
      ? this.state.componentStyle
      : {
          top: event.nativeEvent.y - viewportTopPosition - 25,
          left: event.nativeEvent.x,
        };

    this.setState({
      editLocation: false,
      confirmationState: false,
      displayComponent: false,
      location: itemSelected.value,
      locationLabel: itemSelected.label,
      componentStyle,
    });

    if (this.isTouchScreen) {
      this.setTimeout = setTimeout(() => {
        this.setState({
          displayComponent: false,
        });
      }, 2000);
    }

    const measurementApi = MeasurementApi.Instance;
    Object.keys(measurementApi.tools).forEach(toolType => {
      const measurements = measurementApi.tools[toolType];
      let referenceRoiContour = null;
      if (this.props.measurementData.ROIContourUid) {
        referenceRoiContour = measurements.find(
          measurement =>
            measurement.ROIContourUid ===
            this.props.measurementData.ROIContourUid
        );
      }
      measurements.forEach(measurement => {
        measurement.active = false;
        if (this.props.measurementData._id === measurement._id) {
          if (
            measurement.toolType === 'Angle' ||
            measurement.toolType === 'Length' ||
            measurement.toolType === 'Bidirectional' ||
            measurement.toolType === 'ArrowAnnotate'
          ) {
            const colorSplit = itemSelected.color
              ? itemSelected.color.split(',')
              : null;
            measurement.color =
              measurement.location === itemSelected.value && colorSplit
                ? colorSplit[0] +
                  ',' +
                  colorSplit[1] +
                  ',' +
                  colorSplit[2] +
                  ', 1)'
                : measurement.color;
          } else {
            if (itemSelected.color !== undefined && itemSelected.color !== '') {
              measurement.color =
                measurement.location === itemSelected.value
                  ? itemSelected.color
                  : measurement.color;
            }
          }
          let properties = { color: measurement.color };
          if (referenceRoiContour) {
            properties = {
              ...properties,
              ...{
                description: referenceRoiContour.description,
                color: referenceRoiContour.color,
              },
            };
          }
          this.props.updateLabelling(properties);
        }
      });
    });
    measurementApi.syncMeasurementsAndToolData();
  };

  showLabelling = () => {
    this.setState({
      skipAddLabelButton: true,
      editLocation: false,
    });
  };

  fadeOutAndLeave = () => {
    // Wait for 1 sec to dismiss the labelling component
    this.fadeOutTimer = setTimeout(() => {
      this.setState({
        displayComponent: false,
      });
    }, 1000);
  };

  fadeOutAndLeaveFast = () => {
    this.setState({
      displayComponent: false,
    });
  };

  clearFadeOutTimer = () => {
    if (!this.fadeOutTimer) {
      return;
    }

    clearTimeout(this.fadeOutTimer);
  };

  calculateTopDistance = () => {
    const height = window.innerHeight - window.innerHeight * 0.3;
    let top = this.state.componentStyle.top - height / 2 + 55;
    if (top < 0) {
      top = 0;
    } else {
      if (top + height > window.innerHeight) {
        top -= top + height - window.innerHeight;
      }
    }
    return top;
  };

  repositionComponent = () => {
    // SetTimeout for the css animation to end.
    if (this.state.editLocation) {
      return;
    }
    setTimeout(() => {
      if (!this.mainElement.current) {
        return;
      }
      bounding(this.mainElement);
      if (this.state.editLocation) {
        this.mainElement.current.style.maxHeight = '70vh';
        const top = this.calculateTopDistance();
        this.mainElement.current.style.top = `${top}px`;
      }
    }, 200);
  };
}

export default LabellingFlow;
