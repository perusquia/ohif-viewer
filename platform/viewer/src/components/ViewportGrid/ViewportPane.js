import React, { useEffect } from 'react';
import { useDrop } from 'react-dnd';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import './ViewportPane.css';
import cornerstone from 'cornerstone-core';
import { useSelector } from 'react-redux';

const ViewportPane = function(props) {
  const {
    children,
    onDrop,
    viewportIndex,
    className: propClassName,
    displaySet,
    spanData,
  } = props;

  const orientations = useSelector(
    store => store.timepointManager.orientations
  );

  const [{ hovered, highlighted }, drop] = useDrop({
    accept: 'thumbnail',
    drop: (droppedItem, monitor) => {
      const canDrop = monitor.canDrop();
      const isOver = monitor.isOver();
      const dragDropEventInfo = {
        clientOffset: monitor.getClientOffset(),
      };
      if (canDrop && isOver && onDrop) {
        const { StudyInstanceUID, displaySetInstanceUID } = droppedItem;

        onDrop({
          viewportIndex,
          StudyInstanceUID,
          displaySetInstanceUID,
          dragDropEventInfo,
        });
      }
    },
    // Monitor, and collect props.
    // Returned as values by `useDrop`
    collect: monitor => ({
      highlighted: monitor.canDrop(),
      hovered: monitor.isOver(),
    }),
  });

  if (spanData) {
    // This is for supporting assymetric layout by extending the grid layout viewport with spanning in to multiple rows/columns
    return (
      <div
        className={classNames(
          'viewport-drop-target',
          { hovered: hovered },
          { highlighted: highlighted },
          propClassName
        )}
        ref={drop}
        data-cy={`viewport-container-${viewportIndex}`}
        style={{
          gridColumnStart: `${spanData.startX + 1}`,
          gridColumnEnd: `${spanData.endX + 1}`,
          gridRowStart: `${spanData.startY + 1}`,
          gridRowEnd: `${spanData.endY + 1}`,
        }}
      >
        {children}
      </div>
    );
  } else {
    return (
      <div
        className={classNames(
          'viewport-drop-target',
          { hovered: hovered },
          { highlighted: highlighted },
          propClassName
        )}
        ref={drop}
        data-
        cy={`viewport-container-${viewportIndex}`}
      >
        {children}
      </div>
    );
  }
};

ViewportPane.propTypes = {
  children: PropTypes.node.isRequired,
  viewportIndex: PropTypes.number.isRequired,
  onDrop: PropTypes.func.isRequired,
  className: PropTypes.string,
};

export default ViewportPane;
