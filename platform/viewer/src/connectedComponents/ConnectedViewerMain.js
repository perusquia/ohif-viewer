import OHIF from '@ohif/core';
import isEqual from 'lodash/isEqual';
import { connect } from 'react-redux';
import ViewerMain from './ViewerMain';

import { Redux as FlywheelRedux } from '@flywheel/extension-flywheel';
import { Utils as FlywheelCommonUtils, Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';

const {
  selectActiveProtocolStore,
  selectActiveProject,
  selectProjectConfig,
} = FlywheelRedux.selectors;
const { isProtocolApplicable } = FlywheelCommonUtils;
const { setActiveProtocolStore } = FlywheelRedux.actions;
const { setProtocolLayout } = FlywheelCommonRedux.actions;

const {
  setViewportSpecificData,
  clearViewportSpecificData,
} = OHIF.redux.actions;

const { loadProtocols } = OHIF.hangingProtocols;

const mapStateToProps = state => {
  const { activeViewportIndex, layout, viewportSpecificData } = state.viewports;
  const projectConfig = selectProjectConfig(state);
  const project = selectActiveProject(state);
  const protocolStoreState = selectActiveProtocolStore(state);
  const isLoadProtocol = isProtocolApplicable(state, window.location, true);
  let protocolLayout = null;
  let waitForProtocol = false;

  if (
    isLoadProtocol &&
    projectConfig?.layouts &&
    (!protocolStoreState || protocolStoreState.projectId !== project._id)
  ) {
    const protocolStore = loadProtocols(projectConfig);
    waitForProtocol = true;
    protocolStore.onReady(() => {
      window.store.dispatch(setActiveProtocolStore(project._id, protocolStore, waitForProtocol));
    });
  }

  waitForProtocol =
    waitForProtocol ||
    (protocolStoreState && protocolStoreState.waitForProtocol);

  // Only include the protocolLayout if we have layout settings for this project
  if (
    projectConfig &&
    state.infusions &&
    state.infusions.protocolLayout
  ) {
    protocolLayout = state.infusions.protocolLayout;
  }

  return {
    activeViewportIndex,
    layout,
    viewportSpecificData,
    viewports: state.viewports,
    protocolLayout,
    waitForProtocol,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setViewportSpecificData: (viewportIndex, data) => {
      dispatch(setViewportSpecificData(viewportIndex, data));
    },
    clearViewportSpecificData: viewportIndex => {
      dispatch(clearViewportSpecificData(viewportIndex));
    },
    setProtocolLayout: (layout) => {
      dispatch(setProtocolLayout(layout));
    },
  };
};

const ConnectedViewerMain = connect(
  mapStateToProps,
  mapDispatchToProps
)(ViewerMain);

export default ConnectedViewerMain;
