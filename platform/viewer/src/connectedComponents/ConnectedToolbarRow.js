import { connect } from 'react-redux';
import ToolbarRow from "./ToolbarRow";

const mapStateToProps = (state) => {
  return {
    // To do: Same needs to be done for 'activeTool' to reflect activated tool in
    // toolbar[if not activated by clicking on toolbar] in 2D view.
    activeVtkTool: state.infusions.activeVtkTool,
  };
};

const ConnectedToolbarRow = connect(mapStateToProps)(ToolbarRow);

export default ConnectedToolbarRow;
