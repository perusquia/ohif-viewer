import Header from '../components/Header/Header.js';
import { connect } from 'react-redux';
import { Redux as FlywheelRedux } from '@flywheel/extension-flywheel';

const { selectProjectConfig } = FlywheelRedux.selectors;

const mapStateToProps = state => {
  return {
    user: state.oidc && state.oidc.user,
    projectConfig: selectProjectConfig(state),
  };
};

const ConnectedHeader = connect(mapStateToProps)(Header);

export default ConnectedHeader;
