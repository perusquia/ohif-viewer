const isWebImageDataProtocol = dataProtocol =>
  dataProtocol === 'http' ||
  dataProtocol === 'https' ||
  dataProtocol === 'tiff' ||
  isZipWebImageProtocol(dataProtocol);

const isZipWebImageProtocol = dataProtocol => dataProtocol === 'zip';

export { isWebImageDataProtocol, isZipWebImageProtocol };
