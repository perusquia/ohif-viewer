import { connect } from 'react-redux';
import { CineDialog } from '@ohif/ui';
import OHIF from '@ohif/core';
import { commandsManager } from './../App.js';
import cloneDeep from 'lodash.clonedeep';

const { setViewportSpecificData } = OHIF.redux.actions;
/**
 * Original implement from ConnectedCineDialog
 */
// Why do I need or care about any of this info?
// A dispatch action should be able to pull this at the time of an event?
// `isPlaying` and `cineFrameRate` might matter, but I think we can prop pass for those.
const mapStateToProps = state => {
  // Get activeViewport's `cine` and `stack`
  const { viewportSpecificData, activeViewportIndex } = state.viewports;
  const { cine } = viewportSpecificData[activeViewportIndex] || {};
  const dom = commandsManager.runCommand('getActiveViewportEnabledElement');

  const cineData = cine || {
    isPlaying: false,
    cineFrameRate: 1,
  };

  // New props we're creating?
  return {
    activeEnabledElement: dom,
    activeViewportCineData: cineData,
    activeViewportIndex: state.viewports.activeViewportIndex,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatchSetViewportSpecificData: (viewportIndex, data) => {
      dispatch(setViewportSpecificData(viewportIndex, data));
    },
  };
};

const mergeProps = (propsFromState, propsFromDispatch, ownProps) => {
  const {
    activeEnabledElement,
    activeViewportCineData,
    activeViewportIndex,
  } = propsFromState;

  // equally to original temporal dialog pass down callback methods
  // in this case actions will change time slices (instead of stack)
  return {
    // Using min 1 and max 10. Realistic values.
    // Temporal updating uses app store. In case is required for a greater frame rate, consider changing navigateTimeInViewport and also caching data structures to reduce repainting time.
    cineMinFrameRate: 1,
    cineMaxFrameRate: 10,
    cineFrameRate: activeViewportCineData.cineFrameRate,
    isPlaying: activeViewportCineData.isPlaying,
    onPlayPauseChanged: isPlaying => {
      const cine = cloneDeep(activeViewportCineData);
      cine.isPlaying = !cine.isPlaying;
      cine.isTemporal = true;
      propsFromDispatch.dispatchSetViewportSpecificData(activeViewportIndex, {
        cine,
      });
      commandsManager.runCommand('playTemporalSeries', {
        playing: cine.isPlaying,
      });
    },
    onFrameRateChanged: frameRate => {
      const cine = cloneDeep(activeViewportCineData);
      cine.cineFrameRate = frameRate;
      propsFromDispatch.dispatchSetViewportSpecificData(activeViewportIndex, {
        cine,
      });
    },
    onClickNextButton: () => {
      commandsManager.runCommand('nextTemporalSeries');
    },
    onClickBackButton: () => {
      commandsManager.runCommand('previousTemporalSeries');
    },
    onClickSkipToStart: () => {
      commandsManager.runCommand('firstTemporalSeries');
    },
    onClickSkipToEnd: () => {
      commandsManager.runCommand('lastTemporalSeries');
    },
  };
};

const ConnectedCineDialog = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(CineDialog);

export default ConnectedCineDialog;
