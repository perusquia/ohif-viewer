import React, { useContext } from 'react';
import GoogleCloudApi from '../googleCloud/api/GoogleCloudApi';
import usePrevious from './usePrevious';

import * as GoogleCloudUtilServers from '../googleCloud/utils/getServers';
import { useSelector, useDispatch } from 'react-redux';
import isEqual from 'lodash.isequal';
import { HTTP as FlywheelCommonHTTP } from '@flywheel/extension-flywheel-common';

const { getRequestURL } = FlywheelCommonHTTP.utils.REQUEST_UTILS;
const { GET_CONTAINER_FILES } = FlywheelCommonHTTP.urls;

// Contexts
import AppContext from '../context/AppContext';

const getActiveServer = servers => {
  const isActive = a => a.active === true;

  return servers && servers.servers && servers.servers.find(isActive);
};

class ServersAdpaters {
  constructor() {
    this.tempServers = [];
    this.tempServersMapIndex = {};
  }

  add(
    isValidServer,
    flagServerValue,
    isEqualServer,
    getServersObj,
    getBaseUrl
  ) {
    if (flagServerValue) {
      const baseURL = getBaseUrl();
      const servers = getServersObj(baseURL);
      const validServer = isValidServer ? isValidServer(servers[0]) : true;

      if (validServer) {
        const index = this.tempServersMapIndex[baseURL];

        if (index === undefined) {
          this.tempServersMapIndex[baseURL] = this.length;
          this.tempServers.push({
            servers,
            isEqualServer,
          });
        } else {
          this.tempServers[index] = {
            servers,
            isEqualServer,
          };
        }
      }
    }
  }
  hasAnyServer() {
    return this.tempServers && this.tempServers.length;
  }
  shouldUseServerFromUrl(servers, activeServer, previousServers) {
    // server from url valid only for nifti adapter and gcloud adapter
    if (!this.hasAnyServer()) {
      return false;
    }

    // do not update from url. use state instead.
    if (this.hasServerChanged(previousServers, servers)) {
      return false;
    }

    // if no valid urlbased servers
    if (this.isEmptyServers(this.tempServers)) {
      return false;
    } else if (
      this.isEmptyServers(servers) ||
      this.isEmptyServers(activeServer)
    ) {
      // no current valid server
      return true;
    }

    const newServers = this.getFirstAndActiveTempServers();
    return !this.findTempServer(newServers[0]);
  }

  hasServerChanged(previousServers, servers) {
    return previousServers !== servers && previousServers;
  }

  isEmptyServers(servers) {
    return !servers || !servers.length;
  }

  getFirstAndActiveTempServers() {
    const servers = this.tempServers.length
      ? this.tempServers[0].servers
      : undefined;

    if (servers) {
      servers[0].active = true;
    }

    return servers;
  }

  findTempServer(newServer, servers) {
    let exists = false;

    this.tempServers.forEach(tempObj => {
      const { isEqualServer } = tempObj;
      exists = servers.some(isEqualServer.bind(undefined, newServer));

      if (exists) {
        return true;
      }
    });

    return !exists;
  }
}

const isValidServer = (server, appConfig) => {
  if (appConfig.enableGoogleCloudAdapter) {
    return GoogleCloudUtilServers.isValidServer(server);
  }

  return !!server;
};

const setServers = (dispatch, servers) => {
  const action = {
    type: 'SET_SERVERS',
    servers,
  };
  dispatch(action);
};

const serversAdapters = new ServersAdpaters();

export default function useServer({
  project,
  location,
  dataset,
  dicomStore,
  containerId,
} = {}) {
  // Hooks
  const servers = useSelector(state => state && state.servers);
  const previousServers = usePrevious(servers);
  const dispatch = useDispatch();

  const { appConfig = {} } = useContext(AppContext);
  const activeServer = getActiveServer(servers);

  // gcloud
  serversAdapters.add(
    GoogleCloudUtilServers.isValidServer,
    appConfig.enableGoogleCloudAdapter,
    GoogleCloudUtilServers.isEqualServer,
    baseURL => {
      const data = {
        project,
        location,
        dataset,
        dicomStore,
        wadoUriRoot: baseURL,
        qidoRoot: baseURL,
        wadoRoot: baseURL,
      };
      return GoogleCloudUtilServers.getServers(data, dicomStore);
    },
    () => {
      return GoogleCloudApi.getUrlBaseDicomWeb(
        project,
        location,
        dataset,
        dicomStore
      );
    }
  );

  if (containerId) {
    // nifti
    serversAdapters.add(
      server => server && server.container && server.baseURL,
      appConfig.enableFileAdapter,
      (server, otherServer) => {
        return (
          server.container === otherServer.container &&
          server.baseURL === otherServer.baseURL
        );
      },
      baseURL => {
        return [
          {
            baseURL,
            container: containerId,
          },
        ];
      },
      () => {
        const urlParams = {
          pathParams: {
            containerId: containerId || '',
          },
        };

        const config = { prefixRoot: false };
        return getRequestURL(GET_CONTAINER_FILES, urlParams, config);
      }
    );
  }

  const shouldUpdateServer = serversAdapters.shouldUseServerFromUrl(
    servers.servers,
    activeServer,
    previousServers
  );

  if (shouldUpdateServer) {
    const urlBasedServers = serversAdapters.getFirstAndActiveTempServers();
    setServers(dispatch, urlBasedServers);
  } else if (isValidServer(activeServer, appConfig)) {
    return activeServer;
  }
}
