import { connect } from 'react-redux';

import { FormMeasurementTable } from '@ohif/ui';
import { Redux as FlywheelCommonRedux } from '@flywheel/extension-flywheel-common';
import { withAppContext } from '../../context/AppContext';
import dispatchActions from './dispatchActions.js';
import {
  convertMeasurementsToTableData,
  convertTimepointsToTableData,
  displaySetContainsSopInstance,
  getSaveFunction,
  syncMeasurementsAndToolData,
} from './Utils';

const {
  updateRoiColors,
  updatePaletteColors,

  setMeasurementColorIntensity,
} = FlywheelCommonRedux.actions;

const mapStateToProps = (state, ownProps) => {
  const { timepointManager, servers, colorPalette } = state;
  const { timepoints, measurements } = timepointManager;
  const activeServer = servers.servers.find(a => a.active === true);
  const { appConfig = {} } = ownProps || {};
  const { enableMeasurementTableWarning } = appConfig;

  return {
    timepoints: convertTimepointsToTableData(timepoints),
    measurementCollection: convertMeasurementsToTableData(
      measurements,
      timepoints,
      state.flywheel.allUsers,
      enableMeasurementTableWarning
    ),
    timepointManager: state.timepointManager,
    viewports: state.viewports,
    saveFunction: getSaveFunction(activeServer.type),
    palette: colorPalette.palette,
    paletteIndex: colorPalette.paletteIndex,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    dispatchJumpToRowItem: (
      measurementData,
      viewportsState,
      timepointManagerState,
      options
    ) => {
      dispatchActions.dispatchJumpToRowItem(
        measurementData,
        viewportsState,
        timepointManagerState,
        options
      );
    },
    dispatchUpdateRoiColor: roiColor => {
      dispatch(updateRoiColors(roiColor));
    },
    dispatchUpdatePaletteColors: measurements => {
      dispatch(updatePaletteColors(measurements));
    },
    dispatchMeasurementColorIntensity: status => {
      dispatch(setMeasurementColorIntensity(status));
      syncMeasurementsAndToolData();
    },
    dispatchActiveHoveredMeasurements: measurements => {
      dispatchActions.dispatchActiveHoveredMeasurements(measurements, dispatch);
    },
  };
};

const mergeProps = (propsFromState, propsFromDispatch, ownProps) => {
  const {
    timepoints,
    saveFunction,
    measurementCollection,
    timepointManager,
    palette,
    paletteIndex,
  } = propsFromState;
  const {
    onSaveComplete,
    selectedMeasurementNumber,
    appConfig,
    servicesManager,
    sliceInfo,
    onClickUndo,
    onClickRedo,
  } = ownProps;
  const measurements = timepointManager?.measurements
    ? timepointManager.measurements
    : {};
  return {
    timepoints,
    saveFunction,
    measurementCollection,
    onSaveComplete,
    selectedMeasurementNumber,
    appConfig,
    onClickUndo,
    onClickRedo,
    measurements,
    palette,
    paletteIndex,
    sliceInfo,
    ...propsFromDispatch,
    onItemClick: (event, measurementData) => {
      dispatchActions.onItemClick(
        event,
        measurementData,
        propsFromState,
        propsFromDispatch
      );
    },
    onEditLabelDescriptionClick: (event, measurementData, tableType = '') => {
      const viewportsState = propsFromState.viewports;
      dispatchActions.dispatchEditLabelDescription(
        event,
        measurementData,
        viewportsState,
        tableType
      );
    },
    onEditSubFormClick: (measurementData) => {
      dispatchActions.dispatchEditSubForm(measurementData);
    },
    getImageOrientation: measurementData => {
      return dispatchActions.getImageOrientation(measurementData, {
        timepointManager,
      });
    },
    onDeleteGroupMeasurementClick: (event, measurements) => {
      dispatchActions.onDeleteGroupMeasurementClick(event, measurements, {
        servicesManager,
      });
    },
    setMeasurementColorIntensity: status => {
      propsFromDispatch.dispatchMeasurementColorIntensity(status);
    },
    setActiveHoveredMeasurements: measurements => {
      propsFromDispatch.dispatchActiveHoveredMeasurements(measurements);
    },
    onDeleteClick: (event, measurementData) => {
      dispatchActions.onDeleteClick(event, measurementData, { servicesManager });
    },
    onChangeColor: (color, measurementData) => {
      dispatchActions.onChangeColor(color, measurementData);
    },
    onVisibleClick: (event, measurementData, notifyUpdateAndSync = true) => {
      dispatchActions.onVisibleClick(
        event,
        measurementData,
        notifyUpdateAndSync
      );
    },
  };
};

const ConnectedFormMeasurementTable = withAppContext(
  connect(mapStateToProps, mapDispatchToProps, mergeProps)(FormMeasurementTable)
);

export default ConnectedFormMeasurementTable;
