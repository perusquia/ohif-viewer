import { Components as FlywheelComponents } from '@flywheel/extension-flywheel';
import { Utils as FlywheelCommonUtils } from '@flywheel/extension-flywheel-common';
import React from 'react';
import ConnectedMeasurementTable from './ConnectedMeasurementTable.js';
import ConnectedAnnotationTable from './ConnectedAnnotationTable.js';
import ConnectedFormMeasurementTable from './ConnectedFormMeasurementTable.js';
import store from '@ohif/viewer/src/store';
import init from './init.js';
//import LabellingFlow from '../../components/Labelling/LabellingFlow';

const { shouldShowStudyForm, isMicroscopyData } = FlywheelCommonUtils;

const { MeasurementPanel: FlywheelMeasurementPanel } = FlywheelComponents;

export default {
  /**
   * Only required property. Should be a unique value across all extensions.
   */
  id: 'measurements-table',
  get version() {
    return window.version;
  },

  preRegistration({
    servicesManager,
    commandsManager,
    configuration = {},
    hotkeysManager = {},
  }) {
    init({ servicesManager, commandsManager, configuration, hotkeysManager });
  },

  getPanelModule({ servicesManager, commandsManager }) {
    const { UINotificationService, UIDialogService } = servicesManager.services;

    const showLabellingDialog = (props, measurementData) => {
      if (!UIDialogService) {
        console.warn('Unable to show dialog; no UI Dialog Service available.');
        return;
      }

      UIDialogService.dismiss({ id: 'labelling' });
      UIDialogService.create({
        id: 'labelling',
        centralize: true,
        isDraggable: false,
        showOverlay: true,
        content: null, //LabellingFlow,
        contentProps: {
          measurementData,
          labellingDoneCallback: () =>
            UIDialogService.dismiss({ id: 'labelling' }),
          updateLabelling: ({ location, description, response }) => {
            measurementData.location = location || measurementData.location;
            measurementData.description = description || '';
            measurementData.response = response || measurementData.response;

            commandsManager.runCommand(
              'updateTableWithNewMeasurementData',
              measurementData
            );
          },
          ...props,
        },
      });
    };

    const ExtendedConnectedMeasurementTable = () => (
      <ConnectedMeasurementTable
        servicesManager={servicesManager}
        onRelabel={tool =>
          showLabellingDialog(
            { editLocation: true, skipAddLabelButton: true },
            tool
          )
        }
        onEditDescription={tool =>
          showLabellingDialog({ editDescriptionOnDialog: true }, tool)
        }
        onSaveComplete={message => {
          if (UINotificationService) {
            UINotificationService.show(message);
          }
        }}
      />
    );

    const ExternalFormMeasurementTable = ({ newProps }) => {
      let props = { ...newProps, showStudyForm: null };
      return (
        <FlywheelMeasurementPanel
          {...props}
          servicesManager={servicesManager}
          commandsManager={commandsManager}
        >
          {parentProps => (
            <ConnectedFormMeasurementTable
              servicesManager={servicesManager}
              onClickUndo={event => {
                commandsManager.runCommand('performUndo', {});
              }}
              onClickRedo={event => {
                commandsManager.runCommand('performRedo', {});
              }}
              {...parentProps}
            ></ConnectedFormMeasurementTable>
          )}
        </FlywheelMeasurementPanel>
      );
    };
    const ExternalAnnotationTable = ({ props }) => {
      return (
        <FlywheelMeasurementPanel
          {...props}
          servicesManager={servicesManager}
          commandsManager={commandsManager}
          showStudyFormButtonsOnly={true}
          showStudyForm={false}
        >
          {parentProps => (
            <ConnectedAnnotationTable
              servicesManager={servicesManager}
              commandsManager={commandsManager}
              onClickUndo={event => {
                commandsManager.runCommand('performUndo', {});
              }}
              onClickRedo={event => {
                commandsManager.runCommand('performRedo', {});
              }}
              {...parentProps}
            ></ConnectedAnnotationTable>
          )}
        </FlywheelMeasurementPanel>
      );
    };
    return {
      menuOptions: [
        {
          icon: 'view_list',
          label: 'Forms',
          class: 'material-icons',
          target: 'form-measurement-panel',
          isDisabled: studies => {
            return !hasForm() || isMicroscopyData(studies)
          }
        },
        {
          icon: 'draw',
          label: 'Annotations',
          class: 'material-icons',
          target: 'annotations-measurement-panel',
        },
      ],
      components: [
        {
          id: 'form-measurement-panel',
          component: ExternalFormMeasurementTable,
        },
        {
          id: 'annotations-measurement-panel',
          component: ExternalAnnotationTable,
        },
      ],
      defaultContext: ['VIEWER'],
    };
  },
};

function hasForm() {
  const state = store.getState();
  const projectConfig = state.flywheel?.projectConfig;
  const showStudyForm = shouldShowStudyForm(
    projectConfig,
    state,
    window.location
  );

  return showStudyForm;
}
