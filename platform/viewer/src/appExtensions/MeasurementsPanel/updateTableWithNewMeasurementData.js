import OHIF from '@ohif/core';
import cornerstone from 'cornerstone-core';
import { smAnnotationHandler } from '@ohif/extension-dicom-microscopy';

const { getModality } = OHIF.measurements;

export default function updateTableWithNewMeasurementData({
  toolType,
  measurementNumber,
  location,
  description,
  questionKey,
  answer,
  isSubForm,
  subFormName,
  subFormAnnotationId,
  question,
  color,
  ROIContourUid,
  dirty,
}) {
  const { annotationOperations, handleAnnotationChange } = smAnnotationHandler;
  // Update all measurements by measurement number
  const measurementApi = OHIF.measurements.MeasurementApi.Instance;
  const measurements = measurementApi.tools[toolType].filter(
    m => m.measurementNumber === measurementNumber
  );
  const modality = getModality(store.getState());
  measurements.forEach(measurement => {
    handleAnnotationChange(annotationOperations.CHANGE_COLOR, {
      toolType: measurement.toolType,
      measurementData: {
        _id: measurement._id,
        lesionNamingNumber: measurement.lesionNamingNumber,
        measurementNumber: measurement.measurementNumber,
        color: color,
      },
    });

    measurement.location = location;
    measurement.description = description;
    measurement.color = color;
    if (questionKey && answer) {
      measurement.questionKey = questionKey;
      measurement.answer = answer;
      measurement.isSubForm = isSubForm;
      measurement.subFormName = subFormName;
      measurement.subFormAnnotationId = subFormAnnotationId;
      measurement.question = question;
    }

    if (ROIContourUid) {
      measurement.ROIContourUid = ROIContourUid;
    }
    if (dirty) {
      measurement.dirty = dirty;
    }

    measurementApi.updateMeasurement(measurement.toolType, measurement);
  });

  measurementApi.syncMeasurementsAndToolData();
  if (modality !== 'SM') {
    // Update images in all active viewports
    cornerstone.getEnabledElements().forEach(enabledElement => {
      cornerstone.updateImage(enabledElement.element);
    });
  }
}
