import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { utils, user } from '@ohif/core';
//
import ConnectedViewerRetrieveStudyData from '../connectedComponents/ConnectedViewerRetrieveStudyData';
import useServer from '../customHooks/useServer';
import OHIF from '@ohif/core';
import useQuery from '../customHooks/useQuery';
import usePrevious from '../customHooks/usePrevious';

import { HTTP as FlywheelCommonHTTP } from '@flywheel/extension-flywheel-common';

const { getAuthTokenOnly, getAuthSchema } = FlywheelCommonHTTP.utils.AUTH_UTILS;

const { setLayout } = OHIF.redux.actions;
const { urlUtil: UrlUtil } = utils;

/**
 * Get array of seriesUIDs from param or from queryString
 * @param {*} seriesInstanceUIDs
 * @param {*} location
 */
const getSeriesInstanceUIDs = (seriesInstanceUIDs, routeLocation) => {
  const queryFilters = UrlUtil.queryString.getQueryFilters(routeLocation);
  const querySeriesUIDs = queryFilters && queryFilters['seriesInstanceUID'];
  const _seriesInstanceUIDs = seriesInstanceUIDs || querySeriesUIDs;

  return UrlUtil.paramString.parseParam(_seriesInstanceUIDs);
};

function ViewerRouting({ match: routeMatch, location: routeLocation }) {
  const {
    project,
    location,
    dataset,
    dicomStore,
    studyInstanceUIDs,
    seriesInstanceUIDs,
  } = routeMatch.params;

  // Set the user's default authToken for outbound DICOMWeb requests.
  // Is only applied if target server does not set `requestOptions` property.
  //
  // See: `getAuthorizationHeaders.js`
  let query = useQuery();
  const flywAuthToken = getAuthTokenOnly();
  const flywAuthSchema = getAuthSchema();

  const authToken = flywAuthToken || query.get('token');
  // decorate user methods
  if (authToken) {
    user.getAccessToken = () => authToken;
    user.getAuthSchema = () => flywAuthSchema;
  }

  const server = useServer({ project, location, dataset, dicomStore });
  const studyUIDs = UrlUtil.paramString.parseParam(studyInstanceUIDs);
  const seriesUIDs = getSeriesInstanceUIDs(seriesInstanceUIDs, routeLocation);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(
      setLayout({
        numRows: 1,
        numColumns: 1,
        viewports: [
          {
            height: '100%',
            width: '100%',
          },
        ],
      })
    );
  }, []);
  const isMultiSessionViewEnabled = useSelector(store => {
    return store?.multiSessionData?.isMultiSessionViewEnabled;
  });
  const acquisitionData = useSelector(store => {
    return store?.multiSessionData?.acquisitionData;
  });
  if (isMultiSessionViewEnabled) {
    acquisitionData.forEach(acquisition => {
      if (!studyUIDs.includes(acquisition.studyUid)) {
        studyUIDs.push(acquisition.studyUid);
      }
    });
    if (studyUIDs.length === 1) { // If selecting acquisition in same study
      acquisitionData.forEach(acquisition => {
        if (seriesUIDs && !seriesUIDs.includes(acquisition.seriesUid)) {
          seriesUIDs.push(acquisition.seriesUid);
        }
      });
    }
  }

  if (server && studyUIDs) {
    return (
      <ConnectedViewerRetrieveStudyData
        studyInstanceUIDs={studyUIDs}
        seriesInstanceUIDs={seriesUIDs}
      />
    );
  }

  return null;
}

ViewerRouting.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      studyInstanceUIDs: PropTypes.string.isRequired,
      seriesInstanceUIDs: PropTypes.string,
      dataset: PropTypes.string,
      dicomStore: PropTypes.string,
      location: PropTypes.string,
      project: PropTypes.string,
    }),
  }),
  location: PropTypes.any,
};

export default ViewerRouting;
